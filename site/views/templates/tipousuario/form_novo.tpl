{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>{if {$registro.idTipoUsuario}>0} ALTERAR TIPO DE USUARIO: {$registro.dsTipoUsuario|default:''}{else} INCLUIR NOVO TIPO DE USUARIO{/if}</h1></tt>
            </div>          
            <a href="/tipousuario" class="btn btn-primary"> ABORTAR</a><br>

            <form name="frm-tipousuario" 
                  action="/tipousuario/gravar_tipousuario" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario()">
                <br>
                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idTipoUsuario}>0}
                                <label class="form-group">CODIGO</label>
                                <input type="text" class="form-control" name="idTipoUsuario" id="idTipoUsuario" value="{$registro.idTipoUsuario}" READONLY>           
                        {else}
                                 <label class="form-group">CODIGO</label>
                                 <input type="text" class="form-control" name="idTipoUsuario" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-3">
                        <label class="form-group">DESCRICAO</label>
                        <input type="text" class="form-control" name="dsTipoUsuario" id="dsTipoUsuario" value="{$registro.dsTipoUsuario|default:''}" >           
                    </div> 
                </div> 
                <br>
                  <div class="col-md-3">
                    <div class="row small">
                        <input class="btn btn-primary" type="submit" value="  GRAVAR" name="btnGravar"/>         
                    </div> 
                  </div> 
                <br>
            </form>
            
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/tipousuario/tipousuario_novo.js"></script>



{include file="comuns/footer.tpl"}

