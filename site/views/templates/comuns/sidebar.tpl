<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
            <a class="navbar-brand" href="/dashboard">Thopos</a>          
        </div>        
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">   
        {if isset($smarty.session.user.nome)}
             <ul class="nav navbar-nav navbar-left navbar-">     
                 {$smarty.session.user.menu_sidebar}                
             </ul>        
             <ul class="nav navbar-nav navbar-left navbar-user">
                <li class="dropdown user-dropdown">    
                    <a
                        href="#" id="tirarcor" class="dropdown-toggle" {*{if $smarty.session.user.temMensagem|default:0 eq 1}  style="color: #FF0000" {/if} *}data-toggle="dropdown"><i class="fa fa-user"></i> {if isset($smarty.session.user.nome)}{$smarty.session.user.nome}{/if} <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="/profile"><i class="fa fa-user"></i>&nbsp;&nbsp;Perfil</a></li>                                                
                        <li class="divider"></li>
                        <li><a href="/trocar_senha/troca_senha"><i class="fa fa-key"></i>&nbsp;&nbsp;Trocar Senha</a></li>
{*                        <li class="divider"></li>
                        <li><a onclick ='dashboard.mensagem.montarTelaMensagem();'><i class="fa fa-comments-o"></i>&nbsp;&nbsp;Mensagens</a></li>
*}                        <li class="divider"></li>
                        <li><a href="/login/logout"><i class="fa fa-power-off"></i>&nbsp;&nbsp;Sair</a></li>
                    </ul>
                </li>
             </ul>
        {else}
             <ul class="nav navbar-nav side-nav">
                <li class="active"><a href="/login"><i class="fa fa-dashboard"></i> Login</a></li>
             </ul>
        {/if}
    </div><!-- /.navbar-collapse -->
</nav>