{foreach from=$dados_menu item="menu_principal"}
    {if {$menu_principal.filhos|count}==0}    
        <li><a href="{$menu_principal.link}"><i class="fa fa-bar-chart-o"></i> {$menu_principal.descricao}</a></li>
    {else}
        <li class="dropdown user-dropdown">        
            <a href="{$menu_principal.link}" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {$menu_principal.descricao}<b class="caret"></b></a>        
                <ul class="dropdown-menu">
                    {foreach from=$menu_principal.filhos item="itens_menu"}            
                            <li>                       
                                <a href="{$itens_menu.link}"><i class="fa fa-user"></i> {$itens_menu.descricao}</a>
                            </li>  
                            <li class="divider"></li>                        
                    {/foreach}  
                </ul>            
        </li>                    
    {/if} 
{/foreach}