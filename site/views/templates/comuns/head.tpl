<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
        
    <title>{$title}  - {$nome_aplicacao|default:'Thopos'} </title>
      
    <link rel="shortcut icon" href="/files/images/logopng.png">
    
    <!-- Bootstrap core CSS -->
    <link href="/files/css/bootstrap.css" rel="stylesheet">  <!-- // reislemos (este aqui) -->

    <!-- Add custom CSS here -->
    <link href="/files/css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="/files/font-awesome/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" href="/bower_components/bootstrap/less/variables.less"> -->
    
    <!-- Page Specific CSS -->
{*    <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
*}    <!-- Toast Message -->
    <link rel="stylesheet" href="/files/js/toastmessage/resources/css/jquery.toastmessage.css">
    
 {*   <link rel="stylesheet" href="/files/js/jquery_ui/css/smoothness/jquery-ui-1.10.3.custom.min.css">*}
    <link href="/files/js/jquery_ui/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="/files/css/master.fox.css">
    
    {*<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">*}
    
    {*Geolocalizacao*}
    
{*    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
*}    
    <!-- Bootstrap Core CSS -->
 {*   <link href="/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">*}

    <!-- MetisMenu CSS -->
    <link href="/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="/dist/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="/bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="/files/js/jquery_ui/css/smoothness/jquery.ui.theme.css" rel="stylesheet" type="text/css"/>
    <link href="/files/js/jquery_ui/css/smoothness/minified/jquery.ui.theme.min.css" rel="stylesheet" type="text/css"/>
    <link href="/files/css/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css"/>
    
    <script src="/files/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="/files/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/files/js/markerclusterer.js" type="text/javascript"></script>
    <script src="/files/js/jquery.mask.js" type="text/javascript"></script>
{*    <script src="/files/js/jquery_ui/js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>*}
    <script src="/files/js/jquery_ui/jquery-ui.js" type="text/javascript"></script>
    <script src="/files/js/jquery.price_format.1.3.js" type="text/javascript"></script>
{*    <script type="text/javascript" src="/files/js/dashboard/reload.js" </script> *}
    
    <script type="text/javascript" src="/files/js/dashboard/dashboard.js"></script>        
    
 </head>
{*{date('H:i',$smarty.session.user.registro)} Tempo total da sessão: {$smarty.session.user.limite / 60} minutos *}
<body>
<div id="modal_mensagem">
    {include file="dashboard/mensagem/modal.tpl"}                            
</div>  

    