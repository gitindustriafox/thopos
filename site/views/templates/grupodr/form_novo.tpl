{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>{if {$registro.idGrupoDR}>0} ALTERAR GRUPO DE DESPESA/RECEITA:{$registro.dsGrupoDR|default:''}{else} INCLUIR NOVO GRUPO DE DESPESA/RECEITA{/if}</h1></tt>
            </div>          
            <a href="/grupodr" class="btn btn-primary"> ABORTAR</a><br>

            <form name="frm-grupodr" 
                  action="/grupodr/gravar_grupodr" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario()">
                <br>
                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idGrupoDR}>0}
                                <label for="form-control">CODIGO</label>
                                <input type="text" class="form-control" name="idGrupoDR" id="idGrupoDR" value="{$registro.idGrupoDR}" READONLY>           
                        {else}
                                 <label for="form-control">CODIGO</label>
                                 <input type="text" class="form-control" name="idGrupoDR" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-4">
                        <label for="form-control">DESCRICAO</label>
                        <input type="text" class="form-control" name="dsGrupoDR" id="dsGrupoDR" value="{$registro.dsGrupoDR|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">DEBITO/CREDITO/INVESTIMENTO</label>
                        <input type="text" class="form-control" name="stGrupoDR" id="stGrupoDR" value="{$registro.stGrupoDR|default:'D'}" >           
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">ORCAMENTO</label>
                        <input type="checkbox" class="form-control" name="stOrcamento" id="stOrcamento" value="{$registro.stOrcamento|default:''}" {$checked}  >       
                    </div>                         
                </div> 
                <br>            
                    <input type="submit" value="GRAVAR" name="btnGravar" class="btn btn-primary" />         
                <br>
                <br>
            </form>
            <div class="panel-item panel panel-default"> 
                <div class="panel-heading mostra">
                    <h3> <strong>ITENS DE DESPESAS / RECEITAS</strong> <h3>
                </div> 
                <div class="panel-body mostra" id="painel_colaboradores">
                    <br>
                    <div class="row small" >
                        <h4> &nbsp; ITENS PARA ESTE GRUPO</h4>
                        <br>
                        <div class="col-md-5">
                            <label for="form-control">DESCRIÇÃO DO ITEM</label>
                            <input type="text" class="form-control" {if $registro.idGrupoDR eq ''} disabled {/if}  name="dsItemDR" id="dsItemDR"  value="">       
                        </div> 
                        <br>
                        <div class="col-md-1">
                          <div class="row small">
                              <a class="btn btn-primary" id="btn-adicionaitem" title="ADICIONAR" onclick="grupodr.adicionaritem();" {if $registro.idGrupoDR eq ''} disabled {/if}  >ADICIONAR ITEM</a> 
                          </div> 
                        </div> 
                    </div>
                    <br>
                    <div id="mostraritens">
                         {include file="grupodr/itens.html"}
                    </div>
                </div>
            </div>    
            
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
{*<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
*}<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/grupodr/grupodr.js"></script>



{include file="comuns/footer.tpl"}

