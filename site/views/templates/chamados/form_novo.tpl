{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="modal_fotos">
        {include file="chamados/modalFotos/modal.tpl"}                            
    </div> 
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <br><br>
            <form name="frm-chamado_nova" 
                  action="/chamados/gravar_chamado" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario()" id="frm-chamado_nova">
                <br>
                <input type="hidden" class="form-control" name="idChamado" id="idChamado" value="{$registro.id|default:''}">           
                
                <div class="panel-body panel panel-default" > 
                    <div class="row small">
                        <li class="dropdown user-dropdown">
                            <a
                                href="#" id="menuchamado" class="dropdown-toggle"  style="color: #000000; font-size: large" data-toggle="dropdown"> 
                                <i class="fa fa-list" style="font-size: large;"> &nbsp;&nbsp; Novo Ticket #{$registro.id|default:''}</i> 
                            </a>
                            <ul class="dropdown-menu">
                               <li><a onclick="chamado.gravar_chamado();" ><i class="fa fa-calendar"></i>&nbsp;&nbsp;Salvar as informações</a></li>                                                
                               {if $registro.id|default:'' neq ''}
                                    <li class="divider"></li>
                                    <li><a onclick="chamado.selecionarfotoR({$registro.id|default:''});"><i class="fa fa-adjust"></i>&nbsp;&nbsp;Anexar arquivos</a></li>
                                {/if} 
                               <li class="divider"></li>
                               <li><a href="/menuChamados"><i class="fa fa-eject"></i>&nbsp;&nbsp;Sair</a></li>
                            </ul>
                        </li>
                    </div> 
                </div>
                
                <div  class="row small">
                    <div class="col-md-12">
                        <label for="form-control">Descrição do chamado</label>
                        <textarea class="form-control"  rows="5" name="dsChamado"  id="dsChamado">{$registro.dsChamado|default:''}</textarea>           
                    </div> 
                </div> 
                <div class="row small">
                    <div class="col-md-4">
                        <label for="form-control">Prioridade</label>
                        <select class="form-control"  id="idPrioridade" name="idPrioridade"> 
                                {include file="chamados/lista_prioridade.tpl"}       
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label for="form-control">Data limite da necessidade</label>
                        <input type="text" class="form-control datetime" name="dtLimite" id="dtLimite" value="{$registro.dtLimite|date_format:'%d/%m/%Y %H:%M'|default:''}" >           
                    </div> 
                    <div class="col-md-4">
                        <label for="form-control">Setor que irá executar</label>
                        <select class="form-control"  id="idSetor" name="idSetor"> 
                                {include file="chamados/lista_setor.tpl"}       
                        </select>
                    </div>
                </div>                                             
                <div  class="row small">
                    <div class="col-md-12">
                        <label for="form-control">Observação</label>
                        <textarea class="form-control"  rows="5" name="dsObservacao"  id="dsObservacao">{$registro.obs|default:''}</textarea>           
                    </div> 
                </div> 
                <br>
                <br>  
             </div>
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
{*<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>*}
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/chamados/chamados.js"></script>
<script src="/files/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>
<link href="/files/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css"/>

{include file="comuns/footer.tpl"}

