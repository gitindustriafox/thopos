<div id="chamadosfotos_show" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-lg" role="document" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Anexar arquivos para o ticket</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form name="frm-importar-fotos" 
                    action="/chamados/adicionarfoto" 
                    method="POST" 
                    enctype="multipart/form-data">
                    <div class="modal-body"></div>
            </form>    
        </div>
    </div>
</div>