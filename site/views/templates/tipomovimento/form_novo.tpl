{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>{if {$registro.idTipoMovimento}>0} ALTERAR TIPO DE MOVIMENTO: {$registro.dsTipoMovimento|default:''}{else} INCLUIR NOVO TIPO DE MOVIMENTO{/if}</h1></tt>
            </div>          
            <a href="/tipomovimento" class="btn btn-primary"> ABORTAR</a><br>

            <form name="frm-tipomovimento" 
                  action="/tipomovimento/gravar_tipomovimento" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario()">
                <br>
                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idTipoMovimento}>0}
                                <label for="form-control">CODIGO</label>
                                <input type="text" class="form-control" name="idTipoMovimento" id="idTipoMovimento" value="{$registro.idTipoMovimento}" READONLY>           
                        {else}
                                 <label for="form-control">CODIGO</label>
                                 <input type="text" class="form-control" name="idTipoMovimento" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-4">
                        <label for="form-control">DESCRICAO</label>
                        <input type="text" class="form-control" name="dsTipoMovimento" id="dsTipoMovimento" value="{$registro.dsTipoMovimento|default:''}" >           
                    </div> 
                    <div class="col-md-4">
                        <label for="form-control">ENTRADA OU SAIDA (E/S)</label>
                        <input type="text" class="form-control" name="stDC" id="stDC" value="{$registro.stDC|default:''}" >           
                    </div> 
                </div> 
                <br>            
                    <input type="submit" value="GRAVAR" name="btnGravar" class="btn btn-primary" />         
                <br>
                <br>
            </form>
            
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/tipomovimento/tipomovimento_novo.js"></script>



{include file="comuns/footer.tpl"}

