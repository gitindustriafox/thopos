{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>{if {$registro.idLocalReuniao}>0} ALTERAR LOCAL DE REUNIAO: {$registro.dsLocalReuniao|default:''}{else} INCLUIR NOVO LOCAL DE REUNIAO{/if}</h1></tt>
            </div>          
            <a href="/localreuniao" class="btn btn-primary"> ABORTAR</a><br>

            <form name="frm-localreuniao" 
                  action="/localreuniao/gravar_localreuniao" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario()">
                <br>
                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idLocalReuniao}>0}
                                <label for="form-control">CODIGO</label>
                                <input type="text" class="form-control" name="idLocalReuniao" id="idLocalReuniao" value="{$registro.idLocalReuniao}" READONLY>           
                        {else}
                                 <label for="form-control">CODIGO</label>
                                 <input type="text" class="form-control" name="idLocalReuniao" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-4">
                        <label for="form-control">DESCRICAO</label>
                        <input type="text" class="form-control" name="dsLocalReuniao" id="dsLocalReuniao" value="{$registro.dsLocalReuniao|default:''}" >           
                    </div> 
                </div> 
                <br>            
                    <input type="submit" value="GRAVAR" name="btnGravar" class="btn btn-primary" />         
                <br>
                <br>
            </form>
            
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
{*<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
*}<script src="/files/js/localreuniao/localreuniao_novo.js"></script>



{include file="comuns/footer.tpl"}

