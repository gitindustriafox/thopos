{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}  
            <br>

    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="alert alert-info" >'
            <h1>{if {$registro.idMenu}>0} ALTERAR MENU: {$registro.descricao|default:''}{else} INCLUIR NOVO MENU{/if}</h1>
        </div>
        <br> 
        <a href="/menu" class="btn btn-primary"> ABORTAR</a><br>        
        <form name="frm-menu" 
              action="/menu/gravar_menu" 
              method="POST" 
              enctype="multipart/form-data"
              onsubmit="return validaFormulario()">
            <br> 
            
            <div class="row small">
                <div class="col-md-1">
                    {if {$registro.idMenu}>0}
                            <label class="form-group">ID</label>
                            <input type="text" class="form-control" name="idMenu" id="idMenu" value="{$registro.idMenu}" READONLY>           
                    {else}
                             <label class="form-group">ID</label>
                             <input type="text" class="form-control" name="idMenu" value="" READONLY>           
                    {/if}                     
                </div> 
                <div class="col-md-4">
                    <label class="form-group">DESCRICAO</label>
                    <input type="text" class="form-control" name="descricao" id="descricao" value="{$registro.descricao|default:''}" >           
                </div> 
                <div class="col-md-3">
                    <label class="form-group">URL</label>
                    <input type="text" class="form-control" name="url" id="url" value="{$registro.url|default:''}" >           
                </div> 
                <div class="col-md-2">
                    <label class="form-group">GRUPO (ID DO PAI)</label>
                    <input type="text" class="form-control" name="parent_menu" id="parent_menu" value="{$registro.parent_menu|default:''}" >           
                </div> 
                <div class="col-md-1">
                    <label class="form-group">TARGET</label>
                    <input type="text" class="form-control" name="target" id="target" value="{$registro.target|default:''}" >           
                </div> 
                <div class="col-md-1">
                    <label class="form-group">ORDEM</label>
                    <input type="text" class="form-control" name="ordem" id="ordem" value="{$registro.ordem|default:''}" >           
                </div> 
                
            </div> 
            
            <div type="hidden" class="form-group">        
                <input  type="hidden" class="form-control" name="idMenu" id="id_menu" value="{$registro.idMenu}" />
            </div>               
            <input type="hidden" name="tipo" value="{$registro.tipo}" />
            <br>          
            <input type="hidden" name="status" value="{$registro.status}" />
            <input class="btn btn-primary" type="submit" value="GRAVAR" name="btnGravar" />
            <br>
        </form>
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<!-- JS Especifico do Controller -->
<script src="/files/js/menu/menu_novo.js"></script>

{include file="comuns/footer.tpl"}

