{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>{if {$registro.idConversao}>0} ALTERAR UNIDADE DE CONVERSAO: {$registro.dsConversao|default:''}{else} INCLUIR NOVA UNIDADE DE CONVERSAO{/if}</h1></tt>
            </div>          
            <a href="/conversao" class="btn btn-primary"> ABORTAR</a><br>

            <form name="frm-conversao" 
                  action="/conversao/gravar_conversao" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario();">
                <br>
                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idConversao}>0}
                                <label for="form-control">CODIGO</label>
                                <input type="text" class="form-control" name="idConversao" id="idConversao" value="{$registro.idConversao}" READONLY>           
                        {else}
                                 <label for="form-control">CODIGO</label>
                                 <input type="text" class="form-control" name="idConversao" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-3">
                        <label for="form-control">DESCRICAO</label>
                        <input type="text" class="form-control" name="dsConversao" id="dsConversao" value="{$registro.dsConversao|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="unidadeorigem">UNIDADE ORIGEM</label>
                            <select class="form-control" name="idUnidadeOrigem" id="idUnidadeOrigem">
                                {html_options options=$lista_unidadeorigem selected=$registro.idUnidadeOrigem}
                            </select>                      
                        </div>
                    </div>                     
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="unidadedestino">UNIDADE DESTINO</label>
                            <select class="form-control" name="idUnidadeDestino" id="idUnidadeDestino">
                                {html_options options=$lista_unidadedestino selected=$registro.idUnidadeDestino}
                            </select>                      
                        </div>
                    </div>                     
                    <div class="col-md-1">
                        <label for="form-control">SINAL</label>
                        <input type="text" class="form-control" name="sinal" id="sinal" value="{$registro.sinal|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">VALOR</label>
                        <input type="text" class="form-control" name="valordaconversao" id="valordaconversao" value="{$registro.valordaconversao|default:''}" >           
                    </div> 
                </div>    
                <br>
                  <div class="col-md-3">
                    <div class="row small">
                        <input class="btn btn-primary" type="submit" value="  GRAVAR" name="btnGravar"/>         
                    </div> 
                  </div> 
                <br>
            </form>
            
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/conversao/conversao_novo.js"></script>



{include file="comuns/footer.tpl"}

