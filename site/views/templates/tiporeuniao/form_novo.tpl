{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>{if {$registro.idTipoReuniao}>0} ALTERAR TIPO DE REUNIAO: {$registro.dsTipoReuniao|default:''}{else} INCLUIR NOVO TIPO DE REUNIAO{/if}</h1></tt>
            </div>          
            <a href="/tiporeuniao" class="btn btn-primary"> ABORTAR</a><br>

            <form name="frm-tiporeuniao" 
                  action="/tiporeuniao/gravar_tiporeuniao" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario()">
                <br>
                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idTipoReuniao}>0}
                                <label for="form-control">CODIGO</label>
                                <input type="text" class="form-control" name="idTipoReuniao" id="idTipoReuniao" value="{$registro.idTipoReuniao}" READONLY>           
                        {else}
                                 <label for="form-control">CODIGO</label>
                                 <input type="text" class="form-control" name="idTipoReuniao" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-4">
                        <label for="form-control">DESCRICAO</label>
                        <input type="text" class="form-control" name="dsTipoReuniao" id="dsTipoReuniao" value="{$registro.dsTipoReuniao|default:''}" >           
                    </div> 
                </div> 
                <br>            
                    <input type="submit" value="GRAVAR" name="btnGravar" class="btn btn-primary" />         
                <br>
                <br>
            </form>
            
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
{*<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
*}<script src="/files/js/tiporeuniao/tiporeuniao_novo.js"></script>



{include file="comuns/footer.tpl"}

