{foreach from=$padrao_lista item="data"}
  <tr id="line_{$data.idCentroCusto}">
    <td>{$data.idCentroCusto}</td>
    <td>{$data.cdCentroCusto}</td>    
    <td>{$data.dsCentroCusto}</td>    
    <td>{$data.stAnalitico}</td>    
    <td>{$data.idCentroCustoSuperior}</td>    
    <td>        
      <span class="link_falso" onclick="link_edita_padrao({$data.idCentroCusto})">
        <img src="/files/images/cad_edit.png" alt="Alterar" width="25" height="25" title="Alterar">
      </span>
      <span class="link_falso" onclick="link_exclui_padrao({$data.idCentroCusto})">
        <img src="/files/images/cad_exclui.png" alt="Alterar" width="25" height="25" title="Alterar">
      </span> 
    </td>
  </tr>   
  {foreachelse}    
  <tr>    
    <td>Nenhum centro de custo enontrado</td>
  </tr>
{/foreach}

