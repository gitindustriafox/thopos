{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <h1>{if {$registro.idUsuario}>0} ALTERAR DADOS DO USUARIO {else} INCLUIR NOVO USUARIO {/if}</h1>
            </div>  

            <a href="/usuarios" class="btn btn-primary"> ABORTAR</a><br> <br>
            <form name="frm-usuario" id="frm-usuario" action="/usuarios/gravar_usuario" method="POST" enctype="multipart/form-data" onsubmit="return validaFormulario();">
                <div class="form-group">
                    <label for="idUsuario">            
                           {if {$registro.idUsuario}>0}
                                ID:{$registro.idUsuario}

                           {else}
                                ID: 
                           {/if} 
                    </label>
                    <div type="hidden" class="form-group">        
                        <input type="hidden" class="form-control" name="idUsuario" id="idUsuario" value="{$registro.idUsuario}" />
                    </div>
                    <div class="row small">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nome">NOME</label>
                                <input type="text" class="form-control" name="dsUsuario" id="nome" value="{$registro.dsUsuario}" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="nome">SENHA</label>
                                <input type="password" class="form-control" name="senha" id="senha"  >           
                            </div>
                        </div>              
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="nome">TIPO DE USUARIO</label>
                                <select class="form-control" name="idTipoUsuario" id="idTipoUsuario">
                                    {html_options options=$lista_tipos selected=$registro.idTipoUsuario}
                                </select>                      
                            </div>
                        </div>                                      
                    </div> 
                    <div class="row small">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">E-MAIL</label>
                                <input type="text" class="form-control" name="email" id="email" onblur="validaEmail()" value="{$registro.email}">           
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="telefone1">CELULAR</label>
                                <input type="text" class="form-control" name="telefone1" id="telefone1" value="{$registro.telefone1}" >           
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="telefone2">SKYPE</label>
                                <input type="text" class="form-control" name="telefone2" id="telefone2" value="{$registro.telefone2}" >           
                            </div>
                        </div>                        
                    </div>     
                    <div class="row small">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="nome">DISTRIBUIDOR DE TICKETS</label>
                                <select class="form-control" name="idDistribuidorTickets" id="idDistribuidorTickets">
                                    {html_options options=$lista_distribuidor selected=$registro.idDistribuidorTickets|default:''}
                                </select>                      
                            </div>
                        </div>                                      
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="nome">EXECUTADOR DE TICKETS</label>
                                <select class="form-control" name="idExecutadorTickets" id="idExecutadorTickets">
                                    {html_options options=$lista_executador selected=$registro.idExecutadorTickets|default:''}
                                </select>                      
                            </div>
                        </div>                                      
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="nome">SETOR DO USUARIO</label>
                                <select class="form-control" name="idSetor" id="idSetor">
                                    {html_options options=$lista_setor selected=$registro.idSetor}
                                </select>                      
                            </div>
                        </div>                                      
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="nome">GRUPO DE AGENDA</label>
                                <select class="form-control" name="idGrupoAgenda" id="idGrupoAgenda">
                                    {html_options options=$lista_grupoagenda selected=$registro.idGrupoAgenda}
                                </select>                      
                            </div>
                        </div>                                      
                    </div> 
                    <br>            
                    <input type="submit" value="GRAVAR" name="btnGravar" class="btn btn-primary" />         
                </div>
            </form>
            <br>   
            {if {$registro.idUsuario}>0} 
                    <!--Altere daqui pra cima-->
                    <div class="alert alert-info" >
                        <h1>PERFIS DO USUARIO</h1>
                    </div>          
                    <div class="panel-body">
                    {if ($lista_Perfis > 0)}
                        <form name="frm-usuario-perfil" 
                              action="/usuarios/novo_usuario_perfil/" 
                              method="POST" enctype="multipart/form-data">
                            <div class="row small">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="idPerfil_usuario">PERFIS</label>
                                        <select class="form-control" name="idPerfil" id="idPerfil">
                                            {html_options options=$lista_Perfis}
                                        </select>
                                        <input type="hidden" name="idUsuarioPerfil" id="idUsuarioPerfil"value="{$registro.idUsuario}" />
                                    </div>
                                </div>
                            </div>
                            <div class="row small">
                                <div class="col-md-12">
                                    <input type="submit" value="INCLUIR" name="btnInserir" id="btnInserir" class="btn btn-primary"/>
                                </div>
                            </div>
                        </form>
                    {else}
                        TODOS OS PERFIS JA ESTAO DEFINIDOS PARA ESTE USAURIO.
                    {/if}
                    </div>        

                    <div class="panel-body">                    
                        <table class="table" border="1">
                            <thead>
                                <tr>                        
                                    <th>PERFIL</th>
                                    <th>STATUS</th>
                                    <th>ACAO</th>
                                </tr>
                            </thead>
                            <tbody>                        
                                {foreach from=$lista_Perfil_Usuario item="linha"}
                                <tr>                                                
                                    <td>{$linha.dsPerfil}</td>
                                    <td>{if $linha.stStatus=1} ATIVO {else} INATIVO {/if}</td>                        
                                    <td><a class="glyphicon glyphicon-trash" href="/usuarios/del_usuario_perfil/idUsuario/{$linha.idUsuario}/idPerfil/{$linha.idPerfil}">Excluir</a> </td>
                                </tr>
                                {foreachelse}
                                <tr><td colspan="3">NENHUM PERFIL ASSOCIADO A ESTE USUARIO</td></tr>
                                {/foreach}                                                  
                            </tbody>
                        </table> 
                    </div>  
                    <br>        
                    <!--Altere daqui pra cima-->
                    <div class="alert alert-info" >
                        <h1>PROJETOS DESTE USUARIO</h1>
                    </div>                                  
                    <div class="panel-body">
                    {if ($lista_Projetos > 0)}
                        <form name="frm-usuario-projeto" 
                              action="/usuarios/novo_prodProjetoUsuario/" 
                              method="POST" enctype="multipart/form-data">
                            <div class="row small">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="idProjeto">PROJETOS</label>
                                        <select class="form-control" name="idProjeto" id="idProjeto">
                                            {html_options options=$lista_Projetos}
                                        </select>
                                        <input type="hidden" name="idUsuario" id="idUsuario"value="{$registro.idUsuario}" />
                                    </div>
                                </div>
                            </div>
                            <div class="row small">
                                <div class="col-md-12">
                                    <input type="submit" value="Inserir" name="btnInserir" id="btnInserir" class="btn btn-primary"/>
                                </div>
                            </div>
                        </form>
                    {else}
                        TODOS OS PROJETOS JA ESTAO DEFINIDOS PARA ESTE USUARIO.
                    {/if}
                    </div>        
                    <br>        
                    <div class="panel-body">                    
                        <table class="table" border="1">
                            <thead>
                                <tr>                        
                                    <th>PROJETOS</th>
                                    <th>ACAO</th>
                                </tr>
                            </thead>
                            <tbody>                        
                                {foreach from=$lista_prodProjetoUsuarios item="linha"}
                                <tr>                                                
                                    <td>{$linha.projeto}</td>                                
                                    <td><a class="glyphicon glyphicon-trash" href="/usuarios/del_prodProjetoUsuario/idUsuario/{$linha.idUsuario}/idProjeto/{$linha.idProjeto}">Excluir</a> </td>
                                </tr>
                                {foreachelse}
                                <tr><td colspan="3">NENHUM PROJETO VINCULADO A ESTE USUARIO</td></tr>
                                {/foreach}                                                  
                            </tbody>
                        </table> 
                    </div>    

            {else} 
                NENHUM PERFIL E PROJETO VINCULADO A ESTE USUARIO
            {/if}
        </div>
    </div>
</div>                
<!-- JavaScript -->
<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<!-- JS Especifico do Controller -->
<script src="/files/js/usuarios/usuario_novo.js"></script>

{include file="comuns/footer.tpl"}

