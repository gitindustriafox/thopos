{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>{if {$registro.idColaborador}>0} ALTERAR COLABORADOR: {$registro.dsColaborador|default:''} {else} INCLUIR COLABORADOR{/if}</h1></tt>
            </div>          

            <form name="frm-colaborador" 
                  action="/colaborador/gravar_colaborador" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario();">
                <br>
                <a href="/colaborador" class="btn btn-primary"> ABORTAR</a>
                <input class="btn btn-primary" type="submit" value="  GRAVAR" name="btnGravar"/>         
                <br>
                <br>
                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idColaborador}>0}
                                <label for="form-control">CODIGO</label>
                                <input type="text" class="form-control" name="idColaborador" id="idColaborador" value="{$registro.idColaborador}" READONLY>           
                        {else}
                                 <label for="form-control">CODIGO</label>
                                 <input type="text" class="form-control" name="idColaborador" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">NOME DO COLABORADOR</label>
                        <input type="text" class="form-control" name="descricao" id="descricao" value="{$registro.dsColaborador|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="nomemaoobra">MAO DE OBRA</label>
                            <select class="form-control" name="idMaoObra" id="idMaoObra">
                                {html_options options=$lista_maoobra selected=$registro.idMaoObra}
                            </select>                      
                        </div>
                    </div> 
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="nomesetor">SETOR</label>
                            <select class="form-control" name="idSetor" id="idSetor">
                                {html_options options=$lista_setor selected=$registro.idSetor}
                            </select>                      
                        </div>
                    </div> 
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="nomecargo">CARGO</label>
                            <select class="form-control" name="idCargo" id="idCargo">
                                {html_options options=$lista_cargo selected=$registro.idCargo}
                            </select>                      
                        </div>
                    </div> 
                    <div class="col-md-3">
                        <div class="form-group">
                                 <label for="form-control">EMAIL:</label>
                                 <input type="text" class="form-control" id="dsEmail" name="dsEmail" value="{$registro.dsEmail|default:''}" >           
                        </div>
                    </div> 
                </div> 
                <br>
                <div class="row small">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="fazparteagenda">FAZ PARTE DA AGENDA DE SERVICOS</label>
                            <select class="form-control" name="stFazParteAgenda" id="stFazParteAgenda">
                                {html_options options=$lista_fazparte selected=$registro.stFazParteAgenda}
                            </select>                      
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                                 <label for="form-control">QUANTIDADE DE HORAS/DIA</label>
                                 <input type="text" class="form-control" id="qtHorasDia" name="qtHorasDia" value="{$registro.qtHorasDia|default:''}" >           
                        </div>
                    </div> 
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="usuario">USUARIO DO SISTEMA</label>
                            <select class="form-control" name="idUsuario" id="idUsuario">
                                {html_options options=$lista_usuario selected=$registro.idUsuario}
                            </select>                      
                        </div>
                    </div> 
                </div> 
                <br>
                  <div class="col-md-3">
                    <div class="row small">
                    </div> 
                  </div> 
                <br>
            </form>
            
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/colaborador/colaborador_novo.js"></script>



{include file="comuns/footer.tpl"}

