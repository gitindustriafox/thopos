{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>{if {$registro.idClasseAtivo}>0} ALTERAR CLASSE DE ATIVO:{$registro.dsClasseAtivo|default:''}{else} INCLUIR NOVA CLASSE DE ATIVO{/if}</h1></tt>
            </div>          
            <a href="/classeativo" class="btn btn-primary"> ABORTAR</a><br>

            <form name="frm-classeativo" 
                  action="/classeativo/gravar_classeativo" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario()">
                <br>
                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idClasseAtivo}>0}
                                <label for="form-control">CODIGO</label>
                                <input type="text" class="form-control" name="idClasseAtivo" id="idClasseAtivo" value="{$registro.idClasseAtivo}" READONLY>           
                        {else}
                                 <label for="form-control">CODIGO</label>
                                 <input type="text" class="form-control" name="idClasseAtivo" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-4">
                        <label for="form-control">DESCRICAO</label>
                        <input type="text" class="form-control" name="dsClasseAtivo" id="dsClasseAtivo" value="{$registro.dsClasseAtivo|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">TAXA DE DEPRECIAÇÃO</label>
                        <input type="text" class="form-control" name="vlTaxaDepreciacao" id="vlTaxaDepreciacao" value="{$registro.stClasseAtivo|default:'D'}" >           
                    </div> 
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="tipoativo">TIPO DE ATIVO</label>
                            <select class="form-control" name="idTipoAtivo" id="idTipoAtivo">
                                    {include file="classeativo/lista_tipoativo.tpl"}     
                            </select>                                    
                        </div>
                    </div>                     
                </div> 
                <br>            
                    <input type="submit" value="GRAVAR" name="btnGravar" class="btn btn-primary" />         
                <br>
                <br>
            </form>
            <div class="panel-item panel panel-default"> 
                <div class="panel-heading mostra">
                    <h3> <strong>GRUPOS</strong> <h3>
                </div> 
                <div class="panel-body mostra" id="painel_colaboradores">
                    <br>
                    <div class="row small" >
                        <h4> &nbsp; GRUPOS PARA ESTA CLASSE</h4>
                        <br>
                        <div class="col-md-5">
                            <label for="form-control">DESCRIÇÃO DO GRUPO</label>
                            <input type="text" class="form-control" {if $registro.idClasseAtivo eq ''} disabled {/if}  name="dsGrupoAtivo" id="dsGrupoAtivo"  value="">       
                        </div> 
                        <br>
                        <div class="col-md-1">
                          <div class="row small">
                              <a class="btn btn-primary" id="btn-adicionaitem" title="ADICIONAR" onclick="classeativo.adicionaritem();" {if $registro.idClasseAtivo eq ''} disabled {/if}  >ADICIONAR GRUPO</a> 
                          </div> 
                        </div> 
                    </div>
                    <br>
                    <div id="mostraritens">
                         {include file="classeativo/itens.html"}
                    </div>
                </div>
            </div>    
            
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
{*<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
*}<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/classeativo/classeativo.js"></script>



{include file="comuns/footer.tpl"}

