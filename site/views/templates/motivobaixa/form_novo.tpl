{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>{if {$registro.idMotivoBaixa}>0} ALTERAR MOTIVO DE BAIXA: {$registro.dsMotivoBaixa|default:''}{else} INSERIR NOVO MOTIVO DE BAIXA {/if}</h1></tt>
            </div>          
            <a href="/motivobaixa" class="btn btn-primary"> ABORTAR</a><br>

            <form name="frm-motivobaixa" 
                  action="/motivobaixa/gravar_motivobaixa" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario()">
                <br>

                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idMotivoBaixa}>0}
                                <label for="form-control">ID</label>
                                <input type="text" class="form-control" name="idMotivoBaixa" id="idMotivoBaixa" value="{$registro.idMotivoBaixa}" READONLY>           
                        {else}
                                 <label for="form-control">ID</label>
                                 <input type="text" class="form-control" name="idStatusMaquina" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-3">
                        <label for="form-control">DESCRICAO</label>
                        <input type="text" class="form-control" name="dsMotivoBaixa" id="dsMotivoBaixa" value="{$registro.dsMotivoBaixa|default:''}" >           
                    </div> 
                </div>         
                
                <br>            
                    <input type="submit" value="GRAVAR" name="btnGravar" class="btn btn-primary" />         
                <br>
                <br>
            </form>
            
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/motivobaixa/motivobaixa_novo.js"></script>



{include file="comuns/footer.tpl"}

