{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    <div id="modal_parcelas">
        {include file="financeiro/parcelas/modal.tpl"}                            
    </div>       
    {include file="comuns/sidebar.tpl"}   
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>CONTA A PAGAR / A RECEBER</h1></tt>
            </div> 
                <br>
                <div class="row small">
                    <h3> &nbsp; INFORMACOES DO CABECALHO DA CONTA: </h3>
                    <br>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="financeiro">ID</label>
                            <input type="text" class="form-control" name="idFinanceiro" id="idFinanceiro" value="{$registro.idFinanceiro|default:''}" READONLY>           
                        </div>
                    </div>              
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="tipo">TIPO DE CONTA</label>
                            <select class="form-control" name="idTipo" id="idTipo" value="{$registro.idTipo|default:''}">
                                {html_options options=$lista_tipo selected=$registro.idTipo|default:''}
                            </select>                                    
                        </div>
                    </div>                                                         
                        
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="Parceiro">PARCEIRO</label>                        
                            <input data-label="Parceiro" type="text" data-tipo="dsParceiro" class="form-control complete_Parceiro" name="dsParceiro"  id="dsParceiro" value="{$registro.dsParceiro|default:''}"/>
                            <input type="hidden" id="idParceiro" value="{$registro.idParceiro|default:''}" name="idParceiro"/>
                        </div>
                    </div>                     
                    <div class="col-md-1">
                        <label for="form-control">PEDIDO</label>
                        <input type="text" class="form-control" name="idPedido" id="idPedido" value="{$registro.idPedido|default:''}">       
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">NOTA FISCAL</label>
                        <input type="text" class="form-control" name="nrNF" id="nrNF" value="{$registro.nrNF|default:''}">       
                    </div>                 
                    <div class="col-md-1">
                        <label for="form-control">DOCUMENTO</label>
                        <input type="text" class="form-control" name="cdDocumento" id="cdDocumento" value="{$registro.cdDocumento|default:''}">       
                    </div>   
                    <div class="col-md-1">
                        <label for="form-control">DT DOCUMENTO</label>
                        <input type="text" class="form-control obg standard-mask-date standard-form-date standard-form-require" name="dtEmissao" id="dtEmissao" value="{$registro.dtEmissao|date_format:'%d/%m/%Y'|default:''}" >           
                    </div> 
                </div>
                <div class="row small">                    
                    <div class="col-md-3">
                        <label for="form-control">OBSERVACAO</label>
                        <input type="text" class="form-control" name="dsObservacao"  id="dsObservacao" value="{$registro.dsObservacao|default:''}"> 
                    </div> 
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="situacao">SITUACAO</label>
                            <select class="form-control" readonly name="stStatus" id="stStatus" value="{$registro.stStatus|default:''}">
                                {html_options options=$lista_status selected=$registro.stStatus|default:''}
                            </select>                                    
                        </div>
                    </div>        
                    <div class="col-md-1">
                        <label for="form-control">VALOR </label>
                        <input type="text" class="form-control valor" name="vlTotal" id="vlTotal" value="{$registro.vlTotal|default:''}"> 
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">VENCIMENTO PRIMEIRA</label>
                        <input type="text" {if $registro.idFinanceiro} readonly {/if} class="form-control obg standard-mask-date standard-form-date standard-form-require" name="dtVencimento" id="dtVencimento">           
                    </div>    
                    <div class="col-md-1">
                        <label for="form-control">QTDE PARCELAS</label>
                        <input type="text" {if $registro.idFinanceiro} readonly {/if} class="form-control" name="qtParcelas" id="qtParcelas" value="">      
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">INTERVALO EM DIAS</label>
                        <input type="text" {if $registro.idFinanceiro} readonly {/if}  class="form-control" name="qtIntervalo" id="qtIntervalo" value="">      
                    </div> 
                    <br>
                    <div class="col-md-3">
                        <a class="btn btn-primary" id="btn-gravarcabecalho" title="Clique aqui para gravar o cabeçalho da Financeiro" onclick="financeiro.gravarcabecalho();" {*{if $registro.idFinanceiro  neq  ''}  disabled {/if}*}  >GRAVAR</a> 
                        <a class="btn btn-primary" id="btn-sairtela" title="Clique aqui para sair da tela e voltar a lista de solicitacoes" href="/financeiro"  onclick="financeiro.desabilitaid();"> ABORTAR</a>               
                        {if $registro.idFinanceiro} 
                            <a class="btn btn-primary" id="btn-aplicacao" title="Clique aqui para definir a aplicação" onclick="financeiro.aplicacao('{$registro.idFinanceiro}')"> APLICACAO</a>           
                        {/if}    
                    </div> 
                </div> 
                <div class="row small">                    
                </div> 
                <br>
                <input type="text" class="form-control hidden" name="idFinanceiroParcela" id="idFinanceiroParcela">                           
                {if $registro.idFinanceiro}
                    {include file="financeiro/lista_parcelas.html"}
                {/if}
            
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
<script src="/files/js/financeiro/financeiro.js"></script>

{*<script src="/files/js/jquery.price_format.1.3"></script>
<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
*}<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
{*<script type="text/javascript" src="/files/default/js/jquery.price/jquery.price_format.1.3.js"></script>*}

{include file="comuns/footer.tpl"}

