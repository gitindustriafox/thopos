    <input type="text" class="form-control hidden" name="idFinanceiro" id="idFinanceiro" value="{$idFinanceiro|default:''}">           
    <div class="form-group">
        <div class="row small">
            <div class="col-md-4">
                <label for="form-control">VALOR DA PARCELA</label>
                <input type="text" class="form-control valor" name="vlParcela" id="vlParcela" > 
            </div> 
            <div class="col-md-4">
                <label for="form-control">VENCIMENTO PARCELA</label>
                <input type="text" class="form-control obg standard-mask-date standard-form-date standard-form-require" name="dtVencimento" id="dtVencimento">           
            </div>    
            <br>
            <div class="col-md-4">
              <div class="row small">
                   <a class="btn btn-primary gravar" data-dismiss="modal" onclick="financeiro.adicionarparcela();">GRAVAR</a>
                   <a class="btn btn-primary abortar" data-dismiss="modal" onclick="financeiro.clear_log();">ABORTAR</a>
              </div> 
            </div>             
        </div>    
    </div>
<script src="/files/js/financeiro/financeiro.js" type="text/javascript"></script>