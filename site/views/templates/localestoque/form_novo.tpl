{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>{if {$registro.idLocalEstoque}>0} ALTERAR O LOCAL DE ESTOQUE - {$registro.dsLocalEstoque|default:''}{else} INCLUIR NOVO LOCAL DE ESTOQUE{/if}</h1></tt>
            </div>          
            <a href="/localestoque" class="btn btn-primary"> ABORTAR</a><br>

            <form name="frm-localestoque" 
                  action="/localestoque/gravar_localestoque" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario();">
                <br>
                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idLocalEstoque}>0}
                                <label for="form-control">ID</label>
                                <input type="text" class="form-control" name="idLocalEstoque" id="idLocalEstoque" value="{$registro.idLocalEstoque}" READONLY>           
                        {else}
                                 <label for="form-control">ID</label>
                                 <input type="text" class="form-control" name="idLocalEstoque" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">CODIGO</label>
                        <input type="text" class="form-control" name="cdLocalEstoque" id="cdLocalEstoque" value="{$registro.cdLocalEstoque|default:''}" >           
                    </div> 
                    <div class="col-md-3">
                        <label for="form-control">DESCRICAO</label>
                        <input type="text" class="form-control" name="dsLocalEstoque" id="dsLocalEstoque" value="{$registro.dsLocalEstoque|default:''}" >           
                    </div> 
                    <div class="col-md-3">
                        <label for="form-control">SINTETICO/ANALITICO (S/A)</label>
                        <input type="text" class="form-control" name="stAnalitico" id="stAnalitico" value="{$registro.stAnalitico|default:''}" >           
                    </div> 
                    <div class="col-md-3">
                        <label for="form-control">ID DO PAI (SINTETICO)</label>
                        <input type="text" class="form-control" name="idLocalEstoqueSuperior" id="idLocalEstoqueSuperior" value="{$registro.idLocalEstoqueSuperior|default:''}" >           
                    </div> 
                </div>  
                <br>                    
                <div class="col-md-3">
                  <div class="row small">
                      <input class="btn btn-primary" type="submit" value="GRAVAR" name="btnGravar"/>         
                  </div> 
                </div> 
                <br>
            </form>
                
            
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/localestoque/localestoque.js"></script>



{include file="comuns/footer.tpl"}

