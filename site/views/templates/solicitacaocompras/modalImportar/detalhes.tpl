    <div class="form-group">
        <input type="text" class="form-control hidden" name="idSolicitacao" value="{$idSolicitacao}">                                       
        <input type="text" class="form-control hidden" name="idParceiro" value="{$idParceiro}">                                       
        <input type="text" class="form-control hidden" name="idUnidade" value="{$idUnidade}">                                       
        <input type="text" class="form-control hidden" name="dsObservacaoItem" value="{$dsObservacaoItem}">                                       
        <input type="text" class="form-control hidden" name="idCentroCusto" value="{$idCentroCusto}">                                       
        <input type="text" class="form-control hidden" name="dsParceiroSugerido" value="{$dsParceiroSugerido}">                                       
        <div class="row small">
            <div class="col-md-2" id="botao_enviar">
                 <input class="btn btn-primary" type="submit" value="PROCESSAR" />
            </div> 
            <div class="col-md-2">
                 <input type="file" name="arquivo" />
            </div> 
        </div> 
        <br>
        <div class="row small">
            &nbsp;&nbsp;&nbsp; Somente importar arquivo formato CSV <br>
            &nbsp;&nbsp;&nbsp; Colunas separadas por virgula e os campos textos devem estar dentro por aspas duplas <br>
            &nbsp;&nbsp;&nbsp; As colunas devem estar nesta ordem, NAO DEIXAR LINHAS COM CABEÇALHO: <br>
            &nbsp;&nbsp;&nbsp; 1 - Nome do Produto (Texto)<br>
            &nbsp;&nbsp;&nbsp; 2 - Codigo do Produto (Texto)<br>
            &nbsp;&nbsp;&nbsp; 3 - Quantidade (Formato americano, ponto na decimal e sem separação na milhar)<br>
        </div>
    </div>