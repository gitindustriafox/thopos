    {include file="solicitacaocompras/modalFotos/detalhes.tpl"}  
    <table class="table table-striped" border="1">
        <thead>
            <tr>
                <th>ID</th>
                <th>ARQUIVO</th>
                <th>ACAO</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$fotos_lista item="linha"}  
            <tr>
                <td>{$linha.idDocumento}</td>
                <td>{$linha.dsLocalArquivo}</td> 
                <td><a 
                      class="glyphicon glyphicon-camera"
                      href="http://thopos.tecnologiafox.com/{$linha.dsLocalArquivo}" target="_blank"
                      >  Ver</a>
            </tr>
            {/foreach}        
        </tbody>
    </table>
