    {include file="solicitacaocompras/produtos/detalhes.tpl"} 
    <br>
    <h4> ULTIMOS 15 LANÇAMENTOS DO PRODUTO </h4>
    <div class="panel-body">
        <div class="row small">
            <div id="linhas_movimento">
                 {include file="solicitacaocompras/produtos/thumbnail_lista.tpl"} 
            </div>
        </div>
    </div>
    <br>
    <h4> ESTOQUE/PONTO DE PEDIDO DO PRODUTO </h4>
    <div class="panel-body">
        <div class="row small" id="linhas_estoque">
            <table class="table table-striped" border="1">
                <thead>
                    <tr>
                        <th>LOCAL ESTOQUE</th>
                        <th>ESTOQUE</th>
                        <th>STATUS</th>
                        <th>MINIMO</th>
                        <th>MAXIMO</th>
                    </tr>
                </thead>                    
                <tbody>
                    {foreach from=$estoque item="linha"} 
                        <tr>
                            <td>{$linha.dsLocalEstoque|default:''}</td> 
                            <td>{$linha.estoqueatual|default:'0'|number_format:"2":",":"."}</td> 
                            <td>
                                {if $linha.dtValidade|default:'' neq '0000-00-00 00:00:00'} {if $linha.dtValidade < date('Y-m-d')}VENCIDO{else}VALIDO{/if}{/if}
                            </td> 
                            <td>{$linha.qtEstoqueMinimo|default:'0'|number_format:"2":",":"."}</td> 
                            <td>{$linha.qtLoteReposicao|default:'0'|number_format:"2":",":"."}</td> 
                        </tr>
                    {/foreach}        
                </tbody>
                </tfoot>
            </table>
        </div>
    </div>                    