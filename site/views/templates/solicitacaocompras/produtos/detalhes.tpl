    <div class="form-group">
        <div class="row small">
            <div class="col-md-1">
                <label for="pedido">ID</label>
                <input type="text" id="idInsumo" class="form-control" readonly nome="idInsumo" value="{$produto.idInsumo|default:''}" />
            </div>
            <div class="col-md-1">
                <label for="form-control">CODIGO</label>
                <input type="text" class="form-control" name="cdProduto" readonly id="cdProduto" value="{$produto.cdProduto|default:''}">           
            </div>
            <div class="col-md-3">
                <label for="form-control">NOME DO PRODUTO</label>
                <input type="text" class="form-control" name="dsInsumo" readonly id="dsInsumo" value="{$produto.dsInsumo|default:''}">           
            </div>
            <div class="col-md-1">
                <label for="form-control">UNIDADE</label>
                <input type="text" class="form-control" name="dsUnidade" readonly id="dsUnidade" value="{$produto.dsUnidade|default:''}">           
            </div>
            <div class="col-md-6">
                <label for="form-control">NCM</label>
                <input type="text" class="form-control" name="cdNCM" readonly id="cdNCM" value="{$produto.cdNCM|default:''} - {$produto.dsNCM|default:''}">           
            </div>
        </div>    
            <br>
        <div class="row small">
            <div class="col-md-2">
                <label for="form-control">GRUPO</label>
                <input type="text" class="form-control" name="dsGrupo" readonly id="dsGrupo" value="{$produto.dsGrupo|default:''}">           
            </div>
            <div class="col-md-3">
                <label for="form-control">CODIGO FABRICANTE</label>
                <input type="text" id="dsCodigoDoFabricante" class="form-control" readonly nome="dsCodigoDoFabricante" value="{$produto.dsCodigoDoFabricante|default:''}" />
            </div>
            <div class="col-md-1">
                <label for="form-control">VALIDADE</label>
                <input type="text" class="form-control" name="dtValidade" readonly id="dtValidade" value="{$produto.dtValidade|date_format:'%d/%m/%Y'|default:''}" >           
            </div>
            <div class="col-md-1">
                <label for="form-control">CONTROLADO</label>
                <input type="text" class="form-control" name="controlado" readonly id="controlado" value="{$produto.controlado|default:''}">           
            </div>
            <div class="col-md-2">
                <label for="form-control">ORGAO CONTROLADOR</label>
                <input type="text" class="form-control" name="dsOrgaoControlador" readonly id="dsOrgaoControlador" value="{$produto.dsOrgaoControlador|default:''}">           
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="tipomovimento" >TIPO DE MOVIMENTO</label>
                    <select class="form-control"  id="idTipoMovimento" name="idTipoMovimento" onchange="solicitacaocompras.lerprodutostipomov();"> 
                        {html_options options=$tipomovimento selected={$busca.idTipoMovimento|default:''}}
                    </select>                                    
                </div>
            </div>             
            <br>
            <div class="col-md-1">
              <div class="row small">
                   <a class="btn btn-primary abortar" data-dismiss="modal" onclick="solicitacaocompras.clear_log();">ABORTAR</a>
              </div> 
            </div>             
        </div>    
    </div>