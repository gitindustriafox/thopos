            <table class="table table-striped" border="1">
                <thead>
                    <tr>
                        <th>MOVIMENTO</th>
                        <th>DATA</th>
                        <th>PARCEIRO</th>
                        <th>NOTA FISCAL</th>
                        <th>PEDIDO</th>
                        <th>LOCAL ESTOQUE</th>
                        <th>CENTRO CUSTO</th>
                        <th>OBSERVACAO</th>
                        <th>RETIRADO POR</th>
                        <th>QUANTIDADE</th>
                        <th>VALOR</th>
                    </tr>
                </thead>                    
                <tbody>
                    {foreach from=$linhaitens item="itens"} 
                        <tr>
                        <td>{$itens.dsTipoMovimento|default:''}</td>
                        <td>{$itens.dtMovimento|date_format:'%d/%m/%Y %H:%M'|default:''}</td>
                        <td>{$itens.dsParceiro|default:''}</td> 
                        <td>{$itens.nrNota|default:''}</td> 
                        <td>{$itens.nrPedido|default:''}</td> 
                        <td>{$itens.dsLocalEstoque|default:''}</td> 
                        <td>{$itens.dsCentroCusto|default:''}</td> 
                        <td>{$itens.referencia|default:''}</td> 
                        <td>{$itens.dsColaborador|default:''}</td> 
                        <td>{$itens.qtMovimento|default:'0'|number_format:"2":",":"."}</td>
                        <td>{$itens.vlMovimento|default:'0'|number_format:"2":",":"."}</td> 
                        </tr>
                    {/foreach}        
                </tbody>
                </tfoot>
            </table>