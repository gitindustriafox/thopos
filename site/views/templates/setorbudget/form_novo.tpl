{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>BUDGET PARA O SETOR: {$registro.dsSetor|default:''}</h1></tt>
            </div>   
            <div class="row small">
                <div class="col-md-1">
                    <a href="/setorbudget" class="btn btn-primary"> ABORTAR</a><br>
                </div>   
                <div class="col-md-1">
                    <input class="btn btn-primary" type="button" onclick='budget.ativar();' {if $registro.stBudget|default:'0' eq '0'} value="ATIVAR" {else} value="DESATIVAR" {/if} name="btnAtivar"/>         
                </div>   
            </div>   
            <br>
            <div class="row small">
                <input type="text" class="form-control hidden" name="idSetorBudget" id="idSetorBudget" value="{$registro.idSetorBudget|default:''}">           
                <input type="text" class="form-control hidden" name="stBudget" id="stBudget" value="{$registro.stBudget|default:'0'}">           
                <div class="col-md-1">
                    {if {$registro.idSetor}>0}
                            <label for="form-control">CODIGO</label>
                            <input type="text" class="form-control" name="idSetor" id="idSetor" value="{$registro.idSetor}" READONLY>           
                    {else}
                             <label for="form-control">CODIGO</label>
                             <input type="text" class="form-control" name="idSetor" value="" READONLY>           
                    {/if}                     
                </div> 
                <div class="col-md-2">
                    <label for="form-control">DESCRICAO</label>
                    <input type="text" class="form-control" name="dsSetor" disabled id="dsSetor" value="{$registro.dsSetor|default:''}" >           
                </div> 
                <div class="col-md-1">
                    <label for="form-control">VALOR </label>
                    <input type="text" class="form-control valor" name="vlBudget" id="vlBudget" value=""> 
                </div> 
                <div class="col-md-1">
                    <label for="form-control">ANO</label>
                    <input type="text" class="form-control"  name="idAno" id="idAno"  >           
                </div> 
                <div class="col-md-1">
                    <label for="form-control">MES</label>
                    <input type="text" class="form-control"  name="idMes" id="idMes"  >           
                </div> 
                <div class="col-md-2">
                    <label for="form-control">REPLICA ESTE BUDGET MENSALMENTE</label>
                    <input type="checkbox"  class="form-control" name="optReplica" id="optReplica" value=""> 
                </div>   
                <br>
                <div class="col-md-2">
                  <div class="row small">
                      <a class="btn btn-primary" id="btn-adicionaorcado" title="ADICIONAR" onclick="budget.adicionarbudget();" >ADICIONAR</a> 
                  </div> 
                </div> 
            </div>
            <br>
            <div class="row small">                        
                <div class="col-md-12" id="mostrarbudgets">
                    {include file="setorbudget/budget.html"}
                </div>
            </div>
        </div>                    
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
{*<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
*}<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/setor/budget.js"></script>



{include file="comuns/footer.tpl"}

