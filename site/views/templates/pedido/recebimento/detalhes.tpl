    <div class="form-group">
        <div class="row small">
            <div class="col-md-1">
                <label for="pedido">PEDIDO </label>
                <input type="text" id="idPedidoBaixa" class="form-control" readonly nome="idPedidoBaixa" value="{$cabec.idPedido|default:''}" />
            </div>
            <div class="col-md-4">
                <label for="parceiro">PARCEIRO</label>
                <input type="hidden" id="idParceiro" class="form-control" nome="idParceiro" value='{$cabec.idParceiro|default:''}' readonly/>
                <input type="text" id="dsParceiro" class="form-control" nome="dsParceiro" value='{$cabec.dsParceiro|default:''}' readonly/>
            </div>
            <div class="col-md-1">
                <label for="form-control">NOTA</label>
                <input type="text" class="form-control" name="nrNota" id="nrNota">           
            </div>
            <div class="col-md-2">
                <label for="form-control">DATA</label>
                <input type="text" class="form-control obg standard-mask-date standard-form-date standard-form-require" readonly name="dtPedido" id="dtPedido" value="{$dtMovimento|date_format:'%d/%m/%Y'|default:Date("d/m/Y")}" >           
            </div> 
            <br>
            <div class="col-md-2">
            </div> 
            <div class="col-md-1">
              <div class="row small">
                  <input class="btn btn-primary gravar" type="submit" value="GRAVAR" name="btnGravar"/>         
              </div> 
            </div> 
            <div class="col-md-1">
              <div class="row small">
                   <a class="btn btn-primary abortar" data-dismiss="modal" onclick="recebimento_modal.clear_log();">ABORTAR</a>
              </div> 
            </div>             
        </div>    
    </div>