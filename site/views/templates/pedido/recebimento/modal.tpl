<div id="recebimento_show" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-lg" role="document" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">RECEBIMENTO</h4>
            </div>
            <form name="frm-recebimento" 
                  action="/pedido/gravar_recebimento" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return recebimento_modal.consisteRecebimento();">
                    <div class="modal-body"></div>
            </form>    
        </div>
    </div>
</div>