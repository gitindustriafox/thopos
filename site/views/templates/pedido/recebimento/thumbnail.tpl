    {include file="pedido/recebimento/detalhes.tpl"}     
    <br>
        {assign var="total" value=0}        
        <div class="panel-body">
            <div class="row small" id="linhas_recebimento">
                <table class="table table-striped" border="1">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>PRODUTO</th>
                            <th>NOME DO PRODUTO</th>
                            <th>NCM (separado por ponto)</th>
                            <th>QTDE PEDIDO</th>
                            <th>QTDE RESTANTE</th>
                            <th>VAL UNIT</th>
                            <th>VAL TOT PEDIDO</th>
                            <th>VAL TOT RESTANTE</th>
                            <th>LOCAL ESTOQUE</th>
                            <th>CENTRO DE CUSTO</th>
                        </tr>
                    </thead>                    
                    <tbody>
                        {foreach from=$pedidoitens item="linha"}  
                        <tr>
                            <td>{$linha.idPedidoItem|default:''}</td>
                            <td>{$linha.cdInsumo|default:''}</td>
                            {if $linha.stTipoIS|default:'0' eq '1'}
                                <td>{$linha.dsServico|default:''}</td>
                                <td>
                                    <input  type="text" id="ncm{$linha.idPedidoItem}" name="ncm[{$linha.idPedidoItem}]" disabled="disabled" value='SERVICO' 
                                </td>                                         
                            {else}
                                <td>{$linha.dsInsumo|default:''}</td>
                                <td>
                                    <input  type="text" id="ncm{$linha.idPedidoItem}" name="ncm[{$linha.idPedidoItem}]" value='{$linha.cdNCM|default:''}' 
                                </td>                                         
                            {/if}
                            <td>{$linha.qtPedido|default:''|number_format:2:",":"."|default:"0,00"}</td> 
                                <div style="color:red;"> 
                                    <td> 
                                        <input class="valor" type="text" id="qtde{$linha.idPedidoItem}" name="qtde[{$linha.idPedidoItem}]" value='{$linha.qtPedido - $linha.qtEntregue|default:''|number_format:2:",":"."|default:"0,00"}'
                                    </td> 
                                </div>
                            <td>R$ {$linha.vlUnitario|number_format:2:",":"."|default:"0,00"}</td> 
                            <td>R$ {$linha.vlPedido|number_format:2:",":"."|default:"0,00"}</td> 
                            <td> 
                                <input  class="valor" type="text" id="valo{$linha.idPedidoItem}" name="valor[{$linha.idPedidoItem}]" value='{$linha.vlPedido - $linha.vlEntregue|default:''|number_format:2:",":"."|default:"0,00"}' 
                            </td> 
                            <td> 
                            {if $linha.stTipoIS|default:'0' eq '0'}
                                <select class="form-control" name="localestoque[{$linha.idPedidoItem}]" id="localestoque{$linha.idPedidoItem}"> 
                                    {html_options options=$linha.lista_localestoque selected=$linha.idLocalEstoque}
                                </select>                      
                            {/if}
                            </td>
                            <td> 
                                <select class="form-control" name="centrocusto[{$linha.idPedidoItem}]" id="centrocusto{$linha.idPedidoItem}"> 
                                    {html_options options=$linha.lista_centrocusto selected=$linha.idCentroCusto}
                                </select>                      
                            </td>
                        </tr>
                        {$total = $total + $linha.vlPedido}                
                        
                        {/foreach}        
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td></td>
                            <td><strong>VALOR TOTAL DO PEDIDO:</strong></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><strong>R$ {$total|number_format:2:",":"."|default:"0,00"} </strong></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    <script src="/files/js/pedido/pedido.js"></script>                                        