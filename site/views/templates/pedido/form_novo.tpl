{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>PEDIDO DE COMPRA</h1></tt>
            </div> 
                <br>
                <div class="row small">
                    <h3> &nbsp; INFORMACOES DO CABECALHO DO PEDIDO DE COMPRAS: </h3>
                    <br>
                    <div class="col-md-1">
                        <label for="form-control">SOLICITACAO</label>
                        <input type="text" class="form-control" name="idSolicitacao" id="idSolicitacao" >        
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">ITEM</label>
                        <input type="text" class="form-control" name="idSolicitacaoItem" id="idSolicitacaoItem" >        
                    </div>                     
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="pedido">ID PEDIDO</label>
                            <input type="text" class="form-control" name="idPedido" id="idPedido" value="{$registro.idPedido|default:''}" READONLY>           
                        </div>
                    </div>        
                    <div class="col-md-1">
                        <label for="form-control">DATA</label>
                        <input type="text" class="form-control datetime" readonly name="dtPedido" id="dtPedido" value="{$registro.dtPedido|date_format:'%d/%m/%Y'|default:Date("d/m/Y")}" >           
                    </div> 
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="usuario">USUARIO SOLICITANTE</label>
                            <select class="form-control" name="idUsuarioSolicitante" id="idUsuarioSolicitante" onclick="pedido.lerusuario();">
                                    {include file="pedido/lista_usuarios.tpl"}     
                            </select>                                    
                        </div>
                    </div>                                             
                    <div class="col-md-2">
                        <label for="form-control">SOLICITANTE</label>
                        <input type="text" class="form-control" name="dsSolicitante" id="dsSolicitante" value="{$registro.dsSolicitante|default:''}" >           
                    </div>                             
                    <div class="col-md-4">
                        <label for="Parceiro">PARCEIRO</label>                        
                        <input data-label="Parceiro" type="text" data-tipo="dsParceiro" class="form-control complete_Parceiro" name="Parceiro"  id="Parceiro" value="{$registro.dsParceiro|default:''}"/>     {*onkeypress="pedido.completar_Parceiro();" *}
                        <input type="hidden" id="idParceiro" value="{$registro.idParceiro|default:''}" name="idParceiro"/>                        
                    </div>                                         
{*                    <br>
                    <a class="btn btn-primary" id="btn-gravarcabecalho" title="Clique aqui para gravar o cabeçalho do Pedido"onclick="pedido.lersolicitacao();" {*{if $registro.idPedido  neq  ''}  disabled {/if}  >Ler Solicitacao</a> *}
                </div> 
                <br>
                <div class="row small">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="prioridade">PRIORIDADE</label>
                            <select class="form-control" name="idPrioridade" id="idPrioridade" value="{$registro.idPrioridade|default:''}">
                                {html_options options=$lista_prioridade selected=$registro.idPrioridade|default:''}
                            </select>                                    
                        </div>
                    </div>                                                             
                    <div class="col-md-2">
                        <label for="form-control">OBSERVACAO</label>
                        <input type="text" class="form-control" name="dsObservacao" id="dsObservacao" value="{$registro.dsObservacao|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">PRAZO ENTREGA</label>
                        <input type="text" class="form-control datetime" name="dtPrazoEntrega" id="dtPrazoEntrega" value="{$registro.dtPrazoEntrega|date_format:'%d/%m/%Y'|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">COMPLEMENTO DA ENTREGA</label>
                        <input type="text" class="form-control" name="dsComplementoEntrega" id="dsComplementoEntrega" value="{$registro.dsComplementoEntrega|default:''}" >           
                    </div> 
                    <br>
                    <div class="col-md-4">
                        <a class="btn btn-primary" id="btn-gravarcabecalho" title="Clique aqui para gravar o cabeçalho do Pedido"onclick="pedido.gravarcabecalho();" {*{if $registro.idPedido  neq  ''}  disabled {/if}*}  >GRAVAR</a> 
                        <a class="btn btn-primary" id="btn-gravarfinanceiro" title="Clique aqui para informar o Financeiro" href="/pedido/financeiro/idPedido/{$registro.idPedido}" {if $registro.idPedido  eq  ''}  disabled {/if}  >FINANCEIRO</a> 
                        <a class="btn btn-primary" id="btn-novopedido" title="Clique aqui para começar um novo pedido" onclick="pedido.novopedido();">INCLUIR</a> 
                        <a class="btn btn-primary" id="btn-sairtela" title="Clique aqui para sair da tela e voltar a lista de pedidos" href="/pedidoaberto"  onclick="pedido.desabilitaid();"> ABORTAR</a><br>                
                    </div>
                </div> 
                <br>
                <div class="row small" >
                    <h3> &nbsp; PRODUTOS/SERVICOS PARA ESTE PEDIDO:</h3>
                    <br>
                    <input type="hidden" class="form-control" id="idPedidoItem" name="idPedidoItem" readonly>
                    <div class="col-md-1">
                        </br>
                        <div class="form-group">
                             <input type="radio" onclick='pedido.escolherIS();' id="stTipoI" name="stTipoIS" value="0" checked> Produto<br>
                             <input type="radio" onclick='pedido.escolherIS();' id="stTipoS" name="stTipoIS" value="1"> Servico<br>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
{*                            <div id='labelproduto'>
                                <label for='linsumo' style='color: red!important;' onclick='pedido.atualizaprodutos("0");' >PRODUTO</label>
                            </div>
                            <select class="form-control"  id="idInsumo" name="idInsumo" {if $registro.idPedido  eq ''}  disabled  {/if} onchange="pedido.lerunidade();"> 
                                    {include file="pedido/lista_produtos.tpl"}       
                            </select>                                    
*}

                            <div id='labelproduto'>
                                <label for="linsumo">PRODUTO</label>                        
                            </div>
                            <input data-label="insumo" type="text" data-tipo="dsInsumo" class="form-control complete_produto" name="insumo"  id="insumo" value="{$registro.dsInsumo|default:''}"/>     {*onkeypress="pedido.completar_Parceiro();" *}
                            <input type="hidden" id="idInsumo" value="{$registro.idInsumo|default:''}" name="idInsumo"/>

                        </div>
                    </div>
                            
                    <div class="col-md-1">
                        <label for="form-control">UNIDADE</label>
                        <input type="text" class="form-control" name="dsUnidade" id="dsUnidade" disabled='disabled' value="">       
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">ESTOQUE</label>
                        <input type="text" class="form-control" name="qtEstoque" id="qtEstoque" disabled='disabled' value="">       
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">QUANTIDADE</label>
                        {*{var_dump($registro)}*}
                        <input type="text" class="form-control obg" name="qtPedido" id="qtPedido" {if $registro.idPedido  eq ''}  disabled  {/if} value="">      
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">VALOR TOTAL</label>
                        <input type="text" class="form-control obg" name="vlPedido" {if $registro.idPedido  eq  ''}  disabled {/if} id="vlPedido" value=""> 
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">OBSERVACAO</label>
                        <input type="text" class="form-control" name="dsObservacaoItem" {if $registro.idPedido  eq  ''}  disabled {/if} id="dsObservacaoItem" value=""> 
                    </div> 
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="localestoque">LOCAL PARA ESTOQUE </label>
                            <select class="form-control" name="idLocalEstoque" id="idLocalEstoque" {if $registro.idPedido  eq ''}  disabled  {/if}"> 
                                {html_options options=$lista_localestoque selected=null}
                            </select>                      
                        </div>
                    </div>
                </div>
                <div class="row small" >
                    <h3> &nbsp; DESTINO DESTE PRODUTO:</h3>
                    <br>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="centrocusto">CENTRO DE CUSTO </label>
                            <select class="form-control" name="idCentroCusto" {if $registro.idPedido  eq  ''}  disabled {/if} id="idCentroCusto">
                                {html_options options=$lista_centrocusto selected=null}
                            </select>                      
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="grupodr">GRUPO DE DESPESA</label>
                            <select class="form-control" name="idGrupoDR"  id="idGrupoDR" onclick="solicitacaocompras.lerItemDR();">  
                                {html_options options=$lista_grupodr selected=null}
                            </select>                      
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="itemdr">ITEM DE DESPESA </label>
                            <select class="form-control" name="idItemDR" {if $registro.idPedido  eq  ''}  disabled {/if} id="idItemDR"> 
                                {include file="solicitacaocompras/listaItemDR.tpl"}
                            </select>                      
                        </div>
                    </div>
{*                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="motivo">MOTIVO - CAUSA</label>
                            <select class="form-control" name="idMotivo" {if $registro.idPedido  eq  ''} disabled {/if}  id="idMotivo">  
                                {html_options options=$lista_motivo selected=null}
                            </select>                      
                        </div>
                    </div>
*}   
                        <div class="col-md-2">
                            <label for="form-control">JUSTIFICATIVA</label>
                            <input type="text" class="form-control" name="dsJustificativa"  id="dsJustificativa" value=""> 
                        </div> 
                    <br>
                    <div class="col-md-1">
                          <a class="btn btn-primary" id="btn-adicionaitem" title="Clique aqui para adicionar este insumo/produto na lista abaixo" {if $registro.idPedido  eq  ''}  disabled {/if} onclick="pedido.gravaritem();">GRAVAR</a> 
                    </div> 
                </div>
                <br>
                {include file="pedido/lista.html"}
            
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="../../../files/js/modules/autocomplete.js" type="text/javascript"></script>
<script src="/files/js/solicitacaocompras/solicitacaocompras.js"></script>
<script src="/files/js/pedido/pedido.js"></script>
{include file="comuns/footer.tpl"}

