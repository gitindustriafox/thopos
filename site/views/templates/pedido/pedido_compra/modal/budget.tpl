            <div class="col-md-2">
                <label for="form-control">VALOR GASTO NESTE MES/ANO</label>
                <input type="text" class="form-control valor" disabled name="vlBudgetUsado" id="vlBudgetUsado" value="{$vlBudgetUsado|number_format:2:",":"."|default:"0,00"}"> 
            </div>                     
            <div class="col-md-2">
                <label for="form-control">VALOR TOTAL DISPONÍVEL</label>
                <input type="text" class="form-control valor" disabled name="vlBudgetDisponivel" id="vlBudgetDisponivel" value="{$vlBudgetDisponivel|number_format:2:",":"."|default:"0,00"}"> 
            </div>                     
            <div class="col-md-2">
                <label for="form-control">VALOR DO PEDIDO</label>
                <input type="text" class="form-control valor" disabled name="vlTotalPedido" id="vlTotalPedido" value="{$total|number_format:2:",":"."|default:"0,00"}"> 
            </div>                     
            <div class="col-md-3">
                <label for="form-control">SALDO A USAR EM NOVOS PEDIDOS </label>
                <input type="text" class="form-control valor" disabled name="vlSaldo" id="vlSaldo" value="{$vlSaldo|number_format:2:",":"."|default:"0,00"}"> 
            </div>                     