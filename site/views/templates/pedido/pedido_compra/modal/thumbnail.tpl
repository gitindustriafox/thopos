        {assign var="total" value=0}        
        <div class="panel-body">
            <div class="row small" id="linhas_pedido">
                <table class="table table-striped" border="1">
                    <thead>
                        <tr>
                            <th>CODIGO DO PRODUTO</th>
                            <th>NOME DO PRODUTO/SERVICO</th>
                            <th>QTDE PEDIDO</th>
                            <th>VAL UNIT</th>
                            <th>VAL TOT PEDIDO</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach from=$pedidoitens item="linha"}  
                        <tr>
                            <td>{$linha.cdInsumo|default:''}</td>
                            {if $linha.stTipoIS|default:'0' eq '1'}
                                <td>{$linha.dsServico|default:''}</td>
                            {else}
                                <td>{$linha.dsInsumo|default:''}</td>
                            {/if}
                            <td>{$linha.qtPedido|default:''|number_format:2:",":"."|default:"0,00"}</td> 
                            <td>R$ {$linha.vlUnitario|number_format:2:",":"."|default:"0,00"}</td> 
                            <td>R$ {$linha.vlPedido|number_format:2:",":"."|default:"0,00"}</td> 
                        </tr>
                        {$total = $total + $linha.vlPedido}                
                        
                        {/foreach}        
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td><strong>VALOR TOTAL DO PEDIDO:</strong></td>
                            <td></td>
                            <td></td>
                            <td><strong>R$ {$total|number_format:2:",":"."|default:"0,00"} </strong></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    {include file="pedido/pedido_compra/modal/detalhes.tpl"}     
                        