    <div class="form-group">
        <div class="row small">
            <div class="col-md-1">
                <label for="pedido">PEDIDO </label>
                <input type="text" id="idPedidoEmitir" class="form-control" readonly nome="idPedidoEmitir" value="{$cabec.idPedido|default:''}" />
            </div>
            <div class="col-md-6">
                <label for="parceiro">PARCEIRO</label>
                <input type="hidden" id="idParceiro" class="form-control" nome="idParceiro" value='{$cabec.idParceiro|default:''}' readonly/>
                <input type="text" id="dsParceiro" class="form-control" nome="dsParceiro" value='{$cabec.dsParceiro|default:''}' readonly/>
            </div>
            <div class="col-md-2">
                <label for="form-control">DATA PEDIDO</label>
                <input type="text" class="form-control obg standard-mask-date standard-form-date standard-form-require" readonly name="dtPedido" id="dtPedido" value="{$cabec.dtPedido|date_format:'%d/%m/%Y'|default:Date("d/m/Y")}" >           
            </div> 
            <div class="col-md-3">
                <label for="form-control">NUM ORCAMENTO/ COTACAO PARCEIRO</label>
                <input type="text" class="form-control" name="nrOrcamento" id="nrOrcamento" value='{$cabec.nrOrcamentoParceiro|default:''}'>           
            </div>
        </div> 
        </br>            
        <div class="row small">
            <div class="col-md-3">
                <label for="form-control">CONTATO</label>
                <input type="text" class="form-control" name="dsContato" id="dsContato" value='{$cabec.dsContato|default:''}'>           
            </div>
            <div class="col-md-2">
                <label for="form-control">FONE</label>
                <input type="text" class="form-control" name="dsFone" id="dsFone" value='{$cabec.dsFone|default:''}'>           
            </div>
            <div class="col-md-2">
                <label for="form-control">CELULAR</label>
                <input type="text" class="form-control" name="dsCelular" id="dsCelular" value='{$cabec.dsCelular|default:''}'>           
            </div>
            <div class="col-md-5">
                <label for="form-control">E-MAIL</label>
                <input type="text" class="form-control" name="dsEmail" id="dsEmail" value='{$cabec.dsEmail|default:''}'>           
            </div>
        </div>
        </br>
        <div class="row small">
            <div class="col-md-5">
                <label for="form-control">ENDERECO</label>
                <input type="text" class="form-control" name="dsEndereco" id="dsEndereco" value='{$cabec.dsEndereco|default:''}'>           
            </div>
            <div class="col-md-2">
                <label for="form-control">CIDADE</label>
                <input type="text" class="form-control" name="dsCidade" id="dsCidade" value='{$cabec.dsCidade|default:''}'>           
            </div>
            <div class="col-md-2">
                <label for="form-control">CNPJ</label>
                <input type="text" class="form-control" name="cdCNPJ" id="cdCNPJ" value='{$cabec.cdCNPJ|default:''}'>           
            </div>
            <div class="col-md-3">
                <label for="form-control">PRAZO DE ENTREGA</label>
                <input type="text" class="form-control" name="dtPrazoEntrega" id="dtPrazoEntrega" value='{$cabec.dtPrazoEntrega|date_format:'%d/%m/%Y'|default:''}'>           
            </div>
        </div>
        <br>
        <div class="row small">
            <div class="col-md-5">
                <div class="form-group">
                    <label for="usuario">EMPRESA</label>
                    <select class="form-control" name="idEmpresa" id="idEmpresa" onclick="pedido.lerempresa()">
                            {include file="pedido/lista_empresas.tpl"}     
                    </select>                                    
                </div>
            </div>   
            <div class="col-md-4">
                <label for="form-control">LOCAL DE ENTREGA</label>
                <input type="text" class="form-control" name="dsLocalEntrega" id="dsLocalEntrega1" value='{$cabec.dsLocalEntrega|default:''}'>           
            </div>
            <div class="col-md-3">
                <label for="form-control">FRETE</label>
                <input type="text" class="form-control" name="dsFrete" id="dsFrete" value='{$cabec.dsFrete|default:''}'>           
            </div>
        </div>
        <div class="row small">
            <div class="col-md-3">
                <label for="form-control">CONDICOES DE PAGAMENTO</label>
                <input type="text" class="form-control" name="dsCondicoesPagamento" id="dsCondicoesPagamento" value='{$cabec.dsCondicoesPagamento|default:''}'>           
            </div>
            <div class="col-md-2">
                <label for="form-control">VENCIMENTO</label>
                <input type="text" class="form-control" name="dtVencimento" onchange="pedido.lerbudgetsetor()" id="dtVencimento" value='{$cabec.dtVencimento|date_format:'%d/%m/%Y'|default:''}'>           
            </div>            
            <div class="col-md-4">
                <label for="form-control">OBSERVACOES</label>
                <input type="text" class="form-control" name="dsObservacoes" id="dsObservacoes" value='{$cabec.dsObservacaoEmissao|default:''}'>           
            </div>
            <div class="col-md-3">
                <label for="form-control">LOCAL DO PDF</label>
                <input type="text" class="form-control" name="dsCaminhoPDF" id="dsCaminhoPDF" readonly value='{$cabec.dsCaminhoPDF|default:''}'>           
            </div>
        </div>
        </br>
        <div class="row small">        
            <div class="col-md-3">
                <div class="form-group">
                    <label for="setor">SETOR</label>
                    <select class="form-control" name="idSetor" id="idSetor" onclick="pedido.lerbudgetsetor()">
                            {include file="pedido/lista_setor.tpl"}     
                    </select>                                    
                </div>
            </div>   
            <div id='mostrarvaloresbudget'>
                 {include file="pedido/pedido_compra/modal/budget.tpl"}     
            </div>
        </div>
        <div class="row small">
            <div class="col-md-1" id="botao_criar">
                 <a id="btncriar" class="btn btn-primary"  disabled onclick="emitir_pedido_compra.criar();">CRIAR PDF</a>
            </div> 
            <div class="col-md-1">
                 <a class="btn btn-primary" data-dismiss="modal" onclick="emitir_pedido_compra.clear_log();">ABORTAR</a>
            </div> 
        </div>
{*        <div class="row small">
            </br>
            <div class="col-md-7">
            </div>
            <div class="col-md-1">
                 <a class="btn btn-primary" data-dismiss="modal" onclick="emitir_pedido_compra.criar();">CRIAR PDF</a>
            </div> 
            <div class="col-md-1">
                 <a class="btn btn-primary" data-dismiss="modal" onclick="emitir_pedido_compra.clear_log();">ABORTAR</a>
            </div> 
            
        </div>    
*}    </div>
<script src="/files/js/pedido/pedido.js"></script>