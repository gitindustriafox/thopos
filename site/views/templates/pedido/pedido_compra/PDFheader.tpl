<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
    <head>
        <title>{$title}</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <style type="text/css">
            .page-header {
                padding-bottom: 0;
                margin: 20px 0 20px;
            }

            body { background: #fff; width: 98%; margin: 0 auto; padding-top: 20px; }

            .relatorio { text-transform: uppercase; }
            .table { font-size: 11px; }
            p { font-size: 11px; 
                padding-top: 3px;;
            }

            blockquote footer { font-size: 11px; }

            .table-direita {
                border-left: none;
                border-right: none;
                border-top: 1px nome;
                border-bottom: 1px nome;
            }

            .table-esquerda {
                border-left: none;
                border-right: none;
                border-top: 1px nome;
                border-bottom: 1px nome;
            }

            table th, table td { 
                font-family: sans-serif;
                color: #000000;                
                text-transform: uppercase; }
            {include file="files/css/bootstrap.css"}
            {include file="files/css/bootstrap-theme.css"}
        </style>
    </head>

    <body >