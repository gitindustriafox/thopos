{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>{if {$registro.idMaoObra}>0} ALTERAR MAO DE OBRA: {$registro.dsMaoObra|default:''}{else} INCLUIR NOVA MAO DE OBRA{/if}</h1></tt>
            </div>          
            <a href="/maoobra" class="btn btn-primary"> ABORTAR</a><br>

            <form name="frm-maoobra" 
                  action="/maoobra/gravar_maoobra" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario();">
                <br>
                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idMaoObra}>0}
                                <label for="form-control">CODIGO</label>
                                <input type="text" class="form-control" name="idMaoObra" id="idMaoObra" value="{$registro.idMaoObra}" READONLY>           
                        {else}
                                 <label for="form-control">CODIGO</label>
                                 <input type="text" class="form-control" name="idMaoObra" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-4">
                        <label for="form-control">DESCRICAO</label>
                        <input type="text" class="form-control" name="dsMaoObra" id="dsMaoObra" value="{$registro.dsMaoObra|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="unidade">UNIDADE</label>
                            <select class="form-control" name="idUnidade" id="idUnidade">
                                {html_options options=$lista_unidade selected=$registro.idUnidade}
                            </select>                      
                        </div>
                    </div>     
                            
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="tipomaoobra">TIPO DE MAO DE OBRA</label>
                            <select class="form-control" name="idTipoMaoObra" id="idTipoMaoObra">
                                {html_options options=$lista_tipomaodeobra selected=$registro.idTipoMaoObra}
                            </select>                      
                        </div>
                    </div>                     
                    <div class="col-md-2">
                        <label for="form-control">VALOR DA UNIDADE</label>
                        <input type="text" class="form-control" name="vlUnitario" id="vlUnitario" value="{$registro.vlUnitario|default:''}" >           
                    </div> 
                </div>    
                <br>
                  <div class="col-md-3">
                    <div class="row small">
                        <input class="btn btn-primary" type="submit" value="  GRAVAR" name="btnGravar"/>         
                    </div> 
                  </div> 
                <br>
            </form>
            
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/maoobra/maoobra_novo.js"></script>



{include file="comuns/footer.tpl"}

