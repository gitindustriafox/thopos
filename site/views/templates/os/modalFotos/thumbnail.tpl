    {include file="os/modalFotos/detalhes.tpl"} 
    <div id='mostrarfotos'>
        <table class="table table-striped" border="1">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>ARQUIVO</th>
                    <th>ACAO</th>
                </tr>
            </thead>
            <tbody>
                {foreach from=$fotos_lista item="linha"}  
                <tr>
                    <td>{$linha.idDocumento}</td>
                    <td>{$linha.dsLocalArquivo}</td> 
                    <td><a class="glyphicon glyphicon-camera" href="/{$linha.dsLocalArquivo}" target="_blank" >  Ver Foto</a>
                    <a class="glyphicon glyphicon-trash" onclick="os.delFoto('{$linha.idDocumento}', '{$linha.idTabela}');">  Excluir Foto</a>&nbsp;
                </tr>
                {/foreach}        
            </tbody>
        </table>
    </div>