{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="modal_fotos">
        {include file="os/modalFotos/modal.tpl"}                            
    </div>                 
    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>Solicitar Manutenção ou Reparo</h1></tt>
            </div> 
            <form name="frm-grava-os" action="/os/gravar_solicitacao_os" method="POST" enctype="multipart/form-data">
                <br>
                <div class="row small">
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="pedido">IDENTIFICADOR</label>
                            <input type="text" class="form-control" name="idOS" id="idOS" value="{$registro.idOS|default:''}" READONLY>           
                        </div>
                    </div>                     
                    <div class="col-md-1">
                        <label for="form-control">DATA DA SOLICITAÇÃO</label>
                        <input type="text" class="form-control datetime" name="dtOS" id="dtOS" value="{$registro.dtOS|date_format:'%d/%m/%Y %H:%M'|default:Date("d/m/Y H:i")}" >           
                    </div> 
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="setor">LOCAL DO SERVIÇO A SER EXECUTADO</label>
                            <select class="form-control" name="idSetor" id="idSetor">
                                {html_options options=$lista_setoros selected=$registro.idSetor}
                            </select>                      
                        </div>
                    </div>     
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="setor">CENTRO DE CUSTO</label>
                            <select class="form-control" name="idCentroCusto" id="idCentroCusto">
                                {html_options options=$lista_centrocusto selected=$registro.idCentroCusto}
                            </select>                      
                        </div>
                    </div>     
                    <div class="col-md-6">
                        <label for="form-control">DESCRIÇÃO DA SOLICITAÇÃO/PROBLEMA</label>
                        <textarea type="text" class="form-control" name="dsProblema" id="dsProblema" > {$registro.dsProblema|default:null} </textarea>
                    </div> 
                </div> 
                <br>
                <br>
                <div class="row small">
                    <div class="col-md-12">                                
                        <input title="Clique aqui para gravar as informações" class="btn btn-primary" type="submit" value="GRAVAR A SOLICITAÇÃO" name="btnGravar"/>                             
                        <a {if $registro.idOS|default:'0' eq 0} DISABLED {/if} class="btn btn-primary glyphicon glyphicon-camera" id="btn-sairtela" title="Clique aqui para anexar arquivos ou fotos" onclick="os.selecionarfoto('{$registro.idOS}');" >ANEXAR FOTOS</a>               
                        <a class="btn btn-primary" id="btn-sairtela" title="Clique aqui para sair da tela e voltar gestão de O.S." href="/menuOS" > VOLTAR PARA TELA ANTERIOR</a><br>                
                        <br>  
                    </div>
                </div>
            </form>
        </div>
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
{*<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>*}
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/os/os.js"></script>
<script src="/files/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>
<link href="/files/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css"/>

{include file="comuns/footer.tpl"}

