<div id="reuniaodatas_show" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-lg" role="document" style="width: 90%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h7 class="modal-title" > <strong> Sub tarefas informando seus prazos </strong></h7>
            </div>
            <form name="frm-mostrar-datas" 
                    action="/reuniao/adicionardata" 
                    method="POST" 
                    enctype="multipart/form-data">
                    <div class="modal-body"></div>
            </form>    
        </div>
    </div>
</div>