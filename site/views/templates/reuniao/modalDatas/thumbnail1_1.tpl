        <table class="table table-striped" border="1">
            <thead>
                <tr>
                    <th>Prazo</th>
                    <th>Sub Tarefa</th>
                    <th>Concluido em</th>
                    <th>Ação</th>
                </tr>
            </thead>
            <tbody>
                {foreach from=$datas_lista item="linha"}  
                <tr>
                    <td>{$linha.dtIntermediaria|date_format:'%d/%m/%Y %H:%M'|default:''}</td>
                    <td>{$linha.dsSubTarefa|default:''}</td>
                    <td>{$linha.dtConclusao|date_format:'%d/%m/%Y %H:%M'|default:''}</td>
                    <td>{if {$origem} eq 'ata'} 
                                <a class="glyphicon glyphicon-edit" title ="Editar a tarefa" onclick="reuniao.editardataintermediaria('{$linha.idData}','{$linha.idTarefa}','{$linha.dtIntermediaria}','{$linha.dsSubTarefa}');" ></a> |
                                <a class="glyphicon glyphicon-trash" title ="Excluir a tarefa" onclick="reuniao.excluirdataintermediaria('{$linha.idData}','{$linha.idTarefa}','ata','{$idUsuario|default:'0'}');" ></a> |
                        {else}      
                            {if $smarty.session.user.usuario eq $idUsuario|default:'0'}
                                {if $linha.dtConclusao eq ''}                        
                                    {if {$origem} eq 'ata'} 
                                       <a class="glyphicon glyphicon-edit" title ="Editar a tarefa" onclick="reuniao.editardataintermediaria('{$linha.idData}','{$linha.idTarefa}','{$linha.dtIntermediaria}','{$linha.dsSubTarefa}');" ></a> |
                                       <a class="glyphicon glyphicon-trash" title ="Excluir a tarefa" onclick="reuniao.excluirdataintermediaria('{$linha.idData}','{$linha.idTarefa}','ata','{$idUsuario|default:'0'}');" ></a> |
                                    {/if}
                                    {if $origem eq 'usuario' && $linha.stStatusTarefa eq 0} 
                                       <a class="glyphicon glyphicon-pencil" title ="Concluir a tarefa" onclick="reuniao.concluirSubTarefa('{$linha.idData}','{$linha.idTarefa}','usuario','{$idUsuario|default:'0'}');" ></a>
                                    {/if}
                                {/if}
                            {/if}
                        {/if}
                    </td>
                </tr>
                {/foreach}        
            </tbody>
        </table>