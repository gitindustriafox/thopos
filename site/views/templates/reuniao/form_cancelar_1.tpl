{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="modal_fotos">
        {include file="reuniao/modalFotos/modal.tpl"}                            
    </div>             
    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <br><br>            
{*            <div class="" >
                <tt><h3>{if {$registro.idReuniao}>0}Cancelar uma reunião{/if}</h3></tt>
            </div>          
            <br>
*}            <form name="frm-cancelar-reuniao" 
                  action="/reuniao/gravar_cancelar_reuniao" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario()" id="frm-cancelar-reuniao">
                <br>
                <input type="text" class="hidden" name="opcao" id="opcao" value="{$opcao|default:''}" >                           
                <div class="panel-body panel panel-default" > 
                    <div class="row small">
                        <li class="dropdown user-dropdown">
                            <a
                                href="#" id="menureuniao" class="dropdown-toggle"  style="color: #000000; font-size: large" data-toggle="dropdown"> 
                                <i class="fa fa-list" style="font-size: large;"> &nbsp;&nbsp; Cancelar uma reunião</i> 
                            </a>
                            <ul class="dropdown-menu">
                                <li><a onclick="reuniao.cancelar_reuniao();" ><i class="fa fa-calendar"></i>&nbsp;&nbsp;Salvar as informações</a></li>                                                
                                <li class="divider"></li>
                                <li><a onclick="reuniao.enviaremail('0','{$registro.idReuniao}','Adiar');"><i class="fa fa-list-alt"></i>&nbsp;&nbsp;Enviar e-mail para os participantes</a></li>
                                <li class="divider"></li>
                                <li><a href="/reuniao/acao/opcao/{$opcao|default:null}"><i class="fa fa-eject"></i>&nbsp;&nbsp;Sair</a></li>
                            </ul>
                        </li>
                    </div> 
                </div>
                <div class="row small">
                    <div class="col-md-1 hidden">
                        {if {$registro.idReuniao}>0}
                                <label for="form-control">Identificador</label>
                                <input type="text" class="form-control" name="idReuniao" id="idReuniao" value="{$registro.idReuniao}" READONLY>           
                        {else}
                                 <label for="form-control">Identificador</label>
                                 <input type="text" class="form-control" name="idReuniao" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">Data da reunião</label>
                        <input type="text" class="form-control datetime" disabled name="dtReuniaoA" id="dtReuniaoA" value="{$registro.dtReuniao|date_format:'%d/%m/%Y %H:%M'|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">Duração estimada</label>
                        <input type="text" class="form-control" readonly name="dsTempoDuracao" id="dsTempoDuracao" value="{$registro.dsTempoDuracao|default:''}" >           
                    </div>                     
                    <div class="col-md-2">
                        <label for="form-control">Tipo de reunião</label>
                        <select class="form-control"  id="idTipo" disabled name="idTipo"> 
                                {include file="reuniao/lista_tipo.tpl"}       
                        </select>                                    
                    </div>                                             
                    <div class="col-md-2">
                        <label for="Local">Local da reunião</label>                        
                        <input data-label="Local" type="text" data-tipo="dsLocal" class="form-control" READONLY name="dsLocal"  id="dsLocal" value="{$registro.dsLocalReuniao|default:''}">
                        <input type="hidden" id="idLocal" value="{$registro.idLocalReuniao|default:''}" name="idLocal"/>
                    </div>                                                                     
                    <div class="col-md-2">
                        <label for="form-control">Digite o motivo do cancelamento</label>
                        <input type="text" class="form-control" name="dsCancelado" id="dsCancelado" {if $registro.idReuniao eq ''} disabled {/if} value="{$registro.dsCancelado}">           
                    </div> 
                </div>                                             
                <br>
                <div class="row small">
                    <div class="col-md-12">
                        <label for="form-control">Pauta da reunião</label>
                        <textarea class="form-control"  rows="5" disabled="" name="dsAssunto" id="dsAssunto">{$registro.dsAssunto|default:''}</textarea>           
                    </div> 
                </div> 
{*                <br>            
                    <input type="submit" title="Clique aqui para cancelar a reunião" value="Cancelar" name="btnGravar" class="btn btn-primary" />         
                    <input type="button" title="Clique aqui para enviar e-mail avisando" value="E-mail" onclick="reuniao.enviaremail('0','{$registro.idReuniao}','Cancelar');" id="btnAnexar" name="btnAnexar" class="btn btn-primary" {if $registro.idReuniao eq ''} disabled {/if}/>         
                    {if $voltarmenu|default:null}
                        <a href="/menuReuniao" title="Clique aqui para sair" class="btn btn-primary">Sair</a>
                    {else}    
                        <a href="/reuniao/acao/opcao/{$opcao|default:null}" title="Clique aqui para sair" class="btn btn-primary">Sair</a>
                    {/if}    
                <br>
*}                <br>
            </form>
            <div class="panel-item panel panel-default"> 
                <div class="panel-heading mostra">
                    <h4> <strong>Participantes desta reunião</strong> <h4>
                </div> 
                <div class="panel-body esconde" id="painel_colaboradores">
                    <div id="mostraritens">
                         {include file="reuniao/itens.html"}
                    </div>
                </div>
            </div>    
            
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
{*<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>*}
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/reuniao/reuniao.js"></script>
<script src="/files/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>
<link href="/files/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css"/>

{include file="comuns/footer.tpl"}

