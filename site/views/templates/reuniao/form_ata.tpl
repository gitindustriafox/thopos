{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="modal_fotos">
        {include file="reuniao/modalFotos/modal.tpl"}                            
    </div>             
    <div id="modal_datas">
        {include file="reuniao/modalDatas/modal.tpl"}                            
    </div>             
    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <form name="frm-reuniao" 
                  action="/reuniao/gravar_ata_reuniao" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario()" id="frm-reuniao">
                <br>
                <input type="text" class="hidden" name="opcao" id="opcao" value="{$opcao|default:'iniciar'}" >           
                <input type="text" class="hidden" name="voltarmenu" id="voltarmenu" value="{$voltarmenu|default:'1'}" >    
                <input type="text" class="hidden" name="idReuniao" id="idReuniao" value="{$registro.idReuniao|default:''}" >    
                <br>
                <div class="panel-body panel panel-default" > 
                    <div class="row small">
                        <li class="dropdown user-dropdown">
                            <a
                                href="#" id="menureuniao" class="dropdown-toggle"  style="color: #000000; font-size: large" data-toggle="dropdown"> 
                                <i class="fa fa-list" style="font-size: large;"> &nbsp;&nbsp; {if {$registro.idReuniao}>0}Reunião em andamento{/if}</i> 
                            </a>
                            <ul class="dropdown-menu">
                                {if $registro.idReuniao neq ''}
                                    <li><a onclick="reuniao.gravar_ata_reuniao();" ><i class="fa fa-calendar"></i>&nbsp;&nbsp;Salvar as informações</a></li>                                                
                                {/if}
                                {if $registro.idReuniao neq ''}
                                    <li class="divider"></li>
                                    <li><a onclick="reuniao.selecionarfotoR('ATA');"><i class="fa fa-adjust"></i>&nbsp;&nbsp;Anexar arquivos</a></li>
                                {/if}
                                {if $registro.idReuniao neq ''}
                                    <li class="divider"></li>
                                    <li><a  onclick="reuniao.criarPDF('{$registro.idReuniao}');"><i class="fa fa-cut"></i>&nbsp;&nbsp;Criar PDF da Reunião + ATA</a></li>
                                {/if}
                                {if $registro.idReuniao neq ''}
                                    {if $registro.dsCaminhoPDF neq ''}
                                        <li class="divider"></li>
                                        <li><a href="/{$registro.dsCaminhoPDF}" target="_blanck"><i class="fa fa-user"></i>&nbsp;&nbsp;Ver o PDF criado</a></li>
                                    {/if}
                                {/if}
                                {if $registro.idReuniao neq ''}
                                    <li class="divider"></li>
                                    <li><a onclick="reuniao.enviaremail('0','{$registro.idReuniao}','Ata');"><i class="fa fa-list-alt"></i>&nbsp;&nbsp;Enviar e-mail para os participantes</a></li>
                                {/if}
                                {if $registro.idReuniao neq ''}
                                    {if $registro.stSituacao|default:'0' < 2}                                
                                        <li class="divider"></li>
                                        <li><a href="/reuniao/encerrar/idReuniao/{$registro.idReuniao}/opcao/{$opcao|default:null}/voltarmenu/{$voltarmenu}"><i class="fa fa-pencil-square-o"></i>&nbsp;&nbsp;Encerrar a reunião</a></li>
                                    {/if}
                                {/if}
                                <li class="divider"></li>
                                {if $voltarmenu|default:null}
                                    <li><a href="/menuReuniao"><i class="fa fa-eject"></i>&nbsp;&nbsp;Sair</a></li>
                                {else}    
                                    <li><a href="/reuniao/ata/opcao/{$opcao|default:null}"><i class="fa fa-eject"></i>&nbsp;&nbsp;Sair</a></li>
                                {/if}  
                            </ul>
                        </li>
                    </div> 
                </div>
                                
                <div class="panel-body panel panel-default" > 
{*                    <div class="panel-heading">
                        <h4> <strong>DADOS PRINCIPAIS  -  {$statusreuniao|default:''}</strong> <h4>
                    </div> 
*}                    <div class="panel-item" id="painel_colaboradores" >
                        <br>
                        <div class="row small">
                            <div class="col-md-1 hidden">
                                {if {$registro.idReuniao}>0}
                                        <label for="form-control">Identificador</label>
                                        <input type="text" class="form-control" name="idReuniao" id="idReuniao" value="{$registro.idReuniao}" READONLY>           
                                {else}
                                         <label for="form-control">Identificador</label>
                                         <input type="text" class="form-control" name="idReuniao" value="" READONLY>           
                                {/if}                     
                            </div> 
                            <div class="col-md-2">
                                <label for="form-control">Data da reunião</label>
                                <input type="text" class="form-control datetime" {if $registro.stSituacao|default:'0' > 1} disabled {/if} name="dtReuniao" id="dtReuniao" value="{$registro.dtReuniao|date_format:'%d/%m/%Y %H:%M'|default:''}" >           
                            </div> 
                            <div class="col-md-1">
                                <label for="form-control">Duração estimada</label>
                                <input type="text" readonly class="form-control" {if $registro.stSituacao|default:'0' > 1} disabled {/if} name="dsTempoDuracao" id="dsTempoDuracao" value="{$registro.dsTempoDuracao|default:''}" >           
                            </div>                     
                            <div class="col-md-2">
                                <label for="form-control">Tipo de reunião</label>
                                <select class="form-control" id="idTipo" {if $registro.stSituacao|default:'0' > 1} disabled {/if} name="idTipo"> 
                                        {include file="reuniao/lista_tipo.tpl"}       
                                </select>                                    
                            </div> 
                            <div class="col-md-2">
                                <label for="Local">Local da reunião</label>                        
                                <input data-label="Local" type="text" data-tipo="dsLocal" class="form-control complete_Local" name="dsLocal" {if $registro.stSituacao|default:'0' > 1} disabled {/if}  id="dsLocal" value="{$registro.dsLocalReuniao|default:''}"/>     {*onkeypress="pedido.completar_Parceiro();" *}
                                <input type="hidden" id="idLocal"  value="{$registro.idLocalReuniao|default:''}" name="idLocal"/>
                            </div>                                                                     
                            <div class="col-md-2">
                                <label for="form-control">Data da realização</label>
                                <input type="text" {if $registro.stSituacao|default:'0' > 1} disabled {/if}  class="form-control datetime" name="dtRealizacao" id="dtRealizacao" value="{$registro.dtRealizacao|date_format:'%d/%m/%Y %H:%M'|default:{$registro.dtReuniao|date_format:'%d/%m/%Y %H:%M'}}" >           
                            </div> 
                            <div class="col-md-2">
                                <label for="form-control">Usuario responsavel pela ATA</label>
                                <select class="form-control" name="idUsuarioAta" id="idUsuarioAta" {if $registro.stSituacao|default:'0' > 1} disabled {/if}  {if $registro.idReuniao eq ''} disabled {/if} > 
                                        {include file="reuniao/lista_usuarioAta.tpl"}       
                                </select>                                    
                            </div>   
                            <div class="col-md-1">
                                <label for="form-control">Duração real</label>
                                <input type="text" class="form-control" name="dsTempoDuracaoReal" {if $registro.stSituacao|default:'0' > 1} disabled {/if}  id="dsTempoDuracaoReal" value="{$registro.dsTempoDuracaoReal|default:''}" >           
                            </div>                         
                        </div>                                             
                        <br>
                        <div class="row small">
                            <div class="col-md-7">
                                <label for="form-control">Pauta da reunião</label>
                                <textarea class="form-control" {if $registro.stSituacao|default:'0' > 1} disabled {/if}  rows="5" name="dsAssunto" id="dsAssunto">{$registro.dsAssunto|default:''}</textarea>           
                            </div> 
                            <div class="col-md-5">
                                <br>
                                <label for="form-control">Histórico das reuniões relacionadas a esta</label>
                                <table class="table table-striped" border="0">
                                    <thead>
                                        <tr>
                                            <th style="font-size:8px!important;">Data</th>
                                            <th style="font-size:8px!important;">Local</th>
                                            <th style="font-size:8px!important;">Pauta</th>
                                            <th style="font-size:8px!important;">Ação</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {foreach from=$reuniao_listaRamificada item="linha"}  
                                        <tr>
                                            <td {if $linha.idReuniao eq $registro.idReuniao} style="color: green;" {/if} >{$linha.dtReuniao|date_format:'%d/%m/%Y %H:%M'|default:''}</td> 
                                            <td {if $linha.idReuniao eq $registro.idReuniao} style="color: green;" {/if} >{$linha.dsLocalReuniao|default:''}</td> 
                                            <td {if $linha.idReuniao eq $registro.idReuniao} style="color: green;" {/if} >{$linha.dsPauta|default:''}</td>
                                            <td>
                                                {if $linha.idReuniao neq $registro.idReuniao}
                                                    <a title="Ver" class="glyphicon glyphicon-exclamation-sign" href="/reuniao/novo_ata/idReuniao/{$linha.idReuniao}/opcao/{$opcao|default:null}/voltarmenu/{$voltarmenu}"></a>
                                                {/if}
                                            </td>                                        
                                        </tr>
                                        {/foreach}        
                                    </tbody>
                                </table>
                                <a href="/reuniao/continua_reuniao/idReuniao/{$registro.idReuniao}/opcao/{$opcao|default:null}/voltarmenu/{$voltarmenu}"> Clique aqui </a>  para criar uma nova reunião a partir desta!                        
                            </div>                    
                        </div> 
                        <br> 
{*                        <div {if $registro.stSituacao|default:'0' > 1} readonly {/if} >
                            <input {if $registro.stSituacao|default:'0' > 1} disabled {/if} type="submit" value="SALVAR" name="btnGravar" class="btn btn-primary" />         
                            <input type="button" value="ANEXAR" onclick="reuniao.selecionarfotoR('ATA');" id="btnAnexar" name="btnAnexar" class="btn btn-primary" {if $registro.idReuniao eq ''} disabled {/if}/>         
                            <input type="button" value="CRIAR PDF" onclick="reuniao.criarPDF('{$registro.idReuniao}');" id="btnPDF" name="btnPDF" class="btn btn-primary" {if $registro.idReuniao eq ''} disabled {/if}/>         
                            <a title="Ver o PDF" class="btn btn-primary" href="/{$registro.dsCaminhoPDF}" target="_blanck" {if $registro.dsCaminhoPDF eq ''} disabled {/if} >VER PDF</a>
                            <input type="button" value="E-MAIL" onclick="reuniao.enviaremail('0','{$registro.idReuniao}','Ata');" id="btnAnexar" name="btnAnexar" class="btn btn-primary" {if $registro.idReuniao eq ''} disabled {/if}/>         
                            {if $voltarmenu|default:null}
                                <a href="/menuReuniao" class="btn btn-primary">SAIR</a>
                            {else}    
                                <a href="/reuniao/ata/opcao/{$opcao|default:null}" class="btn btn-primary">SAIR</a>
                            {/if}  
                            <a {if $registro.stSituacao|default:'0' > 1} disabled {/if} href="/reuniao/encerrar/idReuniao/{$registro.idReuniao}/opcao/{$opcao|default:null}/voltarmenu/{$voltarmenu}" class="btn btn-primary">ENCERRAR</a>                                     
                        </div>
                        <br>
*}                    </div>
                </div>
                <div class="panel-item panel panel-default"> 
                    <div class="panel-heading mostra">
                        <h4> <strong>Participantes</strong> <h4>
                    </div> 
                    <div class="panel-body" id="painel_colaboradores">
                        <br>
                        <div class="row small" >
{*                            <div style="font-size: small;"> &nbsp; ADICIONAR PARTICIPANTES PARA ESTA REUNIÃO</div>
                            <br>
*}                            <input type="text" class="form-control hidden" name="idParticipante" id="idParticipante">                           
                            <div class="col-md-2">
                                <label for="form-control">Participante</label>
                                <select  {if $registro.stSituacao|default:'0' > 1} disabled {/if} class="form-control"  id="idUsuario"  {if $registro.idReuniao eq ''} disabled {/if} onchange='reuniao.lerUsuario();'> 
                                        {include file="reuniao/lista_usuario.tpl"}       
                                </select>                                    
                            </div>      
                            <div class="col-md-2">
                                <label for="form-control">Nome</label>
                                <input {if $registro.stSituacao|default:'0' > 1} disabled {/if} type="text" class="form-control" name="dsNome" id="dsNome" {if $registro.idReuniao eq ''} disabled {/if} >           
                            </div> 
                            <div class="col-md-2">
                                <label for="form-control">Email do participante</label>
                                <input {if $registro.stSituacao|default:'0' > 1} disabled {/if} type="text" class="form-control" name="dsEmail" id="dsEmail" {if $registro.idReuniao eq ''} disabled {/if} >           
                            </div> 
                            <div class="col-md-2">
                                <label for="form-control">Celular do participante</label>
                                <input {if $registro.stSituacao|default:'0' > 1} disabled {/if} type="text" class="form-control" name="dsCelular" id="dsCelular" {if $registro.idReuniao eq ''} disabled {/if} >           
                            </div>                                 
                            <div class="col-md-3">
                                <label for="form-control">Assunto específico do participante</label>
                                <input {if $registro.stSituacao|default:'0' > 1} disabled {/if} type="text" class="form-control" id="dsAssuntoE" {if $registro.idReuniao eq ''} disabled {/if} >           
                            </div> 

                            <br>
                            <div class="col-md-1">
                              <div class="row small">
                                  <a {if $registro.stSituacao|default:'0' > 1} disabled {/if} class="btn btn-primary" id="btn-adicionaitem" title="ADICIONAR" onclick="reuniao.adicionaritemAta();" {if $registro.idReuniao eq ''} disabled {/if}  >ADICIONAR</a> 
                              </div> 
                            </div> 
                        </div>
                        <br>
                        <div id="mostraritens">
                             {include file="reuniao/itensata.html"}
                        </div>
                    </div>
                </div>    
                        
                <div class="panel-item panel panel-default"> 
                    <div class="panel-heading mostra">
                        <h4> <strong>ATA da Reunião</strong> <h4>
                    </div> 
                    <div class="panel-body" id="painel_colaboradores">
                        <br>
                        <div class="row small" >
                            <div class="col-md-12">
                                <label for="form-control">Digite a ata da reunião {if $registro.dsCaminhoPDF|default:''} (caminho do arquivo: {$registro.dsCaminhoPDF|default:''}) {/if}</label>
                                <textarea  {if $registro.stSituacao|default:'0' > 1} disabled {/if} class="form-control"  rows="20" name="dsAta" id="dsAta">{$registro.dsAta|default:''}</textarea>           
                            </div>                         
                        </div>
                        <br>
{*                        <input {if $registro.stSituacao|default:'0' > 1} disabled {/if} type="submit" value="SALVAR" name="btnGravar" class="btn btn-primary" />                                     
                        <input type="button" value="ANEXAR" onclick="reuniao.selecionarfotoR('ATA');" id="btnAnexar" name="btnAnexar" class="btn btn-primary" {if $registro.idReuniao eq ''} disabled {/if}/>         
                        <input type="button" value="CRIAR PDF" onclick="reuniao.criarPDF('{$registro.idReuniao}');" id="btnPDF" name="btnPDF" class="btn btn-primary" {if $registro.idReuniao eq ''} disabled {/if}/>         
                        <a title="Ver o PDF" class="btn btn-primary" href="/{$registro.dsCaminhoPDF|default:''}" target="_blanck" {if $registro.dsCaminhoPDF eq ''} disabled {/if} >VER PDF</a>
                        <input type="button" value="E-MAIL" onclick="reuniao.enviaremail('0','{$registro.idReuniao}','Ata');" id="btnAnexar" name="btnAnexar" class="btn btn-primary" {if $registro.idReuniao eq ''} disabled {/if}/>                         
                        {if $voltarmenu|default:null}
                            <a href="/menuReuniao" class="btn btn-primary">SAIR</a>
                        {else}    
                            <a href="/reuniao/ata/opcao/{$opcao|default:null}" class="btn btn-primary">SAIR</a>
                        {/if}    
                        <a {if $registro.stSituacao|default:'0' > 1} disabled {/if} href="/reuniao/encerrar/idReuniao/{$registro.idReuniao}/opcao/{$opcao|default:null}/voltarmenu/{$voltarmenu}" class="btn btn-primary">ENCERRAR</a>                                     
*}                    </div>
                </div>                          
            </form>                      
            <div class="panel-item panel panel-default"> 
                <div class="panel-heading mostra">
                    <h4> <strong>Definir ações aos participantes</strong> <h4>
                </div> 
                <div class="panel-body" id="painel_colaboradores">
                    <br>
                    <div class="row small" >
{*                        <h4> &nbsp; ADICIONAR PARTICIPANTES PARA AS AÇÕES</h4>
                        <br>
*}                      <input type="text" class="form-control hidden" name="idTarefa" id="idTarefa">                           
                        <div class="col-md-3">
                            <label for="form-control">Participante</label>
                            <select  {if $registro.stSituacao|default:'0' > 1} disabled {/if} class="form-control"  id="idUsuarioTarefa" name="idUsuarioTarefa" > 
                                    {include file="reuniao/lista_usuariotarefa.tpl"}       
                            </select>                                    
                        </div>                                             
                        <div class="col-md-6">
                            <label for="form-control">Ação a ser executada</label>
                            <input {if $registro.stSituacao|default:'0' > 1} disabled {/if} type="text" class="form-control" name="dsTarefa" id="dsTarefa" >           
                        </div> 
                        <div class="col-md-2">
                            <label for="form-control">Prazo para entrega</label>
                            <input {if $registro.stSituacao|default:'0' > 1} disabled {/if} type="text" class="form-control datetime" name="dtPrazo" id="dtPrazo" value="" >           
                        </div> 

                        <br>
                        <div class="col-md-1">
                          <div class="row small">
                              <a {if $registro.stSituacao|default:'0' > 1} disabled {/if} class="btn btn-primary" id="btn-adicionaitem" title="Adicionar o participante" onclick="reuniao.adicionarTarefa('ata');" {if $registro.idReuniao eq ''} disabled {/if}  >Adicionar</a> 
                          </div> 
                        </div> 
                    </div>
                    <br>
                    <div id="mostraritenstarefa">
                         {include file="reuniao/itenstarefa.html"}
                    </div>
{*                    <br>
                    <input type="button" value="ANEXAR" onclick="reuniao.selecionarfotoR('ATA');" id="btnAnexar" name="btnAnexar" class="btn btn-primary" {if $registro.idReuniao eq ''} disabled {/if}/>         
                    <input type="button" value="CRIAR PDF" onclick="reuniao.criarPDF('{$registro.idReuniao}');" id="btnPDF" name="btnPDF" class="btn btn-primary" {if $registro.idReuniao eq ''} disabled {/if}/>         
                    <a title="Ver o PDF" class="btn btn-primary" href="/{$registro.dsCaminhoPDF|default:''}" target="_blanck" {if $registro.dsCaminhoPDF eq ''} disabled {/if} >VER PDF</a>
                    <input type="button" value="E-MAIL" onclick="reuniao.enviaremail('0','{$registro.idReuniao}','Ata');" id="btnAnexar" name="btnAnexar" class="btn btn-primary" {if $registro.idReuniao eq ''} disabled {/if}/>                         
                    {if $voltarmenu|default:null}
                        <a href="/menuReuniao" class="btn btn-primary">SAIR</a>
                    {else}    
                        <a href="/reuniao/ata/opcao/{$opcao|default:null}" class="btn btn-primary">SAIR</a>
                    {/if}    
                    <a {if $registro.stSituacao|default:'0' > 1} disabled {/if} href="/reuniao/encerrar/idReuniao/{$registro.idReuniao}/opcao/{$opcao|default:null}/voltarmenu/{$voltarmenu}" class="btn btn-primary">ENCERRAR</a>                                     
*}                </div>
            </div>               
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
{*<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>*}
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/reuniao/reuniao.js"></script>
<script src="/files/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>
<link href="/files/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css"/>

{include file="comuns/footer.tpl"}

