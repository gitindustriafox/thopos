{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="modal_fotos">
        {include file="reuniao/modalFotos/modal.tpl"}                            
    </div>             
    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <br><br>
            <form name="frm-reuniao_nova" 
                  action="/reuniao/gravar_reuniao" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario()" id="frm-reuniao_nova">
                <br>
                <input type="hidden" class="form-control" name="opcao" id="opcao" value="{$opcao}"> 
                <input type="hidden" class="form-control" name="idReuniao" id="idReuniao" value="{$registro.idReuniao|default:''}">           
                
                <div class="panel-body panel panel-default" > 
                    <div class="row small">
                        <li class="dropdown user-dropdown">
                            <a
                                href="#" id="menureuniao" class="dropdown-toggle"  style="color: #000000; font-size: large" data-toggle="dropdown"> 
                                <i class="fa fa-list" style="font-size: large;"> &nbsp;&nbsp; {if $opcao eq 'pessoas'} Adicionar ou excluir pessoas {else} {if $opcao eq 'consultar'} Ver dados da reunião {else} Agendar uma reunião {/if} {/if} </i> 
                            </a>
                            <ul class="dropdown-menu">
                                {if $opcao|default:null neq 'pessoas' && $opcao|default:null neq 'consultar'}
                                    <li><a onclick="reuniao.gravar_reuniao();" ><i class="fa fa-calendar"></i>&nbsp;&nbsp;Salvar as informações</a></li>                                                
                                {/if}
                                {if $opcao|default:null neq 'pessoas' && $opcao|default:null neq 'consultar'}
                                    <li class="divider"></li>
                                    <li><a onclick="reuniao.selecionarfotoR('Reuniao');"><i class="fa fa-adjust"></i>&nbsp;&nbsp;Anexar arquivos</a></li>
                                {/if}
                                {if $opcao|default:null neq 'pessoas' && $opcao|default:null neq 'consultar'}
                                    <li class="divider"></li>
                                    <li><a onclick="reuniao.enviaremail('0','{$registro.idReuniao|default:''}','Reuniao');"><i class="fa fa-list-alt"></i>&nbsp;&nbsp;Enviar e-mail para os participantes</a></li>
                                {/if}
                                <li class="divider"></li>
                                <li><a href="/menuReuniao"><i class="fa fa-eject"></i>&nbsp;&nbsp;Sair</a></li>
                            </ul>
                        </li>
                    </div> 
                </div>
                
                <div class="row small">
                    <div class="col-md-2">
                        <label for="form-control">Data para reunião</label>
                        <input type="text" class="form-control datetime" name="dtReuniao" {if $opcao|default:null eq 'pessoas' || $opcao|default:null eq 'consultar'} READONLY {/if} id="dtReuniao" value="{$registro.dtReuniao|date_format:'%d/%m/%Y %H:%M'|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">Duração da reunião</label>
                        <input type="text" class="form-control" name="dsTempoDuracao" {if $opcao|default:null eq 'pessoas'  || $opcao|default:null eq 'consultar'} READONLY {/if} id="dsTempoDuracao" value="{$registro.dsTempoDuracao|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">Tipo de reunião</label>
                        <select {if $opcao|default:null eq 'pessoas'  || $opcao|default:null eq 'consultar'} DISABLED {/if} class="form-control"  id="idTipo" name="idTipo"> 
                                {include file="reuniao/lista_tipo.tpl"}       
                        </select>                                    
                    </div>                                             
                    <div class="col-md-2">
                        <label for="Local">Local da reunião</label>                        
                        <input data-label="Local" type="text" data-tipo="dsLocal" class="form-control complete_Local" {if $opcao|default:null eq 'pessoas'  || $opcao|default:null eq 'consultar'} READONLY {/if} name="dsLocal"  id="dsLocal" value="{$registro.dsLocalReuniao|default:''}"/>     {*onkeypress="pedido.completar_Parceiro();" *}
                        <input type="hidden" id="idLocal" value="{$registro.idLocalReuniao|default:''}" name="idLocal"/>
                    </div>      
                    
                </div>                                             
                <br>
                <div  class="row small">
                    <div class="col-md-12">
                        <label for="form-control">Pauta da reunião</label>
                        <textarea class="form-control"  rows="5" name="dsAssunto" {if $opcao|default:null eq 'pessoas'  || $opcao|default:null eq 'consultar'} READONLY {/if} id="dsAssunto">{$registro.dsAssunto|default:''}</textarea>           
                    </div> 
                </div> 
                <br>  
            </form>
            {if $opcao|default:null neq 'consultar'} 
                <div class="panel-item panel panel-default"> 
                    <div class="panel-heading mostra">
                        <h4> <strong>Participantes</strong> <h4>
                    </div> 
                    <div class="panel-body" id="painel_colaboradores">
                        <br>
                        <div class="row small" >
                        <input type="text" class="form-control hidden" name="idParticipante" id="idParticipante">                           
                            <div class="col-md-2">
                                <label for="form-control">Participante</label>
                                <select class="form-control"  id="idUsuario" name="idUsuario" {if $registro.idReuniao|default:'' eq ''} disabled {/if} onchange='reuniao.lerUsuario();'> 
                                        {include file="reuniao/lista_usuario.tpl"}       
                                </select>                                    
                            </div>                                             
                            <div class="col-md-2">
                                <label for="form-control">Nome do participante</label>
                                <input type="text" class="form-control" name="dsNome" id="dsNome" {if $registro.idReuniao|default:'' eq ''} disabled {/if} >           
                            </div> 
                            <div class="col-md-2">
                                <label for="form-control">Email do participante</label>
                                <input type="text" class="form-control" name="dsEmail" id="dsEmail" {if $registro.idReuniao|default:'' eq ''} disabled {/if} >           
                            </div> 
                            <div class="col-md-1">
                                <label for="form-control">Celular</label>
                                <input type="text" class="form-control" name="dsCelular" id="dsCelular" {if $registro.idReuniao|default:'' eq ''} disabled {/if} >           
                            </div> 
                            <div class="col-md-4">
                                <label for="form-control">Assunto específico para este participante</label>
                                <input type="text" class="form-control" name="dsAssuntoE" id="dsAssuntoE" {if $registro.idReuniao|default:'' eq ''} disabled {/if} >           
                            </div> 
                            <br>
                            <div class="col-md-1">
                                <a class="btn btn-primary" id="btn-adicionaitem" title="Clique para adicionar este participante" onclick="reuniao.adicionaritem();" {if $registro.idReuniao|default:'' eq ''} disabled {/if}  >Adicionar</a> 
                            </div> 
                        </div>
                        <br>
                        <div id="mostraritens">
                             {include file="reuniao/itens.html"}
                        </div>
                    </div>
                </div>
            {else}
                <div class="panel-item panel panel-default">
                    <br>    
                    <div> <strong> &nbsp; Participantes</strong> </div>
                    <div id="mostraritens">
                         {include file="reuniao/itens.html"}
                    </div>
                </div>                           
            {/if}
        </div>
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
{*<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>*}
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/reuniao/reuniao.js"></script>
<script src="/files/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>
<link href="/files/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css"/>

{include file="comuns/footer.tpl"}

