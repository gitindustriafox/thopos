<div id="reuniaofotos_show" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-lg" role="document" style="width: 90%">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Anexar arquivos para reunião</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form name="frm-importar-fotos" 
                    action="/reuniao/adicionarfoto" 
                    method="POST" 
                    enctype="multipart/form-data">
                    <div class="modal-body"></div>
            </form>    
        </div>
    </div>
</div>