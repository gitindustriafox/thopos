        <table class="table table-striped" border="1">
            <thead>
                <tr>
{*                    <th>ID</th>
*}                    <th>Nome</th>
                    <th>Arquivo</th>
                    <th>Ação</th>
                </tr>
            </thead>
            <tbody>
                {foreach from=$fotos_lista item="linha"}  
                <tr>
{*                    <td>{$linha.idDocumento}</td>
*}                    <td>{$linha.dsNome|default:''}</td> 
                    <td>{$linha.dsLocalArquivo}</td> 
                    <td><a 
                          class="glyphicon glyphicon-camera" title ="Ver o arquivo"
                          href="http://thopos.tecnologiafox.com/{$linha.dsLocalArquivo}" target="_blank"
                          ></a> |
                        <a class="glyphicon glyphicon-trash" title ="Excluir o arquivo" onclick="reuniao.excluiranexo('{$linha.idDocumento}');" ></a>                      
                </tr>
                {/foreach}        
            </tbody>
        </table>