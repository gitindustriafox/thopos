{*    <div class="row small form-group">
        <div class="container relatorio">
            <div class="col-md-12">
                <table class="table" border="1">
                    <tbody>
                        <tr>
                            <td rowspan="1" style="width: 18%;">
                                &nbsp; <img src="http://thopos.tecnologiafox.com/files/images/fox_logo.jpeg" width="80" height="50" alt=""/>
                            </td>
                            <td style="text-align: center; font-size: 12px;" >
                                <strong>
                                    ATA DE REUNIÃO
                                </strong>
                            </td>
                            <td style="text-align: center; font-size: 12px; width: 18%;" >
                                <strong>
                                    FL
                                </strong>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
*}            <div class="col-md-12">
                <table class="table">
                    <tbody>
                        <tr>
                            <td style="text-align: left; font-size: 8px; width: 32%;  border-left: 0.5px solid black;  border-top: 0.5px solid black;  border-bottom: 0.5px solid black; ">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td  style="text-align: left; font-size: 8px;" >
                                                <strong>
                                                    &nbsp; &nbsp; Pauta:
                                                </strong>                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 8px;"> <br> &nbsp; &nbsp; 
                                                {$reuniao.dsAssunto|default:''}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>                                
                            </td>
                            <td style="text-align: left; font-size: 8px; width: 32%;  border-left: 0.5px solid black;  border-top: 0.5px solid black;  border-bottom: 0.5px solid black; " >
                                <table  class="table">
                                    <tbody>
                                        <tr>
                                            <td  style="text-align: left; font-size: 8px;" >
                                                <strong>
                                                    &nbsp; &nbsp; Anexos:
                                                </strong>                                                
                                            </td>
                                        </tr>
                                        {foreach from=$anexos item="linha"}  
                                        <tr>
                                            <td style="font-size: 8px;"> <br> &nbsp; &nbsp; {$linha.dsNome|default:''}</td>
                                        </tr>
                                        {/foreach}        
                                    </tbody>
                                </table>                                
                            </td>
                            <td style="text-align: center; font-size: 8px; width: 18%; border-left: 0.5px solid black;  border-top: 0.5px solid black;  border-bottom: 0.5px solid black;  border-right: 0.5px solid black; ">
                                <table class="table" >
                                    <tbody>
                                        <tr>
                                            <td style="text-align: center; font-size: 8px;" >
                                                <strong>
                                                    &nbsp; &nbsp; Data:
                                                </strong>                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 8px;"> <br> &nbsp; &nbsp; 
                                                {$reuniao.dtRealizacao|date_format:'%d/%m/%Y %H:%M'|default:''}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 8px;"> <br> &nbsp; &nbsp; 
                                                {$reuniao.dsTempoDuracaoReal|default:''}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>                                
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-12">
                <table class="table">
                    <tbody>
                        <tr>
                            <td style="text-align: left; font-size: 10px; border-left: 0.5px solid black;  border-top: 0.5px solid black;  border-right: 0.5px solid black;  border-bottom: 0.5px solid black; " >
                                <strong>
                                    &nbsp; &nbsp; Ata da Reunião - Responsável: 
                                </strong> {$reuniao.usuarioresponsavel|default:''}
                            </td>
                        </tr>                        
                        <tr>
                            <td style="text-align: left; font-size: 8px; border-left: 0.5px solid black;  border-right: 0.5px solid black;  border-bottom: 0.5px solid black; " >
                                <br> &nbsp; &nbsp; {$reuniao.dsAta|default:''} <br> 
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>   
            <div class="col-md-12" >
                <table class="table" >
                    <tbody>
                        <tr>
                            <td style="text-align: left; font-size: 8px;">
                                <strong>
                                    &nbsp; &nbsp; Participantes:
                                </strong>  
                            </td>
                            <td  style="text-align: left; font-size: 8px;" >
                                <strong>
                                    &nbsp; &nbsp; E-mail:
                                </strong>                                                
                            </td>
                            <td  style="text-align: left; font-size: 8px;" >
                                <strong>
                                    &nbsp; &nbsp; Esteve:
                                </strong>                                                
                            </td>
                        </tr>
                        {foreach from=$participantes item="linha"}  
                        <tr>

                            <td style="font-size: 8px;"> <br> &nbsp; &nbsp; {$linha.dsNome|default:''}</td>
                            <td style="font-size: 8px;"> <br> &nbsp; &nbsp; {$linha.dsEmail|default:''}</td>
                            <td style="font-size: 8px;"> <br> &nbsp; &nbsp; {if $linha.ausenteoupresente|default:'' eq 2} Presente {else} Ausente {/if}</td>
                        </tr>
                        {/foreach}        
                    </tbody>
                </table>
            </div>   
            <div class="col-md-12">
                <table class="table">
                    <tbody>
                        <tr>
                            <td style="text-align: center; font-size: 10px; border-left: 0.5px solid black;  border-top: 0.5px solid black;  border-right: 0.5px solid black;  border-bottom: 0.5px solid black; " >
                                <strong>
                                    &nbsp; &nbsp; Ações
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left; font-size: 10px !important; border-left: 0.5px solid black;  border-right: 0.5px solid black;  border-bottom: 0.5px solid black; " >
                                <table  class="table">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center; font-size: 8px !important;">ITEM</th>
                                            <th style="text-align: center; font-size: 8px !important;">DESCRICAO</th>
                                            <th style="text-align: center; font-size: 8px !important;">RESPONSAVEL</th>
                                            <th style="text-align: center; font-size: 8px !important;">PRAZO</th>
                                            <th style="text-align: center; font-size: 8px !important;">STATUS</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {assign var="contadorlinha" value=1}  
                                        {foreach from=$tarefas item="linha"}  
                                        <tr>
                                            <td style="text-align: center; font-size: 8px !important;">{$contadorlinha}</td> 
                                            <td style="text-align: left; font-size: 8px !important;">{$linha.dsTarefa|default:''}</td> 
                                            <td style="text-align: left; font-size: 8px !important;">&nbsp; &nbsp;{$linha.dsNome|default:''}</td> 
                                            <td style="text-align: left; font-size: 8px !important;">&nbsp; &nbsp;{$linha.dtPrazo|date_format:'%d/%m/%Y %H:%M'|default:''}</td> 
                                            <td style="text-align: center; font-size: 8px !important;"> {if $linha.stStatusTarefa|default:'0' eq '1'} <img src="files/images/checktrue.jpeg" width="15" height="15" alt=""/> {/if} </td> 
                                        </tr>
                                        {$contadorlinha = $contadorlinha + 1} 
                                        {/foreach}        
                                    </tbody>
                                </table>                                
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
                                            
        </div>
    </div>
