{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>{if {$registro.idSetor}>0} ALTERAR SETOR: {$registro.dsSetor|default:''}{else} INCLUIR NOVO SETOR{/if}</h1></tt>
            </div>          
            <a href="/setor" class="btn btn-primary"> ABORTAR</a><br>

            <form name="frm-setor" 
                  action="/setor/gravar_setor" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario()">
                <br>
                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idSetor}>0}
                                <label for="form-control">CODIGO</label>
                                <input type="text" class="form-control" name="idSetor" id="idSetor" value="{$registro.idSetor}" READONLY>           
                        {else}
                                 <label for="form-control">CODIGO</label>
                                 <input type="text" class="form-control" name="idSetor" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-3">
                        <label for="form-control">DESCRICAO</label>
                        <input type="text" class="form-control" name="dsSetor" id="dsSetor" value="{$registro.dsSetor|default:''}" >           
                    </div> 
                    <div class="col-md-3">
                        <label for="form-control">E-MAIL</label>
                        <input type="text" class="form-control" name="dsEmail" id="dsEmail" value="{$registro.dsEmail|default:''}" >           
                    </div> 
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nome">EXECUTADOR DE TICKETS</label>
                            <select class="form-control" name="stRecebeTickets" id="idExecutadorTickets">
                                {html_options options=$lista_recebetickets selected=$registro.stRecebeTickets|default:''}
                            </select>                      
                        </div>
                    </div>                                      
                </div> 
                <br>            
                    <input type="submit" value="GRAVAR" name="btnGravar" class="btn btn-primary" />         
                <br>
                <br>
            </form>
            
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/setor/setor_novo.js"></script>



{include file="comuns/footer.tpl"}

