{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>{if {$registro.idTipoOcorrencia}>0}  ALTERAR TIPO DE OCORRENCIA:  {$registro.dsTipoOcorrencia|default:''} {else} INCLUIR NOVO TIPO DE OCORRENCIA{/if}</h1></tt>
            </div>          
            <a href="/tipoocorrencia" class="btn btn-primary"> ABORTAR</a><br>

            <form name="frm-tipoocorrencia" 
                  action="/tipoocorrencia/gravar_tipoocorrencia" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario()">
                <br>
                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idTipoOcorrencia}>0}
                                <label for="form-control">CODIGO</label>
                                <input type="text" class="form-control" name="idTipoOcorrencia" id="idTipoOcorrencia" value="{$registro.idTipoOcorrencia}" READONLY>           
                        {else}
                                 <label for="form-control">CODIGO</label>
                                 <input type="text" class="form-control" name="idTipoOcorrencia" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-3">
                        <label for="form-control">DESCRICAO</label>
                        <input type="text" class="form-control" name="dsTipoOcorrencia" id="dsTipoOcorrencia" value="{$registro.dsTipoOcorrencia|default:''}" >           
                    </div> 
                </div> 
                <br>            
                    <input type="submit" value="GRAVAR" name="btnGravar" class="btn btn-primary" />         
                <br>
                <br>
            </form>
            
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/tipoocorrencia/tipoocorrencia_novo.js"></script>



{include file="comuns/footer.tpl"}

