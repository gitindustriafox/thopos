{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>{if {$registro.idParceiro}>0} ALTERAR PARCEIRO: {$registro.dsParceiro|default:''}{else} INCLUIR NOVO PARCEIRO{/if}</h1></tt>
            </div>          
            <a href="/parceiro" class="btn btn-primary"> ABORTAR</a><br>

            <form name="frm-Parceiro" 
                  action="/parceiro/gravar_Parceiro" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario();">
                <br>
                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idParceiro}>0}
                                <label for="form-control">CODIGO</label>
                                <input type="text" class="form-control" name="idParceiro" id="idParceiro" value="{$registro.idParceiro}" READONLY>           
                        {else}
                                 <label for="form-control">CODIGO</label>
                                 <input type="text" class="form-control" name="idParceiro" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-3">
                        <label for="form-control">NOME DO PARCEIRO</label>
                        <input type="text" class="form-control" name="dsParceiro" id="dsParceiro" value="{$registro.dsParceiro|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">C.N.P.J</label>
                        <input type="text" class="form-control" name="cdCNPJ" id="cdCNPJ" value="{$registro.cdCNPJ|default:''}" >           
                    </div> 
                    <div class="col-md-3">
                        <label for="form-control">ENDERECO</label>
                        <input type="text" class="form-control" name="dsEndereco" id="dsEndereco" value="{$registro.dsEndereco|default:''}" >           
                    </div> 
                    <div class="col-md-3">
                        <label for="form-control">CIDADE</label>
                        <input type="text" class="form-control" name="dsCidade" id="dsCidade" value="{$registro.dsCidade|default:''}" >           
                    </div> 
                </div>  
                <br>                    
                <div class="row small">
                    <div class="col-md-1">
                        <label for="form-control">C.E.P</label>
                        <input type="text" class="form-control" name="cdCEP" id="cdCEP" value="{$registro.cdCEP|default:''}" >           
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">U.F</label>
                        <input type="text" class="form-control" name="cdUF" id="cdUF" value="{$registro.cdUF|default:'SP'}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">CONTATO</label>
                        <input type="text" class="form-control" name="dsContato" id="dsContato" value="{$registro.dsContato|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">FONE</label>
                        <input type="text" class="form-control" name="dsFone" id="dsFone" value="{$registro.dsFone|default:''}" >           
                    </div> 
                    <div class="col-md-3">
                        <label for="form-control">CELULAR</label>
                        <input type="text" class="form-control" name="dsCelular" id="dsCelular" value="{$registro.dsCelular|default:''}" >           
                    </div> 
                    <div class="col-md-3">
                        <label for="form-control">EMAIL</label>
                        <input type="text" class="form-control" name="dsEmail" id="dsEmail" value="{$registro.dsEmail|default:''}" >           
                    </div> 
                </div>    
                <br>                    
                <div class="row small">
                    <div class="col-md-2">
                        <label for="form-control">SITE</label>
                        <input type="text" class="form-control" name="dsSite" id="dsSite" value="{$registro.dsSite|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="nomemaoobra">TIPO DE MAO DE OBRA</label>
                            <select class="form-control" name="idTipoParceiro" id="idTipoParceiro">
                                {html_options options=$lista_tipo selected=$registro.idTipoParceiro}
                            </select>                      
                        </div>
                    </div> 
                </div> 
                <br>
                  <div class="col-md-3">
                    <div class="row small">
                        <input class="btn btn-primary" type="submit" value="    GRAVAR" name="btnGravar"/>         
                    </div> 
                  </div> 
                <br>
            </form>
            
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
{*<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
*}<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/parceiro/Parceiro_novo.js"></script>



{include file="comuns/footer.tpl"}

