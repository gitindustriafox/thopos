{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <br>
            <div class="alert alert-info" >
                <tt><h1>MOVIMENTACAO DO ESTOQUE - ENTRADAS</h1></tt>
            </div> 
            <div class="row small">                
                <div class="col-md-1">
                    <a class="btn btn-primary" id="btn-novaentrada" title="Clique aqui para iniciar a digitação de uma nova entrada de estoque" href="/entradaestoque/novaentrada" {*onclick="entradaestoque.novaentrada();"*}>INCLUIR</a> 
                </div>
                <div class="col-md-2">
                    <a class="btn btn-primary" id="btn-gravarcabecalho" title="Clique aqui para gravar o cabeçalho da Entrada de Estoque"onclick="entradaestoque.gravarcabecalho();"  >GRAVAR CABECALHO</a>  {*{if $registro.idMovimento  neq  ''}  disabled {/if} *}
                </div> 
                <div class="col-md-1">
                    {if $registro.idMovimento|default:''} 
                        <a class="btn btn-primary" id="btn-excluir" title="Clique aqui para excluir este movimento" href="/entradaestoque/delmovimento/idMovimento/{$registro.idMovimento}"> EXCLUIR</a><br>                
                    {/if}    
                </div>                
                <div class="col-md-2">
                    <a  class="btn btn-primary" id="btn-sairtela" title="Clique aqui para sair desta tela e voltar ao menu principal" href="/dashboard"  onclick="entradaestoque.desabilitaid();"> ABORTAR</a><br>                
                </div>
                
            </div>    
                <br>
                <div class="row small">
                    <h3> &nbsp; INFORMACOES DO CABECALHO: </h3>
                    <br>                    
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="movimento">ID</label>
                            <input type="text" class="form-control" name="idMovimento" id="idMovimento" value="{$registro.idMovimento|default:''}" READONLY>           
                        </div>
                    </div>                     
                        <div class="col-md-2">
                        <div class="form-group">
                            <label for="tipomovimento">TIPO DE MOVIMENTO</label>
                            <select class="form-control" name="idTipoMovimento" id="idTipoMovimento">
                                {html_options options=$lista_tipomovimento selected=$registro.idTipoMovimento}
                            </select>                      
                        </div>
                    </div>                     
                    <div class="col-md-6">
                        <label for="Parceiro">PARCEIRO</label>                        
                        <input data-label="Parceiro" type="text" data-tipo="dsParceiro" class="form-control complete_Parceiro" name="Parceiro"  id="Parceiro" value="{$registro.dsParceiro|default:''}"/>     {*onkeypress="pedido.completar_Parceiro();" *}
                        <input type="hidden" id="idParceiro" value="{$registro.idParceiro|default:''}" name="idParceiro"/>                        
                    </div>                     
                    <div class="col-md-2">
                        <label for="form-control">OBSERVACAO</label>
                        <input type="text" class="form-control" name="dsObservacao" id="dsObservacao" value="{$registro.dsObservacao|default:''}" >           
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">DATA</label>
                        <input type="text" class="form-control obg standard-mask-date standard-form-date standard-form-require" name="dtMovimento" id="dtMovimento" value="{$registro.dtMovimento|date_format:'%d/%m/%Y'|default:Date("d/m/Y")}" >           
                    </div> 
                </div> 
                <div class="row small">
                    <div class="col-md-1">
                        <label for="form-control">PEDIDO</label>
                        <input type="text" class="form-control" name="nrPedido" id="nrPedido" value="{$registro.nrPedido|default:''}" >        
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">NOTA</label>
                        <input type="text" class="form-control" name="nrNota" id="nrNota" value="{$registro.nrNota|default:''}" >           
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">QTDE</label>
                        <input type="text" class="form-control valor" name="qtTotalNota" id="qtTotalNota" value="{$registro.qtTotalNota|default:''} " >        
                    </div> 
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="motorista">MOTORISTA</label>
                            <select class="form-control" name="idColaborador" id="idColaborador">
                                {html_options options=$lista_colaborador selected=$registro.idColaborador}
                            </select>                      
                        </div>
                    </div>
                    <div class="col-md-1">
                        <label for="form-control">PLACA</label>
                        <input type="text" class="form-control" name="nrPlaca" id="nrPlaca" value="{$registro.nrPlaca|default:''} " >        
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">LACRE</label>
                        <input type="text" class="form-control" name="nrLacre" id="nrLacre" value="{$registro.nrLacre|default:''} " >        
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">NOTA 2</label>
                        <input type="text" class="form-control" name="nrNota2" id="nrNota2" value="{$registro.nrNota2|default:''}" >           
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">QTDE</label>
                        <input type="text" class="form-control valor" name="qtTotalNota2" id="qtTotalNota2" value="{$registro.qtTotalNota2|default:''} " >        
                    </div> 
                    <hr>
{*                    <div class="col-md-1">
                      <div class="row small">
                          <a class="btn btn-primary" id="btn-lerdadospedido" readonly title="Clique aqui para ler dados do pedido e baixa-lo automaticamente" {if $registro.idMovimento  eq  ''}  enabled {/if}  onclick="entradaestoque.lerpedido();">Ler Pedido </a> 
                      </div> 
                    </div> 
*}                    
                </div> 
                <div class="row small" >
                    <h3> &nbsp; PRODUTOS PARA ESTA ENTRADA:</h3>
                    <br>
                    <input type="hidden" class="form-control" id="idMovimentoItem" name="idMovimentoItem" readonly>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div id='labelproduto'>
                                <label for="linsumo">PRODUTO</label>                        
                            </div>
                            <input data-label="insumo" type="text" data-tipo="dsInsumo" class="form-control complete_produto" name="insumo" onchange="entradaestoque.lerncm();"  id="insumo" {if $registro.idMovimento  eq  ''}  disabled {/if}  value="{$registro.dsInsumo|default:''}"/> 
                            <input type="hidden" id="idInsumo" value="{$registro.idInsumo|default:''}" name="idInsumo"/>                            
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label for="form-control">N.C.M</label>
                        <input data-label="ncm" type="text" data-tipo="ncm" class="form-control complete_ncm"  {if $registro.idMovimento  eq  ''}  disabled {/if} name="dsNCM"  id="dsNCM" value=""/> 
                        <input type="hidden" id="idNCM" value="" name="idNCM"/>                            
                    </div> 
                </div> 
                <br>
                <div class="row small" >
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="localestoque">LOCAL PARA ESTOQUE </label>
                            <select class="form-control" name="idLocalEstoque" id="idLocalEstoque" {if $registro.idMovimento  eq ''}  disabled  {/if} onchange="entradaestoque.lerunidade();"> 
                                {include file="entradaestoque/lista_localestoque.tpl"}
                            </select>                      
                        </div>
                    </div>                            
                    <div class="col-md-1">
                        <label for="form-control">UNIDADE</label>
                        <input type="text" class="form-control" name="dsUnidade" id="dsUnidade" disabled='disabled' value="">       
                    </div> 
                    <div class="col-md-1 hidden">
                        <label for="form-control">ESTOQUE</label>
                        <input type="text" class="form-control valor"  name="qtEstoque" id="qtEstoque" disabled='disabled' value="">       
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">QUANTIDADE</label>
                        <input type="text" class="form-control valor" name="qtMovimento" id="qtMovimento" {if $registro.idMovimento  eq ''}  disabled  {/if} value="">      
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">VALOR TOTAL</label>
                        <input type="text" class="form-control valor" name="vlMovimento" {if $registro.idMovimento  eq  ''}  disabled {/if} id="vlMovimento" value=""> 
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">REFERENCIA</label>
                        <input type="text" class="form-control" name="dsObservacaoItem" {if $registro.idMovimento  eq  ''}  disabled {/if} id="dsObservacaoItem" value=""> 
                    </div> 
                </div>
                <div class="row small" >
                    <h3> &nbsp; DESTINO DO PRODUTO:</h3>
                    <br>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="centrocusto">CENTRO DE CUSTO </label>
                            <select class="form-control" name="idCentroCusto" {if $registro.idMovimento  eq  ''}  disabled {/if} id="idCentroCusto">
                                {html_options options=$lista_centrocusto selected=null}
                            </select>                      
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="maquina">MAQUINA </label>
                            <select class="form-control" name="idMaquina" {if $registro.idMovimento  eq  ''}  disabled {/if} id="idMaquina">  
                                {html_options options=$lista_maquina selected=null}
                            </select>                      
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="ordemservico">O.S </label>
                            <select class="form-control" name="idOS" {if $registro.idMovimento  eq  ''}  disabled {/if} id="idOS"> 
                                {html_options options=$lista_os selected=null}
                            </select>                      
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="motivo">MOTIVO DA O.S </label>
                            <select class="form-control" name="idMotivo" {if $registro.idMovimento  eq  ''} disabled {/if}  id="idMotivo">  
                                {html_options options=$lista_motivo selected=null}
                            </select>                      
                        </div>
                    </div>
                    <br>
                    <div class="col-md-1">
                      <div class="row small">
                          <a class="btn btn-primary" id="btn-adicionaitem" title="adicionaitem" {if $registro.idMovimento  eq  ''}  disabled {/if}  onclick="entradaestoque.gravaritem();">Adiciona Item</a> 
                      </div> 
                    </div> 
                </div>
                <br>
            {include file="entradaestoque/lista.html"}
            
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
{*<script src="/files/js/jquery.price_format.1.3"></script>
<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>*}


<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>

<script src="/files/js/entradaestoque/entradaestoque.js"></script>



{include file="comuns/footer.tpl"}

