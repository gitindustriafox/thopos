    <br>
        <div class="panel-body">
            <div class="row small" id="linhas_mensagens">
                <table class="table table-striped" border="1">
                    <thead>
                        <tr>
                            <th>ORIGEM DA MENSAGEM</th>
                            <th>USUARIO</th>
                            <th>ENVIADO EM</th>
                            <th>LIDO EM</th>
                            <th>ID/DOCUMENTO</th>
                            <th>DESCRICAO DA MENSAGEM</th>
                            <th>TIPO DE MENSAGEM</th>
                            <th>ACAO</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach from=$mensagens|default:'' item="linha"}  
                            <tr>
                                <td {if $linha.stSituacao eq 1} style="color:blue;" {/if}>{$linha.dsNomeAmigavel|default:''}</td> 
                                <td {if $linha.stSituacao eq 1} style="color:blue;" {/if}>{if $linha.idUsuarioDestino eq $smarty.session.user.usuario} De: {$linha.enviadopor|default:''} {else} Para: {$linha.enviadopara|default:''} {/if}</td> 
                                <td {if $linha.stSituacao eq 1} style="color:blue;" {/if}>{$linha.dtEnvio|date_format:"%d/%m/%Y %H:%M:%S"|default:''}</td> 
                                <td {if $linha.stSituacao eq 1} style="color:blue;" {/if}>{$linha.dtLeitura|date_format:"%d/%m/%Y %H:%M:%S"|default:''}</td> 
                                <td {if $linha.stSituacao eq 1} style="color:blue;" {/if}>{$linha.idTabela|default:''}</td> 
                                <td {if $linha.stSituacao eq 1} style="color:blue;" {/if}>
                                    {if $linha.idUsuarioDestino eq $smarty.session.user.usuario}        
                                    <
                                    {else}
                                    > 
                                    {/if}
                                    {if $linha.idMensagem neq $linha.idMensagemAnterior} ..R:....
                                    {/if} {$linha.dsMensagem|default:''}
                                </td> 
                                <td {if $linha.stSituacao eq 1} style="color:blue;" {/if}>{if $linha.idTipoMensagem|default:'' eq '0'} AVISO {else} DIALOGO {/if}</td> 
                                <td {if $linha.stSituacao eq 1} style="color:blue;" {/if}> 
                                    {if $linha.idUsuarioDestino eq $smarty.session.user.usuario}
                                        {if $linha.stSituacao eq 0}
                                            <a class="glyphicon glyphicon-flag" onclick="dashboard.marcarcomolido('{$linha.idMensagemItem|default:''}');">&nbsp;Lido</a> &nbsp;
                                        {/if}
                                        {if $linha.idTipoMensagem|default:'' neq '0'} 
                                            <a class="glyphicon glyphicon-flag" onclick="dashboard.digitarresposta('{$linha.idMensagem|default:''}');">&nbsp;Responder</a> &nbsp;
                                        {/if}
                                    {/if}
                                    {if $linha.idTipoMensagem|default:'' neq '0'} 
                                        {if $linha.dsCaminhoArquivo|default:''}
                                            <a class="glyphicon glyphicon-camera" href="http://thopos.tecnologiafox.com/{$linha.dsCaminhoArquivo|default:''}" target="_blank" >&nbsp;Foto&nbsp;</a>  
                                        {/if}
                                    {/if}
                                    {if $linha.idTipoMensagem|default:'' eq '0'}
                                        <a class="glyphicon glyphicon-trash" onclick="dashboard.excluirmensagem('{$linha.idMensagem|default:''}','{$linha.idMensagemItem|default:''}');">&nbsp;Excluir</a> 
                                    {else}
                                         {if $linha.idUsuarioDestino neq $smarty.session.user.usuario}
                                            {if $linha.stSituacao neq 0}
                                                <a class="glyphicon glyphicon-trash" onclick="dashboard.excluirmensagem('{$linha.idMensagem|default:''}','{$linha.idMensagemItem|default:''}');">&nbsp;Excluir</a> 
                                            {/if} 
                                         {/if}
                                    {/if}
                                </td>
                            </tr>
                        {/foreach}                                
                    </tbody>
                </table>
            </div>
        </div>