<div id="mensagens_show" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-lg" role="document" style="width: 70%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Mensagens</h4>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>
<div id="modal_resposta">
    {include file="dashboard/mensagem_responder/modal.tpl"}                            
</div> 
