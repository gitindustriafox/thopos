{include file="comuns/head.tpl"}

<div id="wrapper">
    <div id="page-wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}
            <div class="row small">
                <div class="col-lg-12">
                    <h1 class="page-header"></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>                
            <div id="modal_grafico">
                {include file="dashboard/grafico/modal.tpl"}                            
            </div>                
            <div id="modal_graficoInsumo">
                {include file="dashboard/grafico/modalInsumo.tpl"}                            
            </div> 
            <!-- /.row -->
            <div class="row small">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row small">
                                <div class="col-md-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div>
                                <div class="col-md-9 text-right">
                                    <div class="huge">{$totalsc|default:''}</div>
                                    <div class="huge_menor">SOLICITACOES EM ABERTO</div>
                                </div>
                            </div>
                        </div>
                        <a href="/dashboard/index_action/tipoos/1">
                            <div class="panel-footer">
                                <span class="pull-left huge_menor">Detalhes</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                                <div> &nbsp</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row small">
                                <div class="col-md-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-md-9 text-right">
                                    <div class="huge">{$totalpa|default:''}</div>
                                    <div class="huge_menor">PEDIDOS EM ABERTO</div>
                                </div>
                            </div>
                        </div>
                        <a onclick="dashboard.lerPedidos();">
                            <div class="panel-footer">
                                <span class="pull-left huge_menor">Detalhes</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                                <div> &nbsp</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row small">
                                <div class="col-md-3">
                                    <i class="fa fa-pause fa-5x"></i>
                                </div>
                                <div class="col-md-9 text-right">
                                    <div class="huge">{$totalpp|default:''}</div>
                                    <div class="huge_menor">PONTO DE PEDIDO</div>                                    
                                </div>
                            </div>
                        </div>
                        <a onclick="dashboard.lerPontoPedido();">
                            <div class="panel-footer">
                                <span class="pull-left huge_menor">Detalhes</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                                <div> &nbsp</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row small">
                                <div class="col-md-3">
                                    <i class="fa fa-dollar fa-5x"></i>
                                </div>
                                <div class="col-md-9 text-right">
                                    <div class="huge">{$totale|default:'0'|number_format:"2":",":"."}</div>
                                    <div class="huge_menor">VALOR TOTAL DO ESTOQUE</div>
                                </div>
                            </div>
                        </div>
                            <div class="panel-footer">
                                <div class="col-md-2 huge_menor">
                                    <span class="pull-left huge_menor fa fa-tasks fa-1x"></span>
                                    <a onclick="dashboard.lerestoquegrupo();">Detalhes</a>
                                </div>
                                <div class="col-md-2 huge_menor">
                                    <span class="pull-right huge_menor fa fa-area-chart fa-1x"></span>
                                    <a onclick="dashboard.grafico.montarGrafico();">Grafico </a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                                    
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row small">
                <div class="col-lg-12">
                    <!-- /.panel -->
                    <div class="panel panel-default">
                        <div id='mostrarprincipal'>
                                    {include file="{$arquivoprincipal|default:'dashboard/listasolcompras.html'}"}
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel .chat-panel -->
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
            <div class="row small">
                <div class="col-lg-12">
                    <!-- /.panel -->
                    <div class="panel panel-default">
                         <div id='mostrardetalhes'>
                                    {include file="{$arquivodetalhes|default:'dashboard/listasolcomprasitens.html'}"}                                    
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel .chat-panel -->
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
</div>

<!-- JavaScript -->
{*<script type="text/javascript" src="/files/js/jquery.mask.js"></script>
<script type="text/javascript" src="/files/js/jquery.price/jquery.price_format.1.3.js"></script>
<script type="text/javascript" src="/files/js/tablesorter/jquery.tablesorter.js"></script>
<script type="text/javascript" src="/files/js/tablesorter/tables.js"></script>
<script type="text/javascript" src="/files/js/jquery_ui/js/jquery-ui-1.10.4.custom.min.js"></script>
<script type="text/javascript" src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<script type="text/javascript" src="/files/js/util.js"></script>
*}
{include file="comuns/footer.tpl"}
