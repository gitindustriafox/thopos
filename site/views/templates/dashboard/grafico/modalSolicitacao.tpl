        <div id="graficoSolicitacao_show" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog  modal-lg" role="document" style="width: 80%">
                <div class="row small">
                    <div class="col-md-12">
                        <div id="piechart_3dSolicitacao" style="width: 1500px; height: 400px;"></div>                        
                    </div>  
                </div>
                <div class="row small">
                    <div class="col-md-12">
                        <div id="piechart_3dSolicitacaoSol" style="width: 1500px; height: 400px;"></div>                                                 
                    </div>
                </div>                
            </div>
        </div>