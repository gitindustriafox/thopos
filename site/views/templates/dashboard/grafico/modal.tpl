        <div id="grafico_show" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog  modal-lg" role="document" style="width: 50%">
                <div class="row small">
                    <div class="col-md-12">
                        <div id="piechart_3d" style="width: 900px; height: 500px;"></div>                        
                    </div>  
                </div>
                <div class="row small">
                    <div class="col-md-12">
                        <div id="piechart_3d1" style="width: 900px; height: 500px;"></div>                                                 
                    </div>
                </div>
            </div>
        </div>