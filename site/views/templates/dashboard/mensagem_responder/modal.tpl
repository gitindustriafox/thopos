<div id="digitarresposta_show" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-lg" role="document" style="width: 40%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">RESPOSTA</h4>
            </div>
            <form name="frm-importar_fotos" 
                    action="/mensagem/adicionarfoto" 
                    method="POST" 
                    enctype="multipart/form-data">
                    <div class="modal-body"></div>
            </form>    
        </div>
    </div>
</div>
{*<script src="/files/js/solicitacaocompras/solicitacaocompras.js"></script>                   *}