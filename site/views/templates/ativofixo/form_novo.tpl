{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <form name="frm-ativofixo" 
                  action="/ativofixo/gravar_ativofixo" 
                  method="POST" 
                  enctype="multipart/form-data">
{*                  onsubmit="return validaFormulario();">*}
                <div class="alert alert-info" >
                    <tt><h1>{if {$registro.idAtivoFixo}>0} ALTERAR ATIVO FIXO: {$registro.dsAtivoFixo|default:''}{else} INCLUIR NOVO ATIVO FIXO {/if}</h1></tt>
                </div>          
                <div class="row small">
                      <div class="col-md-1">
                           <a href="/ativofixo" class="btn btn-primary"> ABORTAR</a><br>
                      </div>                     
                      <div class="col-md-1">                          
                           <input class="btn btn-primary" type="submit" value="  GRAVAR" name="btnGravar" {*{if $registro.idCodigoAtivo|default:'' eq ''} disabled {/if} *}/>         
                      </div> 
                </div>
                <h3>Informações do Bem</h3>
                <br>
                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idAtivoFixo}>0}
                                <label for="form-control">ID</label>
                                <input type="text" class="form-control" name="idAtivoFixo" id="idAtivoFixo" value="{$registro.idAtivoFixo}" READONLY>           
                        {else}
                                 <label for="form-control">ID</label>
                                 <input type="text" class="form-control" name="idAtivoFixo" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">TIPO</label>
                        <select class="form-control"   {if $registro.idAtivoFixo>0} readonly {/if} id="idTipoAtivo" name="idTipoAtivo" onchange='ativofixo.lerClasse();'> 
                                {include file="ativofixo/lista_tipoativo.tpl"}       
                        </select>                                    
                    </div>                         
                    <div class="col-md-2">
                        <label for="form-control">CLASSE</label>
                        <select class="form-control"  {if $registro.idAtivoFixo>0} readonly {/if} id="idClasseAtivo" name="idClasseAtivo"  onchange='ativofixo.lerGrupo();'> 
                                {include file="ativofixo/lista_classeativo.tpl"}       
                        </select>                                    
                    </div>                         
                    <div class="col-md-2">
                        <label for="form-control">GRUPO</label>
                        <select class="form-control"  {if $registro.idAtivoFixo>0} readonly {/if} id="idGrupoAtivo" name="idGrupoAtivo"  onchange='ativofixo.lerCodigo();'> 
                                {include file="ativofixo/lista_grupoativo.tpl"}       
                        </select>                                    
                    </div>                         
                    <div class="col-md-1">
                        <label for="form-control">CODIGO DO ATIVO</label>
                        <input type="text" class="form-control" readonly name="idCodigoAtivo" id="idCodigoAtivo" value="{$registro.idCodigoAtivo|default:''}">       
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">SEQUENCIA</label>
                        <input type="text" class="form-control" readonly name="idSequenciaAtivo" id="idSequenciaAtivo" value="{$registro.idSequenciaAtivo|default:''}">       
                    </div> 
                    <div class="col-md-3">
                        <label for="form-control">DESCRIÇÃO DO ATIVO FIXO</label>
                        <input type="text" class="form-control" name="dsAtivoFixo" id="dsAtivoFixo" value="{$registro.dsAtivoFixo|default:''}">       
                    </div> 
                </div>
                <br>
                <div class="row small">
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="unidade">UNIDADE</label>
                            <select class="form-control" name="idUnidade" id="idUnidade">
                                {html_options options=$lista_unidade selected=$registro.idUnidade}
                            </select>                      
                        </div>
                    </div>                              
                    <div class="col-md-2">
                        <label for="form-control">MARCA</label>
                        <select class="form-control"  id="idMarca" name="idMarca"> 
                                {include file="ativofixo/lista_marca.tpl"}       
                        </select>                                    
                    </div>                         
                    <div class="col-md-2">
                        <label for="form-control">MODELO</label>
                        <select class="form-control"  id="idModelo" name="idModelo"> 
                                {include file="ativofixo/lista_modelo.tpl"}       
                        </select>                                    
                    </div>                         
                    <div class="col-md-2">
                        <label for="form-control">NUMERO DE SÉRIE</label>
                        <input type="text" class="form-control" name="nrSerie" id="nrSerie" value="{$registro.nrSerie|default:''}">       
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">CODIGO DO FABRICANTE</label>
                        <input type="text" class="form-control" name="dsCodigoDoFabricante" id="dsCodigoDoFabricante" value="{$registro.dsCodigoDoFabricante|default:''}">       
                    </div> 
                </div>
                </br>                    
                <div class="row small" >
                    <div class="col-md-12">
                        <label for="form-control">DESCRICAO DETALHADA</label>
                        <textarea data-label="dsDetalhadaAtivoFixo" type="text" name="dsDetalhadaAtivoFixo" id="dsDetalhadaAtivoFixo" rows="3"  class="form-control"> {$registro.dsDetalhadaAtivoFixo|default:''}</textarea>
                    </div> 
                </div>
                <br>
                <div class="row small">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label for="modelo">NCM</label>
                            <input data-label="ncm" type="text" data-tipo="ncm" class="form-control complete_ncm" name="dsNCM"  id="dsNCM" value="{$registro.cdNCM|default:''} - {$registro.dsNCM|default:''}"/> 
                            <input type="hidden" id="idNCM" value="{$registro.idNCM|default:''}" name="idNCM"/>                            
                        </div>
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">COLABORADOR RESPONSAVEL PELO USO</label>
                        <select class="form-control"  id="idColaborador" name="idColaborador"> 
                                {include file="ativofixo/lista_colaborador.tpl"}       
                        </select>                                    
                    </div>    
                </div>   
                {if $registro.idAtivoFixo}
                <h3>Informações da Aquisição</h3>
                </br>
                <div class="row small" >
                    <div class="col-md-4">
                        <label for="Parceiro">PARCEIRO</label>                        
                        <input data-label="Parceiro" type="text" data-tipo="dsParceiro" class="form-control" readonly name="dsParceiroAquisicao"  id="dsParceiroAquisicao" value="{$registro.dsParceiroAquisicao|default:''}"/>
                        <input type="hidden" id="idParceiroAquisicao" value="{$registro.idParceiroAquisicao|default:''}" name="idParceiroAquisicao"/>
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">DATA</label>
                        <input type="text" class="form-control obg standard-mask-date standard-form-date standard-form-require" readonly name="dtAquisicao" id="dtAquisicao" value="{$registro.dtAquisicao|date_format:'%d/%m/%Y'|default:Date('d/m/Y')}" >           
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">NOTA FISCAL</label>
                        <input type="text" class="form-control" name="nrNotaAquisicao" readonly id="nrNotaAquisicao" value="{$registro.nrNotaAquisicao|default:''}">       
                    </div>  
                    <div class="col-md-2">
                        <label for="form-control">VALOR AQUISIÇÃO</label>
                        <input type="text" class="form-control" name="vlAquisicao" readonly id="vlAquisicao" value="{$registro.vlAquisicao|default:'0'|number_format:"2":",":"."}"> 
                    </div>                     
                </div>
                <h3>Informações da Baixa</h3>
                </br>
                <div class="row small" >
                    <div class="col-md-4">
                        <label for="Parceiro">PARCEIRO</label>                        
                        <input data-label="Parceiro" type="text" data-tipo="dsParceiro" class="form-control" name="dsParceiroVenda" readonly id="dsParceiroVenda" value="{$registro.dsParceiroVenda|default:''}"/>    
                        <input type="hidden" id="idParceiroVenda" value="{$registro.idParceiroVenda|default:''}" name="idParceiroVenda"/>
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">DATA BAIXA</label>
                        <input type="text" class="form-control obg standard-mask-date standard-form-date standard-form-require" name="dtBaixa" id="dtBaixa" readonly value="{$registro.dtBaixa|date_format:'%d/%m/%Y'|default:Date('d/m/Y')}" >           
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">NOTA FISCAL</label>
                        <input type="text" class="form-control" name="nrNotaBaixa" id="nrNotaBaixa" readonly value="{$registro.nrNotaBaixa|default:''}">       
                    </div>                             
                    <div class="col-md-2">
                        <label for="form-control">VALOR BAIXA</label>
                        <input type="text" class="form-control" name="vlBaixa" id="vlBaixa" readonly value="{$registro.vlBaixa|default:'0'|number_format:"2":",":"."}"> 
                    </div>   
                    <div class="col-md-2">
                        <label for="form-control">MOTIVO DA BAIXA</label>
                        <select class="form-control"  id="idMotivoBaixa" readonly name="idMotivoBaixa"> 
                                {include file="ativofixo/lista_motivobaixa.tpl"}       
                        </select>                                    
                    </div>                        
                    <div class="col-md-2">
                        <label for="form-control">OBSERVACAO</label>
                        <input type="text" class="form-control" name="dsObsBaixa" readonly id="dsObsBaixa" value="{$registro.dsObsBaixa|default:''}">       
                    </div>                             
                </div>
                <h3>Informações de Depreciação</h3>
                </br>
                <div class="row small" >
                    <div class="col-md-2">
                        <label for="form-control">DATA PRIMEIRA DEPRECIAÇÃO</label>
                        <input type="text" class="form-control obg standard-mask-date standard-form-date standard-form-require" name="dtPrimeiraDepreciacao" id="dtPrimeiraDepreciacao" readonly value="{$registro.dtPrimeiraDepreciacao|date_format:'%d/%m/%Y'|default:Date('d/m/Y')}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">DATA ÚLTIMA DEPRECIAÇÃO</label>
                        <input type="text" class="form-control obg standard-mask-date standard-form-date standard-form-require" name="dtUltimaDepreciacao" id="dtUltimaDepreciacao" readonly value="{$registro.dtUltimaDepreciacao|date_format:'%d/%m/%Y'|default:Date('d/m/Y')}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">VALOR DEPRECIACAO MENSAL</label>
                        <input type="text" class="form-control" name="vlDepreciacao" id="vlDepreciacao" readonly value="{$registro.vlDepreciacao|default:'0'|number_format:"2":",":"."}"> 
                    </div>   
                    <div class="col-md-2">
                        <label for="form-control">VALOR DEPRECIACAO ACUMULADO</label>
                        <input type="text" class="form-control" name="vlDepreciacaoAcumulado" id="vlDepreciacaoAcumulado" readonly value="{$registro.vlDepreciacaoAcumulado|default:'0'|number_format:"2":",":"."}"> 
                    </div>   
                    <div class="col-md-2">
                        <label for="form-control">RESIDUAL CONTABIL</label>
                        <input type="text" class="form-control" name="vlResidualContabil" id="vlResidualContabil" readonly value="{$registro.vlResidualContabil|default:'0'|number_format:"2":",":"."}"> 
                    </div>   
                </div>
                </br>
                <h4>Histórico das Depreciações</h4>
                     {include file="ativofixo/depreciacoes.html"}                       
                </br>
                <div class="row small" >
                </div>
                <h3>Informações de Reparos</h3>
                </br>
                     {include file="ativofixo/reparos.html"}     
                {/if}
            </form>
        </div>
    </div>
</div>

{*<!-- JavaScript -->
<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>*}
<!-- Toast Message -->
{*<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>*}
<script src="/files/js/ativofixo/ativofixo.js"></script>



{include file="comuns/footer.tpl"}

