{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>{if {$registro.idTipoMaoObra}>0} ALTERAR TIPO DE MAO DE OBRA: {$registro.dsTipoMaoObra|default:''}{else} INCLUIR NOVO TIPO DE MAO DE OBRA{/if}</h1></tt>
            </div>          
            <a href="/tipomaoobra" class="btn btn-primary"> ABORTAR</a><br>

            <form name="frm-tipomaoobra" 
                  action="/tipomaoobra/gravar_tipomaoobra" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario()">
                <br>
                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idTipoMaoObra}>0}
                                <label for="form-control">CODIGO</label>
                                <input type="text" class="form-control" name="idTipoMaoObra" id="idTipoMaoObra" value="{$registro.idTipoMaoObra}" READONLY>           
                        {else}
                                 <label for="form-control">CODIGO</label>
                                 <input type="text" class="form-control" name="idTipoMaoObra" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-3">
                        <label for="form-control">DESCRICAO</label>
                        <input type="text" class="form-control" name="dsTipoMaoObra" id="dsTipoMaoObra" value="{$registro.dsTipoMaoObra|default:''}" >           
                    </div> 
                </div> 
                
                <br>            
                    <input type="submit" value="GRAVAR" name="btnGravar" class="btn btn-primary" />         
                <br>
                <br>
            </form>
            
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/tipomaoobra/tipomaoobra_novo.js"></script>



{include file="comuns/footer.tpl"}

