{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>FAZER LANÇAMENTOS PARA: {$registro.dsOrcamento|default:''}</h1></tt>
            </div>    
            <form name="frm-orcar" 
                  method="POST" 
                  enctype="multipart/form-data">
                <br>
                <a href="/lancardespesas" class="btn btn-primary"> ABORTAR</a>
                <br>
                <br>
                <input type="text" class="form-control hidden" name="idColaborador" id="idColaborador" value="{$idColaborador}" READONLY>           
                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idOrcamento}>0}
                                <label for="form-control">ID</label>
                                <input type="text" class="form-control" name="idOrcamento" id="idOrcamento" value="{$registro.idOrcamento}" READONLY>           
                        {else}
                                 <label for="form-control">ID</label>
                                 <input type="text" class="form-control" name="idOrcamento" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-3">
                        <label for="form-control">DESCRICAO DO ORCAMENTO</label>
                        <input type="text" class="form-control" readonly name="dsOrcamento" id="dsOrcamento" value="{$registro.dsOrcamento|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">DATA INCLUSÃO ORCAMENTO</label>
                        <input type="text" class="form-control datetime" readonly readonly name="dtCriacao" id="dtCriacao" value="{$registro.dtCriacao|date_format:'%d/%m/%Y'|default:Date("d/m/Y")}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">DATA VIGENCIA INICIAL</label>
                        <input type="text" class="form-control datetime" readonly name="dtInicioVigencia" id="dtInicioVigencia" value="{$registro.dtInicioVigencia|date_format:'%d/%m/%Y'|default:Date("d/m/Y")}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">DATA VIGENCIA FINAL</label>
                        <input type="text" class="form-control datetime" readonly name="dtFimVigencia" id="dtFimVigencia" value="{$registro.dtFimVigencia|date_format:'%d/%m/%Y'|default:Date("d/m/Y")}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">CRIADO POR</label>
                        <input type="text" class="form-control" name="dsUsuario" id="dsUsuario" value="{$registro.dsUsuario|default:''}" disabled >           
                    </div> 
                </div> 
            </form>
            <br>
            <div class="row small">
                <div class="col-md-12">
                    <label for="form-control">OBSERVACAO</label>
                    <input type="text" class="form-control" name="dsTermoAbertura" readonly id="dsTermoAbertura" value="{$registro.dsTermoAbertura|default:''}" >           
                </div> 
            </div> 
            <br>                
            <div class="row small">
                <input type="text" class="form-control hidden" name="stSituacao" id="stSituacao" value=""> 
                <div class="col-md-3">
                    <div id="labelcc">
                        <label for="form-control">CENTRO DE CUSTO </label>
                    </div>
                    <select class="form-control" name="idCentroCusto" id="idCentroCusto" onclick="orcar.lerlancamentos();"> 
                        {include file="orcamento/form_listacc.tpl"}                                    
                    </select>                      
                </div>
                <div class="col-md-3">
                    <label for="form-control">GRUPO DESPESA/RECEITA </label>
                    <select class="form-control" name="idGrupoDR" id="idGrupoDR" onchange="orcar.carregaritensdr();"> 
                        {html_options options=$lista_grupodr selected=null}                                    
                    </select>                      
                </div>
                <div class="col-md-3">
                    <label for="form-control">ITEM DESPESA/RECEITA </label>
                    <select class="form-control" name="idItemDR" id="idItemDR" onchange="orcar.carregarOItensDR();"> 
                        {include file="orcamento/form_listaitemdr.tpl"}                                    
                    </select>                      
                </div>
                <div class="col-md-1">
                    <label for="form-control">QUANTIDADE</label>
                    <input type="text" class="form-control valor" name="qtOrcado" id="qtOrcado"  value="1,00">      
                </div> 
                <div class="col-md-1">
                    <label for="form-control">VALOR </label>
                    <input type="text" class="form-control valor" name="vlOrcado" id="vlOrcado" value=""> 
                </div> 
                <div class="col-md-1">
                    <label for="form-control">DATA</label>
                    <input type="text" class="form-control datetime"  name="dtValorOrcado" id="dtValorOrcado"  >           
                </div> 
            </div>
            <br>                
            <div class="row small">
                <div class="col-md-3">
                    <label for="form-control">OBS</label>
                    <input type="text" class="form-control" name="dsValorOrcado" id="dsValorOrcado" value=""> 
                </div>                     
                <div class="col-md-3">
                    <label for="form-control">REPLICA ESTE ORÇAMENTO MENSALMENTE</label>
                    <input type="checkbox" disabled class="form-control" name="optReplica" id="optReplica" value=""> 
                </div>                     
                <div class="col-md-3">
                    <label for="form-control">CONSIDERAR PRONTO O ORÇAMENTO PARA ESTE C.C</label>
                    <input type="checkbox" disabled class="form-control" name="optEncerra" id="optEncerra" value="" onchange="orcar.finalizarDigitacaoCC();"> 
                </div>                     
                <br>
                <div class="col-md-1">
                  <div class="row small">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="row small">
                      <a class="btn btn-primary" id="btn-adicionaorcado" title="ADICIONAR" onclick="orcar.adicionarorcado();" >ADICIONAR</a> 
                  </div> 
                </div> 
            </div>
            <br>
            <br>
            <br>
            <div class="row small">                        
                <div class="col-md-12" id="mostrarorcados">
                    {include file="orcamento/orcar.html"}
                </div>
            </div>
        </div>
    </div>
</div>
                
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<script src="/files/js/util.js"></script>
<script src="/files/js/orcamento/orcar.js"></script>

{include file="comuns/footer.tpl"}