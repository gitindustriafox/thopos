    <div class="form-group">
        <input type="text" id="idOrcamentoColaboradorCC" class="form-control hidden" nome="idOrcamentoColaboradorCC" value="{$idOrcamentoColaboradorCC|default:''}" />                
        <input type="text" id="idColaborador" class="form-control hidden" nome="idColaborador" value="{$idColaborador|default:''}" />                
        <input type="text" id="idOrcamento" class="form-control hidden" nome="idOrcamento" value="{$idOrcamento|default:''}" />                
        <input type="text" id="stSituacao" class="form-control hidden" nome="stSituacao" value="{$stSituacao|default:''}" />                
        <div class="row small">
            </br>
            <div class="col-md-2">
                <label for="form-control">VALOR MINIMO PARA RECEITAS</label>
                <input type="text" class="form-control valor" {if $stSituacao|default:'' eq ''} disabled {/if} name="vlLimiteReceitas" id="vlLimiteReceitas" value="{$vlLimiteReceitas|default:'0'|number_format:"2":",":"."}"> 
            </div> 
            <div class="col-md-2">
                <label for="form-control">VALOR MAXIMO PARA DESPESAS</label>
                <input type="text" class="form-control valor" {if $stSituacao|default:'' eq ''} disabled {/if} name="vlLimiteDespesas" id="vlLimiteDespesas" value="{$vlLimiteDespesas|default:'0'|number_format:"2":",":"."}"> 
            </div> 
            <div class="col-md-2">
                <label for="form-control">RECEITAS ORCADAS</label>
                <input type="text" class="form-control valor" name="vlTotalReceitas" readonly id="vlTotalReceitas" value="R$ {$vlTotalReceitas|default:'0'|number_format:"2":",":"."}"> 
            </div> 
            <div class="col-md-2">
                <label for="form-control">DESPESAS ORCADAS</label>
                <input type="text" class="form-control valor" name="vlTotalDespesas" readonly id="vlTotalDespesas" value="R$ {$vlTotalDespesas|default:'0'|number_format:"2":",":"."}"> 
            </div> 
            <br>
            <div class="col-md-2">
                 <a class="btn btn-primary {if $stSituacao|default:'' eq ''} hidden {/if}"   data-dismiss="modal" onclick="orcamento.gravarvalorescc('{$idOrcamentoColaboradorCC|default:''}','{$stSituacao|default:''}','{$idOrcamento|default:''}','{$idColaborador|default:''}');"> GRAVAR VALORES</a>
            </div> 
            <div class="col-md-1">
                 <a class="btn btn-primary {if $stSituacao|default:'' eq ''} hidden {/if} "  data-dismiss="modal" onclick="orcamento.aprovar('{$idOrcamentoColaboradorCC|default:''}','{$stSituacao|default:''}','{$idOrcamento|default:''}','{$idColaborador|default:''}');">{if $stSituacao|default:'' eq 1} APROVAR {else} REPROVAR{/if}</a>
            </div> 
            <div class="col-md-1">
                 <a class="btn btn-primary" data-dismiss="modal" onclick="orcamento.voclear_log();">ABORTAR</a>
            </div>             
        </div>    
    </div>            
    <script src="/files/js/orcamento/orcamento.js"></script>