    {include file="orcamento/verorcamento/detalhes.tpl"}     
    <br>
        {assign var="total[][]" value=0} 
        {assign var="subtotal[][]" value=0} 
        {assign var="totallinha" value=0} 
        {assign var="pv" value=0} 
        {assign var="stGrupoDR" value='X'}
        <div class="panel-body">
            <div class="row small" id="linhas_orcamento">
                <table class="table table-striped" border="1">
                    <thead>
                        {if $stSituacao|default:'' eq ''}
                        <tr>
                            <td>
                                SALDO ANTERIOR
                            </td>
                            <td {if ($saldoinicial) < 0} style='color:red;' {else} style='color:blue;'  {/if}>
                                {$saldoinicial|number_format:2:",":"."|default:"0,00"}
                            </td>
                            {foreach from=$orcamentolista item="cabec"}  
                                <th>
                                </th>
                            {/foreach}                            
                        </tr>
                        {/if}
                        <tr>
                            <th>DESCRICAO</th>
                            {foreach from=$orcamentolista item="cabec"}  
                                <th>{$cabec.meses|default:''}</th>
                                {$total[$cabec.idmesano]['valor']=0}
                                {$subtotal[$cabec.idmesano]['valor']=0}
                            {/foreach}
                            <th>TOTAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach from=$listaGI item="gi"}  
                            {if $gi.stGrupoDR neq $stGrupoDR}
                                <tr>
                                    <th {if $stGrupoDR neq 'X'} style="background-color: #e6e0e0;" {/if}> 
                                        {if $stGrupoDR neq 'X'} 
                                            Sub Total:
                                            {$totallinha=0}
                                            {foreach from=$orcamentolista item="cabec"}  
                                                <th style="background-color: #e6e0e0;">
                                                    {$subtotal[$cabec.idmesano]['valor']|number_format:2:",":"."|default:"0,00"}
                                                    {$totallinha=$totallinha+$subtotal[$cabec.idmesano]['valor']}
                                                    {$subtotal[$cabec.idmesano]['valor'] = 0}
                                                </th>
                                            {/foreach}
                                            <th style="background-color: #e6e0e0;">
                                                {$totallinha|number_format:2:",":"."|default:"0,00"}
                                                {$totallinha=0}
                                            </th>
                                            <tr>
                                                <th>                                                    
                                                    {if $gi.stGrupoDR eq 'D'} DESPESAS {else} {if $gi.stGrupoDR eq 'R'} RECEITAS {else} {if $gi.stGrupoDR eq 'I'} INVESTIMENTOS {/if}{/if}{/if}
                                                </th>
                                                {foreach from=$orcamentolista item="cabec"}  
                                                    <th>
                                                    </th>
                                                {/foreach}
                                                <th>
                                                </th>
                                            </tr>
                                        {else}
                                            {if $gi.stGrupoDR eq 'D'} DESPESAS {else} {if $gi.stGrupoDR eq 'R'} RECEITAS {else} {if $gi.stGrupoDR eq 'I'} INVESTIMENTOS {/if}{/if}{/if}
                                            {foreach from=$orcamentolista item="cabec"}  
                                                <th>
                                                </th>
                                            {/foreach}
                                            <th>
                                            </th>
                                        {/if}
                                    </th>
                                </tr>
                                {$stGrupoDR = $gi.stGrupoDR}
                            {/if}
                            <tr>
                                <td>{$gi.dsGrupoDR|default:''}-{$gi.dsItemDR|default:''}</td>
                                {foreach from=$pedidoitens[$gi.idItemDR] item="valores"}  
                                    {foreach from=$orcamentolista item="cabec"}  
                                        {if $cabec.idmesano eq $valores.idmesano}
                                            <td>{* <a class="glyphicon glyphicon-info-sign" onclick='orcamento.selecionarvalores("{$valores.dsGrupoDR|default:''}","{$valores.idItemDR|default:''}","{$valores.idmesano|default:''}");'> </a>*} {$valores.valormes|number_format:2:",":"."|default:"0,00"}</td> 
                                            {$totallinha=$totallinha+$valores.valormes}
                                            {if $gi.stGrupoDR eq 'R'}
                                                {$total[$cabec.idmesano]['valor'] = ($total[$cabec.idmesano]['valor'] + $valores.valormes)}
                                                {$subtotal[$cabec.idmesano]['valor'] = ($subtotal[$cabec.idmesano]['valor'] + $valores.valormes)}
                                            {else}
                                                {$total[$cabec.idmesano]['valor'] = ($total[$cabec.idmesano]['valor'] - $valores.valormes)}
                                                {$subtotal[$cabec.idmesano]['valor'] = ($subtotal[$cabec.idmesano]['valor'] - $valores.valormes)}
                                            {/if}
                                        {/if}
                                    {/foreach}
                                {/foreach}                                            
                                <th>{$totallinha|number_format:2:",":"."|default:"0,00"}</th>
                                {$totallinha=0}                                
                            </tr>
                        {/foreach}  
                        <tr>
                            <th style="background-color: #e6e0e0;">
                                Sub Total:
                               {$totallinha=0}                                
                            </th>
                            {foreach from=$orcamentolista item="cabec"}  
                                <th style="background-color: #e6e0e0;">
                                    {$subtotal[$cabec.idmesano]['valor']|number_format:2:",":"."|default:"0,00"}
                                    {$totallinha=$totallinha+$subtotal[$cabec.idmesano]['valor']}
                                </th>
                            {/foreach}
                            <th style="background-color: #e6e0e0;">
                               {$totallinha|number_format:2:",":"."|default:"0,00"}  
                               {$totallinha=0}
                            </th>
                        </tr>
                        <tr>
                            <th>
                            </th>
                            {foreach from=$orcamentolista item="cabec"}  
                                <th>
                                </th>
                            {/foreach}
                            <th>
                            </th>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            {$totallinha=0}                                
                            <th style='background-color: #c5c2c2;'>
                                {if $stSituacao|default:'' eq ''} 
                                    TOTAL RECEITA LIQUIDA GERAL:
                                {else}
                                    TOTAL RECEITA LIQUIDA PARA O CENTRO DE CUSTO:
                                {/if}
                            </th>
                            {foreach from=$total item="item"}
                                <th 
                                    {if $stSituacao|default:'' eq '' && $pv eq 0}
                                        {if ($item.valor + $saldoinicial) < 0} 
                                            style='color:red; background-color: #c5c2c2;' 
                                        {else} 
                                            style='color:blue; background-color: #c5c2c2;'  
                                        {/if}
                                    {else} 
                                        {if $item.valor < 0} 
                                            style='color:red; background-color: #c5c2c2;' 
                                        {else} 
                                            style='color:blue; background-color: #c5c2c2;'  
                                        {/if}
                                    {/if}
                                    >    
                                    {if $stSituacao|default:'' eq '' && $pv eq 0}
                                        {($item.valor + $saldoinicial)|number_format:2:",":"."|default:"0,00"}
                                    {else}
                                        {$item.valor|number_format:2:",":"."|default:"0,00"}
                                    {/if}    
                                    {if $stSituacao|default:'' eq '' && $pv eq 0}
                                        {$pv = 1}
                                        {$totallinha=$totallinha+$item.valor+$saldoinicial}
                                    {else}
                                        {$totallinha=$totallinha+$item.valor}
                                    {/if}
                                </th>
                            {/foreach}
                            <th {if $totallinha.valor <0} style='color:red; background-color: #c5c2c2;' {else} style='color:blue; background-color: #c5c2c2;'  {/if} >
                               {$totallinha|number_format:2:",":"."|default:"0,00"}                            
                               {$totallinha=0}                               
                            </th>
                        </tr>
                        <tr>
                            <th style='background-color: #c5c2c2;'>
                                ACUMULADO:
                            </th>
                            {$totallinha=0}                                
                            {if $stSituacao|default:'' eq ''}
                               {$totallinha = $saldoinicial}
                            {/if}
                            {foreach from=$total item="item"}
                                <th {if ($totallinha+$item.valor) <0} style='color:red; background-color: #c5c2c2;' {else} style='color:blue; background-color: #c5c2c2;'  {/if}>
                                    {$totallinha=$totallinha+$item.valor} 
                                    {$totallinha|number_format:2:",":"."|default:"0,00"}                            
                                </th>
                            {/foreach}
                            <th style='background-color: #c5c2c2;'>
                            </th>                            
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>