        <div id="grafico_show" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog  modal-lg" role="document" style="width: 100%">
                <div class="row small">
                    <div class="col-md-3">
                        <div id="piechart_3d_PizzaTotalReceitas" style="margin-left: 5px; margin-right: 5px; margin-bottom: 5px;"></div>                        
                    </div> 
                    <div class="col-md-3">
                        <div id="piechart_3d_PizzaTotalDespesas" style="margin-left: 5px; margin-right: 5px; margin-bottom: 5px;"></div>                                                 
                    </div>
                    <div class="col-md-6">
                        <div id="piechart_3d_LinhaAcumulado" style="margin-left: 5px; margin-right: 5px; margin-bottom: 5px;"></div>                                                 
                    </div>
                </div>
                <div class="row small">
                    <div class="col-md-6">
                        <div id="piechart_3d_LinhaTotalReceitas" style="margin-left: 5px; margin-right: 5px; margin-bottom: 5px;"></div>                        
                    </div> 
                    <div class="col-md-6">
                        <div id="piechart_3d_LinhaTotalDespesas" style="margin-left: 5px; margin-right: 5px; margin-bottom: 5px;"></div>                                                 
                    </div>
                </div>
            </div>
        </div>