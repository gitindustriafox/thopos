{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>{if {$registro.idOrcamento}>0} ALTERAR ORCAMENTO:{$registro.dsOrcamento|default:''}{else} INCLUIR NOVO ORCAMENTO{/if}</h1></tt>
            </div>          
            <form name="frm-orcamento" 
                  action="/orcamento/gravar_orcamento" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario();">
                <br>
                <a href="/orcamento" class="btn btn-primary"> ABORTAR</a>
                <input class="btn btn-primary" type="submit" value="  GRAVAR" name="btnGravar"/>         
                <input class="btn btn-primary" type="button" onclick='orcamento.aprovarOrcamento();' {if $registro.idStatusOrcamento eq 2} value="  APROVAR" {else} value="  REPROVAR" {/if} name="btnAprovar"/>         
                <br>
                <br>
                <input type="text" class="form-control hidden" name="idOrcamentoColaborador" id="idOrcamentoColaborador" value="{$idOrcamentoColaborador}" READONLY>           
                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idOrcamento}>0}
                                <label for="form-control">ID</label>
                                <input type="text" class="form-control" name="idOrcamento" id="idOrcamento" value="{$registro.idOrcamento}" READONLY>           
                        {else}
                                 <label for="form-control">ID</label>
                                 <input type="text" class="form-control" name="idOrcamento" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">DESCRICAO DO ORCAMENTO</label>
                        <input type="text" class="form-control" name="dsOrcamento" id="dsOrcamento" value="{$registro.dsOrcamento|default:''}" >           
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">INSERIDO EM</label>
                        <input type="text" class="form-control datetime" readonly name="dtCriacao" id="dtCriacao" value="{$registro.dtCriacao|date_format:'%d/%m/%Y'|default:Date("d/m/Y")}" >           
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">DATA INICIAL</label>
                        <input type="text" class="form-control datetime" name="dtInicioVigencia" id="dtInicioVigencia" value="{$registro.dtInicioVigencia|date_format:'%d/%m/%Y'|default:Date("d/m/Y")}" >           
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">DATA FINAL</label>
                        <input type="text" class="form-control datetime" name="dtFimVigencia" id="dtFimVigencia" value="{$registro.dtFimVigencia|date_format:'%d/%m/%Y'|default:Date("d/m/Y")}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">SALDO ANTERIOR</label>
                        <input type="text" class="form-control" {if $registro.vlSaldoInicial < 0} style="color:red;" {/if} name="vlSaldoInicial" id="vlSaldoInicial" value="R$ {$registro.vlSaldoInicial|default:'0'|number_format:"2":",":"."}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">VALOR DO ORCAMENTO ATUAL</label>
                        <input type="text" class="form-control" {if $totalorcamento < 0} style="color:red;" {/if} name="vlOrcadoTotal" id="vlOrcadoTotal" value="R$ {$totalorcamento|default:'0'|number_format:"2":",":"."}" disabled >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">VALOR FINAL DO ORCAMENTO</label>
                        <input type="text" class="form-control" {if $registro.vlSaldoFinal < 0} style="color:red;" {/if} name="vlSaldoFinal" id="vlSaldoFinal" value="R$ {$registro.vlSaldoFinal|default:'0'|number_format:"2":",":"."}" disabled >           
                    </div> 
                </div> 
                <br>
                <div id="modal_verorcamento">
                    {include file="orcamento/verorcamento/modal.tpl"}                            
                </div> 
                <div id="modal_grafico">
                    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>                
                    {include file="orcamento/graficocc/modalcc.tpl"}                            
                </div>               
                
                <div class="row small">
                    <div class="col-md-10">
                        <label for="form-control">OBSERVACAO</label>
                        <input type="text" class="form-control" name="dsTermoAbertura" id="dsTermoAbertura" value="{$registro.dsTermoAbertura|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">STATUS</label>
                        <input type="text" class="form-control" name="dsStatusOrcamento" id="dsStatusOrcamento" readonly value="{$registro.dsStatusOrcamento|default:''}" >           
                    </div> 
                </div> 
            </form>
            <br>
            <div class="panel-item panel panel-default"> 
                <div class="panel-heading mostra">
                    <h3> <strong>COLABORADORES</strong> <h3>
                </div> 
                <div class="panel-body esconde" id="painel_colaboradores">
                    <br>
                    <div class="row small" >
                        <h4> &nbsp; COLABORADORES PARA ESTE ORCAMENTO</h4>
                        <br>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="colaborador">COLABORADOR </label>
                                <select class="form-control" name="idColaborador" id="idColaborador"  onchange="orcamento.lerColaborador();"> 
                                    {include file="orcamento/form_listacolaborador.tpl"}                                    
                                </select>                      
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label for="form-control">CARGO</label>
                            <input type="text" class="form-control" name="dsCargo" id="dsCargo" disabled='disabled' value="">       
                        </div> 
                        <div class="col-md-2">
                            <label for="form-control">SETOR</label>
                            <input type="text" class="form-control obg valor" name="dsSetor" id="dsSetor" readonly value="">      
                        </div> 
                        <div class="col-md-3">
                            <label for="form-control">E-MAIL</label>
                            <input type="text" class="form-control obg valor" name="dsEmail" readonly  id="dsEmail" value=""> 
                        </div> 
                        <br>
                        <div class="col-md-1">
                          <div class="row small">
                              <a class="btn btn-primary" id="btn-adicionacolaborador" title="ADICIONAR" onclick="orcamento.adicionarcolaborador();" {if $registro.idOrcamento eq '' || $registro.idStatusOrcamento > 2} disabled {/if}  >ADICIONAR COLABORADOR</a> 
                          </div> 
                        </div> 
                    </div>
                    <div {if $registro.idStatusOrcamento > 2} readonly {/if}  id="mostrarcolaboradores">
                         {include file="orcamento/orcamentocolaborador.html"}
                    </div>
                </div>
            </div>    
            <br>
            <div class="panel-item panel panel-default"> 
                <div class="panel-heading mostra">
                    <h3> <strong>CENTRO DE CUSTO</strong> <h3>
                </div> 
                <div class="panel-body esconde" id="painel_centrocusto">
                    <br>
                    <div class="row small" >
                        <h4> &nbsp; CENTRO DE CUSTO PARA ESTE ORCAMENTO</h4>
                        <br>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="centrocusto">CENTRO DE CUSTO </label>
                                <select class="form-control" name="idCentroCusto" id="idCentroCusto"> 
                                    {include file="orcamento/form_listacc.tpl"}                                    
                                </select>                      
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label for="form-control">VALOR MINIMO PARA RECEITAS</label>
                            <input type="text" class="form-control valor" name="vlLimiteReceitas" id="vlLimiteReceitas" value=""> 
                        </div> 
                        <div class="col-md-3">
                            <label for="form-control">VALOR MAXIMO PARA DESPESAS</label>
                            <input type="text" class="form-control valor" name="vlLimiteDespesas" id="vlLimiteDespesas" value=""> 
                        </div> 
                                
                        <br>
                        <div class="col-md-1">
                          <div class="row small">
                              <a class="btn btn-primary" id="btn-adicionacentrocusto" title="ADICIONAR CENTRO CUSTO" onclick="orcamento.adicionarcentrocusto();" {if $registro.idOrcamento eq '' || $registro.idStatusOrcamento > 2} disabled {/if}  >ADICIONAR CENTRO CUSTO</a> 
                          </div> 
                        </div> 
                    </div>
                    <div {if $registro.idStatusOrcamento > 2} readonly {/if}  id="mostrarcentrocusto" >
                        {include file="orcamento/orcamentocentrocusto.html"}
                    </div>
                </div>
            </div>   
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
{*<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
*}<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/orcamento/orcamento.js"></script>



{include file="comuns/footer.tpl"}

