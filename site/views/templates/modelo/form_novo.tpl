{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>{if {$registro.idModelo}>0} ALTERAR DESCRICAO DO MODELO: {$registro.dsModelo|default:''}{else} INSERIR NOVO MODELO{/if}</h1></tt>
            </div>          
            <a href="/modelo" class="btn btn-primary">Voltar</a><br>

            <form name="frm-modelo" 
                  action="/modelo/gravar_modelo" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario()">
                <br>
                
                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idModelo}>0}
                                <label for="form-control">ID</label>
                                <input type="text" class="form-control" name="idModelo" id="idModelo" value="{$registro.idModelo}" READONLY>           
                        {else}
                                 <label for="form-control">ID</label>
                                 <input type="text" class="form-control" name="idModelo" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-3">
                        <label for="form-control">DESCRICAO</label>
                        <input type="text" class="form-control" name="dsModelo" id="dsModelo" value="{$registro.dsModelo|default:''}" >           
                    </div> 
                </div>         
                
                <br>            
                    <input type="submit" value="GRAVAR" name="btnGravar" class="btn btn-primary" />         
                <br>
                <br>
            </form>
            
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/modelo/modelo_novo.js"></script>



{include file="comuns/footer.tpl"}

