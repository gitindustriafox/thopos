{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>{if {$registro.idPeriodo}>0} ALTERAR PERÍODO:{$registro.dsPeriodo|default:''}{else} INCLUIR NOVO PERÍODO{/if}</h1></tt>
            </div>          
            <form name="frm-periodo" 
                  action="/periodoContabil/gravar_periodo" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return periodoContabil.validaFormulario();">
                <br>
                <a href="/periodoContabil" class="btn btn-primary"> ABORTAR</a>
                <input class="btn btn-primary" type="submit" value="  GRAVAR" name="btnGravar"/>         
                <br>
                <br>
                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idPeriodo}>0}
                                <label for="form-control">ID</label>
                                <input type="text" class="form-control" name="idPeriodo" id="idPeriodo" value="{$registro.idPeriodo}" READONLY>           
                        {else}
                                 <label for="form-control">ID</label>
                                 <input type="text" class="form-control" name="idPeriodo" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-5">
                        <label for="form-control">DESCRICAO DO PERIODO</label>
                        <input type="text" class="form-control" name="dsPeriodo" id="dsPeriodo" value="{$registro.dsPeriodo|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">DATA INCLUSÃO</label>
                        <input type="text" class="form-control datetime" readonly name="dtCriacao" id="dtCriacao" value="{$registro.dtCriacao|date_format:'%d/%m/%Y'|default:Date("d/m/Y")}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">DATA INICIAL DO PERÍODO</label>
                        <input type="text" class="form-control datetime" {if $registro.idPeriodo>0} readonly {/if} name="dtInicial" id="dtInicial" value="{$registro.dtInicial|date_format:'%d/%m/%Y'|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">DATA FINAL DO PERÍODO</label>
                        <input type="text" class="form-control datetime" {if $registro.idPeriodo>0} readonly {/if} name="dtFinal" id="dtFinal" value="{$registro.dtFinal|date_format:'%d/%m/%Y'|default:''}" >           
                    </div> 
                </div> 
            </form>
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
{*<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
*}<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/contabilidade/periodoContabil.js"></script>



{include file="comuns/footer.tpl"}

