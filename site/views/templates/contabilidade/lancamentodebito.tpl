        <br>
        <table class="table" border="0">
            <thead>
                <tr>
                    <th>CONTA REDUZIDA</th>
                    <th>HISTÓRICO</th>
                    <th>VALOR</th>
                    <th>ACAO</th>
                </tr>
            </thead>
            <tbody>
                {foreach from=$lancamentosdebito|default:null item="linha"}  
                <tr>
                    <td>{$linha.idContaReduzida|default:''}-{$linha.dsConta|default:''}</td>
                    <td>{$linha.dsHistorico|default:''}</td>
                    <td>R$ {$linha.vlLancamento|default:'0'|number_format:"2":",":"."}</td> 
                    <td>
                        <a class="glyphicon glyphicon-trash" onclick="LancamentoContabil.delLancamentoDebito('{$linha.idLancamentoDebito}');">  Excluir</a>
                    </td>
                </tr>
                {/foreach}        
            </tbody>
        </table>