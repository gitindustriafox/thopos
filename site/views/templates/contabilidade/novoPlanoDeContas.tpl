{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>PLANO DE CONTAS DO PERÍODO:{$registro.dsPeriodo|default:''}</h1></tt>
            </div>          
            <a href="/planodecontas" class="btn btn-primary"> ABORTAR</a>
            {if $temlista|default:0 eq 0}
                {if $temAnterior|default:0 eq 0}
                    <a onclick="planodecontas.showipc();" class="btn btn-primary"> IMPORTAR ARQUIVO EXTERNO</a>                    
                {/if}                    
                <a href="/planodecontas/importarExAnterior/idPeriodo/{$registro.idPeriodo|default:''}" class="btn btn-primary"> TRAZER EXERCICIO ANTERIOR</a>
            {else}
                {if $temAnterior|default:0 eq 1}
                    <a href="/planodecontas/importarExAnterior/idPeriodo/{$registro.idPeriodo|default:''}" class="btn btn-primary"> TRAZER SALDOS DO EXERCICIO ANTERIOR</a>
                {/if}
            {/if}
            <br>
            <br>
            <form name="frm-planodecontas" 
                  action="/planodecontas/gravar_planodecontas" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return planodecontas.validaFormulario();">
                <br>
                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idPeriodo}>0}
                                <label for="form-control">ID</label>
                                <input type="text" class="form-control" name="idPeriodo" id="idPeriodo" value="{$registro.idPeriodo}" READONLY>           
                        {else}
                                 <label for="form-control">ID</label>
                                 <input type="text" class="form-control" name="idPeriodo" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-5">
                        <label for="form-control">DESCRICAO DO PERIODO</label>
                        <input type="text" class="form-control" readonly name="dsPeriodo" id="dsPeriodo" value="{$registro.dsPeriodo|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">DATA INCLUSÃO</label>
                        <input type="text" class="form-control datetime" readonly name="dtCriacao" id="dtCriacao" value="{$registro.dtCriacao|date_format:'%d/%m/%Y'|default:Date("d/m/Y")}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">DATA INICIAL DO PERÍODO</label>
                        <input type="text" class="form-control datetime" {if $registro.idPeriodo>0} readonly {/if} name="dtInicial" id="dtInicial" value="{$registro.dtInicial|date_format:'%d/%m/%Y'|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">DATA FINAL DO PERÍODO</label>
                        <input type="text" class="form-control datetime" {if $registro.idPeriodo>0} readonly {/if} name="dtFinal" id="dtFinal" value="{$registro.dtFinal|date_format:'%d/%m/%Y'|default:''}" >           
                    </div> 
                </div> 
                <br>
                <br>
                <div class='row small'>
                    <div class="container-fluid">
                            <h4> PLANO DE CONTAS </h4>
                    </div>
                </div>
                <br>
                <div class='row small'>
                        <input type="text" class="form-control hidden" name="idConta" id="idConta" value="">                               
                        <div class="col-md-1">
                            <label for="form-control">REDUZIDO</label>
                            <input type="text" class="form-control" name="idContaReduzida" id="idContaReduzida"  value="" >           
                        </div> 
                        <div class="col-md-1" style="width:5%;">
                            <label for="form-control">Niv 1</label>
                             <input type="text" class="form-control" name="idCE1" id="idCE1" value="0" >           
                        </div>
                        <div class="col-md-1" style="width:5%;" >
                            <label for="form-control">Niv 2</label>
                            <input type="text" class="form-control" name="idCE2" id="idCE2" value="0" >           
                        </div>
                        <div class="col-md-1" style="width:5%;" >
                            <label for="form-control">Niv 3</label>
                            <input type="text" class="form-control" name="idCE3" id="idCE3" value="0" >           
                        </div>
                        <div class="col-md-1"  style="width:5%;">
                            <label for="form-control">Niv 4</label>
                            <input type="text" class="form-control" name="idCE4" id="idCE4" value="0" >           
                        </div>
                        <div class="col-md-1"  style="width:5%;">
                            <label for="form-control">Niv 5</label>
                            <input type="text" class="form-control" name="idCE5" id="idCE5" value="0" >           
                        </div>
                        <div class="col-md-4">
                            <label for="form-control">DESCRICAO DA CONTA</label>
                            <input type="text" class="form-control" name="dsConta" id="dsConta" value="" >           
                        </div> 
                        <div class="col-md-1">
                            <label for="form-control">TIPO (A / S)</label>
                            <input type="text" class="form-control" maxlength="1" name="stTipoConta" id="stTipoConta" value="" >           
                        </div> 
                        <div class="col-md-2">
                            <label for="form-control">SALDO INICIAL</label>
                            <input type="text" class="form-control" name="vlSaldoInicial" id="vlSaldoInicial" onfocus="planodecontas.lerConta();" value="" >           
                        </div> 
                        <br>
                        <div class="col-md-1">
                            <input class="btn btn-primary" type="submit" value="GRAVAR" id="btnGravar" name="btnGravar"/>         
                        </div>            
                </div>
            </form>
            <div id="modal_importaplano">
                {include file="contabilidade/modalPlanoDeContas/modal.tpl"}                            
            </div>                 
                    
            <div class='row small form-group'>
                <div class="container-fluid">
                   {include file="contabilidade/listacontas.html"}                    
                </div>
            </div>
        </div>
    </div>
</div>
                    

<!-- JavaScript -->
{*<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
*}<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
{*<script src="/files/js/contabilidade/planodecontas.js"></script>*}



{include file="comuns/footer.tpl"}

