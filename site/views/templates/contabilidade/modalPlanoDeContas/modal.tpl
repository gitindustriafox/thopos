<div id="importarplanodecontas_show" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-lg" role="document" style="width: 50%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">IMPORTAR ARQUIVO COM PLANO DE CONTAS</h4>
            </div>
            <form name="frm-importar_planodecontas" 
                    action="/planodecontas/importarplanodecontas" 
                    method="POST" 
                    enctype="multipart/form-data">
                    <div class="modal-body"></div>
            </form>    
        </div>
    </div>
</div>
<script src="/files/js/contabilidade/planodecontas.js"></script>                   