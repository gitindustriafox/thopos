    <div class="form-group">
        <input type="text" class="form-control hidden" name="idPeriodo" value="{$idPeriodo}">                                       
        <div class="row small">
            <div class="col-md-2" id="botao_enviar">
                 <input class="btn btn-primary" type="submit" value="PROCESSAR" />
            </div> 
            <div class="col-md-2">
                 <input type="file" name="arquivo" />
            </div> 
        </div> 
        <br>
        <div class="row small">
            &nbsp;&nbsp;&nbsp; Somente importar arquivo formato CSV <br>
            &nbsp;&nbsp;&nbsp; Colunas separadas por virgula e os campos alfanuméricos devem estar dentro por aspas duplas <br>
            &nbsp;&nbsp;&nbsp; As colunas devem estar nesta ordem: <br>
            &nbsp;&nbsp;&nbsp; 1 - Codigo resumido da conta,(Numerico)<br>
            &nbsp;&nbsp;&nbsp; 2 - Campo 1 do nivel estruturado, (Numerico)<br>
            &nbsp;&nbsp;&nbsp; 3 - Campo 2 do nivel estruturado, (Numerico)<br>
            &nbsp;&nbsp;&nbsp; 4 - Campo 3 do nivel estruturado, (Numerico)<br>
            &nbsp;&nbsp;&nbsp; 5 - Campo 4 do nivel estruturado, (Numerico)<br>
            &nbsp;&nbsp;&nbsp; 6 - Campo 5 do nivel estruturado, (Numerico)<br>
            &nbsp;&nbsp;&nbsp; 7 - S para sintético ou A para analitico (Alfaumerico)<br>
            &nbsp;&nbsp;&nbsp; 8 - Nome da Conta (Alfaumerico)<br>
            &nbsp;&nbsp;&nbsp; 9 - Valor do saldo inicial (Formato americano, ponto na decimal e sem separação na milhar)<br>
        </div>
    </div>