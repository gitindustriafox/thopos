{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>LANCAMENTO CONTABIL</h1></tt>
            </div>          
            <a href="/lancamentosContabeis" class="btn btn-primary"> ABORTAR</a>
            <br>
            <br>
            <form name="frm-lancamentosContabeis" 
                  action="/lancamentosContabeis/gravar_LancamentoContabil" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return LancamentoContabil.validaFormulario();">
                <br>
                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idLancamento}>0}
                                <label for="form-control">ID</label>
                                <input type="text" class="form-control" name="idLancamento" id="idLancamento" value="{$registro.idLancamento}" READONLY>           
                        {else}
                                 <label for="form-control">ID</label>
                                 <input type="text" class="form-control" name="idLancamento" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">DATA</label>
                        <input type="text" class="form-control datetime" name="dtLancamento" id="dtLancamento" value="{$registro.dtLancamento|date_format:'%d/%m/%Y'|default:Date("d/m/Y")}" >           
                    </div> 
                    <div class="col-md-5">
                        <label for="form-control">HISTORICO</label>
                        <input type="text" class="form-control" name="dsHistorico" id="dsHistorico" value="{$registro.dsHistorico|default:''}" >           
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">DOCUMENTO</label>
                        <input type="text" class="form-control" name="nrDocumento" id="nrDocumento" value="{$registro.nrDocumento|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">VALOR </label>
                        <input type="text" class="form-control datetime" name="vlLancamento" id="vlLancamento" value="{$registro.vlLancamento|default:'0'|number_format:'2':',':'.'}" >           
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">CONFERIDO</label>
                        <input type="checkbox" class="form-control" name="stConferido" id="stConferido" {$registro.stConferido|default:''}  value="">       
                    </div> 
                    <br>
                    <div class="col-md-1">
                        <input class="btn btn-primary" type="submit" value="GRAVAR" id="btnGravar" name="btnGravar"/>         
                    </div>  
                </div> 
            </form>
        </div>
        <br>
        <div  class="col-md-6" style="width: 50%;">
            <h5><strong> LANCAMENTOS DÉBITO </strong></h5>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row small">
                        <div class="col-md-5">
                            <label for="form-control">CONTA</label>
                            <input data-label="contadebito" type="text" {if $registro.idLancamento eq 0} disabled {/if}  data-tipo="contadebito" class="form-control complete_contaDebito" name="dsContaDebito"  id="dsContaDebito" value=""/> 
                            <input type="hidden" id="idContaDebito" value="" name="idContaDebito"/>                            
                        </div> 
                        <div class="col-md-3">
                            <label for="form-control">HISTORICO</label>
                            <input type="text" class="form-control" {if $registro.idLancamento eq 0} disabled {/if}  name="dsHistoricoDebito" id="dsHistoricoDebito" value="" >           
                        </div> 
                        <div class="col-md-2">
                            <label for="form-control">VALOR </label>
                            <input type="text" class="form-control datetime" {if $registro.idLancamento eq 0} disabled {/if}  name="vlLancamentoDebito" id="vlLancamentoDebito" value="" >           
                        </div>
                        <br>
                        <div class="col-md-1">
                            <input class="btn btn-primary" type="button" value="GRAVAR" {if $registro.idLancamento eq 0} disabled {/if} onclick="LancamentoContabil.gravarlancamentodebito();" id="btnGravarDebito" name="btnGravarDebito"/>         
                        </div>                          
                    </div>
                    <div class="row small">
                         {include file='contabilidade/lancamentodebito.tpl'}
                    </div>
                    <div {if {$totalcredito|default:'0'} neq {$totaldebito|default:'0'} || {$totaldebito|default:'0'} neq {$registro.vlLancamento|default:'0'}} style="color:red;" {/if}
                        <strong> 
                            VALOR TOTAL DOS DÉBITOS  -  R$ {$totaldebito|default:'0'|number_format:"2":",":"."} 
                        </strong>
                    </div>                        
                </div>
            </div>
        </div>
        <div class="col-md-6" style="width: 50%;">
            <h5><strong>LANCAMENTOS CRÉDITO</strong></h5>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row small">
                        <div class="col-md-5">
                            <label for="form-control">CONTA</label>
                            <input data-label="contacredito" type="text" {if $registro.idLancamento eq 0} disabled {/if}  data-tipo="contacredito" class="form-control complete_contaCredito" name="dsContaCredito"  id="dsContaCredito" value=""/> 
                            <input type="hidden" id="idContaCredito" value="" name="idContaCredito"/>                            
                        </div> 
                        <div class="col-md-3">
                            <label for="form-control">HISTORICO</label>
                            <input type="text" class="form-control" {if $registro.idLancamento eq 0} disabled {/if}  name="dsHistoricoCredito" id="dsHistoricoCredito" value="" >           
                        </div> 
                        <div class="col-md-2">
                            <label for="form-control">VALOR </label>
                            <input type="text" class="form-control datetime" {if $registro.idLancamento eq 0} disabled {/if}  name="vlLancamentoCredito" id="vlLancamentoCredito" value="" >           
                        </div>
                        <br>
                        <div class="col-md-1">
                            <input class="btn btn-primary" type="button" value="GRAVAR" {if $registro.idLancamento eq 0} disabled {/if} onclick="LancamentoContabil.gravarlancamentocredito();" id="btnGravarCredito" name="btnGravarCredito"/>         
                        </div>                          
                    </div>
                    <div class="row small">
                         {include file='contabilidade/lancamentocredito.tpl'}
                    </div>  
                    <div {if {$totalcredito|default:'0'} neq {$totaldebito|default:'0'} || {$totalcredito|default:'0'} neq  {$registro.vlLancamento|default:'0'}} style="color:red;" {/if}
                        <strong> 
                            VALOR TOTAL DOS CRÉDITOS  -  R$ {$totalcredito|default:'0'|number_format:"2":",":"."} 
                        </strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
                    

<!-- JavaScript -->
{*<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
*}<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/contabilidade/lancamentosContabeis.js" type="text/javascript"></script>
{include file="comuns/footer.tpl"}

