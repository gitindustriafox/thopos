{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>{if {$registro.idOSGrupo}>0} ALTERAR DADOS DO GRUPO DE MANUTENCAO:  {$registro.dsOSGrupo|default:''} {else} NOVO GRUPO DE MANUTENCAO>{/if}</h1></tt>
            </div>          
            <a href="/osgrupo" class="btn btn-primary"> ABORTAR</a><br>

            <form name="frm-osgrupo" 
                  action="/osgrupo/gravar_osgrupo" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario()">
                <br>

                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idOSGrupo}>0}
                                <label for="form-control">ID</label>
                                <input type="text" class="form-control" name="idOSGrupo" id="idOSGrupo" value="{$registro.idOSGrupo}" READONLY>           
                        {else}
                                 <label for="form-control">ID</label>
                                 <input type="text" class="form-control" name="idOSGrupo" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-3">
                        <label for="form-control">DESCRICAO</label>
                        <input type="text" class="form-control" name="dsOSGrupo" id="dsOSGrupo" value="{$registro.dsOSGrupo|default:''}" >           
                    </div> 
                </div>                                 
                <br>            
                    <input type="submit" value="GRAVAR" name="btnGravar" class="btn btn-primary" />         
                <br>
                <br>
            </form>
            
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
{*<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
*}<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/osgrupo/osgrupo_novo.js"></script>



{include file="comuns/footer.tpl"}

