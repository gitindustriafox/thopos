{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <form name="frm-insumo" 
                  action="/insumo/gravar_insumo" 
                  method="POST" 
                  enctype="multipart/form-data">
{*                  onsubmit="return validaFormulario();">*}
                <div class="alert alert-info" >
                    <tt><h1>{if {$registro.idInsumo}>0} ALTERAR PRODUTO: {$registro.dsInsumo|default:''}{else} INCLUIR NOVO PRODUTO {/if}</h1></tt>
                </div>          
                <div class="row small">
                      <div class="col-md-1">
                           <a href="/insumo" class="btn btn-primary"> ABORTAR</a><br>
                      </div> 
                      <div class="col-md-1">                          
                           <input class="btn btn-primary" {if $ajustar neq 'ikl'} disabled {/if}  type="submit" value="  GRAVAR" name="btnGravar"/>         
                      </div> 
                </div>

{*            <form name="frm-insumo" 
                  action="/insumo/gravar_insumo" 
                  method="POST" 
                  enctype="multipart/form-data">
*}{*                  onsubmit="return validaFormulario();">*}
                <br>
                <div class="row small">
                    <input type="text" class="form-control hidden" name="ajustar" id="ajustar" value="{$ajustar|default:''}" >           
                    <div class="col-md-1">
                        {if {$registro.idInsumo}>0}
                                <label for="form-control">ID</label>
                                <input type="text" class="form-control" name="idInsumo" id="idInsumo" value="{$registro.idInsumo}" READONLY>           
                        {else}
                                 <label for="form-control">ID</label>
                                 <input type="text" class="form-control" name="idInsumo" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-4">
                        <label for="form-control">NOME DO PRODUTO</label>
                        <input type="text" class="form-control" name="dsInsumo" id="dsInsumo" value="{$registro.dsInsumo|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">CODIGO DO PRODUTO</label>
                        <input type="text" class="form-control" name="cdInsumo" id="cdInsumo" value="{$registro.cdInsumo|default:''}" >           
                    </div> 
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="unidade">UNIDADE</label>
                            <select class="form-control" name="idUnidade" id="idUnidade">
                                {html_options options=$lista_unidade selected=$registro.idUnidade}
                            </select>                      
                        </div>
                    </div>                     
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="grupoinsumo">GRUPO DO PRODUTO</label>
                            <select class="form-control" name="idGrupo" id="idGrupo">
                                {html_options options=$lista_grupo selected=$registro.idGrupo}
                            </select>                      
                        </div>
                    </div>                     
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="marca">MARCA</label>
                            <select class="form-control" name="idMarca" id="idMarca">
                                {html_options options=$lista_marca selected=$registro.idMarca}
                            </select>                      
                        </div>
                    </div> 
                </div>    
                <br>
                <div class="row small">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="modelo">MODELO</label>
                            <select class="form-control" name="idModelo" id="idModelo">
                                {html_options options=$lista_modelo selected=$registro.idModelo}
                            </select>                      
                        </div>
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">NUMERO DE SERIE</label>
                        <input type="text" class="form-control" name="nrSerie" id="nrSerie" value="{$registro.nrSerie|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">CODIGO DO FABRICANTE</label>
                        <input type="text" class="form-control" name="dsCodigoDoFabricante" id="dsCodigoDoFabricante" value="{$registro.dsCodigoDoFabricante|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">VALOR UNITARIO</label>
                        <input type="text" class="form-control" name="vlUnitario" id="vlUnitario" value="{$registro.vlUnitario|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="Parceiro">PARCEIRO PADRAO </label>
                            <select class="form-control" name="idParceiro"  id="idParceiro">
                                {html_options options=$lista_Parceiro selected={$registro.idParceiro|default:''}}
                            </select>                      
                        </div>
                    </div>                    
                    <div class="col-md-2">
                        <label for="form-control">VALIDADE</label>
                        <input type="text" class="form-control" name="dtValidade" id="dtValidade" value="{$registro.dtValidade|date_format:'d/m/Y'|default:''}" >           
                    </div> 
                </div> 
                <br>
                <div class="row small">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="modelo">NCM</label>
                            <input data-label="ncm" type="text" data-tipo="ncm" class="form-control complete_ncm" name="dsNCM"  id="dsNCM" value="{$registro.cdNCM|default:''} - {$registro.dsNCM|default:''}"/> 
                            <input type="hidden" id="idNCM" value="{$registro.idNCM|default:''}" name="idNCM"/>                            
                        </div>
                    </div> 
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="controlado">CONTROLADO</label>
                            <select class="form-control" name="stControlado" id="stControlado">
                                {html_options options=$lista_controlado selected=$registro.stControlado}
                            </select>                      
                        </div>
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">ORGÃO CONTROLADOR</label>
                        <input type="text" class="form-control" name="dsOrgaoControlador" id="dsOrgaoControlador" value="{$registro.dsOrgaoControlador|default:''}" >           
                    </div>                             
                </div>
            </form>
            <div class="row small" >
                <h3> &nbsp; LOCAIS DE ESTOQUE / PONTO DE PEDIDO:</h3>
                <br>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="localestoque">LOCAL </label>
                        <select class="form-control" name="idLocalEstoque" id="idLocalEstoque">
                            {html_options options=$lista_localestoque selected=null}
                        </select>                      
                    </div>
                </div>
                <div class="col-md-1">
                    <label for="form-control">ESTOQUE</label>
                    <input type="text" class="form-control" name="qtEstoque" id="qtEstoque" readonly value="" >           
                </div> 
                <div class="col-md-1">
                    <label for="form-control">EST MINIMO</label>
                    <input type="text" class="form-control" name="qtEstoqueMinimo" id="qtEstoqueMinimo" value="" >           
                </div> 
                <div class="col-md-1">
                    <label for="form-control">EST MAXIMO</label>
                    <input type="text" class="form-control" name="qtLoteReposicao" id="qtLoteReposicao" value="" >           
                </div> 
                <br>
                <div class="col-md-1">
                      <a class="btn btn-primary" id="btn-adicionaitem" title="Clique aqui para adicionar este insumo/produto na lista abaixo" onclick="insumo.gravarlocalestoque();">ADICIONAR</a> 
                </div> 
            </div> 
            <br>
            {include file="insumo/localestoque.html"}
            <div class="row small" >
                <h3> &nbsp; ACERTO DO ESTOQUE:</h3>
                <br>
                <div class="col-md-1">
                    <div class="form-group">
                         <input type="radio" onclick='insumo.escolherES();' id="stE" name="stES" value="0" checked> Entrada<br>
                         <input type="radio" onclick='insumo.escolherES();' id="stS" name="stES" value="1"> Saida<br>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <div id="labeltm">
                            <label for='tipomovimento'>TIPO DE MOVIMENTO - ENTRADA</label>
                        </div>
                        <select class="form-control" name="idTipoMovimento" onfocus='insumo.foco();' id="idTipoMovimento">
                            {include file="insumo/listatm.tpl"}
                        </select>                      
                    </div>
                </div>                                     
                <div class="col-md-3">
                    <label for="form-control">MOTIVO</label>
                    <input type="text" class="form-control" name="dsMotivo" id="dsMotivo"onfocus='insumo.foco();' value="AJUSTE DE ESTOQUE" >           
                </div> 
                <div class="col-md-1">
                    <label for="form-control">QUANTIDADE</label>
                    <input type="text" class="form-control" name="qtAcerto" id="qtAcerto"onfocus='insumo.foco();' value="" >           
                </div> 
                <div class="col-md-1">
                    <label for="form-control">VALOR</label>
                    <input type="text" class="form-control" name="vlAcerto" id="vlAcerto" onfocus='insumo.foco();' value="" >           
                </div> 
                <div class="col-md-2">
                    <label for="form-control">LOCAL DE ESTOQUE</label>
                    <input type="text" class="form-control" name="dsLocalEstoqueAcerto" id="dsLocalEstoqueAcerto" DISABLED value="" >           
                    <input type="text" class="form-control hidden" name="idLocalEstoqueAcerto" id="idLocalEstoqueAcerto" value="" >           
                </div> 
                <br>
                <div class="col-md-1">
                    <input type='button' class="btn btn-primary" disabled='disabled' id="btn-acertoestoque" title="Clique aqui para acertar o estoque" onclick="insumo.gravaracertoestoque();" value="ACERTAR"> 
                </div> 
            </div>
                    
        <!--Altere daqui pra cima-->
    </div>
</div>

{*<!-- JavaScript -->
<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>*}
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/insumo/insumo.js"></script>



{include file="comuns/footer.tpl"}

