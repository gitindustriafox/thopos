{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>{if {$registro.idPerfil}>0} ALTERAR PERFIL: {$registro.dsPerfil|default:''}{else} INCLUIR NOVO PERFIL{/if}</h1></tt>
            </div>          
            <a href="/perfil" class="btn btn-primary"> ABORTAR</a><br>

            <form name="frm-perfil" 
                  action="/perfil/gravar_perfil" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario()">
                <br>
                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idPerfil}>0}
                                <label for="form-control">CODIGO</label>
                                <input type="text" class="form-control" name="idPerfil" id="idPerfil" value="{$registro.idPerfil}" READONLY>           
                        {else}
                                 <label for="form-control">CODIGO</label>
                                 <input type="text" class="form-control" name="idPerfil" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-3">
                        <label for="form-control">DESCRICAO</label>
                        <input type="text" class="form-control" name="dsPerfil" id="dsPerfil" value="{$registro.dsPerfil|default:''}" >           
                    </div> 
                    
                    {if {$registro.idPerfil}>0}
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="perfil">COPIAR PERFIL DE</label>
                                <select class="form-control" name="idPerfilCopiar" id="idPerfilCopiar">
                                    {html_options options=$lista_perfis}
                                </select>                      
                            </div>
                        </div>   
                    {/if}        
                    <br>
                    {if {$registro.idPerfil}>0}
                    <input type="button" class="btn btn-primary"  onclick="perfil.copiarperfil();" value="COPIAR" name="brnCopiaPerfil" id="brnCopiaPerfil"/>
                    {/if}        
                    
                </div> 
                <br>            
                    <input type="submit" value="GRAVAR" name="btnGravar" class="btn btn-primary" />         
                <br>
                <br>
            </form>
            
            {if {$registro.idPerfil}>0}
                {if (isset($lista_de_menus)) }
                    <div class="alert alert-info" >
                        <tt><h1>MENUS DISPONIVEIS </h1></tt>
                    </div>                            
                    <form name="frm-perfil-menu" 
                      action="/perfil/insere_menu" 
                      method="POST" enctype="multipart/form-data">
                        <div class="input-group col-lg-12">
                            <div class="input-group col-lg-8">
                                <span class="input-group-addon btn-outline btn-primary">MENUS: </span>                    
                                <select class="form-control col-lg-12" name="idMenu" id="idMenu"> 
                                    {html_options options=$lista_de_menus} 
                                </select>   

                                <span class="input-group-btn">
                                    <input type="submit" class="btn btn-primary" value="INCLUIR" name="btnInsereMenu" id="btnInsereMenu"/>
                                </span>                                      
                            </div><!-- /input-group -->
                        </div>                
                        <input type="hidden" name="idPerfil" id="idPerfil"value="{$registro.idPerfil}" />

                    </form>
               {else}
                   TODOS OS MENUS JA ESTAO ASSOCIADOS A ESTE PERFIL
               {/if}

               <br>
               <br>
               <br>
                
                <div id ="painel_menu"> 
                    <table class="table-bordered" border="1" width="100%">
                        <thead>
                            <tr class="alert alert-info"><td colspan="3"><tt><h1>MENUS VINCULADOS AO PERFIL ACIMA </h1></tt></tr>
                            <tr style="font-size: large">
                                <th>GRUPO DO MENU</th>
                                <th>ITEM DO MENU</th>
                                <th>ACAO</th>
                            </tr>
                        </thead>
                        <tbody> 
                            {if (isset($lista_de_mp))}
                                {foreach from=$lista_de_mp item="linha"}
                                <tr> 
                                    <td>{$linha.descpai}</td>
                                    <td>{$linha.descfilho}</td>
                                    <td><a href="/perfil/excluir_menu/idPerfil/{$linha.idPerfil}/idMenu/{$linha.idMenu}">Excluir</a></td>
                                </tr>
                                {foreachelse}
                                <tr><td colspan="3">NENHUM MENU ASSOCIADO A ESTE PERFIL</td></tr>
                                {/foreach}  
                            {/if}                            
                        </tbody>
                    </table>
                </div>
           {/if} 
        <!--Altere daqui pra cima-->
    </div>
    </div>
</div>

<!-- JavaScript -->
<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/perfil/perfil_novo.js"></script>



{include file="comuns/footer.tpl"}

