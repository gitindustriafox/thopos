{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>GERENCIAR A SOLICITACAO DE COMPRAS CADASTRADA</h1></tt>
            </div> 
                <br>
                <div class="row small">
                    <h3> &nbsp; INFORMACOES DO CABECALHO DA SOLICITACAO DE COMPRAS: </h3>
                    <br>
                    
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="solicitacao">ID</label>
                            <input type="text" class="form-control" name="idSolicitacao" id="idSolicitacao" value="{$registro.idSolicitacao|default:''}" READONLY>           
                        </div>
                    </div>                     
                    <div class="col-md-2">
                        <label for="form-control">DATA DA SOLICITACAO</label>
                        <input type="text" class="form-control datetime" name="dtSolicitacao" readonly id="dtSolicitacao" value="{$registro.dtSolicitacao|date_format:'%d/%m/%Y'|default:Date("d/m/Y")}" >           
                    </div> 
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="usuario">USUARIO SOLICITANTE</label>
                            <select class="form-control" name="idUsuario" id="idUsuario" readonly>
                                    {include file="solicitacaocompras/lista_usuarios.tpl"}     
                            </select>                                    
                        </div>
                    </div>                     
                    <div class="col-md-2">
                        <label for="form-control">SOLICITANTE</label>
                        <input type="text" class="form-control" name="dsSolicitante" readonly id="dsSolicitante" value="{$registro.dsSolicitante|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="prioridade">PRIORIDADE</label>
                            <select class="form-control" name="idPrioridade" id="idPrioridade" readonly value="{$registro.idPrioridade|default:''}">
                                {html_options options=$lista_prioridade selected=$registro.idPrioridade|default:''}
                            </select>                                    
                        </div>
                    </div>                                                             
                    <div class="col-md-3">
                        <label for="form-control">PRAZO ENTREGA - NECESSIDADE</label>
                        <input type="text" class="form-control datetime" name="dtNecessidade" readonly id="dtNecessidade" value="{$registro.dtNecessidade|date_format:'%d/%m/%Y'|default:''}" >           
                    </div> 
                </div>
                <div class="row small">
                    <div class="col-md-3">
                        <label for="form-control">LOCAL PARA ENTREGA</label>
                        <input type="text" class="form-control" name="dsLocalEntrega" id="dsLocalEntrega"  value="{$registro.dsLocalEntrega|default:''}" >           
                    </div> 
                    <div class="col-md-4">
                        <label for="form-control">OBSERVACOES</label>
                        <input type="text" class="form-control" name="dsObservacao" id="dsObservacao" value="{$registro.dsObservacao|default:''}" >           
                    </div> 
                    
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="situacao" >SITUACAO</label>
                            <select class="form-control"  id="idSituacao" name="idSituacao"> 
                                    {include file="solicitacaocompras/lista_situacao.tpl"}       
                            </select>                                    
                        </div>
                    </div> 
                    </br>
                    <div class="col-md-2">
                        <div class="form-group">
                            <a class="btn btn-primary" id="btn-gravarcabecalho" title="Clique aqui para gravar o cabeçalho da Solicitacao"onclick="solicitacaocompras.mudarstatus();" {*{if $registro.idSolicitacao  neq  ''}  disabled {/if}*}  >GRAVAR</a> 
                            <a class="btn btn-primary" id="btn-sairtela" title="Clique aqui para sair da tela e voltar a lista de solicitacoes" href="/solicitacaocomprasmanutencao"  onclick="solicitacaocompras.desabilitaid();"> ABORTAR</a><br>                
                        </div> 
                    </div>
                </div> 
                <br>
                <br>
                {if $registro.idSolicitacao}
                    <div class="row small" >
                        <h3> &nbsp; PRODUTOS/SERVICOS PARA ESTA SOLICITACAO DE COMPRAS:</h3>
                        <br>
                        <input type="hidden" class="form-control" id="idSolicitacaoItem" name="idSolicitacaoItem" readonly>
                        <div class="col-md-1">
                            </br>
                            <div class="form-group">
                                 <input type="radio" onchange='solicitacaocompras.escolherIS();' id="stTipoI" name="stTipoIS" value="0" checked> Produto<br>
                                 <input type="radio" onchange='solicitacaocompras.escolherIS();' id="stTipoS" name="stTipoIS" value="1"> Servico<br>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
{*                                <select class="form-control"  id="idInsumo" name="idInsumo"readonly onchange="solicitacaocompras.lerunidade();"> 
                                        {include file="solicitacaocompras/lista_produtos.tpl"}       
                                </select>    *}
                                <div id='labelproduto'>
                                    <label for="linsumo">PRODUTO</label>                        
                                </div>
                                <input data-label="insumo" type="text" data-tipo="dsInsumo" class="form-control complete_produto" name="insumo" id="insumo" onchange="solicitacaocompras.lerunidade();" />     {*onkeypress="pedido.completar_Parceiro();" *}
                                <input type="hidden" id="idInsumo" name="idInsumo"/>                                
                            </div>
                        </div>                    
                        <div class="col-md-3">
                            <div id='labelnomeproduto'>
                                <label for='form-control'>SUGESTAO PRODUTO</label>
                            </div>
                            <input type="text" class="form-control" name="dsProduto"  readonly id="dsProduto" value="">       
                        </div> 
                        <div class="col-md-1">
                            <label for="form-control">QTDE</label>
                            <input type="text" class="form-control valor" name="qtSolicitacao" id="qtSolicitacao"  readonly value="">      
                        </div> 
                        <div class="col-md-1">
                            <div class="form-group">
                                <label for="unidade" {*style="color: red!important;"  onclick="solicitacaocompras.atualizaunidades();" *}>UN</label>
                                <select class="form-control"  id="idUnidade" readonly name="idUnidade"> 
                                        {include file="solicitacaocompras/lista_unidades.tpl"}       
                                </select>                                    
                            </div>
                        </div> 
                        <div class="col-md-2">
                            <label for="form-control">OBSERVACAO</label>
                            <input type="text" class="form-control" name="dsObservacaoItem"  id="dsObservacaoItem" value="" > 
                        </div> 
                    </div>
                    <div class="row small" >
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="Parceiro">PARCEIRO</label>                        
                                <input data-label="Parceiro" type="text" data-tipo="dsParceiro" class="form-control complete_Parceiro" name="dsParceiro"  id="dsParceiro" onchange="solicitacaocompras.lerParceiro();" value="{$registro.dsParceiro|default:''}"/>     {*onkeypress="pedido.completar_Parceiro();" *}
                                <input type="hidden" id="idParceiro" value="{$registro.idParceiro|default:''}" name="idParceiro"/>                                
                            </div>
                        </div>                    
                        
                        <div class="col-md-3">
                            <label for="form-control">SUGESTAO Parceiro</label>
                                <input type="text" class="form-control" name="dsParceiroSugerido"  id="dsParceiroSugerido" value="" readonly> 
                        </div> 
                        <div class="col-md-2" >
                            <label for="form-control">LINK</label>
                            <input type="text" class="form-control" name="dsLink"  id="dsLink" value="" readonly> 
                        </div> 
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="situacao" >SITUACAO</label>
                                <select class="form-control"  id="idSituacao1" name="idSituacao1"> 
                                        {include file="solicitacaocompras/lista_situacao1.tpl"}       
                                </select>                                    
                            </div>
                        </div> 
                    </div>
                    <div class="row small" >
                        <h3> &nbsp; DESTINO DESTE PRODUTO/SERVICO:</h3>
                        <br>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="centrocusto">CENTRO DE CUSTO </label>
                                <select class="form-control" name="idCentroCusto" id="idCentroCusto" readonly>
                                    {html_options options=$lista_centrocusto selected=null}
                                </select>                      
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="maquina">MAQUINA </label>
                                <select class="form-control" name="idMaquina"  id="idMaquina" readonly>  
                                    {html_options options=$lista_maquina selected=null}
                                </select>                      
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="ordemservico">ORDEM DE SERVICO </label>
                                <select class="form-control" name="idOS"  id="idOS"  readonly> 
                                    {html_options options=$lista_os selected=null}
                                </select>                      
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="motivo">MOTIVO - CAUSA</label>
                                <select class="form-control" name="idMotivo"  id="idMotivo" readonly>  
                                    {html_options options=$lista_motivo selected=null}
                                </select>                      
                            </div>
                        </div>
                        <br>
                        <div class="col-md-1">
                              <a class="btn btn-primary" id="btn-adicionaitem" title="Clique aqui para atualizar o status e a observacao" onclick="solicitacaocompras.gravaritem_manutencao();">GRAVAR</a> 
                        </div> 
                    </div>
                         
                    {include file="solicitacaocompras/lista_itens1.html"}
                {/if}
            
        <!--Altere daqui pra cima-->
    </div>
    <div id="modal_fotos">
        {include file="solicitacaocompras/modalFotos/modal.tpl"}                            
    </div>     
    <div id="modal_mensagem">
        {include file="solicitacaocompras/mensagem/modal.tpl"}                            
    </div>                 
                
    
                
</div>

<!-- JavaScript -->
<script src="/files/js/solicitacaocompras/solicitacaocompras.js"></script> {*
<script src="/files/js/jquery.price_format.1.3"></script>
<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
*}<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
{*<script type="text/javascript" src="/files/default/js/jquery.price/jquery.price_format.1.3.js"></script>
*}


{include file="comuns/footer.tpl"}

