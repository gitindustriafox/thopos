        <div class='row small form-group'>
        <div class="container relatorio">
            <div class="panel panel-default">
                <table class="table" border="0">
                    <tbody>
                        <tr>
                            <td style="width:95%">
                                <table class="table">
                                    <tr>
                                        <td rowspan="4">
                                            &nbsp; <img src="http://thopos.tecnologiafox.com/files/images/fox_logo.jpeg" width="140" height="90" alt=""/>
                                        </td>
                                        <td   style="text-align: center; font-size: 20px;" >
                                        </td>
                                        <td   style="text-align: center; font-size: 20px;" >
                                        </td>
                                        <td style="text-align: right;" >
                                            <table>
                                                <tr> 
                                                    <td style="text-align: right; font-size: 28px;" >
                                                        <strong>
                                                            ORÇAMENTO DE COMPRA &nbsp;
                                                        </strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td   style="text-align: right; font-size: 28px;" >
                                                        <strong>
                                                           {$pedido.idOrcamentoCompras} &nbsp;
                                                        </strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td   style="text-align: right; font-size: 10px;" >
                                                        <strong>
                                                            {$pedido.dtOrcamentoCompras|date_format:'%d/%m/%Y'|default:Date("d/m/Y")} &nbsp; &nbsp; &nbsp;
                                                        </strong>
                                                    </td>                                        
                                                </tr>
                                            </table>                                    
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="table" border="0">
                    <tbody>
                        <tr>
                            <td>
                                <table  id="table-data" >
                                    <tr>
                                        <td style="text-align: left; font-size: 10px; ">  
                                            <div>
                                                <p style="padding-top: 2px;">
                                                    <strong>
                                                    &nbsp;{$pedido.dsEmpresa|default:''}
                                                    </strong>
                                                </p>
                                            </div>
                                        </td>
                                        <td>  
                                            <table>
                                                <tr>
                                                    <td></td>
                                                    <td style="text-align: right; font-size: 8px; ">  
                                                        <div>
                                                            <p>
                                                                COMPRADOR: &nbsp;{$pedido.contatoempresa}
                                                            </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>  
                                    <tr rowspan="5">
                                        <td style="text-align: left; font-size: 10px; ">  
                                            <div>
                                                <p style="padding-top: 10px;">
                                                    <strong>
                                                    &nbsp;{$pedido.enderecoempresa} - {$pedido.bairroempresa}  - {$pedido.cidadeempresa} 
                                                    </strong>
                                                </p>
                                            </div>
                                        </td>
                                        <td>  
                                            <table>
                                                <tr>
                                                    <td></td>                                            
                                                    <td style="text-align: right; font-size: 8px; ">  
                                                        <div>
                                                            <p>
                                                       SOLICITANTE:  &nbsp;{$pedido.dsSolicitante|default:''}
                                                            </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>  
                                    <tr>
                                        <td>  
                                            <div style="text-align: left; font-size: 8px; ">
                                                <p style="padding-top: 2px;">
                                                    &nbsp;{$pedido.cnpjempresa|default:''} 
                                                </p>
                                            </div>
                                        </td>
                                        <td>  
                                            <table>
                                                <tr>
                                                    <td></td>                                            
                                                    <td style="text-align: right; font-size: 8px; ">  
                                                        <div>
                                                            <p>
                                                       FONE:  &nbsp;{$pedido.celularempresa|default:''}
                                                            </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        </div>
                                        
        <div class='row small form-group'>
        <div class="container relatorio">
            <div class="panel panel-default">
                <table style="text-align: left; font-size: 10px; " class="table" border="0">
                    <tbody>
                        <tr>
                            <td>  
                                <div style="text-align: left; font-size: 10px; ">
                                    <p>
                                        <strong>
                                            &nbsp;FORNECEDOR:  <br>
                                        </strong>
                                    </p>
                                </div>
                            </td>   
                            <td></td>
                        </tr>
                        <tr>
                            <td style="text-align: left; font-size: 10px; ">  
                                <div >
                                    <p>
                                        &nbsp;&nbsp;&nbsp;{$pedido.dsParceiro|default:''}
                                    </p>
                                </div>
                            </td>  
                            <td style="text-align: left; font-size: 10px;">  
                                <div >
                                    <p>
                                        C.N.P.J: {$pedido.cdCNPJ|default:''} 
                                    </p>
                                </div>
                            </td>                    
                        </tr>
                        <tr>
                            <td style="text-align: left; font-size: 8px; ">  
                                <div >
                                    <p>
                                        &nbsp;&nbsp;&nbsp;{$pedido.dsEndereco|default:''}
                                    </p>
                                </div>
                            </td>
                            <td  style="text-align: left; font-size: 8px; ">  
                                <div>
                                    <p>
                                       CONTATO: &nbsp;{$pedido.dsContato|default:''}
                                    </p>
                                </div>
                            </td>                    
                        </tr>
                        <tr>
                            <td style="text-align: left; font-size: 2px; ">  
                                <div>
                                    <p>
                                        &nbsp;&nbsp;&nbsp;{$pedido.dsCidade|default:''}
                                    </p>
                                </div>
                            </td>
                            <td style="text-align: left; font-size: 2px; ">  
                                <div>
                                    <p>
                                        CELULAR: {$pedido.dsCelular|default:''}
                                    </p>
                                </div>
                            </td>                    
                        </tr>
                        <tr>
                            <td style="text-align: left; font-size: 2px; ">  
                                <div>
                                    <p>
                                        &nbsp;&nbsp;&nbsp;{$pedido.dsBairro|default:''}
                                    </p>
                                </div>
                            </td>
                            <td style="text-align: left; font-size: 2px; ">  
                                <div>
                                    <p>
                                        TELEFONE: {$pedido.dsFone|default:''}
                                    </p>
                                </div>
                            </td>                    
                        </tr>
                        <tr>
                            <td  style="text-align: left; font-size: 2px; ">  
                                <div>
                                    <p>
                                        &nbsp;&nbsp;&nbsp;
                                    </p>
                                </div>
                            </td>
                            <td>  
                                <div style="text-align: center; font-size: 2px; ">
                                    <p>
                                        EMAIL: {$pedido.dsEmail|default:''}
                                    </p>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        </div>
        <div class='row small form-group'>                                    
        <div class="container relatorio">
            <div class="panel panel-default">
                <table class="table" border="0">
                    <tbody>
                        <tr>
                            <td style="width:80%">
                                <table class="table table-condensed table-sm">
                                    <tr>
                                        <td   style="text-align: center; font-size: 14px;" >
                                            <strong>
                                                PRODUTOS / SERVIÇOS
                                            </strong>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-striped" border="0">
                    <thead>
                        <tr>
                            <th>DESCRIÇÃO</th>
                            <th>QUANTIDADE</th>
                            <th>VALOR UNITARIO</th>
                            <th>VALOR TOTAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach from=$pedidoitens item="linha"}  
                        <tr>
                            {if $linha.stTipoIS|default:'0' eq '1'}
                                <td>{$linha.dsServico|default:''}</td>
                            {else}
                                <td>{$linha.dsInsumo|default:''}</td>
                            {/if}
                            <td>{$linha.qtSolicitacao|default:'0'|number_format:"2":",":"."}</td> 
                            <td>___________________</td> 
                            <td>___________________</td> 
                        </tr>
                        {/foreach}        
                    </tbody>
                </table>
            </div>
        </div>
        </div>
        <div class='row small form-group'>
        <div class="container relatorio form-group">
            <div class="panel panel-default">                       
            <table class="table" border="0">
                <tbody>
                    <tr>
                        <td>
                            <table  id="table-data" style="font-size: 4px;" >
                                <tr>
                                    <td>  
                                        <div style="text-align: center; font-size: 4px; ">
                                            <p style="padding-top: 4px;">
                                                <strong>
                                                    &nbsp;PRAZO DE ENTREGA:  
                                                </strong>
                                                {$pedido.dtPrazoEntrega|date_format:'%d/%m/%Y'|default:Date("d/m/Y")}
                                            </p>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>  
                                        <div style="text-align: center; font-size: 4px; ">
                                            <p style="padding-top: 2px;">
                                                <strong>
                                                    &nbsp;CONDIÇÕES DE PAGAMENTO: 
                                                </strong>
                                                {$pedido.dsCondicoesPagamento|default:''} 
                                            </p>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p style="padding-top: 2px;">
                                                &nbsp;
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>  
                                        <div style="text-align: center; font-size: 4px; ">
                                            <p style="padding-top: 2px;">
                                                <strong>
                                                    &nbsp;LOCAL DE ENTREGA: 
                                                </strong>
                                                {$pedido.localentrega|default:''} 
                                            </p>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>  
                                        <div style="text-align: center; font-size: 4px; ">
                                            <p style="padding-top: 2px;">
                                                <strong>
                                                    &nbsp;FRETE: 
                                                </strong>
                                                {$pedido.dsFrete|default:''} 
                                            </p>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>  
                                        <div style="text-align: center; font-size: 4px; ">
                                            <p style="padding-top: 2px;">
                                                <strong>
                                                    &nbsp;OBSERVAÇÕES: 
                                                </strong>
                                                {$pedido.dsObservacao|default:''} 
                                            </p>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    </div>
    <div class='row small form-group'>
        <table class="table" border="0">
            <tbody>
                <tr>
                    <td colspan="3" style="background-color: black; color: #e1edf7; text-align: center; font-size: 10px; width:70%;">                       
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="background-color: black; color: #e1edf7; text-align: center; font-size: 10px; width:70%;">
                        Notas Fiscais Eletrônicas (danfe e arquivo xml) deverão ser enviadas ao email nfe@industriafox.com
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="background-color: black; color: #e1edf7; text-align: center; font-size: 10px; width:70%;">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="background-color: wite;  text-align: center; font-size: 10px; width:70%;">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="background-color: #0080CC;  text-align: center; font-size: 10px; width:70%;">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="background-color: #0080CC;  text-align: center; font-size: 10px; width:70%;">
                        Condições gerais para fornecimento
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="background-color: #0080CC;  text-align: center; font-size: 10px; width:70%;">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="background-color: wite;  text-align: center; font-size: 10px; width:70%;">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="background-color: wite;  text-align: left; font-size: 8px; width:70%;">
                        *QUALQUER ÔNUS CAUSADO PELO FORNECIMENTO DE MATERIAL/SERVIÇO INCORRETO ACARRETARÁ A GERAÇÃO DE COBRANÇA IMEDIATA AO FORNECEDOR
(BOLETO DE 30 DIAS). O FORNECEDOR TEM 30 DIAS PARA ARGUMENTAR E COMPROVAR O FORNECIMENTO CORRETO, ONDE COMPROVANDO FALHA DA ORGANIZAÇÃO FOX, A COBRANÇA
SERÁ IMEDIDATAMENTE CANCELADA. COBRANÇA SERÁ MEDIANTE CALCULO DE PREJUÍZOS CAUSADOS PELO FORNECIMENTO INCORRETO DO SERVIÇO/MATERIAL

                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="background-color: wite;  text-align: left; font-size: 8px; width:70%;">
                        *A SUBSTITUIÇÃO PELO MATERIAL CORRETO FICA NA RESPONSABILIDADE DO FORNECEDOR, INDEPENDENTE DO TIPO DE TRANSPORTE DO PEDIDO
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="background-color: wite;  text-align: center; font-size: 8px; width:70%;">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="background-color: wite; color:red; text-align: center; font-size: 8px; width:70%;">
                        <strong>
                        *HORÁRIO DE RECEBIMENTO: DE SEGUNDA A SEXTA DAS 8HS ÀS 11:45HS E 13:30HS ÀS 16:30HS*
                        </strong>                        
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="background-color: wite;  text-align: center; font-size: 8px; width:70%;">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="background-color: wite;  text-align: left; font-size: 8px; width:70%;">
                        1. Só serão considerados os reajustes que estiverem neste pedido; <br>
                        2. Informar no corpo da nota fiscal modo de pagamento informado neste pedido. Caso não seja informado, não nos responsabilizaremos por solicitar
                        boletos, dados bancários ou informações aqui combinadas. <br>
                        3. Os prazos de entrega dos materiais devem ser estritamente observados, qualquer alteração deverá ser informada prontamente pelo fornecedor; <br>
                        4. As quantidades mencionadas neste pedido não podem ser modificadas eventuais excessos. <br>
                        5. INDÚSTRIA E COMÉRCIO FOX, poderá suspender, protelar ou antecipar parcial ou totalmente as condições de entrega de todo os quaisquer dos materiais
                        constantes deste pedido, caso lhe seja de conveniência ou mesmo motivos alheios a sua vontade. <br>
                        6. O fornecedor arcará com todas as despesas decorrentes da entrega e devolução de materiais fora dos padrões especificados pela INDÚSTRIA FOX. <br>
                        7. É IMPRESCINDÍVEL CONSTAR NO CORPO DA NOTA FISCAL O NÚMERO DO NOSSO PEDIDO DE COMPRA. <br>
                        8. Notas Fiscais Eletrônicas (danfe e arquivo xml) deverão ser enviadas ao email nfe@industriafox.com <br>
                        9. A via impressa da nota fiscal eletrônica deverá acompanhar o material. A falta da mesma poderá não permitir o recebimento. <br>
                    </td>
                </tr>
                
            </tbody>
        </table>
    </div>  
    <div class='row small form-group'>
        <table class="table" border="0">
            <tbody>
                <tr>
                    <td style="font-size: 8px; text-align: left;">
                        Aceite do fornecedor - Carimbar e assinar
                    </td>
                    <td style="font-size: 8px; text-align: left;">
                        FABIANA ARRUDA - Comprador &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    <td style="font-size: 8px; text-align: right;">
                        Organização Fox
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
