    <div class="form-group">        
        <div class="row small">
            <div class="col-md-1">
                <label for="pedido">ORCAMENTO</label>
                <input type="text" id="idOrcamentoComprasEmitir" class="form-control" readonly nome="idOrcamentoComprasEmitir" value="{$cabec.idOrcamentoCompras|default:''}" />
            </div>
            <div class="col-md-2">
                <label for="form-control">DATA ORCAMENTO</label>
                <input type="text" class="form-control obg standard-mask-date standard-form-date standard-form-require" readonly name="dtOrcamento" id="dtOrcamento" value="{$cabec.dtOrcamento|date_format:'%d/%m/%Y'|default:Date("d/m/Y")}" >           
            </div> 
            <div class="col-md-5">
                <div class="form-group">
                    <label for="usuario">EMPRESA</label>
                    <select class="form-control" name="idEmpresa" id="idEmpresa" onclick="pedido.lerempresa()">
                            {include file="pedido/lista_empresas.tpl"}     
                    </select>                                    
                </div>
            </div>   
            <div class="col-md-4">
                <label for="form-control">LOCAL DE ENTREGA</label>
                <input type="text" class="form-control" name="dsLocalEntrega" id="dsLocalEntrega1" value='{$cabec.dsLocalEntrega|default:''}'>           
            </div>
        </div> 
    </div>
        <div class="panel-body">
            <div class="row small" id="linhas_orcamento">
                <table class="table table-striped" border="1">
                    <thead>
                        <tr>
                            <th>CODIGO DO PRODUTO</th>
                            <th>NOME DO PRODUTO/SERVICO</th>
                            <th>QUANTIDADE</th>
                            <th>VAL UNITARIO</th>
                            <th>VAL TOTAL ITEM</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach from=$orcamentoitens item="linha"}  
                        <tr>
                            <td>{$linha.cdInsumo|default:''}</td>
                                {if $linha.stTipoIS|default:'0' eq '1'}
                                    {if $linha.idInsumo|default:'' neq ''}
                                        <td style="width:40%;">{$linha.dsServico|default:''}</td>
                                    {else}
                                        <td style="width:40%;">{$linha.dsProduto|default:''}</td>
                                    {/if}
                                {else}
                                    {if $linha.idInsumo|default:'0' neq '0'}
                                        <td style="width:40%;">{$linha.dsProduto|default:''}</td>
                                    {else}
                                        <td style="width:40%;">{$linha.dsInsumo|default:''}</td>
                                    {/if}
                                {/if}
                            <td>{$linha.qtSolicitacao|default:''|number_format:2:",":"."|default:"0,00"}</td> 
                            <td>R$ {$linha.vlUnitario|number_format:2:",":"."|default:"0,00"}</td> 
                            <td>R$ {$linha.vlPedido|number_format:2:",":"."|default:"0,00"}</td> 
                        </tr>
                        {/foreach}        
                    </tbody>
                </table>
            </div>
        </div>
    {include file="orcamentocompras/orcamento/modal/detalhes.tpl"}  
                        