    <div class="form-group">
        <div class="row small">
            <div class="col-md-5">
                <label for="Parceiro">PARCEIRO</label>                        
                <input type="hidden" id="idParceiro" class="form-control" nome="idParceiro" value='{$parceiro.idParceiro|default:''}' readonly/>
                <input type="text" id="dsParceiro" class="form-control" nome="dsParceiro" value='{$parceiro.dsParceiro|default:''}' readonly/>
            </div>
        </div> 
        <br>
        <div class="row small">
            <div class="col-md-3">
                <label for="form-control">CONTATO</label>
                <input type="text" class="form-control" name="dsContato" id="dsContato" value='{$parceiro.dsContato|default:''}'>           
            </div>
            <div class="col-md-2">
                <label for="form-control">FONE</label>
                <input type="text" class="form-control" name="dsFone" id="dsFone" value='{$parceiro.dsFone|default:''}'>           
            </div>
            <div class="col-md-2">
                <label for="form-control">CELULAR</label>
                <input type="text" class="form-control" name="dsCelular" id="dsCelular" value='{$parceiro.dsCelular|default:''}'>           
            </div>
            <div class="col-md-5">
                <label for="form-control">E-MAIL</label>
                <input type="text" class="form-control" name="dsEmail" id="dsEmail" value='{$parceiro.dsEmail|default:''}'>           
            </div>
        </div>
        <br>
        <div class="row small">
            <div class="col-md-5">
                <label for="form-control">ENDERECO</label>
                <input type="text" class="form-control" name="dsEndereco" id="dsEndereco" value='{$parceiro.dsEndereco|default:''}'>           
            </div>
            <div class="col-md-2">
                <label for="form-control">CIDADE</label>
                <input type="text" class="form-control" name="dsCidade" id="dsCidade" value='{$parceiro.dsCidade|default:''}'>           
            </div>
            <div class="col-md-2">
                <label for="form-control">CNPJ</label>
                <input type="text" class="form-control" name="cdCNPJ" id="cdCNPJ" value='{$parceiro.cdCNPJ|default:''}'>           
            </div>
            <div class="col-md-3">
                <label for="form-control">PRAZO DE ENTREGA</label>
                <input type="text" class="form-control" name="dtPrazoEntrega" id="dtPrazoEntrega" value='{$parceiro.dtPrazoEntrega|date_format:'%d/%m/%Y'|default:''}'>           
            </div>
        </div>
        <br>
        <div class="row small">
            <div class="col-md-3">
                <label for="form-control">CONDICOES DE PAGAMENTO</label>
                <input type="text" class="form-control" name="dsCondicoesPagamento" id="dsCondicoesPagamento" value='{$parceiro.dsCondicoesPagamento|default:''}'>           
            </div>
            <div class="col-md-3">
                <label for="form-control">OBSERVACOES</label>
                <input type="text" class="form-control" name="dsObservacoes" id="dsObservacoes" value='{$parceiro.dsObservacao|default:''}'>           
            </div>
            <div class="col-md-3">
                <label for="form-control">LOCAL DO PDF</label>
                <input type="text" class="form-control" name="dsCaminhoPDF" id="dsCaminhoPDF" readonly value='{$parceiro.dsCaminhoPDF|default:''}'>           
            </div>
            </br>
            <div class="col-md-1" id="botao_criar">
                 <a class="btn btn-primary"  onclick="emitir_pedido_compra.criarCotacao();">CRIAR PDF</a>
            </div> 
            <div class="col-md-1">
                 <a class="btn btn-primary" data-dismiss="modal" onclick="emitir_pedido_compra.clear_log();">ABORTAR</a>
            </div> 
        </div>
  </div>
  <script src="/files/js/pedido/pedido.js"></script>
  
