{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>{if {$registro.idEmpresa}>0} ALTERAR EMPRESA: {$registro.dsEmpresa|default:''}{else} INCLUIR NOVA EMPRESA{/if}</h1></tt>
            </div>          
            <a href="/empresa" class="btn btn-primary"> ABORTAR</a><br>

            <form name="frm-empresa" 
                  action="/empresa/gravar_empresa" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario();">
                <br>
                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idEmpresa}>0}
                                <label for="form-control">CODIGO</label>
                                <input type="text" class="form-control" name="idEmpresa" id="idEmpresa" value="{$registro.idEmpresa}" READONLY>           
                        {else}
                                 <label for="form-control">CODIGO</label>
                                 <input type="text" class="form-control" name="idEmpresa" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-3">
                        <label for="form-control">NOME DA EMPRESA</label>
                        <input type="text" class="form-control" name="dsEmpresa" id="dsEmpresa" value="{$registro.dsEmpresa|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">C.N.P.J</label>
                        <input type="text" class="form-control" name="cdCNPJ" id="cdCNPJ" value="{$registro.cdCNPJ|default:''}" >           
                    </div> 
                    <div class="col-md-3">
                        <label for="form-control">ENDERECO</label>
                        <input type="text" class="form-control" name="dsEndereco" id="dsEndereco" value="{$registro.dsEndereco|default:''}" >           
                    </div> 
                    <div class="col-md-3">
                        <label for="form-control">BAIRRO</label>
                        <input type="text" class="form-control" name="dsBairro" id="dsBairro" value="{$registro.dsBairro|default:''}" >           
                    </div> 
                </div>  
                <br>                    
                <div class="row small">
                    <div class="col-md-3">
                        <label for="form-control">CIDADE</label>
                        <input type="text" class="form-control" name="dsCidade" id="dsCidade" value="{$registro.dsCidade|default:''}" >           
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">C.E.P</label>
                        <input type="text" class="form-control" name="cdCEP" id="cdCEP" value="{$registro.cdCEP|default:''}" >           
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">U.F</label>
                        <input type="text" class="form-control" name="cdUF" id="cdUF" value="{$registro.cdUF|default:'SP'}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">CONTATO</label>
                        <input type="text" class="form-control" name="dsContato" id="dsContato" value="{$registro.dsContato|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">FONE</label>
                        <input type="text" class="form-control" name="dsFone" id="dsFone" value="{$registro.dsFone|default:''}" >           
                    </div> 
                    <div class="col-md-3">
                        <label for="form-control">CELULAR</label>
                        <input type="text" class="form-control" name="dsCelular" id="dsCelular" value="{$registro.dsCelular|default:''}" >           
                    </div> 
                </div>    
                <br>                    
                <div class="row small">
                    <div class="col-md-3">
                        <label for="form-control">EMAIL</label>
                        <input type="text" class="form-control" name="dsEmail" id="dsEmail" value="{$registro.dsEmail|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">SITE</label>
                        <input type="text" class="form-control" name="dsSite" id="dsSite" value="{$registro.dsSite|default:''}" >           
                    </div> 
                </div> 
                <br>
                  <div class="col-md-3">
                    <div class="row small">
                        <input class="btn btn-primary" type="submit" value="    GRAVAR" name="btnGravar"/>         
                    </div> 
                  </div> 
                <br>
            </form>
            
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
{*<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
*}<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/empresa/empresa_novo.js"></script>



{include file="comuns/footer.tpl"}

