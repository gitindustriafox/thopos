{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>{if {$registro.idNCM}>0}ALTERAR O NCM - {$registro.dsNCM|default:''} {else} INCLUIR NOVO NCM{/if}</h1></tt>
            </div>          
            <a href="/ncm" class="btn btn-primary"> ABORTAR</a><br>

            <form name="frm-ncm" 
                  action="/ncm/gravar_ncm" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario();">
                <br>
                <div class="row small">
                    <div class="col-md-1">
                        {if {$registro.idNCM}>0}
                                <label for="form-control">ID</label>
                                <input type="text" class="form-control" name="idNCM" id="idNCM" value="{$registro.idNCM}" READONLY>           
                        {else}
                                 <label for="form-control">ID</label>
                                 <input type="text" class="form-control" name="idNCM" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">CODIGO</label>
                        <input type="text" class="form-control" name="cdNCM" id="cdNCM" value="{$registro.cdNCM|default:''}" >           
                    </div> 
                    <div class="col-md-10">
                        <label for="form-control">DESCRICAO</label>
                        <input type="text" class="form-control" name="dsNCM" id="dsNCM" value="{$registro.dsNCM|default:''}" >           
                    </div> 
                </div>  
                <br>                    
                <div class="col-md-3">
                  <div class="row small">
                      <input class="btn btn-primary" type="submit" value="GRAVAR" id="btnGravar" name="btnGravar"/>         
                  </div> 
                </div> 
                <br>
            </form>
                
            
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
{*<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>
*}<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/ncm/ncm.js"></script>



{include file="comuns/footer.tpl"}

