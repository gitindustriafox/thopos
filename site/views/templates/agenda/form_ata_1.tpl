{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="modal_fotos">
        {include file="agenda/modalFotos/modal.tpl"}                            
    </div>             
    <div id="modal_datas">
        {include file="agenda/modalDatas/modal.tpl"}                            
    </div>             
    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <form name="frm-agenda" 
                  action="/agenda/gravar_ata_agenda" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario()" id="frm-agenda">
                <br>
                <input type="text" class="hidden" name="opcao" id="opcao" value="{$opcao|default:'iniciar'}" >           
                <input type="text" class="hidden" name="voltarmenu" id="voltarmenu" value="{$voltarmenu|default:'1'}" >    
                <input type="text" class="hidden" name="idagenda" id="idagenda" value="{$registro.idagenda|default:''}" >    
                <br>
                <div class="panel-body panel panel-default" > 
                    <div class="row small">
                        <li class="dropdown user-dropdown">
                            <a
                                href="#" id="menuagenda" class="dropdown-toggle"  style="color: #000000; font-size: large" data-toggle="dropdown"> 
                                <i class="fa fa-list" style="font-size: large;"> &nbsp;&nbsp; {if {$registro.idagenda}>0}Agenda em andamento{/if}</i> 
                            </a>
                            <ul class="dropdown-menu">
                                {if $registro.idagenda neq ''}
                                    <li><a onclick="agenda.gravar_ata_agenda();" ><i class="fa fa-calendar"></i>&nbsp;&nbsp;Salvar as informações</a></li>                                                
                                {/if}
                                {if $registro.idagenda neq ''}
                                    <li class="divider"></li>
                                    <li><a onclick="agenda.selecionarfotoR('ATA');"><i class="fa fa-adjust"></i>&nbsp;&nbsp;Anexar arquivos</a></li>
                                {/if}
                                {if $registro.idagenda neq ''}
                                    <li class="divider"></li>
                                    <li><a  onclick="agenda.criarPDF('{$registro.idagenda}');"><i class="fa fa-cut"></i>&nbsp;&nbsp;Criar PDF da Agenda + ATA</a></li>
                                {/if}
                                {if $registro.idagenda neq ''}
                                    {if $registro.dsCaminhoPDF neq ''}
                                        <li class="divider"></li>
                                        <li><a href="/{$registro.dsCaminhoPDF}" target="_blanck"><i class="fa fa-user"></i>&nbsp;&nbsp;Ver o PDF criado</a></li>
                                    {/if}
                                {/if}
                                {if $registro.idagenda neq ''}
                                    <li class="divider"></li>
                                    <li><a onclick="agenda.enviaremail('0','{$registro.idagenda}','Ata');"><i class="fa fa-list-alt"></i>&nbsp;&nbsp;Enviar e-mail para os participantes</a></li>
                                {/if}
                                {if $registro.idagenda neq ''}
                                    {if $registro.stSituacao|default:'0' < 2}                                
                                        <li class="divider"></li>
                                        <li><a href="/agenda/encerrar/idagenda/{$registro.idagenda}/opcao/{$opcao|default:null}/voltarmenu/{$voltarmenu}"><i class="fa fa-pencil-square-o"></i>&nbsp;&nbsp;Encerrar a Agenda</a></li>
                                    {/if}
                                {/if}
                                <li class="divider"></li>
                                {if $voltarmenu|default:null}
                                    <li><a href="/menuagenda"><i class="fa fa-eject"></i>&nbsp;&nbsp;Sair</a></li>
                                {else}    
                                    <li><a href="/agenda/ata/opcao/{$opcao|default:null}"><i class="fa fa-eject"></i>&nbsp;&nbsp;Sair</a></li>
                                {/if}  
                            </ul>
                        </li>
                    </div> 
                </div>
                                
                <div class="panel-body panel panel-default" > 
{*                    <div class="panel-heading">
                        <h4> <strong>DADOS PRINCIPAIS  -  {$statusagenda|default:''}</strong> <h4>
                    </div> 
*}                    <div class="panel-item" id="painel_colaboradores" >
                        <br>
                        <div class="row small">
                            <div class="col-md-1 hidden">
                                {if {$registro.idagenda}>0}
                                        <label for="form-control">Identificador</label>
                                        <input type="text" class="form-control" name="idagenda" id="idagenda" value="{$registro.idagenda}" READONLY>           
                                {else}
                                         <label for="form-control">Identificador</label>
                                         <input type="text" class="form-control" name="idagenda" value="" READONLY>           
                                {/if}                     
                            </div> 
                            <div class="col-md-2">
                                <label for="form-control">Data da Agenda</label>
                                <input type="text" class="form-control datetime" {if $registro.stSituacao|default:'0' > 1} disabled {/if} name="dtagenda" id="dtagenda" value="{$registro.dtagenda|date_format:'%d/%m/%Y %H:%M'|default:''}" >           
                            </div> 
                            <div class="col-md-1">
                                <label for="form-control">Duração estimada</label>
                                <input type="text" readonly class="form-control" {if $registro.stSituacao|default:'0' > 1} disabled {/if} name="dsTempoDuracao" id="dsTempoDuracao" value="{$registro.dsTempoDuracao|default:''}" >           
                            </div>                     
                            <div class="col-md-2">
                                <label for="form-control">Tipo de Agenda</label>
                                <select class="form-control" id="idTipo" {if $registro.stSituacao|default:'0' > 1} disabled {/if} name="idTipo"> 
                                        {include file="agenda/lista_tipo.tpl"}       
                                </select>                                    
                            </div> 
                            <div class="col-md-2">
                                <label for="Local">Local da Agenda</label>                        
                                <input data-label="Local" type="text" data-tipo="dsLocal" class="form-control complete_Local" name="dsLocal" {if $registro.stSituacao|default:'0' > 1} disabled {/if}  id="dsLocal" value="{$registro.dsLocalagenda|default:''}"/>     {*onkeypress="pedido.completar_Parceiro();" *}
                                <input type="hidden" id="idLocal"  value="{$registro.idLocalagenda|default:''}" name="idLocal"/>
                            </div>                                                                     
                            <div class="col-md-2">
                                <label for="form-control">Data da realização</label>
                                <input type="text" {if $registro.stSituacao|default:'0' > 1} disabled {/if}  class="form-control datetime" name="dtRealizacao" id="dtRealizacao" value="{$registro.dtRealizacao|date_format:'%d/%m/%Y %H:%M'|default:{$registro.dtagenda|date_format:'%d/%m/%Y %H:%M'}}" >           
                            </div> 
                            <div class="col-md-2">
                                <label for="form-control">Usuario responsavel pela ATA</label>
                                <select class="form-control" name="idUsuarioAta" id="idUsuarioAta" {if $registro.stSituacao|default:'0' > 1} disabled {/if}  {if $registro.idagenda eq ''} disabled {/if} > 
                                        {include file="agenda/lista_usuarioAta.tpl"}       
                                </select>                                    
                            </div>   
                            <div class="col-md-1">
                                <label for="form-control">Duração real</label>
                                <input type="text" class="form-control" name="dsTempoDuracaoReal" {if $registro.stSituacao|default:'0' > 1} disabled {/if}  id="dsTempoDuracaoReal" value="{$registro.dsTempoDuracaoReal|default:''}" >           
                            </div>                         
                        </div>                                             
                        <br>
                        <div class="row small">
                            <div class="col-md-7">
                                <label for="form-control">Pauta da Agenda</label>
                                <textarea class="form-control" {if $registro.stSituacao|default:'0' > 1} disabled {/if}  rows="5" name="dsAssunto" id="dsAssunto">{$registro.dsAssunto|default:''}</textarea>           
                            </div> 
                            <div class="col-md-5">
                                <br>
                                <label for="form-control">Histórico das Agenda relacionadas a esta</label>
                                <table class="table table-striped" border="0">
                                    <thead>
                                        <tr>
                                            <th style="font-size:8px!important;">Data</th>
                                            <th style="font-size:8px!important;">Local</th>
                                            <th style="font-size:8px!important;">Pauta</th>
                                            <th style="font-size:8px!important;">Ação</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {foreach from=$agenda_listaRamificada item="linha"}  
                                        <tr>
                                            <td {if $linha.idagenda eq $registro.idagenda} style="color: green;" {/if} >{$linha.dtagenda|date_format:'%d/%m/%Y %H:%M'|default:''}</td> 
                                            <td {if $linha.idagenda eq $registro.idagenda} style="color: green;" {/if} >{$linha.dsLocalagenda|default:''}</td> 
                                            <td {if $linha.idagenda eq $registro.idagenda} style="color: green;" {/if} >{$linha.dsPauta|default:''}</td>
                                            <td>
                                                {if $linha.idagenda neq $registro.idagenda}
                                                    <a title="Ver" class="glyphicon glyphicon-exclamation-sign" href="/agenda/novo_ata/idagenda/{$linha.idagenda}/opcao/{$opcao|default:null}/voltarmenu/{$voltarmenu}"></a>
                                                {/if}
                                            </td>                                        
                                        </tr>
                                        {/foreach}        
                                    </tbody>
                                </table>
                                <a href="/agenda/continua_agenda/idagenda/{$registro.idagenda}/opcao/{$opcao|default:null}/voltarmenu/{$voltarmenu}"> Clique aqui </a>  para criar uma nova Agenda a partir desta!                        
                            </div>                    
                        </div> 
                        <br> 
{*                        <div {if $registro.stSituacao|default:'0' > 1} readonly {/if} >
                            <input {if $registro.stSituacao|default:'0' > 1} disabled {/if} type="submit" value="SALVAR" name="btnGravar" class="btn btn-primary" />         
                            <input type="button" value="ANEXAR" onclick="agenda.selecionarfotoR('ATA');" id="btnAnexar" name="btnAnexar" class="btn btn-primary" {if $registro.idagenda eq ''} disabled {/if}/>         
                            <input type="button" value="CRIAR PDF" onclick="agenda.criarPDF('{$registro.idagenda}');" id="btnPDF" name="btnPDF" class="btn btn-primary" {if $registro.idagenda eq ''} disabled {/if}/>         
                            <a title="Ver o PDF" class="btn btn-primary" href="/{$registro.dsCaminhoPDF}" target="_blanck" {if $registro.dsCaminhoPDF eq ''} disabled {/if} >VER PDF</a>
                            <input type="button" value="E-MAIL" onclick="agenda.enviaremail('0','{$registro.idagenda}','Ata');" id="btnAnexar" name="btnAnexar" class="btn btn-primary" {if $registro.idagenda eq ''} disabled {/if}/>         
                            {if $voltarmenu|default:null}
                                <a href="/menuagenda" class="btn btn-primary">SAIR</a>
                            {else}    
                                <a href="/agenda/ata/opcao/{$opcao|default:null}" class="btn btn-primary">SAIR</a>
                            {/if}  
                            <a {if $registro.stSituacao|default:'0' > 1} disabled {/if} href="/agenda/encerrar/idagenda/{$registro.idagenda}/opcao/{$opcao|default:null}/voltarmenu/{$voltarmenu}" class="btn btn-primary">ENCERRAR</a>                                     
                        </div>
                        <br>
*}                    </div>
                </div>
                <div class="panel-item panel panel-default"> 
                    <div class="panel-heading mostra">
                        <h4> <strong>Participantes</strong> <h4>
                    </div> 
                    <div class="panel-body" id="painel_colaboradores">
                        <br>
                        <div class="row small" >
{*                            <div style="font-size: small;"> &nbsp; ADICIONAR PARTICIPANTES PARA ESTA Agenda</div>
                            <br>
*}                            <input type="text" class="form-control hidden" name="idParticipante" id="idParticipante">                           
                            <div class="col-md-2">
                                <label for="form-control">Participante</label>
                                <select  {if $registro.stSituacao|default:'0' > 1} disabled {/if} class="form-control"  id="idUsuario"  {if $registro.idagenda eq ''} disabled {/if} onchange='agenda.lerUsuario();'> 
                                        {include file="agenda/lista_usuario.tpl"}       
                                </select>                                    
                            </div>      
                            <div class="col-md-2">
                                <label for="form-control">Nome</label>
                                <input {if $registro.stSituacao|default:'0' > 1} disabled {/if} type="text" class="form-control" name="dsNome" id="dsNome" {if $registro.idagenda eq ''} disabled {/if} >           
                            </div> 
                            <div class="col-md-2">
                                <label for="form-control">Email do participante</label>
                                <input {if $registro.stSituacao|default:'0' > 1} disabled {/if} type="text" class="form-control" name="dsEmail" id="dsEmail" {if $registro.idagenda eq ''} disabled {/if} >           
                            </div> 
                            <div class="col-md-2">
                                <label for="form-control">Celular do participante</label>
                                <input {if $registro.stSituacao|default:'0' > 1} disabled {/if} type="text" class="form-control" name="dsCelular" id="dsCelular" {if $registro.idagenda eq ''} disabled {/if} >           
                            </div>                                 
                            <div class="col-md-3">
                                <label for="form-control">Assunto específico do participante</label>
                                <input {if $registro.stSituacao|default:'0' > 1} disabled {/if} type="text" class="form-control" id="dsAssuntoE" {if $registro.idagenda eq ''} disabled {/if} >           
                            </div> 

                            <br>
                            <div class="col-md-1">
                              <div class="row small">
                                  <a {if $registro.stSituacao|default:'0' > 1} disabled {/if} class="btn btn-primary" id="btn-adicionaitem" title="ADICIONAR" onclick="agenda.adicionaritemAta();" {if $registro.idagenda eq ''} disabled {/if}  >ADICIONAR</a> 
                              </div> 
                            </div> 
                        </div>
                        <br>
                        <div id="mostraritens">
                             {include file="agenda/itensata.html"}
                        </div>
                    </div>
                </div>    
                        
                <div class="panel-item panel panel-default"> 
                    <div class="panel-heading mostra">
                        <h4> <strong>ATA da Agenda</strong> <h4>
                    </div> 
                    <div class="panel-body" id="painel_colaboradores">
                        <br>
                        <div class="row small" >
                            <div class="col-md-12">
                                <label for="form-control">Digite a ata da Agenda {if $registro.dsCaminhoPDF|default:''} (caminho do arquivo: {$registro.dsCaminhoPDF|default:''}) {/if}</label>
                                <textarea  {if $registro.stSituacao|default:'0' > 1} disabled {/if} class="form-control"  rows="20" name="dsAta" id="dsAta">{$registro.dsAta|default:''}</textarea>           
                            </div>                         
                        </div>
                        <br>
{*                        <input {if $registro.stSituacao|default:'0' > 1} disabled {/if} type="submit" value="SALVAR" name="btnGravar" class="btn btn-primary" />                                     
                        <input type="button" value="ANEXAR" onclick="agenda.selecionarfotoR('ATA');" id="btnAnexar" name="btnAnexar" class="btn btn-primary" {if $registro.idagenda eq ''} disabled {/if}/>         
                        <input type="button" value="CRIAR PDF" onclick="agenda.criarPDF('{$registro.idagenda}');" id="btnPDF" name="btnPDF" class="btn btn-primary" {if $registro.idagenda eq ''} disabled {/if}/>         
                        <a title="Ver o PDF" class="btn btn-primary" href="/{$registro.dsCaminhoPDF|default:''}" target="_blanck" {if $registro.dsCaminhoPDF eq ''} disabled {/if} >VER PDF</a>
                        <input type="button" value="E-MAIL" onclick="agenda.enviaremail('0','{$registro.idagenda}','Ata');" id="btnAnexar" name="btnAnexar" class="btn btn-primary" {if $registro.idagenda eq ''} disabled {/if}/>                         
                        {if $voltarmenu|default:null}
                            <a href="/menuagenda" class="btn btn-primary">SAIR</a>
                        {else}    
                            <a href="/agenda/ata/opcao/{$opcao|default:null}" class="btn btn-primary">SAIR</a>
                        {/if}    
                        <a {if $registro.stSituacao|default:'0' > 1} disabled {/if} href="/agenda/encerrar/idagenda/{$registro.idagenda}/opcao/{$opcao|default:null}/voltarmenu/{$voltarmenu}" class="btn btn-primary">ENCERRAR</a>                                     
*}                    </div>
                </div>                          
            </form>                      
            <div class="panel-item panel panel-default"> 
                <div class="panel-heading mostra">
                    <h4> <strong>Definir ações aos participantes</strong> <h4>
                </div> 
                <div class="panel-body" id="painel_colaboradores">
                    <br>
                    <div class="row small" >
{*                        <h4> &nbsp; ADICIONAR PARTICIPANTES PARA AS AÇÕES</h4>
                        <br>
*}                      <input type="text" class="form-control hidden" name="idTarefa" id="idTarefa">                           
                        <div class="col-md-3">
                            <label for="form-control">Participante</label>
                            <select  {if $registro.stSituacao|default:'0' > 1} disabled {/if} class="form-control"  id="idUsuarioTarefa" name="idUsuarioTarefa" > 
                                    {include file="agenda/lista_usuariotarefa.tpl"}       
                            </select>                                    
                        </div>                                             
                        <div class="col-md-6">
                            <label for="form-control">Ação a ser executada</label>
                            <input {if $registro.stSituacao|default:'0' > 1} disabled {/if} type="text" class="form-control" name="dsTarefa" id="dsTarefa" >           
                        </div> 
                        <div class="col-md-2">
                            <label for="form-control">Prazo para entrega</label>
                            <input {if $registro.stSituacao|default:'0' > 1} disabled {/if} type="text" class="form-control datetime" name="dtPrazo" id="dtPrazo" value="" >           
                        </div> 

                        <br>
                        <div class="col-md-1">
                          <div class="row small">
                              <a {if $registro.stSituacao|default:'0' > 1} disabled {/if} class="btn btn-primary" id="btn-adicionaitem" title="Adicionar o participante" onclick="agenda.adicionarTarefa('ata');" {if $registro.idagenda eq ''} disabled {/if}  >Adicionar</a> 
                          </div> 
                        </div> 
                    </div>
                    <br>
                    <div id="mostraritenstarefa">
                         {include file="agenda/itenstarefa.html"}
                    </div>
{*                    <br>
                    <input type="button" value="ANEXAR" onclick="agenda.selecionarfotoR('ATA');" id="btnAnexar" name="btnAnexar" class="btn btn-primary" {if $registro.idagenda eq ''} disabled {/if}/>         
                    <input type="button" value="CRIAR PDF" onclick="agenda.criarPDF('{$registro.idagenda}');" id="btnPDF" name="btnPDF" class="btn btn-primary" {if $registro.idagenda eq ''} disabled {/if}/>         
                    <a title="Ver o PDF" class="btn btn-primary" href="/{$registro.dsCaminhoPDF|default:''}" target="_blanck" {if $registro.dsCaminhoPDF eq ''} disabled {/if} >VER PDF</a>
                    <input type="button" value="E-MAIL" onclick="agenda.enviaremail('0','{$registro.idagenda}','Ata');" id="btnAnexar" name="btnAnexar" class="btn btn-primary" {if $registro.idagenda eq ''} disabled {/if}/>                         
                    {if $voltarmenu|default:null}
                        <a href="/menuagenda" class="btn btn-primary">SAIR</a>
                    {else}    
                        <a href="/agenda/ata/opcao/{$opcao|default:null}" class="btn btn-primary">SAIR</a>
                    {/if}    
                    <a {if $registro.stSituacao|default:'0' > 1} disabled {/if} href="/agenda/encerrar/idagenda/{$registro.idagenda}/opcao/{$opcao|default:null}/voltarmenu/{$voltarmenu}" class="btn btn-primary">ENCERRAR</a>                                     
*}                </div>
            </div>               
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
{*<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>*}
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/agenda/agenda.js"></script>
<script src="/files/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>
<link href="/files/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css"/>

{include file="comuns/footer.tpl"}

