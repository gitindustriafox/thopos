<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
    <head>
        <title>{$title}</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <style type="text/css">
            .page-header {
                padding-bottom: 0;
                margin: 20px 0 20px;
            }

            body { background: #fff; width: 98%; margin: 0 auto; padding-top: 20px; }

            .relatorio { text-transform: uppercase; }
            .table { font-size: 11px; }
            p { font-size: 11px; 
                padding-top: 3px;;
            }

            blockquote footer { font-size: 11px; }

            .table-direita {
                border-left: none;
                border-right: none;
                border-top: 1px nome;
                border-bottom: 1px nome;
            }

            .table-esquerda {
                border-left: none;
                border-right: none;
                border-top: 1px nome;
                border-bottom: 1px nome;
            }

            table th, table td { 
                font-family: sans-serif;
                color: #000000;                
                text-transform: uppercase; }
            {include file="files/css/bootstrap.css"}
            {include file="files/css/bootstrap-theme.css"}
        </style>
    </head>

    <body >
        <div class="row small form-group">
            <div class="container relatorio">
                <div class="col-md-12">
                    <table class="table" border="1">
                        <tbody>
                            <tr>
                                <td rowspan="1" style="width: 18%;">
                                    &nbsp; <img src="http://thopos.tecnologiafox.com/files/images/fox_logo.jpeg" width="80" height="50" alt=""/>
                                </td>
                                <td style="text-align: center; font-size: 12px;" >
                                    <strong>
                                        ATA DE Agenda
                                    </strong>
                                </td>
                                <td style="text-align: center; font-size: 12px; width: 18%;" >
                                    <strong>
                                        FL
                                    </strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
        