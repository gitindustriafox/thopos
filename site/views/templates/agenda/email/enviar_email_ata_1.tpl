<html lang="pt-BR">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>{$title|default:""}</title>
        <style>
            th {
                padding:2px;text-align:left;border-bottom:1px solid gray;
            }
            td {
                padding:2px;text-align:left;border-bottom:1px solid gray;
            }
        </style>
    </head>
    <body>
        <table border="0" width="100%" cellpadding="0" cellspacing="0" style="font: 12px arial, sans-serif; margin: 0 auto;">
            <tr>
                <td>
                    {$usuariodestino},<br /><br />
                    Este e-mail foi enviado em <strong>{$data_envio|date_format:'%d/%m/%Y %H:%M'|default:''}</strong> pelo sistema <strong> THOPOS </strong>, FAVOR NÃO RESPONDER<br /><br />

                    Agenda <strong> {$tipoagenda|default:''} </strong> agendado por <strong>{$usuarioenvio}</strong> <br /><br />
                    Data: <strong>{$dtRealizacao|date_format:'%d/%m/%Y %H:%M'|default:''}</strong> <br /><br />
                    Duração Efetiva:  <strong> {$dsTempoDuracaoReal|default:''} </strong> <br /><br />
                    Local: <strong>{$dsLocalagenda|default:''}</strong> <br /><br />
                    Pauta: <strong> {$dsPauta} </strong><br /><br />
                    Ata da Agenda: <strong> {$dsAta} </strong><br /><br />
                    Participantes: <br /><br/>

                    <table class="table" border="0">
                        <thead>
                            <tr>
                                <th>NOME</th>
                                <th>TAREFA</th>
                                <th>PRAZO</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach from=$lista_itens item="linha"}  
                            <tr>
                                <td>{$linha.dsNome|default:''}</td> 
                                <td>{$linha.dsTarefa|default:''}</td> 
                                <td>{$linha.dtPrazo|date_format:'%d/%m/%Y %H:%M'|default:''}</td> 
                            </tr>
                            {/foreach}        
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>