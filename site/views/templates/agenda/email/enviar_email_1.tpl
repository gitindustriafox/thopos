<html lang="pt-BR">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>{$title|default:""}</title>
        <style>
            th {
                padding:2px;text-align:left;border-bottom:1px solid gray;
            }
            td {
                padding:2px;text-align:left;border-bottom:1px solid gray;
            }
        </style>
    </head>
    <body>
        <table border="1" width="100%" cellpadding="0" cellspacing="0" style="font: 12px arial, sans-serif; margin: 0 auto;">
            <tr>
                <td>
                    Caro(a) {$usuariodestino},<br/><br/>
                    <div  style = "width: 100%; font-size: x-large; text-align: center;">
                        <strong>INVITE</strong> <br/><br/>
{*                        <a href="http://thopos.tecnologiafox.com/agenda/recebeEmail/idagenda/idagenda/{$idagenda|default:''}/idParticipante/{$idParticipante|default:''}/confirma/sim" rel="noopener noreferrer"> VOU PARTICIPAR </a> 
                        -
                        <a style="color: #B32B2B;" href="http://local.thopos.com.br/agenda/recebeEmail/idagenda/idagenda/{$idagenda|default:''}/idParticipante/{$idParticipante|default:''}/confirma/nao" target="_blank" rel="noopener noreferrer"> NAO PODEREI PARTICIPAR </a> 
                        <br/><br/>
*}                    </div>
                    
                    Este e-mail foi enviado em <strong>{$data_envio|date_format:'%d/%m/%Y %H:%M'|default:''}</strong> pelo sistema <strong> THOPOS </strong>, mensagem automática, favor não responder<br /><br />

                    Agenda {$dsTipoagenda|default:''} <strong> {$tipoagenda|default:''} </strong> agendado por <strong>{$usuarioenvio}</strong> <br /><br />
                    Data: <strong>{$dtagenda|date_format:'%d/%m/%Y %H:%M'|default:''}</strong> <br /><br />
                    Duração Estimada:  <strong> {$dsTempoDuracao|default:''} </strong> <br /><br />
                    Local: <strong>{$dsLocalagenda|default:''}</strong> <br /><br />
                    Pauta:  <strong> {$dsPauta|default:''} </strong> <br /><br />
                    Participantes: <br /><br/>

                    <table class="table table-striped" border="0">
                        <thead>
                            <tr>
                                <th>
                                    <div <span style="font-size: 12px; border: 0px; padding: 10px; margin: 0px; position: relative; top: 9px; padding-bottom: 9px;">
                                        NOME    
                                        </span>
                                    </div>
                                </th>                                       
                                <th>
                                    <div <span style="font-size: 12px; border: 0px; padding: 10px; margin: 0px; position: relative; top: 9px; padding-bottom: 9px;">
                                        E-MAIL
                                        </span>
                                </div>
                                </th>
                                <th>
                                    <div <span style="font-size: 12px; border: 0px; padding: 10px; margin: 0px; position: relative; top: 9px; padding-bottom: 9px;">
                                        ASSUNTO
                                        </span>
                                </div>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach from=$lista_itens item="linha"}  
                            <tr>
                                <td>
                                    <div <span style="font-size: 12px; border: 0px; padding: 10px; margin: 0px; position: relative; top: 9px; padding-bottom: 9px;">
                                        {$linha.dsNome|default:''}
                                    </div>
                                </td>
                                <td>
                                    <div <span style="font-size: 12px; border: 0px; padding: 10px; margin: 0px; position: relative; top: 9px; padding-bottom: 9px;">
                                        {$linha.dsEmail|default:''}
                                    </div>
                                </td>
                                <td>
                                    <div <span style="font-size: 12px; border: 0px; padding: 10px; margin: 0px; position: relative; top: 9px; padding-bottom: 9px;">
                                        {$linha.dsAssunto|default:''}
                                    </div>
                                </td>
                            </tr>
                            {/foreach}        
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>