{assign var="t_tarefas" value=0} 
{assign var="t_abertas" value=0} 
{assign var="t_concluidas" value=0} 
<table class="table table-striped" border="1">
    <thead>
        <tr>
            <th>Colaborador</th>
            <th>Total atribuidas</th>
            <th>Total em aberto</th>
            <th>Total concluidas</th>
        </tr>
    </thead>
    <tbody>
        {foreach from=$resumo_lista item="linha"}  
        <tr><td>{$linha.Usuario|default:''}</td><td>{$linha.tarefas|default:''}</td><td>{$linha.abertas|default:''}</td><td>{$linha.concluidas|default:''}</td></tr>
        {$t_tarefas = $t_tarefas+{$linha.tarefas|default:''}}
        {$t_abertas = $t_abertas+{$linha.abertas|default:''}}
        {$t_concluidas = $t_concluidas+{$linha.concluidas|default:''}}
        {/foreach} 
    </tbody>
    <tfoot>
        <tr></tr>
        <tr>
            <td>Total de tarefas no período</td>
            <td>{$t_tarefas}</td>
            <td>{$t_abertas}</td>
            <td>{$t_concluidas}</td>            
        </tr>
    </tfoot>
</table>