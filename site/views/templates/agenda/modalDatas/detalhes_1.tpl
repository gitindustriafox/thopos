    <div class="form-group">
        <input type="text" class="form-control hidden" id="idTarefa" name="idTarefa" value="{$idTarefa}">                                       
        <input type="text" class="form-control hidden" id="idData" name="idData" value="{$idData}">                                       
        <input type="text" class="form-control hidden" id="origem" name="origem" value="{$origem}">                                       
        <div class="row small">
            <div class="col-md-4"> 
                <label for="form-control">Sub Tarefa</label>
                <input {if {$origem} eq 'usuario'} disabled {/if}  type="text" class="form-control" id="dsSubTarefa" name="dsSubTarefa">           
            </div> 
            <div class="col-md-2"> 
                <label for="form-control">Prazo para concluir</label>
                <input {if {$origem} eq 'usuario'} disabled {/if} type="text" class="form-control datetime" name="dtIntermediaria" id="dtIntermediaria">           
            </div> 
        </div> 
        <br>
        <div class="row small">
            <div class="col-md-2">   
                 <input {if {$origem} eq 'usuario'} disabled {/if} class="btn btn-primary" type="button"  onclick="agenda.salvardatas({$idTarefa},'ata');" value="Salvar" />
                 <input class="btn btn-primary" type="button" onclick="agenda.closejanelaDatas();" value="Sair" />
            </div>                       
        </div> 
        <div class="row small">
        </div>
    </div>
<script src="/files/js/agenda/agenda.js"></script>        