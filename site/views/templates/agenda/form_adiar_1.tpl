{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="modal_fotos">
        {include file="agenda/modalFotos/modal.tpl"}                            
    </div>             
    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <br> <br>
{*            <div class="" >
                <tt><h3>{if {$registro.idagenda}>0}Adiar uma Agenda{/if}</h3></tt>
            </div>          
            <br>
*}            <form name="frm-adiar-agenda" 
                  action="/agenda/gravar_adiar_agenda" 
                  method="POST" 
                  enctype="multipart/form-data"
                  onsubmit="return validaFormulario()" id="frm-adiar-agenda">
                <br>
                <input type="text" class="hidden" name="opcao" id="opcao" value="{$opcao|default:''}" >           
                <div class="panel-body panel panel-default" > 
                    <div class="row small">
                        <li class="dropdown user-dropdown">
                            <a
                                href="#" id="menuagenda" class="dropdown-toggle"  style="color: #000000; font-size: large" data-toggle="dropdown"> 
                                <i class="fa fa-list" style="font-size: large;"> &nbsp;&nbsp; Adiar uma Agenda</i> 
                            </a>
                            <ul class="dropdown-menu">
                                <li><a onclick="agenda.adiar_agenda();" ><i class="fa fa-calendar"></i>&nbsp;&nbsp;Salvar as informações</a></li>                                                
                                <li class="divider"></li>
                                <li><a onclick="agenda.enviaremail('0','{$registro.idagenda}','Adiar');"><i class="fa fa-list-alt"></i>&nbsp;&nbsp;Enviar e-mail para os participantes</a></li>
                                <li class="divider"></li>
                                <li><a href="/agenda/acao/opcao/{$opcao|default:null}"><i class="fa fa-eject"></i>&nbsp;&nbsp;Sair</a></li>
                            </ul>
                        </li>
                    </div> 
                </div>
                <div class="row small">
                    <div class="col-md-1 hidden">
                        {if {$registro.idagenda}>0}
                                <label for="form-control">Identificador</label>
                                <input type="text" class="form-control" name="idagenda" id="idagenda" value="{$registro.idagenda}" READONLY>           
                        {else}
                                 <label for="form-control">Identificador</label>
                                 <input type="text" class="form-control" name="idagenda" value="" READONLY>           
                        {/if}                     
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">Data atual</label>
                        <input type="text" class="form-control datetime" disabled name="dtagendaA" id="dtagendaA" value="{$registro.dtagenda|date_format:'%d/%m/%Y %H:%M'|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">Duração estimada</label>
                        <input type="text" class="form-control" name="dsTempoDuracao" disabled id="dsTempoDuracao" value="{$registro.dsTempoDuracao|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">Tipo de Agenda</label>
                        <select class="form-control"  id="idTipo" disabled name="idTipo"> 
                                {include file="agenda/lista_tipo.tpl"}       
                        </select>                                    
                    </div>  
                    <div class="col-md-2">
                        <label for="Local">Novo local ou mantenha o mesmo</label>                        
                        <input data-label="Local" type="text" data-tipo="dsLocal" class="form-control complete_Local" name="dsLocal"  id="dsLocal" value="{$registro.dsLocalagenda|default:''}"/>     {*onkeypress="pedido.completar_Parceiro();" *}
                        <input type="hidden" id="idLocal" value="{$registro.idLocalagenda|default:''}" name="idLocal"/>
                    </div>                                                                                             
                    <div class="col-md-2">
                        <label for="form-control">Nova data para a Agenda</label>
                        <input type="text" class="form-control datetime" name="dtagenda" id="dtagenda" value="" >           
                    </div> 
                </div>                                             
                <br>
                <div class="row small">
                    <div class="col-md-12">
                        <label for="form-control">Pauta da Agenda</label>
                        <textarea class="form-control"  rows="5" disabled="" name="dsAssunto" id="dsAssunto">{$registro.dsAssunto|default:''}</textarea>           
                    </div> 
                </div> 
{*                <br>            
                    <input type="submit" value="Salvar" title="Salvar as informações" name="btnGravar" class="btn btn-primary" />         
                    <input type="button" value="E-mail" title="Enviar e-mail aos participantes" onclick="agenda.enviaremail('0','{$registro.idagenda}','Adiar');" id="btnAnexar" name="btnAnexar" class="btn btn-primary" {if $registro.idagenda eq ''} disabled {/if}/>         
                    {if $voltarmenu|default:null}
                        <a href="/menuagenda" class="btn btn-primary">Sair</a>
                    {else}    
                        <a href="/agenda/acao/opcao/{$opcao|default:null}" class="btn btn-primary">Sair</a>
                    {/if}    
                <br>
*}                <br>
            </form>
            <div class="panel-item panel panel-default"> 
                <div class="panel-heading mostra">
                    <h4> <strong>Participantes da Agenda</strong> <h4>
                </div> 
                <div class="panel-body esconde" id="painel_colaboradores">
                    <div id="mostraritens">
                         {include file="agenda/itens.html"}
                    </div>
                </div>
            </div>    
            
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
{*<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>*}
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>
<script src="/files/js/agenda/agenda.js"></script>
<script src="/files/js/jquery.datetimepicker.full.min.js" type="text/javascript"></script>
<link href="/files/css/jquery.datetimepicker.css" rel="stylesheet" type="text/css"/>

{include file="comuns/footer.tpl"}

