{include file="comuns/head.tpl"}
<div id="wrapper">
    <!-- Sidebar -->
    {include file="comuns/sidebar.tpl"}    
    <div id="page-wrapper">
        <!--Altere daqui pra baixo-->
        <div class="panel-body">
            <div class="alert alert-info" >
                <tt><h1>MOVIMENTACAO DE ESTOQUE - SAIDA</h1></tt>
            </div> 
            <div class="row small">                
                <div class="col-md-1">
                    <a class="btn btn-primary" id="btn-novasaida" title="Clique aqui para iniciar a digitação de uma nova saída de estoque" href="/saidaestoque/novasaida" {*onclick="saidaestoque.novasaida();"*}>INCLUIR</a> 
                </div>
                <div class="col-md-1">
                    <a class="btn btn-primary" id="btn-gravarcabecalho" title="Clique aqui para gravar o cabeçalho da Saída de Estoque"onclick="saidaestoque.gravarcabecalho();" {*{if $registro.idMovimento  neq  ''}  disabled {/if} *} >GRAVAR CABECALHO</a> 
                </div> 
                <div class="col-md-1">
                    <a  class="btn btn-primary" id="btn-sairtela" title="Clique aqui para sair desta tela e voltar ao menu principal" href="/dashboard"  onclick="saidaestoque.desabilitaid();"> ABORTAR</a><br>                
                </div>
                {if $registro.idMovimento|default:''} 
                    <div class="col-md-1">
                            <a class="btn btn-primary" id="btn-excluir" title="Clique aqui para excluir este movimento" href="/saidaestoque/delmovimento/idMovimento/{$registro.idMovimento|default:''}"> EXCLUIR</a><br>                
                    </div>                                
                {/if}    
            </div>    
                <br>
                <div class="row small">
                    <h3> &nbsp; INFORMACOES DO CABECALHO: </h3>
                    <br>
                    
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="movimento">ID</label>
                            <input type="text" class="form-control" name="idMovimento" id="idMovimento" value="{$registro.idMovimento|default:''}" READONLY>           
                        </div>
                    </div>                     
                    <div class="col-md-2">
                    <div class="form-group">
                        <label for="tipomovimento">TIPO DE MOVIMENTO</label>
                        <select class="form-control" name="idTipoMovimento" id="idTipoMovimento">
                            {html_options options=$lista_tipomovimento selected=$registro.idTipoMovimento}
                        </select>                      
                    </div>
                    </div>                     
                    <div class="col-md-5">
                        <div class="form-group">
{*                            <label for="parceiro">CLIENTE</label>
                            <select class="form-control" name="idParceiro" id="idParceiro">
                                {html_options options=$lista_parceiro selected=$registro.idParceiro}
                            </select>                      *}
                        <label for="parceiro">PARCEIRO</label>                        
                        <input data-label="parceiro" type="text" data-tipo="dsParceiro" class="form-control complete_parceiro" name="Parceiro"  id="Parceiro" value="{$registro.dsParceiro|default:''}"/>     {*onkeypress="pedido.completar_Parceiro();" *}
                        <input type="hidden" id="idParceiro" value="{$registro.idParceiro|default:''}" name="idParceiro"/>
                            
                        </div>
                    </div>                     
                    <div class="col-md-4">
                        <label for="form-control">OBSERVACAO</label>
                        <input type="text" class="form-control" name="dsObservacao" id="dsObservacao" value="{$registro.dsObservacao|default:''}" >           
                    </div> 
                </div>                     
                <div class="row small">
                    <div class="col-md-1">
                        <label for="form-control">DATA</label>
                        <input type="text" class="form-control obg standard-mask-date standard-form-date standard-form-require" name="dtMovimento" id="dtMovimento" value="{$registro.dtMovimento|date_format:'%d/%m/%Y'|default:Date('d/m/Y')}" >           
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control" >NOTA FISCAL</label>
                        <input type="text" class="form-control" name="nrNota" id="nrNota" value="{$registro.nrNota|default:''}" >           
                    </div> 
                    <div class="col-md-1" >
                        <label for="form-control">PEDIDO</label>
                        <input type="text" class="form-control" name="nrPedido" id="nrPedido" value="{$registro.nrPedido|default:''}" >           
                    </div> 
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="colaborador">RETIRADO POR</label>
                            <select class="form-control" name="idColaborador" id="idColaborador">
                                {html_options options=$lista_colaborador selected=$registro.idColaborador}
                            </select>                      
                        </div>
                    </div>                    
                </div> 
                <br>
                <div class="row small" >
                    <h3> &nbsp; PRODUTOS PARA A SAIDA:</h3>
                    <br>
                    <div class="col-md-10">
                         <input type="hidden" class="form-control" id="idMovimentoItem" name="idMovimentoItem" readonly>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div id='labelproduto'>
                                <label for="linsumo">PRODUTO</label>                        
                            </div>
                            <input data-label="insumo" type="text" data-tipo="dsInsumo" class="form-control complete_produto" name="insumo" onchange="saidaestoque.carregalocalestoque();" id="insumo" value="{$registro.dsInsumo|default:''}"/>     {*onkeypress="pedido.completar_Parceiro();" *}
                            <input type="hidden" id="idInsumo" value="{$registro.idInsumo|default:''}" name="idInsumo"/>                            
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="localestoque">LOCAL DE ESTOQUE ORIGEM</label>
                            <select class="form-control" name="idLocalEstoque" id="idLocalEstoque" {if $registro.idMovimento|default:''  eq ''}  disabled  {/if} onchange="saidaestoque.lerunidade();"> 
                                {include file="saidaestoque/lista_localestoque.tpl"}
                            </select>                      
                        </div>
                    </div>                                                        
                    <div class="col-md-1">
                        <label for="form-control">UNIDADE</label>
                        <input type="text" class="form-control" name="dsUnidade" id="dsUnidade" disabled='disabled' value="">       
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">QT ESTOQUE</label>
                        <input type="text" class="form-control valor" name="qtEstoque" id="qtEstoque" disabled='disabled' value="{$registro.qtEstoque|default:'0'|number_format:'2':',':'.'}">       
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">QUANTIDADE</label>
                        {*{var_dump($registro)}*}
                        <input type="text" class="form-control" name="qtMovimento" id="qtMovimento" {if $registro.idMovimento|default:''  eq ''}  disabled  {/if} onchange="saidaestoque.calcularqtde();" value="">      
                    </div> 
                    <div class="col-md-1">
                        <label for="form-control">VAL DA SAIDA</label>
                        <input type="hidden" class="form-control" name="vlUltimaCompra"  id="vlUltimaCompra" value=""> 
                        <input type="text" class="form-control valor" name="vlMovimento" onfocus="saidaestoque.calcularqtde();" id="vlMovimento" value="{$registro.vlMovimento|default:''}"> 
                    </div> 
                    <div class="col-md-2">
                        <label for="form-control">REFERENCIA</label>
                        <input type="text" class="form-control" name="dsObservacaoItem" {if $registro.idMovimento|default:''  eq  ''}  disabled {/if} id="dsObservacaoItem" value=""> 
                    </div> 
                </div>
                <div class="row small" >
                    <h3> &nbsp; DESTINO DO PRODUTO: </h3>
                    <br>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="setor">SETOR </label>
                            <select class="form-control" name="idSetor" {if $registro.idMovimento|default:''  eq  ''}  disabled {/if} id="idSetor">
                                {html_options options=$lista_setor selected=null}
                            </select>                      
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="centrocusto">CENTRO DE CUSTO </label>
                            <select class="form-control" name="idCentroCusto" {if $registro.idMovimento|default:''  eq  ''}  disabled {/if} id="idCentroCusto">
                                {html_options options=$lista_centrocusto selected=null}
                            </select>                      
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for="maquina">MAQUINA </label>
                            <select class="form-control" name="idMaquina" {if $registro.idMovimento|default:''  eq  ''}  disabled {/if} id="idMaquina">  
                                {html_options options=$lista_maquina selected=null}
                            </select>                      
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="ordemservico">O.S </label>
                            <select class="form-control" name="idOS" {if $registro.idMovimento|default:''  eq  ''}  disabled {/if} id="idOS"> 
                                {html_options options=$lista_os selected=null}
                            </select>                      
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="motivo">MOTIVO O.S </label>
                            <select class="form-control" name="idMotivo" {if $registro.idMovimento|default:''  eq  ''} disabled {/if}  id="idMotivo">  
                                {html_options options=$lista_motivo selected=null}
                            </select>                      
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="localestoque">LOCAL DE ESTOQUE DESTINO </label>
                            <select class="form-control" name="idLocalEstoqueEntrada" id="idLocalEstoqueEntrada" {if $registro.idMovimento|default:''  eq ''}  disabled  {/if}> 
                                {html_options options=$lista_localestoqueentrada selected=null} {*$registro.idLocalEstoqueSaida*}
                            </select>                      
                        </div>
                    </div>                                                                                    
                    <br>
                    <div class="col-md-1">
                      <div class="row small">
                          <a class="btn btn-primary" id="btn-adicionaitem" title="adicionaitem" {if $registro.idMovimento|default:''  eq  ''}  disabled {/if} onclick="saidaestoque.gravaritem();">Adiciona Item</a> 
                      </div> 
                    </div> 
                </div>
                <br>
            {include file="saidaestoque/lista.html"}
            
        <!--Altere daqui pra cima-->
    </div>
</div>

<!-- JavaScript -->
{*<script src="/files/js/jquery.price_format.1.3"></script>
<script src="/files/js/jquery-1.10.2.js"></script>
<script src="/files/js/bootstrap.js"></script>*}
<!-- Toast Message -->
<script src="/files/js/toastmessage/javascript/jquery.toastmessage.js"></script>
<!-- Utils -->
<script src="/files/js/util.js"></script>

<script src="/files/js/saidaestoque/saidaestoque.js"></script>



{include file="comuns/footer.tpl"}

