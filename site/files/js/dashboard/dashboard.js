function reload() {
//    rec = window.location.reload();    
    $.ajax({
        type: "POST",
        url: "/mensagem/versetemmensagem/",
        dataType: "json",
        async: false,
        success: function (dataReturn) {
            if (dataReturn.limpar == 1) {
               $('#tirarcor').css({"color":"#FF0000"});
            } else {
               $('#tirarcor').css({"color":"#C1C6BF"});
            }
        }
    });    
};
setTimeout(reload, 1800);

$(document).ready(function () {
    $(".data").mask("99/99/9999");
    $.datepicker.setDefaults({
        defaultDate: null,
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: 'dd/mm/yy'
    });
    $("#data_inicial").datepicker({
        onClose: function (selectedDate) {
            $("#data_final").datepicker("option", "minDate", selectedDate);
        }
    });
    $("#data_final").datepicker({
        onClose: function (selectedDate) {
            $("#data_inicial").datepicker("option", "maxDate", selectedDate);
        }
    });
    $("#data_inicial_evento").datepicker({
        onClose: function (selectedDate) {
            $("#data_final_evento").datepicker("option", "minDate", selectedDate);
        }
    });
    $("#data_final_evento").datepicker({
        onClose: function (selectedDate) {
            $("#data_inicial_evento").datepicker("option", "maxDate", selectedDate);
        }
    });
    $("#data_inicial_resumo").datepicker({
        onClose: function (selectedDate) {
            $("#data_final_resumo").datepicker("option", "minDate", selectedDate);
        }
    });
    $("#data_final_resumo").datepicker({
        onClose: function (selectedDate) {
            $("#data_inicial_resumo").datepicker("option", "maxDate", selectedDate);
        }
    }); 
    
});


var dashboard = new function () {

            this.grafico = new function() {
//                this.clear_log = function () {
//                    $('#grafico_show').modal('hide');
//                };    

                this.montarGrafico =  function () {
                    jQuery.ajax({
                        async: true,
                        type: "post",
                        dataType: "json",
                        url: "/dashboard/grafico",
                        success: function (dataReturn) {
                            $('#grafico_show').modal('show');                                    
                            var dados = [];
                            dados = dataReturn.local;
                            dados1 = dataReturn.grupo;
                            
                            google.charts.load("current", {packages:["corechart"]});
                            google.charts.setOnLoadCallback(drawChart);
                            
                            function drawChart () {
                                var data = google.visualization.arrayToDataTable(dados,true);
                                var options = {
                                    width: 1200,
                                    height: 400,
                                    title: 'Valor do Estoque por Locais de Estoque - ' + dataReturn.qtInsumoSemEstoque + ' produtos sem valor de estoque',
                                    is3D: true
                                };
                                var data1 = google.visualization.arrayToDataTable(dados1,true);
                                var options1 = {
                                    width: 1200,
                                    height: 400,
                                    title: 'Valor do Estoque por Grupos',
                                    is3D: true
                                };
                                var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
                                var chart1 = new google.visualization.PieChart(document.getElementById('piechart_3d1'));
                                chart.draw(data, options);                                
                                chart1.draw(data1, options1);                                
                            }
                        }
                    });
                };
                this.montarGraficoInsumo =  function (idInsumo) {
                    jQuery.ajax({
                        async: true,
                        type: "post",
                        data: {
                            "idInsumo": idInsumo
                        },                        
                        dataType: "json",
                        url: "/dashboard/graficoInsumo",
                        success: function (dataReturn) {                            
                            $('#graficoInsumo_show').modal('show'); 
                            
                            google.charts.load("current", {packages:["corechart"]});
                            google.charts.setOnLoadCallback(drawChart);
                            
                            function drawChart () {
                                var data = google.visualization.arrayToDataTable(dataReturn,false);
                                var options = {
                                    width: 1200,
                                    height: 400,
                                    title: 'Valor das Entradas e saídas nos últimos 12 meses',
                                    curveType: 'function',
                                    legend: { position: 'right' }                                    
                                };
                                var chart = new google.visualization.LineChart(document.getElementById('piechart_3dInsumo'));
                                chart.draw(data, options);                                
                            }
                        }
                    });
                };
                this.montarGraficoPedido =  function () {
                    jQuery.ajax({
                        async: true,
                        type: "post",
                        dataType: "json",
                        url: "/dashboard/graficoPedido",
                        success: function (dataReturn) {                            
                            $('#graficoPedido_show').modal('show'); 
                            
                            google.charts.load("current", {packages:["corechart"]});
                            google.charts.setOnLoadCallback(drawChart);
                            
                            function drawChart () {
                                var data = google.visualization.arrayToDataTable(dataReturn,false);
                                var options = {
                                    width: 1200,
                                    height: 400,
                                    title: 'Quantidade total de pedidos nos últimos 12 meses',
                                    curveType: 'function',
                                    legend: { position: 'right' }                                    
                                };
                                var chart = new google.visualization.LineChart(document.getElementById('piechart_3dPedido'));
                                chart.draw(data, options);                                
                            }
                        }
                    });
                };
                
                this.montarGraficoSolicitacao =  function () {
                    jQuery.ajax({
                        async: true,
                        type: "post",
                        dataType: "json",
                        url: "/dashboard/graficoSolicitacao",
                        success: function (dataReturn) {                            
                            $('#graficoSolicitacao_show').modal('show'); 
                            
                            google.charts.load("current", {packages:["corechart"]});
                            google.charts.setOnLoadCallback(drawChart);
                            
                            function drawChart () {
                                var data = google.visualization.arrayToDataTable(dataReturn.prioridade,false);
                                var data1 = google.visualization.arrayToDataTable(dataReturn.solicitante,false);
                                var options = {
                                    width: 1600,
                                    height: 400,
                                    title: 'Quantidade total de itens solicitados para compras nos últimos 12 meses por prioridade',
                                    curveType: 'function',
                                    legend: { position: 'right' }                                    
                                };
                                var options1 = {
                                    width: 1600,
                                    height: 400,
                                    title: 'Quantidade total de itens solicitados para compras nos últimos 12 meses por solicitante',
                                    curveType: 'function',
                                    legend: { position: 'right' }                                    
                                };
                                var chart = new google.visualization.LineChart(document.getElementById('piechart_3dSolicitacao'));
                                var chart1 = new google.visualization.LineChart(document.getElementById('piechart_3dSolicitacaoSol'));
                                chart.draw(data, options);                                
                                chart1.draw(data1, options1);                                
                            }
                        }
                    });
                };
            };
            
            this.marcarcomolido =  function (idMensagemItem) {
                $.ajax({
                    type: "POST",
                    data: {
                        "idMensagemItem": idMensagemItem
                    },
                    url: "/mensagem/marcarcomolido/",
                    dataType: "json",
                    async: false,
                    success: function (dataReturn) {
                        if (dataReturn.limpar == 0) {
                           $('#tirarcor').css({"color":"#C1C6BF"});
                        }
                        $('#linhas_mensagens').html(dataReturn.html);
                    }
                });                
            };
            this.digitarresposta =  function (idMensagemItem) {
                jQuery.ajax({
                    async: false,
                    type: "post",
                    dataType: "html",
                    url: "/mensagem/digitarresposta",
                    data: {
                        "idMensagemItem": idMensagemItem
                    },
                    complete: function (event, XMLHttpRequest) {
                        if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                            try {
                                $('#digitarresposta_show').each(function () {
                                    $('.modal-body', this).html(event.responseText);
                                    $(this).modal('show');
                                });
                            } catch (e) {
                                console.log(e);
                            }
                        }
                    }
                });
            };
            this.excluirmensagem =  function (idMensagem, idMensagemItem) {
                $.ajax({
                    type: "POST",
                    data: {
                        "idMensagem": idMensagem,
                        "idMensagemItem": idMensagemItem
                    },
                    url: "/mensagem/excluirmensagem/",
                    dataType: "json",
                    async: false,
                    success: function (dataReturn) {
                        if (dataReturn.limpar == 0) {
                           $('#tirarcor').css({"color":"#C1C6BF"});
                        }
                        $('#linhas_mensagens').html(dataReturn.html);
                    }
                });                
            };
            this.clear_mensagemresposta = function () {
                $('#digitarresposta_show').modal('hide');
            };    
            
            this.mensagem = new function() {
                this.clear_mensagem = function () {
                    $('#mensagens_show').modal('hide');
                };    

                this.montarTelaMensagem =  function () {
                    jQuery.ajax({
                        async: true,
                        type: "post",
                        dataType: "html",
                        url: "/mensagem/mensagens",
                        complete: function (event, XMLHttpRequest) {
                            if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                                try {
                                    $('#mensagens_show').each(function () {
                                        $('.modal-body', this).html(event.responseText);
                                        $(this).modal('show');
                                    });
                                } catch (e) {
                                    console.log(e);
                                }
                            }
                            
                        }
                    });
                };
                this.buscarID =  function () {
                    $.ajax({
                        type: "POST",
                        data: {
                            "idSolicitacao": $('#idSolicitacaoM').val(),
                            "descricao": $('#descricaoM').val()
                        },
                        url: "/mensagem/buscaId/",
                        dataType: "json",
                        async: false,
                        success: function (dataReturn) {
                            $("#mostrarlinhas").html(dataReturn.html);
                        }
                    });                
                };
                this.apagartodas =  function () {
                    if (confirm('DESEJA EXCLUIR TODAS AS MENSAGENS ABAIXO?')) {     
                        $.ajax({
                            type: "POST",
                            url: "/mensagem/apagartodas/",
                            dataType: "json",
                            async: false,
                            success: function (dataReturn) {
                                $("#mostrarlinhas").html(dataReturn.html);
                            }
                        });                
                    };
                };
            };

            this.lersolicitacaodetalhes = function(idSolicitacao) {
                $.ajax({
                    type: "POST",
                    data: {
                        "idSolicitacao": idSolicitacao
                    },
                    url: "/dashboard/lersolicitacaodetalhes/",
                    dataType: "json",
                    async: false,
                    success: function (dataReturn) {
                        $("#mostrardetalhes").html(dataReturn.html);
                    }
                });                
            };
            this.pedidodetalhes = function(idPedido) {
                $.ajax({
                    type: "POST",
                    data: {
                        "idPedido": idPedido
                    },
                    url: "/dashboard/lerpedidodetalhes/",
                    dataType: "json",
                    async: false,
                    success: function (dataReturn) {
                        $("#mostrardetalhes").html(dataReturn.html);
                    }
                });                
            };
            this.lerPedidos = function() {
                $.ajax({
                    type: "POST",
                    url: "/dashboard/lerPedidos/",
                    dataType: "json",
                    async: false,
                    success: function (dataReturn) {
                        $("#mostrarprincipal").html(dataReturn.html);
                        $("#mostrardetalhes").html(dataReturn.html2);
                    }
                });                
            };
                
            this.lerestoquegrupo = function() {
                $.ajax({
                    type: "POST",
                    url: "/dashboard/lerestoquegrupo/",
                    dataType: "json",
                    async: false,
                    success: function (dataReturn) {
                        $("#mostrarprincipal").html(dataReturn.html);
                        $("#mostrardetalhes").html(dataReturn.html2);
                    }
                });                
            };
            
            this.lerestoquegrupoLE = function(cdLocalEstoque) {
                $.ajax({
                    type: "POST",
                    url: "/dashboard/lerestoquegrupoLE/",
                    dataType: "json",
                    data: {
                        "cdLocalEstoque": cdLocalEstoque
                    },
                    async: false,
                    success: function (dataReturn) {
                        $("#mostrarprincipal").html(dataReturn.html);
                        $("#mostrardetalhes").html(dataReturn.html2);
                    }
                });                
            };
            
            this.lerestoquegrupoitens = function(idGrupo) {
                $.ajax({
                    type: "POST",
                    url: "/dashboard/lerestoquegrupoitens/",
                    dataType: "json",
                    data: {
                        "idGrupo": idGrupo
                    },
                    async: false,
                    success: function (dataReturn) {
                        $("#mostrardetalhes").html(dataReturn.html);
                    }
                });                
            };
                
            this.lerestoquelocalestoqueitens = function(cdLocalEstoque) {
                $.ajax({
                    type: "POST",
                    url: "/dashboard/lerestoquelocalestoqueitens/",
                    dataType: "json",
                    data: {
                        "cdLocalEstoque": cdLocalEstoque
                    },
                    async: false,
                    success: function (dataReturn) {
                        $("#mostrardetalhes").html(dataReturn.html);
                    }
                });                
            };
                
            this.lerPontoPedido = function() {
                $.ajax({
                    type: "POST",
                    url: "/dashboard/lerpontopedido/",
                    dataType: "json",
                    async: false,
                    success: function (dataReturn) {
                        $("#mostrarprincipal").html(dataReturn.html);
                        $("#mostrardetalhes").html(dataReturn.html2);
//                        document.getElementById("labeldetalhes").innerHTML =
//                              "<span class='pull-left huge_menor'>Ultimas Compras</span>";      
                    }
                });                
            };
            this.lermovimentoitens = function(idInsumo) {
                $.ajax({
                    type: "POST",
                    url: "/dashboard/lermovimentoitens/",
                    dataType: "json",
                    data: {
                        "idInsumo": idInsumo
                    },
                    async: false,
                    success: function (dataReturn) {
                        $("#mostrardetalhes").html(dataReturn.html);
                    }
                });                
            };
                
            
            this.event = new function () {
                this.reload = function () {
                    $.ajax({
                        type: "POST",
                        data: {
                            "idProjeto": $("#idProjeto_event").val(),
                            "dt_ini": $("#data_inicial_resumo").val(),
                            "dt_fin": $("#data_final_resumo").val()
                        },
                        url: "/dashboard/reloadDataEvent/",
                        dataType: "json",
                        async: false,
                        success: function (dataReturn) {
                            $("#data_event").html(dataReturn);
                        }
                    });
                };
                this.filtraGrafico = function () {
                    if (this.validate('evento')) {
                        $.ajax({
                            type: "POST",
                            data: {
                                "idProjeto": $("#idProjeto_event").val(),
                                "dt_ini": $("#data_inicial_evento").val(),
                                "dt_fin": $("#data_final_evento").val()
                            },
                            url: "/dashboard/reloadGraficoEvent/",
                            dataType: "json",
                            async: false,
                            success: function (dataReturn) {
                                $("#div-event-chart").html(dataReturn);
                            }
                        });
                    }
                };
                this.validate = function (tipo) {
                    var status = true;
                    var error = '';

                    if ($("#data_inicial_" + tipo).val() === "" && $("#data_final_" + tipo).val() === "") {
                        error += "Insira um periodo<br />";
                        status = false;
                    } else {
                        if ($("#data_inicial_" + tipo).val() === "") {
                            error += "Insira uma data inicial<br />";
                            status = false;
                        }
                        if ($("#data_final_" + tipo).val() === "") {
                            error += "Insira uma data final<br />";
                            status = false;
                        }
                    }

                    if (!status) {
                        showMessage(error);
                    }
                    return status;
                };
            };
            this.troca = new function () {
                this.reload = function () {
                    if (this.validate()) {
                        $.ajax({
                            type: "POST",
                            data: {
                                "idProjeto": $("#idProjeto_troca").val(),
                                "dt_ini": $("#data_inicial").val(),
                                "dt_fin": $("#data_final").val()
                            },
                            url: "/dashboard/reloadDataTroca/",
                            dataType: "json",
                            async: false,
                            success: function (dataReturn) {
                                $("#data_troca").html(dataReturn);
                            }
                        });
                    }
                };
                this.validate = function () {
                    var status = true;
                    var error = '';

                    if ($("#data_inicial").val() === "" && $("#data_final").val() === "") {
                        error += "Insira um periodo<br />";
                        status = false;
                    } else {
                        if ($("#data_inicial").val() === "") {
                            error += "Insira uma data inicial<br />";
                            status = false;
                        }
                        if ($("#data_final").val() === "") {
                            error += "Insira uma data final<br />";
                            status = false;
                        }
                    }

                    if (!status) {
                        showMessage(error);
                    }
                    return status;
                };
            };
        };
