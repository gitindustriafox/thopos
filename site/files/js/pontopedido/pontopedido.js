$(document).ready(function () {
    pontopedido.autocomplete.Parceiro();
    //marca e desmarca todos os checkbox
    $('#selecionar_tudo').click(function () {
        if (this.checked === true) {
            $("#listagem_pp").find('input[type=checkbox]').each(function () {
                this.checked = true;
            });
        } else {
            $("#listagem_pp").find('input[type=checkbox]').each(function () {
                this.checked = false;
            });
        }
    });

    $("#frm-busca-pontopedido [type=submit]").click(function(){
        if ($(this).hasClass('buscar')){
            $("#frm-busca-pontopedido").attr('action', '/pontopedido/busca_pontopedido');
            $("#frm-busca-pontopedido").submit();
        } else {
            if (pontopedido.consisteCriarSolicitacao()) {
                $("#frm-busca-pontopedido").attr('action', '/pontopedido/busca_criarsolicitacao');
                $("#frm-busca-pontopedido").submit();
            } else {
                $("#frm-busca-pontopedido").attr('action', '');
            }          
        }
    });
    
    $.datepicker.setDefaults({
        defaultDate: null,
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd/mm/yy"
    });
    $("#dtMovimento").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtMovimento").val("");
        }
    });
    $("#vlMovimento").priceFormat({
        prefix: "",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    });    
    $('.valor').priceFormat({
        prefix: "",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    });
             
});
$(function () {
    pedido.autocomplete.Parceiro();
});

var pontopedido = new function () {
    this.autocomplete = new function () {
        this.Parceiro = function () {
            $('.complete_Parceiro').autocomplete({
                source: "/pedido/getParceiro",
                minLength: 3,
                select: function (event, ui) {
                    var retorno = $(this).attr('data-label');
                    $('#idParceiro').val(ui.item.id);
                    $("#Parceiro").val(ui.item.value);
                }
            });
            $(".complete_Parceiro").bind("blur keyup keydow keypress", function () {
                if ($("#Parceiro" + $(this).attr("data-label")).val() === "") {
                    $('#idParceiro' + $(this).attr("data-label")).val("");
                }
            }); 
        };
    }
    this.editar = function (idInsumo, idLocalEstoque) {
        $("#qtde" + idInsumo + idLocalEstoque).removeAttr('disabled');
    };
    
    this.consisteCriarSolicitacao = function () {
        var stats = false;
        var errorText = 'Favor marcar pelo menos uma opção e colocar a quantidade!!!';                    

        $("#listagem_pp").find('input[type=checkbox]').each(function () {
            if(this.checked === true) {
                var nada = this.id.substring(10,30);
                var conteudo = document.getElementById("qtde" + nada).value;
                var novoValor = conteudo.replace(".","");
                var novoValor1 = novoValor.replace(",",".");
                var valor = parseFloat(novoValor1);
                if (valor>0) {
                    stats = true;
                    errorText = '';
                }
            }
        });
        
        if ($('#dsParceiro').val() == '') {
            var stats = false;
            var errorText = 'Favor digitar um fornecedor!!!';                    
        }
        if ($('#idCentroCusto').val() == '') {
            var stats = false;
            var errorText = 'Favor escolher um centro de custo!!!';                    
        }
        if ($('#idPrioridade').val() == '') {
            var stats = false;
            var errorText = 'Favor escolher uma prioridade!!!';                    
        }
        if ($('#idGrupoDR').val() == '') {
            var stats = false;
            var errorText = 'Favor escolher um grupo de despesa!!!';                    
        }
        if ($('#idItemDR').val() == '') {
            var stats = false;
            var errorText = 'Favor escolher um item de despesa!!!';                    
        }
        
        if (errorText) {
            showMessage(errorText);        
        };
        
        return stats;        
    };        
}; 