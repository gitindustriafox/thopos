$(document).ready(function() {
    $("#frm-busca-insumo [type=submit]").click(function(){
        if ($(this).hasClass('buscar')){
            $("#frm-busca-insumo").attr('action', '/listaInsumos/busca_insumo_cadastro');
            $("#frm-busca-insumo").submit();
        } else {
        if ($(this).hasClass('csv')){
            $("#frm-busca-insumo").attr('action', '/listaInsumos/busca_criarcsv');
            $("#frm-busca-insumo").submit();
        }     
        }     
    });
});
var insumo = new function () {
    this.closejanela = function () {
        $('#insumofotos_show').modal('hide');
    }    
    
    this.atualizagrupo = function (idInsumo) {
        $.ajax({
            url: "/insumo/atualizagrupo",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idInsumo": idInsumo,
                "idGrupo": $('#grupo' + idInsumo).val()
            },
            success: function (dataReturn) {
            }
        });        
    }
    
    this.atualizaunidade = function (idInsumo) {
        $.ajax({
            url: "/insumo/atualizaunidade",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idInsumo": idInsumo,
                "idUnidade": $('#unidade' + idInsumo).val()
            },
            success: function (dataReturn) {
            }
        });
        
    }

    this.selecionarfoto=function(idInsumo) {
        jQuery.ajax({
            async: false,
            type: "post",
            dataType: "html",
            url: "/insumo/importarfotos",
            data: {
                "idInsumo": idInsumo,
                "dsTabela": 'prodInsumo'
            },
            complete: function (event, XMLHttpRequest) {
                if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                    try {
                        $('#insumofotos_show').each(function () {
                            $('.modal-body', this).html(event.responseText);
                            $(this).modal('show');
                        });
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        });
    };
 };