$(document).ready(function() {
    LancamentoContabil.autocomplete.contaDebito();
    LancamentoContabil.autocomplete.contaCredito();
    
    $.datepicker.setDefaults({
        defaultDate: null,
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd/mm/yy"
    });
    $("#dtInicial").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtInicial").val("");
        }
    });
    $("#dtFinal").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtFinal").val("");
        }
    });
    $("#dtLancamento").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtLancamento").val("");
        }
    });
    $("#vlLancamento").priceFormat({
        prefix: "",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    });
    $("#vlLancamentoDebito").priceFormat({
        prefix: "",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    });
    $("#vlLancamentoCredito").priceFormat({
        prefix: "",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    });
    
});

$(function () {
    LancamentoContabil.autocomplete.contaDebito();
    LancamentoContabil.autocomplete.contaCredito();
});

var LancamentoContabil = new function () {    
    this.autocomplete = new function () {
        this.contaDebito = function () {
            $('.complete_contaDebito').autocomplete({
                source: "/lancamentosContabeis/getConta/dtLancamento/" + $('#dtLancamento').val(),
                minLength: 3,
                select: function (event, ui) {
                    var retorno = $(this).attr('data-label');
                    $('#idContaDebito').val(ui.item.id);
                    $("#dsContaDebito").val(ui.item.value);
                }
            });
            $(".complete_contaDebito").bind("blur keyup keydow keypress", function () {
                if ($("#dsContaDebito" + $(this).attr("data-label")).val() === "") {
                    $('#idContaDebito' + $(this).attr("data-label")).val("");
                }
            }); 
        };
        this.contaCredito = function () {
            $('.complete_contaCredito').autocomplete({
                source: "/lancamentosContabeis/getConta/dtLancamento/" + $('#dtLancamento').val(),
                minLength: 3,
                select: function (event, ui) {
                    var retorno = $(this).attr('data-label');
                    $('#idContaCredito').val(ui.item.id);
                    $("#dsContaCredito").val(ui.item.value);
                }
            });
            $(".complete_contaCredito").bind("blur keyup keydow keypress", function () {
                if ($("#dsContaCredito" + $(this).attr("data-label")).val() === "") {
                    $('#idContaCredito' + $(this).attr("data-label")).val("");
                }
            }); 
        };
    };
    
    this.gravarlancamentodebito = function () {
        if (this.validalancamentodebito()) {
            $.ajax({
                url: "/lancamentosContabeis/gravar_lancamentodebito",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idLancamento": $('#idLancamento').val(),
                    "idContaDebito": $('#idContaDebito').val(),
                    "dsHistoricoDebito": $('#dsHistoricoDebito').val(),
                    "vlLancamentoDebito": $('#vlLancamentoDebito').val()
                },
                success: function (dataReturn) {
                    location.reload();                                         
                }
            });        
        }
    };
    
    this.gravarlancamentocredito = function () {
        if (this.validalancamentocredito()) {
            $.ajax({
                url: "/lancamentosContabeis/gravar_lancamentocredito",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idLancamento": $('#idLancamento').val(),
                    "idContaCredito": $('#idContaCredito').val(),
                    "dsHistoricoCredito": $('#dsHistoricoCredito').val(),
                    "vlLancamentoCredito": $('#vlLancamentoCredito').val()
                },
                success: function (dataReturn) {
                    location.reload();                                         
                }
            });        
        }
    };
    
    this.showipc = function () {
        jQuery.ajax({
            async: false,
            type: "post",
            dataType: "html",
            url: "/LancamentoContabil/importarLancamentoContabil_mostrar",
            data: {
                "idPeriodo": $('#idPeriodo').val()
            },
            complete: function (event, XMLHttpRequest) {
                if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                    try {
                        $('#importarLancamentoContabil_show').each(function () {
                            $('.modal-body', this).html(event.responseText);
                            $(this).modal('show');
                        });
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        });
    };
    
    this.lerConta = function () {
        $.ajax({
            url: "/LancamentoContabil/ler_conta",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idPeriodo": $('#idPeriodo').val(),
                "idContaReduzida": $('#idContaReduzida').val()
            },
            success: function (dataReturn) {
                if (dataReturn.saldo) {
                    $('#vlSaldoInicial').val(dataReturn.saldo);  
                    $('#vlSaldoInicial').attr('readonly','readonly');
                    $('#btnGravar').focus();
                } else {
                    $('#vlSaldoInicial').removeAttr('readonly');
                }
            }
        });        
    }
    
    this.delLancamento = function (idPeriodo, idConta) {
        if (confirm('DESEJA EXCLUIR ESTE REGISTRO?')) {
            $.ajax({
                url: "/lancamentosContabeis/del_lancamento",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idPeriodo": idPeriodo,
                    "idConta": idConta
                },
                success: function (dataReturn) {
                    location.reload();                     
                }
            });        
        }
    }
    
    this.delLancamentoDebito = function (idLancamentoDebito) {
        if (confirm('DESEJA EXCLUIR ESTE REGISTRO DE DEBITO?')) {
            $.ajax({
                url: "/lancamentosContabeis/del_lancamentodebito",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idLancamentoDebito": idLancamentoDebito
                },
                success: function (dataReturn) {
                    location.reload();                     
                }
            });        
        }
    }
    
    this.delLancamentoCredito = function (idLancamentoCredito) {
        if (confirm('DESEJA EXCLUIR ESTE REGISTRO DE CREDITO?')) {
            $.ajax({
                url: "/lancamentosContabeis/del_lancamentocredito",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idLancamentoCredito": idLancamentoCredito
                },
                success: function (dataReturn) {
                    location.reload();                     
                }
            });        
        }
    }
    
    this.editarconta = function (idPeriodo, idConta, idContaReduzida, dsConta, stTipoConta, vlSaldoInicial, idCE1, idCE2, idCE3, idCE4, idCE5) {
        $('#idConta').val(idConta);
        $('#idContaReduzida').val(idContaReduzida);
        $('#dsConta').val(dsConta);
        $('#stTipoConta').val(stTipoConta);
        $('#vlSaldoInicial').val(vlSaldoInicial);
        $('#idCE1').val(idCE1);
        $('#idCE2').val(idCE2);
        $('#idCE3').val(idCE3);
        $('#idCE4').val(idCE4);
        $('#idCE5').val(idCE5);
        
        $('#idContaReduzida').attr('readonly','readonly');

        $('#vlSaldoInicial').removeAttr('readonly');
        
        $('#idCE1').focus();        
    }
    
    this.validaFormulario = function() {
        var err = true;
        if ($('#dsHistorico').val() == '') {
            showMessage('Favor digitar o Historico');
            err = false;
        }
        if ($('#dtLancamento').val() == '') {
            showMessage('Favor digitar a data do lancamento');        
            err = false;
        } else {
            $.ajax({
                url: "/lancamentosContabeis/verPeriodo",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "dtLancamento": $('#dtLancamento').val()
                },
                success: function (dataReturn) {
                    if (dataReturn.msg) {
                        showMessage('Data fora do período contabil válido');        
                        err = false;
                    }
                }
            });                    
        }
        if ($('#vlLancamento').val() == '' || $('#vlLancamento').val() == '0,00') {
            showMessage('Favor digitar o valor do lancamento');        
            err = false;
        }
        return err;
    }    
    
    this.validalancamentodebito = function() {
        var err = true;
        if ($('#idContaDebito').val() == '') {
            showMessage('Favor digitar a conta debito');
            err = false;
        }

//        if ($('#dsHistoricoDebito').val() == '') {
//            showMessage('Favor digitar o Historico do lancamento debito');
//            err = false;
//        }
//
        if ($('#vlLancamentoDebito').val() == '' || $('#vlLancamentoDebito').val() == '0,00') {
            showMessage('Favor digitar o valor do lancamento debito');        
            err = false;
        }
        
        return err;
    }    
    
    this.validalancamentocredito = function() {
        var err = true;
        if ($('#idContaCredito').val() == '') {
            showMessage('Favor digitar a conta credito');
            err = false;
        }

//        if ($('#dsHistoricoCredito').val() == '') {
//            showMessage('Favor digitar o Historico do lancamento credito');
//            err = false;
//        }
//
        if ($('#vlLancamentoCredito').val() == '' || $('#vlLancamentoCredito').val() == '0,00') {
            showMessage('Favor digitar o valor do lancamento credito');        
            err = false;
        }
        
        return err;
    }    
};