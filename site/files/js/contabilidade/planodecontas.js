$(document).ready(function() {
    $.datepicker.setDefaults({
        defaultDate: null,
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd/mm/yy"
    });
    $("#dtInicial").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtInicial").val("");
        }
    });
    $("#dtFinal").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtFinal").val("");
        }
    });
//    $("#vlSaldoInicial").priceFormat({
//        prefix: "",
//        centsSeparator: ',',
//        thousandsSeparator: '.',
//        limit: 10,
//        centsLimit: 2,
//        allowNegative: true
//    });
    
});

$(function () {
    $("#vlSaldoInicial").priceFormat({
        prefix: "",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    });
});

var planodecontas = new function () {
    this.showipc = function () {
        jQuery.ajax({
            async: false,
            type: "post",
            dataType: "html",
            url: "/planodecontas/importarplanodecontas_mostrar",
            data: {
                "idPeriodo": $('#idPeriodo').val()
            },
            complete: function (event, XMLHttpRequest) {
                if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                    try {
                        $('#importarplanodecontas_show').each(function () {
                            $('.modal-body', this).html(event.responseText);
                            $(this).modal('show');
                        });
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        });
    };
    
    this.lerConta = function () {
        $.ajax({
            url: "/planodecontas/ler_conta",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idPeriodo": $('#idPeriodo').val(),
                "idContaReduzida": $('#idContaReduzida').val()
            },
            success: function (dataReturn) {
                if (dataReturn.saldo) {
                    $('#vlSaldoInicial').val(dataReturn.saldo);  
                    $('#vlSaldoInicial').attr('readonly','readonly');
                    $('#btnGravar').focus();
                } else {
                    $('#vlSaldoInicial').removeAttr('readonly');
                }
            }
        });        
    }
    
    this.delconta = function (idPeriodo, idConta) {
        if (confirm('DESEJA EXCLUIR ESTE REGISTRO?')) {
            $.ajax({
                url: "/planodecontas/del_conta",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idPeriodo": idPeriodo,
                    "idConta": idConta
                },
                success: function (dataReturn) {
                    location.reload();                     
                }
            });        
        }
    }
    
    this.editarconta = function (idPeriodo, idConta, idContaReduzida, dsConta, stTipoConta, vlSaldoInicial, idCE1, idCE2, idCE3, idCE4, idCE5) {
        $('#idConta').val(idConta);
        $('#idContaReduzida').val(idContaReduzida);
        $('#dsConta').val(dsConta);
        $('#stTipoConta').val(stTipoConta);
        $('#vlSaldoInicial').val(vlSaldoInicial);
        $('#idCE1').val(idCE1);
        $('#idCE2').val(idCE2);
        $('#idCE3').val(idCE3);
        $('#idCE4').val(idCE4);
        $('#idCE5').val(idCE5);
        
//        $('#vlSaldoInicial').attr('readonly','readonly');
//        $('#stTipoConta').attr('readonly','readonly');
//        $('#dsConta').attr('readonly','readonly');
//        $('#idCE1').attr('readonly','readonly');
//        $('#idCE2').attr('readonly','readonly');
//        $('#idCE3').attr('readonly','readonly');
//        $('#idCE4').attr('readonly','readonly');
//        $('#idCE5').attr('readonly','readonly');
        $('#idContaReduzida').attr('readonly','readonly');

        $('#vlSaldoInicial').removeAttr('readonly');
        
        $('#idCE1').focus();        
    }
    
    this.validaFormulario = function() {
        var err = true;
        if ($('#idContaReduzida').val() == '') {
            showMessage('Favor digitar a conta reduzida');
            err = false;
        }
        if ($('#idCE1').val() == '' || $('#idCE1').val() == 0) {
            showMessage('Favor digitar a conta estruturada nível 1');        
            err = false;
        }
        if ($('#idCE2').val() == '') {
            showMessage('Favor digitar a conta estruturada nível 2');        
            err = false;
        }
        if ($('#idCE3').val() == '') {
            showMessage('Favor digitar a conta estruturada nível 3');        
            err = false;
        }
        if ($('#idCE4').val() == '') {
            showMessage('Favor digitar a conta estruturada nível 4');        
            err = false;
        }
        if ($('#idCE5').val() == '') {
            showMessage('Favor digitar a conta estruturada nível 5');        
            err = false;
        }

        if ($('#dsConta').val() == '') {
            showMessage('Favor digitar a descricao da conta');        
            err = false;
        }
        
        if (!($('#stTipoConta').val() == 'A' || $('#stTipoConta').val() == 'S')) {
            showMessage('Favor digitar A para analitico ou S para sintérico');        
            err = false;
        }        
       
        return err;
    }    
};