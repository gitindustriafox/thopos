$(document).ready(function() {
    $.datepicker.setDefaults({
        defaultDate: null,
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd/mm/yy"
    });
    $("#dtInicial").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtInicial").val("");
        }
    });
    $("#dtFinal").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtFinal").val("");
        }
    });
});

var periodoContabil = new function () {
    this.validaFormulario = function() {
        var err = true;
        if ($('#dsPeriodo').val() == '') {
            showMessage('Favor digitar a descricao do período');
            err = false;
        }
        if ($('#dtInicial').val() == '') {
            showMessage('Favor digitar a data inicial');        
            err = false;
        }

        if ($('#dtFinal').val() == '') {
            showMessage('Favor digitar a data final');        
            err = false;
        }
        if (err) {
            var dtInicio = $('#dtInicial').val();
            var dtFim = $('#dtFinal').val();

            dtInicio = (dtInicio.substr(6,4) +  dtInicio.substr(3,2) + dtInicio.substr(0,2)).toString();
            dtFim = (dtFim.substr(6,4) +  dtFim.substr(3,2) + dtFim.substr(0,2)).toString();
            if (dtFim < dtInicio) {
                showMessage('A data final deve ser maior que a data inicial');        
                err = false;
            }
        }
        
        return err;
    }    
};