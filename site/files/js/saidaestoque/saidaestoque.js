
$(document).ready(function() {
    saidaestoque.autocomplete.parceiro();
    saidaestoque.autocomplete.produto();
    
    $.datepicker.setDefaults({
        defaultDate: null,
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd/mm/yy"
    });
    $("#dtMovimento").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtMovimento").val("");
        }
    });
    $("#vlMovimento").priceFormat({
        prefix: "",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    });
    $("#qtMovimento").priceFormat({
        prefix: "",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    });
        
});

$(function () {
    saidaestoque.autocomplete.parceiro();
    saidaestoque.autocomplete.produto();
});

var saidaestoque = new function () {
    this.autocomplete = new function () {
        this.parceiro = function () {
            $('.complete_parceiro').autocomplete({
                source: "/saidaestoque/getParceiro",
                minLength: 3,
                select: function (event, ui) {
                    var retorno = $(this).attr('data-label');
                    $('#idParceiro').val(ui.item.id);
                    $("#parceiro").val(ui.item.value);
                }
            });
            $(".complete_parceiro").bind("blur keyup keydow keypress", function () {
                if ($("#parceiro" + $(this).attr("data-label")).val() === "") {
                    $('#idParceiro' + $(this).attr("data-label")).val("");
                }
            }); 
        };
        this.produto = function () {
            $('.complete_produto').autocomplete({
                source: "/saidaestoque/getProduto/insumo/",
                minLength: 3,
                select: function (event, ui) {
                    var retorno = $(this).attr('data-label');
                    $('#idInsumo').val(ui.item.id);
                    $("#insumo").val(ui.item.value);
                }
            });
            $(".complete_produto").bind("blur keyup keydow keypress", function () {
                if ($("#insumo" + $(this).attr("data-label")).val() === "") {
                    $('#idInsumo' + $(this).attr("data-label")).val("");
                }
            }); 
        };
    };
    
    this.gravarcabecalho = function () {
        if (verificaerrosC()) {                
            $.ajax({
                url: "/saidaestoque/gravar_movimento",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idMovimento": $("#idMovimento").val(),
                    "idTipoMovimento": $("#idTipoMovimento").val(),
                    "idParceiro": $("#idParceiro").val(),
                    "idColaborador": $("#idColaborador").val(),
                    "dtMovimento": $("#dtMovimento").val(),
                    "nrNota": $("#nrNota").val(),
                    "nrPedido": $("#nrPedido").val(),
                    "dsObservacao": $("#dsObservacao").val()
                },
                success: function (dataReturn) {
                 //   $("#idMovimento").val(dataReturn.idMovimento);
                    location.reload(); 
                }
            });
        }
    };
    this.gravaritem = function () {
        if (verificaerrosI()) {        
            $.ajax({
                url: "/saidaestoque/gravar_item",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idMovimento": $("#idMovimento").val(),
                    "dtMovimento": $("#dtMovimento").val(),
                    "idTipoMovimento": $("#idTipoMovimento").val(),
                    "idLocalEstoque": $("#idLocalEstoque").val(),
                    "idLocalEstoqueEntrada": $("#idLocalEstoqueEntrada").val(),
                    "idParceiro": $("#idParceiro").val(),
                    "idColaborador": $("#idColaborador").val(),
                    "idInsumo": $("#idInsumo").val(),
                    "idCentroCusto": $("#idCentroCusto").val(),
                    "idSetor": $("#idSetor").val(),
                    "idOS": $("#idOS").val(),
                    "idMotivo": $("#idMotivo").val(),
                    "idMaquina": $("#idMaquina").val(),
                    "qtMovimento": $("#qtMovimento").val(),
                    "vlMovimento": $("#vlMovimento").val(),
                    "nrNota": $("#nrNota").val(),
                    "nrPedido": $("#nrPedido").val(),
                    "dsObservacao": $("#dsObservacaoItem").val()
                },
                success: function (dataReturn) {
                    $("#idMovimento").val(dataReturn.idMovimento);
                    $("#idMovimentoItem").val(dataReturn.idMovimentoItem);
                    $("#idLocalEstoque").val('');
                    $("#idLocalEstoqueEntrada").val('');
                    $("#idInsumo").val('');
                    $("#qtMovimento").val(0);
                    $("#vlMovimento").val(0);
                    $("#dsObservacaoItem").val('');
                    location.reload(); // '/saidaestoque/novo_saidaestoque/idMovimento/' . dataReturn.idMovimento
                }
            });
        }
    };
    
    this.estornarmovimentoitem = function (idMovimentoItem) {
        if (confirm('DESEJA REALMENTE ESTORNAR ESTE PRODUTO DESTE MOVIMENTO?')) {        
            $.ajax({
                url: "/saidaestoque/estornar",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idMovimentoItem": idMovimentoItem
                },
                success: function (dataReturn) {
                    location.reload(); 
                }
            });
        };
    };
    
    this.carregalocalestoque = function () {
        $.ajax({
            url: "/saidaestoque/lerlocalestoque",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idInsumo": $("#idInsumo").val()
            },
            success: function (dataReturn) {
                $("#idLocalEstoque").html(dataReturn.html);
            }
        });
    };
    
    this.lerunidade = function () {
        $.ajax({
            url: "/saidaestoque/lerunidade",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idInsumo": $("#idInsumo").val(),
                "idLocalEstoque": $("#idLocalEstoque").val()
            },
            success: function (dataReturn) {
                $("#dsUnidade").val(dataReturn.dsUnidade);
                $("#qtEstoque").val(dataReturn.qtEstoque);
                $("#vlUltimaCompra").val(dataReturn.vlUltimaCompra);
            }
        });
    };
    
    
    this.calcularqtde = function () {
        var novaqtde = $("#qtMovimento").val();
        var novaqtde1 = novaqtde.replace(".","");
        var novaqtde2 = novaqtde1.replace(",",".");
        
        var novovalor = $("#vlUltimaCompra").val();
        novovalor = parseFloat(novovalor);
        var valorSaida = (novovalor * novaqtde2);

        valorSaida = valorSaida.toFixed(2);
        var valorFinal = valorSaida.toString().replace(".", ",")
        $("#vlMovimento").val(valorFinal);
        $("#vlMovimento").focus();
    };
    
    this.desabilitaid = function() {
        $.ajax({
            url: "/saidaestoque/desabilitaid",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idMovimento": $("#idMovimento").val()
            },
            success: function () {
                location.reload('/dashboard');  
            }
        });
        return true;
    };
    this.delmovimentoitem = function(item) {
        $.ajax({
            url: "/saidaestoque/delmovimentoitem",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idMovimentoItem": item
            },
            success: function (dataReturn) {
                location.reload();  
            }
        });
    };
    this.editamovimentoitem = function(cdInsumo, dsInsumo, localEstoque, dsLocalEstoque, idLocalEstoque, idMovimentoItem,idCentroCusto,dsCentroCusto,idSetor, dsObservacaoItem, qtMovimento, valorPME, vlMovimento) {
        $('#cdInsumo').val(cdInsumo);
        $('#insumo').val(dsInsumo);
        $('#idLocalEstoque').val(idLocalEstoque);
        $('#dsLocalEstoque').val(dsLocalEstoque);
        $('#idMovimentoItem').val(idMovimentoItem);
        $('#idCentroCusto').val(idCentroCusto);
        $('#idSetor').val(idSetor);
        $('#dsCentroCusto').val(dsCentroCusto);        
        $('#dsObservacaoItem').val(dsObservacaoItem);        
        $('#qtMovimento').val(qtMovimento);        
        $('#vlMovimento').val(vlMovimento);        
    };
    
};
verificaerrosC = function() {
    var err = true;
    if ($('#idTipoMovimento').val() == '') {
        showMessage('Favor escolher o tipo de movimento');        
        err = false;
    }
//    if ($('#idParceiro').val() == '') {
//        showMessage('Favor escolher um parceiro');        
//        err = false;
//    }
    return err;
}

verificaerrosI = function() {
    var err = true;
    if ($('#idLocalEstoque').val() == '') {
        showMessage('Favor escolher um Local de Estoque da saída do Produto');        
        err = false;
    }
    if ($('#idInsumo').val() == '') {
        showMessage('Favor escolher o produto');        
        err = false;
    }
    if ($('#qtMovimento').val() == '' || $('#qtMovimento').val() == '0,00') {
        showMessage('Favor digitar a quantidade da saida');
        err = false;
    }
    if ($('#vlMovimento').val() == '' || $('#vlMovimento').val() == '0,00') {
        showMessage('Favor digitar o valor da saída');
        err = false;
    }
    if ($('#idCentroCusto').val() == '') {
        showMessage('Favor escolher um Centro de Custo da saída do Produto');        
        err = false;
    }
    if ($('#idTipoMovimento').val() == 11) {
        if ($('#idLocalEstoqueEntrada').val() == '') {
            showMessage('Favor escolher um Local de Estoque para o destino do Produto para o caso de transferencia entre locais');        
            err = false;
        }
    }
    
    return err;
}