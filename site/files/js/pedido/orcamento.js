$(document).ready(function() {
    orcamento.autocomplete.Parceiro();
});

$(function () {
    orcamento.autocomplete.Parceiro();
});

var orcamento = new function () {
    this.autocomplete = new function () {
        this.Parceiro = function () {
            $('.complete_ParceiroO').autocomplete({
                source: "/pedido/getParceiro",
                minLength: 3,
                select: function (event, ui) {
                    var retorno = $(this).attr('data-label');
                    $('#idParceiroO').val(ui.item.id);
                    $("#dsParceiroO").val(ui.item.value);
                }
            });
            $(".complete_ParceiroO").bind("blur keyup keydow keypress", function () {
                if ($("#dsParceiroO" + $(this).attr("data-label")).val() === "") {
                    $('#idParceiroO' + $(this).attr("data-label")).val("");
                }
            }); 
        };
    };    
}