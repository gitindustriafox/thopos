
$(document).ready(function() {
    pedido.autocomplete.Parceiro();
    pedido.autocomplete.produto();    
    
    $.datepicker.setDefaults({
        defaultDate: null,
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd/mm/yy"
    });
    $("#dtPrazoEntrega").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtPrazoEntrega").val("");
        }
    });
    $("#dtVencimento").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtVencimento").val("");
        }
    });
    $("#dtFim").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtFim").val("");
        }
    });
    $("#dtInicio").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtInicio").val("");
        }
    });
    $("#qtPedido").priceFormat({
        prefix: "",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    });
    $("#vlPedido").priceFormat({
        prefix: "",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    });
    $("#valor").priceFormat({
        prefix: "",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    });

    $("#frm-busca-pedido-fechado [type=submit]").click(function(){
        if ($(this).hasClass('buscar')){
            $("#frm-busca-pedido-fechado").attr('action', '/pedidofechado/busca_pedido');
            $("#frm-busca-pedido-fechado").submit();
        } else {
        if ($(this).hasClass('csv')){
            $("#frm-busca-pedido-fechado").attr('action', '/pedidofechado/busca_criarcsv');
            $("#frm-busca-pedido-fechado").submit();
        } else {
            $("#frm-busca-pedido-fechado").attr('action', '');
        }}          
    });                
    $("#frm-busca-pedidosemaberto [type=submit]").click(function(){
        if ($(this).hasClass('buscar')){
            $("#frm-busca-pedidosemaberto").attr('action', '/pedidoaberto/busca_pedido');
            $("#frm-busca-pedidosemaberto").submit();
        } else {
        if ($(this).hasClass('csv')){
            $("#frm-busca-pedidosemaberto").attr('action', '/pedidoaberto/busca_criarcsv');
            $("#frm-busca-pedidosemaberto").submit();
        } else {
            $("#frm-busca-pedidosemaberto").attr('action', '');
        }}          
    });                
});

$(function () {
    pedido.autocomplete.Parceiro();
    pedido.autocomplete.produto();
    $("#valor").priceFormat({
        prefix: "",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    });
});

var pedido = new function () {
    this.autocomplete = new function () {
        this.Parceiro = function () {
            $('.complete_Parceiro').autocomplete({
                source: "/pedido/getParceiro",
                minLength: 3,
                select: function (event, ui) {
                    var retorno = $(this).attr('data-label');
                    $('#idParceiro').val(ui.item.id);
                    $("#Parceiro").val(ui.item.value);
                }
            });
            $(".complete_Parceiro").bind("blur keyup keydow keypress", function () {
                if ($("#Parceiro" + $(this).attr("data-label")).val() === "") {
                    $('#idParceiro' + $(this).attr("data-label")).val("");
                }
            }); 
        };
        this.produto = function () {
            if ($("#stTipoI").prop('checked')) {    
                tipo = "/pedido/getProduto/insumo/";
            } else {
                tipo = "/pedido/getProduto/servic/";
            }
            $('.complete_produto').autocomplete({
                source: tipo,
                minLength: 3,
                select: function (event, ui) {
                    var retorno = $(this).attr('data-label');
                    $('#idInsumo').val(ui.item.id);
                    $("#insumo").val(ui.item.value);
                }
            });
            $(".complete_produto").bind("blur keyup keydow keypress", function () {
                if ($("#insumo" + $(this).attr("data-label")).val() === "") {
                    $('#idInsumo' + $(this).attr("data-label")).val("");
                }
            }); 
        };
    };

    this.editarfinanceiro = function (parcela) {
        $("#dtVencimento-" + parcela).removeAttr('disabled');
        $("#vlParcela-" + parcela).removeAttr('disabled');
    };
    this.alterarfinanceiro = function (item, nrParcela) {
        $.ajax({
            url: "/pedido/alterar_financeiro",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idFinanceiroParcela": item,
                "dtVencimento": $("#dtVencimento-" + nrParcela).val(),
                "vlParcela": $("#vlParcela-" + nrParcela).val()
            },
            success: function (dataReturn) {
                location.reload(); 
            }
        });
    };    
    
    this.lerbudgetsetor = function () {
        $.ajax({
            url: "/pedido/lerbudgetsetor",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idPedido": $('#idPedidoEmitir').val(),
                "idSetor": $('#idSetor').val(),
                "dtVencimento": $("#dtVencimento").val(),
            },
            success: function (dataReturn) {
                $("#mostrarvaloresbudget").html(dataReturn.html);                
                if (dataReturn.ativo == 0) {
                    $('#btncriar').removeAttr('disabled');
                } else {
                    if (dataReturn.vlSaldo < 0) {
                        $('#btncriar').attr('disabled','disabled');
                    } else {
                        $('#btncriar').removeAttr('disabled');
                    }
                }
            }
        });
    };    
    
    this.escolherIS = function () {
       $('#insumo').val('');
       $('#idInsumo').val('');        
        if ($("#stTipoI").prop('checked')) {
            document.getElementById("labelproduto").innerHTML =
              "<label for='linsumo'>PRODUTO</label>";
//           this.atualizaprodutos('0');            
        } else {
            document.getElementById("labelproduto").innerHTML =
              "<label for='linsumo'>SERVICO</label>";
//           this.atualizaprodutos('1');
        };
        pedido.autocomplete.produto();   
       $('#insumo').focus();
        
    };
        
    this.lersolicitacao = function () {
        $.ajax({
            url: "/pedido/lersolicitacao",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idSolicitacao": $("#idSolicitacao").val(),
                "idSolicitacaoItem": $("#idSolicitacaoItem").val(),
                "opcao": '0'
            },
            success: function (dataReturn) {
                $("#idPedido").val(dataReturn.idPedido);
                location.reload(); 
            }
        });        
    };

    this.lerempresa = function () {
        $.ajax({
            url: "/pedido/lerempresa",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idEmpresa": $("#idEmpresa").val()
            },
            success: function (dataReturn) {
                $("#dsLocalEntrega1").val(dataReturn.dsLocalEntrega);
            }
        });        
    };

    this.editar = function(item, opcao) {
        if (opcao == '0') {   // produtos
            document.getElementById("stTipoI").checked = true;                    
        } else {
            document.getElementById("stTipoS").checked = true;                    
        }                
        this.escolherIS();                
        
        $.ajax({
            url: "/pedido/ler_dados",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idPedidoItem": item,
                "opcao": opcao                
            },
            success: function (dataReturn) {
                // limpar os dados antes                
                $("#idInsumo").val('');
                $("#dsUnidade").val('');
                $("#qtEstoque").val('');
                $("#qtPedido").val('');
                $("#vlPedido").val('');
                $("#dsObservacaoItem").val('');
                $("#idLocalEstoque").val('');
                $("#idCentroCusto").val('');
                $("#idGrupoDR").val('');
                $("#idItemDR").val('');
                $("#dsJustificativa").val('');                
                
                // carregar os dados
                $("#idPedidoItem").val(dataReturn.dados.idPedidoItem);
                $("#idInsumo").val(dataReturn.dados.idInsumo);
                if (dataReturn.dados.stTipoIS == 0) {
                    $("#insumo").val(dataReturn.dados.dsInsumo);
                } else {
                    $("#insumo").val(dataReturn.dados.dsServico);                    
                }
                $("#dsUnidade").val(dataReturn.dados.dsUnidade);
                $("#qtEstoque").val(dataReturn.dados.qtEstoque);
//                $("#qtPedido").val(dataReturn.qtPedido);
//                $("#vlPedido").val(dataReturn.vlPedido);
                $("#dsObservacaoItem").val(dataReturn.dados.dsObservacao);
                $("#idLocalEstoque").val(dataReturn.dados.idLocalEstoque);
                $("#idCentroCusto").val(dataReturn.dados.idCentroCusto);
                $("#idGrupoDR").val(dataReturn.dados.idGrupoDR);
                $("#dsJustificativa").val(dataReturn.dados.dsJustificativa);
                $('#idItemDR').html(dataReturn.html);
                $("#idItemDR").val(dataReturn.dados.idItemDR);
                
                var novoQtde = dataReturn.dados.qtPedido.replace(".",",");
                $("#qtPedido").val(novoQtde);
                var novoQtde = dataReturn.dados.vlPedido.replace(".",",");
                $("#vlPedido").val(novoQtde);
                
            }
        });
    }

    this.gravarcabecalho = function () {
        if (verificaerrosC()) {
            $.ajax({
                url: "/pedido/gravar_pedido",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idPedido": $("#idPedido").val(),
                    "idParceiro": $("#idParceiro").val(),
                    "idUsuarioSolicitante": $("#idUsuarioSolicitante").val(),
                    "dtPedido": $("#dtPedido").val(),
                    "nrPedido": $("#nrPedido").val(),
                    "dsObservacao": $("#dsObservacao").val(),
                    "dsSolicitante": $("#dsSolicitante").val(),
                    "dtPrazoEntrega": $("#dtPrazoEntrega").val(),
                    "idPrioridade": $("#idPrioridade").val(),
                    "dsComplementoEntrega": $("#dsComplementoEntrega").val()
                },
                success: function (dataReturn) {
                    $("#idPedido").val(dataReturn.idPedido);
                    location.reload(); 
                }
            });
        }
    };
    
    this.atualizaParceiroes = function () {
        $.ajax({
            url: "/pedido/atualiza_Parceiroes",
            dataType: "json",
            async: false,
            type: "POST",
            success: function (dataReturn) {
                $("#idParceiro").html(dataReturn.html);
            }
        });
    };
    
    this.atualizaprodutos = function (opcao) {
        if ($("#stTipoI").prop('checked')) {
           opcao = '0';
        } else {
           opcao = '1';
        }
        
        $.ajax({
            url: "/pedido/atualiza_produtos",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "opcao": opcao,
            },            
            success: function (dataReturn) {
                $("#idInsumo").html(dataReturn.html);
            }
        });
    };
    
    
    this.gravaritem = function () {
        if (verificaerrosI()) {        
            if ($("#stTipoI").prop('checked')) {
               opcao = '0';
            } else {
               opcao = '1';
            }

            $.ajax({
                url: "/pedido/gravar_item",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idPedido": $("#idPedido").val(),
                    "opcao": opcao,
                    "idPedidoItem": $("#idPedidoItem").val(),
                    "idParceiro": $("#idParceiro").val(),
                    "idInsumo": $("#idInsumo").val(),
                    "idLocalEstoque": $("#idLocalEstoque").val(),
                    "idCentroCusto": $("#idCentroCusto").val(),
                    "idItemDR": $("#idItemDR").val(),
                    "dsJustificativa": $("#dsJustificativa").val(),
                    "idGrupoDR": $("#idGrupoDR").val(),
                    "qtPedido": $("#qtPedido").val(),
                    "vlPedido": $("#vlPedido").val(),
                    "dsObservacaoItem": $("#dsObservacaoItem").val()
                },
                success: function (dataReturn) {
                    $("#idPedido").val(dataReturn.idPedido);
                    $("#idPedidoItem").val(dataReturn.idPedidoItem);
                    $("#idInsumo").val('');
                    $("#qtPedido").val(0);
                    $("#vlPedido").val(0);
                    $("#dsObservacaoItem").val('');
                    $("#idCentroCusto").val('');
                    $("#idItemDR").val('');
                    $("#dsJustificativa").val('');
                    $("#idGrupoDR").val('');
                    location.reload(); // '/pedido/novo_pedido/idPedido/' . dataReturn.idPedido
                }
            });
        }
    };
    this.gravarfinanceiro = function () {
        $.ajax({
            url: "/pedido/gravar_financeiro",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idPedido": $("#idPedido").val(),
                "dtPrevisaoEntrega": $("#dtPrevisaoEntrega").val(),
                "dtPrimeiroVencimento": $("#dtPrimeiroVencimento").val(),
                "qtParcelas": $("#qtParcelas").val(),
                "dsObservacao": $("#dsObservacao").val()
            },
            success: function (dataReturn) {
                $("#idFinanceiro").val(dataReturn.idFinanceiro);
                location.reload('/pedido/financeiro'); 
            }
        });
    };
    this.novopedido = function () {
        $.ajax({
            url: "/pedido/novopedido",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idPedido": $("#idPedido").val()
            },
            success: function (dataReturn) {
                $("#idPedido").val(dataReturn.idPedido);   
                location.reload();
            }
        });
    };
    this.lerusuario = function () {
        $.ajax({
            url: "/pedido/lerusuario",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idUsuario": $("#idUsuarioSolicitante").val()
            },
            success: function (dataReturn) {
                $("#dsSolicitante").val((dataReturn.nomeusuario));
            }
        });
    };
    
    this.lerunidade = function () {
        $.ajax({
            url: "/pedido/lerunidade",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idInsumo": $("#idInsumo").val()
            },
            success: function (dataReturn) {
                $("#dsUnidade").val(dataReturn.dsUnidade);
                $("#qtEstoque").val(dataReturn.qtEstoque);
            }
        });
    };
    this.desabilitaid = function() {
        $.ajax({
            url: "/pedido/desabilitaid",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idPedido": $("#idPedido").val()
            },
            success: function () {
                location.reload('/dashboard');  
            }
        });
        return true;
    };
    this.delpedidoitem = function(item) {
        $.ajax({
            url: "/pedido/delpedidoitem",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idPedidoItem": item
            },
            success: function (dataReturn) {
                location.reload();  
            }
        });
    };
    
    this.delfinanceiroitem = function(item) {
        $.ajax({
            url: "/pedido/delfinanceiroitem",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idFinanceiroParcela": item
            },
            success: function (dataReturn) {
                location.reload();  
            }
        });
    };
};
var emitir_pedido_compra = new function () {
    
    this.clear_log = function () {
        $('#pedidocompra_show').modal('hide');
    };
    
    this.enviar = function (idPedido) {
            $.ajax({
                url: "/pedido/enviarEmail",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idPedido": idPedido
                },
                success: function (dataReturn) {
//                    this.clear_log();
                }
            });
    };
    this.criar = function () {
        if (this.consisteEmitirPedidoCompra()) {
            $.ajax({
                url: "/pedido/emitir_pedido_compra",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idPedido": $('#idPedidoEmitir').val(),
                    "idParceiro": $('#idParceiro').val(),
                    "nrOrcamento": $('#nrOrcamento').val(),
                    "dsContato": $('#dsContato').val(),
                    "dsFone": $('#dsFone').val(),
                    "dsCelular": $('#dsCelular').val(),
                    "dsEmail": $('#dsEmail').val(),
                    "dsEndereco": $('#dsEndereco').val(),
                    "dsCidade": $('#dsCidade').val(),
                    "dsFrete": $('#dsFrete').val(),
                    "dsLocalEntrega": $('#dsLocalEntrega1').val(),
                    "idEmpresa": $('#idEmpresa').val(),
                    "idSetor": $('#idSetor').val(),
                    "cdCNPJ": $('#cdCNPJ').val(),
                    "dtPrazoEntrega": $('#dtPrazoEntrega').val(),
                    "dtVencimento": $('#dtVencimento').val(),
                    "dsCondicoesPagamento": $('#dsCondicoesPagamento').val(),
                    "dsObservacoes": $('#dsObservacoes').val(),
                },
                success: function (dataReturn) {
//                    var url = dataReturn.url;
//                    windows.open(url,'_blank');
//                    document.getElementById("botao_criar").innerHTML =
//                      "<a class='btn btn-primary' data-dismiss='modal' onclick='emitir_pedido_compra.criar();'>CRIAR PDF</a>";

//                    emitir_pedido_compra.clear_log();
                    location.reload();  
                    
//                      $('#listagem').html(dataReturn.html);  
                }
            });
        }
    };
    
    this.criarCotacao = function () {
        if (this.consisteEmitirPedidoCompra()) {
            $.ajax({
                url: "/pedido/emitir_orcamento_compra",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idOrcamentoCompras": $('#idOrcamentoComprasEmitir').val(),
                    "idParceiro": $('#idParceiro').val(),
                    "dsContato": $('#dsContato').val(),
                    "dsFone": $('#dsFone').val(),
                    "dsCelular": $('#dsCelular').val(),
                    "dsEmail": $('#dsEmail').val(),
                    "dsEndereco": $('#dsEndereco').val(),
                    "dsCidade": $('#dsCidade').val(),
                    "dsFrete": $('#dsFrete').val(),
                    "dsLocalEntrega": $('#dsLocalEntrega1').val(),
                    "idEmpresa": $('#idEmpresa').val(),
                    "cdCNPJ": $('#cdCNPJ').val(),
                    "dtPrazoEntrega": $('#dtPrazoEntrega').val(),
                    "dsCondicoesPagamento": $('#dsCondicoesPagamento').val(),
                    "dsObservacoes": $('#dsObservacoes').val(),
                },
                success: function (dataReturn) {
//                    var url = dataReturn.url;
//                    windows.open(url,'_blank');
//                    document.getElementById("botao_criar").innerHTML =
//                      "<a class='btn btn-primary' data-dismiss='modal' onclick='emitir_pedido_compra.criar();'>CRIAR PDF</a>";

//                    emitir_pedido_compra.clear_log();
                    location.reload();  
                    
//                      $('#listagem').html(dataReturn.html);  
                }
            });
        }
    };
    this.consisteEmitirPedidoCompra = function () {
        var stats = true;
        if ($('#dsEndereco').val() == '') {
            showMessage('Favor digitar o endereco');        
            stats = false;
        }
        if ($('#dsCidade').val() == '') {
            showMessage('Favor digitar a cidade');        
            stats = false;
        }
        if ($('#idEmpresa').val() == '') {
            showMessage('Favor escolher a empresa');        
            stats = false;
        }
        if ($('#dsLocalEntrega1').val() == '') {
            showMessage('Favor digitar o local de entrega');        
            stats = false;
        }

        return stats;
    };
    
    this.show = function (idPedido) {
        jQuery.ajax({
            async: false,
            type: "post",
            dataType: "html",
            url: "/pedido/emitirpedidocompra",
            data: {
                "idPedido": idPedido
            },
            complete: function (event, XMLHttpRequest) {
                if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                    try {
                        $('#pedidocompra_show').each(function () {
                            $('.modal-body', this).html(event.responseText);
                            $(this).modal('show');
                        });
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        });
    };
    this.showOrcamento = function (idOrcamentoCompras) {
        jQuery.ajax({
            async: false,
            type: "post",
            dataType: "html",
            url: "/orcamentocompras/emitirorcamentocompra",
            data: {
                "idOrcamentoCompras": idOrcamentoCompras
            },
            complete: function (event, XMLHttpRequest) {
                if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                    try {
                        $('#orcamentocompra_show').each(function () {
                            $('.modal-body', this).html(event.responseText);
                            $(this).modal('show');
                        });
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        });
    };
};

var recebimento_modal = new function () {
    this.clear_log = function () {
        $('#recebimento_show').modal('hide');
    };    
    
    this.consisteRecebimento = function() {
        
        var stats = false;
        var errorText = 'Favor selecionar o local de estoque e o centro de custo!!!';                    

        $("#linhas_recebimento").find('input[type=text]').each(function () {
            var nada = this.id.substring(4,30);
            var conteudo = document.getElementById("qtde" + nada).value;
            var novoQtde = conteudo.replace(".","");
            var novoQtde1 = novoQtde.replace(",",".");
            var Qtde = parseFloat(novoQtde1);
            

            var conteudo = document.getElementById("valo" + nada).value;
            var novoValor = conteudo.replace(".","");
            var novoValor1 = novoValor.replace(",",".");
            var valor = parseFloat(novoValor1);
            if (Qtde>0 && valor>0) {
                stats = true;
                errorText = '';
            } else {
                stats = false;
                showMessage('Favor digitar um valor e uma quantidade para a baixa!!!');                    
            }
            
            var conteudo = document.getElementById("localestoque" + nada).value;
            if (conteudo == '') {
                stats = false;
                showMessage('Favor selecionar o Local de Estoque!!!');                    
            }
            var conteudo = document.getElementById("centrocusto" + nada).value;
            if (conteudo == '') {
                stats = false;
                showMessage('Favor selecionar o Centro de Custo!!!');                    
            }
        });
        
        if (errorText) {
            showMessage(errorText);        
        };
        
        return stats;                
    };
    
    this.show = function (idPedido) {
        jQuery.ajax({
            async: false,
            type: "post",
            dataType: "html",
            url: "/pedido/recebimento",
            data: {
                "idPedido": idPedido
            },
            complete: function (event, XMLHttpRequest) {
                if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                    try {
                        $('#recebimento_show').each(function () {
                            $('.modal-body', this).html(event.responseText);
                            $(this).modal('show');
                        });
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        });
    };
};

verificaerrosC = function() {
    var err = true;
    if ($('#dsSolicitante').val() == '' && $('#idUsuarioSolicitante').val() == '') {
        showMessage('Favor escolher ou digitar o solicitante');
        err = false;
    }
    if ($('#idParceiro').val() == '') {
        showMessage('Favor escolher um Parceiro');        
        err = false;
    }
    
    if ($('#idPrioridade').val() == '') {
        showMessage('Favor escolher a prioridade');        
        err = false;
    }
    if ($('#dsLocalEntrega').val() == '') {
        showMessage('Favor digitar o local de entrega');
        err = false;
    }
    return err;
}

verificaerrosI = function() {
    var err = true;
    if ($('#idInsumo').val() == '') {
        showMessage('Favor escolher o produto');        
        err = false;
    }
    if ($('#qtPedido').val() == '' || $('#qtPedido').val() == '0,00') {
        showMessage('Favor digitar a quantidade pedida');
        err = false;
    }
    if ($('#vlPedido').val() == '' || $('#vlPedido').val() == '0,00') {
        showMessage('Favor digitar o valor pedida');
        err = false;
    }
    if ($('#idCentroCusto').val() == '') {
        showMessage('Favor escolher um Centro de Custo para o destino do Produto');        
        err = false;
    }
    if ($('#idGrupoDR').val() == '') {
        showMessage('Favor escolher um grupo de despesa para este pedido');        
        err = false;
    }
    if ($('#idItemDR').val() == '') {
        showMessage('Favor escolher um item de despesa para este pedido');        
        err = false;
    }
    return err;
}