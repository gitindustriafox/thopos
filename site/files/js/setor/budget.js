//Valida Formulario Antes de Enviar
function validaFormulario() {
    var stats = true;
    var errorText = '';  
    

    if ($.trim($('#descricao').val()) === '') {
        errorText += 'A descrição é obrigatória!';
        stats = false;
    }

    if (errorText) {
        showMessage(errorText);
    }

    return stats;
}

$('#btnInsereMenu').click(function(){
   $('#painel_menu').fadeOut(3000);
   $('#painel_menu').fadeIn(3000);     
});

$(document).ready(function(){
  $('#descricao').focus();  
});
  
var budget = new function () {
    this.adicionarbudget = function() {
        if ($("#optReplica").prop('checked')) {
           replica = 'sim';
        } else {
           replica = 'nao';
        }            
        $.ajax({
            url: "/setorbudget/adicionarbudget",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idSetor": $("#idSetor").val(),
                "idSetorBudget": $("#idSetorBudget").val(),
                "idAno": $("#idAno").val(),
                "idMes": $("#idMes").val(),
                "vlBudget": $("#vlBudget").val(),
                "replica": replica
            },
            success: function (dataReturn) {
                $("#idSetorBudget").val('');
                $("#vlBudget").val('');
                $("#idAno").val('');
                $("#idMes").val('');
                $("#mostrarbudgets").html(dataReturn.html);
            }
        });        
    }
    this.ativar = function () {
        $.ajax({
            url: "/setorbudget/ativar",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                    "idSetor": $("#idSetor").val(),
                    "stBudget": $("#stBudget").val()
            },
            success: function (dataReturn) {
                location.reload();
            }
        });        
    }
    
    this.delbudget = function(idSetorBudget) {
        $.ajax({
            url: "/setorbudget/del_setorbudgest",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                    "idSetorBudget": idSetorBudget,
                    "idSetor": $("#idSetor").val()
            },
            success: function (dataReturn) {
                $("#mostrarbudgets").html(dataReturn.html);
            }
        });        
    }
    
}
  