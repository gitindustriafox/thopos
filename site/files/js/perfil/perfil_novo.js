//Valida Formulario Antes de Enviar
function validaFormulario() {
    var stats = true;
    var errorText = '';  
    

    if ($.trim($('#dsPerfil').val()) === '') {
        errorText += 'A descrição é obrigatória!';
        stats = false;
    }

    if (errorText) {
        showMessage(errorText);
    }

    return stats;
}

  $('#btnInsereMenu').click(function(){
     $('#painel_menu').fadeOut(3000);
     $('#painel_menu').fadeIn(3000);     
  });
   
  $(document).ready(function(){
    $('#dsPerfil').focus();  
  });
  
var perfil = new function () {
    this.copiarperfil = function () {
        $.ajax({
            url: "/perfil/copiar",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idPerfil": $('#idPerfil').val(),
                "idPerfilCopiar": $('#idPerfilCopiar').val()
            },
            success: function (dataReturn) {
                location.reload(); 
            }
        });
    };    
};