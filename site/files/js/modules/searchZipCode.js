SearchZipCode = function (params) {
    var object = this;
    this.constructor = function (params) {
        this.params = params;
        this.selector = params.selector;
        this.formObject = params.formObject;
        this.onChange = params.onChange;
        this.typingTimer = 0;
        this.doneTypingInterval = 5000;
        this.run();
    };
    this.run = function () {
        if (undefined != this.selector) {
            $(this.selector).on('blur', function () {

                var zipBlur = $(this).val();
                var zipOrigin =  $(this).attr('data-origin');
                if(zipBlur != zipOrigin){                    
                    $(this).attr('data-origin',zipBlur);  
                    object.clearFields();
                    if (undefined != $(this).val() && '' != $(this).val()) {
                        object.search($(this).val());
                    }
                } 
            });
        }
    };
    this.search = function (zipcode) {
        standard.form.block(object.formObject, true);
        $.ajax({
            async: true,
            type: 'GET',
            dataType: 'json',
            url: '/searchZipCode/ws/' + zipcode,
            data: {
                url: zipcode
            },
            complete: function (event, XMLHttpRequest) {
                try {
                    var data = jQuery.parseJSON(event.responseText);
                    object.fillFields(data.result);
                    standard.form.block(object.formObject, false);
                } catch (e) {
                    console.log(e);
                    standard.form.block(object.formObject, false);
                }
            }
        });
    };
    this.fillFields = function (data) {
        if ($(object.formObject).is("form") && (undefined != data) && ('' != data)) {
            if ((undefined != data.cep_formatado)) {
                $("input#cep", object.formObject).val(data.cep_formatado);
            }
            if ((undefined != data.id_municipio) && (undefined != data.desCity) && (undefined != data.ufState)) {
                $("input#id_municipio", object.formObject).val(data.id_municipio).trigger('change');
                $("input#des_municipio", object.formObject).val(data.desCity + ' - ' + data.ufState + ' / ' + data.desCountrySigla);
                object.onChange();
            }
            if ((undefined != data.id_bairro) && (undefined != data.desNeighbourhood)) {
                $("input#id_bairro", object.formObject).val(data.id_bairro).trigger('change');
                $("input#des_bairro", object.formObject).val(data.desNeighbourhood);
            }
            if ((undefined != data.id_logradouro)) {
                $("select#id_logradouro", object.formObject).val(data.id_logradouro);
            }
            if ((undefined != data.endereco)) {
                $("input#endereco", object.formObject).val(data.endereco);
            }
            if ((undefined != data.lat)) {
                $("input#lat", object.formObject).val(data.lat);
            }
            if ((undefined != data.lon)) {
                $("input#lon", object.formObject).val(data.lon);
            }
            if ((undefined != data.complemento)) {
                $("textarea#complemento", object.formObject).val(data.complemento);
            }
        }
    };
    this.clearFields = function () {
        $("input#id_municipio", object.formObject).val('');
        $("input#des_municipio", object.formObject).val('');
        $("input#id_bairro", object.formObject).val('');
        $("input#des_bairro", object.formObject).val('');
        $("select#id_logradouro", object.formObject).val('');
        $("input#endereco", object.formObject).val('');
        $("input#lat", object.formObject).val('');
        $("input#lon", object.formObject).val('');
        $("textarea#complemento", object.formObject).val('');
    };
    this.constructor(params);
}