/*
 * Gestor Fox - Módulo de Autocomplete Padrão
 */

if ('undefined' == typeof modules) {
    var modules = new function () {
    }
}

/* Só registra o módulo uma única vez, mesmo se o arquivo for chamado várias vezes */
if ('undefined' == typeof modules.autocomplete || undefined === modules.autocomplete) {

    modules.AutocompletePadrao = function (params) {
        var object = this;
        this.constructor = function (params) {
            object.currentRequest = null; /* We should have only one ajax request at a time. We use this to abort the previous one. */
            this.params = params;
            this.validateParams();
            /* If input is already an autocomplete, destroy the old one before creating a new one */
            if ($(object.params.selectorForDescription).data('autocomplete')) {
                this.destroy();
            }
            this.render();
            this.setupCleaning();
            this.autoLoadInitialValue();
        }
        this.validateParams = function () {
            /* Check if all required parameters exist */
            var requiredParams = ['selectorForDescription', 'selectorForId', 'url'];
            for (var i = 0; i < requiredParams.length; i++) {
                if (!(requiredParams[i] in object.params)) {
                    console.log('Error - modules.AutocompletePadrao - Required parameter not found: ' + requiredParams[i]);
                }
            }
            /* Check if some unknown parameter was used */
            var availableParams = ['selectorForDescription', 'selectorForId', 'url', 'allowPartial', 'onChoose', 'onSendParams', 'onUnchoose', 'autoLoadInitialValue', 'keepDisabled', 'onClear', 'onReceiveData', 'minLength'];
            for (param in object.params) {
                if (availableParams.indexOf(param) === -1) {
                    console.log('Error - modules.AutocompletePadrao - Parameter unknown: ' + param);
                }
            }
            /* Also check if DOM elements exist */
            if (!$(object.params.selectorForDescription).length) {
                console.log('Error - modules.AutocompletePadrao - Description element not found: ' + object.params.selectorForDescription);
            }
            if (!$(object.params.selectorForId).length) {
                console.log('Error - modules.AutocompletePadrao - Id element not found: ' + object.params.selectorForId);
            }

        }
        /* Carrega automaticamente o valor inicial, apenas se o ID for maior do que 0 */
        this.autoLoadInitialValue = function () {
            if (true === object.params.autoLoadInitialValue && parseInt($(object.params.selectorForId).val()) > 0) {
                this.autoLoadDescriptionFromId();
            } else {
                $(object.params.selectorForDescription).removeClass('ui-autocomplete-initializing');
                if (!object.params.keepDisabled) {
                    $(object.params.selectorForDescription).removeAttr("disabled");
                }
            }
        }
        this.autoLoadDescriptionFromId = function () {
            if (!$(object.params.selectorForId).val()) {
                return false;
            }
            $.ajax({
                method: 'post',
                url: object.params.url,
                data: {'id': $(object.params.selectorForId).val()},
                dataType: "json",
                success: function (data) {
                    if (data !== undefined && data[0] !== undefined && data[0].value !== undefined) {
                        $(object.params.selectorForDescription).val(data[0].value);
                        $(object.params.selectorForId).trigger('change');
                    } else {
                        $(object.params.selectorForDescription).val('SEM PERMISSAO - ID ' + $(object.params.selectorForId).val() + '');
                    }
                },
                complete: function () {
                    $(object.params.selectorForDescription).removeAttr("disabled");
                    $(object.params.selectorForDescription).removeClass('ui-autocomplete-initializing');
                }
            });
        }
        this.setupCleaning = function () {
            if ('undefined' == typeof object.params.allowPartial) {
                object.params.allowPartial = false;
            }
            /* Automatically clean autocomplete */
            $(object.params.selectorForDescription).on('blur.autocompletePadrao', function () {
                if (!object.params.allowPartial) {
                    /**
                     * Necessário para caso o usuário escreva parte do nome e não
                     * não clicar na lista do autocomplete, o id retorne para vazio
                     */
                    if ($(object.params.selectorForId).val() == false) {
                        object.clear();
                    }
                }
                object.unchoose();
            })
            /* Sempre que é digitado algo, o valor do ID retorna pra 0 */
            $(object.params.selectorForDescription).on('input.autocompletePadrao', function () {
                if ($(object.params.selectorForId).val() != false) {
                    $(object.params.selectorForId).data('autocomplete_must_run_callback_unchoose', true);
                }
                $(object.params.selectorForId).val('').trigger('change');
            });
            /* Sempre que o valor do ID for alterado, altera a classe CSS */
            $(object.params.selectorForId).on('change.autocompletePadrao', function () {
                if (parseInt($(this).val()) > 0) {
                    $(object.params.selectorForDescription).addClass('autocomplete-id-selected');
                    $(object.params.selectorForDescription).removeClass('autocomplete-id-not-selected');
                } else {
                    $(object.params.selectorForDescription).addClass('autocomplete-id-not-selected');
                    $(object.params.selectorForDescription).removeClass('autocomplete-id-selected');
                }
            }).trigger('change');
        }
        /**
         * @param function onChoose Callback disparado no
         *    momento em que uma opção do autocomplete é selecionada
         */
        this.render = function () {
            if ('undefined' == typeof object.params.minLength) {
                object.params.minLength = 3;
            }
            var sourceFunction = function (request, response) {
                if (typeof object.params.onSendParams === 'function') {
                    var postData = object.params.onSendParams();
                }
                if (typeof postData === 'undefined') {
                    var postData = {}
                }
                postData.term = request.term
                object.currentRequest = $.ajax({
                    method: 'post',
                    url: object.params.url,
                    data: postData,
                    dataType: "json",
                    success: function (data) {
                        if (typeof object.params.onReceiveData === 'function') {
                            object.params.onReceiveData(data);
                        }
                        response(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        data = {};
                        if (typeof object.params.onReceiveData === 'function') {
                            object.params.onReceiveData({});
                        }
                        if (jqXHR.status !== 200 || jqXHR.responseText) {
                            var data = [
                                {id: '', value: 'ERRO AO CONSULTAR REGISTROS'}
                            ];
                        }
                        response(data);
                    },
                    complete: function () {
                    },
                    beforeSend: function () {
                        /* Abort previous requests pending from this same autocomplete object */
                        if (object.currentRequest !== null) {
                            object.currentRequest.abort();
                        }
                    }
                });
            };
            $(object.params.selectorForDescription).autocomplete({
                source: sourceFunction,
                minLength: object.params.minLength,
                select: function (event, ui) {
                    // Atualiza o campo da descrição
                    $(object.params.selectorForDescription).val(ui.item.label).trigger('change');
                    // Atualiza o campo do ID
                    $(object.params.selectorForId).val(ui.item.id).trigger('change');
                    // Desativa a flag que exige rodar callback onUnchoose
                    ($(object.params.selectorForId).data('autocomplete_must_run_callback_unchoose', false));
                    if (typeof object.params.onChoose === 'function') {
                        /**
                         * Passa para o callback um primeiro parâmetro contendo
                         * o ID do objeto escolhido (que é suficiente na grande maioria dos casos),
                         * e um segundo parâmetro contendo todos os outros dados recebidos por ajax,
                         * que pode ser uma lista de vários
                         */
                        object.params.onChoose(ui.item.id, ui.item);
                    }
                }
            })
        };
        this.clear = function () {
            $(object.params.selectorForId).val('').trigger('change');
            $(object.params.selectorForDescription).val('').trigger('change');
            $(object.params.selectorForDescription).removeClass("ui-autocomplete-loading");
            if (typeof object.params.onClear === 'function') {
                object.params.onClear();
            }
        }
        /* Destroys the autocomplete object */
        this.destroy = function () {
            /* Remove jQuery component from form element */
            $(object.params.selectorForDescription).autocomplete('destroy');
            $(object.params.selectorForDescription).removeData('autocomplete');
            /* Remove bound events */
            $(object.params.selectorForDescription).off('.autocompletePadrao');
            $(object.params.selectorForId).off('.autocompletePadrao');
            /* Remove CSS classes */
            $(object.params.selectorForDescription).removeClass('autocomplete-id-not-selected');
            $(object.params.selectorForDescription).removeClass('autocomplete-id-selected');
        }
        /**
         * @param function onChoose Callback disparado no
         *    momento em que o autocomplete é resetado
         */
        this.unchoose = function () {
            /* Só executa o callback onUnchoose se o valor foi de não nulo para nulo */
            if ($(object.params.selectorForId).data('autocomplete_must_run_callback_unchoose')) {
                $(object.params.selectorForId).data('autocomplete_must_run_callback_unchoose', false);
                if (typeof object.params.onUnchoose === 'function') {
                    object.params.onUnchoose();
                }
            }
        }
        this.val = function () {
            return $(object.params.selectorForId).val();
        }
        this.setValueAndLoadDescription = function (id) {
            if (id > 0) {
                $(object.params.selectorForId).val(id);
                this.autoLoadDescriptionFromId();
            }
        }
        this.trigger = function (eventName) {
            switch (eventName) {
                case 'change':
                    object.params.onChoose(object.val());
                    break;
            }
        }
        this.enable = function () {
            $(object.params.selectorForDescription).removeAttr('readonly');
        }
        this.disable = function () {
            $(object.params.selectorForDescription).attr('readonly', 'readonly');
        }
        this.enableId = function () {
            $(object.params.selectorForId).removeAttr('disabled');
        }
        this.disableId = function () {
            $(object.params.selectorForId).attr('disabled', 'disabled');
        }
        this.show = function () {
            $(object.params.selectorForDescription).show();
        }
        this.hide = function () {
            $(object.params.selectorForDescription).hide();
        }
        this.constructor(params);
        return this;
    }

    /**
     *
     * THE FOLLOWING METHOD IS OBSOLETE!
     *
     * The following module was kept for backward compatibility,
     * but from now on, one can instantiate a new autocomplete by calling directly
     *
     * PLEASE USE THIS CODE, INSTEAD:
     * myAutocomplete = new modules.AutocompletePadrao({
     *    ... params ...
     * });
     *
     */
    modules.autocomplete = new function () {
        this.setup = function (selectorForDescriptionOrParameters, selectorForId, url, allowPartial, onChoose, onSendParams, onUnchoose, autoLoadInitialValue, keepDisabled, onClear, onReceiveData) {
            if (typeof selectorForDescriptionOrParameters !== 'object') {
                var params = {
                    selectorForDescription: selectorForDescriptionOrParameters,
                    selectorForId: selectorForId,
                    url: url,
                    allowPartial: allowPartial,
                    onChoose: onChoose,
                    onSendParams: onSendParams,
                    onUnchoose: onUnchoose,
                    autoLoadInitialValue: autoLoadInitialValue,
                    keepDisabled: keepDisabled,
                    onClear: onClear,
                    onReceiveData: onReceiveData
                }
            } else {
                var params = selectorForDescriptionOrParameters
            }
            var autocompletePadrao = new modules.AutocompletePadrao(params)
            return autocompletePadrao;
        };
        /* This method is obsolete and should be kept here for avoid breaking old code */
        this.setup_cleaning = function () {
            return false;
        }
    }
}
