GestorTabs = function (params) {
    var object = this;
    this.constructor = function (params) {
        this.params = params;
        this.navSelector = params.navSelector;
        this.namespace = 'tabDisabled-' + this.randomId();
        this.disableAllTabs();
        if (params.activeTab) {
            this.setActiveTab(params.activeTab);
        }
        if (params.enabledTabs && params.enabledTabs.constructor === Array) {
            params.enabledTabs.forEach(function (currentValue) {
                object.enableTab(currentValue);
            });
        }
    };
    this.disableAllTabs = function () {
        $(this.navSelector).find('li').each(function () {
            object.disableTab('#' + $(this).attr('id'));
        });
    };
    this.enableTab = function (tabId) {
        $(this.navSelector).find(tabId).each(function () {
            $(this)
                .removeClass('disabled')
                .css('pointer-events', '')
                .find('a').each(function () {
                    $(this)
                        .attr('href', $(this).data('href'))
                        .off('click.' + object.namespace);
                });
        }); 
    };
    this.setActiveTab = function (tabId) {
        this.disableTab(tabId);
        $(this.navSelector).find(tabId).removeClass('disabled');
        $(this.navSelector).find(tabId).addClass('active');
    };
    this.disableTab = function (tabId) {
        $(this.navSelector).find(tabId).find('a').each(function () {
            $(this).data('href', $(this).attr('href'));
            $(this).attr('href', '#');
            $(this).on('click.' + object.namespace, function(evt) {
                evt.stopPropagation();
                evt.preventDefault();
            });
            $(this).closest('li')
                .addClass('disabled')
                .css('pointer-events', 'none');
        });
    }
    this.randomId = function () {
        return Math.random().toString(36).substr(2, 10);
    }
    /* Call constructor */
    this.constructor(params);
}
