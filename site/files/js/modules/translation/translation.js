if ('undefined' === typeof modules) {
    var modules = {};
}

if (undefined === modules.translation) {
    modules.translation = {};
}

if (undefined === modules.translation.translation) {

    modules.translation.translation = new function () {

        var object = this;
        this.localStringCache = {};

        this.get = function (stringKey) {
            var translatedString = stringKey;
            if (undefined === object.localStringCache[stringKey]) {
                translatedString = object.getFromServer(stringKey);
                this.localStringCache[stringKey] = translatedString;
            } else {
                translatedString = object.localStringCache[stringKey];
            }
            return translatedString;
        }

        this.getFromServer = function (stringKey) {
            var translatedString = stringKey;
            $.ajax({
                type: "POST",
                url: "/publiccommon/ajax_traducao_mensagem_js",
                data: {
                    string_traduzir: stringKey
                },
                dataType: "json",
                async: false,
                success: function (data) {
                    translatedString = data.traducao;
                },
                error: function () {
                    translatedString = stringKey;
                }

            });
            return translatedString;
        }
    }

}
