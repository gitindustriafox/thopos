if ('undefined' === typeof modules.smartyPluginFormIconFile) {

    modules.smartyPluginFormIconFile = function (htmlId, modalTitle) {

        var object = this;

        object.domSelector = null; 
        object.$group = null; //The convention is that variables starting with $ store jquery result objects
        object.previewModal = null;
        object.params = {};

        this.construct = function (htmlId, modalTitle) {
            object.params.modalTitle = modalTitle;
            object.domSelector = '#' + htmlId;
            object.$group = $(object.domSelector);
            this.ready();
            return object;
        };

        /**
         * Method onchange file
         */
        this.ready = function () {
            object.previewModal = new AjaxModal({
                'title': object.params.modalTitle,
                'showLoading': false,
                'onReady' : function () {
                    object.modalIsReady();
                }
            });
            object.$group.find('.smarty_plugin_form_icon_file_selector').change(function (event) {
                if (event && event.target && event.target.files && event.target.files[0]) {
                    object.upload(event);
                } else {
                    object.delete();
                }
            });
            object.$group.find('.smarty_plugin_form_icon_file_delete').click(function () {
                object.delete();
            });
        };

        this.modalIsReady = function () {
            /* Enable image preview */
            object.$group.find('.smarty_plugin_form_icon_file_img').addClass('pointer').click(function () {
                if ($(this).data('original-src')) {
                    object.previewModal.render('/Common/TemporaryFile/action/renderPreviewModal', {
                        img: $(this).data('original-src')
                    }); 
                }
            });
        }

        /**
         * Upload file to /storage/tmp/smarty_plugin_form_icon_file_randomId
         */
        this.upload = function (event) {
            $path = object.$group.find('.smarty_plugin_form_icon_file_path');
            $path.attr('name', $path.attr('id'));
            var data = new FormData();
            data.append('file', event.target.files[0]);
            util.sweetAjax({
                url: "/Common/TemporaryFile/action/upload",
                type: 'POST',
                data: data,
                dataType: "JSON",
                async: true,
                processData: false,
                contentType: false,
                file: true,
                success: function (data) {
                    object.$group.find('.smarty_plugin_form_icon_file_img').attr('src', data.body.publicUrl);
                    object.$group.find('.smarty_plugin_form_icon_file_path').val(data.body.tempName);
                },
            });
        };

        this.delete = function () {
            object.$group.find('.smarty_plugin_form_icon_file_selector').val('');
            object.$group.find('.smarty_plugin_form_icon_file_img').attr('src', '/files/default/images/no-image.png');
            $path = object.$group.find('.smarty_plugin_form_icon_file_path');
            $path.val('').attr('name', $path.attr('id'));
        }

        this.construct(htmlId, modalTitle);
    };
}
