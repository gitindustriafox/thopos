$(document).ready(function() {
    financeiro.autocomplete.Parceiro();
    
    $("#frm-busca-financeiro [type=submit]").click(function(){
        if ($(this).hasClass('buscar')){
            $("#frm-busca-financeiro").attr('action', '/financeiro/busca_financeiro');
            $("#frm-busca-financeiro").submit();
        } else {
            if (financeiro.consisteCriarCSV()) {
                $("#frm-busca-financeiro").attr('action', '/financeiro/busca_criarcsv');
                $("#frm-busca-financeiro").submit();
            } else {
                $("#frm-busca-financeiro").attr('action', '');
            }          
        }
    });

    $.datepicker.setDefaults({
        defaultDate: null,
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd/mm/yy"
    });
    $("#dtVencimento").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtVencimento").val("");
        }
    });
    $("#dtEmissao").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtEmissao").val("");
        }
    });
    $("#vlTotal").priceFormat({
        prefix: "",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    }); 
    $("#vlParcela").priceFormat({
        prefix: "",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    }); 
     
    util.styleTitleLeft();    

});

$(function () {
    financeiro.autocomplete.Parceiro();
    util.styleTitleLeft();    
});

var financeiro = new function () {
    this.autocomplete = new function () {
        this.Parceiro = function () {
            $('.complete_Parceiro').autocomplete({
                source: "/financeiro/getParceiro",
                minLength: 3,
                select: function (event, ui) {
                    var retorno = $(this).attr('data-label');
                    $('#idParceiro').val(ui.item.id);
                    $("#dsParceiro").val(ui.item.value);
                }
            });
            $(".complete_Parceiro").bind("blur keyup keydow keypress", function () {
                if ($("#dsParceiro" + $(this).attr("data-label")).val() === "") {
                    $('#idParceiro' + $(this).attr("data-label")).val("");
                }
            }); 
        };
        
        this.produto = function () {
            if ($("#stTipoI").prop('checked')) {    
                tipo = "/financeiro/getProduto/insumo/";
            } else {
                tipo = "/financeiro/getProduto/servic/";
            }
            $('.complete_produto').autocomplete({
                source: tipo,
                minLength: 3,
                select: function (event, ui) {
                    var retorno = $(this).attr('data-label');
                    $('#idInsumo').val(ui.item.id);
                    $("#insumo").val(ui.item.value);
                }
            });
            $(".complete_produto").bind("blur keyup keydow keypress", function () {
                if ($("#insumo" + $(this).attr("data-label")).val() === "") {
                    $('#idInsumo' + $(this).attr("data-label")).val("");
                }
            }); 
        };
    };
    this.showiprodutos = function () {
        jQuery.ajax({
            async: false,
            type: "post",
            dataType: "html",
            url: "/financeiro/importarprodutos_mostrar",
            data: {
                "idFinanceiro": $("#idFinanceiro").val(),
                "idUnidade": $("#idUnidade").val(),
                "idParceiro": $("#idParceiro").val(),
                "dsObservacaoItem": $("#dsObservacaoItem").val(),
                "idCentroCusto": $("#idCentroCusto").val(),
                "dsParceiroSugerido": $("#dsParceiroSugerido").val()
            },
            complete: function (event, XMLHttpRequest) {
                if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                    try {
                        $('#importarprodutos_show').each(function () {
                            $('.modal-body', this).html(event.responseText);
                            $(this).modal('show');
                        });
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        });
    };
    this.clear_log = function () {
        $('#parcelas_show').modal('hide');
    };        
    
    this.adicionarparcela = function () {
        $.ajax({
            url: "/financeiro/gravar_item",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idFinanceiro": $('#idFinanceiro').val(),
                "vlParcela": $('#vlParcela').val(),
                "dtVencimento": $('#dtVencimento').val(),
            },
            success: function (dataReturn) {
            }
        });
        this.clear_log();
        location.reload();         
    };
    
    this.verdadosproduto = function (idInsumo) {
        jQuery.ajax({
            async: false,
            type: "post",
            dataType: "html",
            url: "/financeiro/mostrar_dados_produto",
            data: {
                "idInsumo": idInsumo
            },
            complete: function (event, XMLHttpRequest) {
                if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                    try {
                        $('#insumo_show').each(function () {
                            $('.modal-body', this).html(event.responseText);
                            $(this).modal('show');
                        });
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        });
    };
    
    this.selecionarfoto=function(idFinanceiroItem) {
        jQuery.ajax({
            async: false,
            type: "post",
            dataType: "html",
            url: "/financeiro/importarfotos",
            data: {
                "idFinanceiro": $('#idFinanceiro').val(),
                "dsTabela": 'prodFinanceiroItens',
                "idFinanceiroItem": idFinanceiroItem
            },
            complete: function (event, XMLHttpRequest) {
                if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                    try {
                        $('#financeirofotos_show').each(function () {
                            $('.modal-body', this).html(event.responseText);
                            $(this).modal('show');
                        });
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        });
    };
    
    this.digitarmensagem=function(idFinanceiroItem) {
        jQuery.ajax({
            async: false,
            type: "post",
            dataType: "html",
            url: "/financeiro/digitarmensagem",
            data: {
                "idFinanceiro": $('#idFinanceiro').val(),
                "dsTabela": 'prodFinanceiroItens',
                "idFinanceiroItem": idFinanceiroItem
            },
            complete: function (event, XMLHttpRequest) {
                if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                    try {
                        $('#mensagem_show').each(function () {
                            $('.modal-body', this).html(event.responseText);
                            $(this).modal('show');
                        });
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        });
    };
    
    this.closejanela = function () {
        $('#financeirofotos_show').modal('hide');
    }
    
    this.closemensagem = function () {
        $('#mensagem_show').modal('hide');
    }
    
    this.calculanecessidade = function () {
        $.ajax({
            url: "/financeiro/calcularnecessidade",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idPrioridade": $('#idPrioridade').val()
            },
            success: function (dataReturn) {
                $('#dtNecessidade').val(dataReturn.datanova);
            }
        });
        
    }
    this.lerprodutostipomov = function () {
        $.ajax({
            url: "/financeiro/lerprodutostipomov",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idTipoMovimento": $('#idTipoMovimento').val(),
                "idInsumo": $('#idInsumo').val()
            },
            success: function (dataReturn) {
                $('#linhas_movimento').html(dataReturn.html);
            }
        });
        
    }
    this.lerItemDR = function () {
        $.ajax({
            url: "/financeiro/lerItemDR",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idGrupoDR": $('#idGrupoDR').val()
            },
            success: function (dataReturn) {
                $('#idItemDR').html(dataReturn.html);
            }
        });
        
    }
    this.consisteCriarPedido = function () {
        
        var stats = false;
        var errorText = 'Favor marcar pelo menos uma opção e colocar o valor!!!';                    

        if ($('#idParceiro').val() == '' || $('#idParceiro').val() == 0) {
            stats = false;
            errorText = 'Favor digitar o fornecedor';              
        } else {
            $("#listagem").find('input[type=checkbox]').each(function () {
                if(this.checked === true) {
                    var nada = this.id.substring(10,30);
                    var conteudo = document.getElementById("valor" + nada).value;
                    var novoValor = conteudo.replace(".","");
                    var novoValor1 = novoValor.replace(",",".");
                    var valor = parseFloat(novoValor1);
                    if (valor>0) {
                        stats = true;
                        errorText = '';
                    }
                }
            });
        }
        
        if (errorText) {
            showMessage(errorText);   
        };
        
        return stats;        
    };
    this.consisteCriarCotacao = function () {
        
        var stats = false;
        var errorText = 'Favor marcar pelo menos uma opção!!!';                    

        $("#listagem").find('input[type=checkbox]').each(function () {
            if(this.checked === true) {
                stats = true;
                errorText = '';
            }
        });
        
        if (errorText) {
            showMessage(errorText);   
        };
        
        return stats;        
    };
    
    this.consisteCriarCSV = function () {
        
        var stats = false;
        var errorText = 'Favor marcar pelo menos uma opção !!!';                    

        $("#listagem").find('input[type=checkbox]').each(function () {
            if(this.checked === true) {
                stats = true;
                errorText = '';
            }
        });
        
        if (errorText) {
            showMessage(errorText);   
        };
        
        return stats;        
    };
    
    this.editarfinanceiro = function (parcela) {
        $("#dtVencimento-" + parcela).removeAttr('disabled');
        $("#vlParcela-" + parcela).removeAttr('disabled');
    };
    this.alterarfinanceiro = function (item, nrParcela) {
        $.ajax({
            url: "/financeiro/alterar_financeiro",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idFinanceiroParcela": item,
                "dtVencimento": $("#dtVencimento-" + nrParcela).val(),
                "vlParcela": $("#vlParcela-" + nrParcela).val()
            },
            success: function (dataReturn) {
                location.reload(); 
            }
        });
    };    
    this.excluirfinanceiro = function (idFinanceiro) {
        if (confirm('DESEJA EXCLUIR ESTA CONTA?')) {
            $.ajax({
                url: "/financeiro/delfinanceiro",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idFinanceiro": idFinanceiro,
                },
                success: function (dataReturn) {
                    if (dataReturn.ok ==1) {
                        showMessage('Lançamento excluido com sucesso!!!');                        
                    }
                    location.reload(); 
                }
            });
        };
    };    
    this.arquivarfinanceiro = function (idFinanceiro, idFinanceiroItem) {
        if (confirm('DESEJA ARQUIVAR ESTE ITEM DA SOLICITACAO DE COMPRAS?')) {
            $.ajax({
                url: "/financeiro/arquivar",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idFinanceiro": idFinanceiro,
                    "idFinanceiroItem": idFinanceiroItem
                },
                success: function (dataReturn) {
                    location.reload(); 
                }
            });
        };
    };    
    this.atualizaunidades = function () {
        $.ajax({
            url: "/financeiro/atualiza_unidades",
            dataType: "json",
            async: false,
            type: "POST",
            success: function (dataReturn) {
                $("#idUnidade").html(dataReturn.html);
            }
        });
    };
    
    this.atualizaprodutos = function (opcao) {
        if ($("#stTipoI").prop('checked')) {
           opcao = '0';
        } else {
           opcao = '1';
        }
        
        $.ajax({
            url: "/financeiro/atualiza_produtos",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "opcao": opcao,
            },
            success: function (dataReturn) {
                $("#idInsumo").html(dataReturn.html);
            }
        });
    };

    this.atualizaParceiroes = function () {
        $.ajax({
            url: "/financeiro/atualiza_Parceiroes",
            dataType: "json",
            async: false,
            type: "POST",
            success: function (dataReturn) {
                $("#idParceiro").html(dataReturn.html);
            }
        });
    };

    this.gravarcabecalho = function () {   
        if (!verificarerros()) {
            $.ajax({
                url: "/financeiro/gravar_financeiro",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idFinanceiro": $("#idFinanceiro").val(),
                    "idParceiro": $("#idParceiro").val(),
                    "idTipo": $("#idTipo").val(),
                    "idPedido": $("#idPedido").val(),
                    "cdDocumento": $("#cdDocumento").val(),
                    "nrNF": $("#nrNF").val(),
                    "dtVencimento": $("#dtVencimento").val(),
                    "dtEmissao": $("#dtEmissao").val(),
                    "dsObservacao": $("#dsObservacao").val(),
                    "stStatus": $("#stStatus").val(),
                    "qtParcelas": $("#qtParcelas").val(),
                    "qtIntervalo": $("#qtIntervalo").val(),
                    "vlTotal": $("#vlTotal").val()
                },
                success: function (dataReturn) {
                    $("#idFinanceiro").val(dataReturn.idFinanceiro);
                    window.location.href = "/financeiro/nova_financeiro/idFinanceiro/" + dataReturn.idFinanceiro;
                }
            });
        }
    };
    
    this.escolherIS = function () {
        $('#insumo').val('');
        $('#idInsumo').val('');        
        
        if ($("#stTipoI").prop('checked')) {
            document.getElementById("labelproduto").innerHTML =
              "<label for='linsumo'>PRODUTO</label>";
            document.getElementById("labelnomeproduto").innerHTML =
              "<label for='form-control'>SUGESTAO PRODUTO</label>";
//           this.atualizaprodutos('0');
       } else {
            document.getElementById("labelproduto").innerHTML =
              "<label for='linsumo'>SERVICO</label>";
            document.getElementById("labelnomeproduto").innerHTML =
              "<label for='form-control'>SUGESTAO SERVICO</label>";
//           this.atualizaprodutos('1');
       };
       
       financeiro.autocomplete.produto();
       $('#insumo').focus();
       
    };
    this.editarParcela = function (idFinanceiro, idFinanceiroParcela) {
        jQuery.ajax({
            async: false,
            type: "post",
            dataType: "html",
            url: "/financeiro/ler_dados",
            data: {
                "idFinanceiro": idFinanceiro,
                "idFinanceiroParcela": idFinanceiroParcela                
            },
            complete: function (event, XMLHttpRequest) {
                if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                    try {
                        $('#parcelas_show').each(function () {
                            $('.modal-body', this).html(event.responseText);
                            $(this).modal('show');
                        });
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        });
    };
    
    this.mudarstatus = function () {        
        if (!verificarerrosM()) {
            $.ajax({
                url: "/financeiromanutencao/gravar_financeiro",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idFinanceiro": $("#idFinanceiro").val(),
                    "dsObservacao": $("#dsObservacao").val(),
                    "dsLocalEntrega": $("#dsLocalEntrega").val(),
                    "idSituacao": $("#idSituacao").val()
                },
                success: function (dataReturn) {
                    $("#idFinanceiro").val(dataReturn.idFinanceiro);
                    location.reload(); 
                }
            });
        }
    };
    this.lerusuario = function () {
        $.ajax({
            url: "/financeiro/lerusuario",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idUsuario": $("#idUsuario").val()
            },
            success: function (dataReturn) {
                $("#dsSolicitante").val((dataReturn.nomeusuario));
            }
        });
    };
    
    this.lerParceiro = function () {
        $.ajax({
            url: "/financeiro/lerParceiro",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idParceiro": $("#idParceiro").val()
            },
            success: function (dataReturn) {
                $("#dsParceiroSugerido").val((dataReturn.nomeParceiro));
            }
        });
    };
    
    this.gravaritem = function () {
        if (!verificarerrosI()) {
            if ($("#stTipoI").prop('checked')) {
               opcao = '0';
            } else {
               opcao = '1';
            }

            $.ajax({
                url: "/financeiro/gravar_item",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idFinanceiro": $("#idFinanceiro").val(),
                    "idFinanceiroItem": $("#idFinanceiroItem").val(),
                    "dsProduto": $("#dsProduto").val(),
                    "qtFinanceiro": $("#qtFinanceiro").val(),
                    "idUnidade": $("#idUnidade").val(),
                    "idInsumo": $("#idInsumo").val(),
                    "idParceiro": $("#idParceiro").val(),
                    "dsObservacao": $("#dsObservacaoItem").val(),
                    "idCentroCusto": $("#idCentroCusto").val(),
                    "idItemDR": $("#idItemDR").val(),
                    "dsJustificativa": $("#dsJustificativa").val(),
                    "idGrupoDR": $("#idGrupoDR").val(),
                    "dsParceiroSugerido": $("#dsParceiroSugerido").val(),
                    "opcao": opcao,
                    "dsLink": $("#dsLink").val()
                },
                success: function (dataReturn) {
                    $("#idFinanceiro").val(dataReturn.idFinanceiro);
                    $("#idFinanceiroItem").val(dataReturn.idFinanceiroItem);
                    $("#dsProduto").val('');
                    $("#qtFinanceiro").val('');
                    $("#idUnidade").val('');
                    $("#idInsumo").val('');
                    $("#idParceiro").val('');
                    $("#dsObservacaoItem").val('');
                    $("#dsParceiroSugerido").val('');
                    $("#dsLink").val('');
                    $("#idCentroCusto").val('');
                    $("#idItemDR").val('');
                    $("#dsJustificativa").val('');
                    $("#idGrupoDR").val('');                
                    location.reload(); // '/financeiro/novo_financeiro/idPedido/' . dataReturn.idPedido
                }
            });
        }
    };
    
    this.gravaritem_manutencao = function () {
        if (!verificarerrosIM()) {        
            if ($("#stTipoI").prop('checked')) {
               opcao = '0';
            } else {
               opcao = '1';
            }

            $.ajax({
                url: "/financeiromanutencao/gravar_item",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idFinanceiroItem": $("#idFinanceiroItem").val(),
                    "idFinanceiro": $("#idFinanceiro").val(),
                    "idParceiro": $("#idParceiro").val(),
                    "idInsumo": $("#idInsumo").val(),
                    "dsObservacaoItem": $("#dsObservacaoItem").val(),
                    "opcao": opcao,
                    "idSituacao": $("#idSituacao1").val()
                },
                success: function (dataReturn) {
                    $("#dsProduto").val('');
                    $("#qtFinanceiro").val('');
                    $("#idUnidade").val('');
                    $("#idInsumo").val('');
                    $("#idParceiro").val('');
                    $("#dsObservacaoItem").val('');
                    $("#dsParceiroSugerido").val('');
                    $("#dsProduto").val('');
                    $("#dsLink").val('');
                    $("#idCentroCusto").val('');
                    $("#idItemDR").val('');
                    $("#dsJustificativa").val('');
                    $("#idGrupoDR").val('');                
                    location.reload(); // '/financeiro/novo_financeiro/idPedido/' . dataReturn.idPedido
                }
            });
        }
    };
    this.gravarfinanceiro = function () {
        $.ajax({
            url: "/financeiro/gravar_financeiro",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idPedido": $("#idPedido").val(),
                "dtPrevisaoEntrega": $("#dtPrevisaoEntrega").val(),
                "dtPrimeiroVencimento": $("#dtPrimeiroVencimento").val(),
                "qtParcelas": $("#qtParcelas").val(),
                "dsObservacao": $("#dsObservacao").val()
            },
            success: function (dataReturn) {
                $("#idFinanceiro").val(dataReturn.idFinanceiro);
                location.reload('/financeiro/financeiro'); 
            }
        });
    };
    this.novofinanceiro = function () {
        $.ajax({
            url: "/financeiro/novofinanceiro",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idPedido": $("#idPedido").val()
            },
            success: function (dataReturn) {
                $("#idPedido").val(dataReturn.idPedido);   
                location.reload();
            }
        });
    };
    this.lerunidade = function () {
        if ($("#stTipoI").prop('checked')) {
           opcao = '0';
        } else {
           opcao = '1';
        }
               
        $.ajax({
            url: "/financeiro/lerunidade",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idInsumo": $("#idInsumo").val(),
                "opcao": opcao
            },
            success: function (dataReturn) {
                $("#idUnidade").val(dataReturn.idUnidade);
                $("#dsProduto").val(dataReturn.dsProduto);
            }
        });
    };
    this.desabilitaid = function() {
        $.ajax({
            url: "/financeiro/desabilitaid",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idPedido": $("#idPedido").val()
            },
            success: function () {
                location.reload('/dashboard');  
            }
        });
        return true;
    };
    this.delfinanceiroitem = function(idFinanceiroItem) {
        if (confirm('DESEJA EXCLUIR ESTE ITEM DA SOLICITACAO DE COMPRAS?')) {        
            $.ajax({
                url: "/financeiro/delfinanceiroitem",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idFinanceiroItem": idFinanceiroItem
                },
                success: function (dataReturn) {
                    location.reload();  
                }
            });
        }
    };
    this.delfinanceiroitem = function(item) {
        $.ajax({
            url: "/financeiro/delfinanceiroitem",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idFinanceiroParcela": item
            },
            success: function (dataReturn) {
                location.reload();  
            }
        });
    };
};
verificarerros = function() {
    var err = false;
    if ($('#dsSolicitante').val() == '') {
        showMessage('Favor digitar o solicitante');
        err = true;
    }
    if ($('#idPrioridade').val() == '') {
        showMessage('Favor escolher a prioridade');        
        err = true;
    }
//    if ($('#dsLocalEntrega').val() == '') {
//        showMessage('Favor digitar o local de entrega');
//        err = true;
//    }
    return err;
}
verificarerrosM = function() {
    var err = false;
    if ($('#idSituacao').val() == '') {
        showMessage('Favor escolher a situacao');        
        err = true;
    }
//    if ($('#dsLocalEntrega').val() == '') {
//        showMessage('Favor digitar o local de entrega');
//        err = true;
//    }
    return err;
}
verificarerrosI = function() {
    err = verificarerros();
    if ($('#dsProduto').val() == '') {
        showMessage('Favor digitar ou escolher um produto');        
        err = true;
    }
    if ($('#qtFinanceiro').val() == '' || $('#qtFinanceiro').val() == '0,00') {
        showMessage('Favor digitar a quantidade solicitada');
        err = true;
    }
    if ($('#idUnidade').val() == '') {
        showMessage('Favor escolher a unidade');        
        err = true;
    }
    if ($('#dsParceiroSugerido').val() == '' && $('#idParceiro').val() == '') {
        showMessage('Favor sugerir um Parceiro');        
        err = true;
    }
    if ($('#idCentroCusto').val() == '') {
        showMessage('Favor escolher um Centro de Custo para o destino do Produto/Servico');        
        err = true;
    }
    if ($('#idGrupoDR').val() == '') {
        showMessage('Favor escolher um Grupo de Despesa para o Produto/Servico');        
        err = true;
    }
    if ($('#idItemDR').val() == '') {
        showMessage('Favor escolher um Item de Despesa para o Produto/Servico');        
        err = true;
    }
    if ($('#dsJustificativa').val() == '') {
        showMessage('Favor digiar a justificativa');        
        err = true;
    }
    
    return err;
}
verificarerrosIM = function() {
    err = verificarerrosI();
    if ($('#idSituacao').val() == '') {
        showMessage('Favor escolher a situacao');        
        err = true;
    }    
    if ($('#idSituacao1').val() == '') {
        showMessage('Favor escolher a situacao para o ítem da financeiro');        
        err = true;
    }
    return err;
}
