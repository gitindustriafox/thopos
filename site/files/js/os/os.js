$(document).ready(function () {
    
    $.datepicker.setDefaults({
        defaultDate: null,
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd/mm/yy",
        timeFormat: "hh:mm",
        interval: 15,
        showMinute: true,
        pick12HourFormat: true,
        orientation: 'top left',
        pickTime: true
    });
    
    $("#dtOS").datetimepicker({
        language: 'pt-BR',
        i18n: {
            pt: {
                months: [
                    'Janeiro', 'Fevereiro', 'Março', 'Abril',
                    'Maio', 'Junho', 'Julho', 'Agosto',
                    'Setembro', 'Outubro', 'Novembro', 'Dezembro'
                ],
                dayOfWeek: [
                    "Dom", "Seg", "Ter", "Qua",
                    "Qui", "Sex", "Sab"
                ]
            }
        },
        format: 'd/m/Y H:i'
    });
    
    $("#dtInicio").datetimepicker({
        language: 'pt-BR',
        i18n: {
            pt: {
                months: [
                    'Janeiro', 'Fevereiro', 'Março', 'Abril',
                    'Maio', 'Junho', 'Julho', 'Agosto',
                    'Setembro', 'Outubro', 'Novembro', 'Dezembro'
                ],
                dayOfWeek: [
                    "Dom", "Seg", "Ter", "Qua",
                    "Qui", "Sex", "Sab"
                ]
            }
        },
        format: 'd/m/Y H:i'
    });
    
    $("#dtFim").datetimepicker({
        language: 'pt-BR',
        i18n: {
            pt: {
                months: [
                    'Janeiro', 'Fevereiro', 'Março', 'Abril',
                    'Maio', 'Junho', 'Julho', 'Agosto',
                    'Setembro', 'Outubro', 'Novembro', 'Dezembro'
                ],
                dayOfWeek: [
                    "Dom", "Seg", "Ter", "Qua",
                    "Qui", "Sex", "Sab"
                ]
            }
        },
        format: 'd/m/Y H:i'
    });
    
    $("#vlOS").priceFormat({
        prefix: "R$",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    });
    
    $.datetimepicker.setLocale('pt');
    
});

var os = new function () {
    this.verhoras = function () {
        $.ajax({
            url: "/os/verhoras",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "dtInicio": $("#dtInicio").val(),
                "dtFim": $("#dtFim").val()
            },
            success: function (dataReturn) {
                $("#mostraragendacompleta").html(dataReturn.html);
            }
        });
    };
    this.novoos = function () {
        $.ajax({
            url: "/os/novoos",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idOS": $("#idOS").val()
            },
            success: function (dataReturn) {
                $("#idOS").val(dataReturn.idOS);   
                location.reload();
            }
        });
    };
    
    this.delOS = function(idOS) {
        if (confirm('DESEJA EXCLUIR ESTA OS?')) {        
            $.ajax({
                url: "/os/delOS",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idOS": idOS
                },
                success: function (dataReturn) {
                    location.reload();  
                }
            });
        };
    };
    
    this.delFoto = function(idDocumento, idTabela) {
        if (confirm('DESEJA EXCLUIR ESTA FOTO?')) {        
            $.ajax({
                url: "/os/delFoto",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idOS": idTabela,
                    "idDocumento": idDocumento
                },
                success: function (dataReturn) {
                    $("#mostrarfotos").html(dataReturn.html);
                }
            });
        };
    };
    
    this.delOSColaborador = function(idOS, idColaborador) {
        if (confirm('DESEJA EXCLUIR ESTE COLABORADOR?')) {        
            $.ajax({
                url: "/os/delOSColaborador",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "dtInicio": $("#dtInicio").val(),
                    "dtFim": $("#dtFim").val(),
                    "idOS": idOS,
                    "idColaborador": idColaborador
                },
                success: function (dataReturn) {
                    $("#mostrarcolaboradoresos").html(dataReturn.html);
                }
            });
        };
    };
    this.reservarColaborador = function(idColaborador, dsColaborador) {
        document.getElementById("labelcolaborador").innerHTML ='Selecionar o Colaborador: ' + dsColaborador + ' para esta O.S.';
        $("#idColaboradorEscolhido").val(idColaborador);
        $("#btnReservar").removeAttr('disabled');
        $("#datainicion").removeAttr('readonly');
        $("#datafinaln").removeAttr('readonly');
    };
    this.selecionaColaborador = function() {
        var idColaborador = $("#idColaboradorEscolhido").val();
        $.ajax({
            url: "/os/editarColaborador",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idColaborador":  idColaborador,
                "idOS": $("#idOS").val(),
                "dtInicio": $("#datainicion").val(),
                "dtFim": $("#datafinaln").val()
            },
            success: function (dataReturn) {
                location.reload();                 
            }
        });    
    };

    this.closejanela = function () {
        $('#osfotos_show').modal('hide');
    }
    
//    this.adicionarfoto = function(idOS, dsTabela) {
//        var uploadArquivo = $("#arquivo");        
////        $.ajax({
////            url: "/os/adicionarfoto",
////            dataType: "json",
////            async: false,
////            type: "POST",
////            data: {
////                "idOS":  idOS,
////                "dsTabela":  dsTabela,
////                "arquivo":  $('arquivo').val()
////            },
////            success: function (dataReturn) {
////                $('#mostrarfotos').html(dataReturn.html);        
////            }
////        });    
////        
////        
//        
//        uploadArquivo.on('change', function(e) {
//          files = e.target.files;
//          var formData = new FormData(),
//            file = [];
//
//          $.each(files, function(key, val) {
//            file[key] = val;
//          });
//
//          formData.append('file', file);
//
//          $.ajax({
//            url: '/os/adicionarfoto',
//            cache: false,
//            contentType: false,
//            processData: false,
//            data: formData,
//            type: 'post',
//            success: function (dataReturn) {
//                $('#mostrarfotos').html(dataReturn.html);        
//            }
//          });
//
//        });        
//    };

    this.selecionarfoto=function(idOS) {
        jQuery.ajax({
            async: false,
            type: "post",
            dataType: "html",
            url: "/os/importarfotos",
            data: {
                "idOS": idOS,
                "dsTabela": 'prodOS'
            },
            complete: function (event, XMLHttpRequest) {
                if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                    try {
                        $('#osfotos_show').each(function () {
                            $('.modal-body', this).html(event.responseText);
                            $(this).modal('show');
                        });
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        });
    };
    
};