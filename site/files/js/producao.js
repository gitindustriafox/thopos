$(document).ready(function () {
    $("#frm-busca-estoque [type=submit]").click(function(){
        if ($(this).hasClass('buscar')){
            $("#frm-busca-estoque").attr('action', '/listagemestoque/busca_estoque');
            $("#frm-busca-estoque").submit();
        } else {
        if ($(this).hasClass('csv')){
            $("#frm-busca-estoque").attr('action', '/listagemestoque/busca_criarcsv');
            $("#frm-busca-estoque").submit();
        }          
        }          
    });
      $("#frm-busca-movimento-total [type=submit]").click(function(){
        if ($(this).hasClass('buscar')){
            $("#frm-busca-movimento-total").attr('action', '/listamovimentoestoquetotal/busca_movimento');
            $("#frm-busca-movimento-total").submit();
        } else {
        if ($(this).hasClass('csv')){
            $("#frm-busca-movimento-total").attr('action', '/listamovimentoestoquetotal/busca_criarcsv');
            $("#frm-busca-movimento-total").submit();
        }          
        }          
    });
      
    $.datepicker.setDefaults({
        defaultDate: null,
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd/mm/yy"
    });
    $("#dtFim").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtFim").val("");
        }
    });
    $("#dtInicio").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtInicio").val("");
        }
    });
    util.styleTitle();    
    
});

$(function () {
    util.styleTitle();    
});
