$(function () {
    reuniao.autocomplete.Local();   
});

$(document).ready(function () {
    
    $(".panel-item div.panel-body").hide();
    $("div.panel-heading").bind("click", function () {
        $(this).next().slideToggle('slow');
        return false;
    });
    
    reuniao.autocomplete.Local();
    
    $.datepicker.setDefaults({
        defaultDate: null,
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd/mm/yy",
        timeFormat: "hh:mm",
        interval: 15,
        showMinute: true,
        pick12HourFormat: true,
        orientation: 'top left',
        pickTime: true
    });
    
    $("#dtReuniao").datetimepicker({
        language: 'pt-BR',
        i18n: {
            pt: {
                months: [
                    'Janeiro', 'Fevereiro', 'Março', 'Abril',
                    'Maio', 'Junho', 'Julho', 'Agosto',
                    'Setembro', 'Outubro', 'Novembro', 'Dezembro'
                ],
                dayOfWeek: [
                    "Dom", "Seg", "Ter", "Qua",
                    "Qui", "Sex", "Sab"
                ]
            }
        },
        format: 'd/m/Y H:i'
    });
    $("#dtRealizacao").datetimepicker({
        language: 'pt-BR',
        i18n: {
            pt: {
                months: [
                    'Janeiro', 'Fevereiro', 'Março', 'Abril',
                    'Maio', 'Junho', 'Julho', 'Agosto',
                    'Setembro', 'Outubro', 'Novembro', 'Dezembro'
                ],
                dayOfWeek: [
                    "Dom", "Seg", "Ter", "Qua",
                    "Qui", "Sex", "Sab"
                ]
            }
        },
        format: 'd/m/Y H:i'
    });
    $("#dtPrazo").datetimepicker({
        orientation: 'top',
        language: 'pt-BR',
        i18n: {
            pt: {
                months: [
                    'Janeiro', 'Fevereiro', 'Março', 'Abril',
                    'Maio', 'Junho', 'Julho', 'Agosto',
                    'Setembro', 'Outubro', 'Novembro', 'Dezembro'
                ],
                dayOfWeek: [
                    "Dom", "Seg", "Ter", "Qua",
                    "Qui", "Sex", "Sab"
                ]
            }
        },
        format: 'd/m/Y H:i'
    });
    $("#dtIntermediaria").datetimepicker({
        language: 'pt-BR',
        i18n: {
            pt: {
                months: [
                    'Janeiro', 'Fevereiro', 'Março', 'Abril',
                    'Maio', 'Junho', 'Julho', 'Agosto',
                    'Setembro', 'Outubro', 'Novembro', 'Dezembro'
                ],
                dayOfWeek: [
                    "Dom", "Seg", "Ter", "Qua",
                    "Qui", "Sex", "Sab"
                ]
            }
        },
        format: 'd/m/Y H:i'
    });
    $("#dtInicio").datetimepicker({
        language: 'pt-BR',
        i18n: {
            pt: {
                months: [
                    'Janeiro', 'Fevereiro', 'Março', 'Abril',
                    'Maio', 'Junho', 'Julho', 'Agosto',
                    'Setembro', 'Outubro', 'Novembro', 'Dezembro'
                ],
                dayOfWeek: [
                    "Dom", "Seg", "Ter", "Qua",
                    "Qui", "Sex", "Sab"
                ]
            }
        },
        format: 'd/m/Y H:i'
    });
    $("#dtFim").datetimepicker({
        language: 'pt-BR',
        i18n: {
            pt: {
                months: [
                    'Janeiro', 'Fevereiro', 'Março', 'Abril',
                    'Maio', 'Junho', 'Julho', 'Agosto',
                    'Setembro', 'Outubro', 'Novembro', 'Dezembro'
                ],
                dayOfWeek: [
                    "Dom", "Seg", "Ter", "Qua",
                    "Qui", "Sex", "Sab"
                ]
            }
        },
        format: 'd/m/Y H:i'
    });
    $.datetimepicker.setLocale('pt');
});

var reuniao = new function () {
    this.autocomplete = new function () {
        this.Local = function () {
            $('.complete_Local').autocomplete({
                source: "/reuniao/getLocal",
                minLength: 3,
                select: function (event, ui) {
                    var retorno = $(this).attr('data-label');
                    $('#idLocal').val(ui.item.id);
                    $("#dsLocal").val(ui.item.value);
                }
            });
            $(".complete_Local").bind("blur keyup keydow keypress", function () {
                if ($("#dsLocal" + $(this).attr("data-label")).val() === "") {
                    $('#idLocal' + $(this).attr("data-label")).val("");
                }
            }); 
        };
    };
    this.criarPDF = function () {
        $.ajax({
            url: "/reuniao/emitir_pdf",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idReuniao": $('#idReuniao').val()
            },
            success: function (dataReturn) {
                location.reload();  
            }
        });
    };
    this.menuReuniao = function () {
        $.ajax({
            url: "/menuReuniao/index_action",
            dataType: "json",
            async: false,
            type: "POST"
        });
    };
    
    this.lerUsuario = function () {
        $.ajax({
            url: "/reuniao/lerUsuario",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idUsuario": $('#idUsuario').val()
            },
            success: function (dataReturn) {
                $('#dsNome').val(dataReturn.dsNome);  
                $('#dsEmail').val(dataReturn.dsEmail);  
                $('#dsCelular').val(dataReturn.dsCelular);  
            }
        });
    };
    this.montarGrafico =  function () {
        jQuery.ajax({
            async: true,
            type: "post",
            dataType: "json",
            url: "/reuniao/grafico",
            success: function (dataReturn) {
                $('#grafico_show').modal('show');                                    
                var dados = [];
                dados = dataReturn.tarefas;

                google.charts.load("current", {packages:["corechart"]});
                google.charts.setOnLoadCallback(drawChart);

                function drawChart () {
                    var data = google.visualization.arrayToDataTable(dados,false);
                    var options = {
                        width: 1200,
                        height: 400,
                        title: 'Tarefas por período',
                        is3D: true
                    };
                    var chart = new google.visualization.LineChart(document.getElementById('piechart_3d'));
                    chart.draw(data, options);                                
                }
            }
        });
    };
    
    this.montarResumo =  function () {
        jQuery.ajax({
            async: false,
            type: "post",
            dataType: "html",
            url: "/reuniao/montarResumo",
            complete: function (event, XMLHttpRequest) {
                if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                    try {
                        $('#reuniaoresumo_show').each(function () {
                            $('.modal-body', this).html(event.responseText);
                            $(this).modal('show');
                        });
                    } catch (e) {
                        console.log(e);
                    }
                }
                
            }
        });
    };
    
    this.closejanela = function () {
        $('#reuniaofotos_show').modal('hide');
    };
    
    this.closejanelaDatas = function () {
        $('#reuniaodatas_show').modal('hide');
    };
    
    this.closejanelaResumo = function () {
        $('#reuniaoresumo_show').modal('hide');
    };
    
    this.adicionaritem = function () {
        if (verificaerrosItem()) {    
            $.ajax({
                url: "/reuniao/adicionaritem",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idReuniao": $("#idReuniao").val(),
                    "idParticipante": $("#idParticipante").val(),
                    "idUsuario": $("#idUsuario").val(),
                    "dsNome": $("#dsNome").val(),
                    "dsCelular": $("#dsCelular").val(),
                    "dsEmail": $("#dsEmail").val(),
                    "dsAssunto": $("#dsAssuntoE").val()
                },
                success: function (dataReturn) {
                    $("#dsAssuntoE").val('');
                    $("#idUsuario").val('');
                    $("#idParticipante").val('');
                    $("#dsNome").val('');
                    $("#dsCelular").val('');
                    $("#dsEmail").val('');
                    $('#mostraritens').html(dataReturn.html);
                    $('#idUsuario').html(dataReturn.htmlU);                    
                    $("#idUsuario").focus();
                }
            });        
        }
    };
    
    this.salvardatas = function (idTarefa, origem) {        
        if (verificaerrosData()) {    
            $.ajax({
                url: "/reuniao/adicionardata",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idTarefa": idTarefa,
                    "idData": $("#idData").val(),
                    "dtIntermediaria": $("#dtIntermediaria").val(),
                    "dsSubTarefa": $("#dsSubTarefa").val(),
                    "origem": $("#origem").val()
                },
                success: function (dataReturn) {
                    $('#mostrarlinhas').html(dataReturn.html);
                    $("#dtIntermediaria").val('');
                    $("#dsSubTarefa").val('');
                    $("#botao_sair").focus();
                    $('#idData').val('');                    
                }
            });        
        }
    };
    
    this.Confirmar = function (idReuniao) {
        $.ajax({
            url: "/reuniao/confirmar",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idReuniao": idReuniao
            },
            success: function (dataReturn) {
                $('#presenca').html(dataReturn.html);
            }
        });
    };
    
    this.NaoConfirmar = function (idReuniao) {
        $.ajax({
            url: "/reuniao/naoconfirmar",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idReuniao": idReuniao
            },
            success: function (dataReturn) {
                $('#presenca').html(dataReturn.html);
            }
        });
    };
    
    this.gravar_ata_reuniao = function () {
        var serialize = $('#frm-reuniao').serialize();
        util.request({
            url: '/reuniao/gravar_ata_reuniao',
            data: serialize
        });
        showMessage('Dados salvos com sucesso.', null, 'success');
        window.location.href = '/reuniao/novo_ata/idReuniao/' + $('#idReuniao').val() + '/opcao/inciar/voltarmenu/0';
    };

    this.gravar_reuniao = function () {
        $.ajax({
            url: "/reuniao/gravar_reuniao",
            dataType: "json",
            async: false,
            type: "POST",
            data: $('#frm-reuniao_nova').serialize(),
            success: function (dataReturn) {
                showMessage('Dados salvos com sucesso.', null, 'success');
                window.location.href = '/reuniao/novo_reuniao/idReuniao/' + dataReturn.id + '/opcao/marcar';
            }
        });        
    };

    this.adiar_reuniao = function () {
        $.ajax({
            url: "/reuniao/gravar_adiar_reuniao",
            dataType: "json",
            async: false,
            type: "POST",
            data: $('#frm-adiar-reuniao').serialize(),
            success: function (dataReturn) {
                showMessage('Dados salvos com sucesso.', null, 'success');
                window.location.href = '/reuniao/adiar_reuniao/idReuniao/' + dataReturn.id + '/opcao/adiar';
            }
        });        
    };

    this.cancelar_reuniao = function () {
        $.ajax({
            url: "/reuniao/gravar_cancelar_reuniao",
            dataType: "json",
            async: false,
            type: "POST",
            data: $('#frm-cancelar-reuniao').serialize(),
            success: function (dataReturn) {
                showMessage('Dados salvos com sucesso.', null, 'success');
                window.location.href = '/reuniao/cancelar_reuniao/idReuniao/' + dataReturn.id + '/opcao/cancelar';
            }
        });        
    };

    this.excluiranexo = function (idDocumento) {
        $.ajax({
            url: "/reuniao/excluiranexo",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idDocumento": idDocumento,
                "idReuniao": $('#idReuniao').val(),
                "idParticipante": $('#idParticipante').val(),
                "dsTabela": $('#dsTabela').val()
                
            },
            success: function (dataReturn) {
                $('#mostrarlinhas').html(dataReturn.html);
            }
        });        
    };
    
    this.adicionarTarefa = function (origem) {
        if (verificaerrosTarefa()) {    
            $.ajax({
                url: "/reuniao/adicionartarefa",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idReuniao": $("#idReuniao").val(),
                    "idTarefa": $("#idTarefa").val(),
                    "idUsuarioTarefa": $("#idUsuarioTarefa").val(),
                    "dsTarefa": $("#dsTarefa").val(),
                    "dtPrazo": $("#dtPrazo").val(),
                    "origem": origem
                },
                success: function (dataReturn) {
                    $("#idUsuarioTarefa").val('');
                    $("#dsTarefa").val('');
                    $("#dtPrazo").val('');
                    $('#mostraritenstarefa').html(dataReturn.html);
                    $("#idUsuarioTarefa").focus();
                }
            });        
        }
    };
    
    this.concluirTarefa = function (idTarefa) {
        $.ajax({
            url: "/reuniao/concluirtarefa",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idTarefa": idTarefa
            },
            success: function (dataReturn) {
                location.reload();  
            }
        });
    };
    
    this.concluirSubTarefa = function (idData, idTarefa, origem, idUsuario) {
        $.ajax({
            url: "/reuniao/concluirsubtarefa",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idData": idData,
                "origem": origem,
                "idUsuario": idUsuario,
                "idTarefa": idTarefa
            },
            success: function (dataReturn) {
                $('#mostrarlinhas').html(dataReturn.html);
                $('#idData').val('');                
            }
        });
    };
    
    this.adicionaritemAta = function () {
        if (verificaerrosItem()) {    
            $.ajax({
                url: "/reuniao/adicionaritemAta",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idReuniao": $("#idReuniao").val(),
                    "idParticipante": $("#idParticipante").val(),
                    "idUsuario": $("#idUsuario").val(),
                    "dsNome": $("#dsNome").val(),
                    "dsCelular": $("#dsCelular").val(),
                    "dsEmail": $("#dsEmail").val(),
                    "dsAssunto": $("#dsAssuntoE").val()
                },
                success: function (dataReturn) {
                    $("#dsAssuntoE").val('');
                    $("#idUsuario").val('');
                    $("#idParticipante").val('');                    
                    $("#dsNome").val('');                    
                    $("#dsCelular").val('');                    
                    $("#dsEmail").val('');                    
                    $('#mostraritens').html(dataReturn.html);
                    $('#idUsuario').html(dataReturn.htmlU);
                    $('#idUsuarioTarefa').html(dataReturn.htmlUA);
                    $("#idUsuario").focus();
                }
            });        
        }
    };
    
    this.delitem = function (idReuniao, idParticipante) {
        if (confirm('DESEJA EXCLUIR ESTE PARTICIPANTE?')) {        
            $.ajax({
                url: "/reuniao/delparticipante",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idReuniao": idReuniao,
                    "idParticipante": idParticipante
                },
                success: function (dataReturn) {
                    $('#mostraritens').html(dataReturn.html);
                    showMessage(dataReturn.ok);
                    $("#idUsuario").focus();
                }
            });        
        };
    };
    
    this.excluirdataintermediaria = function (idData, idTarefa, origem, idUsuario) {
        if (confirm('DESEJA EXCLUIR ESTA DATA INTERMEDIARIA?')) {        
            $.ajax({
                url: "/reuniao/deldata",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idData": idData,
                    "idTarefa": idTarefa,
                    "origem": origem,
                    "idUsuario": idUsuario
                },
                success: function (dataReturn) {
                    $('#mostrarlinhas').html(dataReturn.html);
                    $('#idData').val('');
                }
            });        
        };
    };
    
    this.editardataintermediaria = function (idData, idTarefa, dtIntermediaria, dsSubTarefa) {
         var novadata = dtIntermediaria.substr(8,2) + '/' + dtIntermediaria.substr(5,2) + '/' + dtIntermediaria.substr(0,4)  + ' ' + dtIntermediaria.substr(11,5);
         $('#idData').val(idData);
         $('#idTarefa').val(idTarefa);
         $('#dtIntermediaria').val(novadata);
         $('#dsSubTarefa').val(dsSubTarefa);
    };
    
    this.excluirtarefa = function (idReuniao, idTarefa) {
        if (confirm('DESEJA EXCLUIR ESTA TAREFA?')) {
            $.ajax({
                url: "/reuniao/deltarefa",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idReuniao": idReuniao,
                    "idTarefa": idTarefa
                },
                success: function (dataReturn) {
                    $('#mostraritenstarefa').html(dataReturn.html);
                    showMessage(dataReturn.ok);
                    $("#idUsuarioTarefa").focus();
                }
            });        
        };
    };
    
    this.presente = function (idReuniao, idParticipante, idUsuario) {
        $.ajax({
            url: "/reuniao/presente",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idReuniao": idReuniao,
                "idParticipante": idParticipante,
                "idUsuario": idUsuario
            },
            success: function (dataReturn) {
                $('#mostraritens').html(dataReturn.html);
                showMessage(dataReturn.ok);
                $("#idUsuario").focus();
            }
        });        
    };
    
    this.ausente = function (idReuniao, idParticipante, idUsuario) {
        $.ajax({
            url: "/reuniao/ausente",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idReuniao": idReuniao,
                "idParticipante": idParticipante,
                "idUsuario": idUsuario
            },
            success: function (dataReturn) {
                $('#mostraritens').html(dataReturn.html);
                showMessage(dataReturn.ok);
                $("#idUsuario").focus();
            }
        });        
    };
    
    this.enviaremail = function (idParticipante, idReuniao, deOnde) {
        $.ajax({
            url: "/reuniao/envairemail",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idReuniao": idReuniao,
                "idParticipante": idParticipante,
                "deOnde": deOnde
            },
            success: function (dataReturn) {
                showMessage(dataReturn.ok);
            }
        });        
    };
    
    this.editarparticipante = function (idReuniao, idParticipante, idUsuario, dsAssunto, dsUsuario,dsNome,dsEmail,dsCelular) {
         $('#idUsuario').append('<option value="' + idUsuario + '">' + dsUsuario + '</option>');
         $('#idUsuario').val(idUsuario);
         $('#dsNome').val(dsNome);
         $('#dsEmail').val(dsEmail);
         $('#dsCelular').val(dsCelular);
         $('#dsAssuntoE').val(dsAssunto);
         $('#idParticipante').val(idParticipante);
    };
    
    this.editartarefa = function (idReuniao, idTarefa, idUsuario, dsTarefa, dtPrazo) {
         $('#idUsuarioTarefa').val(idUsuario);
         $('#idTarefa').val(idTarefa);
         $('#dsTarefa').val(dsTarefa);
         $('#dtPrazo').val(dtPrazo);
    };

    this.selecionarfoto=function(idParticipante, Origem) {
        jQuery.ajax({
            async: false,
            type: "post",
            dataType: "html",
            url: "/reuniao/importarfotos",
            data: {
                "idReuniao": $('#idReuniao').val(),
                "idParticipante": idParticipante,
                "dsTabela": 'prodReuniaoParticipantes',
                "Origem": Origem
            },
            complete: function (event, XMLHttpRequest) {
                if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                    try {
                        $('#reuniaofotos_show').each(function () {
                            $('.modal-body', this).html(event.responseText);
                            $(this).modal('show');
                        });
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        });
    };

    this.selecionardatas=function(idReuniao, idTarefa, origem, idUsuario) {
        jQuery.ajax({
            async: false,
            type: "post",
            dataType: "html",
            url: "/reuniao/verdatas",
            data: {
                "idReuniao": idReuniao,
                "idTarefa": idTarefa,
                "idUsuario": idUsuario,
                "origem": origem
            },
            complete: function (event, XMLHttpRequest) {
                if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                    try {
                        $('#reuniaodatas_show').each(function () {
                            $('.modal-body', this).html(event.responseText);
                            $(this).modal('show');
                        });
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        });
    };

    this.selecionarfotoR=function(Origem) {
        jQuery.ajax({
            async: false,
            type: "post",
            dataType: "html",
            url: "/reuniao/importarfotosR",
            data: {
                "idReuniao": $('#idReuniao').val(),
                "dsTabela": 'prodReuniao',
                "Origem": Origem
            },
            complete: function (event, XMLHttpRequest) {
                if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                    try {
                        $('#reuniaofotos_show').each(function () {
                            $('.modal-body', this).html(event.responseText);
                            $(this).modal('show');
                        });
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        });
    };    
 };   
 
verificaerrosItem = function() {
    var err = true;
//    if ($('#dsAssuntoE').val() == '') {
//        showMessage('Favor digitar uma descricao para o Assunto');
//        err = false;
//    }
    return err;
};       
verificaerrosData = function() {
    var err = true;
    if ($('#dtIntermediaria').val() == '') {
        showMessage('Favor digitar uma data');
        err = false;
    }
    return err;
};       
verificaerrosTarefa = function() {
    var err = true;
    if ($('#dsTarefa').val() == '') {
        showMessage('Favor digitar uma descricao para a tarefa');
        err = false;
    }
    return err;
};       
