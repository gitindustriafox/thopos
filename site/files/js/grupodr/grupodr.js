var grupodr = new function () {
    this.adicionaritem = function () {
        if (verificaerrosItem()) {    
            $.ajax({
                url: "/grupodr/adicionaritem",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idGrupoDR": $("#idGrupoDR").val(),
                    "dsItemDR": $("#dsItemDR").val()
                },
                success: function (dataReturn) {
                    $("#dsItemDR").val('');
                    $('#mostraritens').html(dataReturn.html);
                }
            });        
        }
    };
    this.delitem = function (idItemDR) {
        $.ajax({
            url: "/grupodr/delitem",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idGrupoDR": $("#idGrupoDR").val(),
                "idItemDR": idItemDR
            },
            success: function (dataReturn) {
                $('#mostraritens').html(dataReturn.html);
                showMessage(dataReturn.ok);
            }
        });        
    };
};
verificaerrosItem = function() {
    var err = true;
    if ($('#dsItemDR').val() == '') {
        showMessage('Favor digitar uma descricao para o Item');
        err = false;
    }
    return err;
}   

$(document).ready(function(){
  $('#dsGrupoDR').focus();  
});