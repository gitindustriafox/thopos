$(document).ready(function () {
    insumo.autocomplete.ncm();
    $("#frm-busca-insumo [type=submit]").click(function(){
        if ($(this).hasClass('buscar')){
            $("#frm-busca-insumo").attr('action', '/insumo/busca_insumo');
            $("#frm-busca-insumo").submit();
        } else {
        if ($(this).hasClass('csv')){
            $("#frm-busca-insumo").attr('action', '/insumo/busca_criarcsv');
            $("#frm-busca-insumo").submit();
        }     
        }     
    });
    $.datepicker.setDefaults({
        defaultDate: null,
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd/mm/yy"
    });
    $("#dtValidade").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtValidade").val("");
        }
    });
    $("#qtAcerto").priceFormat({
        prefix: "",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    });
    $("#vlAcerto").priceFormat({
        prefix: "",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    });
    $("#qtLoteReposicao").priceFormat({
        prefix: "",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    });
    $("#qtEstoqueMinimo").priceFormat({
        prefix: "",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    });    
});

$(function () {
    insumo.autocomplete.ncm();
});

var insumo = new function () {
    this.autocomplete = new function () {
        this.ncm = function () {
            $('.complete_ncm').autocomplete({
                source: "/insumo/getNCM",
                minLength: 3,
                select: function (event, ui) {
                    var retorno = $(this).attr('data-label');
                    $('#idNCM').val(ui.item.id);
                    $("#dsNCM").val(ui.item.value);
                }
            });
            $(".complete_ncm").bind("blur keyup keydow keypress", function () {
                if ($("#dsNCM" + $(this).attr("data-label")).val() === "") {
                    $('#idNCM' + $(this).attr("data-label")).val("");
                }
            }); 
        };
    };
    this.closejanela = function () {
        $('#insumofotos_show').modal('hide');
    }    
    
    this.selecionarfoto=function(idInsumo) {
        jQuery.ajax({
            async: false,
            type: "post",
            dataType: "html",
            url: "/insumo/importarfotos",
            data: {
                "idInsumo": idInsumo,
                "dsTabela": 'prodInsumo'
            },
            complete: function (event, XMLHttpRequest) {
                if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                    try {
                        $('#insumofotos_show').each(function () {
                            $('.modal-body', this).html(event.responseText);
                            $(this).modal('show');
                        });
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        });
    };
    
    this.escolherES = function () {
        if ($("#stE").prop('checked')) {
            document.getElementById("labeltm").innerHTML =
              "<label for='tipomovimento'>TIPO DE MOVIMENTO - ENTRADA</label>";
        } else {
            document.getElementById("labeltm").innerHTML =
              "<label for='tipomovimento'>TIPO DE MOVIMENTO - SAIDA</label>";
        };
        $('#idTipoMovimento').focus();
        this.carregaTipoMovimento();
    };
    
    this.foco = function () {
        $('#btn-acertoestoque').removeAttr('disabled');
    };

    this.ajustarPME = function (idEstoque) {
        $.ajax({
            url: "/insumo/ajustarPME",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idEstoque": idEstoque
            },
            success: function (dataReturn) {
                location.reload(); 
            }
        });
        
    };
    
    this.acertar = function(idLocalEstoque,dsLocalEstoqueSuperior,dsLocalEstoque, qtdeEstoque, qtEstoqueMinimo, qtLoteReposicao) {
        $('#dsLocalEstoqueAcerto').val(dsLocalEstoqueSuperior + '-' + dsLocalEstoque);
        $('#idLocalEstoqueAcerto').val(idLocalEstoque);
        $('#idLocalEstoque').val(idLocalEstoque); 
        $('#qtEstoque').val(qtdeEstoque); 
        $('#qtEstoqueMinimo').val(qtEstoqueMinimo); 
        $('#qtLoteReposicao').val(qtLoteReposicao); 
        $('#btn-acertoestoque').removeAttr('disabled');
    }
    
    this.gravaracertoestoque = function () {
        if (this.consistiracerto()) {
            var entrada_saida = ''
            if ($("#stE").prop('checked')) {
                entrada_saida = 'E';
            } else {
                entrada_saida = 'S';
            }
            $.ajax({
                url: "/insumo/gravaracertoestoque",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "entrada_saida": entrada_saida,
                    "dsMotivo": $('#dsMotivo').val(),
                    "qtAcerto": $('#qtAcerto').val(),
                    "vlAcerto": $('#vlAcerto').val(),
                    "idTipoMovimento": $('#idTipoMovimento').val(),
                    "idLocalEstoque": $('#idLocalEstoqueAcerto').val(),
                    "idInsumo": $('#idInsumo').val()
                },
                success: function (dataReturn) {
                    location.reload(); 
                }
            });
        } else {
            $('#btn-acertoestoque').attr('disabled','disabled');
        }
    }
    
    this.consistiracerto = function () {
        err = true;
        if ($('#idTipoMovimento').val() == '') {
            showMessage('Favor escolher um tipo de movimento');        
            err = false;
        }
        if ($('#dsMotivo').val() == '') {
            showMessage('Favor digitar um motivo');        
            err = false;
        }
//        if ($('#qtAcerto').val() == '') {
//            $('#qtAcerto').val('0,00');
//        }
//        if ($('#vlAcerto').val() == '') {
//            $('#vlAcerto').val('0,00');
//        }
//        if ($('#vlAcerto').val() == '0,00' &&  $('#qtAcerto').val('0,00')) {
//            showMessage('Favor digitar a quantidade ou o valor');        
//            err = false;
//        }    
        if ($('#idLocalEstoque').val() == '') {
            showMessage('Favor escolher um local de estoque - clique no botao AJUSTAR');        
            err = false;
        }
        
        return err;
    }
    
    this.carregaTipoMovimento = function () {
        var entrada_saida = ''
        if ($("#stE").prop('checked')) {
            entrada_saida = 'E';
        } else {
            entrada_saida = 'S';
        }
        
        $.ajax({
            url: "/insumo/carrega_tipo_mov",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "entrada_saida": entrada_saida
            },
            success: function (dataReturn) {
                $("#idTipoMovimento").html(dataReturn.html);
            }
        });
    };
    
    this.gravarlocalestoque = function () {
        $.ajax({
            url: "/insumo/gravar_loalestoque",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idInsumo": $("#idInsumo").val(),
                "idLocalEstoque": $("#idLocalEstoque").val(),
                "qtEstoqueMinimo": $("#qtEstoqueMinimo").val(),
                "qtLoteReposicao": $("#qtLoteReposicao").val(),
            },
            success: function (dataReturn) {
                $("#idLocalEstoque").val('');
                $("#qtEstoqueMinimo").val('');
                $("#qtLoteReposicao").val('');
                location.reload(); 
            }
        });
    };
 };   
