$(document).ready(function() {
    solicitacaocontrolados.autocomplete.Parceiro();
    //marca e desmarca todos os checkbox
    $('#selecionar_tudo').click(function () {
        if (this.checked === true) {
            $("#listagem").find('input[type=checkbox]').each(function () {
                this.checked = true;
            });
        } else {
            $("#listagem").find('input[type=checkbox]').each(function () {
                this.checked = false;
            });
        }
    });
    
    $("#frm-busca-controlados [type=submit]").click(function(){
        if ($(this).hasClass('buscar')){
            $("#frm-busca-controlados").attr('action', '/solicitacaocontrolados/busca_controlados');
            $("#frm-busca-controlados").submit();
        } else {
            if ($(this).hasClass('autorizar')){
                if (solicitacaocontrolados.consisteAutorizar()) {
                    $("#frm-busca-controlados").attr('action', '/solicitacaocontrolados/busca_autorizar');
                    $("#frm-busca-controlados").submit();
                } else {
                    $("#frm-busca-controlados").attr('action', '');
                }          
            } else {
                if (solicitacaocontrolados.consisteCriarCSV()) {
                    $("#frm-busca-controlados").attr('action', '/solicitacaocontrolados/busca_criarcsv');
                    $("#frm-busca-controlados").submit();
                } else {
                    $("#frm-busca-controlados").attr('action', '');
                }          
            }
        }
    });
});

$(function () {
    solicitacaocontrolados.autocomplete.Parceiro();
});

var solicitacaocontrolados = new function () {
    this.autocomplete = new function () {
        this.Parceiro = function () {
            $('.complete_Parceiro').autocomplete({
                source: "/solicitacaocontrolados/getParceiro",
                minLength: 3,
                select: function (event, ui) {
                    var retorno = $(this).attr('data-label');
                    $('#idParceiro').val(ui.item.id);
                    $("#dsParceiro").val(ui.item.value);
                }
            });
            $(".complete_Parceiro").bind("blur keyup keydow keypress", function () {
                if ($("#dsParceiro" + $(this).attr("data-label")).val() === "") {
                    $('#idParceiro' + $(this).attr("data-label")).val("");
                }
            }); 
        };
    };
    
    this.consisteAutorizar = function () {
        
        var stats = false;
        var errorText = 'Favor marcar pelo menos uma opção!!!';                    

        $("#listagem").find('input[type=checkbox]').each(function () {
            if(this.checked === true) {
                stats = true;
                errorText = '';
            }
        });
        
        if (errorText) {
            showMessage(errorText);   
        };
        
        return stats;        
    };
    
    this.consisteCriarCSV = function () {
        
        var stats = false;
        var errorText = 'Favor marcar pelo menos uma opção !!!';                    

        $("#listagem").find('input[type=checkbox]').each(function () {
            if(this.checked === true) {
                stats = true;
                errorText = '';
            }
        });
        
        if (errorText) {
            showMessage(errorText);   
        };
        
        return stats;        
    };
};
