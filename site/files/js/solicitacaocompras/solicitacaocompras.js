$(document).ready(function() {
    solicitacaocompras.autocomplete.Parceiro();
    solicitacaocompras.autocomplete.produto();    
    //marca e desmarca todos os checkbox
    $('#selecionar_tudo').click(function () {
        if (this.checked === true) {
            $("#listagem").find('input[type=checkbox]').each(function () {
                this.checked = true;
            });
        } else {
            $("#listagem").find('input[type=checkbox]').each(function () {
                this.checked = false;
            });
        }
    });
    
    $("#frm-busca-solicitacao [type=submit]").click(function(){
        if ($(this).hasClass('buscar')){
            $("#frm-busca-solicitacao").attr('action', '/solicitacaocomprasmanutencao/busca_solicitacaocompras');
            $("#frm-busca-solicitacao").submit();
        } else {
            if ($(this).hasClass('pedido')){
                if (solicitacaocompras.consisteCriarPedido()) {
                    $("#frm-busca-solicitacao").attr('action', '/solicitacaocomprasmanutencao/busca_criarpedido');
                    $("#frm-busca-solicitacao").submit();
                } else {
                    $("#frm-busca-solicitacao").attr('action', '');
                }          
            } else {
            if ($(this).hasClass('cotacao')){
                if (solicitacaocompras.consisteCriarCotacao()) {
                    $("#frm-busca-solicitacao").attr('action', '/solicitacaocomprasmanutencao/busca_criarcotacao');
                    $("#frm-busca-solicitacao").submit();
                } else {
                    $("#frm-busca-solicitacao").attr('action', '');
                }          
            } else {
//                if (solicitacaocompras.consisteCriarCSV()) {
                    $("#frm-busca-solicitacao").attr('action', '/solicitacaocomprasmanutencao/busca_criarcsv');
                    $("#frm-busca-solicitacao").submit();
//                } else {
//                    $("#frm-busca-solicitacao").attr('action', '');
//                }          
            }}
        }
    });

    $.datepicker.setDefaults({
        defaultDate: null,
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd/mm/yy"
    });
    $("#dtSolicitacao").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtSolicitacao").val("");
        }
    });
    $("#dtNecessidade").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtNecessidade").val("");
        }
    });
    $("#qtSolicitacao").priceFormat({
        prefix: "",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    }); 
     
    util.styleTitleLeft();    

});

$(function () {
    solicitacaocompras.autocomplete.Parceiro();
    solicitacaocompras.autocomplete.produto();
    util.styleTitleLeft();    
});

var solicitacaocompras = new function () {
    this.autocomplete = new function () {
        this.Parceiro = function () {
            $('.complete_Parceiro').autocomplete({
                source: "/solicitacaocompras/getParceiro",
                minLength: 3,
                select: function (event, ui) {
                    var retorno = $(this).attr('data-label');
                    $('#idParceiro').val(ui.item.id);
                    $("#dsParceiro").val(ui.item.value);
                }
            });
            $(".complete_Parceiro").bind("blur keyup keydow keypress", function () {
                if ($("#dsParceiro" + $(this).attr("data-label")).val() === "") {
                    $('#idParceiro' + $(this).attr("data-label")).val("");
                }
            }); 
        };
        
        this.produto = function () {
            if ($("#stTipoI").prop('checked')) {    
                tipo = "/solicitacaocompras/getProduto/insumo/";
            } else {
                tipo = "/solicitacaocompras/getProduto/servic/";
            }
            $('.complete_produto').autocomplete({
                source: tipo,
                minLength: 3,
                select: function (event, ui) {
                    var retorno = $(this).attr('data-label');
                    $('#idInsumo').val(ui.item.id);
                    $("#insumo").val(ui.item.value);
                }
            });
            $(".complete_produto").bind("blur keyup keydow keypress", function () {
                if ($("#insumo" + $(this).attr("data-label")).val() === "") {
                    $('#idInsumo' + $(this).attr("data-label")).val("");
                }
            }); 
        };
    };
    this.showiprodutos = function () {
        jQuery.ajax({
            async: false,
            type: "post",
            dataType: "html",
            url: "/solicitacaocompras/importarprodutos_mostrar",
            data: {
                "idSolicitacao": $("#idSolicitacao").val(),
                "idUnidade": $("#idUnidade").val(),
                "idParceiro": $("#idParceiro").val(),
                "dsObservacaoItem": $("#dsObservacaoItem").val(),
                "idCentroCusto": $("#idCentroCusto").val(),
                "dsParceiroSugerido": $("#dsParceiroSugerido").val()
            },
            complete: function (event, XMLHttpRequest) {
                if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                    try {
                        $('#importarprodutos_show').each(function () {
                            $('.modal-body', this).html(event.responseText);
                            $(this).modal('show');
                        });
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        });
    };
    this.clear_log = function () {
        $('#insumo_show').modal('hide');
    };        
    this.verdadosproduto = function (idInsumo) {
        jQuery.ajax({
            async: false,
            type: "post",
            dataType: "html",
            url: "/solicitacaocompras/mostrar_dados_produto",
            data: {
                "idInsumo": idInsumo
            },
            complete: function (event, XMLHttpRequest) {
                if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                    try {
                        $('#insumo_show').each(function () {
                            $('.modal-body', this).html(event.responseText);
                            $(this).modal('show');
                        });
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        });
    };
    
    this.selecionarfoto=function(idSolicitacaoItem) {
        jQuery.ajax({
            async: false,
            type: "post",
            dataType: "html",
            url: "/solicitacaocompras/importarfotos",
            data: {
                "idSolicitacao": $('#idSolicitacao').val(),
                "dsTabela": 'prodSolicitacaoComprasItens',
                "idSolicitacaoItem": idSolicitacaoItem
            },
            complete: function (event, XMLHttpRequest) {
                if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                    try {
                        $('#solicitacaocomprasfotos_show').each(function () {
                            $('.modal-body', this).html(event.responseText);
                            $(this).modal('show');
                        });
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        });
    };
    
    this.digitarmensagem=function(idSolicitacaoItem) {
        jQuery.ajax({
            async: false,
            type: "post",
            dataType: "html",
            url: "/solicitacaocompras/digitarmensagem",
            data: {
                "idSolicitacao": $('#idSolicitacao').val(),
                "dsTabela": 'prodSolicitacaoComprasItens',
                "idSolicitacaoItem": idSolicitacaoItem
            },
            complete: function (event, XMLHttpRequest) {
                if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                    try {
                        $('#mensagem_show').each(function () {
                            $('.modal-body', this).html(event.responseText);
                            $(this).modal('show');
                        });
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        });
    };
    
    this.closejanela = function () {
        $('#solicitacaocomprasfotos_show').modal('hide');
    }
    
    this.closemensagem = function () {
        $('#mensagem_show').modal('hide');
    }
    
    this.calculanecessidade = function () {
        $.ajax({
            url: "/solicitacaocompras/calcularnecessidade",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idPrioridade": $('#idPrioridade').val()
            },
            success: function (dataReturn) {
                $('#dtNecessidade').val(dataReturn.datanova);
            }
        });
        
    }
    this.lerprodutostipomov = function () {
        $.ajax({
            url: "/solicitacaocompras/lerprodutostipomov",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idTipoMovimento": $('#idTipoMovimento').val(),
                "idInsumo": $('#idInsumo').val()
            },
            success: function (dataReturn) {
                $('#linhas_movimento').html(dataReturn.html);
            }
        });
        
    }
    this.lerItemDR = function () {
        $.ajax({
            url: "/solicitacaocompras/lerItemDR",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idGrupoDR": $('#idGrupoDR').val()
            },
            success: function (dataReturn) {
                $('#idItemDR').html(dataReturn.html);
            }
        });
        
    }
    this.consisteCriarPedido = function () {
        
        var stats = false;
        var errorText = 'Favor marcar pelo menos uma opção e colocar o valor!!!';                    

        if ($('#idParceiro').val() == '' || $('#idParceiro').val() == 0) {
            stats = false;
            errorText = 'Favor digitar o fornecedor';              
        } else {
            $("#listagem").find('input[type=checkbox]').each(function () {
                if(this.checked === true) {
                    var nada = this.id.substring(10,30);
                    var conteudo = document.getElementById("valor" + nada).value;
                    var novoValor = conteudo.replace(".","");
                    var novoValor1 = novoValor.replace(",",".");
                    var valor = parseFloat(novoValor1);
                    if (valor>0) {
                        stats = true;
                        errorText = '';
                    }
                }
            });
        }
        
        if (errorText) {
            showMessage(errorText);   
        };
        
        return stats;        
    };
    this.consisteCriarCotacao = function () {
        
        var stats = false;
        var errorText = 'Favor marcar pelo menos uma opção!!!';                    

        $("#listagem").find('input[type=checkbox]').each(function () {
            if(this.checked === true) {
                stats = true;
                errorText = '';
            }
        });
        
        if (errorText) {
            showMessage(errorText);   
        };
        
        return stats;        
    };
    
    this.consisteCriarCSV = function () {
        
        var stats = false;
        var errorText = 'Favor marcar pelo menos uma opção !!!';                    

        $("#listagem").find('input[type=checkbox]').each(function () {
            if(this.checked === true) {
                stats = true;
                errorText = '';
            }
        });
        
        if (errorText) {
            showMessage(errorText);   
        };
        
        return stats;        
    };
    
    this.editarfinanceiro = function (parcela) {
        $("#dtVencimento-" + parcela).removeAttr('disabled');
        $("#vlParcela-" + parcela).removeAttr('disabled');
    };
    this.alterarfinanceiro = function (item, nrParcela) {
        $.ajax({
            url: "/solicitacao/alterar_financeiro",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idFinanceiroParcela": item,
                "dtVencimento": $("#dtVencimento-" + nrParcela).val(),
                "vlParcela": $("#vlParcela-" + nrParcela).val()
            },
            success: function (dataReturn) {
                location.reload(); 
            }
        });
    };    
    this.excluirsolicitacao = function (idSolicitacao) {
        if (confirm('DESEJA EXCLUIR ESTA SOLICITACAO DE COMPRAS?\n\
            PARA EXCLUIR APENAS UM ITEM DA SOLICITACAO, FAVOR EDITAR E EXCLUIR O \n\
            ITEM DESEJADO DENTRO DA SOLICITACAO')) {
            $.ajax({
                url: "/solicitacaocompras/delsolicitacaocompras",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idSolicitacao": idSolicitacao,
                },
                success: function (dataReturn) {
                    location.reload(); 
                }
            });
        };
    };    
    this.arquivarsolicitacao = function (idSolicitacao, idSolicitacaoItem) {
        if (confirm('DESEJA ARQUIVAR ESTE ITEM DA SOLICITACAO DE COMPRAS?')) {
            $.ajax({
                url: "/solicitacaocompras/arquivar",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idSolicitacao": idSolicitacao,
                    "idSolicitacaoItem": idSolicitacaoItem
                },
                success: function (dataReturn) {
                    location.reload(); 
                }
            });
        };
    };    
    this.atualizaunidades = function () {
        $.ajax({
            url: "/solicitacaocompras/atualiza_unidades",
            dataType: "json",
            async: false,
            type: "POST",
            success: function (dataReturn) {
                $("#idUnidade").html(dataReturn.html);
            }
        });
    };
    
    this.atualizaprodutos = function (opcao) {
        if ($("#stTipoI").prop('checked')) {
           opcao = '0';
        } else {
           opcao = '1';
        }
        
        $.ajax({
            url: "/solicitacaocompras/atualiza_produtos",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "opcao": opcao,
            },
            success: function (dataReturn) {
                $("#idInsumo").html(dataReturn.html);
            }
        });
    };

    this.atualizaParceiroes = function () {
        $.ajax({
            url: "/solicitacaocompras/atualiza_Parceiroes",
            dataType: "json",
            async: false,
            type: "POST",
            success: function (dataReturn) {
                $("#idParceiro").html(dataReturn.html);
            }
        });
    };

    this.gravarcabecalho = function () {   
        if (!verificarerros()) {
            $.ajax({
                url: "/solicitacaocompras/gravar_solicitacao",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idSolicitacao": $("#idSolicitacao").val(),
                    "dsLocalEntrega": $("#dsLocalEntrega").val(),
                    "idUsuario": $("#idUsuario").val(),
                    "dtSolicitacao": $("#dtSolicitacao").val(),
                    "dtNecessidade": $("#dtNecessidade").val(),
                    "nrSolicitacao": $("#nrSolicitacao").val(),
                    "dsObservacao": $("#dsObservacao").val(),
                    "idPrioridade": $("#idPrioridade").val(),
                    "dsSolicitante": $("#dsSolicitante").val()
                },
                success: function (dataReturn) {
                    $("#idSolicitacao").val(dataReturn.idSolicitacao);
                    location.reload(); 
                }
            });
        }
    };
    
    this.escolherIS = function () {
        $('#insumo').val('');
        $('#idInsumo').val('');        
        
        if ($("#stTipoI").prop('checked')) {
            document.getElementById("labelproduto").innerHTML =
              "<label for='linsumo'>PRODUTO</label>";
            document.getElementById("labelnomeproduto").innerHTML =
              "<label for='form-control'>SUGESTAO PRODUTO</label>";
//           this.atualizaprodutos('0');
       } else {
            document.getElementById("labelproduto").innerHTML =
              "<label for='linsumo'>SERVICO</label>";
            document.getElementById("labelnomeproduto").innerHTML =
              "<label for='form-control'>SUGESTAO SERVICO</label>";
//           this.atualizaprodutos('1');
       };
       
       solicitacaocompras.autocomplete.produto();
       $('#insumo').focus();
       
   };
    
    this.editar = function(item, opcao) {
        if (opcao == '0') {   // produtos
            document.getElementById("stTipoI").checked = true;                    
        } else {
            document.getElementById("stTipoS").checked = true;                    
        }                
        this.escolherIS();                
        $.ajax({
            url: "/solicitacaocomprasmanutencao/ler_dados",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idSolicitacaoItem": item,
                "opcao": opcao
            },
            success: function (dataReturn) {
                // limpar os dados antes                
                $("#idInsumo").val('');
                $("#dsProduto").val('');
                $("#insumo").val('');
                $("#qtSolicitacao").val('');
                $("#idUnidade").val('');
                $("#dsObservacaoItem").val('');
                $("#idParceiro").val('');
                $("#dsParceiroSugerido").val('');
                $("#dsLink").val('');
                $("#idSituacao1").val('');
                $("#idCentroCusto").val('');
                $("#idGrupoDR").val('');
                $("#idItemDR").val('');
                $("#dsJustificativa").val('');                
                
                // carregar os dados
                $("#idSolicitacaoItem").val(dataReturn.dados.idSolicitacaoItem);
                $("#idInsumo").val(dataReturn.dados.idInsumo);
                if (dataReturn.dados.stTipoIS == '0') {
                    $("#insumo").val(dataReturn.dados.dsInsumo);
                } else {
                    $("#insumo").val(dataReturn.dados.dsServico);                    
                }
                $("#idUnidade").val(dataReturn.dados.idUnidade);
                $("#dsObservacaoItem").val(dataReturn.dados.dsObservacao);
                $("#idParceiro").val(dataReturn.dados.idParceiro);
                $("#dsParceiroSugerido").val(dataReturn.dados.dsParceiroSugerido);
                $("#dsParceiro").val(dataReturn.dados.dsParceiro);
                $("#dsProduto").val(dataReturn.dados.dsProduto);
                $("#dsLink").val(dataReturn.dados.dsLink);
                $("#idSituacao1").val(dataReturn.dados.idSituacao);
                $("#idCentroCusto").val(dataReturn.dados.idCentroCusto);
                $("#idGrupoDR").val(dataReturn.dados.idGrupoDR);
                $('#idItemDR').html(dataReturn.html);
                $("#idItemDR").val(dataReturn.dados.idItemDR);
                $("#dsJustificativa").val(dataReturn.dados.dsJustificativa);

                var novoQtde = dataReturn.dados.qtSolicitacao.replace(".",",");
                $("#qtSolicitacao").val(novoQtde);

            }
        });
    }
    
    this.mudarstatus = function () {        
        if (!verificarerrosM()) {
            $.ajax({
                url: "/solicitacaocomprasmanutencao/gravar_solicitacao",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idSolicitacao": $("#idSolicitacao").val(),
                    "dsObservacao": $("#dsObservacao").val(),
                    "dsLocalEntrega": $("#dsLocalEntrega").val(),
                    "idSituacao": $("#idSituacao").val()
                },
                success: function (dataReturn) {
                    $("#idSolicitacao").val(dataReturn.idSolicitacao);
                    location.reload(); 
                }
            });
        }
    };
    this.lerusuario = function () {
        $.ajax({
            url: "/solicitacaocompras/lerusuario",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idUsuario": $("#idUsuario").val()
            },
            success: function (dataReturn) {
                $("#dsSolicitante").val((dataReturn.nomeusuario));
            }
        });
    };
    
    this.lerParceiro = function () {
        $.ajax({
            url: "/solicitacaocompras/lerParceiro",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idParceiro": $("#idParceiro").val()
            },
            success: function (dataReturn) {
                $("#dsParceiroSugerido").val((dataReturn.nomeParceiro));
            }
        });
    };
    
    this.gravaritem = function () {
        if (!verificarerrosI()) {
            if ($("#stTipoI").prop('checked')) {
               opcao = '0';
            } else {
               opcao = '1';
            }

            $.ajax({
                url: "/solicitacaocompras/gravar_item",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idSolicitacao": $("#idSolicitacao").val(),
                    "idSolicitacaoItem": $("#idSolicitacaoItem").val(),
                    "dsProduto": $("#dsProduto").val(),
                    "qtSolicitacao": $("#qtSolicitacao").val(),
                    "idUnidade": $("#idUnidade").val(),
                    "idInsumo": $("#idInsumo").val(),
                    "idParceiro": $("#idParceiro").val(),
                    "dsObservacao": $("#dsObservacaoItem").val(),
                    "idCentroCusto": $("#idCentroCusto").val(),
                    "idItemDR": $("#idItemDR").val(),
                    "dsJustificativa": $("#dsJustificativa").val(),
                    "idGrupoDR": $("#idGrupoDR").val(),
                    "dsParceiroSugerido": $("#dsParceiroSugerido").val(),
                    "opcao": opcao,
                    "dsLink": $("#dsLink").val()
                },
                success: function (dataReturn) {
                    $("#idSolicitacao").val(dataReturn.idSolicitacao);
                    $("#idSolicitacaoItem").val(dataReturn.idSolicitacaoItem);
                    $("#dsProduto").val('');
                    $("#qtSolicitacao").val('');
                    $("#idUnidade").val('');
                    $("#idInsumo").val('');
                    $("#idParceiro").val('');
                    $("#dsObservacaoItem").val('');
                    $("#dsParceiroSugerido").val('');
                    $("#dsLink").val('');
                    $("#idCentroCusto").val('');
                    $("#idItemDR").val('');
                    $("#dsJustificativa").val('');
                    $("#idGrupoDR").val('');                
                    location.reload(); // '/solicitacao/novo_solicitacao/idPedido/' . dataReturn.idPedido
                }
            });
        }
    };
    
    this.gravaritem_manutencao = function () {
        if (!verificarerrosIM()) {        
            if ($("#stTipoI").prop('checked')) {
               opcao = '0';
            } else {
               opcao = '1';
            }

            $.ajax({
                url: "/solicitacaocomprasmanutencao/gravar_item",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idSolicitacaoItem": $("#idSolicitacaoItem").val(),
                    "idSolicitacao": $("#idSolicitacao").val(),
                    "idParceiro": $("#idParceiro").val(),
                    "idInsumo": $("#idInsumo").val(),
                    "dsObservacaoItem": $("#dsObservacaoItem").val(),
                    "opcao": opcao,
                    "idSituacao": $("#idSituacao1").val()
                },
                success: function (dataReturn) {
                    $("#dsProduto").val('');
                    $("#qtSolicitacao").val('');
                    $("#idUnidade").val('');
                    $("#idInsumo").val('');
                    $("#idParceiro").val('');
                    $("#dsObservacaoItem").val('');
                    $("#dsParceiroSugerido").val('');
                    $("#dsProduto").val('');
                    $("#dsLink").val('');
                    $("#idCentroCusto").val('');
                    $("#idItemDR").val('');
                    $("#dsJustificativa").val('');
                    $("#idGrupoDR").val('');                
                    location.reload(); // '/solicitacao/novo_solicitacao/idPedido/' . dataReturn.idPedido
                }
            });
        }
    };
    this.gravarfinanceiro = function () {
        $.ajax({
            url: "/solicitacao/gravar_financeiro",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idPedido": $("#idPedido").val(),
                "dtPrevisaoEntrega": $("#dtPrevisaoEntrega").val(),
                "dtPrimeiroVencimento": $("#dtPrimeiroVencimento").val(),
                "qtParcelas": $("#qtParcelas").val(),
                "dsObservacao": $("#dsObservacao").val()
            },
            success: function (dataReturn) {
                $("#idFinanceiro").val(dataReturn.idFinanceiro);
                location.reload('/solicitacao/financeiro'); 
            }
        });
    };
    this.novosolicitacao = function () {
        $.ajax({
            url: "/solicitacao/novosolicitacao",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idPedido": $("#idPedido").val()
            },
            success: function (dataReturn) {
                $("#idPedido").val(dataReturn.idPedido);   
                location.reload();
            }
        });
    };
    this.lerunidade = function () {
        if ($("#stTipoI").prop('checked')) {
           opcao = '0';
        } else {
           opcao = '1';
        }
               
        $.ajax({
            url: "/solicitacaocompras/lerunidade",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idInsumo": $("#idInsumo").val(),
                "opcao": opcao
            },
            success: function (dataReturn) {
                $("#idUnidade").val(dataReturn.idUnidade);
                $("#dsProduto").val(dataReturn.dsProduto);
            }
        });
    };
    this.desabilitaid = function() {
        $.ajax({
            url: "/solicitacao/desabilitaid",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idPedido": $("#idPedido").val()
            },
            success: function () {
                location.reload('/dashboard');  
            }
        });
        return true;
    };
    this.delsolicitacaoitem = function(idSolicitacaoItem) {
        if (confirm('DESEJA EXCLUIR ESTE ITEM DA SOLICITACAO DE COMPRAS?')) {        
            $.ajax({
                url: "/solicitacaocompras/delsolicitacaocomprasitem",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idSolicitacaoItem": idSolicitacaoItem
                },
                success: function (dataReturn) {
                    location.reload();  
                }
            });
        }
    };
    this.delfinanceiroitem = function(item) {
        $.ajax({
            url: "/solicitacao/delfinanceiroitem",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idFinanceiroParcela": item
            },
            success: function (dataReturn) {
                location.reload();  
            }
        });
    };
};
verificarerros = function() {
    var err = false;
    if ($('#dsSolicitante').val() == '') {
        showMessage('Favor digitar o solicitante');
        err = true;
    }
    if ($('#idPrioridade').val() == '') {
        showMessage('Favor escolher a prioridade');        
        err = true;
    }
//    if ($('#dsLocalEntrega').val() == '') {
//        showMessage('Favor digitar o local de entrega');
//        err = true;
//    }
    return err;
}
verificarerrosM = function() {
    var err = false;
    if ($('#idSituacao').val() == '') {
        showMessage('Favor escolher a situacao');        
        err = true;
    }
//    if ($('#dsLocalEntrega').val() == '') {
//        showMessage('Favor digitar o local de entrega');
//        err = true;
//    }
    return err;
}
verificarerrosI = function() {
    err = verificarerros();
    if ($('#dsProduto').val() == '') {
        showMessage('Favor digitar ou escolher um produto');        
        err = true;
    }
    if ($('#qtSolicitacao').val() == '' || $('#qtSolicitacao').val() == '0,00') {
        showMessage('Favor digitar a quantidade solicitada');
        err = true;
    }
    if ($('#idUnidade').val() == '') {
        showMessage('Favor escolher a unidade');        
        err = true;
    }
    if ($('#dsParceiroSugerido').val() == '' && $('#idParceiro').val() == '') {
        showMessage('Favor sugerir um Parceiro');        
        err = true;
    }
    if ($('#idCentroCusto').val() == '') {
        showMessage('Favor escolher um Centro de Custo para o destino do Produto/Servico');        
        err = true;
    }
    if ($('#idGrupoDR').val() == '') {
        showMessage('Favor escolher um Grupo de Despesa para o Produto/Servico');        
        err = true;
    }
    if ($('#idItemDR').val() == '') {
        showMessage('Favor escolher um Item de Despesa para o Produto/Servico');        
        err = true;
    }
    if ($('#dsJustificativa').val() == '') {
        showMessage('Favor digiar a justificativa');        
        err = true;
    }
    
    return err;
}
verificarerrosIM = function() {
    err = verificarerrosI();
    if ($('#idSituacao').val() == '') {
        showMessage('Favor escolher a situacao');        
        err = true;
    }    
    if ($('#idSituacao1').val() == '') {
        showMessage('Favor escolher a situacao para o ítem da solicitacao');        
        err = true;
    }
    return err;
}
