$(document).ready(function () {

    $(".somenteNumeros").bind("keyup blur focus", function (e) {
        e.preventDefault();
        var expre = /[^\d]/g;
        $(this).val($(this).val().replace(expre, ''));
    });

    $(".somenteLetras").bind("keyup blur focus", function (e) {
        e.preventDefault();
        var expre = /[a-zA-Z]/;
        $(this).val($(this).val().replace(expre, ''));
    });
});

function showMessage(errorText, time, type, redirect) {

    if (time == null)
        time = 4000;
    if (type == null)
        type = 'error';

    redirect = redirect || null;

    $().toastmessage('showToast', {
        text: errorText,
        sticky: false,
        stayTime: time,
        position: 'middle-center',
        type: type,
        close: function () {
            if (redirect != null)
                window.location = redirect
        }
    });
}


function valida_cnpj(cnpj) {
    var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
    digitos_iguais = 1;
    if (cnpj.length != 14)
        return false;
    for (i = 0; i < cnpj.length - 1; i++)
        if (cnpj.charAt(i) != cnpj.charAt(i + 1)) {
            digitos_iguais = 0;
            break;
        }
    if (!digitos_iguais) {
        tamanho = cnpj.length - 2
        numeros = cnpj.substring(0, tamanho);
        digitos = cnpj.substring(tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
            return false;
        tamanho = tamanho + 1;
        numeros = cnpj.substring(0, tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
            return false;
        return true;
    }
    else
        return false;
}

function validateEmail(email) {
    er = /^[a-zA-Z0-9][a-zA-Z0-9\.\-_-a]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/;
    if (er.exec(email)) {
        return true;
    } else {
        return false;
    }
}

/*
 Mensagem Padrao do Bootstrap Fox
 type = alert-danger
 type = alert-success     
 type = alert-info
 */
function mensagem_dock(texto, type) {
    if (type === null)
        type = 'alert-success';
    var caixa = '';
    caixa += '<div class="alert alert-dismissable ' + type + '">';
    caixa += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
    caixa += texto;
    caixa += '</div>';
    $("#mensagem-dock").html(caixa);
}

function valida_data(valor) {

    var date = valor;
    var ardt = new Array;
    var ExpReg = new RegExp("(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/[12][0-9]{3}");

    ardt = date.split("/");

    var erro = false;

    if (date.search(ExpReg) === -1) {
        erro = true;
    } else if (((ardt[1] === '04') || (ardt[1] === '06') || (ardt[1] === '09') || (ardt[1] === '11')) && (ardt[0] > '30')) {
        erro = true;
    } else if (ardt[1] === '02') {
        if ((ardt[0] > '28') && ((ardt[2] % 4) !== 0)) {
            erro = true;
        }
        if ((ardt[0] > '29') && ((ardt[2] % 4) === 0)) {
            erro = true;
        }
    }

    if (erro) {
        return false;
    }

    return true;
}

function valida_cpf(cpf) {
    var numeros, digitos, soma, i, resultado, digitos_iguais;
    digitos_iguais = 1;

    if (cpf.length < 11) {
        return false;
    }

    for (i = 0; i < cpf.length - 1; i++) {
        if (cpf.charAt(i) != cpf.charAt(i + 1))
        {
            digitos_iguais = 0;
            break;
        }
    }

    if (!digitos_iguais) {
        numeros = cpf.substring(0, 9);
        digitos = cpf.substring(9);
        soma = 0;
        for (i = 10; i > 1; i--) {
            soma += numeros.charAt(10 - i) * i;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0)) {
            return false;
        }
        numeros = cpf.substring(0, 10);
        soma = 0;
        for (i = 11; i > 1; i--) {
            soma += numeros.charAt(11 - i) * i;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1)) {
            return false;
        }
        return true;
    } else {
        return false;
    }
}


    this.styleTitle = function () {
        $('[data-toggle="popover"]').popover({
            trigger: "hover",
            html: true,
            placement: 'right'
        });
    };


function somenteLetra(e) {
    var text = $(e).val();
    $(e).val(text.replace(/[^(a-z )\s]/gi, ""));
    //onkeypress="return somenteLetra(this);" onkeyup="return somenteLetra(this);"
}

function semCaracterEspecial(e) {
    var text = $(e).val();
    $(e).val(text.replace(/[^(a-z 0-9)\s]/gi, ""));
}

function somenteNumero(e) {
    var text = $(e).val();
    $(e).val(text.replace(/[^(0-9.,)\s]/gi, ""));
}

function somenteNumeroAll(e) {
    var text = $(e).val();
    $(e).val(text.replace(/[^(0-9)\s]/gi, ""));
}

function somenteNumeroSemVirgula(e) {
    var text = $(e).val();
    $(e).val(text.replace(/[^(0-9.)\s]/gi, ""));
}

function somenteNumeroSemPonto(e) {
    var text = $(e).val();
    $(e).val(text.replace(/[^(0-9,)\s]/gi, ""));
}

function somenteNumeroTraco(e) {
    var text = $(e).val();
    $(e).val(text.replace(/[^(0-9-.,)\s]/gi, ""));
}


$(document).ready(function () {

    /* Extensão do jquery utilizada no método de filtrar por Empresa/Perfil acima */
    $.extend($.expr[":"], {
        "contains-ci": function (elem, i, match, array) {
            return (elem.textContent || elem.innerText || $(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
        }
    });

    /* validate user session / open login modal */
    //util.userSession.auth = true;
    //util.userSession.isExpired();

    /* Exibe message automaticamente se houver cookie */
    if (util.getCookie('showMessageMessage')) {
        showMessage(util.getCookie('showMessageMessage'), null, util.getCookie('showMessageStatus'));
    }

    $(".num").bind("keyup blur focus", function (e) {
        e.preventDefault();
        var expre = /[^\d]/g;
        $(this).val($(this).val().replace(expre, ''));
    });

    $(".mostrarBotaoSalvar").click(function () {
        $("#salvar").slideDown();
    });

    $(".esconderBotaoSalvar").click(function () {
        $("#salvar").slideUp();
    });

    $('form').on('submit', function () {
        /**
         * Mecanismo utilizado em várias telas para o funcionamento do workflow
         * que automaticamente envia por POST o nome da última aba ativa
         */
        if ($(this).attr('method') && $(this).attr('method').toLowerCase() == 'post') {
            /* Sempre envia pelos forms a aba ativa atualmente */
            aba_ativa = util.getActiveTab();
            if (aba_ativa) {
                /* Se não existir o elemento #util_aba_ativa, criar dinamicamente */
                if ($('#util_aba_ativa').length === 0) {
                    $(this).append('<input type="hidden" id="util_aba_ativa" name="util_aba_ativa"/>');
                }
                $('#util_aba_ativa').val(aba_ativa);
            }
        }
    });

    //Fechar o alert
    $('#showAlert').ready(function () {
        setTimeout(function () {
            $('#showAlert').hide('slow');
        }, 4000);
    });

//    $('#busca_navegacao').autocomplete({
//        source: "/navegacao/getNavegacao/termo/",
//        minLength: 1,
//        select: function (event, ui) {
//            window.location.href = ui.item.url;
//        }
//    });
//
//    $('#search_navigation').autocomplete({
//        source: "/navigation/search",
//        minLength: 1,
//        select: function (event, ui) {
//            window.location.href = ui.item.url;
//        }
//    });

    /**
     * Muda automaticamente para outra aba se for passado o parâmetro hash pela URL
     * /url#tab_<nome_da_aba>
     * Exemplo: /.a.#tab=dados_principais
     */
    tab_regex = /#tab=(.+)/g;
    tab_regex_matches = tab_regex.exec(window.location.hash);
    if (tab_regex_matches && undefined !== tab_regex_matches[1]) {
        tab_to_activate = tab_regex_matches[1];
        /* Dispara a ação click no link da aba para abri-la */
        $('#' + tab_to_activate).click();
    } else {
        tab_to_activate = null;
    }

    //onchange empresa do sistema
    $('#nav_id_empresa').change(function () {
        var empresa = $('#nav_id_empresa').val();
        $('#nav_id_empresa').attr('disabled', 'disabled');
        $('#nav_id_empresa').find(":selected").text(util.traduzir_js("ALTERANDO..."));
        $.ajax({
            url: "/empresa/mudar_empresa/empresa/" + empresa,
            async: true,
            complete: function (event, XMLHttpRequest) {
                document.location.reload();
            }
        });
    });

    //onchange papel do sistema
    $('#nav_papel_id_usuario').change(function () {
        var usuario = $('#nav_papel_id_usuario').val();
        $('#nav_papel_id_usuario').attr('disabled', 'disabled');
        $('#nav_papel_id_usuario').find(":selected").text(util.traduzir_js("ALTERANDO..."));
        $.ajax({
            url: "/user/role/id_usuario/" + usuario,
            async: true,
            complete: function (event, XMLHttpRequest) {
                document.location.reload();
            }
        });
    });

    //ripple
    var mask, d, x, y;
    $(".btn").click(function (e) {
        if ($(this).find(".mask").length === 0) {
            $(this).prepend("<span class='mask'></span>");
        }
        mask = $(this).find(".mask");
        mask.removeClass("animate");
        if (!mask.height() && !mask.width()) {
            d = Math.max($(this).outerWidth(), $(this).outerHeight());
            mask.css({height: d, width: d});
        }
        x = e.pageX - $(this).offset().left - mask.width() / 2;
        y = e.pageY - $(this).offset().top - mask.height() / 2;
        mask.css({top: y + 'px', left: x + 'px'}).addClass("animate");
    });

    /**
     * Source: http://stackoverflow.com/a/14223553/1501575
     * For each element that is classed as 'pull-down', set its margin-top to the difference between its own height and the height of its parent
     */
    $('.pull-down').each(function () {
        var $this = $(this);
        $this.css('margin-top', $this.parent().height() - $this.height())
    });
});


function somenteFloat(obj) {
    var value = $(obj).val();
    value = value.replace(/\D/g, "");
    value = value.replace(/^(\d{1,})+(\d{2})$/, "$1,$2");
    $(obj).val(value);
}

function showMessage(errorText, time, type, redirect) {

    /* Armazena a mensagem em cookie e limpa automaticamente após 1 segundo.
     * que deve ser suficiente para a mensagem ser lida.
     * Isso é útil para quando acontecer um redirect na página antes
     * de a mensagem ser lida */
    util.setCookie('showMessageMessage', errorText);
    util.setCookie('showMessageStatus', type);
    setTimeout(function () {
        util.setCookie('showMessageMessage', '');
        util.setCookie('showMessageStatus', '');
    }, 1000);

    if (time == null) {
        time = 4000;
    }

    if (type == null) {
        type = 'error';
    }
    redirect = redirect || null;
    $().toastmessage('showToast', {
        text: errorText,
        sticky: false,
        stayTime: time,
        position: 'top-right',
        type: type,
        close: function () {
            if (redirect == "reload") {
                location.reload(true);
            } else if (redirect != null) {
                window.location = redirect;
            }
        }
    });
}

function valida_data(valor) {

    var date = valor;
    var ardt = new Array;
    var ExpReg = new RegExp("(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/[12][0-9]{3}");

    ardt = date.split("/");

    /*
     * ardt[0] = dia
     * ardt[1] = mês
     * ardt[2] = ano
     */

    var erro = false;

    if (date.search(ExpReg) === -1) {
        erro = true;
    } else if (((ardt[1] === '04') || (ardt[1] === '06') || (ardt[1] === '09') || (ardt[1] === '11')) && (ardt[0] > '30')) {
        erro = true;
    } else if (ardt[1] === '02') {
        if ((ardt[0] > '28') && ((ardt[2] % 4) !== 0)) {
            erro = true;
        }
        if ((ardt[0] > '29') && ((ardt[2] % 4) === 0)) {
            erro = true;
        }
    }

    if (erro) {
        return false;
    }

    return true;
}

function valida_cpf(cpf) {
    var numeros, digitos, soma, i, resultado, digitos_iguais;
    digitos_iguais = 1;

    if (cpf.length < 11) {
        return false;
    }

    for (i = 0; i < cpf.length - 1; i++) {
        if (cpf.charAt(i) != cpf.charAt(i + 1)) {
            digitos_iguais = 0;
            break;
        }
    }

    if (!digitos_iguais) {
        numeros = cpf.substring(0, 9);
        digitos = cpf.substring(9);
        soma = 0;
        for (i = 10; i > 1; i--) {
            soma += numeros.charAt(10 - i) * i;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0)) {
            return false;
        }
        numeros = cpf.substring(0, 10);
        soma = 0;
        for (i = 11; i > 1; i--) {
            soma += numeros.charAt(11 - i) * i;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1)) {
            return false;
        }
        return true;
    } else {
        return false;
    }
}

function validateEmail(email) {
    er = /^[a-zA-Z0-9][a-zA-Z0-9\.\-_-a]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/;
    if (er.exec(email)) {
        return true;
    } else {
        return false;
    }
}

function valida_cnpj(cnpj) {

    var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
    digitos_iguais = 1;
    if (cnpj.length != 14)
        return false;
    for (i = 0; i < cnpj.length - 1; i++)
        if (cnpj.charAt(i) != cnpj.charAt(i + 1)) {
            digitos_iguais = 0;
            break;
        }
    if (!digitos_iguais) {
        tamanho = cnpj.length - 2;
        numeros = cnpj.substring(0, tamanho);
        digitos = cnpj.substring(tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2) {
                pos = 9;
            }
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0)) {
            return false;
        }
        tamanho = tamanho + 1;
        numeros = cnpj.substring(0, tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2) {
                pos = 9;
            }
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1)) {
            return false;
        }

        return true;
    } else {
        return false;
    }
}

function somenteLetra(e) {
    var text = $(e).val();
    $(e).val(text.replace(/[^(a-z )\s]/gi, ""));
    //onkeypress="return somenteLetra(this);" onkeyup="return somenteLetra(this);"
}

function semCaracterEspecial(e) {
    var text = $(e).val();
    $(e).val(text.replace(/[^(a-z 0-9)\s]/gi, ""));
}

function somenteNumero(e) {
    var text = $(e).val();
    $(e).val(text.replace(/[^(0-9.,)\s]/gi, ""));
}

function somenteNumeroAll(e) {
    var text = $(e).val();
    $(e).val(text.replace(/[^(0-9)\s]/gi, ""));
}

function somenteNumeroSemVirgula(e) {
    var text = $(e).val();
    $(e).val(text.replace(/[^(0-9\.)\s]/gi, ""));
}

function somenteNumeroSemPonto(e) {
    var text = $(e).val();
    $(e).val(text.replace(/[^(0-9,)\s]/gi, ""));
}

function somenteNumeroTraco(e) {
    var text = $(e).val();
    $(e).val(text.replace(/[^(0-9-.,)\s]/gi, ""));
}

function replaceAll(string, token, newtoken) {
    while (string.indexOf(token) !== -1) {
        string = string.replace(token, newtoken);
    }
    return string;
}

function contaDiasIntervaloDatas(data1, data2) {

    //Valida o formato da data
    if (valida_data(data1) && valida_data(data2)) {

        //Numero de minlisegundos de um dia
        var um_dia = 1000 * 60 * 60 * 24;

        //Converte as dias datas para milisegundos
        var data1_ms = getUnixtimeFromPtDate(data1);
        var data2_ms = getUnixtimeFromPtDate(data2);

        //Calcula a diferença em milisegundos
        var diferenca_ms = Math.abs(data1_ms - data2_ms)

        //Converte de volta em dias e retorna
        return Math.round(diferenca_ms / um_dia);

    } else {
        return false;
    }
}

function converteDataPtEn(data) {
    //Explode a data num array
    dataArray = data.split("/");
    //Concatena de forma inversa numa string
    return dataArray[2] + "/" + dataArray[1] + "/" + dataArray[0];
}

function getUnixtimeFromPtDate(date) {
    //Cria um objeto de data com a data em português
    var unixtime = new Date(converteDataPtEn(date));
    //Transforma em um inteiro com o unixtime da data
    return unixtime.getTime();
}

function convertFlotToString(valor, prefix) {
    var inteiro = null, decimal = null, c = null, j = null;
    var aux = new Array();
    valor = "" + valor;
    c = valor.indexOf(".", 0);
    //encontrou o ponto na string
    if (c > 0) {
        //separa as partes em inteiro e decimal
        inteiro = valor.substring(0, c);
        decimal = valor.substring(c + 1, valor.length);
    } else {
        inteiro = valor;
    }

    //pega a parte inteiro de 3 em 3 partes
    for (j = inteiro.length, c = 0; j > 0; j -= 3, c++) {
        aux[c] = inteiro.substring(j - 3, j);
    }

    //percorre a string acrescentando os pontos
    inteiro = "";
    for (c = aux.length - 1; c >= 0; c--) {
        inteiro += aux[c] + '.';
    }

    //retirando o ultimo ponto e finalizando a parte inteiro
    inteiro = inteiro.substring(0, inteiro.length - 1);
    if (isNaN(decimal) || decimal === null) {
        decimal = "00";
    } else {
        decimal = "" + decimal;
        if (decimal.length === 1) {
            decimal = decimal + "0";
        }
    }
    if (prefix === undefined) {
        valor = "R$ " + inteiro + "," + decimal;
    } else {
        valor = inteiro + "," + decimal;
    }
    return valor;
}

function convertStringToFloat(valor) {
    valor = valor.replace('R$', '');
    valor = replaceAll(valor, '.', '');
    valor = valor.replace(',', '.');
    return parseFloat(valor);
}

// Transforma o valor informado em fload 
function getMoneyFloat(e) {
    var money = $('#' + e).val().replace(/(\.)/g, '').replace(',', '.');
    return parseFloat(money);
}

function convertMoney(valor) {
    var inteiro = null, decimal = null, c = null, j = null;
    var aux = new Array();
    valor = "" + valor;
    c = valor.indexOf(".", 0);
    //encontrou o ponto na string
    if (c > 0) {
        //separa as partes em inteiro e decimal
        inteiro = valor.substring(0, c);
        decimal = valor.substring(c + 1, valor.length);
    } else {
        inteiro = valor;
    }

    //pega a parte inteiro de 3 em 3 partes
    for (j = inteiro.length, c = 0; j > 0; j -= 3, c++) {
        aux[c] = inteiro.substring(j - 3, j);
    }

    //percorre a string acrescentando os pontos
    inteiro = "";
    for (c = aux.length - 1; c >= 0; c--) {
        inteiro += aux[c] + '.';
    }
    //retirando o ultimo ponto e finalizando a parte inteiro

    inteiro = inteiro.substring(0, inteiro.length - 1);

    decimal = parseInt(decimal);
    if (isNaN(decimal)) {
        decimal = "00";
    } else {
        decimal = "" + decimal;
        if (decimal.length === 1) {
            decimal = "0" + decimal;
        }
    }
    valor = inteiro + "," + decimal;
    return valor;
}

function remove_acento(palavra) {
    var com_acento = "áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ";
    var sem_acento = "aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC";
    var nova = "";
    for (i = 0; i < palavra.length; i++) {
        if (com_acento.search(palavra.substr(i, 1)) >= 0) {
            nova += sem_acento.substr(com_acento.search(palavra.substr(i, 1)), 1);
        } else {
            nova += palavra.substr(i, 1);
        }
    }
    return nova;
}

function GestorFileUpload() {
    this.reset = function () {
        $(this.preview_id).attr('src', '/files/default/images/no-image.png');
        $(this.file_id).val('');
        $(this.excluir_id).val(0);
        $("#icone").val(''); // Compatibilidade com tela antiga
    };
    this.delete = function () {
        this.reset();
        $(this.excluir_id).val(1);
    };
    this.show = function (file_path) {
        this.reset();
        if (file_path) {
            $(this.preview_id).attr('src', '/storage/' + file_path);
        } else {
            $(this.preview_id).attr('src', '/files/default/images/no-image.png');
        }
        $(this.excluir_id).val(0);
    };
    this.setup = function (file_id) {
        file_id = (typeof file_id !== 'undefined') ? file_id : 'upload_foto';
        this.file_id = '#' + file_id;
        this.preview_id = this.file_id + '_preview';
        this.excluir_id = this.file_id + '_excluir';
        this.excluir_botao_id = this.file_id + '_excluir_btn';
        $(this.file_id).change((function (obj) {
            return function () {
                element = this;
                $(obj.preview_id).attr('src', window.URL.createObjectURL(element.files[0]));
            };
        })(this));
        $(this.excluir_botao_id).click((function (obj) {
            return function () {
                obj.delete();
            };
        })(this));
    };
}

var util = new function () {

    /* Extension of jQuery ajax, but automatically uses sweetAlert and showMessage */
    this.sweetAjax = function (params) {
        var originalCallbacks = {
            success: params.success,
            error: params.error,
            beforeSend: params.beforeSend,
            complete: params.complete
        };
        if (params.form !== undefined) {
            if (!util.validateFieldsByForm({"form": params.form})) {
                return false;
            } else {
                if (params.data === undefined) {
                    params.data = util.getSerializedFields({"form": params.form});
                }
            }
        }
        if (params.showLoading === undefined) {
            params.showLoading = true;
        }
        if (params.dataType === undefined) {
            params.dataType = 'json';
        }
        if (params.method === undefined) {
            params.method = 'post';
        }
        params.success = function (data) {
            util.showReturnMessage(data.retorno !== undefined ? data.retorno : data);
            if (typeof originalCallbacks.success === 'function') {
                originalCallbacks.success(data);
            }
        };
        params.error = function () {
            util.showReturnMessage();
            if (typeof originalCallbacks.error === 'function') {
                originalCallbacks.error();
            }
        };
        params.beforeSend = function () {
            if (params.showLoading) {
                util.showLoading();
            }
            if (typeof originalCallbacks.beforeSend === 'function') {
                originalCallbacks.beforeSend();
            }
        };
        params.complete = function () {
            util.hideLoading();
            if (typeof originalCallbacks.complete === 'function') {
                originalCallbacks.complete();
            }
        };
        $.ajax(params);
    };

    this.showReturnMessage = function (dataReturn) {
        if (!dataReturn) {
            dataReturn = Object;
        }
        if (dataReturn.type) {
            dataReturn.tipo = dataReturn.type;
        }
        if (!dataReturn.tipo && dataReturn.status) {
            if (dataReturn.status) {
                dataReturn.tipo = 'success';
            } else {
                dataReturn.tipo = 'error';
            }
        }
        if (!dataReturn.msg && dataReturn.tipo !== 'success') {
            dataReturn.msg = 'ERRO AO ENVIAR REQUISICAO.';
        }
        if (!dataReturn.tipo) {
            dataReturn.tipo = 'error';
        }
        if (dataReturn.msg) {
            showMessage(dataReturn.msg, null, dataReturn.tipo);
        }
    };

    this.confirmDelete = function (onConfirm, params) {

        if (!params) {
            params = {}
        }
        if (!params.title) {
            params.title = "TEM CERTEZA?";
        }
        if (!params.text) {
            params.text = "DESEJA REALMENTE EXCLUIR ESTE REGISTRO?";
        }
        if (!params.type) {
            params.type = "warning";
        }
        if (undefined === params.showCancelButton) {
            params.showCancelButton = true;
        }
        if (!params.confirmButtonColor) {
            params.confirmButtonColor = "#DD6B55";
        }
        if (!params.cancelButtonText) {
            params.cancelButtonText = "ABORTAR";
        }
        if (!params.confirmButtonText) {
            params.confirmButtonText = "CONFIRMAR";
        }
        if (undefined === params.closeOnConfirm) {
            params.closeOnConfirm = false;
        }

        /* Por enquanto utilizamos o confirm do javascript, em vez do sweetAlert */
        //sweetAlert(params, onConfirm);
        if (confirm(params.text)) {
            if (typeof onConfirm === 'function') {
                onConfirm();
            }
        }
    };

    this.validateFieldsByForm = function (form) {
        var auth = true;
        for (var namespace in form) {
            var input = $(form[namespace]);
            if (!standard.form.validate.run(input)) {
                auth = false;
                break;
            }
        }
        return auth;
    };

    this.getSerializedFields = function (inputs) {
        var serialize = "";
        for (var namespace in inputs) {
            var input = $(inputs[namespace]);
            if (serialize !== "") {
                serialize += "&";
            }
            serialize += input.serialize();
        }
        return serialize;
    };

    this.showLoading = function (data) {
        sweetAlert({
            title: data && data.title ? data.title : '',
            text: data && data.text ? data.text : util.traduzir_js("AGUARDE,_CARREGANDO..."),
            imageUrl: '/files/default/images/carregando.gif',
            showConfirmButton: false,
            allowEscapeKey: false
        });
    };

    this.hideLoading = function () {
        sweetAlert.close();
    };

    /**
     * Carrega modal geral de documentos
     * @param integer id_menu
     * @param integer id_registro
     */
    this.modalDocumento = function (id_menu, id_registro, funcao) {
        iniciaFormularioUpload();
        carregaDados(id_menu, id_registro, funcao);
        $('#modal-pedidos').modal("hide");
        $('#modal-documento-geral').modal("show");
    };

    this.modalChart = function (id_menu, id_registro, funcao) {
        $('#modal-chart').modal("show");
    };

    this.getActiveTab = function () {
        $aba_ativa = $('ul li.active a');
        if ($aba_ativa.length > 0) {
            aba_ativa_href = $aba_ativa.attr('href');
            if (aba_ativa_href) {
                return aba_ativa_href.replace('#', '');
            }
        }
        return false;
    };
    /**
     * Altera a aba atual
     * @param string tab_id Nome da aba (normalmente iniciado por #)
     */
    this.setActiveTab = function (tab_id) {
        /* O click() serve apenas para chamar métodos que podem estar atrelados à ação do botão */
        $('.nav-tabs a[href="' + tab_id + '"').tab('show').click();
        /* Também executa click sobre o <li> pai */
        $('.nav-tabs a[href="' + tab_id + '"').closest('li').click();
    };

    this.getCookie = function (cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    };

    this.setCookie = function (cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    };

    this.removeCookie = function (cname) {
        document.cookie = cname + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    }

    /**
     * Verifica se a sessao do usuario expirou e abre o modal para logar novamente
     */
    this.userSession = new function () {
        this.isExpired = function () {
            setInterval(function () {
                $.ajax({
                    url: '/login/checkSession',
                    type: 'POST',
                    async: true,
                    dataType: 'json',
                    success: function (data) {
                        if (undefined != data.status && !data.status) {
                            if (util.userSession.auth) {
                                $('#modal-login-footer').modal('show');
                                util.userSession.auth = false;
                            }

                        }
                        if (undefined != data.status && data.status) {
                            if (!util.userSession.auth) {
                                $('#modal-login-footer').modal('hide');
                                util.userSession.auth = true;
                            }
                        }
                    }
                });
            }, 1000);
        };
        this.login = function () {
            var auth = true;
            var serialize = "";
            var inputs = {
                "form": "#form-modalLoginFooter"
            };
            empresaAfterLoginModal = $('#nav_id_empresa').val();
            for (var namespace in inputs) {
                var input = $(inputs[namespace]);
                if (standard.form.validate.run(input)) {
                    if (serialize !== "") {
                        serialize += "&";
                    }
                    serialize += input.serialize();
                } else {
                    auth = false;
                    break;
                }
            }
            if (auth) {
                $.ajax({
                    url: '/login/logar/type/isAjax',
                    type: 'POST',
                    data: serialize,
                    async: true,
                    dataType: 'json',
                    success: function (dataReturn) {
                        if (!dataReturn.status) {
                            showMessage(dataReturn.msg, 2000, 'error');
                        }
                        if (dataReturn.status) {
                            showMessage(dataReturn.msg, 2000, 'success');
                            $('#modal-login-footer').modal('hide');
                            util.userSession.auth = true;
                            changeEmpresa(empresaAfterLoginModal);
                        }
                    },
                    error: function () {
                        showMessage('ERRO AO TENTAR FAZER LOGIN', 2000, 'error');
                    }
                });
            }
        };
    };

    this.processFilterSessionStorage = function (params) {
        if (params.identifier === undefined || params.name === undefined || params.url === undefined) {
            console.log('util.processFilterSessionStorage :: Required parameters were not provided');
            return false;
        }
        if (typeof sessionStorage === 'undefined') {
            console.log('util.processFilterSessionStorage :: There is no support for sessionStorage');
            return false;
        }
        if (params.clear) {
            sessionStorage.removeItem(params.name + '-identifier');
            sessionStorage.removeItem(params.name + '-pagina');
        } else if (params.identifier) {
            sessionStorage.setItem(params.name + '-identifier', params.identifier);
            sessionStorage.setItem(params.name + '-pagina', params.pageNumber);
        } else {
            var identifier = sessionStorage.getItem(params.name + '-identifier');
            var pageNumber = sessionStorage.getItem(params.name + '-pagina');
            var suffix = '';
            if (identifier) {
                suffix += 'filter_identifier/' + identifier;
            }
            if (pageNumber) {
                suffix += '/pagina/' + pageNumber;
            }
            if (suffix) {
                util.showLoading();
                window.location.href = params.url + suffix;
            }
        }
    };

    /**
     * Carreca todos os campos do form caso ids estejam em conformidade com o banco de dados
     * @param {object} form
     * @param {object} data
     */
    this.charge = function (form, data) {

        if (data !== null) {
            $(form).find(':input').each(function () {
                var input = $(this).prop('id').replace($(form).prop('id').replace('form', ''), '');
                if (data[input] !== undefined) {
                    if ($('[id=' + $(this).prop('id') + ']').prop('autocomplete')) {
                        $('[id=' + $(this).prop('id') + ']').val(data[input]);
                    } else if ($('[id=' + $(this).prop('id') + ']').hasClass('standard-form-date')) {
                        $('[id=' + $(this).prop('id') + ']').val($.datepicker.formatDate('dd/mm/yy', new Date(data[input])));
                    } else {
                        $('[id=' + $(this).prop('id') + ']').val(data[input]).blur();
                    }
                    if ($('[name=' + input + ']').is(':checkbox')) {
                        if (data[input] === 1) {
                            $('[name=' + input + ']').prop('checked', true);
                        } else {
                            $('[name=' + input + ']').prop('checked', false);
                        }
                    }
                }
            });
        }

    };

    /**
     * Requisicoes ajax
     * @param JSON {url: string, data: JSON, file: boolean} param
     * @returns JSON
     */
    this.request = function (param) {
        var request = null;
        $.ajax({
            url: param.url,
            type: 'post',
            data: param.data,
            dataType: 'json',
            processData: (param.file !== undefined) ? false : true,
            contentType: (param.file !== undefined) ? false : 'application/x-www-form-urlencoded',
            async: false,
            success: function (data) {
                request = data;
            }
        });
        return request;
    };

    /**
     * Exibe gif de load caso nao informado JSON
     * @param {object} div
     * @param {object} html
     */
    this.loadTable = function (div, data) {
        div.html((data !== undefined) ? data : '<center><img src="/files/default/images/carregando.gif" /></center>');
    };

    this.getSystemTable = function (tableName) {
        var request = null;
        $.ajax({
            url: "/tag/getSystemTable/tablename/" + tableName,
            async: false,
            success: function (data) {
                request = replaceAll(data, '"', '');
            }
        });
        return request;
    }

    this.getSystemTableFilter = function (tableName, fieldName) {
        var request = null;
        $.ajax({
            url: "/tag/getSystemTableFilter/tablename/" + tableName + "/fieldname/" + fieldName,
            async: false,
            success: function (data) {
                request = replaceAll(data, '"', '');
            }
        });
        return request;
    }
    this.setTagType = function (tableName) {
        $.ajax({
            url: "/tag/setTagType/tablename/" + tableName,
            async: false,
        });
    }

    this.dropdown = function (className) {
        var options = [];
        $('.dropdown-menu ' + className).on('click', function (event) {
            var target = $(event.currentTarget),
                    val = target.attr('data-value'),
                    inp = target.find('input'),
                    idx;

            if ((idx = options.indexOf(val)) > -1) {
                options.splice(idx, 1);
                setTimeout(function () {
                    inp.prop('checked', false);
                }, 0);
            } else {
                options.push(val);
                setTimeout(function () {
                    inp.prop('checked', true);
                }, 0);
            }

            $(event.target).blur();

//            console.log(options);
//            var string = options.toString();
//            var count = string.split(',').length;
//            if (count > 2) {
//                string == '' ? $('.dropdown-text').text('SELECIONE') : $('.dropdown-text').text('SELECIONADOS ' + '(' + count + ')');
//            } else {
//                string == '' ? $('.dropdown-text').text('SELECIONE') : $('.dropdown-text').text(string);
//            }

            return false;
        });
    };

    this.styleTitle = function () {
        $('[data-toggle="popover"]').popover({
            trigger: "hover",
            html: true,
            placement: 'right'
        });
    };

    this.styleTitleLeft = function () {
        $('[data-toggle="popover"]').popover({
            trigger: "hover",
            html: true,
            placement: 'left'
        });
    };

    this.tooltip = function () {
        //content with title html included
        $('[data-toggle="tooltip"]').tooltip({
            track: true,
            content: function () {
                var element = $(this);
                element.css("width", "500px;")
                if (element.is("[title]")) {
                    return element.attr("title");
                }
            }
        });
    };

    this.traduzir_js = function (string_traduzir) {
        return modules.translation.translation.get(string_traduzir);
    };
    this.runWhen = function (conditionChecker, callBack) {
        var loop = function () {
            if (conditionChecker()) {
                callBack();
            } else {
                setTimeout(loop, 100);
            }
        }
        loop();
    };
    this.modal = new function () {
        this.customization = new function () {
            this.open = function () {
                this.showLoading();
                $.ajax({
                    dataType: 'json',
                    url: '/user/customization/tab/open',
                    async: true,
                    success: function (response) {
                        if (response.html) {
                            util.modal.customization.onLoadTab(response.html);
                            util.modal.customization.hideLoading();
                            $('#modal-customization').modal('show');
                        }
                    },
                    error: function () {
                        util.modal.customization.hideLoading();
                        showMessage('erro', 4000, 'error');
                    }
                });
            };
            this.showLoading = function (data) {
                swal({
                    title: data ? data : '',
//                    text: data && data.text ? data.text : "",
                    imageUrl: '/files/default/images/carregando.gif',
                    showConfirmButton: false,
                    allowEscapeKey: false
                });
            };
            this.hideLoading = function () {
                swal.close();
            };
            this.onLoadTab = function (html) {
                $('#openModalContent').html(html);
            };
        };
    };
};

/* global para recuperar a sessao depois de logar pelo modal */
var empresaAfterLoginModal = '';

function changeEmpresa(empresa) {
    $.ajax({
        url: "/empresa/mudar_empresa/empresa/" + empresa,
        async: true
    });
}