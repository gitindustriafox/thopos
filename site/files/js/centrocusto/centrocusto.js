//Valida Formulario Antes de Enviar
   
$(document).ready(function(){
  $('#dsCentroCusto').focus(); 

  $("#frm-busca-centrocusto [type=submit]").click(function(){
      if ($(this).hasClass('buscar')){
          $("#frm-busca-centrocusto").attr('action', '/centrocusto/busca_centrocusto');
          $("#frm-busca-centrocusto").submit();
      } else {
      if ($(this).hasClass('csv')){
          $("#frm-busca-centrocusto").attr('action', '/centrocusto/busca_criarcsv');
          $("#frm-busca-centrocusto").submit();
      }     
      }     
  });

});   

function validaFormulario() {
    var stats = true;
    var errorText = '';  
    

    if ($.trim($('#dsCentroCusto').val()) === '') {
        errorText += 'A descrição é obrigatória!';
        stats = false;
    }

    if (errorText) {
        showMessage(errorText);
    }

    return stats;
}

$('#btnInsereMenu').click(function(){
   $('#painel_menu').fadeOut(3000);
   $('#painel_menu').fadeIn(3000);     
});
