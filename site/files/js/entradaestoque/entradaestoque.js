
$(document).ready(function() {
    entradaestoque.autocomplete.Parceiro();
    entradaestoque.autocomplete.produto();
    entradaestoque.autocomplete.ncm();
    
    $.datepicker.setDefaults({
        defaultDate: null,
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd/mm/yy"
    });
    $("#dtMovimento").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtMovimento").val("");
        }
    });
//    $("#vlMovimento").priceFormat({
//        prefix: "",
//        centsSeparator: ',',
//        thousandsSeparator: '.',
//        limit: 10,
//        centsLimit: 2,
//        allowNegative: true
//    });
    $('.valor').priceFormat({
        prefix: "",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    });
                 
});

$(function () {
    entradaestoque.autocomplete.Parceiro();
    entradaestoque.autocomplete.produto();
});

var entradaestoque = new function () {
    this.autocomplete = new function () {
        this.Parceiro = function () {
            $('.complete_Parceiro').autocomplete({
                source: "/entradaestoque/getParceiro",
                minLength: 3,
                select: function (event, ui) {
                    var retorno = $(this).attr('data-label');
                    $('#idParceiro').val(ui.item.id);
                    $("#Parceiro").val(ui.item.value);
                }
            });
            $(".complete_Parceiro").bind("blur keyup keydow keypress", function () {
                if ($("#Parceiro" + $(this).attr("data-label")).val() === "") {
                    $('#idParceiro' + $(this).attr("data-label")).val("");
                }
            }); 
        };
        this.produto = function () {
            $('.complete_produto').autocomplete({
                source: "/entradaestoque/getProduto/insumo/",
                minLength: 3,
                select: function (event, ui) {
                    var retorno = $(this).attr('data-label');
                    $('#idInsumo').val(ui.item.id);
                    $("#insumo").val(ui.item.value);
                }
            });
            $(".complete_produto").bind("blur keyup keydow keypress", function () {
                if ($("#insumo" + $(this).attr("data-label")).val() === "") {
                    $('#idInsumo' + $(this).attr("data-label")).val("");
                }
            }); 
        };
        this.ncm = function () {
            $('.complete_ncm').autocomplete({
                source: "/insumo/getNCM",
                minLength: 3,
                select: function (event, ui) {
                    var retorno = $(this).attr('data-label');
                    if (ui.item.id) {
                        $('#idNCM').val(ui.item.id);
                        $("#dsNCM").val(ui.item.value);
                    } else {
                        $('#idNCM').val('');
                        $("#dsNCM").val('');
                    }
                }
            });
            $(".complete_ncm").bind("blur keyup keydow keypress", function () {
                if ($("#dsNCM" + $(this).attr("data-label")).val() === "") {
                    $('#idNCM' + $(this).attr("data-label")).val("");
                }
            }); 
        };        
    };
    
    this.lerncm = function () {
        $.ajax({
            url: "/entradaestoque/lerncm",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idInsumo": $("#idInsumo").val()
            },
            success: function (dataReturn) {
                $('#idNCM').val(dataReturn.idNCM);
                $('#dsNCM').val(dataReturn.cdNCM + ' - ' + dataReturn.dsNCM);
                $("#idLocalEstoque").html(dataReturn.html);                
            }
        });
    };
    
    this.gravarcabecalho = function () {
        if (verificaerrosC()) {        
            $.ajax({
                url: "/entradaestoque/gravar_movimento",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idTipoMovimento": $("#idTipoMovimento").val(),
                    "idParceiro": $("#idParceiro").val(),
                    "dtMovimento": $("#dtMovimento").val(),
                    "nrNota": $("#nrNota").val(),
                    "nrPedido": $("#nrPedido").val(),
                    "qtTotalNota": $("#qtTotalNota").val(),
                    "idColaborador": $("#idColaborador").val(),
                    "nrPlaca": $("#nrPlaca").val(),
                    "nrLacre": $("#nrLacre").val(),
                    "nrNota2": $("#nrNota2").val(),
                    "qtTotalNota2": $("#qtTotalNota2").val(),
                    "idMovimento": $("#idMovimento").val(),
                    "dsObservacao": $("#dsObservacao").val()
                },
                success: function (dataReturn) {
                    $("#idMovimento").val(dataReturn.idMovimento);
                    location.reload(); 
                }
            });
        }
    };
    this.gravaritem = function () {
        if (verificaerrosI()) {        
            $.ajax({
                url: "/entradaestoque/gravar_item",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idMovimento": $("#idMovimento").val(),
                    "idMovimentoItem": $("#idMovimentoItem").val(),
                    "dtMovimento": $("#dtMovimento").val(),
                    "idTipoMovimento": $("#idTipoMovimento").val(),
                    "idParceiro": $("#idParceiro").val(),
                    "idInsumo": $("#idInsumo").val(),
                    "idCentroCusto": $("#idCentroCusto").val(),
                    "idLocalEstoque": $("#idLocalEstoque").val(),
                    "idOS": $("#idOS").val(),
                    "idMotivo": $("#idMotivo").val(),
                    "idMaquina": $("#idMaquina").val(),
                    "qtMovimento": $("#qtMovimento").val(),
                    "cdNCM": $("#idNCM").val(),
                    "vlMovimento": $("#vlMovimento").val(),
                    "nrNota": $("#nrNota").val(),
                    "nrPedido": $("#nrPedido").val(),
                    "qtTotalNota": $("#qtTotalNota").val(),
                    "idColaborador": $("#idColaborador").val(),
                    "nrPlaca": $("#nrPlaca").val(),
                    "nrLacre": $("#nrLacre").val(),
                    "nrNota2": $("#nrNota2").val(),
                    "qtTotalNota2": $("#qtTotalNota2").val(),
                    "dsObservacao": $("#dsObservacaoItem").val()
                },
                success: function (dataReturn) {
                    $("#idMovimento").val(dataReturn.idMovimento);
                    $("#idMovimentoItem").val(dataReturn.idMovimentoItem);
                    $("#idInsumo").val('');
                    $("#idLocalEstoque").val('');
                    $("#qtMovimento").val(0);
                    $("#vlMovimento").val(0);
                    $("#idNCM").val(0);
                    $("#dsNCM").val('');
                    $("#dsObservacaoItem").val('');
                    location.reload(); // '/entradaestoque/novo_entradaestoque/idMovimento/' . dataReturn.idMovimento
                }
            });
        }
    };
//    this.novaentrada = function () {
//        $.ajax({
//            url: "/entradaestoque/novaentrada",
//            dataType: "json",
//            async: false,
//            type: "POST",
//            data: {
//                "idMovimento": $("#idMovimento").val()
//            },
//            success: function (dataReturn) {
//                location.reload(); // '/entradaestoque/novo_entradaestoque/idMovimento/' . dataReturn.idMovimento
//            }
//        });
//    };
    this.lerunidade = function () {
        $.ajax({
            url: "/entradaestoque/lerunidade",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idInsumo": $("#idInsumo").val(),
                "idLocalEstoque": $("#idLocalEstoque").val()
            },
            success: function (dataReturn) {
                $("#dsUnidade").val(dataReturn.dsUnidade);
                $("#qtEstoque").val(dataReturn.qtEstoque);
            }
        });
    };
    this.editamovimentoitem = function(idMovimentoItem,idCentroCusto,dsCentroCusto) {
        $('#idMovimentoItem').val(idMovimentoItem);
        $('#idCentroCusto').val(idCentroCusto);
        $('#dsCentroCusto').val(dsCentroCusto);      
    };
    this.lerpedido = function () {
        $.ajax({
            url: "/entradaestoque/lerpedido",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idTipoMovimento": $("#idTipoMovimento").val(),
                "idParceiro": $("#idParceiro").val(),
                "dtMovimento": $("#dtMovimento").val(),
                "nrNota": $("#nrNota").val(),
                "nrPedido": $("#nrPedido").val(),
                "dsObservacao": $("#dsObservacao").val()
            },
            success: function (dataReturn) {
                if (!dataReturn.ok) {
                    alert('Pedido não concluido ou não existe')
                }
                $("#idMovimento").val(dataReturn.idMovimento);
                location.reload(); 
            }
        });
    };
    this.desabilitaid = function() {
        $.ajax({
            url: "/entradaestoque/desabilitaid",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idMovimento": $("#idMovimento").val()
            },
            success: function () {
                location.reload('/dashboard');  
            }
        });
        return true;
    };
    this.delmovimentoitem = function(item) {
        $.ajax({
            url: "/entradaestoque/delmovimentoitem",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idMovimentoItem": item
            },
            success: function (dataReturn) {
                location.reload();  
            }
        });
    };
};

verificaerrosC = function() {
    var err = true;
    if ($('#idTipoMovimento').val() == '') {
        showMessage('Favor escolher o tipo de movimento');        
        err = false;
    }
    if ($('#idParceiro').val() == '') {
        showMessage('Favor escolher um Parceiro');        
        err = false;
    }
    return err;
}

verificaerrosI = function() {
    var err = true;
    if (!$('#idMovimentoItem').val() == '') {
        if ($('#idCentroCusto').val() == '') {
            showMessage('Favor escolher um Centro de Custo para o destino do Produto');        
            err = false;
        }
    } else {
        if ($('#idInsumo').val() == '') {
            showMessage('Favor escolher o produto');        
            err = false;
        }
        if ($('#qtMovimento').val() == '' || $('#qtMovimento').val() == '0,00') {
            showMessage('Favor digitar a quantidade da entrada');
            err = false;
        }
        if ($('#vlMovimento').val() == '' || $('#vlMovimento').val() == '0,00') {
            showMessage('Favor digitar o valor da entrada');
            err = false;
        }
        if ($('#idLocalEstoque').val() == '') {
            showMessage('Favor escolher um Local de Estoque para o destino do Produto');        
            err = false;
        }
        if ($('#idCentroCusto').val() == '') {
            showMessage('Favor escolher um Centro de Custo para o destino do Produto');        
            err = false;
        }
    }
    
    return err;
}