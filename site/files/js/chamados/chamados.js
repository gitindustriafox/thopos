$(document).ready(function () {
    
    $(".panel-item div.panel-body").hide();
    $("div.panel-heading").bind("click", function () {
        $(this).next().slideToggle('slow');
        return false;
    });

    $.datepicker.setDefaults({
        defaultDate: null,
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd/mm/yy",
        timeFormat: "hh:mm",
        interval: 15,
        showMinute: true,
        pick12HourFormat: true,
        orientation: 'top left',
        pickTime: true
    });
    
    $("#dtLimite").datetimepicker({
        language: 'pt-BR',
        i18n: {
            pt: {
                months: [
                    'Janeiro', 'Fevereiro', 'Março', 'Abril',
                    'Maio', 'Junho', 'Julho', 'Agosto',
                    'Setembro', 'Outubro', 'Novembro', 'Dezembro'
                ],
                dayOfWeek: [
                    "Dom", "Seg", "Ter", "Qua",
                    "Qui", "Sex", "Sab"
                ]
            }
        },
        format: 'd/m/Y H:i'
    });
    $.datetimepicker.setLocale('pt');
});

var chamado = new function () {
    this.menuChamado = function () {
        $.ajax({
            url: "/menuChamado/index_action",
            dataType: "json",
            async: false,
            type: "POST"
        });
    };
    
    this.lerUsuario = function () {
        $.ajax({
            url: "/chamados/lerUsuario",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idUsuario": $('#idUsuario').val()
            },
            success: function (dataReturn) {
                $('#dsNome').val(dataReturn.dsNome);  
                $('#dsEmail').val(dataReturn.dsEmail);  
                $('#dsCelular').val(dataReturn.dsCelular);  
            }
        });
    };
    this.closejanela = function () {
        $('#chamadosfotos_show').modal('hide');
    };
    this.gravar_chamado = function () {
        $.ajax({
            url: "/chamados/gravar_chamado",
            dataType: "json",
            async: false,
            type: "POST",
            data: $('#frm-chamado_nova').serialize(),
            success: function (dataReturn) {
                showMessage('Dados salvos com sucesso.', null, 'success');
                window.location.href = '/chamados/novo_chamado/idChamado/' + dataReturn.id;
            }
        });        
    };

    this.excluiranexo = function (idDocumento) {
        $.ajax({
            url: "/chamados/excluiranexo",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idDocumento": idDocumento,
                "idChamado": $('#idChamado').val()                
            },
            success: function (dataReturn) {
                $('#mostrarlinhas').html(dataReturn.html);
            }
        });        
    };
        
    this.selecionarfotoR=function(id) {
        // alert('id:' + id); 
        jQuery.ajax({
            async: false,
            type: "post",
            dataType: "html",
            url: "/chamados/importarfotosR",
            data: {
                "idChamado": id
            },
            complete: function (event, XMLHttpRequest) {
                if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                    try {
                        $('#chamadosfotos_show').each(function () {
                            $('.modal-body', this).html(event.responseText);
                            $(this).modal('show');
                        });
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        });
    };    
 };   
 
verificaerrosItem = function() {
    var err = true;
//    if ($('#dsAssuntoE').val() == '') {
//        showMessage('Favor digitar uma descricao para o Assunto');
//        err = false;
//    }
    return err;
};       
verificaerrosData = function() {
    var err = true;
    if ($('#dtIntermediaria').val() == '') {
        showMessage('Favor digitar uma data');
        err = false;
    }
    return err;
};       
verificaerrosTarefa = function() {
    var err = true;
    if ($('#dsTarefa').val() == '') {
        showMessage('Favor digitar uma descricao para a tarefa');
        err = false;
    }
    return err;
};       
