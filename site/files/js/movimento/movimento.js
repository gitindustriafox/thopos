$(document).ready(function() {
    $("#frm-lista-movimento [type=submit]").click(function(){
        if ($(this).hasClass('buscar')){
            $("#frm-lista-movimento").attr('action', '/listamovimentoestoque/busca_movimento');
            $("#frm-lista-movimento").submit();
        } else {
        if ($(this).hasClass('csv')){
            $("#frm-lista-movimento").attr('action', '/listamovimentoestoque/busca_criarcsv');
            $("#frm-lista-movimento").submit();
        }     
        }     
    });
    $("#frm-lista-movimento-setor [type=submit]").click(function(){
        if ($(this).hasClass('buscar')){
            $("#frm-lista-movimento-setor").attr('action', '/listamovimentoestoquesetor/busca_movimento');
            $("#frm-lista-movimento-setor").submit();
        } else {
        if ($(this).hasClass('csv')){
            $("#frm-lista-movimento-setor").attr('action', '/listamovimentoestoquesetor/busca_criarcsv');
            $("#frm-lista-movimento-setor").submit();
        }     
        }     
    });
    $.datepicker.setDefaults({
        defaultDate: null,
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd/mm/yy"
    });
    $("#dtInicio").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtSolicitacao").val("");
        }
    });    
    $("#dtFim").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtSolicitacao").val("");
        }
    });    
});