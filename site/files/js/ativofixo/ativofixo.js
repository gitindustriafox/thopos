$(document).ready(function () {
    ativofixo.autocomplete.Parceiro();
    ativofixo.autocomplete.ativofixo(); 
    ativofixo.autocomplete.ncm();
    
    $("#frm-busca-ativofixo [type=submit]").click(function(){
        if ($(this).hasClass('buscar')){
            $("#frm-busca-ativofixo").attr('action', '/ativofixo/busca_ativofixo');
            $("#frm-busca-ativofixo").submit();
        } else {
        if ($(this).hasClass('csv')){
            $("#frm-busca-ativofixo").attr('action', '/ativofixo/busca_criarcsv');
            $("#frm-busca-ativofixo").submit();
        }     
        }     
    });

    $.datepicker.setDefaults({
        defaultDate: null,
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd/mm/yy"
    });
    $("#dtInicio").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtInicio").val("");
        }
    });
    $("#dtFim").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtFim").val("");
        }
    });
    $("#dtAquisicao").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtAquisicao").val("");
        }
    });
    $("#dtBaixa").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtBaixa").val("");
        }
    });
});

$(function () {
    ativofixo.autocomplete.Parceiro();
    ativofixo.autocomplete.ativofixo();
    ativofixo.autocomplete.ncm();    
});

var ativofixo = new function () {
    this.autocomplete = new function () {
        this.ncm = function () {
            $('.complete_ncm').autocomplete({
                source: "/insumo/getNCM",
                minLength: 3,
                select: function (event, ui) {
                    var retorno = $(this).attr('data-label');
                    $('#idNCM').val(ui.item.id);
                    $("#dsNCM").val(ui.item.value);
                }
            });
            $(".complete_ncm").bind("blur keyup keydow keypress", function () {
                if ($("#dsNCM" + $(this).attr("data-label")).val() === "") {
                    $('#idNCM' + $(this).attr("data-label")).val("");
                }
            }); 
        };        
        this.Parceiro = function () {
            $('.complete_Parceiro').autocomplete({
                source: "/ativofixo/getParceiro",
                minLength: 3,
                select: function (event, ui) {
                    var retorno = $(this).attr('data-label');
                    $('#idParceiro').val(ui.item.id);
                    $("#dsParceiro").val(ui.item.value);
                }
            });
            $(".complete_Parceiro").bind("blur keyup keydow keypress", function () {
                if ($("#dsParceiro" + $(this).attr("data-label")).val() === "") {
                    $('#idParceiro' + $(this).attr("data-label")).val("");
                }
            }); 
        };
        
        this.ativofixo = function () {
            $('.complete_ativofixo').autocomplete({
                source: "/ativofixo/getAtivoFixo/ativofixo/",
                minLength: 3,
                select: function (event, ui) {
                    var retorno = $(this).attr('data-label');
                    $('#idAtivoFixo').val(ui.item.id);
                    $("#ativofixo").val(ui.item.value);
                }
            });
            $(".complete_ativofixo").bind("blur keyup keydow keypress", function () {
                if ($("#ativofixo" + $(this).attr("data-label")).val() === "") {
                    $('#idAtivoFixo' + $(this).attr("data-label")).val("");
                }
            }); 
        };
    };
    this.closejanela = function () {
        $('#ativofixofotos_show').modal('hide');
    }    
    
    this.selecionarfoto=function(idAtivoFixo) {
        jQuery.ajax({
            async: false,
            type: "post",
            dataType: "html",
            url: "/ativofixo/importarfotos",
            data: {
                "idAtivoFixo": idAtivoFixo,
                "dsTabela": 'prodAtivoFixo'
            },
            complete: function (event, XMLHttpRequest) {
                if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                    try {
                        $('#ativofixofotos_show').each(function () {
                            $('.modal-body', this).html(event.responseText);
                            $(this).modal('show');
                        });
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        });
    };
    
    this.escolherES = function () {
        if ($("#stE").prop('checked')) {
            document.getElementById("labeltm").innerHTML =
              "<label for='tipomovimento'>TIPO DE MOVIMENTO - ENTRADA</label>";
        } else {
            document.getElementById("labeltm").innerHTML =
              "<label for='tipomovimento'>TIPO DE MOVIMENTO - SAIDA</label>";
        };
        $('#idTipoMovimento').focus();
        this.carregaTipoMovimento();
    };
    
    this.foco = function () {
        $('#btn-acertoestoque').removeAttr('disabled');
    };

    this.ajustarPME = function (idEstoque) {
        $.ajax({
            url: "/ativofixo/ajustarPME",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idEstoque": idEstoque
            },
            success: function (dataReturn) {
                location.reload(); 
            }
        });
        
    };
    
    this.lerClasse = function () {
        $.ajax({
            url: "/ativofixo/lerClasseAtivo",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idTipoAtivo": $('#idTipoAtivo').val()
            },
            success: function (dataReturn) {
                $('#idClasseAtivo').html(dataReturn.html); 
            }
        });        
    };
    
    this.lerGrupo = function () {
        $.ajax({
            url: "/ativofixo/lerGrupoAtivo",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idClasseAtivo": $('#idClasseAtivo').val()
            },
            success: function (dataReturn) {
                $('#idGrupoAtivo').html(dataReturn.html); 
            }
        });        
    };
    
    this.lerCodigo = function () {
        $.ajax({
            url: "/ativofixo/lerCodigoAtivo",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idTipoAtivo": $('#idTipoAtivo').val(),
                "idClasseAtivo": $('#idClasseAtivo').val(),
                "idGrupoAtivo": $('#idGrupoAtivo').val()
            },
            success: function (dataReturn) {
                $('#idCodigoAtivo').val(dataReturn.idCodigoAtivo);
                $('#idSequenciaAtivo').val(dataReturn.idSequenciaAtivo);
            }
        });        
    };
        
    this.consistiracerto = function () {
        err = true;
        if ($('#idTipoMovimento').val() == '') {
            showMessage('Favor escolher um tipo de movimento');        
            err = false;
        }
        if ($('#dsMotivo').val() == '') {
            showMessage('Favor digitar um motivo');        
            err = false;
        }
//        if ($('#qtAcerto').val() == '') {
//            $('#qtAcerto').val('0,00');
//        }
//        if ($('#vlAcerto').val() == '') {
//            $('#vlAcerto').val('0,00');
//        }
//        if ($('#vlAcerto').val() == '0,00' &&  $('#qtAcerto').val('0,00')) {
//            showMessage('Favor digitar a quantidade ou o valor');        
//            err = false;
//        }    
        if ($('#idLocalEstoque').val() == '') {
            showMessage('Favor escolher um local de estoque - clique no botao AJUSTAR');        
            err = false;
        }
        
        return err;
    }
    
    this.carregaTipoMovimento = function () {
        var entrada_saida = ''
        if ($("#stE").prop('checked')) {
            entrada_saida = 'E';
        } else {
            entrada_saida = 'S';
        }
        
        $.ajax({
            url: "/ativofixo/carrega_tipo_mov",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "entrada_saida": entrada_saida
            },
            success: function (dataReturn) {
                $("#idTipoMovimento").html(dataReturn.html);
            }
        });
    };
    
    this.gravarlocalestoque = function () {
        $.ajax({
            url: "/ativofixo/gravar_loalestoque",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idAtivoFixo": $("#idAtivoFixo").val(),
                "idLocalEstoque": $("#idLocalEstoque").val(),
                "qtEstoqueMinimo": $("#qtEstoqueMinimo").val(),
                "qtLoteReposicao": $("#qtLoteReposicao").val(),
            },
            success: function (dataReturn) {
                $("#idLocalEstoque").val('');
                $("#qtEstoqueMinimo").val('');
                $("#qtLoteReposicao").val('');
                location.reload(); 
            }
        });
    };
 };   
