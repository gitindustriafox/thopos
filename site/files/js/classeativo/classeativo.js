$(document).ready(function(){
    $('#dsClasseAtivo').focus();  
    
    $("#vlTaxaDepreciacao").priceFormat({
        prefix: "",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    }); 
});

var classeativo = new function () {
    this.adicionaritem = function () {
        if (verificaerrosItem()) {    
            $.ajax({
                url: "/classeativo/adicionaritem",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idClasseAtivo": $("#idClasseAtivo").val(),
                    "dsGrupoAtivo": $("#dsGrupoAtivo").val()
                },
                success: function (dataReturn) {
                    $("#dsGrupoAtivo").val('');
                    $('#mostraritens').html(dataReturn.html);
                }
            });        
        }
    };
    this.delitem = function (idGrupoAtivo) {
        $.ajax({
            url: "/classeativo/delgrupo",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idClasseAtivo": $("#idClasseAtivo").val(),
                "idGrupoAtivo": idGrupoAtivo
            },
            success: function (dataReturn) {
                $('#mostraritens').html(dataReturn.html);
                showMessage(dataReturn.ok);
            }
        });        
    };
};
verificaerrosItem = function() {
    var err = true;
    if ($('#dsItem').val() == '') {
        showMessage('Favor digitar uma descricao para o Grupo');
        err = false;
    }
    return err;
}   