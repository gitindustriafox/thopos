$(document).ready(function() {
    $(".panel-item div.panel-body").hide();
    $("div.panel-heading").bind("click", function () {
        $(this).next().slideToggle('slow');
        return false;
    });
    
    $.datepicker.setDefaults({
        defaultDate: null,
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd/mm/yy"
    });
    $("#dtInicioVigencia").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtInicioVigencia").val("");
        }
    });
    $("#dtFimVigencia").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtFimVigencia").val("");
        }
    });
    $("#vlLimiteReceitas").priceFormat({
        prefix: "",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    });        
    $("#vlLimiteDespesas").priceFormat({
        prefix: "",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    });        
});

var orcamento = new function () {
    this.adicionarcolaborador = function () {
        if (verificaerrosColaborador()) {    
            $.ajax({
                url: "/orcamento/adicionarcolaborador",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idOrcamento": $("#idOrcamento").val(),
                    "idColaborador": $("#idColaborador").val()
                },
                success: function (dataReturn) {
                    $('#mostrarcolaboradores').html(dataReturn.html);
                    $('#idColaborador').html(dataReturn.html1);
                }
            });        
        }
    };
    this.delColaborador = function (idOrcamento,idOrcamentoColaborador) {
        if (confirm('DESEJA EXCLUIR ESTE REGISTRO?')) {
            $.ajax({
                url: "/orcamento/delcolaborador",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idOrcamento": idOrcamento,
                    "idOrcamentoColaborador": idOrcamentoColaborador
                },
                success: function (dataReturn) {
                    $('#mostrarcolaboradores').html(dataReturn.html);
                    $('#idColaborador').html(dataReturn.html1);
                }
            });   
        }
    };
    this.delcentrocusto = function (idOrcamento, idColaborador, idOrcamentoColaboradorCC) {
        if (confirm('DESEJA EXCLUIR ESTE REGISTRO?')) {
            $.ajax({
                url: "/orcamento/delcentrocusto",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idOrcamento": idOrcamento,
                    "idColaborador": idColaborador,
                    "idOrcamentoColaboradorCC": idOrcamentoColaboradorCC
                },
                success: function (dataReturn) {
                    $('#mostrarcentrocusto').html(dataReturn.html);
                    $('#idCentroCusto').html(dataReturn.html1);                
                }
            });        
        }
    };
    this.verCentroCusto = function(idOrcamento, idColaborador) {
        $('#idOrcamentoColaborador').val(idColaborador);
        $('#painel_centrocusto').removeClass('hidden');
        $.ajax({
            url: "/orcamento/vercentrocusto",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idOrcamento": idOrcamento,
                "idColaborador": idColaborador
            },
            success: function (dataReturn) {
                $('#mostrarcentrocusto').html(dataReturn.html);
            }
        });        
    }
    this.adicionarcentrocusto = function () {
        if (verificaerrosCentroCusto()) {    
            $.ajax({
                url: "/orcamento/adicionarcentrocusto",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idOrcamento": $("#idOrcamento").val(),
                    "idColaborador": $("#idOrcamentoColaborador").val(),
                    "idCentroCusto": $("#idCentroCusto").val(),
                    "vlLimiteDespesas": $("#vlLimiteDespesas").val(),
                    "vlLimiteReceitas": $("#vlLimiteReceitas").val()
                },
                success: function (dataReturn) {
                    $("#vlLimiteDespesas").val('');
                    $("#vlLimiteReceitas").val('');
                    $('#mostrarcentrocusto').html(dataReturn.html);
                    $('#idCentroCusto').html(dataReturn.html1);                
                }
            });        
        }
    };
    this.gravarorcamento = function () {
        if (verificaerrosOrcamento()) {
            $.ajax({
                url: "/orcamento/gravar_orcamento",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idOrcamento": $("#idOrcamento").val(),
                    "dtOrcamento": $("#dtOrcamento").val(),
                    "dtInicioVigencia": $("#dtInicioVigencia").val(),
                    "dtFimVigencia": $("#dtFimVigencia").val(),
                    "dsTermoAbertura": $("#dsTermoAbertura").val()
                },
                success: function (dataReturn) {
                    $("#idOrcamento").val(dataReturn.idOrcamento);
                    location.reload(); 
                }
            });
        }
    };    
    this.selecionarvalores = function (idGrupoDR, idItemDR, idAnoMes) {
        alert(idGrupoDR);
        alert(idItemDR);
        alert(idAnoMes);
//        $.ajax({
//            url: "/reuniao/envairemail",
//            dataType: "json",
//            async: false,
//            type: "POST",
//            data: {
//                "idReuniao": idReuniao,
//                "idParticipante": idParticipante,
//                "deOnde": deOnde
//            },
//            success: function (dataReturn) {
//                showMessage(dataReturn.ok);
//            }
//        });        
    };
    
    this.lerColaborador = function () {
        $.ajax({
            url: "/orcamento/ler_colaborador",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idColaborador": $("#idColaborador").val(),
            },
            success: function (dataReturn) {
                $("#dsSetor").val(dataReturn.dsSetor);
                $("#dsCargo").val(dataReturn.dsCargo);
                $("#dsEmail").val(dataReturn.dsEmail);
            }
        });
    };  

    this.voclear_log = function () {
        $('#verorcamento_show').modal('hide');
    };    

    this.aprovar = function (idOrcamentoColaboradorCC,stSituacao,idOrcamento,idColaborador) {
        $.ajax({
            url: "/orcamento/aprovar",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idOrcamentoColaboradorCC": idOrcamentoColaboradorCC,
                "stSituacao": stSituacao,
                "idOrcamento": idOrcamento,
                "idColaborador": idColaborador
            },
            success: function (dataReturn) {
                $('#mostrarcentrocusto').html(dataReturn.html);                
                this.voclear_log();
            }
        });
    };  
    this.aprovarOrcamento = function () {
        $.ajax({
            url: "/orcamento/aprovarOrcamento",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idOrcamento": $('#idOrcamento').val()
            },
            success: function () {
                location.reload();                 
            }
        });
    };  
 
    this.gravarvalorescc = function (idOrcamentoColaboradorCC,stSituacao,idOrcamento,idColaborador) {
        $.ajax({
            url: "/orcamento/gravarvalorescc",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idOrcamentoColaboradorCC": idOrcamentoColaboradorCC,
                "stSituacao": stSituacao,
                "idOrcamento": idOrcamento,
                "vlLimiteReceitas": $('#vlLimiteReceitas').val(),
                "vlLimiteDespesas": $('#vlLimiteDespesas').val(),
                "idColaborador": idColaborador
            },
            success: function (dataReturn) {
                $('#mostrarcentrocusto').html(dataReturn.html);                
                this.voclear_log();
            }
        });
    };  

    this.showVerOrcamento = function (idOrcamentoColaboradorCC, stSituacao, idColaborador, idOrcamento, idCentroCusto) {
        jQuery.ajax({
            async: false,
            type: "post",
            dataType: "html",
            url: "/orcamento/verorcamento",
            data: {
                "idOrcamentoColaboradorCC": idOrcamentoColaboradorCC,
                "stSituacao": stSituacao,
                "idOrcamento": idOrcamento,
                "idColaborador": idColaborador,
                "idCentroCusto": idCentroCusto,
                "dtInicioVigencia": $("#dtInicioVigencia").val(),
                "dtFimVigencia": $("#dtFimVigencia").val()
            },
            complete: function (event, XMLHttpRequest) {
                if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                    try {
                        $('#verorcamento_show').each(function () {
                            $('.modal-body', this).html(event.responseText);
                            $(this).modal('show');
                        });
                    } catch (e) {
                        console.log(e);
                    }                    
                }
            }
        });
    };    
    this.showVerOrcamentoG = function (idOrcamento) {
        jQuery.ajax({
            async: false,
            type: "post",
            dataType: "html",
            url: "/orcamento/verorcamentoG",
            data: {
                "idOrcamento": idOrcamento
            },
            complete: function (event, XMLHttpRequest) {
                if (("success" == XMLHttpRequest) && (undefined != event.responseText)) {
                    try {
                        $('#verorcamento_show').each(function () {
                            $('.modal-body', this).html(event.responseText);
                            $(this).modal('show');
                        });
                    } catch (e) {
                        console.log(e);
                    }                    
                }
            }
        });
    };    
    
    this.grafico = new function() {
        this.montarGrafico =  function (idOrcamentoColaboradorCC, idOrcamento, origem) {
            jQuery.ajax({
                async: true,
                type: "post",
                dataType: "json",
                url: "/orcamento/graficocc",
                data: {'idOrcamentoColaboradorCC' : idOrcamentoColaboradorCC,
                        'idOrcamento' : idOrcamento,
                        'origem' : origem},
                success: function (dataReturn) {
                    $('#grafico_show').modal('show');                                    
                    var dados = [];
                    dados = dataReturn.receitas;
                    dados1 = dataReturn.despesas;
                    dados2 = dataReturn.gruporeceitasmes;
                    dados3 = dataReturn.grupodespesasmes;
                    dados4 = dataReturn.acumulado;

                    google.charts.load("current", {packages:["corechart"]});
                    google.charts.setOnLoadCallback(drawChart);

                    function drawChart () {
                        var data = google.visualization.arrayToDataTable(dados,true);
                        var options = {
                            width: 470,
                            height: 400,
                            title: 'Valor das Receitas Orçadas por Grupo',
                            is3D: true
                        };
                        var data1 = google.visualization.arrayToDataTable(dados1,true);
                        var options1 = {
                            width: 470,
                            height: 400,
                            title: 'Valor das Despesas Orçadas por Grupo',
                            is3D: true
                        };
                            
                        var data2 = google.visualization.arrayToDataTable(dados2,false);
                        var options2 = {
                            width: 950,
                            height: 400,
                            title: 'Receitas por Grupo',
                            curveType: 'function',
                            pointSize: 3,                            
                            legend: { position: 'right' }                                    
                        };
                        
                        var data3 = google.visualization.arrayToDataTable(dados3,false);
                        var options3 = {
                            width: 920,
                            height: 400,
                            title: 'Despesas por Grupo',
                            pointSize: 3,                            
                            curveType: 'function',
                            legend: { position: 'right' }                                    
                        };
                        
                        var data4 = google.visualization.arrayToDataTable(dados4,false);
                        var options4 = {
                            width: 920,
                            height: 400,
                            title: 'Acumulado no Período',
                            curveType: 'function',
                            pointSize: 3,
                            series: {
                                0: { color: '#1f12f4' },
                                1: { color: '#f70707' },
                                2: { color: '#FFFF00' },                                
                                3: { color: '#31a70f', pointSize: 6, lineWidth: 4, lineDashStyle: [14, 2, 7, 2], pointShape: 'circle'  }
                            },
                            legend: { position: 'right' }                                    
                        };
                        
                        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d_PizzaTotalReceitas'));
                        var chart1 = new google.visualization.PieChart(document.getElementById('piechart_3d_PizzaTotalDespesas'));
                        var chart2 = new google.visualization.LineChart(document.getElementById('piechart_3d_LinhaTotalReceitas'));
                        var chart3 = new google.visualization.LineChart(document.getElementById('piechart_3d_LinhaTotalDespesas'));
                        var chart4 = new google.visualization.LineChart(document.getElementById('piechart_3d_LinhaAcumulado'));
                        chart.draw(data, options);                                
                        chart1.draw(data1, options1);  
                        chart2.draw(data2, options2);                                
                        chart3.draw(data3, options3);                                
                        chart4.draw(data4, options4);                                
                        
                    }
                }
            });
        };        
    }
};


verificaerrosOrcamento = function() {
    var err = true;
    if ($('#dsOrcamento').val() == '') {
        showMessage('Favor digitar a descricao do Orcamento');
        err = false;
    }
    if ($('#dtInicioVigencia').val() == '') {
        showMessage('Favor digitar a data da vigencia inicial');        
        err = false;
    }
    
    if ($('#dtFimVigencia').val() == '') {
        showMessage('Favor digitar a data da vigencia final');        
        err = false;
    }
    return err;
}
verificaerrosColaborador = function() {
    var err = true;
    if ($('#idColaborador').val() == '') {
        showMessage('Favor escolher o colaborador');
        err = false;
    }
    return err;
}
verificaerrosCentroCusto = function() {
    var err = true;
    if ($('#idCentroCusto').val() == '') {
        showMessage('Favor escolher o centro de custo');
        err = false;
    }
    return err;
}