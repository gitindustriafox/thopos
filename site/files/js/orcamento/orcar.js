
$(document).ready(function() {
    $.datepicker.setDefaults({
        defaultDate: null,
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: "dd/mm/yy"
    });
    $("#dtValorOrcado").datepicker({
        defaultDate: null,
        onClose: function (selectedDate) {
            var dia = selectedDate[0] + "" + selectedDate[1];
            var mes = selectedDate[3] + "" + selectedDate[4];
            if ((dia > 31 || dia < 1) || (mes > 12 || mes < 1))
                $("#dtValorOrcado").val("");
        }
    });
    $("#qtOrcado").priceFormat({
        prefix: "",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    });
    $("#vlOrcado").priceFormat({
        prefix: "",
        centsSeparator: ',',
        thousandsSeparator: '.',
        limit: 10,
        centsLimit: 2,
        allowNegative: true
    });    
});

var orcar = new function () {
    this.carregaritensdr = function() {
        $("#idItemDR").html("<option value selected='selected'>SELECIONE</option>");
        $.ajax({
            url: "/lancardespesas/carrega_itemdr",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idCentroCusto": $("#idCentroCusto").val(),
                "idOrcamento": $("#idOrcamento").val(),
                "idColaborador": $("#idColaborador").val(),
                "idGrupoDR": $("#idGrupoDR").val()
            },
            success: function (dataReturn) {
                $("#idItemDR").html(dataReturn.html);
                if (dataReturn.html1) {
                    $("#mostrarorcados").html(dataReturn.html1);
                }
            }
        });        
    }
    this.carregarOItensDR = function() {
        $.ajax({
            url: "/lancardespesas/carregarOItensDR",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idCentroCusto": $("#idCentroCusto").val(),
                "idOrcamento": $("#idOrcamento").val(),
                "idColaborador": $("#idColaborador").val(),
                "idGrupoDR": $("#idGrupoDR").val(),
                "idItemDR": $("#idItemDR").val()
            },
            success: function (dataReturn) {
                if (dataReturn.html) {
                    $("#mostrarorcados").html(dataReturn.html);
                }
            }
        });        
    }
    this.lerlancamentos = function() {
        $.ajax({
            url: "/lancardespesas/carrega_lancamentos",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                    "idCentroCusto": $("#idCentroCusto").val(),
                    "idOrcamento": $("#idOrcamento").val(),
                    "idColaborador": $("#idColaborador").val()
            },
            success: function (dataReturn) {
                $("#stSituacao").val(dataReturn.stSituacao);                
                if (dataReturn.encerrado == 'sim') {
                    document.getElementById("labelcc").innerHTML =
                      "<label style='color:blue;' for='centrocusto'>CENTRO CUSTO (PRONTO)</label>";
                    $('#idGrupoDR').removeAttr('disabled');
                    $('#idItemDR').removeAttr('disabled');
                    $('#qtOrcado').removeAttr('disabled');
                    $('#vlOrcado').removeAttr('disabled');
                    $('#dtValorOrcado').removeAttr('disabled');
                    $('#dsValorOrcado').removeAttr('disabled');
                    $('#optReplica').removeAttr('disabled');
                    $('#optEncerra').removeAttr('disabled');
                    document.getElementById("optEncerra").checked = true;
                    $('#adicionaorcado').removeAttr('disabled');
                } else {
                if (dataReturn.encerrado == 'nao') {
                    document.getElementById("labelcc").innerHTML =
                      "<label for='centrocusto'>CENTRO CUSTO</label>";
                    $('#idGrupoDR').removeAttr('disabled');
                    $('#idGrupoDR').removeAttr('disabled');
                    $('#idItemDR').removeAttr('disabled');
                    $('#qtOrcado').removeAttr('disabled');
                    $('#vlOrcado').removeAttr('disabled');
                    $('#dtValorOrcado').removeAttr('disabled');
                    $('#dsValorOrcado').removeAttr('disabled');
                    $('#optReplica').removeAttr('disabled');
                    $('#optEncerra').removeAttr('disabled');
                    $('#adicionaorcado').removeAttr('disabled');
                } else {
                    document.getElementById("labelcc").innerHTML =
                      "<label style='color:green;' for='centrocusto'>CENTRO CUSTO (APROVADO)</label>";
                    $('#idGrupoDR').attr('disabled','disabled');
                    $('#idItemDR').attr('disabled','disabled');
                    $('#qtOrcado').attr('disabled','disabled');
                    $('#vlOrcado').attr('disabled','disabled');
                    $('#dtValorOrcado').attr('disabled','disabled');
                    $('#dsValorOrcado').attr('disabled','disabled');
                    $('#optReplica').attr('disabled','disabled');
                    $('#optEncerra').attr('disabled','disabled');
                    $('#btn-adicionaorcado').attr('disabled','disabled');
                }
                }
                $("#mostrarorcados").html(dataReturn.html);
            }
        });        
    }
    this.dellancamento = function(idLancamento) {
        $.ajax({
            url: "/lancardespesas/del_lancamento",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                    "idCentroCusto": $("#idCentroCusto").val(),
                    "idOrcamento": $("#idOrcamento").val(),
                    "idColaborador": $("#idColaborador").val(),
                    "idLancamento": idLancamento
            },
            success: function (dataReturn) {
                $("#mostrarorcados").html(dataReturn.html);
            }
        });        
    }
    
    this.finalizarDigitacaoCC = function () {
        var encerra = '';
        if ($("#optEncerra").prop('checked')) {
           encerra = 'sim';
        } else {
           encerra = 'nao';
        }
        $.ajax({
            url: "/orcamento/finalizarDigitacaoCC",
            dataType: "json",
            async: false,
            type: "POST",
            data: {
                "idOrcamento": $('#idOrcamento').val(),
                "idCentroCusto": $('#idCentroCusto').val(),
                "encerra": encerra
            },
            success: function () {
                location.reload();                 
            }
        });
    }; 
    
    this.adicionarorcado = function() {
        var encerra = '';
        var replica = '';
        if ($("#optEncerra").prop('checked')) {
           encerra = 'sim';
        } else {
           encerra = 'nao';
        }
        if (!verificarerrosIORC(encerra)) {
            if ($("#optReplica").prop('checked')) {
               replica = 'sim';
            } else {
               replica = 'nao';
            }            
            $.ajax({
                url: "/lancardespesas/adicionarItemorcado",
                dataType: "json",
                async: false,
                type: "POST",
                data: {
                    "idCentroCusto": $("#idCentroCusto").val(),
                    "idOrcamento": $("#idOrcamento").val(),
                    "idColaborador": $("#idColaborador").val(),
                    "idGrupoDR": $("#idGrupoDR").val(),
                    "idItemDR": $("#idItemDR").val(),
                    "qtOrcado": $("#qtOrcado").val(),
                    "vlOrcado": $("#vlOrcado").val(),
                    "dtValorOrcado": $("#dtValorOrcado").val(),
                    "dtInicioVigencia": $("#dtInicioVigencia").val(),
                    "dtFimVigencia": $("#dtFimVigencia").val(),
                    "dsValorOrcado": $("#dsValorOrcado").val(),
                    "replica": replica,
                    "encerra": encerra
                },
                success: function (dataReturn) {
                    $("#idItemDR").val('');
                    $("#qtOrcado").val('1,00');
                    $("#vlOrcado").val('');
                    $("#dtValorOrcado").val('');
                    $("#dsValorOrcado").val('');
                    $("#mostrarorcados").html(dataReturn.html);
                }
            });        
        }
    }
};

verificarerrosIORC = function(encerra) {
    err = false;
    if (encerra == 'nao') {
        if ($('#idCentroCusto').val() == '') {
            showMessage('Favor escolher um Centro de Custo para Orçamento');        
            err = true;
        }
        if ($('#idGrupoDR').val() == '') {
            showMessage('Favor escolher um Grupo de Despesa/Receita para Orçamento');        
            err = true;
        }
        if ($('#idItemDR').val() == '') {
            showMessage('Favor escolher um Item de Despesa/Receita para Orçamento');        
            err = true;
        }
        if ($('#qtOrcado').val() == '' || $('#qtOrcado').val() == '0,00') {
            showMessage('Favor digitar a quantidade para orçamento');
            err = true;
        }
        if ($('#vlOrcado').val() == '' || $('#vlOrcado').val() == '0,00') {
            showMessage('Favor digitar o valor para orçamento');
            err = true;
        }
        if ($('#dtValorOrcado').val() == '') {
            showMessage('Favor digitar a data');        
            err = true;
        }

        if (!err) {
            var dtInicio = $('#dtInicioVigencia').val();
            var dtFim = $('#dtFimVigencia').val();
            var dtOrcado = $('#dtValorOrcado').val();

            dtInicio = (dtInicio.substr(6,4) +  dtInicio.substr(3,2) + dtInicio.substr(0,2)).toString();
            dtFim = (dtFim.substr(6,4) +  dtFim.substr(3,2) + dtFim.substr(0,2)).toString();
            dtOrcado = (dtOrcado.substr(6,4) +  dtOrcado.substr(3,2) + dtOrcado.substr(0,2)).toString();
            if (dtOrcado < dtInicio) {
                showMessage('A data deve ser maior que a data inicial');        
                err = true;
            }
            if (dtOrcado > dtFim) {
                showMessage('A data deve ser menor que a data final');        
                err = true;
            }
        }
    }
    return err;
}