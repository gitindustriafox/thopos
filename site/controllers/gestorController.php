<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class gestor extends controller {

    private $dsInsumo = null;
    private $idInteracao = null;
    private $idInsumo = null;
    private $idUnidade = null;
    private $idLocalEstoque = 803;
    private $idGrupo = 1;
    private $idPrioridade = 4;
    private $idParceiro = 1057;
    private $idCentroCusto = 9;
    private $qtde = 0;
    private $observacao = '';
    private $qtEmEstoque = 0;
    private $qtAComprar = 0;
    private $idSolicitacaoItem = 0;
    private $dsLocalEntrega = 'CABREUVA';
    private $dtInteracao = null;
            
    public function index_action() {

        //Inicializa o Template
        $this->template->run();
        
        // consultar o estoque e o produto conforme a descrição que recebemos
        $this->consulta_estoque();
        
        // criar o produto caso nao exista
        if (!$this->idInsumo) {
            $this->idInsumo = $this->criar_produto(); 
        }
        
        // criar estoque caso necessite
        $this->criar_estoque();        
        
        // criar solicitacao de compras caso nao tenha em estoque
        if ($$this->qtAComprar>0) {
            $id = $this->cria_solicitacao();
            $this->idSolicitacaoItem = $this->cria_solicitacao_item($id);
        }

        $this->gravarLog();
    }


    private function consulta_estoque() {
        $modelProduto = new insumoModel();
        $msql = "a.dsInsumo = '" . $this->dsInsumo . "'";
        $dados_produto = $modelProduto->getInsumo($msql);
        if ($dados_produto) {
            $this->idInsumo = $dados_produto[0]['idInsumo'];
            $this->idUnidade = $dados_produto[0]['idInsumo'];
            $modelEstoque = new estoqueModel();
            $msql = "e.cdInsumo = " . $dados_produto[0]['idInsumo'] . " and e.idLocalEstoque = " . $this->idLocalEstoque;
            $dados_estoque = $modelEstoque->getEstoque($msql);
            if ($dados_estoque) {
                if ($dados_estoque[0]['qtEstoque'] >= $this->qtde) {
                    $this->observacao = 'PRODUTO COM ESTOQUE';
                    $this->qtEmEstoque = $dados_estoque[0]['qtEstoque'];
                    $this->qtAComprar = 0;
                } else {
                    $this->observacao = 'PRODUTO SEM ESTOQUE - FOI CRIADO SOLICITACAO COMPRAS DA DIFERENCA';
                    $this->qtEmEstoque = $dados_estoque[0]['qtEstoque'];
                    $this->qtAComprar = $this->qtde - $dados_estoque[0]['qtEstoque'];
                } 
            } else {
                $this->observacao = 'PRODUTO NAO CADASTRADO - FOI CRIADO PRODUTO E SOLICITADO COMPRAS TOTAL';
                $this->qtEmEstoque = 0;
                $this->qtAComprar = $this->qtde - $dados_estoque[0]['qtEstoque'];
            }
        }
    }
    
    private function criar_produto() {
        $data['idInsumo'] = null;
        $data['dsInsumo'] = $this->dsInsumo;
        $data['cdInsumo'] = null;
        $data['idUnidade'] = $this->idUnidade;
        $data['dtValidade'] = null;
        $data['idMarca'] = null;
        $data['idModelo'] = null;
        $data['nrSerie'] = null;
        $data['dsCodigoDoFabricante'] =  null;
        $data['vlUnitario'] =  null;
        $data['idGrupo'] = $this->idGrupo;
        $data['idParceiro'] = $this->idParceiro;
        $model = new insumoModel();
        $id = $model->setinsumo($data);
        return $id;
    }
    
    private function criar_estoque() {
        $model = new estoqueModel();
        $where = 'e.idInsumo = ' . $this->idInsumo . ' and e.idEstoque = ' . $this->idEstoque;
        $retorno = $model->getEstoque($where);
        if (!$retorno) {
            $data['idEstoque'] = null;
            $data['idInsumo'] = $this->idInsumo;
            $data['idLocalEstoque'] = $this->idLocalEstoque;
            $model->setEstoque($data);        
        }        
    }
    
    private function cria_solicitacao() {        

        $model = new solicitacaocomprasModel();
        $data = array();
        $data['idSolicitacao'] = null;;
        $data['idUsuarioSolicitante'] = $_SESSION['user']['usuario'];
        $data['dsObservacao'] = $this->observacao;
        $data['dtSolicitacao'] = date('Y-m-d');
        $data['dsSolicitante'] = $_SESSION['user']['nome'];
        $data['idUsuarioDigitacao'] = $_SESSION['user']['usuario'];
        $data['idPrioridade'] = $this->idPrioridade;
        $data['dsLocalEntrega'] = $this->dsLocalEntrega;
        $data['idSituacao'] = 1;
        $id = $model->setSolicitacaoCompras($data);        
        return $id;
    }
    
    private function cria_solicitacao_item($id) {        

        $model = new solicitacaocomprasModel();
        $data = array();
        $data['idSolicitacaoItem'] = null;
        $data['idSolicitacao'] = $id;
        $data['dsProduto'] = $this->dsInsumo;
        $data['qtSolicitacao'] = $this->qtAComprar;
        $data['idUnidade'] = $this->idUnidade;
        $data['idInsumo'] = $this->idInsumo;
        $data['idParceiro'] = $this->idParceiro;
        $data['dsObservacao'] = $this->observacao;
        $data['dsParceiroSugerido'] = null;
        $data['dsLink'] = null;
        $data['idSituacao'] = 1;
        $data['idCentroCusto'] = $this->idCentroCusto;
        $data['stTipoIS'] = 0;
        $idItem = $model->setSolicitacaoComprasItem($data);
        return $idItem;
    }
    
    private function gravarLog() {        

        $model = new gestorModel();
        $data = array();
        $data['idSequencia'] = null;
        $data['idInteracao'] = $this->idInteracao;
        $data['idInsumo'] = $this->idInsumo;
        $data['qtInteracao'] = $this->qtde;
        $data['dsInsumo'] = $this->dsInsumo;
        $data['idLocalEstoque'] = $this->idLocalEstoque;
        $data['qtEstoque'] = $this->qtEmEstoque;
        $data['qtComprar'] = $this->qtAComprar;
        $data['idSolicitacaoItem'] = $this->idSolicitacaoItem;
        $data['idParceiro'] = $this->idParceiro;
        $data['dtProcesso'] = $this->dtInteracao;
        $data['dsObservacao'] = $this->observacao;
        $model->setProdutos105($data);
    }
}

?>