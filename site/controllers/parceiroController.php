<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class parceiro extends controller {

    public function index_action() {
//die("chegou");
        //Inicializa o Template
        $this->template->run();

        $model = new parceiroModel();
        $Parceiro_lista = $model->getParceiro(null,$paginacao=true);

        $this->smarty->assign('Parceiro_lista', $Parceiro_lista);
        $this->smarty->display('parceiro/lista.html');
    }

//Funcao de Busca
    public function busca_Parceiro() {
        //se nao existir o indice estou como padrao '';
        $texto = isset($_POST['buscadescricao']) ? $_POST['buscadescricao'] : '';
        //$texto = '';
        $model = new parceiroModel();
        $sql = "stStatus <> 0 and upper(dsParceiro) like upper('%" . $texto . "%')"; //somente os nao excluidos
        $resultado = $model->getParceiro($sql, $paginacao=true);

        if (sizeof($resultado) > 0) {
            $this->smarty->assign('Parceiro_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'Parceiro');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('parceiro/lista.html');
        } else {
            $this->smarty->assign('Parceiro_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'Parceiro');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('parceiro/lista.html');
        }
    }

    //Funcao de Inserir
    public function novo_Parceiro() {
        $sy = new system\System();

        $idParceiro = $sy->getParam('idParceiro');

        $model = new parceiroModel();

        if ($idParceiro > 0) {
            $registro = $model->getParceiro('idParceiro=' . $idParceiro);
            $registro = $registro[0]; //Passando Parceiro
        } else {
            //Novo Registro
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        $modelTipoF = new tipoparceiroModel();
        $lista_tipo = array('' => 'SELECIONE');
        foreach ($modelTipoF->getTipoParceiro() as $value) {
            $lista_tipo[$value['idTipoParceiro']] = $value['dsTipoParceiro'];
        }
        
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('lista_tipo', $lista_tipo);
        $this->smarty->assign('title', 'Novo Parceiro');
        $this->smarty->display('parceiro/form_novo.tpl');
    }

    // Gravar Padrao
    public function gravar_Parceiro() {
        $model = new parceiroModel();

        $data = $this->trataPost($_POST);

        if ($data['idParceiro'] == NULL)
            $model->setParceiro($data);
        else
            $model->updParceiro($data); //update
        
        header('Location: /parceiro');        
        return;
    }

    //Trata dados antes de Enviar para o Gravar
    private function trataPost($post) {
        $data['idParceiro'] = ($post['idParceiro'] != '') ? $post['idParceiro'] : null;
        $data['dsParceiro'] = ($post['dsParceiro'] != '') ? $post['dsParceiro'] : null;
        $data['dsContato'] = ($post['dsContato'] != '') ? $post['dsContato'] : null;
        $data['dsFone'] = ($post['dsFone'] != '') ? $post['dsFone'] : null;
        $data['dsEmail'] = ($post['dsEmail'] != '') ? $post['dsEmail'] : null;
        $data['dsCelular'] = ($post['dsCelular'] != '') ? $post['dsCelular'] : null;
        $data['dsSite'] = ($post['dsSite'] != '') ? $post['dsSite'] : null;
        $data['dsEndereco'] = ($post['dsEndereco'] != '') ? $post['dsEndereco'] : null;
        $data['dsCidade'] = ($post['dsCidade'] != '') ? $post['dsCidade'] : null;
        $data['cdCEP'] = ($post['cdCEP'] != '') ? $post['cdCEP'] : null;
        $data['cdUF'] = ($post['cdUF'] != '') ? $post['cdUF'] : null;
        $data['cdCNPJ'] = ($post['cdCNPJ'] != '') ? $post['cdCNPJ'] : null;
        $data['idTipoParceiro'] = ($post['idTipoParceiro'] != '') ? $post['idTipoParceiro'] : null;
        return $data;
    }

    // Remove Padrao
    public function delParceiro() {
        $sy = new system\System();
                
        $idParceiro = $sy->getParam('idParceiro');
        
        $Parceiro = $idParceiro;
        
        if (!is_null($Parceiro)) {    
            $model = new parceiroModel();
            $dados['idParceiro'] = $Parceiro;             
            $model->delParceiro($dados);
        }

        header('Location: /parceiro');
    }

    public function relatorioParceiro_pre() {
        $this->template->run();

        $this->smarty->assign('title', 'Pre Relatorio de Parceiroes');
        $this->smarty->display('parceiro/relatorio_pre.html');
    }

    public function relatorioParceiro() {
        $this->template->run();

        $model = new parceiroModel();
        $Parceiro_lista = $model->getParceiro();
        //Passa a lista de registros
        $this->smarty->assign('Parceiro_lista', $Parceiro_lista);
        $this->smarty->assign('titulo_relatorio');
        //Chama o Smarty
        $this->smarty->assign('title', 'Relatorio de Parceiroes');
        $this->smarty->display('parceiro/relatorio.html');
    }

}

?>