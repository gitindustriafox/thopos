<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class financeiro extends controller {

    public function index_action() {
        //Inicializa o Template
        $this->template->run();
        unset($_SESSION['financeiro']['id']);
        
        $model = new financeiroModel();
        $registro = $model->getFinanceiro('f.stStatus < 3',true);
        
        $lista_tipoconta = $this->tipo();
        $lista_situacao = $this->status();
        $this->smarty->assign('lista_tipo', $lista_tipoconta);
        $this->smarty->assign('lista_status', $lista_situacao);            
        $this->smarty->assign('lista_setor', $this->carrega_setor());            
        $this->smarty->assign('lista_centrocusto', $this->carrega_centrocusto());            
        $this->smarty->assign('financeiro', $registro);
        $this->smarty->assign('title', 'Financeiro');
        $this->smarty->display('financeiro/lista.html');        
    }

    private function status() {
        $status = array('' => 'SELECIONE',
            '1' => 'A INICIAR',
            '2' => 'EM ANDAMENTO',
            '3' => 'LIQUIDADO',
            '4' => 'EXCLUIDO',
            '5' => 'CANCELADO');
        return $status;
    }
    
    private function statusParcela() {
        $status = array('' => 'SELECIONE',
            '1' => 'ABERTA',
            '2' => 'BAIXADA',
            '3' => 'EXCLUIDA',
            '4' => 'CANCELADA',
            '5' => 'ADIADA');
        return $status;
    }
    
    private function tipo() {
        $tipo = array('' => 'SELECIONE','1' => 'A PAGAR','2' => 'A RECEBER');
        return $tipo;
    }
    
    private function carrega_setor() {
        $lista_setor = null;
        $model = new setorModel();
        $lista_setor = array('' => 'SELECIONE');
        foreach ($model->getSetor() as $value) {
            $lista_setor[$value['idSetor']] =  $value['dsSetor'];
        }
        return $lista_setor;
    }
    
    private function carrega_centrocusto() {
        $lista_centrocusto = null;
        $model = new centrocustoModel();
        $lista_centrocusto = array('' => 'SELECIONE');
        foreach ($model->getCentroCustoCombo() as $value) {
                $lista_centrocusto[$value['idCentroCusto']] = $value['codigocusto'];
        }
        return $lista_centrocusto;
    }
    
//Funcao de Busca
    public function busca_financeiro() {
        
        $idFinanceiro = isset($_POST['idFinanceiro']) ? $_POST['idFinanceiro'] : '';
        $dsParceiro = isset($_POST['dsParceiro']) ? $_POST['dsParceiro'] : '';
        $idParceiro = isset($_POST['idParceiro']) ? $_POST['idParceiro'] : '';
        $idPedido = isset($_POST['idPedido']) ? $_POST['idPedido'] : '';
        $nrNF = isset($_POST['nrNF']) ? $_POST['nrNF'] : '';
        $cdDocumento = isset($_POST['cdDocumento']) ? $_POST['cdDocumento'] : '';
        $idTipo = isset($_POST['idTipo']) ? $_POST['idTipo'] : '';
        $stStatus = isset($_POST['stStatus']) ? $_POST['stStatus'] : '';
        $dsObservacao = isset($_POST['dsObservacao']) ? $_POST['dsObservacao'] : '';
        $dtEmissaoI = ($_POST['dtEmissaoI'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $_POST["dtEmissaoI"]))) : '';
        $dtEmissaoF = ($_POST['dtEmissaoF'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $_POST["dtEmissaoF"]))) : '';
        $dtVencimentoI = ($_POST['dtVencimentoI'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $_POST["dtVencimentoI"]))) : '';
        $dtVencimentoF = ($_POST['dtVencimentoF'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $_POST["dtVencimentoF"]))) : '';
        $idSetor = isset($_POST['idSetor']) ? $_POST['idSetor'] : '';
        $idCentroCusto = isset($_POST['idCentroCusto']) ? $_POST['idCentroCusto'] : '';
        //$texto = '';
        $model = new financeiroModel();
        
        $lista_tipoconta = $this->tipo();
        $lista_situacao = $this->status();
        
        $this->smarty->assign('lista_tipo', $lista_tipoconta);
        $this->smarty->assign('lista_status', $lista_situacao);            
        $this->smarty->assign('lista_setor', $this->carrega_setor());            
        $this->smarty->assign('lista_centrocusto', $this->carrega_centrocusto());            
        
        $busca = array();
        $sql = 'f.idFinanceiro > 0';
        if ($idFinanceiro) {
            $sql = $sql . " and f.idFinanceiro = " . $idFinanceiro;
            $busca['idFinanceiro'] = $idFinanceiro;
        }
        if ($idPedido) {
            $sql = $sql . " and UPPER(f.idPedido) like '%" . strtoupper($idPedido) . "%'";
            $busca['idPedido'] = $idPedido;
        }
        if ($nrNF) {
            $sql = $sql . " and UPPER(f.nrNF) like '%" . strtoupper($nrNF) . "%'";
            $busca['nrNF'] = $nrNF;
        }
        if ($cdDocumento) {
            $sql = $sql . " and UPPER(f.cdDocumento) like '%" . strtoupper($cdDocumento) . "%'";
            $busca['cdDocumento'] = $cdDocumento;
        }
        if ($idTipo) {
            $sql = $sql . " and f.idTipo = " . $idTipo;
            $busca['idTipo'] = $idTipo;
        }
        if ($stStatus) {
            $sql = $sql . " and f.stStatus = " . $stStatus;
            $busca['stStatus'] = $stStatus;
        }
        if ($dsObservacao) {
            $sql = $sql . " and UPPER(f.dsObservacao) like '%" . strtoupper($dsObservacao) . "%'";
            $busca['dsObservacao'] = $dsObservacao;
        }
        if ($dtEmissaoI) {
            $sql = $sql . " and f.dtEmissao >= '" . $dtEmissaoI . "'";
            $busca['dtEmissaoI'] = $dtEmissaoI;
        }
        if ($dtEmissaoF) {
            $sql = $sql . " and f.dtEmissao <= '" . $dtEmissaoF . "'";
            $busca['dtEmissaoF'] = $dtEmissaoF;
        }
        
        if ($idParceiro) {
            if ($dsParceiro) {
                $busca['idParceiro'] = $idParceiro;
                $busca['dsParceiro'] = $dsParceiro;
            } else {
                $busca['idParceiro'] = null;
            }
        } else {
        if ($dsParceiro) {
            $sql = $sql . " and upper(par.dsParceiro) like upper('%" . $dsParceiro . "%')";
            $busca['idParceiro'] = null;
            $busca['dsParceiro'] = $dsParceiro;
        }
        }
        $cpagina = null;
        if (isset($_POST['cpagina'])) { 
            $cpagina = true;
            $busca['cpagina'] = 1;
        }
        
        $_SESSION['financeiro']['where'] = $sql;
        $resultado = $model->getFinanceiroBusca($sql, $cpagina);

        if (sizeof($resultado) > 0) {
            
            $this->smarty->assign('financeiro', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'financeiro');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('financeiro/lista.html');
        } else {
            $this->smarty->assign('financeiro', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'financeiro');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('financeiro/lista.html');
        }
    }
    
    public function busca_criarcsv() {
        $sql = $_SESSION['financeiro']['where']['sql'];
        $model = new pedidoModel();
        $resultado = $model->getPedido($sql, false);

        if ($resultado) {
            $this->criarCSV($resultado);
        }
     //   header('Location: /solicitacaocomprasmanutencao');
    }
    
    public function criarCSV($resultado) {
        
        $limit = 30000;
        $offset = 0;

        global $PATH;
        $caminho = "/var/www/html/thopos.com.br/site/storage/tmp/csv/";

        // Storage
        if (!is_dir($caminho)) {
          mkdir($caminho, 0777, true);
        }

        $filename = "pedidosemaberto_" . date("YmsHis") . ".csv";

        $headers[] = implode(";", array(
          "\"SOLICITANTE\"",
          "\"EMPRESA\"",
          "\"CENTRO CUSTO\"",
          "\"GRUPO DESPESA\"",
          "\"ITEM DESPESA\"",
          "\"FORNECEDOR\"",
          "\"ID PEDIDO\"",
          "\"DATA PEDIDO\"",
          "\"PRIORIDADE\"",
          "\"STATUS PEDIDO\"",
          "\"PRODUTO\"",
          "\"QUANTIDADE\"",
          "\"VAL ITEM\""
        ));

        if (file_exists("{$caminho}" . '/' . "{$filename}")) {
          unlink("{$caminho}" . '/' . "{$filename}");
        }

        // Arquivo
        $handle = fopen("{$caminho}" . '/' . "{$filename}", 'w+');
        fwrite($handle, implode(";" . PHP_EOL, $headers));
        fwrite($handle, ";" . PHP_EOL);
        fflush($handle);

        // Fecha o arquivo da $_SESSION para liberar o servidor para servir outras requisições
        session_write_close();

        $output = array();
        foreach ($resultado as $value) {
            $output[] = implode(";", array(
              "\"{$value["dsSolicitante"]}\"",
              "\"{$value["dsEmpresa"]}\"",
              "\"{$value["dsCentroCusto"]}\"",
              "\"{$value["dsGrupoDR"]}\"",
              "\"{$value["dsItemDR"]}\"",
              "\"{$value["dsParceiro"]}\"",
              "\"{$value["idPedido"]}\"",
              "\"{$value["dtPedido"]}\"",
              "\"{$value["dsPrioridade"]}\"",
              "\"{$value["dsSituacaoPedido"]}\"",
              "\"{$value["dsInsumo"]}\"",
              "\"{$value["qtEntregue"]}\"",
              "\"{$value["vlTotalPedido"]}\""
            ));
        }
        fwrite($handle, implode(";" . PHP_EOL, $output));
        fwrite($handle, ";" . PHP_EOL);
        fflush($handle);
        fclose($handle);
        $this->download($caminho . '/' . $filename, 'CSV', $filename);        
    }

    private function download($nome, $tipo, $filename) {
      if (!empty($nome)) {
        if (file_exists($nome)) {
          header('Content-Transfer-Encoding: binary'); // For Gecko browsers mainly
          header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($nome)) . ' GMT');
          header('Accept-Ranges: bytes'); // For download resume
          header('Content-Length: ' . filesize($nome)); // File size
          header('Content-Encoding: none');
          header("Content-Type: application/{$tipo}"); // Change this mime type if the file is not PDF
          header('Content-Disposition: attachment; filename=' . $filename);
          // Make the browser display the Save As dialog
          readfile($nome);
          unlink($nome);
        }
      }
    }    

    //Funcao de Inserir
    public function nova_financeiro() {
        $sy = new system\System();
        if ((!$sy->getParam('idFinanceiro'))) {
            $idFinanceiro=null;
            if (isset($_SESSION['financeiro']['id'])) {
                $idFinanceiro = $_SESSION['financeiro']['id'];
            }
        } else {
           $idFinanceiro=$sy->getParam('idFinanceiro');
        }
        $model = new financeiroModel();
        if (isset($idFinanceiro)) {
            if ((bool) $idFinanceiro) {
                $registro = $model->getFinanceiro('f.idFinanceiro=' . $idFinanceiro, false);  
                if ($registro) {
                    $registro = $registro[0];
                } else {
                    //Novo Registro
                    $registro = $model->estrutura_vazia();
                    $registro = $registro[0];                    
                }
            } else {
                //Novo Registro
                $registro = $model->estrutura_vazia();
                $registro = $registro[0];                    
            }
        } else {
            //Novo Registro
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        $lista_tipoconta = $this->tipo();
        $lista_situacao = $this->status();
        $this->smarty->assign('lista_tipo', $lista_tipoconta);
        $this->smarty->assign('lista_status', $lista_situacao);            
        $this->smarty->assign('lista_setor', $this->carrega_setor());            
        $this->smarty->assign('lista_centrocusto', $this->carrega_centrocusto());            

        if ($idFinanceiro) {
            $parcelas = $model->getFinanceiroParcelas('f.idFinanceiro = ' . $idFinanceiro);
        } else {
            $parcelas = null;
        }
      
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('financeiro_parcelas', $parcelas);
        
        $this->smarty->assign('title', 'Financeiro');
        $this->smarty->display('financeiro/form_novo.tpl');
    }

    public function ler_dados() {
        $idFinanceiro = $_POST['idFinanceiro'];
        $idFinanceiroParcela = $_POST['idFinanceiroParcela'];
        $model = new financeiroModel();
        $dados = $model->getFinanceiroParcelas('p.idFinanceiroParcela = ' . $idFinanceiroParcela);
        
        $lista_status = $this->statusParcela();
        
        $this->smarty->assign('idFinanceiro', $idFinanceiro);
        $this->smarty->assign('lista_status', $lista_status);
        $this->smarty->display("financeiro/parcelas/thumbnail.tpl");        
    }
    
    public function lerItemDR() {
        
        $idGrupoDR = $_POST['idGrupoDR'];
        $modelItemDR = new grupodrModel();
        $lista_itemdr = array('' => 'SELECIONE');
        foreach ($modelItemDR->getItemDr("i.idGrupoDR = " . $idGrupoDR . " and i.dsCanal = 'compras' and g.stGrupoDR = 'D'") as $value) {
            $lista_itemdr[$value['idItemDR']] = $value['dsItemDR'];
        }
        
        $this->smarty->assign('lista_itemdr', $lista_itemdr);
        $html = $this->smarty->fetch('financeiro/listaItemDR.tpl');
        $retorno = array('html' => $html);
        echo json_encode($retorno);
    }
    // Gravar Padrao
    public function novofinanceiro() {        
        
        $_SESSION['financeiro']['id'] = 0;
        $jsondata["idFinanceiro"] = null;
        $jsondata["ok"] = true;

        echo json_encode($jsondata);
    }
    
    public function mostrar_dados_produto() {
        $idInsumo = $_POST['idInsumo'];
        $lista = null;
        $movimentacao = null;
        $estoque = null;
        if ($idInsumo) {
            $model = new insumoModel();        
            $lista = $model->getInsumo("a.idInsumo = " . $idInsumo);
            if ($lista) {
                $lista = $lista[0];
            }
            $model = new movimentoModel();
            $movimentacao = $model->getMovimento('mi.idInsumo = ' . $idInsumo, 'm.dtMovimento desc', 15);

            $model = new estoqueModel();
            $estoque = $model->getEstoqueAtualPP("i.idInsumo = " . $idInsumo);
        }
        $modelTipoMovimento = new tipomovimentoModel();
        $lista_tipomovimento = array('' => 'SELECIONE');
        foreach ($modelTipoMovimento->getTipoMovimento() as $value) {
                $lista_tipomovimento[$value['idTipoMovimento']] = $value['dsTipoMovimento'];
        }
        
        $this->smarty->assign('tipomovimento', $lista_tipomovimento);
        $this->smarty->assign('produto', $lista);
        $this->smarty->assign('linhaitens', $movimentacao);
        $this->smarty->assign('estoque', $estoque);
        $this->smarty->display("financeiro/produtos/thumbnail.tpl");        
    }

    public function lerprodutostipomov() {
        $idInsumo = $_POST['idInsumo'];
        $idTipoMovimento = $_POST['idTipoMovimento'];
        $movimentacao = null;
        if ($idTipoMovimento) {
            $model = new movimentoModel();
            $movimentacao = $model->getMovimento('mi.idInsumo = ' . $idInsumo . ' and mi.idTipoMovimento = ' . $idTipoMovimento, 'm.dtMovimento desc', 15);
        }        
        $this->smarty->assign('linhaitens', $movimentacao);
        $html = $this->smarty->fetch("financeiro/produtos/thumbnail_lista.tpl");        
        $retorno = array('html' => $html);
        echo json_encode($retorno);
    }
    
    public function importarfotos() {
        $idFinanceiroItem = $_POST['idFinanceiroItem'];
        $dsTabela = $_POST['dsTabela'];
        $idFinanceiro = $_POST['idFinanceiro'];
        
        $model = new documentoModel();        
        $lista = $model->getDocumento("dsTabela = '" . $dsTabela . "' and idTabela  = " . $idFinanceiroItem);
        $this->smarty->assign('idFinanceiroItem', $idFinanceiroItem);
        $this->smarty->assign('idFinanceiroPai', $idFinanceiro);
        $this->smarty->assign('dsTabela', $dsTabela);
        $this->smarty->assign('fotos_lista', $lista);
        $this->smarty->display("financeiro/modalFotos/thumbnail.tpl");        
    }

    public function digitarmensagem() {
        $idFinanceiroItem = $_POST['idFinanceiroItem'];
        $dsTabela = $_POST['dsTabela'];
        $idFinanceiro = $_POST['idFinanceiro'];
        
        $this->smarty->assign('idFinanceiroItem', $idFinanceiroItem);
        $this->smarty->assign('idFinanceiroPai', $idFinanceiro);
        $this->smarty->assign('dsTabela', $dsTabela);
        $this->smarty->display("financeiro/mensagem/thumbnail.tpl");        
    }

    public function adicionarmensagem() {
        $idFinanceiroItem = $_POST['idFinanceiroItem'];
        $dsTabela = $_POST['dsTabela'];
        $idFinanceiro = $_POST['idFinanceiroPai'];
        $mensagem = $_POST['dsMensagem'];
        if ($mensagem) {
            // ler usuario da solictiacao para enviar msg
            $modelSC = new financeiroModel();    
            $dadosS = $modelSC->getFinanceiroItens('a.idFinanceiroItem = ' . $idFinanceiroItem);
            if ($dadosS) {
                if ($dadosS[0]['idUsuarioSolicitante'] <> $_SESSION['user']['usuario']) {            
                    $dadosi = array(
                        'idUsuarioOrigem' => $_SESSION['user']['usuario'],
                        'idUsuarioDestino' => $dadosS[0]['idUsuarioSolicitante'],
                        'dsNomeTabela' => 'prodFinanceiro',
                        'idOrigemInformacao' => 4,
                        'idTabela' => $idFinanceiroItem,
                        'idTipoMensagem' => 1,
                        'stSituacao' => 1,
                        'dtEnvio' => date('Y-m-d H:i:s'),
                        'dsMensagem' => $mensagem,
                        'dsCaminhoArquivo' => null
                    );
                    $this->criarMensagem($dadosi);
                }
            }        
        }
     //   var_dump($idFinanceiroItem, $dsTabela, $idFinanceiro, $mensagem); die;
        header('Location: /financeiromanutencao/nova_financeiro/idFinanceiro/' . $idFinanceiro);        
    }

    public function adicionarfoto() {
        $idFinanceiroItem = $_POST['idFinanceiroItem'];
        $dsTabela = $_POST['dsTabela'];
        $idFinanceiro = $_POST['idFinanceiroPai'];
        $mensagem = $_POST['dsMensagem'];
        
        $extensao = explode('.',$_FILES['arquivo']['name'])[1];
        $novonome = $idFinanceiroItem . '_' . date('Ymdis') . '.' . $extensao;
        
        $_UP['pasta'] = 'storage/tmp/documentos/';
        // Tamanho máximo do arquivo (em Bytes)
        $_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
        // Array com as extensões permitidas
        $_UP['extensoes'] = array('jpg','jpeg','png','bmp');
        // Renomeia o arquivo? (Se true, o arquivo será salvo como .jpg e um nome único)
        $_UP['renomeia'] = false;
        // Array com os tipos de erros de upload do PHP
        $_UP['erros'][0] = 'Não houve erro';
        $_UP['erros'][1] = 'O arquivo no upload é maior do que o limite do PHP';
        $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
        $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
        $_UP['erros'][4] = 'Não foi feito o upload do arquivo';
        // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
        if ($_FILES['arquivo']['error'] != 0) {
          die("Não foi possível fazer o upload, erro:" . $_UP['erros'][$_FILES['arquivo']['error']]);
          exit; // Para a execução do script
        }
        // Caso script chegue a esse ponto, não houve erro com o upload e o PHP pode continuar
        // Faz a verificação da extensão do arquivo
//        $extensao = strtolower(end(explode('.', $_FILES['arquivo']['name'])));
//        if (array_search($extensao, $_UP['extensoes']) === false) {
//          echo "Por favor, envie arquivos com as seguinte extensão: csv";
//          exit;
//        }
        // Faz a verificação do tamanho do arquivo
        if ($_UP['tamanho'] < $_FILES['arquivo']['size']) {
          echo "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
          exit;
        }
        // O arquivo passou em todas as verificações, hora de tentar movê-lo para a pasta
        // Primeiro verifica se deve trocar o nome do arquivo
//        if ($_UP['renomeia'] == true) {
//          // Cria um nome baseado no UNIX TIMESTAMP atual e com extensão .jpg
//          $nome_final = md5(time()).'.csv';
//        } else {
          // Mantém o nome original do arquivo
//          $nome_final = $_FILES['arquivo']['name'];
//        }

        // Depois verifica se é possível mover o arquivo para a pasta escolhida
        if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $_UP['pasta'] . $novonome)) {
//          Upload efetuado com sucesso, exibe uma mensagem e um link para o arquivo
//          echo "Upload efetuado com sucesso!";
//          echo '<a href="' . $_UP['pasta'] . $nome_final . '">Clique aqui para acessar o arquivo</a>';
//          
//            exit;
//          $this->importar($_UP['pasta'] . $nome_final, $idPeriodo);
//          header('Location: /planodecontas/novo_planodecontas/idPeriodo/' . $idPeriodo);

            $model = new documentoModel();    
            $dados = array(
                'idDocumento' => null,
                'dsTabela' => $dsTabela,
                'idTabela' => $idFinanceiroItem,
                'dsLocalArquivo' => $_UP['pasta'] . $novonome                
            );
            $model->setDocumento($dados);
            
            if ($mensagem) {
                // ler usuario da solictiacao para enviar msg
                $modelSC = new financeiroModel();    
                $dadosS = $modelSC->getFinanceiroItens('a.idFinanceiroItem = ' . $idFinanceiroItem);
                if ($dadosS) {
                    if ($dadosS[0]['idUsuarioSolicitante'] <> $_SESSION['user']['usuario']) {            
                        $dadosi = array(
                            'idUsuarioOrigem' => $_SESSION['user']['usuario'],
                            'idUsuarioDestino' => $dadosS[0]['idUsuarioSolicitante'],
                            'dsNomeTabela' => 'prodFinanceiro',
                            'idOrigemInformacao' => 4,
                            'idTabela' => $idFinanceiro,
                            'idTipoMensagem' => 1,
                            'stSituacao' => 1,
                            'dtEnvio' => date('Y-m-d H:i:s'),
                            'dsMensagem' => $mensagem,
                            'dsCaminhoArquivo' => $_UP['pasta'] . $novonome
                        );
                        $this->criarMensagem($dadosi);
                    }
                }                        
            }
            
        } else {
          // Não foi possível fazer o upload, provavelmente a pasta está incorreta
          echo "Não foi possível enviar o arquivo, tente novamente";
        }    
        header('Location: /financeiro/nova_financeiro/idFinanceiro/' . $idFinanceiro);
        
    }
    
    public function calcularnecessidade() {
        $idPrioridade = $_POST['idPrioridade'];   
        if ($idPrioridade == 4) {
            $dataNova = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d'))));
        } else {
        if ($idPrioridade == 3) {
            $dataNova = date('d/m/Y', strtotime('+3 days', strtotime(date('Y-m-d'))));
        } else {
        if ($idPrioridade == 2) {
            $dataNova = date('d/m/Y', strtotime('+5 days', strtotime(date('Y-m-d'))));
        } else {
            $dataNova = date('d/m/Y', strtotime('+10 days', strtotime(date('Y-m-d'))));
        }}}
        $retorno = array('datanova' => $dataNova);
        echo json_encode($retorno);        
    }
    
    public function lerusuario() {
        $idUsuario = $_POST['idUsuario'];
        $modelUsuario = new usuariosModel();
        $retorno = $modelUsuario->getUsuario('L.idUsuario = ' . $idUsuario);
        $retorno = array(
            'nomeusuario' => $retorno[0]['dsUsuario']
        );
        echo json_encode($retorno);
    }

    public function lerParceiro() {
        $idParceiro = $_POST['idParceiro'];
        $modelParceiro = new parceiroModel();
        $retorno = $modelParceiro->getParceiro('a.idParceiro = ' . $idParceiro);
        $retorno = array(
            'nomeParceiro' => $retorno[0]['dsParceiro']
        );
        echo json_encode($retorno);
    }

    public function lerunidade() {
        $idInsumo = $_POST['idInsumo'];
        $dsUnidade = null;
        $qtEstoque = null;
        $nome_produto = null;
        if ($idInsumo) {
            $modelUnidade = new unidadeModel();
            $opcao = $_POST['opcao'];
            if ($opcao == '0') { // ler produtos
                $where = 'idInsumo = ' . $idInsumo;
                $retorno = $modelUnidade->getInsumoUnidade($where);
                if ($retorno) {
                    $idUnidade = $retorno[0]['idUnidade'];
                    $nome_produto = $retorno[0]['dsInsumo'];
                }
            } else {
                $where = 'idServico = ' . $idInsumo;
                $retorno = $modelUnidade->getServicoUnidade($where);
                if ($retorno) {
                    $idUnidade = $retorno[0]['idUnidade'];
                    $nome_produto = $retorno[0]['dsServico'];
                }
            }
        }
        $jsondata["idUnidade"] = $dsUnidade;
        $jsondata["dsProduto"] = $nome_produto;
        $jsondata["ok"] = true;
        echo json_encode($jsondata);
    }
        
    public function atualiza_unidades() {
        $modelUnidade = new unidadeModel();
        $lista_unidade = array('' => 'SELECIONE');
        foreach ($modelUnidade->getUnidade() as $value) {
            $lista_unidade[$value['idUnidade']] = $value['dsUnidade'];
        }
        $this->smarty->assign('lista_unidades', $lista_unidade);
        $html = $this->smarty->fetch('financeiro/lista_unidades.tpl');
        $retorno = array(
            'html' => $html
        );
        echo json_encode($retorno);
    }

//    public function atualiza_produtos() {
//        
//        $opcao = $_POST['opcao'];
//        
//        if ($opcao == '0') { // ler produtos
//            $modelInsumo = new insumoModel();
//            $lista_insumo = array('' => 'SELECIONE');
//            foreach ($modelInsumo->getInsumo() as $value) {
//                $lista_insumo[$value['idInsumo']] = $value['cdInsumo'] . '-' . $value['dsInsumo'];
//            }
//        } else { // ler servicos
//            $modelInsumo = new servicoModel();
//            $lista_insumo = array('' => 'SELECIONE');
//            foreach ($modelInsumo->getServico() as $value) {
//                $lista_insumo[$value['idServico']] = $value['dsServico'];
//            }
//        }
//        
//        $this->smarty->assign('lista_insumo', $lista_insumo);
//        $html = $this->smarty->fetch('pedido/lista_produtos.tpl');
//        $retorno = array(
//            'html' => $html
//        );
//        echo json_encode($retorno);
//    }
//    
//    public function atualiza_Parceiroes() {
//        $modelParceiro = new parceiroModel();
//        $lista_Parceiro = array('' => 'SELECIONE');
//        foreach ($modelParceiro->getParceiro() as $value) {
//            $lista_Parceiro[$value['idParceiro']] = $value['dsParceiro'];
//        }
//        $this->smarty->assign('lista_Parceiro', $lista_Parceiro);
//        $html = $this->smarty->fetch('financeiro/lista_Parceiroes.tpl');
//        $retorno = array(
//            'html' => $html
//        );
//        echo json_encode($retorno);
//    }
//    
    
    public function desabilitaid() {
        unset($_SESSION['financeiro']['id']);
        echo json_encode(true);
    }
    
    public function gravar_financeiro() {
        
        $model = new financeiroModel();
        $data = $this->trataPost($_POST);

        if ($_POST['idFinanceiro'] == '') {
            $id = $model->setFinanceiro($data);
            $dtVencimento = ($_POST['dtVencimento'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $_POST["dtVencimento"]))) : date('Y-m-d h:m:s');
            $qtParcelas = $_POST['qtParcelas'];
            $qtIntervalo = $_POST['qtIntervalo'];
            $dtVencimento_parcela = $dtVencimento;
            $dataP = array();
            for ($x=1;$x<=$qtParcelas;$x++) {
                $dataP['idFinanceiroParcela'] = null;
                $dataP['idFinanceiro'] = $id;
                $dataP['nrParcela'] = $x;
                $dataP['stSituacao'] = 1;
                $dataP['vlParcela'] = $data['vlTotal'] / $qtParcelas;
                $dataP['dtVencimento'] = $dtVencimento_parcela;
                $model->setFinanceiroP($dataP);  
                $dtVencimento_parcela = date('Y-m-d', strtotime('+' . $qtIntervalo . ' days', strtotime($dtVencimento_parcela))) . ' 00:00:00';                
            }
        } else {
            $id = $_POST['idFinanceiro'];
            $where = 'idFinanceiro = ' . $id;
            $model->updFinanceiro($data, $where);             
        }
        
        $parcelas = $model->getFinanceiroParcelas('idFinanceiro = ' . $id);

        $_SESSION['financeiro']['id'] = $id;
//        $jsondata["html"] = "financeiro/form_novo.tpl";
//        $jsondata["html1"] = "financeiro/lista_parcelas.html";
        $jsondata["idFinanceiro"] = $id;
        $jsondata["ok"] = true;
        echo json_encode($jsondata);
    }

    public function gravar_item() {
        $idFinanceiro = $_POST['idFinanceiro'];
        $vlParcela = ($_POST['vlParcela'] != '') ? str_replace(",",".",str_replace(".","",$_POST['vlParcela'])) : null;
        $dtVencimento = ($_POST['dtVencimento'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $_POST["dtVencimento"]))) : date('Y-m-d h:m:s');
        
        $model = new financeiroModel();
        $dataP=array();
        $dataP['idFinanceiroParcela'] = null;
        $dataP['idFinanceiro'] = $idFinanceiro;
        $dataP['nrParcela'] = 10;
        $dataP['stSituacao'] = 1;
        $dataP['vlParcela'] = $vlParcela;
        $dataP['dtVencimento'] = $dtVencimento;
        $model->setFinanceiroP($dataP);  
        $jsondata["ok"] = true;
        echo json_encode($jsondata);
    }
    
    //Trata dados antes de Enviar para o Gravar
    private function trataPost($post) {
        $data = array();
        $data['idFinanceiro'] = ($post['idFinanceiro'] != '') ? $post['idFinanceiro'] : null;;
        $data['idParceiro'] = ($post['idParceiro'] != '') ? $post['idParceiro'] : null;
        $data['dsObservacao'] = ($post['dsObservacao'] != '') ? $post['dsObservacao'] : null;
        $data['dtEmissao'] = ($post['dtEmissao'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $post["dtEmissao"]))) : date('Y-m-d h:m:s');
        $data['idTipo'] = ($post['idTipo'] != '') ? $post['idTipo'] : null;
        $data['idUsuario'] = $_SESSION['user']['usuario'];
        $data['idPedido'] = ($post['idPedido'] != '') ? $post['idPedido'] : null;
        $data['cdDocumento'] = ($post['cdDocumento'] != '') ? $post['cdDocumento'] : null;
        $data['nrNF'] = ($post['nrNF'] != '') ? $post['nrNF'] : null;
        $data['stStatus'] = ($post['stStatus'] != '') ? $post['stStatus'] : null;
        $data['cdDocumento'] = ($post['cdDocumento'] != '') ? $post['cdDocumento'] : null;
        $data['vlTotal'] = ($post['vlTotal'] != '') ? str_replace(",",".",str_replace(".","",$post['vlTotal'])) : null;
        return $data;
    }

    private function trataPostItem($post) {
        $data = array();
        $data['idFinanceiroItem'] = ($post['idFinanceiroItem'] != '') ? $post['idFinanceiroItem'] : null;
        $data['idFinanceiro'] = ($post['idFinanceiro'] != '') ? $post['idFinanceiro'] : null;
        $data['dsProduto'] = ($post['dsProduto'] != '') ? $post['dsProduto'] : null;
        $data['qtFinanceiro'] = ($post['qtFinanceiro'] != '') ? str_replace(",",".",str_replace(".","",$post['qtFinanceiro'])) : null;
        $data['idUnidade'] = ($post['idUnidade'] != '') ? $post['idUnidade'] : null;
        $data['idInsumo'] = ($post['idInsumo'] != '') ? $post['idInsumo'] : null;
        $data['idParceiro'] = ($post['idParceiro'] != '') ? $post['idParceiro'] : null;
        $data['dsObservacao'] = ($post['dsObservacao'] != '') ? $post['dsObservacao'] : null;
        $data['dsParceiroSugerido'] = ($post['dsParceiroSugerido'] != '') ? $post['dsParceiroSugerido'] : null;
        $data['dsLink'] = ($post['dsLink'] != '') ? $post['dsLink'] : null;
        $data['idSituacao'] = 1;
        $data['idItemDR'] = ($post['idItemDR'] != '') ? $post['idItemDR'] : null;
        $data['dsJustificativa'] = ($post['dsJustificativa'] != '') ? $post['dsJustificativa'] : null;
        $data['idGrupoDR'] = ($post['idGrupoDR'] != '') ? $post['idGrupoDR'] : null;
        $data['idCentroCusto'] = ($post['idCentroCusto'] != '') ? $post['idCentroCusto'] : null;
        $data['stTipoIS'] = ($post['opcao'] != '') ? $post['opcao'] : null;
        
        return $data;
    }

    // Remove Padrao
    public function delfinanceiroitem() {                
        $idFinanceiroItem = $_POST['idFinanceiroItem'];
        $model = new financeiroModel();
        $where = 'idFinanceiroItem = ' . $idFinanceiroItem;             
        $model->delFinanceiroItem($where);
        echo json_encode(array());        
    }
    public function arquivar() {   
        $idFinanceiro = $_POST['idFinanceiro'];
        $idFinanceiroItem = $_POST['idFinanceiroItem'];
        $model = new financeiroModel();
        $dados = array('idSituacao' => 9);
        $where = 'idFinanceiro = ' . $idFinanceiro . ' and idFinanceiroItem = ' . $idFinanceiroItem;             
        $model->updFinanceiroItem($dados, $where);
        
        $where = 'idFinanceiro = ' . $idFinanceiro . ' and idSituacao <> 9';
        $retorno = $model->getFinanceiroItensE($where);
        if (!$retorno) {
            $where = 'idFinanceiro = ' . $idFinanceiro;
            $model->updFinanceiro($dados, $where);
        }
        echo json_encode(array());        
    }

    public function delfinanceiro() {
        $idFinanceiro = $_POST['idFinanceiro'];
        $model = new financeiroModel();
        $where = '(f.idFinanceiro = ' . $idFinanceiro . ' and f.stStatus = 0) '
                . ' or (isnull(p.idFinanceiro))';
        
        $ok = $model->getFinanceiroParcelas($where);
        if ($ok) {
            $dados = array('idFinanceiro' => $idFinanceiro, 'stStatus' => 4);
            $model->updFinanceiro($dados,'idFinanceiro = ' . $idFinanceiro);
            $status = 1;
        } else {
            $status = 0;
        }
        echo json_encode(array());
    }

    public function baixamanual() {                
        $idFinanceiro = $sy->getParam('idFinanceiro');
        $model = new financeiroModel();
        $where = 'idFinanceiro = ' . $idFinanceiro;             
        $dados = array('stSituacao' => 2, 'idUsuarioBaixa' => $_SESSION['user']['usuario'], 'dtBaixa' => date('Y-m-d h:m:s'), 'nrNota' => '', 'idOrigemInformacao' => 1);
        $model->updFinanceiro($dados, $where);
        header('Location: /financeiroaberto');        
        return;
    }
    public function relatoriofinanceiro_pre() {
        $this->template->run();

        $this->smarty->assign('title', 'Pre Relatorio de Financeiros');
        $this->smarty->display('financeiro/relatorio_pre.html');
    }

    public function relatoriofinanceiro() {
        $this->template->run();

        $model = new financeiroModel();
        $financeiro_lista = $model->getFinanceiro();
        //Passa a lista de registros
        $this->smarty->assign('financeiro_lista', $financeiro_lista);
        $this->smarty->assign('titulo_relatorio');
        //Chama o Smarty
        $this->smarty->assign('title', 'Relatorio de Financeiros');
        $this->smarty->display('financeiro/relatorio.html');
    }
    
    public function getParceiro() {
        $url = explode("=", $_SERVER['REQUEST_URI']);
        $key = str_replace('+', ' ', $url[1]);
        if (!empty($key)) {
          $busca = trim($key);
          $model = new parceiroModel();
          $where = "(UPPER(dsParceiro) like UPPER('%{$key}%') OR UPPER(cdCNPJ) like UPPER('%{$key}%'))";
          $retorno = $model->getParceiro($where);
          $return = array();
          if (count($retorno)) {
            $row = array();
            for ($i = 0; $i < count($retorno); $i++) {
              $cnpj = $retorno[$i]["cdCNPJ"];
              $row['value'] = strtoupper($retorno[$i]["dsParceiro"]) . '-' . $cnpj;
              $row["id"] = $retorno[$i]["idParceiro"];
              array_push($return, $row);
            }
          }
          echo json_encode($return);
        }
    }    
    
    public function getProduto() {
        $url = explode("=", $_SERVER['REQUEST_URI']);
        $key = str_replace('+', ' ', $url[1]);
        $tipo = substr($url[0],31,6); 
        if ($tipo=='insumo') {
            if (!empty($key)) {
              $busca = trim($key);
              $model = new insumoModel();
              $where = "(UPPER(a.dsInsumo) like UPPER('%{$busca}%') OR UPPER(a.cdInsumo) like UPPER('%{$busca}%'))";
              $retorno = $model->getInsumo($where);
              $return = array();
              if (count($retorno)) {
                $row = array();
                for ($i = 0; $i < count($retorno); $i++) {
                  $cdInsump = $retorno[$i]["cdInsumo"];
                  $row['value'] = strtoupper($retorno[$i]["dsInsumo"]) . '-' . $cdInsump;
                  $row["id"] = $retorno[$i]["idInsumo"];
                  array_push($return, $row);
                }
              }
              echo json_encode($return);
            }
        } else {
            if (!empty($key)) {
              $busca = trim($key);
              $model = new servicoModel();
              $where = "(UPPER(a.dsServico) like UPPER('%{$busca}%'))";
              $retorno = $model->getServico($where);
              $return = array();
              if (count($retorno)) {
                $row = array();
                for ($i = 0; $i < count($retorno); $i++) {                  
                  $row['value'] = strtoupper($retorno[$i]["dsServico"]);
                  $row["id"] = $retorno[$i]["idServico"];
                  array_push($return, $row);
                }
              }
              echo json_encode($return);
            }
        } 
    }
    
    public function criarMensagem($array) {
        
        $modelMensagem = new mensagemModel();
        $dados = array(
          'idMensagem' => null,  
          'idOrigemInformacao' => $array['idOrigemInformacao'],
          'dsNomeTabela' => $array['dsNomeTabela'],
          'idTabela' => $array['idTabela']
        );
        $id = $modelMensagem->setMensagem($dados);
        $dados = array(
          'idMensagemAnterior' => $id,              
        );        
        $modelMensagem->updMensagem($dados, 'idMensagem = ' . $id);
        $dados = array(
            'idMensagemItem' => null,  
            'idMensagem' => $id,  
            'idUsuarioOrigem' => $array['idUsuarioOrigem'],
            'idUsuarioDestino' => $array['idUsuarioDestino'],
            'idTipoMensagem' => $array['idTipoMensagem'],
            'stSituacao' => $array['stSituacao'],
            'dtEnvio' => $array['dtEnvio'],
            'dsMensagem' => $array['dsMensagem'],
            'dsCaminhoArquivo' => $array['dsCaminhoArquivo']
        );
        $id = $modelMensagem->setMensagemItem($dados);
        return;
    }

    public function importarprodutos_mostrar() {
        $idFinanceiro = $_POST['idFinanceiro'];
        $idParceiro = $_POST['idParceiro'];
        $idUnidade = $_POST['idUnidade'];
        $dsObservacaoItem = $_POST['dsObservacaoItem'];
        $idCentroCusto = $_POST['idCentroCusto'];
        $dsParceiroSugerido = $_POST['dsParceiroSugerido'];
        
        $this->smarty->assign('idFinanceiro', $idFinanceiro);
        $this->smarty->assign('idParceiro', $idParceiro);
        $this->smarty->assign('idUnidade', $idUnidade);
        $this->smarty->assign('dsObservacaoItem', $dsObservacaoItem);
        $this->smarty->assign('idCentroCusto', $idCentroCusto);
        $this->smarty->assign('dsParceiroSugerido', $dsParceiroSugerido);
        $this->smarty->display("financeiro/modalImportar/thumbnail.tpl");        
    }
    
    public function importarprodutos() {
        $idFinanceiro = $_POST['idFinanceiro'];
        $idParceiro = $_POST['idParceiro'];
        $idUnidade = $_POST['idUnidade'];
        $dsObservacaoItem = $_POST['dsObservacaoItem'];
        $idCentroCusto = $_POST['idCentroCusto'];
        $dsParceiroSugerido = $_POST['dsParceiroSugerido'];
        
        $_UP['pasta'] = 'storage/tmp/arquivosparaimportar/';
        // Tamanho máximo do arquivo (em Bytes)
        $_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
        // Array com as extensões permitidas
        $_UP['extensoes'] = array('csv');
        // Renomeia o arquivo? (Se true, o arquivo será salvo como .jpg e um nome único)
        $_UP['renomeia'] = false;
        // Array com os tipos de erros de upload do PHP
        $_UP['erros'][0] = 'Não houve erro';
        $_UP['erros'][1] = 'O arquivo no upload é maior do que o limite do PHP';
        $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
        $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
        $_UP['erros'][4] = 'Não foi feito o upload do arquivo';
        // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
        if ($_FILES['arquivo']['error'] != 0) {
          die("Não foi possível fazer o upload, erro:" . $_UP['erros'][$_FILES['arquivo']['error']]);
          exit; // Para a execução do script
        }
        // Caso script chegue a esse ponto, não houve erro com o upload e o PHP pode continuar
        // Faz a verificação da extensão do arquivo
//        $extensao = strtolower(end(explode('.', $_FILES['arquivo']['name'])));
//        if (array_search($extensao, $_UP['extensoes']) === false) {
//          echo "Por favor, envie arquivos com as seguinte extensão: csv";
//          exit;
//        }
        // Faz a verificação do tamanho do arquivo
        if ($_UP['tamanho'] < $_FILES['arquivo']['size']) {
          echo "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
          exit;
        }
        // O arquivo passou em todas as verificações, hora de tentar movê-lo para a pasta
        // Primeiro verifica se deve trocar o nome do arquivo
        if ($_UP['renomeia'] == true) {
          // Cria um nome baseado no UNIX TIMESTAMP atual e com extensão .jpg
          $nome_final = md5(time()).'.csv';
        } else {
          // Mantém o nome original do arquivo
          $nome_final = $_FILES['arquivo']['name'];
        }

        // Depois verifica se é possível mover o arquivo para a pasta escolhida
        if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $_UP['pasta'] . $nome_final)) {
//          Upload efetuado com sucesso, exibe uma mensagem e um link para o arquivo
//          echo "Upload efetuado com sucesso!";
//          echo '<a href="' . $_UP['pasta'] . $nome_final . '">Clique aqui para acessar o arquivo</a>';
//          
//            exit;
          $this->importar($_UP['pasta'] . $nome_final, $idFinanceiro, $idParceiro,$idUnidade,$dsObservacaoItem,$idCentroCusto, $dsParceiroSugerido);
          header('Location: /financeiro/nova_financeiro/idFinanceiro/' . $idFinanceiro);
          
        } else {
          // Não foi possível fazer o upload, provavelmente a pasta está incorreta
          echo "Não foi possível enviar o arquivo, tente novamente";
        }    
        
    }
    
    private function importar($arquivo, $idFinanceiro, $idParceiro,$idUnidade,$dsObservacaoItem,$idCentroCusto, $dsParceiroSugerido) {
        global $PATH;

        $delimitador = ',';
        $cerca = '"';
        $f = fopen($arquivo, 'r');
        
        while (($data = fgetcsv($f, 0, $delimitador, $cerca)) !== FALSE) {
            $dados = array();
            $dados['dsProduto'] = $data[0];
            $dados['cdProduto'] = $data[1];
            $dados['qtProduto'] = $data[2];
            $this->lerdados_csv($dados, $idFinanceiro, $idParceiro,$idUnidade,$dsObservacaoItem,$idCentroCusto, $dsParceiroSugerido);  
        }     

        fclose($f);        
    }

    private function lerdados_csv($dados, $idFinanceiro, $idParceiro,$idUnidade,$dsObservacaoItem,$idCentroCusto, $dsParceiroSugerido) {

        $model = new insumoModel();
        $where = '';
        $dadosProduto = $model->getInsumo("a.cdInsumo = '" . $dados['cdProduto'] . "' or a.dsCodigoDoFabricante = '" . $dados['cdProduto'] . "'");
        if ($dadosProduto) {
            $data['idInsumo'] = $dadosProduto[0]['idInsumo'];
        } else {
            // cadastrar o produto
            $dadosdoproduto = array(
              'idInsumo' => null,
              'cdInsumo' => $dados['cdProduto'],
              'dsCodigoDoFabricante' => $dados['cdProduto'],
              'idGrupo' => 66,
              'idUnidade' => $idUnidade,
              'dtCadastro' => date('Y-m-d H:i:s'),
              'dsInsumo' => $dados['dsProduto'] . ' - ' . $dados['cdProduto']
            );
            $data['idInsumo'] = $model->setInsumo($dadosdoproduto);
        }
        
        $data['idFinanceiroItem'] = null;
        $data['idFinanceiro'] = $idFinanceiro;
        $data['dsProduto'] = $dados['dsProduto'] . ' - ' . $dados['cdProduto'];
        $data['qtFinanceiro'] = $dados['qtProduto'];
        $data['idUnidade'] = $idUnidade;        
        $data['idParceiro'] = $idParceiro;
        $data['dsObservacao'] = $dsObservacaoItem;
        $data['dsParceiroSugerido'] = $dsParceiroSugerido;
        $data['dsLink'] =  null;
        $data['idSituacao'] = 1;
        $data['idItemDR'] =  null;
        $data['dsJustificativa'] = null;
        $data['idGrupoDR'] =  null;
        $data['idCentroCusto'] = $idCentroCusto;
        $data['stTipoIS'] = 0;

        $model = new financeiroModel();
        $model->setFinanceiroItem($data);
    }    
}

?>