<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class solicitacaocompras extends controller {

    public function index_action() {
        //Inicializa o Template
        $this->template->run();
        unset($_SESSION['solicitacaocompras']['id']);
        
        $model = new solicitacaocomprasModel();
        $registro = $model->getSolicitacaoComprasManutencao('a.idUsuarioSolicitante = ' . $_SESSION['user']['usuario'], $paginacao=true);
        
        $pModel = new prioridadeModel();        
        $lista_Prioridade = array('' => 'SELECIONE');
        foreach ($pModel->getPrioridade() as $value) {
            $lista_Prioridade[$value['idPrioridade']] = $value['dsPrioridade'];
        }
        
        $modelSituacao = new situacaoModel();
        $lista_situacao = array('' => 'SELECIONE');
        foreach ($modelSituacao->getSituacao() as $value) {
            $lista_situacao[$value['idSituacao']] = $value['dsSituacao'];
        }

        $this->smarty->assign('lista_prioridade', $lista_Prioridade);
        $this->smarty->assign('lista_situacao', $lista_situacao);            
        $this->smarty->assign('solicitacaocompras', $registro);
        $this->smarty->assign('title', 'Solicitacao de Compras');
        $this->smarty->display('solicitacaocompras/lista.html');
        
    }

//Funcao de Busca
    public function busca_solicitacaocompras() {
        
        //se nao existir o indice estou como padrao '';
        $dsSolicitante = isset($_POST['dsSolicitante']) ? $_POST['dsSolicitante'] : '';
        $dsProduto = isset($_POST['dsProduto']) ? $_POST['dsProduto'] : '';
        $cdInsumo = isset($_POST['cdInsumo']) ? $_POST['cdInsumo'] : '';
        $dsParceiro = isset($_POST['dsParceiro']) ? $_POST['dsParceiro'] : '';
        $idParceiro = isset($_POST['idParceiro']) ? $_POST['idParceiro'] : '';
        $dsObservacao = isset($_POST['dsObservacao']) ? $_POST['dsObservacao'] : '';
        $idSituacao = isset($_POST['idSituacao']) ? $_POST['idSituacao'] : '';
        $idPrioridade = isset($_POST['idPrioridade']) ? $_POST['idPrioridade'] : '';
        $dsLocalEntrega = isset($_POST['dsLocalEntrega']) ? $_POST['dsLocalEntrega'] : '';
        //$texto = '';
        $model = new solicitacaocomprasModel();
        
        $pModel = new prioridadeModel();        
        $lista_Prioridade = array('' => 'SELECIONE');
        foreach ($pModel->getPrioridade() as $value) {
            $lista_Prioridade[$value['idPrioridade']] = $value['dsPrioridade'];
        }
        
        $modelSituacao = new situacaoModel();
        $lista_situacao = array('' => 'SELECIONE');
        foreach ($modelSituacao->getSituacao() as $value) {
            $lista_situacao[$value['idSituacao']] = $value['dsSituacao'];
        }
        
        $busca = array();
        $sql = 'a.idSolicitacao > 0';
        if ($idSituacao) {
            $sql = $sql . " and si.idSituacao = " . $idSituacao;
            $busca['idSituacao'] = $idSituacao;
        }
        if ($idPrioridade) {
            $sql = $sql . " and a.idPrioridade = " . $idPrioridade;
            $busca['idPrioridade'] = $idPrioridade;
        }
        if ($dsProduto) {
            $sql = $sql . " and upper(i.dsProduto) like upper('%" . $dsProduto . "%')";
            $busca['dsProduto'] = $dsProduto;
        }
        if ($idParceiro) {
            if ($dsParceiro) {
                $sql = $sql . " and i.idParceiro = " . $idParceiro;
                $busca['idParceiro'] = $idParceiro;
                $busca['dsParceiro'] = $dsParceiro;
            } else {
                $busca['idParceiro'] = null;
            }
        } else {
        if ($dsParceiro) {
            $sql = $sql . " and upper(i.dsParceiroSugerido) like upper('%" . $dsParceiro . "%')";
            $busca['idParceiro'] = null;
            $busca['dsParceiro'] = $dsParceiro;
        }
        }
        if ($dsObservacao) {
            $sql = $sql . " and upper(a.dsObservacao) like upper('%" . $dsObservacao . "%')";
            $busca['dsObservacao'] = $dsObservacao;
        }
        if ($dsSolicitante) {
            $sql = $sql . " and upper(a.dsSolicitante) like upper('%" . $dsSolicitante . "%')";
            $busca['dsSolicitante'] = $dsSolicitante;
        }
        if ($dsLocalEntrega) {
            $sql = $sql . " and upper(a.dsLocalEntrega) like upper('%" . $dsLocalEntrega . "%')";
            $busca['dsLocalEntrega'] = $dsLocalEntrega;
        }
        if ($cdInsumo) {
            $sql = $sql . " and upper(ins.cdInsumo) like upper('%" . $cdInsumo . "%')";
            $busca['cdInsumo'] = $cdInsumo;
        }

        $cpagina = null;
        if (isset($_POST['cpagina'])) { 
            $cpagina = true;
            $busca['cpagina'] = 1;
        }
        
        $_SESSION['solicitacao']['where'] = $sql;
        $resultado = $model->getSolicitacaoComprasManutencao($sql, $cpagina);

        if (sizeof($resultado) > 0) {
            
            $this->smarty->assign('lista_prioridade', $lista_Prioridade);
            $this->smarty->assign('lista_situacao', $lista_situacao);            
            $this->smarty->assign('solicitacaocompras', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'solicitacaocompras');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('solicitacaocompras/lista.html');
        } else {
            $this->smarty->assign('lista_prioridade', $lista_Prioridade);
            $this->smarty->assign('lista_situacao', $lista_situacao);            
            $this->smarty->assign('solicitacaocompras', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'solicitacaocompras');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('solicitacaocompras/lista.html');
        }
    }

    //Funcao de Inserir
    public function nova_solicitacao() {
        $sy = new system\System();
 
        if (!isset($_SESSION['solicitacaocompras']['id'])) {
           $idSolicitacao = $sy->getParam('idSolicitacao');
        } else {
          if ($_SESSION['solicitacaocompras']['id'] == 0) {
             $idSolicitacao = null;
          } else {
            $idSolicitacao = $_SESSION['solicitacaocompras']['id'];            
          }
        }
        
        $model = new solicitacaocomprasModel();
        if (isset($idSolicitacao)) {
            if ((bool) $idSolicitacao) {
                $registro = $model->getSolicitacaoCompras('a.idSolicitacao=' . $idSolicitacao);  
                if ($registro) {
                    $registro = $registro[0];
                } else {
                    //Novo Registro
                    $registro = $model->estrutura_vazia();
                    $registro = $registro[0];                    
                }
            } else {
                //Novo Registro
                $registro = $model->estrutura_vazia();
                $registro = $registro[0];                    
                $registro['idUsuarioSolicitante'] = $_SESSION['user']['usuario'];
                $registro['dsSolicitante'] = $_SESSION['user']['nome'];
            }
        } else {
            //Novo Registro
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
            $registro['idUsuarioSolicitante'] = $_SESSION['user']['usuario'];
            $registro['dsSolicitante'] = $_SESSION['user']['nome'];
        }
        
//        var_dump($registro); 
        $modelUsuario = new usuariosModel();
        $lista_usuario = array('' => 'SELECIONE');
        foreach ($modelUsuario->getUsuario() as $value) {
            $lista_usuario[$value['idUsuario']] = $value['dsUsuario'];
        }
        $modelUnidade = new unidadeModel();
        $lista_unidade = array('' => 'SELECIONE');
        foreach ($modelUnidade->getUnidade() as $value) {
            $lista_unidade[$value['idUnidade']] = $value['dsUnidade'];
        }
        
//        $modelInsumo = new insumoModel();
//        $lista_insumo = array('' => 'SELECIONE');
//        foreach ($modelInsumo->getInsumo() as $value) {
//            $lista_insumo[$value['idInsumo']] = $value['cdInsumo'] . '-' . $value['dsInsumo'];
//        }
//        
//        $modelParceiro = new parceiroModel();
//        $lista_Parceiro = array('' => 'SELECIONE');
//        foreach ($modelParceiro->getParceiro() as $value) {
//            $lista_Parceiro[$value['idParceiro']] = $value['dsParceiro'];
//        }
//
        $modelCentroCusto = new centrocustoModel();
        $lista_centrocusto = array('' => 'SELECIONE');
        foreach ($modelCentroCusto->getCentroCustoCombo() as $value) {
            $lista_centrocusto[$value['idCentroCusto']] = $value['codigocusto'];
        }
        $modelGrupoDR = new grupodrModel();
        $lista_grupodr = array('' => 'SELECIONE');
        foreach ($modelGrupoDR->getGrupoDR() as $value) {
            $lista_grupodr[$value['idGrupoDR']] = $value['dsGrupoDR'];
        }
//        $motivolOS = new motivoModel();
//        $lista_Motivo = array('' => 'SELECIONE');
//        foreach ($motivolOS->getMotivo() as $value) {
//            $lista_Motivo[$value['idMotivo']] = $value['dsMotivo'];
//        }
        
        $pModel = new prioridadeModel();        
        $lista_Prioridade = array('' => 'SELECIONE');
        foreach ($pModel->getPrioridade() as $value) {
            $lista_Prioridade[$value['idPrioridade']] = $value['dsPrioridade'];
        }

        $solicitacaocomprasitens = array();
        if($idSolicitacao) {
            $where = "a.idSolicitacao = " . $idSolicitacao;
            $solicitacaocomprasitens = $model->getSolicitacaoComprasItens($where);
        }        
        
//        $this->smarty->assign('nrSolicitacao', null);
//        if (!$idSolicitacao) {
//            $nrUltimaSolicitacao = $model->getUltimaSolicitacao()[0];
//            if ($nrUltimaSolicitacao['ultimo']) {                
//                $registro['nrSolicitacao'] = ($nrUltimaSolicitacao['ultimo'] !='') ? $nrUltimaSolicitacao['ultimo'] + 1 :null;
//            }            
//        }
//        
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('lista_usuario', $lista_usuario);
        $this->smarty->assign('lista_prioridade', $lista_Prioridade);
        $this->smarty->assign('lista_unidades', $lista_unidade);
//        $this->smarty->assign('lista_insumo', $lista_insumo);
//        $this->smarty->assign('lista_Parceiro', $lista_Parceiro);
        $this->smarty->assign('lista_centrocusto', $lista_centrocusto);
        $this->smarty->assign('lista_grupodr', $lista_grupodr);
        $this->smarty->assign('lista_itemdr', null);
//        $this->smarty->assign('lista_motivo', $lista_Motivo);
        
        $this->smarty->assign('solicitacaocomprasitens', $solicitacaocomprasitens);
        $this->smarty->assign('title', 'Novo Solicitacao de Compra');
        $this->smarty->display('solicitacaocompras/form_novo.tpl');
    }
    
    public function lerItemDR() {
        
        $idGrupoDR = $_POST['idGrupoDR'];
        $modelItemDR = new grupodrModel();
        $lista_itemdr = array('' => 'SELECIONE');
        foreach ($modelItemDR->getItemDr("i.idGrupoDR = " . $idGrupoDR . " and i.dsCanal = 'compras' and g.stGrupoDR = 'D'") as $value) {
            $lista_itemdr[$value['idItemDR']] = $value['dsItemDR'];
        }
        
        $this->smarty->assign('lista_itemdr', $lista_itemdr);
        $html = $this->smarty->fetch('solicitacaocompras/listaItemDR.tpl');
        $retorno = array('html' => $html);
        echo json_encode($retorno);
    }
    // Gravar Padrao
    public function novosolicitacaocompras() {        
        
        $_SESSION['solicitacaocompras']['id'] = 0;
        $jsondata["idSolicitacao"] = null;
        $jsondata["ok"] = true;

        echo json_encode($jsondata);
    }
    
    public function mostrar_dados_produto() {
        $idInsumo = $_POST['idInsumo'];
        $lista = null;
        $movimentacao = null;
        $estoque = null;
        if ($idInsumo) {
            $model = new insumoModel();        
            $lista = $model->getInsumo("a.idInsumo = " . $idInsumo);
            if ($lista) {
                $lista = $lista[0];
            }
            $model = new movimentoModel();
            $movimentacao = $model->getMovimento('mi.idInsumo = ' . $idInsumo, 'm.dtMovimento desc', 15);

            $model = new estoqueModel();
            $estoque = $model->getEstoqueAtualPP("i.idInsumo = " . $idInsumo);
        }
        $modelTipoMovimento = new tipomovimentoModel();
        $lista_tipomovimento = array('' => 'SELECIONE');
        foreach ($modelTipoMovimento->getTipoMovimento() as $value) {
                $lista_tipomovimento[$value['idTipoMovimento']] = $value['dsTipoMovimento'];
        }
        
        $this->smarty->assign('tipomovimento', $lista_tipomovimento);
        $this->smarty->assign('produto', $lista);
        $this->smarty->assign('linhaitens', $movimentacao);
        $this->smarty->assign('estoque', $estoque);
        $this->smarty->display("solicitacaocompras/produtos/thumbnail.tpl");        
    }

    public function lerprodutostipomov() {
        $idInsumo = $_POST['idInsumo'];
        $idTipoMovimento = $_POST['idTipoMovimento'];
        $movimentacao = null;
        if ($idTipoMovimento) {
            $model = new movimentoModel();
            $movimentacao = $model->getMovimento('mi.idInsumo = ' . $idInsumo . ' and mi.idTipoMovimento = ' . $idTipoMovimento, 'm.dtMovimento desc', 15);
        }        
        $this->smarty->assign('linhaitens', $movimentacao);
        $html = $this->smarty->fetch("solicitacaocompras/produtos/thumbnail_lista.tpl");        
        $retorno = array('html' => $html);
        echo json_encode($retorno);
    }
    
    public function importarfotos() {
        $idSolicitacaoItem = $_POST['idSolicitacaoItem'];
        $dsTabela = $_POST['dsTabela'];
        $idSolicitacao = $_POST['idSolicitacao'];
        
        $model = new documentoModel();        
        $lista = $model->getDocumento("dsTabela = '" . $dsTabela . "' and idTabela  = " . $idSolicitacaoItem);
        $this->smarty->assign('idSolicitacaoItem', $idSolicitacaoItem);
        $this->smarty->assign('idSolicitacaoPai', $idSolicitacao);
        $this->smarty->assign('dsTabela', $dsTabela);
        $this->smarty->assign('fotos_lista', $lista);
        $this->smarty->display("solicitacaocompras/modalFotos/thumbnail.tpl");        
    }

    public function digitarmensagem() {
        $idSolicitacaoItem = $_POST['idSolicitacaoItem'];
        $dsTabela = $_POST['dsTabela'];
        $idSolicitacao = $_POST['idSolicitacao'];
        
        $this->smarty->assign('idSolicitacaoItem', $idSolicitacaoItem);
        $this->smarty->assign('idSolicitacaoPai', $idSolicitacao);
        $this->smarty->assign('dsTabela', $dsTabela);
        $this->smarty->display("solicitacaocompras/mensagem/thumbnail.tpl");        
    }

    public function adicionarmensagem() {
        $idSolicitacaoItem = $_POST['idSolicitacaoItem'];
        $dsTabela = $_POST['dsTabela'];
        $idSolicitacao = $_POST['idSolicitacaoPai'];
        $mensagem = $_POST['dsMensagem'];
        if ($mensagem) {
            // ler usuario da solictiacao para enviar msg
            $modelSC = new solicitacaocomprasModel();    
            $dadosS = $modelSC->getSolicitacaoComprasItens('a.idSolicitacaoItem = ' . $idSolicitacaoItem);
            if ($dadosS) {
                if ($dadosS[0]['idUsuarioSolicitante'] <> $_SESSION['user']['usuario']) {            
                    $dadosi = array(
                        'idUsuarioOrigem' => $_SESSION['user']['usuario'],
                        'idUsuarioDestino' => $dadosS[0]['idUsuarioSolicitante'],
                        'dsNomeTabela' => 'prodSolicitacaoCompras',
                        'idOrigemInformacao' => 4,
                        'idTabela' => $idSolicitacaoItem,
                        'idTipoMensagem' => 1,
                        'stSituacao' => 0,
                        'dtEnvio' => date('Y-m-d H:i:s'),
                        'dsMensagem' => $mensagem,
                        'dsCaminhoArquivo' => null
                    );
                    $this->criarMensagem($dadosi);
                }
            }        
        }
     //   var_dump($idSolicitacaoItem, $dsTabela, $idSolicitacao, $mensagem); die;
        header('Location: /solicitacaocomprasmanutencao/nova_solicitacao/idSolicitacao/' . $idSolicitacao);        
    }

    public function adicionarfoto() {
        $idSolicitacaoItem = $_POST['idSolicitacaoItem'];
        $dsTabela = $_POST['dsTabela'];
        $idSolicitacao = $_POST['idSolicitacaoPai'];
        $mensagem = $_POST['dsMensagem'];
        
        $extensao = explode('.',$_FILES['arquivo']['name'])[1];
        $novonome = $idSolicitacaoItem . '_' . date('Ymdis') . '.' . $extensao;
        
        $_UP['pasta'] = 'storage/tmp/documentos/';
        // Tamanho máximo do arquivo (em Bytes)
        $_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
        // Array com as extensões permitidas
        $_UP['extensoes'] = array('jpg','jpeg','png','bmp');
        // Renomeia o arquivo? (Se true, o arquivo será salvo como .jpg e um nome único)
        $_UP['renomeia'] = false;
        // Array com os tipos de erros de upload do PHP
        $_UP['erros'][0] = 'Não houve erro';
        $_UP['erros'][1] = 'O arquivo no upload é maior do que o limite do PHP';
        $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
        $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
        $_UP['erros'][4] = 'Não foi feito o upload do arquivo';
        // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
        if ($_FILES['arquivo']['error'] != 0) {
          die("Não foi possível fazer o upload, erro:" . $_UP['erros'][$_FILES['arquivo']['error']]);
          exit; // Para a execução do script
        }
        // Caso script chegue a esse ponto, não houve erro com o upload e o PHP pode continuar
        // Faz a verificação da extensão do arquivo
//        $extensao = strtolower(end(explode('.', $_FILES['arquivo']['name'])));
//        if (array_search($extensao, $_UP['extensoes']) === false) {
//          echo "Por favor, envie arquivos com as seguinte extensão: csv";
//          exit;
//        }
        // Faz a verificação do tamanho do arquivo
        if ($_UP['tamanho'] < $_FILES['arquivo']['size']) {
          echo "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
          exit;
        }
        // O arquivo passou em todas as verificações, hora de tentar movê-lo para a pasta
        // Primeiro verifica se deve trocar o nome do arquivo
//        if ($_UP['renomeia'] == true) {
//          // Cria um nome baseado no UNIX TIMESTAMP atual e com extensão .jpg
//          $nome_final = md5(time()).'.csv';
//        } else {
          // Mantém o nome original do arquivo
//          $nome_final = $_FILES['arquivo']['name'];
//        }

        // Depois verifica se é possível mover o arquivo para a pasta escolhida
        if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $_UP['pasta'] . $novonome)) {
//          Upload efetuado com sucesso, exibe uma mensagem e um link para o arquivo
//          echo "Upload efetuado com sucesso!";
//          echo '<a href="' . $_UP['pasta'] . $nome_final . '">Clique aqui para acessar o arquivo</a>';
//          
//            exit;
//          $this->importar($_UP['pasta'] . $nome_final, $idPeriodo);
//          header('Location: /planodecontas/novo_planodecontas/idPeriodo/' . $idPeriodo);

            $model = new documentoModel();    
            $dados = array(
                'idDocumento' => null,
                'dsTabela' => $dsTabela,
                'idTabela' => $idSolicitacaoItem,
                'dsLocalArquivo' => $_UP['pasta'] . $novonome                
            );
            $model->setDocumento($dados);
            
            if ($mensagem) {
                // ler usuario da solictiacao para enviar msg
                $modelSC = new solicitacaocomprasModel();    
                $dadosS = $modelSC->getSolicitacaoComprasItens('a.idSolicitacaoItem = ' . $idSolicitacaoItem);
                if ($dadosS) {
                    if ($dadosS[0]['idUsuarioSolicitante'] <> $_SESSION['user']['usuario']) {            
                        $dadosi = array(
                            'idUsuarioOrigem' => $_SESSION['user']['usuario'],
                            'idUsuarioDestino' => $dadosS[0]['idUsuarioSolicitante'],
                            'dsNomeTabela' => 'prodSolicitacaoCompras',
                            'idOrigemInformacao' => 4,
                            'idTabela' => $idSolicitacao,
                            'idTipoMensagem' => 1,
                            'stSituacao' => 0,
                            'dtEnvio' => date('Y-m-d H:i:s'),
                            'dsMensagem' => $mensagem,
                            'dsCaminhoArquivo' => $_UP['pasta'] . $novonome
                        );
                        $this->criarMensagem($dadosi);
                    }
                }                        
            }
            
        } else {
          // Não foi possível fazer o upload, provavelmente a pasta está incorreta
          echo "Não foi possível enviar o arquivo, tente novamente";
        }    
        header('Location: /solicitacaocompras/nova_solicitacao/idSolicitacao/' . $idSolicitacao);
        
    }
    
    public function calcularnecessidade() {
        $idPrioridade = $_POST['idPrioridade'];   
        if ($idPrioridade == 4) {
            $dataNova = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d'))));
        } else {
        if ($idPrioridade == 3) {
            $dataNova = date('d/m/Y', strtotime('+3 days', strtotime(date('Y-m-d'))));
        } else {
        if ($idPrioridade == 2) {
            $dataNova = date('d/m/Y', strtotime('+5 days', strtotime(date('Y-m-d'))));
        } else {
            $dataNova = date('d/m/Y', strtotime('+10 days', strtotime(date('Y-m-d'))));
        }}}
        $retorno = array('datanova' => $dataNova);
        echo json_encode($retorno);        
    }
    
    public function lerusuario() {
        $idUsuario = $_POST['idUsuario'];
        $modelUsuario = new usuariosModel();
        $retorno = $modelUsuario->getUsuario('L.idUsuario = ' . $idUsuario);
        $retorno = array(
            'nomeusuario' => $retorno[0]['dsUsuario']
        );
        echo json_encode($retorno);
    }

    public function lerParceiro() {
        $idParceiro = $_POST['idParceiro'];
        $modelParceiro = new parceiroModel();
        $retorno = $modelParceiro->getParceiro('a.idParceiro = ' . $idParceiro);
        $retorno = array(
            'nomeParceiro' => $retorno[0]['dsParceiro']
        );
        echo json_encode($retorno);
    }

    public function lerunidade() {
        $idInsumo = $_POST['idInsumo'];
        $dsUnidade = null;
        $qtEstoque = null;
        $nome_produto = null;
        if ($idInsumo) {
            $modelUnidade = new unidadeModel();
            $opcao = $_POST['opcao'];
            if ($opcao == '0') { // ler produtos
                $where = 'idInsumo = ' . $idInsumo;
                $retorno = $modelUnidade->getInsumoUnidade($where);
                if ($retorno) {
                    $idUnidade = $retorno[0]['idUnidade'];
                    $nome_produto = $retorno[0]['dsInsumo'];
                }
            } else {
                $where = 'idServico = ' . $idInsumo;
                $retorno = $modelUnidade->getServicoUnidade($where);
                if ($retorno) {
                    $idUnidade = $retorno[0]['idUnidade'];
                    $nome_produto = $retorno[0]['dsServico'];
                }
            }
        }
        $jsondata["idUnidade"] = $dsUnidade;
        $jsondata["dsProduto"] = $nome_produto;
        $jsondata["ok"] = true;
        echo json_encode($jsondata);
    }
        
    public function atualiza_unidades() {
        $modelUnidade = new unidadeModel();
        $lista_unidade = array('' => 'SELECIONE');
        foreach ($modelUnidade->getUnidade() as $value) {
            $lista_unidade[$value['idUnidade']] = $value['dsUnidade'];
        }
        $this->smarty->assign('lista_unidades', $lista_unidade);
        $html = $this->smarty->fetch('solicitacaocompras/lista_unidades.tpl');
        $retorno = array(
            'html' => $html
        );
        echo json_encode($retorno);
    }

//    public function atualiza_produtos() {
//        
//        $opcao = $_POST['opcao'];
//        
//        if ($opcao == '0') { // ler produtos
//            $modelInsumo = new insumoModel();
//            $lista_insumo = array('' => 'SELECIONE');
//            foreach ($modelInsumo->getInsumo() as $value) {
//                $lista_insumo[$value['idInsumo']] = $value['cdInsumo'] . '-' . $value['dsInsumo'];
//            }
//        } else { // ler servicos
//            $modelInsumo = new servicoModel();
//            $lista_insumo = array('' => 'SELECIONE');
//            foreach ($modelInsumo->getServico() as $value) {
//                $lista_insumo[$value['idServico']] = $value['dsServico'];
//            }
//        }
//        
//        $this->smarty->assign('lista_insumo', $lista_insumo);
//        $html = $this->smarty->fetch('pedido/lista_produtos.tpl');
//        $retorno = array(
//            'html' => $html
//        );
//        echo json_encode($retorno);
//    }
//    
//    public function atualiza_Parceiroes() {
//        $modelParceiro = new parceiroModel();
//        $lista_Parceiro = array('' => 'SELECIONE');
//        foreach ($modelParceiro->getParceiro() as $value) {
//            $lista_Parceiro[$value['idParceiro']] = $value['dsParceiro'];
//        }
//        $this->smarty->assign('lista_Parceiro', $lista_Parceiro);
//        $html = $this->smarty->fetch('solicitacaocompras/lista_Parceiroes.tpl');
//        $retorno = array(
//            'html' => $html
//        );
//        echo json_encode($retorno);
//    }
//    
    
    public function desabilitaid() {
        unset($_SESSION['solicitacaocompras']['id']);
        echo json_encode(true);
    }
    
    public function gravar_solicitacao() {
        
        $model = new solicitacaocomprasModel();
        $data = $this->trataPost($_POST);

        if ($_POST['idSolicitacao'] == '') {
            $data['idSituacao'] = 1;
            $id = $model->setSolicitacaoCompras($data);
        } else {
            $id = $_POST['idSolicitacao'];
            $where = 'idSolicitacao = ' . $id;
            $model->updSolicitacaoCompras($data, $where);             
        }

        $_SESSION['solicitacaocompras']['id'] = $id;
        $jsondata["html"] = "solicitacaocompras/form_novo.tpl";
        $jsondata["idSolicitacao"] = $id;
        $jsondata["ok"] = true;
        echo json_encode($jsondata);
    }

    public function gravar_item() {
        $model = new solicitacaocomprasModel();
        $data = $this->trataPostItem($_POST);
        if ($data['idSolicitacaoItem']) {
            $where = 'idSolicitacaoItem = ' . $data['idSolicitacaoItem'];
            $model->updSolicitacaoComprasItemManut($data, $where);
        } else {
            $model->setSolicitacaoComprasItem($data);            
        }
    
        $jsondata["html"] = "solicitacaocompras/lista_produtos.tpl";
        $jsondata["idSolicitacaoItem"] = $data['idSolicitacaoItem'];
        $jsondata["ok"] = true;
        echo json_encode($jsondata);
    }
    
    //Trata dados antes de Enviar para o Gravar
    private function trataPost($post) {
        $data = array();
        $data['idSolicitacao'] = ($post['idSolicitacao'] != '') ? $post['idSolicitacao'] : null;;
        $data['idUsuarioSolicitante'] = ($post['idUsuario'] != '') ? $post['idUsuario'] : null;
        $data['dsObservacao'] = ($post['dsObservacao'] != '') ? $post['dsObservacao'] : null;
        $data['dtSolicitacao'] = ($post['dtSolicitacao'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $_POST["dtSolicitacao"]))) : date('Y-m-d h:m:s');
        $data['dtNecessidade'] = ($post['dtNecessidade'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $_POST["dtNecessidade"]))) : date('Y-m-d h:m:s');
        $data['dsSolicitante'] = ($post['dsSolicitante'] != '') ? $post['dsSolicitante'] : null;
        $data['idUsuarioDigitacao'] = $_SESSION['user']['usuario'];
        $data['idPrioridade'] = ($post['idPrioridade'] != '') ? $post['idPrioridade'] : null;
        $data['dsLocalEntrega'] = ($post['dsLocalEntrega'] != '') ? $post['dsLocalEntrega'] : null;
        return $data;
    }

    private function trataPostItem($post) {
        $data = array();
        $data['idSolicitacaoItem'] = ($post['idSolicitacaoItem'] != '') ? $post['idSolicitacaoItem'] : null;
        $data['idSolicitacao'] = ($post['idSolicitacao'] != '') ? $post['idSolicitacao'] : null;
        $data['dsProduto'] = ($post['dsProduto'] != '') ? $post['dsProduto'] : null;
        $data['qtSolicitacao'] = ($post['qtSolicitacao'] != '') ? str_replace(",",".",str_replace(".","",$post['qtSolicitacao'])) : null;
        $data['idUnidade'] = ($post['idUnidade'] != '') ? $post['idUnidade'] : null;
        $data['idInsumo'] = ($post['idInsumo'] != '') ? $post['idInsumo'] : null;
        $data['idParceiro'] = ($post['idParceiro'] != '') ? $post['idParceiro'] : null;
        $data['dsObservacao'] = ($post['dsObservacao'] != '') ? $post['dsObservacao'] : null;
        $data['dsParceiroSugerido'] = ($post['dsParceiroSugerido'] != '') ? $post['dsParceiroSugerido'] : null;
        $data['dsLink'] = ($post['dsLink'] != '') ? $post['dsLink'] : null;
        $data['idSituacao'] = 1;
        $data['idItemDR'] = ($post['idItemDR'] != '') ? $post['idItemDR'] : null;
        $data['dsJustificativa'] = ($post['dsJustificativa'] != '') ? $post['dsJustificativa'] : null;
        $data['idGrupoDR'] = ($post['idGrupoDR'] != '') ? $post['idGrupoDR'] : null;
        $data['idCentroCusto'] = ($post['idCentroCusto'] != '') ? $post['idCentroCusto'] : null;
        $data['stTipoIS'] = ($post['opcao'] != '') ? $post['opcao'] : null;
        
        return $data;
    }

    // Remove Padrao
    public function delsolicitacaocomprasitem() {                
        $idSolicitacaoItem = $_POST['idSolicitacaoItem'];
        $model = new solicitacaocomprasModel();
        $where = 'idSolicitacaoItem = ' . $idSolicitacaoItem;             
        $model->delSolicitacaoComprasItem($where);
        echo json_encode(array());        
    }

    public function delfinanceiroitem() {                
        $idFinanceiroParcela = $_POST['idFinanceiroParcela'];
        $model = new solicitacaocomprasModel();
        $where = 'idFinanceiroParcela = ' . $idFinanceiroParcela;             
        $model->delFinanceiroItem($where);
        $jsondata["ok"] = true;
        echo json_encode($jsondata);        
    }

    public function arquivar() {   
        $idSolicitacao = $_POST['idSolicitacao'];
        $idSolicitacaoItem = $_POST['idSolicitacaoItem'];
        $model = new solicitacaocomprasModel();
        $dados = array('idSituacao' => 9);
        $where = 'idSolicitacao = ' . $idSolicitacao . ' and idSolicitacaoItem = ' . $idSolicitacaoItem;             
        $model->updSolicitacaoComprasItem($dados, $where);
        
        $where = 'idSolicitacao = ' . $idSolicitacao . ' and idSituacao <> 9';
        $retorno = $model->getSolicitacaoComprasItensE($where);
        if (!$retorno) {
            $where = 'idSolicitacao = ' . $idSolicitacao;
            $model->updSolicitacaoCompras($dados, $where);
        }
        echo json_encode(array());        
    }
    
    public function delsolicitacaocompras() {
        $idSolicitacao = $_POST['idSolicitacao'];
        $model = new solicitacaocomprasModel();
        $where = 'idSolicitacao = ' . $idSolicitacao;
//        
//        $ok = $model->getSolicitacaoComprasItensE($where);
//        if (!$ok) {
//            $model->delSolicitacaoComprasItem($where);
            $model->delSolicitacaoCompras($where);
//        } else {
//            header('Location: /solicitacaocompras/nova_solicitacao/idSolicitacao/' . $idSolicitacao);        
//        }
        echo json_encode(array());
    }

    public function baixamanual() {                
        $idSolicitacao = $sy->getParam('idSolicitacao');
        $model = new solicitacaocomprasModel();
        $where = 'idSolicitacao = ' . $idSolicitacao;             
        $dados = array('stSituacao' => 2, 'idUsuarioBaixa' => $_SESSION['user']['usuario'], 'dtBaixa' => date('Y-m-d h:m:s'), 'nrNota' => '', 'idOrigemInformacao' => 1);
        $model->updSolicitacao($dados, $where);
        header('Location: /solicitacaocomprasaberto');        
        return;
    }
    public function relatoriosolicitacaocompras_pre() {
        $this->template->run();

        $this->smarty->assign('title', 'Pre Relatorio de Solicitacaos');
        $this->smarty->display('solicitacaocompras/relatorio_pre.html');
    }

    public function relatoriosolicitacaocompras() {
        $this->template->run();

        $model = new solicitacaocomprasModel();
        $solicitacaocompras_lista = $model->getSolicitacao();
        //Passa a lista de registros
        $this->smarty->assign('solicitacaocompras_lista', $solicitacaocompras_lista);
        $this->smarty->assign('titulo_relatorio');
        //Chama o Smarty
        $this->smarty->assign('title', 'Relatorio de Solicitacaos');
        $this->smarty->display('solicitacaocompras/relatorio.html');
    }
    
    public function getParceiro() {
        $url = explode("=", $_SERVER['REQUEST_URI']);
        $key = str_replace('+', ' ', $url[1]);
        if (!empty($key)) {
          $busca = trim($key);
          $model = new parceiroModel();
          $where = "(UPPER(dsParceiro) like UPPER('%{$key}%') OR UPPER(cdCNPJ) like UPPER('%{$key}%'))";
          $retorno = $model->getParceiro($where);
          $return = array();
          if (count($retorno)) {
            $row = array();
            for ($i = 0; $i < count($retorno); $i++) {
              $cnpj = $retorno[$i]["cdCNPJ"];
              $row['value'] = strtoupper($retorno[$i]["dsParceiro"]) . '-' . $cnpj;
              $row["id"] = $retorno[$i]["idParceiro"];
              array_push($return, $row);
            }
          }
          echo json_encode($return);
        }
    }    
    
    public function getProduto() {
        $url = explode("=", $_SERVER['REQUEST_URI']);
        $key = str_replace('+', ' ', $url[1]);
        $tipo = substr($url[0],31,6); 
        if ($tipo=='insumo') {
            if (!empty($key)) {
              $busca = trim($key);
              $model = new insumoModel();
              $where = "(UPPER(a.dsInsumo) like UPPER('%{$busca}%') OR UPPER(a.cdInsumo) like UPPER('%{$busca}%'))";
              $retorno = $model->getInsumo($where);
              $return = array();
              if (count($retorno)) {
                $row = array();
                for ($i = 0; $i < count($retorno); $i++) {
                  $cdInsump = $retorno[$i]["cdInsumo"];
                  $row['value'] = strtoupper($retorno[$i]["dsInsumo"]) . '-' . $cdInsump;
                  $row["id"] = $retorno[$i]["idInsumo"];
                  array_push($return, $row);
                }
              }
              echo json_encode($return);
            }
        } else {
            if (!empty($key)) {
              $busca = trim($key);
              $model = new servicoModel();
              $where = "(UPPER(a.dsServico) like UPPER('%{$busca}%'))";
              $retorno = $model->getServico($where);
              $return = array();
              if (count($retorno)) {
                $row = array();
                for ($i = 0; $i < count($retorno); $i++) {                  
                  $row['value'] = strtoupper($retorno[$i]["dsServico"]);
                  $row["id"] = $retorno[$i]["idServico"];
                  array_push($return, $row);
                }
              }
              echo json_encode($return);
            }
        } 
    }
    
    public function criarMensagem($array) {
        
        $modelMensagem = new mensagemModel();
        $dados = array(
          'idMensagem' => null,  
          'idOrigemInformacao' => $array['idOrigemInformacao'],
          'dsNomeTabela' => $array['dsNomeTabela'],
          'idTabela' => $array['idTabela']
        );
        $id = $modelMensagem->setMensagem($dados);
        $dados = array(
          'idMensagemAnterior' => $id,              
        );        
        $modelMensagem->updMensagem($dados, 'idMensagem = ' . $id);
        $dados = array(
            'idMensagemItem' => null,  
            'idMensagem' => $id,  
            'idUsuarioOrigem' => $array['idUsuarioOrigem'],
            'idUsuarioDestino' => $array['idUsuarioDestino'],
            'idTipoMensagem' => $array['idTipoMensagem'],
            'stSituacao' => $array['stSituacao'],
            'dtEnvio' => $array['dtEnvio'],
            'dsMensagem' => $array['dsMensagem'],
            'dsCaminhoArquivo' => $array['dsCaminhoArquivo']
        );
        $id = $modelMensagem->setMensagemItem($dados);
        return;
    }

    public function importarprodutos_mostrar() {
        $idSolicitacao = $_POST['idSolicitacao'];
        $idParceiro = $_POST['idParceiro'];
        $idUnidade = $_POST['idUnidade'];
        $dsObservacaoItem = $_POST['dsObservacaoItem'];
        $idCentroCusto = $_POST['idCentroCusto'];
        $dsParceiroSugerido = $_POST['dsParceiroSugerido'];
        
        $this->smarty->assign('idSolicitacao', $idSolicitacao);
        $this->smarty->assign('idParceiro', $idParceiro);
        $this->smarty->assign('idUnidade', $idUnidade);
        $this->smarty->assign('dsObservacaoItem', $dsObservacaoItem);
        $this->smarty->assign('idCentroCusto', $idCentroCusto);
        $this->smarty->assign('dsParceiroSugerido', $dsParceiroSugerido);
        $this->smarty->display("solicitacaocompras/modalImportar/thumbnail.tpl");        
    }
    
    public function importarprodutos() {
        $idSolicitacao = $_POST['idSolicitacao'];
        $idParceiro = $_POST['idParceiro'];
        $idUnidade = $_POST['idUnidade'];
        $dsObservacaoItem = $_POST['dsObservacaoItem'];
        $idCentroCusto = $_POST['idCentroCusto'];
        $dsParceiroSugerido = $_POST['dsParceiroSugerido'];
        
        $_UP['pasta'] = 'storage/tmp/arquivosparaimportar/';
        // Tamanho máximo do arquivo (em Bytes)
        $_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
        // Array com as extensões permitidas
        $_UP['extensoes'] = array('csv');
        // Renomeia o arquivo? (Se true, o arquivo será salvo como .jpg e um nome único)
        $_UP['renomeia'] = false;
        // Array com os tipos de erros de upload do PHP
        $_UP['erros'][0] = 'Não houve erro';
        $_UP['erros'][1] = 'O arquivo no upload é maior do que o limite do PHP';
        $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
        $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
        $_UP['erros'][4] = 'Não foi feito o upload do arquivo';
        // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
        if ($_FILES['arquivo']['error'] != 0) {
          die("Não foi possível fazer o upload, erro:" . $_UP['erros'][$_FILES['arquivo']['error']]);
          exit; // Para a execução do script
        }
        // Caso script chegue a esse ponto, não houve erro com o upload e o PHP pode continuar
        // Faz a verificação da extensão do arquivo
//        $extensao = strtolower(end(explode('.', $_FILES['arquivo']['name'])));
//        if (array_search($extensao, $_UP['extensoes']) === false) {
//          echo "Por favor, envie arquivos com as seguinte extensão: csv";
//          exit;
//        }
        // Faz a verificação do tamanho do arquivo
        if ($_UP['tamanho'] < $_FILES['arquivo']['size']) {
          echo "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
          exit;
        }
        // O arquivo passou em todas as verificações, hora de tentar movê-lo para a pasta
        // Primeiro verifica se deve trocar o nome do arquivo
        if ($_UP['renomeia'] == true) {
          // Cria um nome baseado no UNIX TIMESTAMP atual e com extensão .jpg
          $nome_final = md5(time()).'.csv';
        } else {
          // Mantém o nome original do arquivo
          $nome_final = $_FILES['arquivo']['name'];
        }

        // Depois verifica se é possível mover o arquivo para a pasta escolhida
        if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $_UP['pasta'] . $nome_final)) {
//          Upload efetuado com sucesso, exibe uma mensagem e um link para o arquivo
//          echo "Upload efetuado com sucesso!";
//          echo '<a href="' . $_UP['pasta'] . $nome_final . '">Clique aqui para acessar o arquivo</a>';
//          
//            exit;
          $this->importar($_UP['pasta'] . $nome_final, $idSolicitacao, $idParceiro,$idUnidade,$dsObservacaoItem,$idCentroCusto, $dsParceiroSugerido);
          header('Location: /solicitacaocompras/nova_solicitacao/idSolicitacao/' . $idSolicitacao);
          
        } else {
          // Não foi possível fazer o upload, provavelmente a pasta está incorreta
          echo "Não foi possível enviar o arquivo, tente novamente";
        }    
        
    }
    
    private function importar($arquivo, $idSolicitacao, $idParceiro,$idUnidade,$dsObservacaoItem,$idCentroCusto, $dsParceiroSugerido) {
        global $PATH;

        $delimitador = ',';
        $cerca = '"';
        $f = fopen($arquivo, 'r');
        
        while (($data = fgetcsv($f, 0, $delimitador, $cerca)) !== FALSE) {
            $dados = array();
            $dados['dsProduto'] = $data[0];
            $dados['cdProduto'] = $data[1];
            $dados['qtProduto'] = $data[2];
            $this->lerdados_csv($dados, $idSolicitacao, $idParceiro,$idUnidade,$dsObservacaoItem,$idCentroCusto, $dsParceiroSugerido);  
        }     

        fclose($f);        
    }

    private function lerdados_csv($dados, $idSolicitacao, $idParceiro,$idUnidade,$dsObservacaoItem,$idCentroCusto, $dsParceiroSugerido) {

        $model = new insumoModel();
        $where = '';
        $dadosProduto = $model->getInsumo("a.cdInsumo = '" . $dados['cdProduto'] . "' or a.dsCodigoDoFabricante = '" . $dados['cdProduto'] . "'");
        if ($dadosProduto) {
            $data['idInsumo'] = $dadosProduto[0]['idInsumo'];
        } else {
            // cadastrar o produto
            $dadosdoproduto = array(
              'idInsumo' => null,
              'cdInsumo' => $dados['cdProduto'],
              'dsCodigoDoFabricante' => $dados['cdProduto'],
              'idGrupo' => 66,
              'idUnidade' => $idUnidade,
              'dtCadastro' => date('Y-m-d H:i:s'),
              'dsInsumo' => $dados['dsProduto'] . ' - ' . $dados['cdProduto']
            );
            $data['idInsumo'] = $model->setInsumo($dadosdoproduto);
        }
        
        $data['idSolicitacaoItem'] = null;
        $data['idSolicitacao'] = $idSolicitacao;
        $data['dsProduto'] = $dados['dsProduto'] . ' - ' . $dados['cdProduto'];
        $data['qtSolicitacao'] = $dados['qtProduto'];
        $data['idUnidade'] = $idUnidade;        
        $data['idParceiro'] = $idParceiro;
        $data['dsObservacao'] = $dsObservacaoItem;
        $data['dsParceiroSugerido'] = $dsParceiroSugerido;
        $data['dsLink'] =  null;
        $data['idSituacao'] = 1;
        $data['idItemDR'] =  null;
        $data['dsJustificativa'] = null;
        $data['idGrupoDR'] =  null;
        $data['idCentroCusto'] = $idCentroCusto;
        $data['stTipoIS'] = 0;

        $model = new solicitacaocomprasModel();
        $model->setSolicitacaoComprasItem($data);
    }    
}

?>