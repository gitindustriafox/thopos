<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class menuReuniao extends controller {

    public function index_action() {

        $this->template->run();
        if (!isset($_SESSION['user']['usuario'])) {
            header('Location: /login');            
        }
        
        $model = new reuniaoModel();
        $sql = "r.dtReuniao >= '" . date('Y-m-d H:i:s') . "' and r.idReuniao > 0 and r.stSituacao = 0 and (r.idUsuarioSolicitante = " . $_SESSION['user']['usuario'] . " or p.idUsuario = " . $_SESSION['user']['usuario'] . ")";        
        $reuniao_lista = $model->getReuniao($sql);
    //    print_a_die($reuniao_lista);
        $this->smarty->assign('reuniao_lista', $reuniao_lista);
        $this->smarty->display('reuniao/menuIndex.html');
    }

}

?>