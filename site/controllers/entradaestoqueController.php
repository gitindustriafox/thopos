<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */
// use system\Action;

class entradaestoque extends controller {

    private $cdNCM = null;
    
    public function index_action() {
        //Inicializa o Template
        $this->template->run();
        $this->novo_entradaestoque();
    }

//Funcao de Busca
    public function busca_entradaestoque() {
        //se nao existir o indice estou como padrao '';
        $texto = isset($_POST['buscadescricao']) ? $_POST['buscadescricao'] : '';
        //$texto = '';
        $model = new entradaestoqueModel();
        $sql = "stStatus <> 0 and upper(dsMovimento) like upper('%" . $texto . "%')"; //somente os nao excluidos
        $resultado = $model->getMovimento($sql);

        if (sizeof($resultado) > 0) {
            $this->smarty->assign('entradaestoque_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'entradaestoque');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('entradaestoque/lista.html');
        } else {
            $this->smarty->assign('entradaestoque_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'entradaestoque');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('entradaestoque/lista.html');
        }
    }

    //Funcao de Inserir
    public function novo_entradaestoque() {
        $sy = new system\System();
        $idMovimento = $sy->getParam('idMovimento');
        if ($idMovimento) {
            $_SESSION['movimento']['id'] = $idMovimento;
        }

        if (isset($_SESSION['movimento']['id'])) {
            if (!is_null($_SESSION['movimento']['id'])) {
                $idMovimento = $_SESSION['movimento']['id'];
            }
        }
        
        $model = new entradaestoqueModel();
        if (isset($idMovimento)) {
            if ((bool) $idMovimento) {
                $registro = $model->getMovimento('idMovimento=' . $idMovimento);  
                if ($registro) {
                    $registro = $registro[0];
                } else {
                    //Novo Registro
                    $registro = $model->estrutura_vazia();
                    $registro = $registro[0];
                }
            } else {
                //Novo Registro
                $registro = $model->estrutura_vazia();
                $registro = $registro[0];
            }
        } else {
            //Novo Registro
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        
        $modelTipoMovimento = new tipomovimentoModel();
        $lista_tipomovimento = array('' => 'SELECIONE');
        foreach ($modelTipoMovimento->getTipoMovimento("stDC='E'") as $value) {
            $lista_tipomovimento[$value['idTipoMovimento']] = $value['dsTipoMovimento'];
        }
//        $modelParceiro = new parceiroModel();
//        $lista_Parceiro = array('' => 'SELECIONE');
//        foreach ($modelParceiro->getParceiro() as $value) {
//            $lista_Parceiro[$value['idParceiro']] = $value['dsParceiro'];
//        }
//        $modelInsumo = new insumoModel();
//        $lista_insumo = array('' => 'SELECIONE');
//        foreach ($modelInsumo->getInsumo() as $value) {
//            $lista_insumo[$value['idInsumo']] = $value['cdInsumo'] . '-' . $value['dsInsumo'];
//        }
        $modelCentroCusto = new centrocustoModel();
        $lista_centrocusto = array('' => 'SELECIONE');
        foreach ($modelCentroCusto->getCentroCustoCombo() as $value) {
            $lista_centrocusto[$value['idCentroCusto']] = $value['codigocusto'];
        }
        $modelMaquina = new maquinaModel();
        $lista_maquina = array('' => 'SELECIONE');
        foreach ($modelMaquina->getMaquina() as $value) {
            $lista_maquina[$value['idMaquina']] = $value['dsMaquina'];
        }
        $modelOS = new osModel();
        $lista_OS = array('' => 'SELECIONE');
        foreach ($modelOS->getOS() as $value) {
            $lista_OS[$value['idOS']] = $value['nrOS'];
        }
        $motivolOS = new motivoModel();
        $lista_Motivo = array('' => 'SELECIONE');
        foreach ($motivolOS->getMotivo() as $value) {
            $lista_Motivo[$value['idMotivo']] = $value['dsMotivo'];
        }
        $modellocalestoque = new localestoqueModel();
        $lista_localestoque = array('' => 'SELECIONE');
        foreach ($modellocalestoque->getLocalEstoque() as $value) {
            $desanterior = $modellocalestoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
            if ($desanterior) {
                if ($value['idLocalEstoque'] == $value['idLocalEstoqueSuperior']) {
                    $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
                } else {
                    $lista_localestoque[$value['idLocalEstoque']] = $desanterior[0]['dsLocalEstoque'] . '-' . $value['dsLocalEstoque'];
                }
            } else {
                $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
            }
        }
        $colaborador = new colaboradorModel();
        $lista_colaborador = array('' => 'SELECIONE');
        foreach ($colaborador->getColaborador("upper(d.dsMaoObra) like upper('%motorista%')") as $value) {
            $lista_colaborador[$value['idColaborador']] = $value['dsColaborador'];
        }
        
        $movimentoitens = array();
        $valortotal = null;
        if($idMovimento) {
            $where = "a.idMovimento = " . $idMovimento . " and t.stDC = 'E'";
            $movimentoitens = $model->getMovimentoItens($where);
            
            $x=0;
            foreach($movimentoitens as $value) {
                $desanterior = $modellocalestoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
                if ($desanterior) {
                    if ($value['idLocalEstoque'] == $value['idLocalEstoqueSuperior']) {
                        $movimentoitens[$x]['localEstoque'] = $value['dsLocalEstoque'];
                    } else {
                        $movimentoitens[$x]['localEstoque'] = $desanterior[0]['dsLocalEstoque'] . '-' . $value['dsLocalEstoque'];
                    }
                } else {
                    $movimentoitens[$x]['localEstoque'] = $value['dsLocalEstoque'];
                }
                $x++;
            }
            
            $totalmovimento = $model->getTotalMovimentoItens($where);
            $valortotal = $totalmovimento[0]['totalmovimento'];
        }        
        $this->smarty->assign('registro', $registro);
            $this->smarty->assign('lista_colaborador', $lista_colaborador);
        $this->smarty->assign('lista_tipomovimento', $lista_tipomovimento);
//        $this->smarty->assign('lista_Parceiro', $lista_Parceiro);
//        $this->smarty->assign('lista_insumo', $lista_insumo);
        $this->smarty->assign('lista_centrocusto', $lista_centrocusto);
        $this->smarty->assign('lista_localestoque', $lista_localestoque);
        $this->smarty->assign('lista_maquina', $lista_maquina);
        $this->smarty->assign('lista_os', $lista_OS);
        $this->smarty->assign('lista_motivo', $lista_Motivo);
        $this->smarty->assign('movimentoitens', $movimentoitens);
        $this->smarty->assign('totalmovimento', $valortotal);
        $this->smarty->assign('title', 'Novo Movimento');
        $this->smarty->display('entradaestoque/form_novo.tpl');
    }
    // Gravar Padrao
    public function novaentrada() {
        $_SESSION['movimento']['id'] = null;
        header('Location: /entradaestoque/novo_entradaestoque');                
    }
    public function lerunidade() {
        $idInsumo = $_POST['idInsumo'];
        $idLocalEstoque = $_POST['idLocalEstoque'];
        $dsUnidade = null;
        $qtEstoque = null;
        $modelEstoque = new estoqueModel();
        
        $where = 'e.idInsumo = ' . $idInsumo . ' and e.idLocalEstoque = ' . $idLocalEstoque;
        $retorno = $modelEstoque->getEstoque($where);
        //var_dump($retorno);
        if ($retorno) {
            $dsUnidade = $retorno[0]['dsUnidade'];
            $qtEstoque = $retorno[0]['qtEstoque'];
        }
        $jsondata["dsUnidade"] = $dsUnidade;
        $jsondata["qtEstoque"] = $qtEstoque;
        $jsondata["ok"] = true;
        echo json_encode($jsondata);
    }
    
    public function lerncm() {
        $idInsumo = $_POST['idInsumo'];
        $model = new insumoModel();
        $jsondata = array();
        $where = 'a.idInsumo = ' . $idInsumo;
        $retorno = $model->getInsumo($where);
        if ($retorno) {
            $jsondata["cdNCM"] = $retorno[0]['cdNCM'];
            $jsondata["dsNCM"] = $retorno[0]['dsNCM'];
            $jsondata["idNCM"] = $retorno[0]['idNCM'];
        }
        
        $modellocalestoque = new localestoqueModel();
        $lista_localestoque = array('' => 'SELECIONE');
        if ($idInsumo) {
            foreach ($modellocalestoque->getLocalEstoqueProdutos('e.idInsumo = ' . $idInsumo) as $value) {
//            foreach ($modellocalestoque->getLocalEstoque() as $value) {
                if ($value['idLocalEstoque'] == $value['idLocalEstoqueSuperior']) {
                   $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
                } else {
                    $desanterior = $modellocalestoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
                    if ($desanterior) {
                       $lista_localestoque[$value['idLocalEstoque']] = $desanterior[0]['dsLocalEstoque'] . '-' . $value['dsLocalEstoque'];
                    } else {
                       $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
                    }
                }
            }
        }
        if (count($lista_localestoque)==1) {
            foreach ($modellocalestoque->getLocalEstoque() as $value) {
                if ($value['idLocalEstoque'] == $value['idLocalEstoqueSuperior']) {
                   $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
                } else {
                    $desanterior = $modellocalestoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
                    if ($desanterior) {
                       $lista_localestoque[$value['idLocalEstoque']] = $desanterior[0]['dsLocalEstoque'] . '-' . $value['dsLocalEstoque'];
                    } else {
                       $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
                    }
                }
            }
        }    
        
        $this->smarty->assign('lista_localestoque', $lista_localestoque);        
        $html = $this->smarty->fetch('saidaestoque/lista_localestoque.tpl');        
        $jsondata["html"] = $html;
        
        echo json_encode($jsondata);
    }
    
    public function lerpedido() {
        $idItem = null;
        $data = $this->trataPost($_POST);
        $datacabec = array('dtMovimento' => $data['dtMovimento']);
        $nrPedido = $_POST['nrPedido'];
        if (is_null($data['idTipoMovimento'])) {
            $modelTM = new tipomovimentoModel();
            // se for compras, atualizar dados do item
            $tm = $modelTM->getTipoMovimento("dsTipoMovimento like '%Compras%'");
            if($tm) {
                $data['idTipoMovimento'] = $tm[0]['idTipoMovimento'];
            } else {
                $data['idTipoMovimento'] = 1;
            }
        }

        if ($nrPedido) {
            $modelPedido = new pedidoModel();
            $where = 'nrPedido = ' . $nrPedido . ' and stSituacao = 1';
            $retorno = $modelPedido->getPedido($where);
            if ($retorno) {
                $idParceiro = $retorno[0]['idParceiro'];
                if (!$idParceiro) {
                    $idParceiro = null;
                }
                $data['idParceiro'] = $idParceiro;
                if (is_null($data['dsObservacao'])) {
                    $data['dsObservacao'] = $retorno[0]['dsObservacao'];
                }

                $model = new entradaestoqueModel();
                $idMovimento = $model->setMovimento($data);
                $where = 'nrPedido = ' . $nrPedido;
                $retorno = $modelPedido->getPedidoItens($where);
                if ($retorno) {
                    foreach ($retorno as $value) {
                        $dados = array();
                        $dados['idMovimentoItem'] = null;
                        $dados['idInsumo'] = ($value['idInsumo'] != '') ? $value['idInsumo'] : null;
                        $dados['qtMovimento'] = ($value['qtPedido'] != '') ? $value['qtPedido'] : null;
                        $dados['vlMovimento'] = ($value['vlPedido'] != '') ? $value['vlPedido'] : null;
                        $dados['idLocalEstoque'] = ($value['idLocalEstoque'] != '') ? $value['idLocalEstoque'] : null;
                        $dados['idMovimento'] = $idMovimento;
                        $dados['idTipoMovimento'] = $data['idTipoMovimento'];
                        $dados['dsObservacao'] = ($value['dsObservacao'] != '') ? $value['dsObservacao'] : null;
                        // dados do item    
                        $dataitemoutros = array();
                        $dataitemoutros['idTipoMovimento'] = $data['idTipoMovimento'];
                        $dataitemoutros['idParceiro'] = $data['idParceiro'];
                        $dataitemoutros['idCentroCusto'] = ($value['idCentroCusto'] != '') ? $value['idCentroCusto'] : null;
                        $dataitemoutros['idMaquina'] = ($value['idMaquina'] != '') ? $value['idMaquina'] : null;
                        $dataitemoutros['idOS'] = ($value['idOS'] != '') ? $value['idOS'] : null;
                        $dataitemoutros['idMotivo'] = ($value['idMotivo'] != '') ? $value['idMotivo'] : null;
                        $dataitemoutros['nrNota'] = $data['nrNota'];
                     //   var_dump($dataitemoutros);
                        
                        $retornado = $this->gravar_itemmesmo($datacabec, $dados,$dataitemoutros);
                        $idItem = $retornado['id']; 
                        $idOrigemInformacao = $retornado['idOrigemInformacao'];
                    }
                }
            }
        }
        $jsondata["html"] = "entradaestoque/form_novo.tpl";
        if ($idMovimento) {
            
            $modelPedido = new pedidoModel();
            $dados = array('stSituacao' => 2, 'idUsuarioBaixa' => $_SESSION['user']['usuario'], 'dtBaixa' => date('Y-m-d h:m:s'), 'nrNota' => $data['nrNota'], 'idOrigemInformacao' => $idOrigemInformacao);
            $where = 'nrPedido = ' . $nrPedido;
            $modelPedido->updPedido($dados, $where);
            
            
            $_SESSION['movimento']['id'] = $idMovimento;            
            $jsondata["idMovimento"] = $idMovimento;
            $jsondata["ok"] = true;
        } else {
            $_SESSION['movimento']['id'] = null;
            $jsondata["idMovimento"] = null;
            $jsondata["ok"] = false;
        }
        echo json_encode($jsondata);        
        
    }
    
    public function desabilitaid() {
        $_SESSION['movimento']['id'] = null;
        echo json_encode(true);
    }

    /**
     * 
     */
    public function gravar_movimento() {
        
        $model = new entradaestoqueModel();

        $data = $this->trataPost($_POST);
        if ($data['idMovimento']) {
            $model->updMovimento($data);
            $id = $data['idMovimento'];
        } else {
            $id = $model->setMovimento($data);
        }
        $_SESSION['movimento']['id'] = $id;
        $jsondata["idMovimento"] = $id;
        $jsondata["html"] = "entradaestoque/form_novo.tpl";
        $jsondata["ok"] = true;
        echo json_encode($jsondata);
    }

    public function gravar_item() {
        $datacabec = $this->trataPost($_POST);
        $data = $this->trataPostItem($_POST);
        // dados do item    
        $dataitemoutros = $this->trataPostItemOutros($_POST);        
        
        if ($data['idMovimentoItem']) {
            $model = new movimentoModel();
            $dados = array(
                'idMovimentoItem' => $data['idMovimentoItem'],
                'idCentroCusto' => $dataitemoutros['idCentroCusto']
            );
            $model->updMovimentoItem($dados);
            $id = $data['idMovimentoItem'];   
        } else {            
            $retornado = $this->gravar_itemmesmo($datacabec, $data, $dataitemoutros);
            $id = $retornado['id'];                        
        }

        $jsondata["html"] = "entradaestoque/lista.tpl";
        $jsondata["idMovimentoItem"] = $id;
        $jsondata["ok"] = true;
        echo json_encode($jsondata);        
    }    
    
    /**
     * 
     * @param type $data 'Dados do Cabeçalho do Movimento'
     * @param type $dataitemoutros  'Dados dos insumos do movimento'
     * @return type
     */
    private function gravar_itemmesmo($datacabec, $data, $dataitemoutros) {
        
        $idNCM = null;
        if ($this->cdNCM) {
            $modelNCM = new ncmModel();
            $dadosNCM = $modelNCM->getNCM("cdNCM = '" . $this->cdNCM . "'");
            if ($dadosNCM) {
                $idNCM = $dadosNCM[0]['idNCM'];
            }
        }
        
        $modelEstoque = new estoqueModel();
        
        // baixar pedido
        
//        var_dump($data);
       // var_dump($datacabec); die;
        
//        if ($datacabec['nrPedido'] <> '') {
//            // vamos ler o pedido
//            $where = 'i.idPedido = ' . $datacabec['nrPedido'] . ' and i.idInsumo = ' . $data['idInsumo'];
//            $modelPedido = new pedidoModel();
//            $retornoPedido = $modelPedido->getPedido($where);
//            if ($retornoPedido) {
//                $qtPedido = ($retornoPedido[0]['qtPedido'] + $retornoPedido[0]['qtEntregue']);
//                if ($qtPedido <> $data['qtMovimento']) {
//                    $situacao = 2;
//                } else {
//                    $situacao = 3;
//                }
//            }
//            $dadosBaixa = array(
//                'idSituacaoPedido ' => $situacao,
//                'qtEntregue ' => $data['qtMovimento'],
//                'vlEntregue ' => $data['vlMovimento'],
//                'nrNota ' => $datacabec['nrNota'],
//                'dtEntrega ' => $datacabec['dtMovimento']
//            );
//            
//            // baixa o pedido
//            
//            $where = 'idPedidoItem = ' . $retornoPedido[0]['idPedidoItem'];
//            $modelPedido->updPedidoItem($dadosBaixa,$where);
//            
//            $where = 'idPedido = ' . $retornoPedido[0]['idPedido'];
//            $dadosBaixa = array(
//                'idSituacaoPedido ' => $situacao
//            );
//            $modelPedido->updPedido($dadosBaixa,$where);
//            
//            // baixa solicitacao de compras
//            
//            $situacao = $situacao + 1;
//            $where = 'idPedido = ' . $datacabec['nrPedido'] . ' and idInsumo = ' . $data['idInsumo'] . ' and idPedidoItem = ' . $retornoPedido[0]['idPedidoItem'];
//            $dataSol=array(
//                'idSituacao ' => $situacao,
//                'qtEntregue ' => $data['qtMovimento'],
//                'vlEntregue ' => $data['vlMovimento'],
//                'nrNota ' => $datacabec['nrNota'],
//                'dtEntrega ' => $datacabec['dtMovimento']                
//            );
//            
//            $modelSolicitacao = new solicitacaocomprasModel();
//            $modelSolicitacao->updSolicitacaoComprasItemManut($dataSol, $where);            
//        }
        
        $where = 'e.idInsumo = ' . $data['idInsumo'] . ' and e.idLocalEstoque = ' . $data['idLocalEstoque'];
        $retorno = $modelEstoque->getEstoque($where);
        if ($retorno) {
            $idEstoque = $retorno[0]['idEstoque'];
            $qtdeatual = $retorno[0]['qtEstoque'] + $data['qtMovimento'];
            $valoratual = $retorno[0]['vlEstoque'] + $data['vlMovimento'];
            
            $dataE = array(
                'idEstoque' => $idEstoque,
                'qtEmSolicitacao' => 0,
                'stEmSolicitacao' => 0,
                'qtEstoque' => $qtdeatual,
                'vlEstoque' => $valoratual,
                'vlPME' => $valoratual / $qtdeatual
            );
            $modelEstoque->updEstoque($dataE);
        } else {
            $dataE = array(
                'idInsumo' => $data['idInsumo'],
                'idLocalEstoque' => $data['idLocalEstoque'],
                'qtEmSolicitacao' => 0,
                'stEmSolicitacao' => 0,
                'qtEstoque' => $data['qtMovimento'],
                'vlEstoque' => $data['vlMovimento'],
                'vlPME' => $data['vlMovimento'] / $data['qtMovimento']
            );
            $idEstoque = $modelEstoque->setEstoque($dataE);
        }
        
        $model = new entradaestoqueModel();
        $data['idCentroCusto'] = $dataitemoutros['idCentroCusto'];
        $data['idMaquina'] = $dataitemoutros['idMaquina'];
        $data['idOS'] = $dataitemoutros['idOS'];
        $data['idMotivo'] = $dataitemoutros['idMotivo'];
        $id = $model->setMovimentoItem($data);
        
        $modelInsumo = new insumoModel();
        $idOrigemInformacao = null;
        $mTeveCC = false;
        $mTeveOS = false;
        $mTeveMaquina = false;
        $modelTM = new tipomovimentoModel();
        // se for compras, atualizar dados do item
        $nome = $modelTM->getTipoMovimento("idTipoMovimento = " . $dataitemoutros['idTipoMovimento']);
        $dsTipoMovimento = $nome[0]['dsTipoMovimento'];
        if ($dsTipoMovimento == 'Compras') {
            $where = "idInsumo = " . $data['idInsumo'];
            
            $qtExistente = $modelInsumo->getInsumo($where)[0];
            if (!$qtExistente) {
                $qtNova = $data['qtMovimento'];
            } else {
                $qtNova = $qtExistente['qtEstoque'] + $data['qtMovimento'];
            }
            
            $datainsumo = array(
                'idParceiroUltimaCompra' => $dataitemoutros['idParceiro'],
                'idUltimoCentroCusto' => $dataitemoutros['idCentroCusto'],
                'idUltimaOS' => $dataitemoutros['idOS'],
                'idMaquina' => $dataitemoutros['idMaquina'],
                'dtUltimaCompra' => $datacabec['dtMovimento'],
                'nrNotaUltimaCompra' => $dataitemoutros['nrNota'],
                'vlUltimaCompra' => $data['vlMovimento'],
                'qtUltimaCompra' => $data['qtMovimento'],
                'idNCM' => $idNCM,
                'qtEstoque ' => $qtNova
            );
//            print_a_die($data);
            $model->updMovimentoItem($datainsumo, $where);
        }    
        // pegar a origem da informacao
        $modelOI = new origeminformacaoModel();
        $retorno = $modelOI->getOrigemInformacao("dsOrigemInformacao = '" . $dsTipoMovimento . "'");
        if ($retorno) {
            $idOrigemInformacao = $retorno[0]['idOrigemInformacao'];
        } else {
            // não tem origem da informacao, entao criar um e pegar o ID   
            $dadosorigem = array(
                'dsOrigemInformacao' => $dsTipoMovimento
            );
            $idOrigemInformacao = $modelOI->setOrigemInformacao($dadosorigem);
        }
        // sempre que tiver centro de custo, gravar um rateio e depois dar saida por aplicacao direta
        if ($dataitemoutros['idCentroCusto']) {
            $modelOI = new origeminformacaoModel();
            // gravar o rateio do valor
            $dadosaprarateio = array(
                'idRateio' => null,
                'dtRateio' => $datacabec['dtMovimento'],
                'idCentroCusto' => $dataitemoutros['idCentroCusto'],
                'idOrigemInformacao' => $idOrigemInformacao,
                'dsDocOrigem' => $dataitemoutros['nrNota'],
                'vlRateio' => $data['vlMovimento'],
                'stDC' => 'D'                    
            );
            $modelRateio = new rateioModel();
            $modelRateio->setRateio($dadosaprarateio);  

            // dar saida de estoque
            $mTeveCC = true;                    
        }
        if ($dataitemoutros['idMaquina']) {
            // dar saida de estoque
            $mTeveMaquina = true;                    
        }
        // gravar ocorrencia com a Ordem de Serviço
        if ($dataitemoutros['idOS']) {
            // dar saida de estoque
            $mTeveOS = true;   

            // pegar o tipo de ocorrencia
            $modelTO = new tipoocorrenciaModel();
            $retorno = $modelTO->getTipoOcorrencia("dsTipoOcorrencia = '" . $dsTipoMovimento . "'");
            if ($retorno) {
                $idTipoOcorrencia = $retorno[0]['idTipoOcorrencia'];
            } else {
                // não tem tipo de ocorrencia, entao criar um e pegar o ID   
                $dadostipoocorrencia = array(
                    'dsTipoOcorrencia' => $dsTipoMovimento
                );
                $idTipoOcorrencia = $modelTO->setTipoOcorrencia($dadostipoocorrencia);
            }                       
            $dadosOcorrencia = array(
                'idOcorrencia' => null,
                'dsOcorrencia' => 'Compra de ' . $data['qtMovimento'] . ' para uso ',
                'idOS' => $dataitemoutros['idOS'],
                'dtOcorrencia' => $datacabec['dtMovimento'],
                'idTipoOcorrencia' => $idTipoOcorrencia,
                'dtInicio' => $datacabec['dtMovimento'],
                'dtFim' => $datacabec['dtMovimento'],
                'idOrigemInformacao' => $idOrigemInformacao,
                'stStatus' => 1,
                'nrDocumento' => $dataitemoutros['nrNota'],
                'idMotivo' => $dataitemoutros['idCentroCusto']
            );
            $modelOcorrencias = new ocorrenciaModel();            
            $idOcorrencia = $modelOcorrencias->setOcorrencia($dadosOcorrencia);
            $dadosocorrenciainsumo = array(
                    'idOcorrenciasInsumo' => null,
                    'idOcorrencia' => $idOcorrencia,
                    'idOS' => $dataitemoutros['idOS'],
                    'idInsumo' => $data['idInsumo'],
                    'qtUtilizada' => $data['qtMovimento'],
                    'vlInsumo' => $data['vlMovimento'],
                    'stStatus' => 1
                    );
            $modelOcorrencias->setOcorrenciaInsumo($dadosocorrenciainsumo);
        }

        $datainsumo = array(
            'idUltimoMovimento' => $data['idMovimento'],
            'idInsumo' => $data['idInsumo']
        );
        $modelInsumo->updInsumo($datainsumo);
        
        // gravar saida do insumo caso tenha escolhido algum destes itens para aplicacao direta
//        if ($mTeveCC === true or $mTeveOS === true or $mTeveMaquina === true) {
//            
//            // pegar o tipo de movimento de saída
//            $retorno = $modelTM->getTipoMovimento("dsTipoMovimento = 'Aplicação Direta'");
//            if ($retorno) {
//                $idTipoMovimentoSaida = $retorno[0]['idTipoMovimento'];
//            } else {
//                // não tem tipo de MOVIMENTO, entao criar um e pegar o ID   
//                $dadostipomovimento = array(
//                    'dsTipoMovimento' => 'Aplicação Direta',
//                    'stDC' => 'S'
//                );
//                $idTipoMovimentoSaida = $modelTM->setTipoMovimento($dadostipomovimento);
//            }  
//            
//            // gravar o movimento de saída
//            
//            $data['idTipoMovimento'] = $idTipoMovimentoSaida;            
//            $id = $model->setMovimentoItem($data);    
//            
//            $where = "idInsumo = " . $data['idInsumo'];
//            $qtExistente = $modelInsumo->getInsumo($where)[0];
//            if (!$qtExistente) {
//                $qtNova = $data['qtMovimento'];
//            } else {
//                $qtNova = $qtExistente['qtEstoque'] - $data['qtMovimento'];
//            }
//            
//            $datainsumo = array(
//                'qtEstoque ' => $qtNova                
//            );
//            $model->updMovimentoItem($datainsumo, $where);            
            
//        }       
        
        $retornar = array(
          'id' => $id,  
          'idOrigemInformacao' => $idOrigemInformacao 
        );
        return  $retornar;
    }
    
    
    //Trata dados antes de Enviar para o Gravar
    private function trataPost($post) {
        $data = array();
        $data['idMovimento'] = ($post['idMovimento'] != '') ? $post['idMovimento'] : null;
        $data['idParceiro'] = ($post['idParceiro'] != '') ? $post['idParceiro'] : null;
        $data['idTipoMovimento'] = ($post['idTipoMovimento'] != '') ? $post['idTipoMovimento'] : null;
        $data['dsObservacao'] = ($post['dsObservacao'] != '') ? $post['dsObservacao'] : null;
        $data['dtMovimento'] = ($post['dtMovimento'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $_POST["dtMovimento"]))) : date('Y-m-d h:m:s');
        $data['nrNota'] = ($post['nrNota'] != '') ? $post['nrNota'] : null;
        $data['nrPedido'] = ($post['nrPedido'] != null) ? $post['nrPedido'] : null;
        $data['qtTotalNota'] = ($post['qtTotalNota'] != '') ? $post['qtTotalNota'] : null;
        $data['idColaborador'] = ($post['idColaborador'] != '') ? $post['idColaborador'] : null;
        $data['nrPlaca'] = ($post['nrPlaca'] != '') ? $post['nrPlaca'] : null;
        $data['nrLacre'] = ($post['nrLacre'] != '') ? $post['nrLacre'] : null;
        $data['nrNota2'] = ($post['nrNota2'] != '') ? $post['nrNota2'] : null;
        $data['qtTotalNota2'] = ($post['qtTotalNota2'] != '') ? $post['qtTotalNota2'] : null;
        $data['idUsuario'] = $_SESSION['user']['usuario'];
        return $data;
    }

    private function trataPostItem($post) {
 //      var_dump($post);
//        echo ' valor: ' . number_format(doubleval($post['vlMovimento']), 2, '.', ''); die;
        $data = array();
//        $data['idMovimentoItem'] = null;
        $data['idInsumo'] = ($post['idInsumo'] != '') ? $post['idInsumo'] : null;
        $data['idMovimentoItem'] = ($post['idMovimentoItem'] != '') ? $post['idMovimentoItem'] : null;
        $data['qtMovimento'] = ($post['qtMovimento'] != '') ? str_replace(",",".",str_replace(".","",$post['qtMovimento'])) : null;
        $data['vlMovimento'] = ($post['vlMovimento'] != '') ? str_replace(",",".",str_replace(".","",$post['vlMovimento'])) : null;
        $data['idLocalEstoque'] = ($post['idLocalEstoque'] != '') ? $post['idLocalEstoque'] : null;
        $data['idMovimento'] = ($post['idMovimento'] != '') ? $post['idMovimento'] : null;
        $data['idTipoMovimento'] = ($post['idTipoMovimento'] != '') ? $post['idTipoMovimento'] : null;
        $data['dsObservacao'] = ($post['dsObservacao'] != '') ? $post['dsObservacao'] : null;
        
        $this->cdNCM = ($post['cdNCM'] != '') ? $post['cdNCM'] : null;
        
        return $data;
    }

    private function trataPostItemOutros($post) {
        $dataoutros = array();
        $dataoutros['idTipoMovimento'] = ($post['idTipoMovimento'] != '') ? $post['idTipoMovimento'] : null;
        $dataoutros['idParceiro'] = ($post['idParceiro'] != '') ? $post['idParceiro'] : null;
        $dataoutros['idCentroCusto'] = ($post['idCentroCusto'] != '') ? $post['idCentroCusto'] : null;
        $dataoutros['idMaquina'] = ($post['idMaquina'] != '') ? $post['idMaquina'] : null;
        $dataoutros['idOS'] = ($post['idOS'] != '') ? $post['idOS'] : null;
        $dataoutros['idMotivo'] = ($post['idMotivo'] != '') ? $post['idMotivo'] : null;
        $dataoutros['nrNota'] = ($post['nrNota'] != '') ? $post['nrNota'] : null;
        return $dataoutros;
    }
    // Remove Padrao
    public function delmovimentoitem() {                
        $idMovimentoItem = $_POST['idMovimentoItem'];
        $model = new entradaestoqueModel();
        $where = 'idMovimentoItem = ' . $idMovimentoItem;             
        $model->delMovimentoItem($where);
        $jsondata["ok"] = true;
        echo json_encode($jsondata);        
    }

    public function delmovimento() {
        $sy = new system\System();
        
        $idMovimento = $sy->getParam('idMovimento');
        $model = new entradaestoqueModel();
        $where = 'idMovimento = ' . $idMovimento;
        $ok = $model->getTotalMovimentoItensE($where);
        if (!$ok) {
            $model->delMovimentoItem($where);
            $model->delMovimento($where);
            header('Location: /entradaestoque');        
        } else {
            header('Location: /entradaestoque/novo_entradaestoque/idMovimento/' . $idMovimento);        
        }
        
        return;
    }
    
    public function relatorioentradaestoque_pre() {
        $this->template->run();

        $this->smarty->assign('title', 'Pre Relatorio de Movimentos');
        $this->smarty->display('entradaestoque/relatorio_pre.html');
    }

    public function relatorioentradaestoque() {
        $this->template->run();

        $model = new entradaestoqueModel();
        $entradaestoque_lista = $model->getMovimento();
        //Passa a lista de registros
        $this->smarty->assign('entradaestoque_lista', $entradaestoque_lista);
        $this->smarty->assign('titulo_relatorio');
        //Chama o Smarty
        $this->smarty->assign('title', 'Relatorio de Movimentos');
        $this->smarty->display('entradaestoque/relatorio.html');
    }
    
    public function getParceiro() {
        $url = explode("=", $_SERVER['REQUEST_URI']);
        $key = str_replace('+', ' ', $url[1]);
        if (!empty($key)) {
          $busca = trim($key);
          $model = new parceiroModel();
          $where = "(UPPER(dsParceiro) like UPPER('%{$key}%') OR UPPER(cdCNPJ) like UPPER('%{$key}%'))";
          $retorno = $model->getParceiro($where);
          $return = array();
          if (count($retorno)) {
            $row = array();
            for ($i = 0; $i < count($retorno); $i++) {
              $cnpj = $retorno[$i]["cdCNPJ"];
              $row['value'] = strtoupper($retorno[$i]["dsParceiro"]) . '-' . $cnpj;
              $row["id"] = $retorno[$i]["idParceiro"];
              array_push($return, $row);
            }
          }
          echo json_encode($return);
        }
    }    
    
    public function getProduto() {
        $url = explode("=", $_SERVER['REQUEST_URI']);
        $key = str_replace('+', ' ', $url[1]);
        if (!empty($key)) {
          $busca = trim($key);
          $model = new insumoModel();
          $where = "(UPPER(a.dsInsumo) like UPPER('%{$key}%') OR UPPER(a.cdInsumo) like UPPER('%{$key}%'))";
          $retorno = $model->getInsumo($where);
          $return = array();
          if (count($retorno)) {
            $row = array();
            for ($i = 0; $i < count($retorno); $i++) {
              $cdInsump = $retorno[$i]["cdInsumo"];
              $row['value'] = strtoupper($retorno[$i]["dsInsumo"]) . '-' . $cdInsump;
              $row["id"] = $retorno[$i]["idInsumo"];
              array_push($return, $row);
            }
          }
          echo json_encode($return);
        }
    } 
    
//    public function getNCM() {
//        $url = explode("=", $_SERVER['REQUEST_URI']);
//        $key = $url[1];
//        if (!empty($key)) {
//          $busca = trim($key);
//          $model = new ncmModel();
//          $where = "(UPPER(dsNCM) like UPPER('%{$key}%') OR UPPER(cdNCM) like UPPER('%{$key}%'))";
//          $retorno = $model->getNCM($where,false);
//          $return = array();
//          if (count($retorno)) {
//            $row = array();
//            for ($i = 0; $i < count($retorno); $i++) {
//              $row['value'] = $retorno[$i]["cdNCM"] . '-' . $retorno[$i]["dsNCM"];
//              $row["id"] = $retorno[$i]["idNCM"];
//              array_push($return, $row);
//            }
//          }
//          echo json_encode($return);
//        }        
//    }            
    
}

?>