<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class grupoagenda extends controller {

    public function index_action() {
//die("chegou");
        //Inicializa o Template
        $this->template->run();

        $model = new grupoagendaModel();
        $grupoagenda_lista = $model->getGrupoAgenda();

        $this->smarty->assign('grupoagenda_lista', $grupoagenda_lista);
        $this->smarty->display('grupoagenda/lista.html');
    }

//Funcao de Busca
    public function busca_grupoagenda() {
        //se nao existir o indice estou como padrao '';
        $texto = isset($_POST['buscadescricao']) ? $_POST['buscadescricao'] : '';
        //$texto = '';
        $model = new grupoagendaModel();
        $sql = "upper(dsGrupoAgenda) like upper('%" . $texto . "%')"; //somente os nao excluidos
        $resultado = $model->getGrupoAgenda($sql);

        if (sizeof($resultado) > 0) {
            $this->smarty->assign('grupoagenda_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'grupoagenda');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('grupoagenda/lista.html');
        } else {
            $this->smarty->assign('grupoagenda_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'grupoagenda');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('grupoagenda/lista.html');
        }
    }

    //Funcao de Inserir
    public function novo_grupoagenda() {
        $sy = new system\System();

        $idGrupoAgenda = $sy->getParam('idGrupoAgenda');

        $model = new grupoagendaModel();

        if ($idGrupoAgenda > 0) {

            $registro = $model->getGrupoAgenda('idGrupoAgenda=' . $idGrupoAgenda);
            $registro = $registro[0]; //Passando GrupoAgenda
        } else {
            //Novo Registro
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        
        //Obter lista a de tipos fk
        $objLista = new grupoagendaModel();
        //criar uma lista
        $lista_tipos = $objLista->getGrupoAgenda('idGrupoAgenda <> 0');
        foreach ($lista_tipos as $value) {
            $lista_tipos_log[$value['idGrupoAgenda']] = $value['dsGrupoAgenda'];
        }
        //Passar a lista de Tipo
        $this->smarty->assign('lista_grupoagenda', $lista_tipos);
        //var_dump($lista_tipos_log);die;
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('title', 'Novo GrupoAgenda');
        $this->smarty->display('grupoagenda/form_novo.tpl');
    }

    // Gravar Padrao
    public function gravar_grupoagenda() {
        $model = new grupoagendaModel();

        $data = $this->trataPost($_POST);

        if ($data['idGrupoAgenda'] == NULL)
            $model->setgrupoagenda($data);
        else
            $model->updgrupoagenda($data); //update
        
        header('Location: /grupoagenda');        
        return;
    }

    //Trata dados antes de Enviar para o Gravar
    private function trataPost($post) {
        $data['idGrupoAgenda'] = ($post['idGrupoAgenda'] != '') ? $post['idGrupoAgenda'] : null;
        $data['dsGrupoAgenda'] = ($post['dsGrupoAgenda'] != '') ? $post['dsGrupoAgenda'] : null;
        return $data;
    }

    // Remove Padrao
    public function delgrupoagenda() {
        $sy = new system\System();
                
        $idGrupoAgenda = $sy->getParam('idGrupoAgenda');
        
        $grupoagenda = $idGrupoAgenda;
        
        if (!is_null($grupoagenda)) {    
            $model = new grupoagendaModel();
            $dados['idGrupoAgenda'] = $grupoagenda;             
            $model->delGrupoAgenda($dados);
        }

        header('Location: /grupoagenda');
    }

    public function relatoriogrupoagenda_pre() {
        $this->template->run();

        $this->smarty->assign('title', 'Pre Relatorio de Grupo de Agendas');
        $this->smarty->display('grupoagenda/relatorio_pre.html');
    }

    public function relatoriogrupoagenda() {
        $this->template->run();

        $model = new grupoagendaModel();
        $grupoagenda_lista = $model->getGrupoAgenda();
        //Passa a lista de registros
        $this->smarty->assign('grupoagenda_lista', $grupoagenda_lista);
        $this->smarty->assign('titulo_relatorio');
        //Chama o Smarty
        $this->smarty->assign('title', 'Relatorio de Grupo de Agendas');
        $this->smarty->display('grupoagenda/relatorio.html');
    }

}

?>