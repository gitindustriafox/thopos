<?php
namespace controllers\reuniao;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Mail as Mail;

/**
 * Description of enviaremail
 *
 * @author reis
 */
class EnviarEmail extends Action {
    //put your code here
    public function render() {

        $idParticipante = $_POST['idParticipante'];
        $idReuniao = $_POST['idReuniao'];
        $ok = 'E-mail enviado com sucesso';
        $model = new reuniaoModel();        
        $retorno = $model->getParticipantes('P.idParticipante = ' . $idParticipante);
        $participantes = $model->getParticipantes('P.idReuniao = ' . $retorno[0]['idReuniao']);
        
        $to = array();

        $emails = array();
        if ($retorno) {
            $emails = $retorno;
        }
        
        if ((bool)$emails) {
            foreach ($emails as $value) {
                $to[] = filter_var(strtolower($value['email']), FILTER_VALIDATE_EMAIL);
            }
        }

        array_filter($to);

        if (!empty($to)) {
            $title = "REUNIÃO";
            $this->smarty->assign('title', $title);
            $this->smarty->assign('usuariodestino', $retorno[0]['dsUsuario']);
            $this->smarty->assign('data_envio', date('Y-m-d H:i'));
            $this->smarty->assign('usuarioenvio', $_SESSION['user']['nome']);
            $this->smarty->assign('dtReuniao', $retorno[0]['dtReuniao']);
            $this->smarty->assign('dsAssunto', $retorno[0]['dsAssunto']);
            $this->smarty->assign('lista_itens',$participantes);
            $body = $this->smarty->fetch('reuniao/email/enviar_email.tpl');

            $nome_anexo = $retorno[0]['dsCaminhoAnexo'];

            $phpmail = new Mail();
            foreach ($to as $value) {
                $phpmail->Send(array('0' => array(
                    'email' => $value,
                    'nome' => $value)), 'TESTE', $body, array(), array('0' => array(
                    'path_anexo' => null,
                    'nome_anexo' => $nome_anexo)));
            }
            return true;
        }
        
        $jsondata = array(
            'ok' => $ok
        );
        echo json_encode($jsondata);        
    }    
}
