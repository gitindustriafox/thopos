<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */
// use controllers\reuniao\EnviarEmail;

class reuniao extends controller {
    private $pastaPedidos = PEDIDOS;
    private $opcao = null;
    private $voltarmenu = null;
    public function index_action() {
        //Inicializa o Template
        $this->template->run();
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }

        $model = new reuniaoModel();
        $sql = 'r.idReuniao > 0 and r.stSituacao = 0 and r.idUsuarioSolicitante = ' . $_SESSION['user']['usuario'];
        
        $reuniao_lista = $model->getReuniao($sql);
        
        $this->smarty->assign('lista_tipo', $this->carregaTipoReuniao());          
        $this->smarty->assign('lista_local', $this->carregaLocalReuniao());   

        $listaS = array('' => 'SELECIONE', '0' => 'AGENDADA', '1' => 'ADIADA', '2' => 'CANCELADA', '3' => 'ARQUIVADA', '4' => 'REALIZADA');
        $this->smarty->assign('opcao', $this->opcao);            
        $this->smarty->assign('lista_status', $listaS);            
                
        $this->smarty->assign('reuniao_lista', $reuniao_lista);
        $this->smarty->display('reuniao/lista.html');
    }

    public function ata() {
        //Inicializa o Template
        $this->template->run();
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $sy = new system\System();
        $this->opcao = $sy->getParam('opcao');

        $model = new reuniaoModel();
        $sql = 'r.idReuniao > 0 and r.stSituacao < 2 and r.idUsuarioSolicitante = ' . $_SESSION['user']['usuario'];
        $this->opcao = isset($_POST['opcao']) ? $_POST['opcao'] : '';
        
        $reuniao_lista = $model->getReuniao($sql);
        
        $this->smarty->assign('lista_tipo', $this->carregaTipoReuniao());          
        $this->smarty->assign('lista_local', $this->carregaLocalReuniao());   
        $listaS = array('' => 'SELECIONE', '0' => 'AGENDADA', '1' => 'ADIADA', '2' => 'CANCELADA', '3' => 'ARQUIVADA', '4' => 'REALIZADA');
        $this->smarty->assign('lista_status', $listaS);            

        $this->smarty->assign('opcao', $this->opcao);
        $this->smarty->assign('reuniao_lista', $reuniao_lista);
        $this->smarty->display('reuniao/ata.html');
    }
    
    public function recebeEmail() {

        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $url = explode("/", $_GET['url']);
        $idReuniao = $url[4];        
        $idParticipante = $url[6];        
        $status = $url[8];            
        $model = new reuniaoModel();        
        $dados = array('idReuniao' => $idReuniao, 'idParticipante' => $idParticipante, 'stRespostaEmail' => $status);        
        $model->updReuniaoParticipantes($dados);        
    }

    private function finaliza() {
        header('Location: /login');            
        die;            
    }
    
    public function minhastarefas() {
        //Inicializa o Template
        $this->template->run();
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }

        $model = new reuniaoModel();
        $sql = 'P.idReuniao > 0 and P.stStatusTarefa = 0 and P.idUsuario = ' . $_SESSION['user']['usuario'];
        $x=0;
        $resultado = $model->getParticipantesTarefa($sql, 'P.dtPrazo');
        foreach ($resultado as $value) {
            $temoutrasdatas = $model->getDatas('idTarefa = ' . $value['idTarefa']);
            if ($temoutrasdatas) {
                $resultado[$x]['temOutrasDatas'] = 1;
            } else {
                $resultado[$x]['temOutrasDatas'] = 0;
            }
            $x++;
        }
        
        $this->smarty->assign('lista_statusTarefa', array('' => 'SELECIONE', '0' => 'ABERTA', '1' => 'CONCLUIDA'));            
        $this->smarty->assign('tarefas_lista', $resultado);
        //Chama o Smarty
        
        $this->smarty->assign('title', 'reuniao');
        $this->smarty->assign('busca', null);
        $this->smarty->display('reuniao/minhastarefas.html');
    }
    public function gestaotarefas() {
        //Inicializa o Template
        $this->template->run();

        $model = new reuniaoModel();
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $sql = 'P.idReuniao > 0 and r.idUsuarioSolicitante = ' . $_SESSION['user']['usuario'];
        
        $resultado = $model->getParticipantesTarefa($sql, 'ust.dsUsuario, P.dtPrazo');
        $x=0;
        foreach ($resultado as $value) {
            $temoutrasdatas = $model->getDatas('idTarefa = ' . $value['idTarefa']);
            if ($temoutrasdatas) {
                $resultado[$x]['temOutrasDatas'] = 1;
            } else {
                $resultado[$x]['temOutrasDatas'] = 0;
            }
            $x++;
        }        
        $_SESSION['sql']['gestaotarefas'] = $sql;
        $sql = $sql . ' and P.stStatusTarefa = 0 ';
        
        //Chama o Smarty
        $this->smarty->assign('lista_statusTarefa', array('' => 'SELECIONE', '0' => 'ABERTA', '1' => 'CONCLUIDA'));            
        $this->smarty->assign('lista_usuarioTarefa', $this->carregarusuariosTarefa(null));  
        $this->smarty->assign('tarefas_lista', $resultado);
        $this->smarty->assign('title', 'reuniao');
        
        $this->smarty->assign('busca', null);
        $this->smarty->display('reuniao/gestaotarefas.html');
    }
//Funcao de Busca
    public function busca_reuniao() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        //se nao existir o indice estou como paao '';
        $dsAssunto = isset($_POST['dsAssunto']) ? $_POST['dsAssunto'] : '';
        $dtInicio = isset($_POST['dtInicio']) ? $_POST['dtInicio'] : '';
        $dtFim = isset($_POST['dtFim']) ? $_POST['dtFim'] : '';
        $idTipo = isset($_POST['idTipo']) ? $_POST['idTipo'] : '';
        $idLocal = isset($_POST['idLocal']) ? $_POST['idLocal'] : '';
        $idReuniao = isset($_POST['idReuniao']) ? $_POST['idReuniao'] : '';
        $stStatus = isset($_POST['stStatus']) ? $_POST['stStatus'] : '';
        $this->opcao = isset($_POST['opcao']) ? $_POST['opcao'] : '';
        $busca = array();
        $sql = 'r.idReuniao > 0 and r.stSituacao < 3 and r.idUsuarioSolicitante = ' . $_SESSION['user']['usuario'];
        $registro = null;
        if ($idTipo) {
            $sql = $sql . " and r.idTipoReuniao = " . $idTipo;
            $registro['idTipoReuniao'] = $idTipo;
        }
        if ($idLocal) {
            $sql = $sql . " and r.idLocalReuniao = " . $idLocal;
            $registro['idLocalReuniao'] = $idLocal;
        }
        if ($idReuniao) {
            $sql = $sql . " and r.idReuniao = " . $idReuniao;
            $busca['idReuniao'] = $idReuniao;
        }
        if ($dsAssunto) {
            $sql = $sql . " and upper(r.dsAssunto) like upper('%" . $dsAssunto . "%')";
            $busca['dsAssunto'] = $dsAssunto;
        }
        if ($stStatus) {
            $sql = $sql . " and r.stSituacao = " . $stStatus;
            $registro['stStatus'] = $stStatus;
        }
        
        $dtInicio = ($dtInicio != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $dtInicio))) : '';
        $dtFim = ($dtFim != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $dtFim))) : '';
        if ($dtInicio && $dtFim) {
            $sql = $sql . " and r.dtReuniao >= '" . $dtInicio . "' and r.dtReuniao <= '" . $dtFim . "'";  
//            $sql = $sql . " and r.dtReuniao >= '" . $dtInicio . " 00:00:00' and r.dtReuniao <= '" . $dtFim . " 23:59:59'";  
        }
        $busca['dtInicio'] = $dtInicio;
        $busca['dtFim'] = $dtFim;

        $model = new reuniaoModel();
        $resultado = $model->getReuniao($sql);

        $this->smarty->assign('lista_tipo', $this->carregaTipoReuniao());          
        $this->smarty->assign('lista_local', $this->carregaLocalReuniao());   

        $listaS = array('' => 'SELECIONE', '0' => 'AGENDADA', '1' => 'ADIADA', '2' => 'CANCELADA', '3' => 'ARQUIVADA', '4' => 'REALIZADA');
        $this->smarty->assign('lista_status', $listaS);            
        $this->smarty->assign('opcao', $this->opcao);            
                
        if (sizeof($resultado) > 0) {
            $this->smarty->assign('reuniao_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'reuniao');
            $this->smarty->assign('busca', $busca);
            $this->smarty->assign('registro', $registro);
            $this->smarty->display('reuniao/lista.html');
        } else {
            $this->smarty->assign('reuniao_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'reuniao');
            $this->smarty->assign('busca', $busca);
            $this->smarty->assign('registro', $registro);
            $this->smarty->display('reuniao/lista.html');
        }
    }

    public function busca_ata_reuniao() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        //se nao existir o indice estou como paao '';
        $dsAssunto = isset($_POST['dsAssunto']) ? $_POST['dsAssunto'] : '';
        $dtInicio = isset($_POST['dtInicio']) ? $_POST['dtInicio'] : '';
        $dtFim = isset($_POST['dtFim']) ? $_POST['dtFim'] : '';
        $idTipo = isset($_POST['idTipo']) ? $_POST['idTipo'] : '';
        $idLocal = isset($_POST['idLocal']) ? $_POST['idLocal'] : '';
        $idReuniao = isset($_POST['idReuniao']) ? $_POST['idReuniao'] : '';
        $stStatus = isset($_POST['stStatus']) ? $_POST['stStatus'] : '';
        $busca = array();
        $sql = 'r.idReuniao > 0 and r.idUsuarioSolicitante = ' . $_SESSION['user']['usuario'];
        $registro = null;
        if ($idTipo) {
            $sql = $sql . " and r.idTipoReuniao = " . $idTipo;
            $registro['idTipoReuniao'] = $idTipo;
        }
        if ($idLocal) {
            $sql = $sql . " and r.idLocalReuniao = " . $idLocal;
            $registro['idLocalReuniao'] = $idLocal;
        }        
        if ($idReuniao) {
            $sql = $sql . " and r.idReuniao = " . $idReuniao;
            $busca['idReuniao'] = $idReuniao;
        }
        if ($dsAssunto) {
            $sql = $sql . " and upper(r.dsAssunto) like upper('%" . $dsAssunto . "%')";
            $busca['dsAssunto'] = $dsAssunto;
        }
        if ($stStatus) {
            $sql = $sql . " and r.stSituacao = " . $stStatus;
            $registro['stStatus'] = $stStatus;
        } else {
        if ($stStatus == 0) {
            $sql = $sql . " and r.stSituacao = 0";
            $registro['stStatus'] = $stStatus;
        }}
        
        $dtInicio = ($dtInicio != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $dtInicio))) : '';
        $dtFim = ($dtFim != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $dtFim))) : '';
        if ($dtInicio && $dtFim) {
            $sql = $sql . " and r.dtReuniao >= '" . $dtInicio . "' and r.dtReuniao <= '" . $dtFim . "'";  
//            $sql = $sql . " and r.dtReuniao >= '" . $dtInicio . " 00:00:00' and r.dtReuniao <= '" . $dtFim . " 23:59:59'";  
        }
        $busca['dtInicio'] = $dtInicio;
        $busca['dtFim'] = $dtFim;

        $model = new reuniaoModel();
        $resultado = $model->getReuniao($sql);

        $this->smarty->assign('lista_tipo', $this->carregaTipoReuniao());          
        $this->smarty->assign('lista_local', $this->carregaLocalReuniao());   
        
        $listaS = array('' => 'SELECIONE', '0' => 'AGENDADA', '1' => 'ADIADA', '2' => 'CANCELADA', '3' => 'ARQUIVADA', '4' => 'REALIZADA');
        $this->smarty->assign('lista_status', $listaS);            
        
        if (sizeof($resultado) > 0) {
            $this->smarty->assign('reuniao_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'reuniao');
            $this->smarty->assign('busca', $busca);
            $this->smarty->assign('registro', $registro);
            $this->smarty->display('reuniao/ata.html');
        } else {
            $this->smarty->assign('reuniao_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'reuniao');
            $this->smarty->assign('busca', $busca);
            $this->smarty->assign('registro', $registro);
            $this->smarty->display('reuniao/ata.html');
        }
    }
    public function busca_tarefas() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        //se nao existir o indice estou como paao '';
        $dtInicio = isset($_POST['dtInicio']) ? $_POST['dtInicio'] : '';
        $dtFim = isset($_POST['dtFim']) ? $_POST['dtFim'] : '';
        $idTarefa = isset($_POST['idTarefa']) ? $_POST['idTarefa'] : '';
        $stStatusTarefa = isset($_POST['stStatusTarefa']) ? $_POST['stStatusTarefa'] : '';
        $busca = array();
        $sql = 'P.idReuniao > 0 and P.idUsuario = ' . $_SESSION['user']['usuario'];
        $registro = null;
        if ($idTarefa) {
            $sql = $sql . " and P.idTarefa = " . $idTarefa;
            $busca['idTarefa'] = $idTarefa;
        }
        $dtInicio = ($dtInicio != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $dtInicio))) : '';
        $dtFim = ($dtFim != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $dtFim))) : '';
        $busca['dtInicio'] = $dtInicio;
        $busca['dtFim'] = $dtFim;

        if ($dtInicio && $dtFim) {
            $sql = $sql . " and P.dtPrazo >= '" . $dtInicio . "' and P.dtPrazo <= '" . $dtFim . "'";  
        }
        
        $_SESSION['sql']['gestaotarefas'] = $sql;
        
        if ($stStatusTarefa <> '') {
            $sql = $sql . " and P.stStatusTarefa = " . $stStatusTarefa;
            $busca['stStatusTarefa'] = $stStatusTarefa;
        }

        //Inicializa o Template
        $model = new reuniaoModel();
        $x=0;
        $resultado = $model->getParticipantesTarefa($sql, 'P.dtPrazo');
        foreach ($resultado as $value) {
            $temoutrasdatas = $model->getDatas('idTarefa = ' . $value['idTarefa']);
            if ($temoutrasdatas) {
                $resultado[$x]['temOutrasDatas'] = 1;
            } else {
                $resultado[$x]['temOutrasDatas'] = 0;
            }
            $x++;
        }
        
        $this->smarty->assign('lista_statusTarefa', array('' => 'SELECIONE', '0' => 'ABERTA', '1' => 'CONCLUIDA'));            
        $this->smarty->assign('tarefas_lista', $resultado);
        
        $this->smarty->assign('title', 'reuniao');
        $this->smarty->assign('busca', $busca);
        $this->smarty->display('reuniao/minhastarefas.html');            
    }
    
    public function busca_gestao_tarefas() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        //se nao existir o indice estou como paao '';
        $dtInicio = isset($_POST['dtInicio']) ? $_POST['dtInicio'] : '';
        $dtFim = isset($_POST['dtFim']) ? $_POST['dtFim'] : '';
        $idTarefa = isset($_POST['idTarefa']) ? $_POST['idTarefa'] : '';
        $dsPauta = isset($_POST['dsPauta']) ? $_POST['dsPauta'] : '';
        $idUsuario = isset($_POST['idUsuarioTarefa']) ? $_POST['idUsuarioTarefa'] : '';
        $stStatusTarefa = isset($_POST['stStatusTarefa']) ? $_POST['stStatusTarefa'] : '';
        $busca = array();
        $sql = 'P.idTarefa > 0 and r.idUsuarioSolicitante = ' . $_SESSION['user']['usuario'];
        $registro = null;
        if ($idTarefa) {
            $sql = $sql . " and P.idTarefa = " . $idTarefa;
            $busca['idTarefa'] = $idTarefa;
        }
        $this->smarty->assign('lista_usuarioTarefa', $this->carregarusuariosTarefa(null));  
        if ($idUsuario) {
            $sql = $sql . " and P.idUsuario = " . $idUsuario;
            $busca['idUsuarioTarefa'] = $idUsuario;
        }
        if ($stStatusTarefa <> '') {
            $sql = $sql . " and P.stStatusTarefa = " . $stStatusTarefa;
            $busca['stStatusTarefa'] = $stStatusTarefa;
        }
        if ($dsPauta <> '') {
            $sql = $sql . " and upper(P.dsTarefa) like '%" . strtoupper($dsPauta) . "%'";
            $busca['dsPauta'] = $dsPauta;
        }
        
        $dtInicio = ($dtInicio != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $dtInicio))) : '';
        $dtFim = ($dtFim != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $dtFim))) : '';
        if ($dtInicio && $dtFim) {
            $sql = $sql . " and P.dtPrazo >= '" . $dtInicio . "' and P.dtPrazo <= '" . $dtFim . "'";  
        }
        $busca['dtInicio'] = $dtInicio;
        $busca['dtFim'] = $dtFim;

        $model = new reuniaoModel();
        $resultado = $model->getParticipantesTarefa($sql, 'ust.dsUsuario, P.dtPrazo');
        $x=0;
        foreach ($resultado as $value) {
            $temoutrasdatas = $model->getDatas('idTarefa = ' . $value['idTarefa']);
            if ($temoutrasdatas) {
                $resultado[$x]['temOutrasDatas'] = 1;
            } else {
                $resultado[$x]['temOutrasDatas'] = 0;
            }
            $x++;
        }
        
        $this->smarty->assign('lista_statusTarefa', array('' => 'SELECIONE', '0' => 'ABERTA', '1' => 'CONCLUIDA'));            
        $_SESSION['sql']['gestaotarefas'] = $sql;
        
        if (sizeof($resultado) > 0) {
            $this->smarty->assign('tarefas_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'reuniao');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('reuniao/gestaotarefas.html');
        } else {
            $this->smarty->assign('tarefas_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'tarefas');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('reuniao/gestaotarefas.html');
        }
    }
    //Funcao de Inserir
    public function novo_reuniao() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $sy = new system\System();
        $this->opcao = $sy->getParam('opcao');
        $voltarMenu = false;
        $idReuniao = $sy->getParam('idReuniao');
        if ($idReuniao == 999999) {
            $idReuniao = null;
            $voltarMenu = true;
        }
        
        $model = new reuniaoModel();

        if ($idReuniao > 0) {
            $registro = $model->getReuniao('r.idReuniao=' . $idReuniao);
            $registro = $registro[0]; //Passando Reuniao
            $lista = $model->getParticipantes('P.idReuniao = ' . $idReuniao);
        } else {
            //Novo Registro
            $lista = null;
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        $this->smarty->assign('lista_tipo', $this->carregaTipoReuniao());          
        $this->smarty->assign('voltarmenu', $voltarMenu);          

        $this->smarty->assign('lista_usuario', $this->carregarusuarios($idReuniao));  
        $this->smarty->assign('lista_itens',$lista);
        
        $this->smarty->assign('opcao', $this->opcao);
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('title', 'Novo Reuniao');
        $this->smarty->display('reuniao/form_novo.tpl');
    }
    
    //Funcao de participantes
    public function participantes() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $sy = new system\System();
        $voltarMenu = false;
        $idReuniao = $sy->getParam('idReuniao');
        $this->opcao = $sy->getParam('opcao');
        if ($idReuniao == 999999) {
            $idReuniao = null;
            $voltarMenu = true;
        }
        
        $model = new reuniaoModel();

        if ($idReuniao > 0) {

            $registro = $model->getReuniao('r.idReuniao=' . $idReuniao);
            $registro = $registro[0]; //Passando Reuniao
            $lista = $model->getParticipantes('P.idReuniao = ' . $idReuniao);
        } else {
            //Novo Registro
            $lista = null;
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        $this->smarty->assign('lista_tipo', $this->carregaTipoReuniao());          
        $this->smarty->assign('voltarmenu', $voltarMenu);          
    //    $this->smarty->assign('lista_local', $this->carregaLocalReuniao());   

        $this->smarty->assign('lista_usuario', $this->carregarusuarios($idReuniao));  
        $this->smarty->assign('lista_itens',$lista);
        
        $this->smarty->assign('opcao', $this->opcao);
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('title', 'Novo Reuniao');
        $this->smarty->display('reuniao/form_novo.tpl');
    }

    public function acao() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $sy = new system\System();
        $this->opcao = $sy->getParam('opcao');
        $this->index_action();        
    }
    
    public function continua_reuniao() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $sy = new system\System();
        $idReuniao = $sy->getParam('idReuniao');
        $this->opcao = $sy->getParam('opcao');
        $this->voltarmenu = $sy->getParam('voltarmenu');
        
        $model = new reuniaoModel();
        $registro = $model->getReuniao('r.idReuniao=' . $idReuniao);
        
        if ($registro) {
            $dados = array(
                'idReuniao' => null,
                'idTipoReuniao' => $registro[0]['idTipoReuniao'],
                'dsPauta' => $registro[0]['dsPauta'],
                'dsAssunto' => $registro[0]['dsAssunto'],
                'idUsuarioSolicitante' => $registro[0]['idUsuarioSolicitante'],
                'idReuniaoPai' => $idReuniao,
                'stSituacao' => 0,
                'idLocalReuniao' => $registro[0]['idLocalReuniao'],
                'dsTempoDuracao' => $registro[0]['dsTempoDuracao'],
                'idReuniaoAnterior' => $idReuniao
            );
            $idNovaReuniao = $model->setReuniao($dados);
            
            $participantes = $model->getParticipantes('P.idReuniao = ' . $idReuniao);
            foreach($participantes as $value) {
                $dados = array(
                    'idParticipante' => null,
                    'idReuniao' => $idNovaReuniao,
                    'idUsuario' => $value['idUsuario'],
                    'dsAssunto' => $value['dsAssunto'],
                    'idParceiro' => $value['idParceiro'],
                    'dsCaminhoAnexo' => $value['dsCaminhoAnexo'],
                    'stSituacao' => 0,
                    'dsEmail' => $value['dsEmail'],
                    'dsNome' => $value['dsNome'],
                    'dsCelular' => $value['dsCelular']
                );
                $model->setParticipantes($dados);
            }
        }
        
        header('Location: /reuniao/novo_ata/idReuniao/' . $idNovaReuniao . '/opcao/' . $this->opcao . '/voltarmenu/' . $this->voltarmenu);                        
    }

    public function novo_ata() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $sy = new system\System();
        $idReuniao = $sy->getParam('idReuniao');
        $modelT = new reuniaoModel();
        $this->opcao = $sy->getParam('opcao');
        $this->voltarmenu = $sy->getParam('voltarmenu');

        if ($idReuniao == 999999) {
            // reuniao nao agendada            
            $idReuniao = $this->naoAgendada();            
        }
        
        $registro = $modelT->getSoReuniao('r.idReuniao=' . $idReuniao);
        $registro = $registro[0]; //Passando Reuniao
        if ($registro) {
            $ramificada = array();
            $ramificada[0]['idReuniao'] = $registro['idReuniao'];
            $ramificada[0]['dtReuniao'] = $registro['dtReuniao'];
            $ramificada[0]['idReuniaoPai'] = $registro['idReuniaoPai'];
            $ramificada[0]['dsLocalReuniao'] = $registro['dsLocalReuniao'];
            $ramificada[0]['dsPauta'] = $registro['dsAssunto'];
            $this->smarty->assign('reuniao_listaRamificada', $this->verRamificacao($registro['idReuniao'], $registro['idReuniaoPai'],$ramificada));
        }

        if ($registro['stSituacao'] == '4') {
            $this->smarty->assign('statusreuniao','REUNIÃO REALIZADA');
        } else {
        if ($registro['stSituacao'] == '3') {
            $this->smarty->assign('statusreuniao','REUNIÃO ARQUIVADA');
        } else {
        if ($registro['stSituacao'] == '2') {
            $this->smarty->assign('statusreuniao','REUNIÃO CANCELADA');
        } else {
            $this->smarty->assign('statusreuniao','REUNIÃO EM ANDAMENTO');
        }}}
        
        $lista = $modelT->getParticipantes('P.idReuniao = ' . $idReuniao);
        $this->smarty->assign('lista_tipo', $this->carregaTipoReuniao());          
        
        $this->smarty->assign('lista_usuario', $this->carregarusuarios($idReuniao));  
        $this->smarty->assign('lista_usuarioAta', $this->carregarusuariosAta());  
        $this->smarty->assign('lista_usuariotarefa', $this->carregarusuariosTarefa($idReuniao));  
        
        $this->smarty->assign('lista_itens',$lista);
        $listaUT = $modelT->getParticipantesTarefa('P.idReuniao = ' . $idReuniao);
        $this->smarty->assign('lista_itensTarefa',$listaUT);
        
        $this->smarty->assign('opcao', $this->opcao);
        $this->smarty->assign('voltarmenu', $this->voltarmenu);
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('title', 'Ata Reuniao');
        $this->smarty->display('reuniao/form_ata.tpl');
    }
    
    private function verRamificacao($idReuniao, $idReuniaoPai,$ramificada) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $x = 1;
        $modelT = new reuniaoModel();
        if ($idReuniaoPai) {
            for ($y=1;$y++;$y<50) {
                $retorno = $modelT->getSoReuniao('r.idReuniao = ' . $idReuniaoPai);
              //  var_dump($retorno);
                if ($retorno) {
                    foreach($retorno as $value) {
                        $ramificada[$x]['idReuniao'] = $value['idReuniao'];
                        $ramificada[$x]['dtReuniao'] = $value['dtReuniao'];
                        $ramificada[$x]['idReuniaoPai'] = $value['idReuniaoPai'];
                        $ramificada[$x]['dsLocalReuniao'] = $value['dsLocalReuniao'];
                        $ramificada[$x]['dsPauta'] = $value['dsAssunto'];
                        $idReuniaoPai = $value['idReuniaoPai'];
                        $x++;
                    } 
                    if (!$idReuniaoPai) {
                        break;
                    }                    
                } else {
                    break;
                }
            }
        }    
        //die;
        // VER SE TEM PARA FRENTE
        
        for ($y=1;$y++;$y<50) {
            $retorno = $modelT->getSoReuniao('r.idReuniaoPai = ' . $idReuniao);
         //   var_dump($retorno); die;
            if ($retorno) {
                foreach($retorno as $value) {
                    $ramificada[$x]['idReuniao'] = $value['idReuniao'];
                    $ramificada[$x]['dtReuniao'] = $value['dtReuniao'];
                    $ramificada[$x]['idReuniaoPai'] = $value['idReuniaoPai'];
                    $ramificada[$x]['dsLocalReuniao'] = $value['dsLocalReuniao'];
                    $ramificada[$x]['dsPauta'] = $value['dsAssunto'];
                    $idReuniao = $value['idReuniao'];
                    $x++;
                } 
                if (!$idReuniao) {
                    break;
                }                    
            } else {
                break;
            }
        }
        
        foreach ($ramificada as $user) {
            $ids[] = $user['idReuniao'];
        }
        array_multisort($ids, SORT_ASC, $ramificada);
        
        return $ramificada;        
    }
    
    public function grafico() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $sql = $_SESSION['sql']['gestaotarefas'];
        $x=0;
        $model = new reuniaoModel();
        $registro = $model->getParticipantesTarefaG($sql);
        
        $x = 0;
        foreach($registro as $value) {
            $sql = $_SESSION['sql']['gestaotarefas'] . " and P.stStatusTarefa = 0 and u.dsUsuario = '" . $value['Usuario'] . "'";
            $valores = $model->getParticipantesTarefaGStatus($sql);
            if ($valores) {
                $registro[$x]['abertas'] = $valores[0]['totalstatus'];
            } else {
                $registro[$x]['abertas'] = 0;
            }
            $sql = $_SESSION['sql']['gestaotarefas'] . " and P.stStatusTarefa = 1 and u.dsUsuario = '" . $value['Usuario'] . "'";
            $valores = $model->getParticipantesTarefaGStatus($sql);
            if ($valores) {
                $registro[$x]['concluidas'] = $valores[0]['totalstatus'];
            } else {
                $registro[$x]['concluidas'] = 0;
            }
            $x++;
        }
        
        $retorno = array();
        foreach($registro as $value) {
            $retorno[$value['Usuario']]['tarefas']  = $value['tarefas'];
            $retorno[$value['Usuario']]['abertas']  = $value['abertas'];
            $retorno[$value['Usuario']]['concluidas']  = $value['concluidas'];            
        }
        
//        print_a($retorno); die;
        
        $retornogeral = array(
            'tarefas' => $retorno,            
        );

        echo json_encode($retornogeral);
        
    }
    
    public function montarResumo() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $sql = $_SESSION['sql']['gestaotarefas'];
        $x=0;
        $model = new reuniaoModel();
        $registro = $model->getParticipantesTarefaG($sql);
        
        $x = 0;
        foreach($registro as $value) {
            $sql = $_SESSION['sql']['gestaotarefas'] . " and P.stStatusTarefa = 0 and u.dsUsuario = '" . $value['Usuario'] . "'";
            $valores = $model->getParticipantesTarefaGStatus($sql);
            if ($valores) {
                $registro[$x]['abertas'] = $valores[0]['totalstatus'];
            } else {
                $registro[$x]['abertas'] = 0;
            }
            $sql = $_SESSION['sql']['gestaotarefas'] . " and P.stStatusTarefa = 1 and u.dsUsuario = '" . $value['Usuario'] . "'";
            $valores = $model->getParticipantesTarefaGStatus($sql);
            if ($valores) {
                $registro[$x]['concluidas'] = $valores[0]['totalstatus'];
            } else {
                $registro[$x]['concluidas'] = 0;
            }
            $x++;
        }
        $this->smarty->assign('resumo_lista', $registro);
        $html = $this->smarty->fetch("reuniao/modalResumo/thumbnail1.tpl");        
        var_dump($html); die;
        
        echo json_encode(array('html' => $html,'ok' => 'ok'));
    }
    
    public function naoAgendada() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $model = new reuniaoModel();
        $dados = array(
            'idReuniao' => null,
            'idTipoReuniao' => 3,
            'idLocalReuniao' => 3,
            'dtReuniao' => date('Y-m-d H:i:s'),
            'dtRealizacao' => date('Y-m-d H:i:s'),
            'idUsuarioSolicitante' => $_SESSION['user']['usuario'],
            'dsAssunto' => null
            );
        
        $id =  $model->setReuniao($dados);
        header('Location: /reuniao/novo_ata/idReuniao/' . $id . '/opcao/iniciar/voltarmenu/1');        
    }
    
    public function adiar_reuniao() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }

        $sy = new system\System();

        $idReuniao = $sy->getParam('idReuniao');
        $this->opcao = $sy->getParam('opcao');

        $model = new reuniaoModel();

        if ($idReuniao > 0) {

            $registro = $model->getReuniao('r.idReuniao=' . $idReuniao);
            $registro = $registro[0]; //Passando Reuniao
            $lista = $model->getParticipantes('P.idReuniao = ' . $idReuniao);
        } else {
            //Novo Registro
            $lista = null;
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        $this->smarty->assign('lista_tipo', $this->carregaTipoReuniao());          
        $this->smarty->assign('lista_local', $this->carregaLocalReuniao());   
        $this->smarty->assign('lista_usuario', $this->carregarusuarios($idReuniao));  
        
        $this->smarty->assign('lista_itens',$lista);
        $this->smarty->assign('opcao',$this->opcao);
        
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('title', 'Ata Reuniao');
        $this->smarty->display('reuniao/form_adiar.tpl');
    }

    public function encerrar() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $model = new reuniaoModel();
        $sy = new system\System();

        $idReuniao = $sy->getParam('idReuniao');
        $this->opcao = $sy->getParam('opcao');
        $this->voltarmenu = $sy->getParam('voltarmenu');
        
        $dados = array('stSituacao' => 4, 'idReuniao' => $idReuniao);
        $model = new reuniaoModel();
        $model->updReuniao($dados);        
        
        header('Location: /reuniao/novo_ata/idReuniao/' . $idReuniao . '/opcao/' . $this->opcao . '/voltarmenu/' . $this->voltarmenu);        
        return;
        
    }

    private function carregarusuarios($idReuniao) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $model = new usuariosModel();        
        $lista = array('' => 'SELECIONE');
        if ($idReuniao) {
            foreach ($model->getUsuarioReuniao(
                    "(L.idUsuario NOT IN (SELECT 
            idUsuario
        FROM
            prodReuniaoParticipantes
        WHERE
            idReuniao = {$idReuniao})
        OR L.dsUsuario = 'USUARIO EXTERNO') and L.dsUsuario <> 'THOPOS'", $idReuniao) as $value) {
                if ($value['dsUsuario'] == 'USUARIO EXTERNO') {
                    $lista[$value['idUsuario']] = $value['dsUsuario'];                    
                } else {
                    $lista[$value['idUsuario']] = $value['dsUsuario'];
                }
            }
        } else {
            foreach ($model->getUsuario() as $value) {
                $lista[$value['idUsuario']] = $value['dsUsuario'];
            }            
        }
        return $lista;
    }
    
    private function carregarusuariosAta() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $model = new usuariosModel();        
        $lista = array('' => 'SELECIONE');
        foreach ($model->getUsuario() as $value) {
            $lista[$value['idUsuario']] = $value['dsUsuario'];
        }
        return $lista;
    }
    
    private function carregarusuariosTarefa($idReuniao) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $model = new usuariosModel();        
        $lista = array('' => 'SELECIONE');
        if ($idReuniao) {
            foreach ($model->getUsuarioReuniaoAta('idReuniao = ' . $idReuniao) as $value) {
                $lista[$value['idUsuario']] = $value['dsNome'];
            }
        } else {
            foreach ($model->getUsuario() as $value) {
                $lista[$value['idUsuario']] = $value['dsUsuario'];
            }            
        }
        return $lista;
    }
    
    public function cancelar_reuniao() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $sy = new system\System();
        $this->opcao = $sy->getParam('opcao');
        $idReuniao = $sy->getParam('idReuniao');
        
        $model = new reuniaoModel();

        if ($idReuniao > 0) {

            $registro = $model->getReuniao('r.idReuniao=' . $idReuniao);
            $registro = $registro[0]; //Passando Reuniao
            $lista = $model->getParticipantes('P.idReuniao = ' . $idReuniao);
        } else {
            //Novo Registro
            $lista = null;
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        
        $this->smarty->assign('lista_tipo', $this->carregaTipoReuniao());          
    //    $this->smarty->assign('lista_local', $this->carregaLocalReuniao());   
        $this->smarty->assign('lista_usuario', $this->carregarusuarios($idReuniao));          
        $this->smarty->assign('lista_itens',$lista);
        
        $this->smarty->assign('opcao',$this->opcao);
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('title', 'Ata Reuniao');
        $this->smarty->display('reuniao/form_cancelar.tpl');
    }

    private function carregaLocalReuniao() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $model = new localreuniaoModel();
        $listaL = array('' => 'SELECIONE');
        foreach ($model->getLocalReuniao() as $value) {
            $listaL[$value['idLocalReuniao']] = $value['dsLocalReuniao'];
        }
        return $listaL;        
    }
    
    private function carregaTipoReuniao() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $model = new tiporeuniaoModel();
        $lista = array('' => 'SELECIONE');
        foreach ($model->getTipoReuniao() as $value) {
            $lista[$value['idTipoReuniao']] = $value['dsTipoReuniao'];
        }
        return $lista;        
    }
    
    public function lerUsuario() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $idUsuario = $_POST['idUsuario'];
        $usuarioModel = new usuariosModel();
        $dadosU = $usuarioModel->getUsuario('L.idUsuario = ' . $idUsuario);
        $retorno = array();
        if ($dadosU) {
            $retorno['dsNome'] = $dadosU[0]['dsUsuario'];
            if ($dadosU[0]['dsUsuario'] == "USUARIO EXTERNO") {
                $retorno['dsNome'] = "";
            }
            $retorno['dsEmail'] = $dadosU[0]['email'];
            $retorno['dsCelular'] = $dadosU[0]['telefone1'];
        }
        
        echo json_encode($retorno);
    }
    // Gravar Paao
    public function gravar_reuniao() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }

        $dsLocalReuniao = ($_POST['dsLocal'] != '') ? $_POST['dsLocal'] : null;        
        $this->opcao = ($_POST['opcao'] != '') ? $_POST['opcao'] : null;        
        $model = new reuniaoModel();

        $data = $this->trataPost($_POST);

        if ($dsLocalReuniao) {
            $modelLR = new localreuniaoModel();
            
            $where = "dsLocalReuniao = '" . $dsLocalReuniao . "'";
            $locais = $modelLR->getLocalReuniao($where);
            if ($locais) {
                $data['idLocalReuniao'] = $locais[0]['idLocalReuniao'];
            } else {
                $array = array(
                    'idLocalReuniao' => null,
                    'dsLocalReuniao' => $dsLocalReuniao
                );
                $data['idLocalReuniao'] = $modelLR->setLocalReuniao($array);
            }
        } else {
            $data['idLocalReuniao'] = null;
        }        
        
        if ($data['idReuniao'] == NULL) {
          $id =  $model->setReuniao($data);
        } else {
          $id = $data['idReuniao'];
          $model->updReuniao($data); //update
        }       
        
        //header('Location: /reuniao/novo_reuniao/idReuniao/' . $id . '/opcao/' . $this->opcao);        
        $retorno = array('id' => $id);
        echo json_encode($retorno);
        
        return;
    }

    public function gravar_adiar_reuniao() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $model = new reuniaoModel();
        $data = $this->trataPostA($_POST);
        $id = $data['idReuniao'];
        $data['stSituacao'] = 1;
        $this->opcao = $_POST['opcao'];
        $model->updReuniao($data); //update
        
        // limpar as mensagens
        $modelM = new mensagemModel();
        $lista = $model->getParticipantes('P.idReuniao = ' . $id);
        foreach ($lista as $value) {
            $mensagem = $modelM->getMensagemItem('m.idTabela = ' . $value['idParticipante'] . ' and m.dsNomeTabela = "prodReuniaoParticipantes"');
            if ($mensagem) {
                $dados = array(
                  'idMensagemItem' => $mensagem[0]['idMensagemItem']  
                );
                $modelM->delMensagemItem($dados);
                $dados = array(
                  'idMensagem' => $mensagem[0]['idMensagem']  
                );
                $modelM->delMensagem($dados);
            }
            // enviar mensagens avisando do adiamento da reuniao
            $this->adicionarmensagem($value['idUsuarioSolicitante'], $value['idParticipante'], $value['idReuniao'], 'ADIAR',0);
        }
        
       // header('Location: /reuniao/adiar_reuniao/idReuniao/' . $id . '/opcao/' . $this->opcao);        

        $retorno = array('id' => $id);
        echo json_encode($retorno);
        
        return;
    }

    public function gravar_cancelar_reuniao() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }

        $model = new reuniaoModel();
        $data = $this->trataPostC($_POST);
        $id = $data['idReuniao'];
        $data['stSituacao'] = 2;
        $model->updReuniao($data); //update
        
        // limpar as mensagens
        $modelM = new mensagemModel();
        $lista = $model->getParticipantes('P.idReuniao = ' . $id);
        foreach ($lista as $value) {
            $mensagem = $modelM->getMensagemItem('m.idTabela = ' . $value['idParticipante'] . ' and m.dsNomeTabela = "prodReuniaoParticipantes"');
            if ($mensagem) {
                $dados = array(
                  'idMensagemItem' => $mensagem[0]['idMensagemItem']  
                );
                $modelM->delMensagemItem($dados);
                $dados = array(
                  'idMensagem' => $mensagem[0]['idMensagem']  
                );
                $modelM->delMensagem($dados);
            }
            // enviar mensagens avisando do adiamento da reuniao
            $this->adicionarmensagem($value['idUsuarioSolicitante'], $value['idParticipante'], $value['idReuniao'], 'CANCELAR',0);
        }
        
//        header('Location: /reuniao/cancelar_reuniao/idReuniao/' . $id);        
        $retorno = array('id' => $id);
        echo json_encode($retorno);
        
        return;
    }
    public function gravar_ata_reuniao() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }

        $dsLocalReuniao = ($_REQUEST['dsLocal'] != '') ? $_REQUEST['dsLocal'] : null;
        $model = new reuniaoModel();
        $data = $this->trataPostAta($_REQUEST);
        $id = $data['idReuniao'];
        $this->opcao = ($_REQUEST['opcao'] != '') ? $_REQUEST['opcao'] : null;        
        $this->voltarmenu = ($_REQUEST['voltarmenu'] != '') ? $_REQUEST['voltarmenu'] : null;        
        
        if ($dsLocalReuniao) {
            $modelLR = new localreuniaoModel();
            
            $where = "dsLocalReuniao = '" . $dsLocalReuniao . "'";
            $locais = $modelLR->getLocalReuniao($where);
            if ($locais) {
                $data['idLocalReuniao'] = $locais[0]['idLocalReuniao'];
            } else {
                $array = array(
                    'idLocalReuniao' => null,
                    'dsLocalReuniao' => $dsLocalReuniao
                );
                $data['idLocalReuniao'] = $modelLR->setLocalReuniao($array);
            }
        } else {
            $data['idLocalReuniao'] = null;
        }
        
        $model->updReuniao($data); //update        
        header('Location: /reuniao/novo_ata/idReuniao/' . $id . '/opcao/' . $this->opcao . '/voltarmenu/' . $this->voltarmenu);        
        return;
    }
    
    public function adicionaritem() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
      
        $data = $this->trataPostItem($_POST);
        $modelG = new reuniaoModel();
        if ($data['idParticipante']) {
            $id = $data['idParticipante'];
            $modelG->updParticipantes($data);
        } else {
            $id = $modelG->setParticipantes($data);
        }               
        
        $lista = $modelG->getParticipantes('P.idParticipante = ' . $id);
        // ENVIAR MENSAGEM  
        if ($lista) {
            $this->adicionarmensagem($lista[0]['idUsuarioSolicitante'], $lista[0]['idParticipante'], $lista[0]['idReuniao'],'AGENDAR',1);
        }
        $lista = $modelG->getParticipantes('P.idReuniao = ' . $data['idReuniao']);
        
        $this->smarty->assign('lista_usuario', $this->carregarusuarios($data['idReuniao']));                  
        $this->smarty->assign('lista_itens',$lista);
        $html = $this->smarty->fetch("reuniao/itens.html");
        $htmlU = $this->smarty->fetch("reuniao/lista_usuario.tpl");
        
        $jsondata = array(
            'html' => $html,
            'htmlU' => $htmlU
        );
        echo json_encode($jsondata);        
    }
    
    public function adicionartarefa() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $data = $this->trataPostItemTarefa($_POST);
        $origem = $_POST['origem'];
        
        $modelT = new reuniaoModel();
        if ($data['idTarefa']) {
            $id = $data['idTarefa'];
            $modelT->updTarefas($data);
        } else {
            $id = $modelT->setTarefas($data);
        }               
        
        $lista = $modelT->getParticipantesTarefa('P.idTarefa = ' . $id);
        // ENVIAR MENSAGEM  
        if ($lista) {
            $this->adicionarmensagem($lista[0]['idUsuarioSolicitante'], $lista[0]['idTarefa'], $lista[0]['idReuniao'],'TAREFA',0);
        }
        
        $listaUT = $modelT->getParticipantesTarefa('P.idReuniao = ' . $data['idReuniao']);
//        var_dump($listaUT); die;
        $this->smarty->assign('lista_itensTarefa',$listaUT);
//        
//        $this->smarty->assign('lista_usuario', $this->carregarusuarios($data['idReuniao']));                  
        $html = $this->smarty->fetch("reuniao/itenstarefa.html");
//        $htmlU = $this->smarty->fetch("reuniao/lista_usuario.tpl");
        
        $jsondata = array(
            'html' => $html
        );
        echo json_encode($jsondata);        
    }
    
    public function adicionaritemAta() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $data = $this->trataPostItem($_POST);
        $modelG = new reuniaoModel();
        if ($data['idParticipante']) {
            $id = $data['idParticipante'];
            $modelG->updParticipantes($data);
        } else {
            $id = $modelG->setParticipantes($data);
        }               
        
        $lista = $modelG->getParticipantes('P.idParticipante = ' . $id);
        // ENVIAR MENSAGEM  
        if ($lista) {
            $this->adicionarmensagem($lista[0]['idUsuarioSolicitante'], $lista[0]['idParticipante'], $lista[0]['idReuniao'],'AGENDAR',0);
        }
        $lista = $modelG->getParticipantes('P.idReuniao = ' . $data['idReuniao']);
        $this->smarty->assign('lista_itens',$lista);
        $this->smarty->assign('lista_usuario', $this->carregarusuarios($data['idReuniao']));                  
        $this->smarty->assign('lista_usuariotarefa', $this->carregarusuariosTarefa($data['idReuniao']));                  
        $html = $this->smarty->fetch("reuniao/itensata.html");
        $htmlU = $this->smarty->fetch("reuniao/lista_usuario.tpl");
        $htmlUA = $this->smarty->fetch("reuniao/lista_usuariotarefa.tpl");
        
        $jsondata = array(
            'html' => $html,
            'htmlU' => $htmlU,
            'htmlUA' => $htmlUA
        );
        echo json_encode($jsondata);        
    }

    public function envairemail() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
      
        $idParticipante = $_POST['idParticipante'];
        $idReuniao = $_POST['idReuniao'];
        $origem = $_POST['deOnde'];
        $retorno = $this->envair_mesmo_email($idReuniao,$idParticipante, $origem);
        if ($retorno) {
            $ok = 'E-mail enviado com sucesso';
        } else {
            $ok = 'ERRO AO ENVIAR E-MAIL';
        }
        $jsondata = array(
            'ok' => $ok
        );
        echo json_encode($jsondata);   
    }
    
    private function envair_mesmo_email($idReuniao, $idParticipante, $origem) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $model = new reuniaoModel();  
        if ($idParticipante) {
            $retorno = $model->getParticipantes('P.idParticipante = ' . $idParticipante);
        } else {
            $retorno = $model->getParticipantes('P.idReuniao = ' . $idReuniao);
        }
        $participantes = $model->getParticipantes('P.idReuniao = ' . $idReuniao);

        if ($origem == 'Ata') {
            $titulo = 'REUNIAO REALIZADA';
        } else {
        if ($origem == 'Reuniao') {
            $titulo = 'REUNIAO AGENDADA';
        } else {
        if ($origem == 'Cancelar') {
            $titulo = 'REUNIAO CANCELADA';
        } else {
            $titulo = 'REUNIAO ADIADA';
        }}}

        $caminhoAta = $retorno[0]['dsCaminhoPDF'];
        
        $to = array();
        $emails = array();
        if ($retorno) {
            $emails = $retorno;
        }
        $x=0;
        if ((bool)$emails) {
            foreach ($emails as $value) {
                $to[$x]['email'] = filter_var(strtolower($value['email']), FILTER_VALIDATE_EMAIL);
                $to[$x]['nome'] = $value['dsUsuario'];
                $x++;
            }
        }
        array_filter($to);        
//                print_a_die($retorno); die;
        
        if (!empty($to)) {
            $x=0;
            $this->smarty->assign('idReuniao', $retorno[$x]['idReuniao']);
            $this->smarty->assign('idParticipante', $retorno[$x]['idParticipante']);
            foreach ($to as $value) {
                $title = $titulo;
                $this->smarty->assign('title', $title);
                $this->smarty->assign('usuariodestino', $retorno[$x]['dsUsuario']);
                $this->smarty->assign('data_envio', date('Y-m-d H:i'));
                $this->smarty->assign('usuarioenvio', $retorno[$x]['UsuarioSolicitante']);
                $this->smarty->assign('tiporeuniao', $retorno[$x]['dsTipoReuniao']);
                $this->smarty->assign('dtReuniao', $retorno[$x]['dtReuniao']);
                $this->smarty->assign('dsPauta', $retorno[$x]['dsPauta']);
                $this->smarty->assign('dsAssunto', $retorno[$x]['dsAssunto']);
                $this->smarty->assign('dsTempoDuracao', $retorno[$x]['dsTempoDuracao']);
                $this->smarty->assign('dsLocalReuniao', $retorno[$x]['dsLocalReuniao']);
                $this->smarty->assign('lista_itens',$participantes);
                if ($origem == 'Ata') {
                    $this->smarty->assign('dsAta', $retorno[$x]['dsAta']);
                    $this->smarty->assign('dsTempoDuracaoReal', $retorno[$x]['dsTempoDuracaoReal']);
                    $this->smarty->assign('dtRealizacao', $retorno[$x]['dtRealizacao']);
//                    $body = $this->smarty->fetch('reuniao/email/enviar_email_ata.tpl');
//                } else {
//                    $body = $this->smarty->fetch('reuniao/email/enviar_email.tpl');
                }
                $body = $this->smarty->fetch('reuniao/email/enviar_email.tpl');

                $anexos = array();
                $modelD = new documentoModel();
                $where = "dsTabela = 'prodReuniao' and idTabela = " . $idReuniao;
                $retanex = $modelD->getDocumento($where);
                $y=0;
                foreach ($retanex as $valuea) {
                    $anexos[$y]['path_anexo'] = $valuea['dsLocalArquivo'];
                    $anexos[$y]['nome_anexo'] = $valuea['dsNome'];
                    $y++;
                }

                $modelD = new documentoModel();
                $where = "dsTabela = 'prodReuniaoParticipantes' and idTabela = " . $retorno[$x]['idParticipante'];
                $retanex = $modelD->getDocumento($where);
                foreach ($retanex as $valuea) {
                    $anexos[$y]['path_anexo'] = $valuea['dsLocalArquivo'];
                    $anexos[$y]['nome_anexo'] = $valuea['dsNome'];
                    $y++;
                }

                // envia a ata como anexo
                if ($caminhoAta) {
                    $anexos[$y]['path_anexo'] = $caminhoAta;
                    $anexos[$y]['nome_anexo'] = 'ATA DA REUNIÃO';
                }
                
                $ok = $this->Send(array('0' => array(
                    'email' => $value['email'],
                    'nome' => $value['nome'])), $titulo, $body, array(), $anexos);
                $x++;
            }
            return $ok;
        } else {
            return false;
        }
    }

    private function Send($to_list, $subject, $body, $addbcc = array(), $attachment = array()) {    
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->SMTPDebug = 2;
        $mail->SMTPSecure = 'tls';            
        $mail->Port = 587;
        $mail->FromName = 'Thopos';
        $mail->IsHTML('text/html');    
        $mail->CharSet = 'UTF-8';
        
        $model = new reuniaoModel('idDE = 1');
        $valores = $model->getValores();
        if ($valores) {
            $mail->Host = ($valores[0]['H']);
            $mail->Username = ($valores[0]['U']);
            $mail->Password = ($valores[0]['P']);
            $mail->From = ($valores[0]['F']);
        } else {
            return false;            
        }
        
        $mail->ClearAddresses();
        
        $concat_to = array();
        foreach ($to_list as $to) {
          if (empty($to['nome'])) {
            $to['nome'] = $to['email'];
          }
          $concat_to[] = "{$to['nome']} &lt;{$to['email']}&gt;"; 
        }
        foreach ($to_list as $to) {
            $mail->AddAddress($to["email"], $to["nome"]);
        }
          
        $mail->Subject = $subject;
        $mail->Body = $body;
        
        foreach ($attachment as $att) {     
            $mail->AddAttachment($att['path_anexo'], $att['nome_anexo']);
        }

        if ($mail->Send()) {
            return true;
        } else {
            return false;
        }
    }
    public function emitir_pdf() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $idReuniao = ($_POST['idReuniao'] != '') ? $_POST['idReuniao'] : null;        
        $url = $this->enviarPDF($idReuniao);
        $retorno = array();
        echo json_encode($retorno);        
    }

    public function enviarPDF($idReuniao) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $idReuniao = ($_POST['idReuniao'] != '') ? $_POST['idReuniao'] : null;
        global $PATH;
        $caminho = $GLOBALS['PATH'];
        if (!is_dir($this->pastaPedidos)) {
            mkdir($this->pastaPedidos, 0777, true);
        }
        $url = null;
        if (file_exists($this->pastaPedidos)) {
            
            $where = 'r.idReuniao = ' . $idReuniao;
            $model = new reuniaoModel();
            $modelD = new documentoModel();
            $cabec = $model->getReuniao($where)[0];
            $this->smarty->assign("reuniao", $cabec);
            $participantes = $model->getParticipantes($where);
            $this->smarty->assign("participantes", $participantes);
            $tarefas = $model->getParticipantesTarefa($where);
//            var_dump($participantes);
//            var_dump($tarefas); die;
            $this->smarty->assign("tarefas", $tarefas);
            $anexos = $modelD->getDocumento("dsTabela = 'prodReuniao' and idTabela = " . $idReuniao);
            if ($anexos) {
                $this->smarty->assign("anexos", $anexos);
            } else {
                $this->smarty->assign("anexos", null);
            }

            // Filename
            $filename = "ATA_Reuniao_" . $idReuniao . "_" . $cabec['dtReuniao'];
            // Dependecias
            require_once 'system/libs/mpdf/mpdf.php';
            $mpdf = new mPDF('c', 'A4', '5', 'Arial');

            $template = "reuniao/pdf/PDFTemplate.tpl";

            $this->smarty->clearCache($template);
            $mpdf->WriteHTML($this->smarty->fetch($template));
            if (file_exists("{$this->pastaPedidos}{$filename}.pdf")) {
                unlink("{$this->pastaPedidos}{$filename}.pdf");
            }
            // Arquivo
            $mpdf->Output("{$this->pastaPedidos}{$filename}.pdf", 'F');

            if(file_exists("{$this->pastaPedidos}{$filename}.pdf")  ){
                $url = "{$this->pastaPedidos}{$filename}.pdf";
            }
            // ATUALIZR O CAMINHO DO PDF NO PEDIDO
            $data = array(
                'dsCaminhoPDF' => $url,
                'idReuniao' => $idReuniao
            );
            $model->updReuniao($data);
        }
        
        return $url;
    }    
    
    public function excluiranexo() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $model = new documentoModel();        
        $idDocumento = $_POST['idDocumento'];
        $idReuniao = $_POST['idReuniao'];
        $dsTabela = $_POST['dsTabela'];
        $idParticipante = $_POST['idParticipante'];

        $ok = 'Anexo excluido';
        $where = array('idDocumento' => $idDocumento);
        $model->delDocumento($where);
        if ($idParticipante) {
            $lista = $model->getDocumento("dsTabela = '" . $dsTabela . "' and idTabela  = " . $idParticipante);
        } else {
            $lista = $model->getDocumento("dsTabela = '" . $dsTabela . "' and idTabela  = " . $idReuniao);
        }
        $this->smarty->assign('fotos_lista', $lista);
        $html = $this->smarty->fetch("reuniao/modalFotos/thumbnail1.tpl");        
        
        $jsondata = array(
            'html' => $html,
            'ok' => $ok
        );
        echo json_encode($jsondata);
    }
    
    public function delparticipante() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $model = new reuniaoModel();        
        $idReuniao = $_POST['idReuniao'];
        $idParticipante = $_POST['idParticipante'];
        $ok = 'Participante excluido';
        $where = 'idParticipante = ' . $idParticipante;
        $model->delParticipantes($where);        
        
        $lista = $model->getParticipantes('P.idReuniao = ' . $idReuniao);
        $this->smarty->assign('lista_itens',$lista);
        $this->smarty->assign('lista_usuario', $this->carregarusuarios($idReuniao));                  
        
        $html = $this->smarty->fetch("reuniao/itens.html");
        $htmlU = $this->smarty->fetch("reuniao/lista_usuario.tpl");
        
        $jsondata = array(
            'html' => $html,
            'htmlU' => $htmlU,
            'ok' => $ok
        );
        echo json_encode($jsondata);
    }
    public function deltarefa() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $model = new reuniaoModel();        
        $idReuniao = $_POST['idReuniao'];
        $idTarefa = $_POST['idTarefa'];
        $ok = 'Tarefa excluida';
        $where = 'idTarefa = ' . $idTarefa;
        $model->delTarefa($where);        
        
        $listaUT = $model->getParticipantesTarefa('P.idReuniao = ' . $idReuniao);
//        var_dump($listaUT); die;        
        $this->smarty->assign('lista_itensTarefa',$listaUT);
        $html = $this->smarty->fetch("reuniao/itenstarefa.html");
        
        $jsondata = array(
            'html' => $html,
            'ok' => $ok
        );
        echo json_encode($jsondata);
    }
    
    public function concluirtarefa() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $model = new reuniaoModel();        
        $idTarefa = $_POST['idTarefa'];
        $array = array('idTarefa' => $idTarefa,
                'stStatusTarefa' => 1,
                'dtConclusao' => date('Y-m-d H:i:s')
                );
        $model->updTarefas($array);        
        echo json_encode(array());
    }
    
    public function concluirsubtarefa() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $model = new reuniaoModel();        
        $idData = $_POST['idData'];
        $origem = $_POST['origem'];
        $idTarefa = $_POST['idTarefa'];
        $idUsuario = $_POST['idUsuario'];
        $array = array('idData' => $idData,
                'stStatusTarefa' => 1,
                'dtConclusao' => date('Y-m-d H:i:s')
                );
        $model->updDatas($array);    
        
        $lista = $model->getDatas("idTarefa  = " . $idTarefa);
        $this->smarty->assign('datas_lista', $lista);
        $this->smarty->assign('origem', $origem);
        $this->smarty->assign('idTarefa', $idTarefa);
        $this->smarty->assign('idUsuario', $idUsuario);
        $html = $this->smarty->fetch("reuniao/modalDatas/thumbnail1.tpl");        
        
        echo json_encode(array('html'=>$html));
    }
    
    public function presente() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $model = new reuniaoModel();        
        $idReuniao = $_POST['idReuniao'];
        $idParticipante = $_POST['idParticipante'];
        $ok = 'Participante presente';
        $array = array('idParticipante' => $idParticipante,
                'stSituacao' => 2
                );
        $model->updParticipantes($array);        
        
        $lista = $model->getParticipantes('P.idReuniao = ' . $idReuniao);
        $this->smarty->assign('lista_itens',$lista);
        $html = $this->smarty->fetch("reuniao/itensata.html");
        
        $jsondata = array(
            'html' => $html,
            'ok' => $ok
        );
        echo json_encode($jsondata);
    }
    
    public function confirmar() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $model = new reuniaoModel();        
        $idReuniao = $_POST['idReuniao'];
        $idUsuario = $_SESSION['user']['usuario'];
        $ok = 1;

        $array = array('idUsuario' => $idUsuario,
                'idReuniao' => $idReuniao,
                'stPresenca' => 1
                );
        $model->updParticipantesPresenca($array);        

        $sql = "r.dtReuniao >= '" . date('Y-m-d H:i:s') . "' and r.idReuniao > 0 and r.stSituacao = 0 and (r.idUsuarioSolicitante = " . $_SESSION['user']['usuario'] . " or p.idUsuario = " . $_SESSION['user']['usuario'] . ")";        
        $reuniao_lista = $model->getReuniao($sql);
        
        $this->smarty->assign('reuniao_lista', $reuniao_lista);
        $html = $this->smarty->fetch('reuniao/menuIndex_lista.html');
        
        $jsondata = array(
            'ok' => $ok,
            'html' => $html
        );
        echo json_encode($jsondata);
    }
    
    public function naoconfirmar() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $model = new reuniaoModel();        
        $idReuniao = $_POST['idReuniao'];
        $idUsuario = $_SESSION['user']['usuario'];
        $ok = 1;

        $array = array('idUsuario' => $idUsuario,
                'idReuniao' => $idReuniao,
                'stPresenca' => 2
                );
        $model->updParticipantesPresenca($array);        
        
        $sql = "r.dtReuniao >= '" . date('Y-m-d H:i:s') . "' and r.idReuniao > 0 and r.stSituacao = 0 and (r.idUsuarioSolicitante = " . $_SESSION['user']['usuario'] . " or p.idUsuario = " . $_SESSION['user']['usuario'] . ")";        
        $reuniao_lista = $model->getReuniao($sql);
        
        $this->smarty->assign('reuniao_lista', $reuniao_lista);
        $html = $this->smarty->fetch('reuniao/menuIndex_lista.html');
        
        $jsondata = array(
            'ok' => $ok,
            'html' => $html
        );
        echo json_encode($jsondata);
    }
    
    public function ausente() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $model = new reuniaoModel();        
        $idReuniao = $_POST['idReuniao'];
        $idParticipante = $_POST['idParticipante'];
        $ok = 'Participante ausente';
        $array = array('idParticipante' => $idParticipante,
                'stSituacao' => 1
                );
        $model->updParticipantes($array);        
        
        $lista = $model->getParticipantes('P.idReuniao = ' . $idReuniao);
        $this->smarty->assign('lista_itens',$lista);
        $html = $this->smarty->fetch("reuniao/itensata.html");
        
        $jsondata = array(
            'html' => $html,
            'ok' => $ok
        );
        echo json_encode($jsondata);
    }
    
    //Trata dados antes de Enviar para o Gravar
    private function trataPost($post) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $data['idReuniao'] = ($post['idReuniao'] != '') ? $post['idReuniao'] : null;
        $data['idTipoReuniao'] = ($post['idTipo'] != '') ? $post['idTipo'] : null;
        $data['idLocalReuniao'] = ($post['idLocal'] != '') ? $post['idLocal'] : null;
        $data['dsTempoDuracao'] = ($post['dsTempoDuracao'] != '') ? $post['dsTempoDuracao'] : null;
        $data['dtReuniao'] = ($post['dtReuniao'] != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $_POST["dtReuniao"]))) : date('Y-m-d h:m');
        $data['dsAssunto'] = ($post['dsAssunto'] != '') ? $post['dsAssunto'] : null;
        $data['idUsuarioSolicitante'] = $_SESSION['user']['usuario'];
        return $data;
    }

    private function trataPostA($post) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $data['idReuniao'] = ($post['idReuniao'] != '') ? $post['idReuniao'] : null;
        $data['dtReuniao'] = ($post['dtReuniao'] != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $_POST["dtReuniao"]))) : date('Y-m-d h:m');
        return $data;
    }

    private function trataPostAta($post) {    
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $data['idReuniao'] = ($post['idReuniao'] != '') ? $post['idReuniao'] : null;
        $data['dtRealizacao'] = ($post['dtRealizacao'] != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $_POST["dtRealizacao"]))) : date('Y-m-d h:m');
        $data['idUsuarioAta'] = ($post['idUsuarioAta'] != '') ? $post['idUsuarioAta'] : null;
        $data['idTipoReuniao'] = ($post['idTipo'] != '') ? $post['idTipo'] : null;
        $data['idLocalReuniao'] = ($post['idLocal'] != '') ? $post['idLocal'] : null;
        $data['dtReuniao'] = ($post['dtReuniao'] != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $_POST["dtReuniao"]))) : date('Y-m-d h:m');
        $data['dsAssunto'] = ($post['dsAssunto'] != '') ? $post['dsAssunto'] : null;
        $data['dsTempoDuracaoReal'] = ($post['dsTempoDuracaoReal'] != '') ? $post['dsTempoDuracaoReal'] : null;
        $data['dsAta'] = ($post['dsAta'] != '') ? $post['dsAta'] : null;
        return $data;
    }

    private function trataPostC($post) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $data['idReuniao'] = ($post['idReuniao'] != '') ? $post['idReuniao'] : null;
        $data['dsCancelado'] = ($post['dsCancelado'] != '') ? $post['dsCancelado'] : null;
        return $data;
    }

    private function trataPostItem($post) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $data['idReuniao'] = ($post['idReuniao'] != '') ? $post['idReuniao'] : null;
        $data['idParticipante'] = ($post['idParticipante'] != '') ? $post['idParticipante'] : null;
        $data['idUsuario'] = ($post['idUsuario'] != '') ? $post['idUsuario'] : null;
        $data['dsAssunto'] = ($post['dsAssunto'] != '') ? $post['dsAssunto'] : null;
        $data['dsEmail'] = ($post['dsEmail'] != '') ? $post['dsEmail'] : null;
        $data['dsNome'] = ($post['dsNome'] != '') ? $post['dsNome'] : null;
        $data['dsCelular'] = ($post['dsCelular'] != '') ? $post['dsCelular'] : null;
        $data['idParceiro'] =  null;
        $data['dsCaminhoAnexo'] = NULL;
        return $data;
    }

    private function trataPostItemTarefa($post) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $data['idReuniao'] = ($post['idReuniao'] != '') ? $post['idReuniao'] : null;
        $data['idTarefa'] = ($post['idTarefa'] != '') ? $post['idTarefa'] : null;
        $data['idUsuario'] = ($post['idUsuarioTarefa'] != '') ? $post['idUsuarioTarefa'] : null;
        $data['dsTarefa'] = ($post['dsTarefa'] != '') ? $post['dsTarefa'] : null;
        $data['stStatusTarefa'] = 0;
        $data['dtPrazo'] = ($post['dtPrazo'] != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $_POST["dtPrazo"]))) : date('Y-m-d h:m');
        return $data;
    }

    // Remove Paao
    public function delreuniao() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $sy = new system\System();                
        $idReuniao = $sy->getParam('idReuniao');        
        if (!is_null($idReuniao)) {    
            $model = new reuniaoModel();
            $dados['idReuniao'] = $idReuniao;             
            $dados['stSituacao'] = 3;             
            $model->updReuniao($dados);
        }
        header('Location: /reuniao');
    }
    
    public function arquivar() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $sy = new system\System();                
        $idReuniao = $sy->getParam('idReuniao');        
        if (!is_null($idReuniao)) {    
            $model = new reuniaoModel();
            $dados['idReuniao'] = $idReuniao;             
            $dados['stSituacao'] = 3;             
            $model->updReuniao($dados);
        }
        header('Location: /reuniao');
    }
    
    public function importarfotos() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $idReuniao = $_POST['idReuniao'];
        $dsTabela = $_POST['dsTabela'];
        $idParticipante = $_POST['idParticipante'];
        $Origem = $_POST['Origem'];
        
        $model = new documentoModel();        
        $lista = $model->getDocumento("dsTabela = '" . $dsTabela . "' and idTabela  = " . $idParticipante);
        $this->smarty->assign('idParticipante', $idParticipante);
        $this->smarty->assign('idReuniao', $idReuniao);
        $this->smarty->assign('dsTabela', $dsTabela);
        $this->smarty->assign('fotos_lista', $lista);
        $this->smarty->assign('Origem', $Origem);
        $this->smarty->display("reuniao/modalFotos/thumbnail.tpl");        
    }

    public function importarfotosR() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $idReuniao = $_POST['idReuniao'];
        $dsTabela = $_POST['dsTabela'];
        $Origem = $_POST['Origem'];
        
        $model = new documentoModel();        
        $lista = $model->getDocumento("dsTabela = '" . $dsTabela . "' and idTabela  = " . $idReuniao);
        $this->smarty->assign('idParticipante', null);
        $this->smarty->assign('idReuniao', $idReuniao);
        $this->smarty->assign('dsTabela', $dsTabela);
        $this->smarty->assign('Origem', $Origem);
        $this->smarty->assign('fotos_lista', $lista);
        $this->smarty->display("reuniao/modalFotos/thumbnail.tpl");        
    }

    public function adicionarfoto() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $idReuniao = $_POST['idReuniao'];
        $dsTabela = $_POST['dsTabela'];
        $idParticipante = $_POST['idParticipante'];
        $dsNome = $_POST['dsNome'];
        $Origem = $_POST['Origem'];
        if (!$idParticipante) {
            $idParticipante = $idReuniao;
        }
        $extensao = explode('.',$_FILES['arquivo']['name'])[1];
        $novonome = $idParticipante . '_' . date('Ymdis') . '.' . $extensao;
        
        $_UP['pasta'] = 'storage/tmp/documentos/';
        // Tamanho máximo do arquivo (em Bytes)
        $_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
        // Array com as extensões permitidas
        $_UP['extensoes'] = array('jpg','jpeg','png','bmp');
        // Renomeia o arquivo? (Se true, o arquivo será salvo como .jpg e um nome único)
        $_UP['renomeia'] = false;
        // Array com os tipos de erros de upload do PHP
        $_UP['erros'][0] = 'Não houve erro';
        $_UP['erros'][1] = 'O arquivo no upload é maior do que o limite do PHP';
        $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
        $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
        $_UP['erros'][4] = 'Não foi feito o upload do arquivo';
        // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
        if ($_FILES['arquivo']['error'] != 0) {
          die("Não foi possível fazer o upload, erro:" . $_UP['erros'][$_FILES['arquivo']['error']]);
          exit; // Para a execução do script
        }
        // Caso script chegue a esse ponto, não houve erro com o upload e o PHP pode continuar
        // Faz a verificação da extensão do arquivo
        // Faz a verificação do tamanho do arquivo
        if ($_UP['tamanho'] < $_FILES['arquivo']['size']) {
          echo "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
          exit;
        }

        // Depois verifica se é possível mover o arquivo para a pasta escolhida
        if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $_UP['pasta'] . $novonome)) {
            $model = new documentoModel();    
            $dados = array(
                'idDocumento' => null,
                'dsTabela' => $dsTabela,
                'idTabela' => $idParticipante,
                'dsNome' => $dsNome,
                'dsLocalArquivo' => $_UP['pasta'] . $novonome                
            );
            $model->setDocumento($dados);                        
        } else {
          // Não foi possível fazer o upload, provavelmente a pasta está incorreta
          echo "Não foi possível enviar o arquivo, tente novamente";
        }    
        if ($Origem == "Reuniao") {
            header('Location: /reuniao/novo_reuniao/idReuniao/' . $idReuniao);                
        } else {
            header('Location: /reuniao/novo_ata/idReuniao/' . $idReuniao);                            
        }
    }    
    
    public function adicionardata() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $idData = $_POST['idData'];
        $idTarefa = $_POST['idTarefa'];
        $dtIntermediaria = $_POST['dtIntermediaria'];
        $dsSubTarefa = $_POST['dsSubTarefa'];
        $origem = $_POST['origem'];
        $model = new reuniaoModel();    
        if ($idData) {
            $dados = array(
                'idData' => $idData,
                'dsSubTarefa' => $dsSubTarefa,
                'dtIntermediaria' => ($dtIntermediaria != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $dtIntermediaria))) : date('Y-m-d h:m')
            );
            $model->updDatas($dados);                        
        } else {
            $dados = array(
                'idData' => null,
                'idTarefa' => $idTarefa,
                'dsSubTarefa' => $dsSubTarefa,
                'dtIntermediaria' => ($dtIntermediaria != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $dtIntermediaria))) : date('Y-m-d h:m')
            );
            $model->setDatas($dados);                        
        }
        $lista = $model->getDatas("idTarefa  = " . $idTarefa);
        $this->smarty->assign('datas_lista', $lista);
        $this->smarty->assign('origem', $origem);
        $html = $this->smarty->fetch("reuniao/modalDatas/thumbnail1.tpl");        
        echo json_encode(array('html' => $html));
    }    
    public function deldata() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $idData = $_POST['idData'];
        $idTarefa = $_POST['idTarefa'];
        $origem = $_POST['origem'];
        $idUsuario = $_POST['idUsuario'];
        
        $model = new reuniaoModel();    
        $where = 'idData = ' . $idData;
        $model->delData($where);                        
        $lista = $model->getDatas("idTarefa  = " . $idTarefa);
        $this->smarty->assign('datas_lista', $lista);
        $this->smarty->assign('origem', $origem);
        $this->smarty->assign('idUsuario', $idUsuario);
        $html = $this->smarty->fetch("reuniao/modalDatas/thumbnail1.tpl");        
        echo json_encode(array('html' => $html));
    }    
    public function verdatas() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $idReuniao = $_POST['idReuniao'];
        $idTarefa = $_POST['idTarefa'];
        $origem = $_POST['origem'];
        $idUsuario = $_POST['idUsuario'];
        
        
        $model = new reuniaoModel();        
        $lista = $model->getDatas("idTarefa  = " . $idTarefa);
        //$lista[0] = array('dtIntermediaria' => date('Y-m-d'), 'dsSubTarefa' => 'teste', 'dtConclusao' => date('Y-m-d'), 'stStatusTarefa' => '0', 'idData' => 1, 'idTarefa' => 1);
        //var_dump($lista); die;
        $this->smarty->assign('idTarefa', $idTarefa);
        $this->smarty->assign('idData', null);
        $this->smarty->assign('origem', $origem);
        $this->smarty->assign('idUsuario', $idUsuario);
        $this->smarty->assign('datas_lista', $lista);
        $this->smarty->display("reuniao/modalDatas/thumbnail.tpl");        
    }

    private function adicionarmensagem($idUsuario, $idParticipante, $idReuniao, $qual, $tipo) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $mensagem = 'Reunião: ';
        $model = new reuniaoModel();    
        if ($qual == 'TAREFA') {
            $dados = $model->getParticipantesTarefa('P.idTarefa = ' . $idParticipante);
        } else {
            $dados = $model->getParticipantes('P.idParticipante = ' . $idParticipante);
        }
        
        if ($dados) {
            if ($dados[0]['idUsuario'] <> $_SESSION['user']['usuario']) { 
                if ($qual == 'AGENDAR') {
                    $descricao = 'Reunião: ' . $dados[0]['dsTipoReuniao'] . ' - Agendado por: ' . $dados[0]['UsuarioSolicitante'] . ' - Data ' . date("d/m/Y H:i", strtotime(str_replace("-", "/", $dados[0]['dtReuniao']))) . ' - Pauta: ' . $dados[0]['dsPauta'] . ' - Seu Assunto: ' . $dados[0]['assuntoP'];
                } else {
                if ($qual == 'ADIAR') {
                    $descricao = 'ADIADA - Reunião: ' . $dados[0]['dsTipoReuniao'] . ' - Agendado por: ' . $dados[0]['UsuarioSolicitante'] . ' - Data ' . date("d/m/Y H:i", strtotime(str_replace("-", "/", $dados[0]['dtReuniao']))) . ' - Pauta: ' . $dados[0]['dsPauta'] . ' - Seu Assunto: ' . $dados[0]['assuntoP'];
                } else {
                if ($qual == 'TAREFA') {
                    $descricao = 'TAREFA - Reunião: ' . $dados[0]['dsTipoReuniao'] . ' - Agendado por: ' . $dados[0]['UsuarioSolicitante'] . ' - Data ' . date("d/m/Y H:i", strtotime(str_replace("-", "/", $dados[0]['dtRealizacao']))) . ' - Pauta: ' . $dados[0]['dsPauta'] . ' - Sua Tarefa: ' . $dados[0]['tarefa'] . ' - Prazo ' . date("d/m/Y H:i", strtotime(str_replace("-", "/", $dados[0]['dtRealizacao'])));
                } else {
                    $descricao = 'CANCELADA - Reunião: ' . $dados[0]['dsTipoReuniao'] . ' - Agendado por: ' . $dados[0]['UsuarioSolicitante'] . ' - Data ' . date("d/m/Y H:i", strtotime(str_replace("-", "/", $dados[0]['dtReuniao']))) . ' - Pauta: ' . $dados[0]['dsPauta'] . ' - Seu Assunto: ' . $dados[0]['assuntoP'];
                }}}
                $dadosi = array(
                    'idUsuarioOrigem' => $_SESSION['user']['usuario'],
                    'idUsuarioDestino' => $dados[0]['idUsuario'],
                    'dsNomeTabela' => 'prodReuniaoParticipantes',
                    'idOrigemInformacao' => 99,
                    'idTabela' => $idParticipante,
                    'idTipoMensagem' => $tipo,
                    'stSituacao' => 0,
                    'dtEnvio' => date('Y-m-d H:i:s'),
                    'dsMensagem' => $descricao,
                    'dsCaminhoArquivo' => null
                );
                $this->criarMensagem($dadosi);
            }
        }        
    }
    
    private function criarMensagem($array) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $modelMensagem = new mensagemModel();
        $dados = array(
          'idMensagem' => null,  
          'idOrigemInformacao' => $array['idOrigemInformacao'],
          'dsNomeTabela' => $array['dsNomeTabela'],
          'idTabela' => $array['idTabela']
        );
        $id = $modelMensagem->setMensagem($dados);
        $dados = array(
          'idMensagemAnterior' => $id,              
        );        
        $modelMensagem->updMensagem($dados, 'idMensagem = ' . $id);
        $dados = array(
            'idMensagemItem' => null,  
            'idMensagem' => $id,  
            'idUsuarioOrigem' => $array['idUsuarioOrigem'],
            'idUsuarioDestino' => $array['idUsuarioDestino'],
            'idTipoMensagem' => $array['idTipoMensagem'],
            'stSituacao' => $array['stSituacao'],
            'dtEnvio' => $array['dtEnvio'],
            'dsMensagem' => $array['dsMensagem'],
            'dsCaminhoArquivo' => $array['dsCaminhoArquivo']
        );
        $id = $modelMensagem->setMensagemItem($dados);
    }    
    public function getLocal() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $url = explode("=", $_SERVER['REQUEST_URI']);
        $key = str_replace('+', ' ', $url[1]);
        if (!empty($key)) {
          $busca = trim($key);
          $model = new localreuniaoModel();
          $where = "(UPPER(dsLocalReuniao) like UPPER('%{$key}%'))";
          $retorno = $model->getLocalReuniao($where);
          $return = array();
          if (count($retorno)) {
            $row = array();
            for ($i = 0; $i < count($retorno); $i++) {
              $row['value'] = strtoupper($retorno[$i]["dsLocalReuniao"]);
              $row["id"] = $retorno[$i]["idLocalReuniao"];
              array_push($return, $row);
            }
          }
          echo json_encode($return);
        }
    }    
}

?>