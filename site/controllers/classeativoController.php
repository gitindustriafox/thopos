<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class classeativo extends controller {

    public function index_action() {
//die("chegou");
        //Inicializa o Template
        $this->template->run();

        $model = new classeativoModel();
        $classeativo_lista = $model->getClasseAtivo();

        $this->smarty->assign('classeativo_lista', $classeativo_lista);
        $this->smarty->display('classeativo/lista.html');
    }

//Funcao de Busca
    public function busca_classeativo() {
        //se nao existir o indice estou como paao '';
        $texto = isset($_POST['buscadescricao']) ? $_POST['buscadescricao'] : '';
        //$texto = '';
        $model = new classeativoModel();
        $sql = "stStatus <> 0 and upper(dsClasseAtivo) like upper('%" . $texto . "%')"; //somente os nao excluidos
        $resultado = $model->getClasseAtivo($sql);

        if (sizeof($resultado) > 0) {
            $this->smarty->assign('classeativo_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'classeativo');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('classeativo/lista.html');
        } else {
            $this->smarty->assign('classeativo_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'classeativo');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('classeativo/lista.html');
        }
    }

    //Funcao de Inserir
    public function novo_classeativo() {
        $sy = new system\System();

        $idClasseAtivo = $sy->getParam('idClasseAtivo');

        $model = new classeativoModel();

        if ($idClasseAtivo > 0) {

            $registro = $model->getClasseAtivo('idClasseAtivo=' . $idClasseAtivo);
            $registro = $registro[0]; //Passando ClasseAtivo
            $lista = $model->getItem('idClasseAtivo = ' . $idClasseAtivo);
        } else {
            //Novo Registro
            $lista = null;
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        $modelTipoAtivo = new tipoativoModel();
        $lista_tipoativo = array('' => 'SELECIONE');
        foreach ($modelTipoAtivo->getTipoAtivo() as $value) {
            $lista_tipoativo[$value['idTipoAtivo']] = $value['dsTipoAtivo'];
        }
        $this->smarty->assign('lista_tipoativo', $lista_tipoativo);            
        $this->smarty->assign('lista_itens',$lista);
        
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('title', 'Novo ClasseAtivo');
        $this->smarty->display('classeativo/form_novo.tpl');
    }

    // Gravar Paao
    public function gravar_classeativo() {
        $model = new classeativoModel();

        $data = $this->trataPost($_POST);

        if ($data['idClasseAtivo'] == NULL) {
          $id =  $model->setclasseativo($data);
        } else {
          $id = $data['idClasseAtivo'];
          $model->updclasseativo($data); //update
        }
        header('Location: /classeativo/novo_classeativo/idClasseAtivo/' . $id);        
        return;
    }

    public function adicionaritem() {
        $idClasseAtivo = $_POST['idClasseAtivo'];
        $dsGrupoAtivo = $_POST['dsGrupoAtivo'];
        $data = array(
            'idClasseAtivo' => $idClasseAtivo,
            'idGrupoAtivo' => null,
            'dsGrupoAtivo' => $dsGrupoAtivo
        );
        $modelG = new classeativoModel();
        $modelG->setItem($data);
        
        $lista = $modelG->getItem('idClasseAtivo = ' . $idClasseAtivo);
        $this->smarty->assign('lista_itens',$lista);
        $html = $this->smarty->fetch("classeativo/itens.html");
        $jsondata = array(
            'html' => $html
        );
        echo json_encode($jsondata);
        
    }
    
    public function delgrupo() {
        $model = new classeativoModel();        
        $idClasseAtivo = $_POST['idClasseAtivo'];
        $idGrupoAtivo = $_POST['idGrupoAtivo'];
//        var_dump($_POST); die;
        $ok = 'Item excluido';
//        $modelOrcamento = new orcamentoModel();
//        $where = 'idGrupoAtivo = ' . $idGrupoAtivo . ' and idClasseAtivo = ' . $idClasseAtivo;
//        $retorno = $modelOrcamento->getOrcamentoItem($where);
//        if ($retorno) {
//            $ok = 'Item não pode ser excluido, existe(m) orçamento(s) com este item';
//        } else {
            $where = 'idGrupoAtivo = ' . $idGrupoAtivo;
            $model->delItem($where);        
//        }
        
        $lista = $model->getItem('idClasseAtivo = ' . $idClasseAtivo);
        $this->smarty->assign('lista_itens',$lista);
        $html = $this->smarty->fetch("classeativo/itens.html");
        $jsondata = array(
            'html' => $html,
            'ok' => $ok
        );
        echo json_encode($jsondata);
        
    }
    
    //Trata dados antes de Enviar para o Gravar
    private function trataPost($post) {
        $data['idClasseAtivo'] = ($post['idClasseAtivo'] != '') ? $post['idClasseAtivo'] : null;
        $data['idTipoAtivo'] = ($post['idTipoAtivo'] != '') ? $post['idTipoAtivo'] : null;
        $data['dsClasseAtivo'] = ($post['dsClasseAtivo'] != '') ? $post['dsClasseAtivo'] : null;
        $data['vlTaxaDepreciacao'] = ($post['vlTaxaDepreciacao'] != '') ? str_replace(",",".",str_replace(".","",$post['vlTaxaDepreciacao'])) : null;
        return $data;
    }

    // Remove Paao
    public function delclasseativo() {
        $sy = new system\System();                
        $idClasseAtivo = $sy->getParam('idClasseAtivo');        
//        $modelOrcamento = new orcamentoModel();
//        $where = 'idClasseAtivo = ' . $idClasseAtivo;
//        $retorno = $modelOrcamento->getOrcamentoItem($where);
//        if (!$retorno) {
            if (!is_null($idClasseAtivo)) {    
                $model = new classeativoModel();
                $dados['idClasseAtivo'] = $idClasseAtivo;             
                $model->delClasseAtivo($dados);
            }
//        }    
        header('Location: /classeativo');
    }
}

?>