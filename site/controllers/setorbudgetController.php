<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class setorbudget extends controller {

    public function index_action() {
//die("chegou");
        //Inicializa o Template
        $this->template->run();

        $model = new setorbudgetModel();
        $setorbudget_lista = $model->getSetor();

        $this->smarty->assign('setorbudget_lista', $setorbudget_lista);
        $this->smarty->display('setorbudget/lista.html');
    }

//Funcao de Busca
    public function busca_setorbudget() {
        //se nao existir o indice estou como padrao '';
        $texto = isset($_POST['buscadescricao']) ? $_POST['buscadescricao'] : '';
        //$texto = '';
        $model = new setorbudgetModel();
        $sql = "stStatus <> 0 and upper(dsSetorBudget) like upper('%" . $texto . "%')"; //somente os nao excluidos
        $resultado = $model->getSetorBudget($sql);

        if (sizeof($resultado) > 0) {
            $this->smarty->assign('setorbudget_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'setorbudget');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('setorbudget/lista.html');
        } else {
            $this->smarty->assign('setorbudget_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'setorbudget');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('setorbudget/lista.html');
        }
    }

    //Funcao de Inserir
    public function novo_setorbudget() {
        $sy = new system\System();

        $idSetor = $sy->getParam('idSetor');

        $model = new setorbudgetModel();

        if ($idSetor > 0) {

            $registro = $model->getSetor('idSetor=' . $idSetor);
            $registro = $registro[0]; //Passando SetorBudget
        } else {
            //Novo Registro
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        $lista = $model->getSetorBudget('s.idSetor = ' . $idSetor, 'sb.idAnoMes');
        $this->smarty->assign('lista_budget', $lista);
        
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('title', 'Nova SetorBudget');
        $this->smarty->display('setorbudget/form_novo.tpl');
    }
    
    public function adicionarbudget() {
        $dados = $this->trataPost($_POST);
        
        if (!$dados['idSetorBudget']) {
            if ($dados['replica'] == 'nao') {
                $data = array(
                    'idSetorBudget' => null,
                    'idSetor' => $dados['idSetor'],
                    'idAnoMes' => $dados['idAno'] . $dados['idMes'],
                    'vlBudget' => $dados['vlBudget']
                );
                $this->incluir($data);
            } else {
                $data = array(
                    'idSetorBudget' => null,
                    'idSetor' => $dados['idSetor'],
                    'vlBudget' => $dados['vlBudget']
                );
                for ($x=1;$x<13;$x++) {
                    $mes = $x;
                    if ($x < 9) {
                        $mes = '0' . $x;
                    }
                    $data['idAnoMes'] = $dados['idAno'] . $mes;
                    $this->incluir($data);
                }
            }            
        }
        
        $model = new setorbudgetModel();
        $registro = $model->getSetorBudget('s.idSetor = ' . $dados['idSetor'], 'sb.idAnoMes');
        $this->smarty->assign('lista_budget', $registro);
        $html = $this->smarty->fetch('setorbudget/budget.html');
        echo json_encode(array('html' => $html));
    }
    
    private function incluir ($array) {
        $model = new setorbudgetModel();        
        $model->setSetorBudget($array);                
    }

    //Trata dados antes de Enviar para o Gravar
    private function trataPost($post) {
        $data['idSetor'] = ($post['idSetor'] != '') ? $post['idSetor'] : null;
        $data['idSetorBudget'] = ($post['idSetorBudget'] != '') ? $post['idSetorBudget'] : null;
        $data['idAno'] = ($post['idAno'] != '') ? $post['idAno'] : null;
        $data['idMes'] = ($post['idMes'] != '') ? $post['idMes'] : null;
        $data['vlBudget'] = ($post['vlBudget'] != '') ? str_replace(",",".",str_replace(".","",$post['vlBudget'])) : null;
        $data['replica'] = ($post['replica'] != '') ? $post['replica'] : 'nao';
        return $data;
    }

    // Remove Padrao
    public function del_setorbudgest() {
        $idSetorBudget = $_POST['idSetorBudget'];
        $idSetor = $_POST['idSetor'];
        $model = new setorbudgetModel();
        
        if (!is_null($idSetorBudget)) {    
            $dados['idSetorBudget'] = $idSetorBudget;             
            $model->delSetorBudget($dados);
        }
        $registro = $model->getSetorBudget('s.idSetor = ' . $idSetor, 'sb.idAnoMes');
        $this->smarty->assign('lista_budget', $registro);
        $html = $this->smarty->fetch('setorbudget/budget.html');
        echo json_encode(array('html' => $html));
    }
    public function ativar() {
        $stBudget = $_POST['stBudget'];
        $idSetor = $_POST['idSetor'];
        $model = new setorModel();
        
        if (!is_null($idSetor)) { 
            if ($stBudget == '0') {
                $dados = array('idSetor' => $idSetor,
                    'stBudget' => 1);  
            } else {
                $dados = array('idSetor' => $idSetor,
                    'stBudget' => 0);  
            }
            $model->updSetor($dados);
        }
        echo json_encode(array());
    }
}

?>