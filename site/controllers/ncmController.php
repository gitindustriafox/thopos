<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class ncm extends controller {

    public function index_action() {

        //Inicializa o Template
        $this->template->run();

        $model = new ncmModel();
        $ncm_lista = $model->getNCM(null,true);

        $this->smarty->assign('ncm_lista', $ncm_lista);
        $this->smarty->display('ncm/lista.html');
    }

//Funcao de Busca
    public function busca_ncm() {
        //se nao existir o indice estou como padrao '';
        $texto = isset($_POST['buscadescricao']) ? $_POST['buscadescricao'] : '';
        //$texto = '';
        $model = new ncmModel();
        $sql = "upper(dsNCM) like upper('%" . $texto . "%')"; //somente os nao excluidos
        $resultado = $model->getNCM($sql, true);

        if (sizeof($resultado) > 0) {
            $this->smarty->assign('ncm_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'ncm');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('ncm/lista.html');
        } else {
            $this->smarty->assign('ncm_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'ncm');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('ncm/lista.html');
        }
    }

    //Funcao de Inserir
    public function novo_ncm() {
        $sy = new system\System();

        $idNCM = $sy->getParam('idNCM');

        $model = new ncmModel();

        if ($idNCM > 0) {

            $registro = $model->getNCM('idNCM=' . $idNCM);
            $registro = $registro[0]; //Passando NCM
        } else {
            //Novo Registro
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        
        //criar uma lista
        //var_dump($lista_tipos_log);die;
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('title', 'Nova NCM');
        $this->smarty->display('ncm/form_novo.tpl');
    }

    // Gravar Padrao
    public function gravar_ncm() {
        $model = new ncmModel();

        $data = $this->trataPost($_POST);

        if ($data['idNCM'] == NULL)
            $model->setncm($data);
        else
            $model->updncm($data); //update
        
        header('Location: /ncm');        
        return;
    }

    //Trata dados antes de Enviar para o Gravar
    private function trataPost($post) {
        $data['idNCM'] = ($post['idNCM'] != '') ? $post['idNCM'] : null;
        $data['dsNCM'] = ($post['dsNCM'] != '') ? $post['dsNCM'] : null;
        $data['cdNCM'] = ($post['cdNCM'] != '') ? $post['cdNCM'] : null;
        return $data;
    }

    // Remove Padrao
    public function delncm() {
        $sy = new system\System();
                
        $idNCM = $sy->getParam('idNCM');
        
        $ncm = $idNCM;
        
        if (!is_null($ncm)) {    
            $model = new ncmModel();
            $dados['idNCM'] = $ncm;             
            $model->delNCM($dados);
        }

        header('Location: /ncm');
    }

    public function relatorioncm_pre() {
        $this->template->run();

        $this->smarty->assign('title', 'Pre Relatorio de Centro de Custos');
        $this->smarty->display('ncm/relatorio_pre.html');
    }

    public function relatorioncm() {
        $this->template->run();

        $model = new ncmModel();
        $ncm_lista = $model->getNCM();
        //Passa a lista de registros
        $this->smarty->assign('ncm_lista', $ncm_lista);
        $this->smarty->assign('titulo_relatorio');
        //Chama o Smarty
        $this->smarty->assign('title', 'Relatorio de Centro de Custos');
        $this->smarty->display('ncm/relatorio.html');
    }

}

?>