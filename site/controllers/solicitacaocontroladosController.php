<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */
use mensagem as mensagem;

class solicitacaocontrolados extends controller {

    private $onUm = 0;
    private $idPedido = 0;
    private $idOrcamentoCompras = 0;
    
    public function index_action() {
        //Inicializa o Template
        $this->template->run();
        unset($_SESSION['solicitacaocompras']['id']);
        
        $model = new solicitacaocomprasModel();
        $registro = $model->getSolicitacaoComprasManutencao('si.idSituacao < 4 and ins.stControlado = 1 and stAprovado = 0',false);
        $_SESSION['solicitacao']['where'] = 'si.idSituacao < 4 and i.stControlado = 1 and stAprovado = 0';

        $pModel = new prioridadeModel();        
        $lista_Prioridade = array('' => 'SELECIONE');
        foreach ($pModel->getPrioridade() as $value) {
            $lista_Prioridade[$value['idPrioridade']] = $value['dsPrioridade'];
        }
        
        $modelSituacao = new situacaoModel();
        $lista_situacao = array('' => 'SELECIONE');
        foreach ($modelSituacao->getSituacao() as $value) {
            $lista_situacao[$value['idSituacao']] = $value['dsSituacao'];
        }
        
        $this->smarty->assign('lista_prioridade', $lista_Prioridade);
        $this->smarty->assign('lista_situacao', $lista_situacao);
        $this->smarty->assign('solicitacaocompras', $registro);
        $this->smarty->assign('title', 'Solicitacao de Compras');
        $this->smarty->display('solicitacaocontrolados/listaManutencao.html');
        
    }

//Funcao de Busca
    
    public function busca_autorizar() {

        if (isset($_POST['selecionar'])) {
            $this->criarAutorizados($_POST['selecionar']);
        }
        
        header('Location: /solicitacaocontrolados');
    }
    
    public function busca_criarcsv() {

        if (isset($_POST['selecionar'])) {
            $dsParceiro = isset($_POST['dsParceiro']) ? $_POST['dsParceiro'] : '';
            $idParceiro = isset($_POST['idParceiro']) ? $_POST['idParceiro'] : '';
            $this->criarCSV($_POST['selecionar'], $idParceiro, $dsParceiro);
        }
     //   header('Location: /solicitacaocomprasmanutencao');
    }
    
    public function criarCSV($dados, $idParceiro, $dsParceiro) {
        
        $limit = 30000;
        $offset = 0;

        global $PATH;
        $caminho = "/var/www/html/thopos.com.br/site/storage/tmp/csv/";

        // Storage
        if (!is_dir($caminho)) {
          mkdir($caminho, 0777, true);
        }

        $filename = "{$idParceiro}_" . date("YmsHis") . ".csv";

        $headers[] = implode(";", array(
          "\"SOLICITANTE\"",
          "\"PARCEIRO\"",
          "\"PRODUTO\"",
          "\"QTE\"",
          "\"UNIDADE\"",
          "\"PRIORIDADE\""
        ));

        if (file_exists("{$caminho}" . '/' . "{$filename}")) {
          unlink("{$caminho}" . '/' . "{$filename}");
        }

        // Arquivo
        $handle = fopen("{$caminho}" . '/' . "{$filename}", 'w+');
        fwrite($handle, implode(";" . PHP_EOL, $headers));
        fwrite($handle, ";" . PHP_EOL);
        fflush($handle);

        // Fecha o arquivo da $_SESSION para liberar o servidor para servir outras requisições
        session_write_close();

        $output = array();
        $modelsc = new solicitacaocomprasModel();
        foreach ($dados as $key => $value) {
            $idSolicitacao = $key;
            foreach ($dados[$key] as $idSolicitacaoItem => $value1) {
                $dadosret = $modelsc->getSolicitacaoComprasItens('a.idSolicitacao = ' . $idSolicitacao . ' and a.idSolicitacaoItem = ' . $idSolicitacaoItem);
                $value = $dadosret[0];
                $valueitem = array();
                if ($value["dsParceiro"]) {
                    $valueitem['dsParceiro'] = $value["dsParceiro"];
                } else {
                    $valueitem['dsParceiro'] = $value["dsParceiroSugerido"];
                }
                $valueitem['qtSolicitacao'] = $value['qtSolicitacao'];
                if ($value['dsServico']) {
                    $valueitem['dsInsumo'] = $value['dsServico'];
                } else {
                    $valueitem['dsInsumo'] = $value['dsProduto'];
                }
                $valueitem['dsUnidade'] = $value['dsUnidade'];

                $output[] = implode(";", array(
                  "\"{$value["dsSolicitante"]}\"",
                  "\"{$valueitem["dsParceiro"]}\"",
                  "\"{$valueitem["dsInsumo"]}\"",
                  "\"{$valueitem["qtSolicitacao"]}\"",
                  "\"{$valueitem["dsUnidade"]}\"",
                  "\"{$value["dsPrioridade"]}\""
                ));
            }
        }
        fwrite($handle, implode(";" . PHP_EOL, $output));
        fwrite($handle, ";" . PHP_EOL);
        fflush($handle);
        fclose($handle);
        $this->download($caminho . '/' . $filename, 'CSV', $filename);        
    }

    private function download($nome, $tipo, $filename) {
      if (!empty($nome)) {
        if (file_exists($nome)) {
          header('Content-Transfer-Encoding: binary'); // For Gecko browsers mainly
          header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($nome)) . ' GMT');
          header('Accept-Ranges: bytes'); // For download resume
          header('Content-Length: ' . filesize($nome)); // File size
          header('Content-Encoding: none');
          header("Content-Type: application/{$tipo}"); // Change this mime type if the file is not PDF
          header('Content-Disposition: attachment; filename=' . $filename);
          // Make the browser display the Save As dialog
          readfile($nome);
          unlink($nome);
        }
      }
    }
    
    public function busca_controlados() {
        
        //se nao existir o indice estou como padrao '';
        $dsSolicitante = isset($_POST['dsSolicitante']) ? $_POST['dsSolicitante'] : '';
        $dsProduto = isset($_POST['dsProduto']) ? $_POST['dsProduto'] : '';
        $cdInsumo = isset($_POST['cdInsumo']) ? $_POST['cdInsumo'] : '';
        $dsParceiro = isset($_POST['dsParceiro']) ? $_POST['dsParceiro'] : '';
        $idParceiro = isset($_POST['idParceiro']) ? $_POST['idParceiro'] : '';
        $dsObservacao = isset($_POST['dsObservacao']) ? $_POST['dsObservacao'] : '';
        $idSituacao = isset($_POST['idSituacao']) ? $_POST['idSituacao'] : '';
        $idPrioridade = isset($_POST['idPrioridade']) ? $_POST['idPrioridade'] : '';
        $dsLocalEntrega = isset($_POST['dsLocalEntrega']) ? $_POST['dsLocalEntrega'] : '';
        $dsOrgaoControlador = isset($_POST['dsOrgaoControlador']) ? $_POST['dsOrgaoControlador'] : '';
        //$texto = '';
        $model = new solicitacaocomprasModel();
        
        $pModel = new prioridadeModel();        
        $lista_Prioridade = array('' => 'SELECIONE');
        foreach ($pModel->getPrioridade() as $value) {
            $lista_Prioridade[$value['idPrioridade']] = $value['dsPrioridade'];
        }
        
        $modelSituacao = new situacaoModel();
        $lista_situacao = array('' => 'SELECIONE');
        foreach ($modelSituacao->getSituacao() as $value) {
            $lista_situacao[$value['idSituacao']] = $value['dsSituacao'];
        }
        
        $busca = array();
        $sql = 'a.idSolicitacao > 0 and ins.stControlado = 1';
        if ($idSituacao) {
            $sql = $sql . " and si.idSituacao = " . $idSituacao;
            $busca['idSituacao'] = $idSituacao;
        }
        if ($idPrioridade) {
            $sql = $sql . " and a.idPrioridade = " . $idPrioridade;
            $busca['idPrioridade'] = $idPrioridade;
        }
        if ($dsProduto) {
            $sql = $sql . " and upper(i.dsProduto) like upper('%" . $dsProduto . "%')";
            $busca['dsProduto'] = $dsProduto;
        }
        if ($idParceiro) {
            if ($dsParceiro) {
                $sql = $sql . " and i.idParceiro = " . $idParceiro;
                $busca['idParceiro'] = $idParceiro;
                $busca['dsParceiro'] = $dsParceiro;
            } else {
                $busca['idParceiro'] = null;
            }
        } else {
        if ($dsParceiro) {
            $sql = $sql . " and upper(i.dsParceiroSugerido) like upper('%" . $dsParceiro . "%')";
            $busca['idParceiro'] = null;
            $busca['dsParceiro'] = $dsParceiro;
        }
        }
        if ($dsObservacao) {
            $sql = $sql . " and upper(a.dsObservacao) like upper('%" . $dsObservacao . "%')";
            $busca['dsObservacao'] = $dsObservacao;
        }
        if ($dsSolicitante) {
            $sql = $sql . " and upper(a.dsSolicitante) like upper('%" . $dsSolicitante . "%')";
            $busca['dsSolicitante'] = $dsSolicitante;
        }
        if ($dsLocalEntrega) {
            $sql = $sql . " and upper(a.dsLocalEntrega) like upper('%" . $dsLocalEntrega . "%')";
            $busca['dsLocalEntrega'] = $dsLocalEntrega;
        }
        if ($cdInsumo) {
            $sql = $sql . " and upper(ins.cdInsumo) like upper('%" . $cdInsumo . "%')";
            $busca['cdInsumo'] = $cdInsumo;
        }
        if ($dsOrgaoControlador) {
            $sql = $sql . " and upper(ins.dsOrgaoControlador) like upper('%" . $dsOrgaoControlador . "%')";
            $busca['dsOrgaoControlador'] = $dsOrgaoControlador;
        }

        $cpagina = null;
        if (isset($_POST['cpagina'])) { 
            $cpagina = true;
            $busca['cpagina'] = 1;
        }
        
        $_SESSION['solicitacao']['where'] = $sql;
        $resultado = $model->getSolicitacaoComprasManutencao($sql, $cpagina);

        if (sizeof($resultado) > 0) {
            
            $this->smarty->assign('lista_prioridade', $lista_Prioridade);
            $this->smarty->assign('lista_situacao', $lista_situacao);            
            $this->smarty->assign('solicitacaocompras', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'solicitacaocompras');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('solicitacaocontrolados/listaManutencao.html');
        } else {
            $this->smarty->assign('lista_prioridade', $lista_Prioridade);
            $this->smarty->assign('lista_situacao', $lista_situacao);            
            $this->smarty->assign('solicitacaocompras', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'solicitacaocompras');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('solicitacaocontrolados/listaManutencao.html');
        }
    }

    public function criarAutorizados($dados) {
        foreach ($dados as $key => $value) {
            $solicitacao = $key;
            foreach ($dados[$key] as $key1 => $value1) {
                $this->gravarAutorizados($solicitacao,$key1);
            }
        }
    }

    private function gravarAutorizados($idSolicitacao, $idSolicitacaoItem) {    
        
        $modelsc = new solicitacaocomprasModel();
        $value=array('stAprovado' => 1);
        $where = 'idSolicitacao = ' . $idSolicitacao . ' and idSolicitacaoItem = ' . $idSolicitacaoItem;
        $modelsc->updSolicitacaoComprasItemManut($value, $where);
    }
}

?>