<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class pedidofechado extends controller {

    public function index_action() {
        //Inicializa o Template
        $this->template->run();
        
        $model = new pedidoModel();
        $registro = $model->getPedido('a.idSituacaoPedido = 2', $paginacao=true);
        $_SESSION['buscar_pedidos']['sql'] = 'a.idSituacaoPedido = 2';
        $modelSituacaoPedido = new situacaoPedidoModel();
        $lista_situacao = array('' => 'SELECIONE');
        foreach ($modelSituacaoPedido->getSituacaoPedido() as $value) {
            if ($value['idSituacaoPedido'] > 2) {
                $lista_situacao[$value['idSituacaoPedido']] = $value['dsSituacaoPedido'];
            }
        }
        $modelempresa = new empresaModel();
        $lista_empresa = array('' => 'SELECIONE');
        foreach ($modelempresa->getEmpresa() as $value) {
            $lista_empresa[$value['idEmpresa']] = $value['dsEmpresa'];
        }
        $modelCentroCusto = new centrocustoModel();
        $lista_centrocusto = array('' => 'SELECIONE');
        foreach ($modelCentroCusto->getCentroCustoCombo() as $value) {
            $lista_centrocusto[$value['idCentroCusto']] = $value['codigocusto'];
        }
        $modelGrupoDR = new grupodrModel();
        $lista_grupodr = array('' => 'SELECIONE');
        foreach ($modelGrupoDR->getGrupoDR() as $value) {
            $lista_grupodr[$value['idGrupoDR']] = $value['dsGrupoDR'];
        }
        $this->smarty->assign('lista_centrocusto', $lista_centrocusto);
        $this->smarty->assign('lista_grupodr', $lista_grupodr);
        $this->smarty->assign('lista_empresa', $lista_empresa);
        $this->smarty->assign('lista_itemdr', null);
        
        $this->smarty->assign('lista_situacao', $lista_situacao);
        $this->smarty->assign('pedidos', $registro);
        $this->smarty->assign('title', 'Pedidos de Compra em Aberto');
        $this->smarty->display('pedido/pedidofechado.html');
        
    }
    
    public function busca_pedido() {
        //se nao existir o indice estou como padrao '';
        $idPedido = isset($_POST['idPedido']) ? $_POST['idPedido'] : '';
        $dsSolicitante = isset($_POST['dsSolicitante']) ? $_POST['dsSolicitante'] : '';
        $dsProduto = isset($_POST['dsProduto']) ? $_POST['dsProduto'] : '';
        $dsParceiro = isset($_POST['dsParceiro']) ? $_POST['dsParceiro'] : '';
        $dsObservacao = isset($_POST['dsObservacao']) ? $_POST['dsObservacao'] : '';
        $idSituacaoPedido = isset($_POST['idSituacaoPedido']) ? $_POST['idSituacaoPedido'] : '';
        $idCentroCusto = isset($_POST['idCentroCusto']) ? $_POST['idCentroCusto'] : '';
        $idEmpresa = isset($_POST['idEmpresa']) ? $_POST['idEmpresa'] : '';
        $idGrupoDR = isset($_POST['idGrupoDR']) ? $_POST['idGrupoDR'] : '';
        $idItemDR = isset($_POST['idItemDR']) ? $_POST['idItemDR'] : '';
        $dtInicio = isset($_POST['dtInicio']) ? $_POST['dtInicio'] : '';
        $dtFim = isset($_POST['dtFim']) ? $_POST['dtFim'] : '';

        $model = new pedidoModel();

        $modelSituacaoPedido = new situacaoPedidoModel();
        $lista_situacao = array('' => 'SELECIONE');
        foreach ($modelSituacaoPedido->getSituacaoPedido() as $value) {
            if ($value['idSituacaoPedido'] > 2) {
                $lista_situacao[$value['idSituacaoPedido']] = $value['dsSituacaoPedido'];
            }
        }

        $modelempresa = new empresaModel();
        $lista_empresa = array('' => 'SELECIONE');
        foreach ($modelempresa->getEmpresa() as $value) {
            $lista_empresa[$value['idEmpresa']] = $value['dsEmpresa'];
        }
        $modelCentroCusto = new centrocustoModel();
        $lista_centrocusto = array('' => 'SELECIONE');
        foreach ($modelCentroCusto->getCentroCustoCombo() as $value) {
            $lista_centrocusto[$value['idCentroCusto']] = $value['codigocusto'];
        }
        $modelGrupoDR = new grupodrModel();
        $lista_grupodr = array('' => 'SELECIONE');
        foreach ($modelGrupoDR->getGrupoDR() as $value) {
            $lista_grupodr[$value['idGrupoDR']] = $value['dsGrupoDR'];
        }
        
        $busca = array();
        $sql = 'a.idPedido > 0';
        if ($idPedido) {
            $sql = $sql . " and a.idPedido = " . $idPedido;
            $busca['idPedido'] = $idPedido;
        }
        if ($idSituacaoPedido) {
            $sql = $sql . " and a.idSituacaoPedido = " . $idSituacaoPedido;
            $busca['idSituacaoPedido'] = $idSituacaoPedido;
        }
        if ($idGrupoDR) {
            $sql = $sql . " and si.idGrupoDR = " . $idGrupoDR;
            $busca['idGrupoDR'] = $idGrupoDR;
        }
        if ($idItemDR) {
            $sql = $sql . " and si.idItemDR = " . $idItemDR;
            $busca['idItemDR'] = $idItemDR;
        }
        if ($idEmpresa) {
            $sql = $sql . " and emp.idEmpresa = " . $idEmpresa;
            $busca['idEmpresa'] = $idEmpresa;
        }
        if ($idCentroCusto) {
            $sql = $sql . " and si.idCentroCusto = " . $idCentroCusto;
            $busca['idCentroCusto'] = $idCentroCusto;
        }
        if ($dsProduto) {
            $sql = $sql . " and upper(prod.dsInsumo) like upper('%" . $dsProduto . "%')";
            $busca['dsProduto'] = $dsProduto;
        }
        if ($dsParceiro) {
            $sql = $sql . " and upper(m.dsParceiro) like upper('%" . $dsParceiro . "%')";
            $busca['dsParceiro'] = $dsParceiro;
        }
        if ($dsObservacao) {
            $sql = $sql . " and upper(a.dsObservacao) like upper('%" . $dsObservacao . "%')";
            $busca['dsObservacao'] = $dsObservacao;
        }
        if ($dsSolicitante) {
            $sql = $sql . " and upper(a.dsSolicitante) like upper('%" . $dsSolicitante . "%')";
            $busca['dsSolicitante'] = $dsSolicitante;
        }
        
        $cpagina = null;
        if (isset($_POST['cpagina'])) { 
            $cpagina = true;
            $busca['cpagina'] = 1;
        }
        
        if ($dtInicio && $dtFim) {
            $dtInicio = ($dtInicio != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $dtInicio))) : date('Y-m-d h:m:s');
            $dtFim = ($dtFim != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $dtFim))) : date('Y-m-d h:m:s');
            $sql = $sql . " and a.dtPedido >= '" . $dtInicio . " 00:00:00' and a.dtPedido <= '" . $dtFim . " 23:59:59'";        
        }
        
        $busca['dtInicio'] = $dtInicio;
        $busca['dtFim'] = $dtFim;
        
        $_SESSION['buscar_pedidos']['sql'] = $sql;
        $resultado = $model->getPedido($sql, $cpagina);

        $this->smarty->assign('lista_centrocusto', $lista_centrocusto);
        $this->smarty->assign('lista_grupodr', $lista_grupodr);
        $this->smarty->assign('lista_empresa', $lista_empresa);
        if ($idGrupoDR) {
            $this->smarty->assign('lista_itemdr', $this->carregaItemDR($idGrupoDR));
        } else {
            $this->smarty->assign('lista_itemdr', null);
        }
        
        if (sizeof($resultado) > 0) {
            $this->smarty->assign('lista_situacao', $lista_situacao);            
            $this->smarty->assign('pedidos', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'Pedidos atendidos');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('pedido/pedidofechado.html');
        } else {
            $this->smarty->assign('lista_situacao', $lista_situacao);            
            $this->smarty->assign('pedidos', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'Pedidos atendidos');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('pedido/pedidofechado.html');
        }
    }
    
    private function carregaItemDR($idGrupoDR) {
        
        $modelItemDR = new grupodrModel();
        $lista_itemdr = array('' => 'SELECIONE');
        foreach ($modelItemDR->getItemDr("i.idGrupoDR = " . $idGrupoDR . " and i.dsCanal = 'compras' and g.stGrupoDR = 'D'") as $value) {
            $lista_itemdr[$value['idItemDR']] = $value['dsItemDR'];
        }        
        return $lista_itemdr;
    }    
    
    public function busca_criarcsv() {
        $sql = $_SESSION['buscar_pedidos']['sql'];
        $model = new pedidoModel();
        $resultado = $model->getPedido($sql, false);

        if ($resultado) {
            $this->criarCSV($resultado);
        }
     //   header('Location: /solicitacaocomprasmanutencao');
    }
    
    public function criarCSV($resultado) {
        
        $limit = 30000;
        $offset = 0;

        global $PATH;
        $caminho = "/var/www/html/thopos.com.br/site/storage/tmp/csv/";

        // Storage
        if (!is_dir($caminho)) {
          mkdir($caminho, 0777, true);
        }

        $filename = "pedidosatendidos_" . date("YmsHis") . ".csv";

        $headers[] = implode(";", array(
          "\"EMPRESA\"",
          "\"CENTRO CUSTO\"",
          "\"GRUPO DESPESA\"",
          "\"ITEM DESPESA\"",
          "\"FORNECEDOR\"",
          "\"ID PEDIDO\"",
          "\"DATA PEDIDO\"",
          "\"PRIORIDADE\"",
          "\"STATUS PEDIDO\"",
          "\"PRODUTO\"",
          "\"QUANTIDADE\"",
          "\"VAL ITEM\""
        ));

        if (file_exists("{$caminho}" . '/' . "{$filename}")) {
          unlink("{$caminho}" . '/' . "{$filename}");
        }

        // Arquivo
        $handle = fopen("{$caminho}" . '/' . "{$filename}", 'w+');
        fwrite($handle, implode(";" . PHP_EOL, $headers));
        fwrite($handle, ";" . PHP_EOL);
        fflush($handle);

        // Fecha o arquivo da $_SESSION para liberar o servidor para servir outras requisições
        session_write_close();

        $output = array();
        foreach ($resultado as $value) {
            $output[] = implode(";", array(
              "\"{$value["dsEmpresa"]}\"",
              "\"{$value["dsCentroCusto"]}\"",
              "\"{$value["dsGrupoDR"]}\"",
              "\"{$value["dsItemDR"]}\"",
              "\"{$value["dsParceiro"]}\"",
              "\"{$value["idPedido"]}\"",
              "\"{$value["dtPedido"]}\"",
              "\"{$value["dsPrioridade"]}\"",
              "\"{$value["dsSituacaoPedido"]}\"",
              "\"{$value["dsInsumo"]}\"",
              "\"{$value["qtEntregue"]}\"",
              "\"{$value["vlTotalPedido"]}\""
            ));
        }
        fwrite($handle, implode(";" . PHP_EOL, $output));
        fwrite($handle, ";" . PHP_EOL);
        fflush($handle);
        fclose($handle);
        $this->download($caminho . '/' . $filename, 'CSV', $filename);        
    }

    private function download($nome, $tipo, $filename) {
      if (!empty($nome)) {
        if (file_exists($nome)) {
          header('Content-Transfer-Encoding: binary'); // For Gecko browsers mainly
          header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($nome)) . ' GMT');
          header('Accept-Ranges: bytes'); // For download resume
          header('Content-Length: ' . filesize($nome)); // File size
          header('Content-Encoding: none');
          header("Content-Type: application/{$tipo}"); // Change this mime type if the file is not PDF
          header('Content-Disposition: attachment; filename=' . $filename);
          // Make the browser display the Save As dialog
          readfile($nome);
          unlink($nome);
        }
      }
    }    
        
}

?>