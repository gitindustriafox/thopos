<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class lancardespesas extends controller {

    public function index_action() {
        $this->template->run();

        $model = new orcamentoModel();
        $orcamento_lista = $model->getOrcamentoLancar($_SESSION['user']['usuario']);
        $this->smarty->assign('orcamento_lista', $orcamento_lista);
        $this->smarty->display('orcamento/lancardespesas.html');
    }

//Funcao de Busca
    public function busca_orcamento() {
        //se nao existir o indice estou como padrao '';
        //$texto = '';
        $model = new orcamentoModel();
        $resultado = $model->getOrcamentoLancar($_SESSION['user']['usuario']);

        if (sizeof($resultado) > 0) {
            $this->smarty->assign('orcamento_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'orcamento');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('orcamento/lancardespesas.html');
        } else {
            $this->smarty->assign('orcamento_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'orcamento');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('orcamento/lancardespesas.html');
        }
    }

    //Funcao de Inserir
    public function fazerlancamento() {
        $sy = new system\System();
        $idOrcamento = $sy->getParam('idOrcamento');
        $idColaborador = $sy->getParam('idColaborador');

        $model = new orcamentoModel();

        if ($idOrcamento > 0) {
            $registro = $model->getOrcamento('idOrcamento=' . $idOrcamento);
            $registro = $registro[0]; //Passando Orcamento
        } else {
            //Novo Registro
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        
        $lista_cc = $this->carrega_cc($idOrcamento, $idColaborador);
        $this->smarty->assign('lista_cc',$lista_cc);
        $lista_grupodr = $this->carrega_grupodr();
        $this->smarty->assign('lista_grupodr',$lista_grupodr);
//        $lista_itemdr = $this->carrega_itemdr();
        $this->smarty->assign('lista_itemdr',null);
        
        $lista_orcamentocc = null;
        $lista_orcamentocolaborador = null;
        
        $totalorcamento = null;

        if(!$lista_orcamentocc) {
            $lista_orcamentocc = null;
        }
        if(!$lista_orcamentocolaborador) {
            $lista_orcamentocolaborador = null;
        }
        if ($idOrcamento) {
            $where = 'oc.idOrcamento = ' . $idOrcamento;
            $lista_orcamentocolaborador = $model->getOrcamentoColaboradores($where);        
        }
        $this->smarty->assign('lista_orcamentocolaborador', $lista_orcamentocolaborador);

        $this->smarty->assign('lista_orcamentocc', $lista_orcamentocc);
        $this->smarty->assign('idColaborador', $idColaborador);
        
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('totalorcamento', $totalorcamento);
        $this->smarty->assign('title', 'Novo Orcamento');
        $this->smarty->display('orcamento/form_orcar.tpl');
    }

    public function ler_colaborador() {
        $idColaborador = $_POST['idColaborador'];
        $modelColaborador = new colaboradorModel();
        $where = 'a.idColaborador = ' . $idColaborador;
        $retorno = $modelColaborador->getColaborador($where);
        $jsondata = array();
        if ($retorno) {
            $jsondata["dsSetor"] = $retorno[0]['dsSetor'];
            $jsondata["dsCargo"] = $retorno[0]['dsCargo'];
            $jsondata["dsEmail"] = $retorno[0]['dsEmail'];
        }
        echo json_encode($jsondata);
    }
    public function lercentrocusto($idCentroCusto, $idColaborador, $idOrcamento) {
        $model = new orcamentoModel();
        $where = 'occ.idColaborador = ' . $idColaborador . ' and occ.idOrcamento = ' . $idOrcamento . ' and occ.idCentroCusto = ' . $idCentroCusto;
        $dados = $model->getOrcamentoColaboradoresCC($where);
        $retorno = array();
        if ($dados) {
           $retorno['id'] = $dados[0]['idOrcamentoColaboradorCC'];
           $retorno['stSituacao'] = $dados[0]['stSituacao'];
        } else {
           $retorno['id'] = null;
           $retorno['stSituacao'] = 0;
        }
        return $retorno;
    }
    private function carrega_cc($idOrcamento, $idColaborador) {
        $lista_cc = null;
        if ($idOrcamento && $idColaborador) {
            $whereCC = ' c.idCentroCusto in (select idCentroCusto from prodOrcamentoColaboradoresCC where idOrcamento = ' . $idOrcamento . ' and idColaborador = ' . $idColaborador . ')';
            $modelCC = new centrocustoModel();
            $lista_cc = array('' => 'SELECIONE');
            foreach ($modelCC->getCentroCustoCombo($whereCC) as $value) {
                $lista_cc[$value['idCentroCusto']] =  $value['cdCentroCusto'] . '-' . $value['dsCentroCusto'];
            }
        }
        return $lista_cc;
    }
    private function carrega_grupodr() {
        $model = new grupodrModel();
        $lista = array('' => 'SELECIONE');
        foreach ($model->getGrupoDR() as $value) {
            $lista[$value['idGrupoDR']] =   $value['dsGrupoDR'];
        }
        
        return $lista;
    }
    public function carrega_itemdr() {
        $idGrupoDR = $_POST['idGrupoDR'];
        $idCentroCusto = $_POST['idCentroCusto'];
        $idOrcamento = $_POST['idOrcamento'];
        $idColaborador = $_POST['idColaborador'];
        
        $model = new grupodrModel();
        $lista = array('' => 'SELECIONE');
        foreach ($model->getItemDR('i.idGrupoDR = ' . $idGrupoDR) as $value) {
            $lista[$value['idItemDR']] =   $value['dsItemDR'];
        }
        
        $this->smarty->assign('lista_itemdr', $lista);    
        $html = $this->smarty->fetch("orcamento/form_listaitemdr.tpl");
        $html1 = null;
        if ($idCentroCusto) {
            $model = new orcamentoModel();            
            $retorno = $this->lercentrocusto($idCentroCusto, $idColaborador, $idOrcamento);
            $id = $retorno['id'];
            $where = 'l.idOrcamentoColaboradorCC = ' . $id . ' and l.idGrupoDR = ' . $idGrupoDR;
            $lista_orcamentocc = $model->getOrcamentoLancamento($where);        

            $this->smarty->assign('lista_orcamentocc', $lista_orcamentocc);    
            $html1 = $this->smarty->fetch("orcamento/orcar.html");
        }
        
        // criar o array de retorno
        $jasonretorno = array(
            'html' => $html,
            'html1' => $html1,
        );
        echo json_encode($jasonretorno);        
    }
    
    public function carregarOItensDR() {
        $idGrupoDR = $_POST['idGrupoDR'];
        $idItemDR = $_POST['idItemDR'];
        $idCentroCusto = $_POST['idCentroCusto'];
        $idOrcamento = $_POST['idOrcamento'];
        $idColaborador = $_POST['idColaborador'];
        $html = null;
        if ($idCentroCusto) {
            $model = new orcamentoModel();            
            $retorno = $this->lercentrocusto($idCentroCusto, $idColaborador, $idOrcamento);
            $id = $retorno['id'];
            $where = 'l.idOrcamentoColaboradorCC = ' . $id . ' and l.idGrupoDR = ' . $idGrupoDR  . ' and l.idItemDR = ' . $idItemDR;
            $lista_orcamentocc = $model->getOrcamentoLancamento($where);        

            $this->smarty->assign('lista_orcamentocc', $lista_orcamentocc);    
            $html = $this->smarty->fetch("orcamento/orcar.html");
        }
        
        // criar o array de retorno
        $jasonretorno = array(
            'html' => $html,
        );
        echo json_encode($jasonretorno);        
    }
    
    public function adicionarItemorcado() {
        $model = new orcamentoModel();
        
        $encerra = $_POST['encerra'];
        $idCentroCusto = $_POST['idCentroCusto'];
        $idOrcamento = $_POST['idOrcamento'];
        $idColaborador = $_POST['idColaborador'];
        if ($encerra == 'sim') {
            $id = $this->encerrarCC($idOrcamento, $idColaborador, $idCentroCusto);
        } else {
            $idGrupoDR = $_POST['idGrupoDR'];
            $idItemDR = $_POST['idItemDR'];
            $qtOrcado = ($_POST['qtOrcado'] != '') ? str_replace(",",".",str_replace(".","",$_POST['qtOrcado'])) : null;
            $vlOrcado = ($_POST['vlOrcado'] != '') ? str_replace(",",".",str_replace(".","",$_POST['vlOrcado'])) : null;
            $dtValorOrcado = ($_POST['dtValorOrcado'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $_POST["dtValorOrcado"]))) : date('Y-m-d h:m:s');
            $dtInicioVigencia = ($_POST['dtInicioVigencia'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $_POST["dtInicioVigencia"]))) : date('Y-m-d h:m:s');
            $dtFimVigencia = ($_POST['dtFimVigencia'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $_POST["dtFimVigencia"]))) : date('Y-m-d h:m:s');
            $dsValorOrcado = $_POST['dsValorOrcado'];
            $replica = $_POST['replica'];

            // atualiza o status para em planejamento

            $data = array(
                'idOrcamento' => $idOrcamento,
                'idStatusOrcamento' => 2
                );
            $model->updOrcamento($data);
            
            $retorno = $this->lercentrocusto($idCentroCusto, $idColaborador, $idOrcamento);
            $id = $retorno['id'];
            if ($id) {
                $data = array(
                    'idLancamento' => null,
                    'idOrcamentoColaboradorCC' => $id,
                    'idGrupoDR' => $idGrupoDR,
                    'idItemDR' => $idItemDR,
                    'qtOrcado' => $qtOrcado,
                    'vlOrcado' => $vlOrcado,
                    'dtValorOrcado' => $dtValorOrcado,
                    'dsValorOrcado' => $dsValorOrcado
                );
                $this->incluir($data);
            }

            if ($replica == 'sim') {
                $dtOrcado = $dtValorOrcado;
                for ($x=1;$x<50;$x++) {
                    $dtOrcado = date('Y-m-d', strtotime('+1 months', strtotime($dtOrcado)));
                    if ($dtOrcado <= $dtFimVigencia) {
                        $data['dtValorOrcado'] =$dtOrcado;
                        $this->incluir($data);
                    } else {
                        break;
                    }
                }
            }
        }
        $where = 'l.idOrcamentoColaboradorCC = ' . $id;
        $lista_orcamentocc = $model->getOrcamentoLancamento($where);        
        
        $this->smarty->assign('lista_orcamentocc', $lista_orcamentocc);    
        $html = $this->smarty->fetch("orcamento/orcar.html");

        // criar o array de retorno
        $jasonretorno = array(
            'html' => $html
        );
        echo json_encode($jasonretorno);        
    }
    
    private function incluir ($array) {
        $model = new orcamentoModel();        
        $model->setOrcamentoOrcado($array);                
    }
    
    private function encerrarCC ($idOrcamento, $idColaborador, $idCentroCusto) {
        
        $model = new orcamentoModel();        
        $retorno = $this->lercentrocusto($idCentroCusto, $idColaborador, $idOrcamento);
        $id = $retorno['id'];
        $campos = array('stSituacao' => 1);
        $model->updOrcamentoCC($campos, $id);
        return $id;        
    }
    
    public function carrega_lancamentos() {
        $idCentroCusto = $_POST['idCentroCusto'];
        $idOrcamento = $_POST['idOrcamento'];
        $idColaborador = $_POST['idColaborador'];

        $model = new orcamentoModel();
        
        $retorno = $this->lercentrocusto($idCentroCusto, $idColaborador, $idOrcamento);
        $id = $retorno['id'];
        $where = 'l.idOrcamentoColaboradorCC = ' . $id;
        $lista_orcamentocc = $model->getOrcamentoLancamento($where);        
        $this->smarty->assign('lista_orcamentocc', $lista_orcamentocc);    
        $html = $this->smarty->fetch("orcamento/orcar.html");
        if ($retorno['stSituacao'] == 0) {
            $encerrado = 'nao';
        } else {
        if ($retorno['stSituacao'] == 1) {
            $encerrado = 'sim';
        } else {
            $encerrado = 'aprovado';
        }
        }
        
        // criar o array de retorno
        $jasonretorno = array(
            'html' => $html,
            'encerrado' => $encerrado
        );
        echo json_encode($jasonretorno);        
    }
    
    public function del_lancamento() {
        $idCentroCusto = $_POST['idCentroCusto'];
        $idOrcamento = $_POST['idOrcamento'];
        $idColaborador = $_POST['idColaborador'];
        $idLancamento = $_POST['idLancamento'];

        $model = new orcamentoModel();
        $retorno = $this->lercentrocusto($idCentroCusto, $idColaborador, $idOrcamento);
        $id = $retorno['id'];
        
        $where = 'idLancamento = ' . $idLancamento;
        $model->delOrcamentoLancamento($where);        
        
        $where = 'l.idOrcamentoColaboradorCC = ' . $id;
        $lista_orcamentocc = $model->getOrcamentoLancamento($where);        
        $this->smarty->assign('lista_orcamentocc', $lista_orcamentocc);    
        $html = $this->smarty->fetch("orcamento/orcar.html");

        // criar o array de retorno
        $jasonretorno = array(
            'html' => $html
        );
        echo json_encode($jasonretorno);        
    }
    
    public function vercentrocusto() {
        $sy = new system\System();
        $idColaborador = $_POST['idColaborador'];
        $idOrcamento = $_POST['idOrcamento'];
        $model = new orcamentoModel();
        $where = 'occ.idOrcamento = ' . $idOrcamento . ' and occ.idColaborador = ' . $idColaborador;
        $lista_orcamentocc = $model->getOrcamentoColaboradoresCC($where);        

        $lista_cc = $this->carrega_cc($idOrcamento);
        $this->smarty->assign('lista_cc',$lista_cc);
        
        $this->smarty->assign('lista_orcamentocc', $lista_orcamentocc);    
        $html = $this->smarty->fetch("orcamento/orcamentocentrocusto.html");

        // criar o array de retorno
        $jasonretorno = array(
            'html' => $html
        );
        echo json_encode($jasonretorno);        
    }    
}

?>