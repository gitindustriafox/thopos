<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */
// use controllers\agenda\EnviarEmail;

class agenda extends controller {
    private $pastaPedidos = PEDIDOS;
    private $opcao = null;
    private $voltarmenu = null;
    public function index_action() {
        //Inicializa o Template
        $this->template->run();
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }

        $model = new agendaModel();
        $sql = 'r.idAgenda > 0 and r.stSituacao = 0 and r.idUsuarioSolicitante = ' . $_SESSION['user']['usuario'];
        
        $agenda_lista = $model->getAgenda($sql);
        
        $this->smarty->assign('lista_tipo', $this->carregaTipoAgenda());          
        $this->smarty->assign('lista_local', $this->carregaLocalAgenda());   

        $listaS = array('' => 'SELECIONE', '0' => 'AGENDADA', '1' => 'ADIADA', '2' => 'CANCELADA', '3' => 'ARQUIVADA', '4' => 'REALIZADA');
        $this->smarty->assign('opcao', $this->opcao);            
        $this->smarty->assign('lista_status', $listaS);            
                
        $this->smarty->assign('agenda_lista', $agenda_lista);
        $this->smarty->display('agenda/lista.html');
    }

    public function ata() {
        //Inicializa o Template
        $this->template->run();
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $sy = new system\System();
        $this->opcao = $sy->getParam('opcao');

        $model = new agendaModel();
        $sql = 'r.idAgenda > 0 and r.stSituacao < 2 and r.idUsuarioSolicitante = ' . $_SESSION['user']['usuario'];
        $this->opcao = isset($_POST['opcao']) ? $_POST['opcao'] : '';
        
        $agenda_lista = $model->getAgenda($sql);
        
        $this->smarty->assign('lista_tipo', $this->carregaTipoAgenda());          
        $this->smarty->assign('lista_local', $this->carregaLocalAgenda());   
        $listaS = array('' => 'SELECIONE', '0' => 'AGENDADA', '1' => 'ADIADA', '2' => 'CANCELADA', '3' => 'ARQUIVADA', '4' => 'REALIZADA');
        $this->smarty->assign('lista_status', $listaS);            

        $this->smarty->assign('opcao', $this->opcao);
        $this->smarty->assign('agenda_lista', $agenda_lista);
        $this->smarty->display('agenda/ata.html');
    }
    
    public function recebeEmail() {

        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $url = explode("/", $_GET['url']);
        $idAgenda = $url[4];        
        $idParticipante = $url[6];        
        $status = $url[8];            
        $model = new agendaModel();        
        $dados = array('idAgenda' => $idAgenda, 'idParticipante' => $idParticipante, 'stRespostaEmail' => $status);        
        $model->updAgendaParticipantes($dados);        
    }

    private function finaliza() {
        header('Location: /login');            
        die;            
    }
    
    public function minhastarefas() {
        //Inicializa o Template
        $this->template->run();
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }

        $model = new agendaModel();
        $sql = 'P.idAgenda > 0 and P.stStatusTarefa = 0 and P.idUsuario = ' . $_SESSION['user']['usuario'];
        $x=0;
        $resultado = $model->getParticipantesTarefa($sql, 'P.dtPrazo');
        foreach ($resultado as $value) {
            $temoutrasdatas = $model->getDatas('idTarefa = ' . $value['idTarefa']);
            if ($temoutrasdatas) {
                $resultado[$x]['temOutrasDatas'] = 1;
            } else {
                $resultado[$x]['temOutrasDatas'] = 0;
            }
            $x++;
        }
        
        $this->smarty->assign('lista_statusTarefa', array('' => 'SELECIONE', '0' => 'ABERTA', '1' => 'CONCLUIDA'));            
        $this->smarty->assign('tarefas_lista', $resultado);
        //Chama o Smarty
        
        $this->smarty->assign('title', 'agenda');
        $this->smarty->assign('busca', null);
        $this->smarty->display('agenda/minhastarefas.html');
    }
    public function gestaotarefas() {
        //Inicializa o Template
        $this->template->run();

        $model = new agendaModel();
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $sql = 'P.idAgenda > 0 and r.idUsuarioSolicitante = ' . $_SESSION['user']['usuario'];
        
        $resultado = $model->getParticipantesTarefa($sql, 'ust.dsUsuario, P.dtPrazo');
        $x=0;
        foreach ($resultado as $value) {
            $temoutrasdatas = $model->getDatas('idTarefa = ' . $value['idTarefa']);
            if ($temoutrasdatas) {
                $resultado[$x]['temOutrasDatas'] = 1;
            } else {
                $resultado[$x]['temOutrasDatas'] = 0;
            }
            $x++;
        }        
        $_SESSION['sql']['gestaotarefas'] = $sql;
        $sql = $sql . ' and P.stStatusTarefa = 0 ';
        
        //Chama o Smarty
        $this->smarty->assign('lista_statusTarefa', array('' => 'SELECIONE', '0' => 'ABERTA', '1' => 'CONCLUIDA'));            
        $this->smarty->assign('lista_usuarioTarefa', $this->carregarusuariosTarefa(null));  
        $this->smarty->assign('tarefas_lista', $resultado);
        $this->smarty->assign('title', 'agenda');
        
        $this->smarty->assign('busca', null);
        $this->smarty->display('agenda/gestaotarefas.html');
    }
//Funcao de Busca
    public function busca_agenda() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        //se nao existir o indice estou como paao '';
        $dsAssunto = isset($_POST['dsAssunto']) ? $_POST['dsAssunto'] : '';
        $dtInicio = isset($_POST['dtInicio']) ? $_POST['dtInicio'] : '';
        $dtFim = isset($_POST['dtFim']) ? $_POST['dtFim'] : '';
        $idTipo = isset($_POST['idTipo']) ? $_POST['idTipo'] : '';
        $idLocal = isset($_POST['idLocal']) ? $_POST['idLocal'] : '';
        $idAgenda = isset($_POST['idAgenda']) ? $_POST['idAgenda'] : '';
        $stStatus = isset($_POST['stStatus']) ? $_POST['stStatus'] : '';
        $this->opcao = isset($_POST['opcao']) ? $_POST['opcao'] : '';
        $busca = array();
        $sql = 'r.idAgenda > 0 and r.stSituacao < 3 and r.idUsuarioSolicitante = ' . $_SESSION['user']['usuario'];
        $registro = null;
        if ($idTipo) {
            $sql = $sql . " and r.idTipoAgenda = " . $idTipo;
            $registro['idTipoAgenda'] = $idTipo;
        }
        if ($idLocal) {
            $sql = $sql . " and r.idLocalAgenda = " . $idLocal;
            $registro['idLocalAgenda'] = $idLocal;
        }
        if ($idAgenda) {
            $sql = $sql . " and r.idAgenda = " . $idAgenda;
            $busca['idAgenda'] = $idAgenda;
        }
        if ($dsAssunto) {
            $sql = $sql . " and upper(r.dsAssunto) like upper('%" . $dsAssunto . "%')";
            $busca['dsAssunto'] = $dsAssunto;
        }
        if ($stStatus) {
            $sql = $sql . " and r.stSituacao = " . $stStatus;
            $registro['stStatus'] = $stStatus;
        }
        
        $dtInicio = ($dtInicio != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $dtInicio))) : '';
        $dtFim = ($dtFim != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $dtFim))) : '';
        if ($dtInicio && $dtFim) {
            $sql = $sql . " and r.dtAgenda >= '" . $dtInicio . "' and r.dtAgenda <= '" . $dtFim . "'";  
//            $sql = $sql . " and r.dtAgenda >= '" . $dtInicio . " 00:00:00' and r.dtAgenda <= '" . $dtFim . " 23:59:59'";  
        }
        $busca['dtInicio'] = $dtInicio;
        $busca['dtFim'] = $dtFim;

        $model = new agendaModel();
        $resultado = $model->getAgenda($sql);

        $this->smarty->assign('lista_tipo', $this->carregaTipoAgenda());          
        $this->smarty->assign('lista_local', $this->carregaLocalAgenda());   

        $listaS = array('' => 'SELECIONE', '0' => 'AGENDADA', '1' => 'ADIADA', '2' => 'CANCELADA', '3' => 'ARQUIVADA', '4' => 'REALIZADA');
        $this->smarty->assign('lista_status', $listaS);            
        $this->smarty->assign('opcao', $this->opcao);            
                
        if (sizeof($resultado) > 0) {
            $this->smarty->assign('agenda_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'agenda');
            $this->smarty->assign('busca', $busca);
            $this->smarty->assign('registro', $registro);
            $this->smarty->display('agenda/lista.html');
        } else {
            $this->smarty->assign('agenda_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'agenda');
            $this->smarty->assign('busca', $busca);
            $this->smarty->assign('registro', $registro);
            $this->smarty->display('agenda/lista.html');
        }
    }

    public function busca_ata_agenda() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        //se nao existir o indice estou como paao '';
        $dsAssunto = isset($_POST['dsAssunto']) ? $_POST['dsAssunto'] : '';
        $dtInicio = isset($_POST['dtInicio']) ? $_POST['dtInicio'] : '';
        $dtFim = isset($_POST['dtFim']) ? $_POST['dtFim'] : '';
        $idTipo = isset($_POST['idTipo']) ? $_POST['idTipo'] : '';
        $idLocal = isset($_POST['idLocal']) ? $_POST['idLocal'] : '';
        $idAgenda = isset($_POST['idAgenda']) ? $_POST['idAgenda'] : '';
        $stStatus = isset($_POST['stStatus']) ? $_POST['stStatus'] : '';
        $busca = array();
        $sql = 'r.idAgenda > 0 and r.idUsuarioSolicitante = ' . $_SESSION['user']['usuario'];
        $registro = null;
        if ($idTipo) {
            $sql = $sql . " and r.idTipoAgenda = " . $idTipo;
            $registro['idTipoAgenda'] = $idTipo;
        }
        if ($idLocal) {
            $sql = $sql . " and r.idLocalAgenda = " . $idLocal;
            $registro['idLocalAgenda'] = $idLocal;
        }        
        if ($idAgenda) {
            $sql = $sql . " and r.idAgenda = " . $idAgenda;
            $busca['idAgenda'] = $idAgenda;
        }
        if ($dsAssunto) {
            $sql = $sql . " and upper(r.dsAssunto) like upper('%" . $dsAssunto . "%')";
            $busca['dsAssunto'] = $dsAssunto;
        }
        if ($stStatus) {
            $sql = $sql . " and r.stSituacao = " . $stStatus;
            $registro['stStatus'] = $stStatus;
        } else {
        if ($stStatus == 0) {
            $sql = $sql . " and r.stSituacao = 0";
            $registro['stStatus'] = $stStatus;
        }}
        
        $dtInicio = ($dtInicio != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $dtInicio))) : '';
        $dtFim = ($dtFim != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $dtFim))) : '';
        if ($dtInicio && $dtFim) {
            $sql = $sql . " and r.dtAgenda >= '" . $dtInicio . "' and r.dtAgenda <= '" . $dtFim . "'";  
//            $sql = $sql . " and r.dtAgenda >= '" . $dtInicio . " 00:00:00' and r.dtAgenda <= '" . $dtFim . " 23:59:59'";  
        }
        $busca['dtInicio'] = $dtInicio;
        $busca['dtFim'] = $dtFim;

        $model = new agendaModel();
        $resultado = $model->getAgenda($sql);

        $this->smarty->assign('lista_tipo', $this->carregaTipoAgenda());          
        $this->smarty->assign('lista_local', $this->carregaLocalAgenda());   
        
        $listaS = array('' => 'SELECIONE', '0' => 'AGENDADA', '1' => 'ADIADA', '2' => 'CANCELADA', '3' => 'ARQUIVADA', '4' => 'REALIZADA');
        $this->smarty->assign('lista_status', $listaS);            
        
        if (sizeof($resultado) > 0) {
            $this->smarty->assign('agenda_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'agenda');
            $this->smarty->assign('busca', $busca);
            $this->smarty->assign('registro', $registro);
            $this->smarty->display('agenda/ata.html');
        } else {
            $this->smarty->assign('agenda_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'agenda');
            $this->smarty->assign('busca', $busca);
            $this->smarty->assign('registro', $registro);
            $this->smarty->display('agenda/ata.html');
        }
    }
    public function busca_tarefas() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        //se nao existir o indice estou como paao '';
        $dtInicio = isset($_POST['dtInicio']) ? $_POST['dtInicio'] : '';
        $dtFim = isset($_POST['dtFim']) ? $_POST['dtFim'] : '';
        $idTarefa = isset($_POST['idTarefa']) ? $_POST['idTarefa'] : '';
        $stStatusTarefa = isset($_POST['stStatusTarefa']) ? $_POST['stStatusTarefa'] : '';
        $busca = array();
        $sql = 'P.idAgenda > 0 and P.idUsuario = ' . $_SESSION['user']['usuario'];
        $registro = null;
        if ($idTarefa) {
            $sql = $sql . " and P.idTarefa = " . $idTarefa;
            $busca['idTarefa'] = $idTarefa;
        }
        $dtInicio = ($dtInicio != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $dtInicio))) : '';
        $dtFim = ($dtFim != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $dtFim))) : '';
        $busca['dtInicio'] = $dtInicio;
        $busca['dtFim'] = $dtFim;

        if ($dtInicio && $dtFim) {
            $sql = $sql . " and P.dtPrazo >= '" . $dtInicio . "' and P.dtPrazo <= '" . $dtFim . "'";  
        }
        
        $_SESSION['sql']['gestaotarefas'] = $sql;
        
        if ($stStatusTarefa <> '') {
            $sql = $sql . " and P.stStatusTarefa = " . $stStatusTarefa;
            $busca['stStatusTarefa'] = $stStatusTarefa;
        }

        //Inicializa o Template
        $model = new agendaModel();
        $x=0;
        $resultado = $model->getParticipantesTarefa($sql, 'P.dtPrazo');
        foreach ($resultado as $value) {
            $temoutrasdatas = $model->getDatas('idTarefa = ' . $value['idTarefa']);
            if ($temoutrasdatas) {
                $resultado[$x]['temOutrasDatas'] = 1;
            } else {
                $resultado[$x]['temOutrasDatas'] = 0;
            }
            $x++;
        }
        
        $this->smarty->assign('lista_statusTarefa', array('' => 'SELECIONE', '0' => 'ABERTA', '1' => 'CONCLUIDA'));            
        $this->smarty->assign('tarefas_lista', $resultado);
        
        $this->smarty->assign('title', 'agenda');
        $this->smarty->assign('busca', $busca);
        $this->smarty->display('agenda/minhastarefas.html');            
    }
    
    public function busca_gestao_tarefas() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        //se nao existir o indice estou como paao '';
        $dtInicio = isset($_POST['dtInicio']) ? $_POST['dtInicio'] : '';
        $dtFim = isset($_POST['dtFim']) ? $_POST['dtFim'] : '';
        $idTarefa = isset($_POST['idTarefa']) ? $_POST['idTarefa'] : '';
        $dsPauta = isset($_POST['dsPauta']) ? $_POST['dsPauta'] : '';
        $idUsuario = isset($_POST['idUsuarioTarefa']) ? $_POST['idUsuarioTarefa'] : '';
        $stStatusTarefa = isset($_POST['stStatusTarefa']) ? $_POST['stStatusTarefa'] : '';
        $busca = array();
        $sql = 'P.idTarefa > 0 and r.idUsuarioSolicitante = ' . $_SESSION['user']['usuario'];
        $registro = null;
        if ($idTarefa) {
            $sql = $sql . " and P.idTarefa = " . $idTarefa;
            $busca['idTarefa'] = $idTarefa;
        }
        $this->smarty->assign('lista_usuarioTarefa', $this->carregarusuariosTarefa(null));  
        if ($idUsuario) {
            $sql = $sql . " and P.idUsuario = " . $idUsuario;
            $busca['idUsuarioTarefa'] = $idUsuario;
        }
        if ($stStatusTarefa <> '') {
            $sql = $sql . " and P.stStatusTarefa = " . $stStatusTarefa;
            $busca['stStatusTarefa'] = $stStatusTarefa;
        }
        if ($dsPauta <> '') {
            $sql = $sql . " and upper(P.dsTarefa) like '%" . strtoupper($dsPauta) . "%'";
            $busca['dsPauta'] = $dsPauta;
        }
        
        $dtInicio = ($dtInicio != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $dtInicio))) : '';
        $dtFim = ($dtFim != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $dtFim))) : '';
        if ($dtInicio && $dtFim) {
            $sql = $sql . " and P.dtPrazo >= '" . $dtInicio . "' and P.dtPrazo <= '" . $dtFim . "'";  
        }
        $busca['dtInicio'] = $dtInicio;
        $busca['dtFim'] = $dtFim;

        $model = new agendaModel();
        $resultado = $model->getParticipantesTarefa($sql, 'ust.dsUsuario, P.dtPrazo');
        $x=0;
        foreach ($resultado as $value) {
            $temoutrasdatas = $model->getDatas('idTarefa = ' . $value['idTarefa']);
            if ($temoutrasdatas) {
                $resultado[$x]['temOutrasDatas'] = 1;
            } else {
                $resultado[$x]['temOutrasDatas'] = 0;
            }
            $x++;
        }
        
        $this->smarty->assign('lista_statusTarefa', array('' => 'SELECIONE', '0' => 'ABERTA', '1' => 'CONCLUIDA'));            
        $_SESSION['sql']['gestaotarefas'] = $sql;
        
        if (sizeof($resultado) > 0) {
            $this->smarty->assign('tarefas_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'agenda');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('agenda/gestaotarefas.html');
        } else {
            $this->smarty->assign('tarefas_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'tarefas');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('agenda/gestaotarefas.html');
        }
    }
    //Funcao de Inserir
    public function novo_agenda() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $sy = new system\System();
        $this->opcao = $sy->getParam('opcao');
        $voltarMenu = false;
        $idAgenda = $sy->getParam('idAgenda');
        if ($idAgenda == 999999) {
            $idAgenda = null;
            $voltarMenu = true;
        }
        
        $model = new agendaModel();

        if ($idAgenda > 0) {
            $registro = $model->getAgenda('r.idAgenda=' . $idAgenda);
            $registro = $registro[0]; //Passando Agenda
            $lista = $model->getParticipantes('P.idAgenda = ' . $idAgenda);
        } else {
            //Novo Registro
            $lista = null;
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        $this->smarty->assign('lista_tipo', $this->carregaTipoAgenda());          
        $this->smarty->assign('voltarmenu', $voltarMenu);          

        $this->smarty->assign('lista_usuario', $this->carregarusuarios($idAgenda));  
        $this->smarty->assign('lista_itens',$lista);
        
        $this->smarty->assign('opcao', $this->opcao);
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('title', 'Novo Agenda');
        $this->smarty->display('agenda/form_novo.tpl');
    }
    
    //Funcao de participantes
    public function participantes() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $sy = new system\System();
        $voltarMenu = false;
        $idAgenda = $sy->getParam('idAgenda');
        $this->opcao = $sy->getParam('opcao');
        if ($idAgenda == 999999) {
            $idAgenda = null;
            $voltarMenu = true;
        }
        
        $model = new agendaModel();

        if ($idAgenda > 0) {

            $registro = $model->getAgenda('r.idAgenda=' . $idAgenda);
            $registro = $registro[0]; //Passando Agenda
            $lista = $model->getParticipantes('P.idAgenda = ' . $idAgenda);
        } else {
            //Novo Registro
            $lista = null;
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        $this->smarty->assign('lista_tipo', $this->carregaTipoAgenda());          
        $this->smarty->assign('voltarmenu', $voltarMenu);          
    //    $this->smarty->assign('lista_local', $this->carregaLocalAgenda());   

        $this->smarty->assign('lista_usuario', $this->carregarusuarios($idAgenda));  
        $this->smarty->assign('lista_itens',$lista);
        
        $this->smarty->assign('opcao', $this->opcao);
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('title', 'Novo Agenda');
        $this->smarty->display('agenda/form_novo.tpl');
    }

    public function acao() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $sy = new system\System();
        $this->opcao = $sy->getParam('opcao');
        $this->index_action();        
    }
    
    public function continua_agenda() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $sy = new system\System();
        $idAgenda = $sy->getParam('idAgenda');
        $this->opcao = $sy->getParam('opcao');
        $this->voltarmenu = $sy->getParam('voltarmenu');
        
        $model = new agendaModel();
        $registro = $model->getAgenda('r.idAgenda=' . $idAgenda);
        
        if ($registro) {
            $dados = array(
                'idAgenda' => null,
                'idTipoAgenda' => $registro[0]['idTipoAgenda'],
                'dsPauta' => $registro[0]['dsPauta'],
                'dsAssunto' => $registro[0]['dsAssunto'],
                'idUsuarioSolicitante' => $registro[0]['idUsuarioSolicitante'],
                'idAgendaPai' => $idAgenda,
                'stSituacao' => 0,
                'idLocalAgenda' => $registro[0]['idLocalAgenda'],
                'dsTempoDuracao' => $registro[0]['dsTempoDuracao'],
                'idAgendaAnterior' => $idAgenda
            );
            $idNovaAgenda = $model->setAgenda($dados);
            
            $participantes = $model->getParticipantes('P.idAgenda = ' . $idAgenda);
            foreach($participantes as $value) {
                $dados = array(
                    'idParticipante' => null,
                    'idAgenda' => $idNovaAgenda,
                    'idUsuario' => $value['idUsuario'],
                    'dsAssunto' => $value['dsAssunto'],
                    'idParceiro' => $value['idParceiro'],
                    'dsCaminhoAnexo' => $value['dsCaminhoAnexo'],
                    'stSituacao' => 0,
                    'dsEmail' => $value['dsEmail'],
                    'dsNome' => $value['dsNome'],
                    'dsCelular' => $value['dsCelular']
                );
                $model->setParticipantes($dados);
            }
        }
        
        header('Location: /agenda/novo_ata/idAgenda/' . $idNovaAgenda . '/opcao/' . $this->opcao . '/voltarmenu/' . $this->voltarmenu);                        
    }

    public function novo_ata() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $sy = new system\System();
        $idAgenda = $sy->getParam('idAgenda');
        $modelT = new agendaModel();
        $this->opcao = $sy->getParam('opcao');
        $this->voltarmenu = $sy->getParam('voltarmenu');

        if ($idAgenda == 999999) {
            // agenda nao agendada            
            $idAgenda = $this->naoAgendada();            
        }
        
        $registro = $modelT->getSoAgenda('r.idAgenda=' . $idAgenda);
        $registro = $registro[0]; //Passando Agenda
        if ($registro) {
            $ramificada = array();
            $ramificada[0]['idAgenda'] = $registro['idAgenda'];
            $ramificada[0]['dtAgenda'] = $registro['dtAgenda'];
            $ramificada[0]['idAgendaPai'] = $registro['idAgendaPai'];
            $ramificada[0]['dsLocalAgenda'] = $registro['dsLocalAgenda'];
            $ramificada[0]['dsPauta'] = $registro['dsAssunto'];
            $this->smarty->assign('agenda_listaRamificada', $this->verRamificacao($registro['idAgenda'], $registro['idAgendaPai'],$ramificada));
        }

        if ($registro['stSituacao'] == '4') {
            $this->smarty->assign('statusagenda','REUNIÃO REALIZADA');
        } else {
        if ($registro['stSituacao'] == '3') {
            $this->smarty->assign('statusagenda','REUNIÃO ARQUIVADA');
        } else {
        if ($registro['stSituacao'] == '2') {
            $this->smarty->assign('statusagenda','REUNIÃO CANCELADA');
        } else {
            $this->smarty->assign('statusagenda','REUNIÃO EM ANDAMENTO');
        }}}
        
        $lista = $modelT->getParticipantes('P.idAgenda = ' . $idAgenda);
        $this->smarty->assign('lista_tipo', $this->carregaTipoAgenda());          
        
        $this->smarty->assign('lista_usuario', $this->carregarusuarios($idAgenda));  
        $this->smarty->assign('lista_usuarioAta', $this->carregarusuariosAta());  
        $this->smarty->assign('lista_usuariotarefa', $this->carregarusuariosTarefa($idAgenda));  
        
        $this->smarty->assign('lista_itens',$lista);
        $listaUT = $modelT->getParticipantesTarefa('P.idAgenda = ' . $idAgenda);
        $this->smarty->assign('lista_itensTarefa',$listaUT);
        
        $this->smarty->assign('opcao', $this->opcao);
        $this->smarty->assign('voltarmenu', $this->voltarmenu);
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('title', 'Ata Agenda');
        $this->smarty->display('agenda/form_ata.tpl');
    }
    
    private function verRamificacao($idAgenda, $idAgendaPai,$ramificada) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $x = 1;
        $modelT = new agendaModel();
        if ($idAgendaPai) {
            for ($y=1;$y++;$y<50) {
                $retorno = $modelT->getSoAgenda('r.idAgenda = ' . $idAgendaPai);
              //  var_dump($retorno);
                if ($retorno) {
                    foreach($retorno as $value) {
                        $ramificada[$x]['idAgenda'] = $value['idAgenda'];
                        $ramificada[$x]['dtAgenda'] = $value['dtAgenda'];
                        $ramificada[$x]['idAgendaPai'] = $value['idAgendaPai'];
                        $ramificada[$x]['dsLocalAgenda'] = $value['dsLocalAgenda'];
                        $ramificada[$x]['dsPauta'] = $value['dsAssunto'];
                        $idAgendaPai = $value['idAgendaPai'];
                        $x++;
                    } 
                    if (!$idAgendaPai) {
                        break;
                    }                    
                } else {
                    break;
                }
            }
        }    
        //die;
        // VER SE TEM PARA FRENTE
        
        for ($y=1;$y++;$y<50) {
            $retorno = $modelT->getSoAgenda('r.idAgendaPai = ' . $idAgenda);
         //   var_dump($retorno); die;
            if ($retorno) {
                foreach($retorno as $value) {
                    $ramificada[$x]['idAgenda'] = $value['idAgenda'];
                    $ramificada[$x]['dtAgenda'] = $value['dtAgenda'];
                    $ramificada[$x]['idAgendaPai'] = $value['idAgendaPai'];
                    $ramificada[$x]['dsLocalAgenda'] = $value['dsLocalAgenda'];
                    $ramificada[$x]['dsPauta'] = $value['dsAssunto'];
                    $idAgenda = $value['idAgenda'];
                    $x++;
                } 
                if (!$idAgenda) {
                    break;
                }                    
            } else {
                break;
            }
        }
        
        foreach ($ramificada as $user) {
            $ids[] = $user['idAgenda'];
        }
        array_multisort($ids, SORT_ASC, $ramificada);
        
        return $ramificada;        
    }
    
    public function grafico() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $sql = $_SESSION['sql']['gestaotarefas'];
        $x=0;
        $model = new agendaModel();
        $registro = $model->getParticipantesTarefaG($sql);
        
        $x = 0;
        foreach($registro as $value) {
            $sql = $_SESSION['sql']['gestaotarefas'] . " and P.stStatusTarefa = 0 and u.dsUsuario = '" . $value['Usuario'] . "'";
            $valores = $model->getParticipantesTarefaGStatus($sql);
            if ($valores) {
                $registro[$x]['abertas'] = $valores[0]['totalstatus'];
            } else {
                $registro[$x]['abertas'] = 0;
            }
            $sql = $_SESSION['sql']['gestaotarefas'] . " and P.stStatusTarefa = 1 and u.dsUsuario = '" . $value['Usuario'] . "'";
            $valores = $model->getParticipantesTarefaGStatus($sql);
            if ($valores) {
                $registro[$x]['concluidas'] = $valores[0]['totalstatus'];
            } else {
                $registro[$x]['concluidas'] = 0;
            }
            $x++;
        }
        
        $retorno = array();
        foreach($registro as $value) {
            $retorno[$value['Usuario']]['tarefas']  = $value['tarefas'];
            $retorno[$value['Usuario']]['abertas']  = $value['abertas'];
            $retorno[$value['Usuario']]['concluidas']  = $value['concluidas'];            
        }
        
//        print_a($retorno); die;
        
        $retornogeral = array(
            'tarefas' => $retorno,            
        );

        echo json_encode($retornogeral);
        
    }
    
    public function montarResumo() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $sql = $_SESSION['sql']['gestaotarefas'];
        $x=0;
        $model = new agendaModel();
        $registro = $model->getParticipantesTarefaG($sql);
        
        $x = 0;
        foreach($registro as $value) {
            $sql = $_SESSION['sql']['gestaotarefas'] . " and P.stStatusTarefa = 0 and u.dsUsuario = '" . $value['Usuario'] . "'";
            $valores = $model->getParticipantesTarefaGStatus($sql);
            if ($valores) {
                $registro[$x]['abertas'] = $valores[0]['totalstatus'];
            } else {
                $registro[$x]['abertas'] = 0;
            }
            $sql = $_SESSION['sql']['gestaotarefas'] . " and P.stStatusTarefa = 1 and u.dsUsuario = '" . $value['Usuario'] . "'";
            $valores = $model->getParticipantesTarefaGStatus($sql);
            if ($valores) {
                $registro[$x]['concluidas'] = $valores[0]['totalstatus'];
            } else {
                $registro[$x]['concluidas'] = 0;
            }
            $x++;
        }
        $this->smarty->assign('resumo_lista', $registro);
        $html = $this->smarty->fetch("agenda/modalResumo/thumbnail1.tpl");        
        var_dump($html); die;
        
        echo json_encode(array('html' => $html,'ok' => 'ok'));
    }
    
    public function naoAgendada() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $model = new agendaModel();
        $dados = array(
            'idAgenda' => null,
            'idTipoAgenda' => 3,
            'idLocalAgenda' => 3,
            'dtAgenda' => date('Y-m-d H:i:s'),
            'dtRealizacao' => date('Y-m-d H:i:s'),
            'idUsuarioSolicitante' => $_SESSION['user']['usuario'],
            'dsAssunto' => null
            );
        
        $id =  $model->setAgenda($dados);
        header('Location: /agenda/novo_ata/idAgenda/' . $id . '/opcao/iniciar/voltarmenu/1');        
    }
    
    public function adiar_agenda() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }

        $sy = new system\System();

        $idAgenda = $sy->getParam('idAgenda');
        $this->opcao = $sy->getParam('opcao');

        $model = new agendaModel();

        if ($idAgenda > 0) {

            $registro = $model->getAgenda('r.idAgenda=' . $idAgenda);
            $registro = $registro[0]; //Passando Agenda
            $lista = $model->getParticipantes('P.idAgenda = ' . $idAgenda);
        } else {
            //Novo Registro
            $lista = null;
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        $this->smarty->assign('lista_tipo', $this->carregaTipoAgenda());          
        $this->smarty->assign('lista_local', $this->carregaLocalAgenda());   
        $this->smarty->assign('lista_usuario', $this->carregarusuarios($idAgenda));  
        
        $this->smarty->assign('lista_itens',$lista);
        $this->smarty->assign('opcao',$this->opcao);
        
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('title', 'Ata Agenda');
        $this->smarty->display('agenda/form_adiar.tpl');
    }

    public function encerrar() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $model = new agendaModel();
        $sy = new system\System();

        $idAgenda = $sy->getParam('idAgenda');
        $this->opcao = $sy->getParam('opcao');
        $this->voltarmenu = $sy->getParam('voltarmenu');
        
        $dados = array('stSituacao' => 4, 'idAgenda' => $idAgenda);
        $model = new agendaModel();
        $model->updAgenda($dados);        
        
        header('Location: /agenda/novo_ata/idAgenda/' . $idAgenda . '/opcao/' . $this->opcao . '/voltarmenu/' . $this->voltarmenu);        
        return;
        
    }

    private function carregarusuarios($idAgenda) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $model = new usuariosModel();        
        $lista = array('' => 'SELECIONE');
        if ($idAgenda) {
            foreach ($model->getUsuarioAgenda(
                    "(L.idUsuario NOT IN (SELECT 
            idUsuario
        FROM
            prodAgendaParticipantes
        WHERE
            idAgenda = {$idAgenda})
        OR L.dsUsuario = 'USUARIO EXTERNO') and L.dsUsuario <> 'THOPOS' AND L.dsUsuario = 'USUARIO EXTERNO'", $idAgenda) as $value) {
                if ($value['dsUsuario'] == 'USUARIO EXTERNO') {
                    $lista[$value['idUsuario']] = $value['dsUsuario'];                    
                } else {
                    $lista[$value['idUsuario']] = $value['dsUsuario'];
                }
            }
        } else {
            foreach ($model->getUsuario("L.dsUsuario = 'USUARIO EXTERNO'") as $value) {
                $lista[$value['idUsuario']] = $value['dsUsuario'];
            }            
        }
        return $lista;
    }
    
    private function carregarusuariosAta() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $model = new usuariosModel();        
        $lista = array('' => 'SELECIONE');
        foreach ($model->getUsuario() as $value) {
            $lista[$value['idUsuario']] = $value['dsUsuario'];
        }
        return $lista;
    }
    
    private function carregarusuariosTarefa($idAgenda) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $model = new usuariosModel();        
        $lista = array('' => 'SELECIONE');
        if ($idAgenda) {
            foreach ($model->getUsuarioAgendaAta('idAgenda = ' . $idAgenda) as $value) {
                $lista[$value['idUsuario']] = $value['dsNome'];
            }
        } else {
            foreach ($model->getUsuario() as $value) {
                $lista[$value['idUsuario']] = $value['dsUsuario'];
            }            
        }
        return $lista;
    }
    
    public function cancelar_agenda() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $sy = new system\System();
        $this->opcao = $sy->getParam('opcao');
        $idAgenda = $sy->getParam('idAgenda');
        
        $model = new agendaModel();

        if ($idAgenda > 0) {

            $registro = $model->getAgenda('r.idAgenda=' . $idAgenda);
            $registro = $registro[0]; //Passando Agenda
            $lista = $model->getParticipantes('P.idAgenda = ' . $idAgenda);
        } else {
            //Novo Registro
            $lista = null;
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        
        $this->smarty->assign('lista_tipo', $this->carregaTipoAgenda());          
    //    $this->smarty->assign('lista_local', $this->carregaLocalAgenda());   
        $this->smarty->assign('lista_usuario', $this->carregarusuarios($idAgenda));          
        $this->smarty->assign('lista_itens',$lista);
        
        $this->smarty->assign('opcao',$this->opcao);
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('title', 'Ata Agenda');
        $this->smarty->display('agenda/form_cancelar.tpl');
    }

    private function carregaLocalAgenda() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $model = new localagendaModel();
        $listaL = array('' => 'SELECIONE');
        foreach ($model->getLocalAgenda() as $value) {
            $listaL[$value['idLocalAgenda']] = $value['dsLocalAgenda'];
        }
        return $listaL;        
    }
    
    private function carregaTipoAgenda() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $model = new tipoagendaModel();
        $lista = array('' => 'SELECIONE');
        foreach ($model->getTipoAgenda() as $value) {
            $lista[$value['idTipoAgenda']] = $value['dsTipoAgenda'];
        }
        return $lista;        
    }
    
    public function lerUsuario() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $idUsuario = $_POST['idUsuario'];
        $usuarioModel = new usuariosModel();
        $dadosU = $usuarioModel->getUsuario('L.idUsuario = ' . $idUsuario);
        $retorno = array();
        if ($dadosU) {
            $retorno['dsNome'] = $dadosU[0]['dsUsuario'];
            if ($dadosU[0]['dsUsuario'] == "USUARIO EXTERNO") {
                $retorno['dsNome'] = "";
            }
            $retorno['dsEmail'] = $dadosU[0]['email'];
            $retorno['dsCelular'] = $dadosU[0]['telefone1'];
        }
        
        echo json_encode($retorno);
    }
    // Gravar Paao
    public function gravar_agenda() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }

        $dsLocalAgenda = ($_POST['dsLocal'] != '') ? $_POST['dsLocal'] : null;        
        $this->opcao = ($_POST['opcao'] != '') ? $_POST['opcao'] : null;        
        $model = new agendaModel();

        $data = $this->trataPost($_POST);

        if ($dsLocalAgenda) {
            $modelLR = new localagendaModel();
            
            $where = "dsLocalAgenda = '" . $dsLocalAgenda . "'";
            $locais = $modelLR->getLocalAgenda($where);
            if ($locais) {
                $data['idLocalAgenda'] = $locais[0]['idLocalAgenda'];
            } else {
                $array = array(
                    'idLocalAgenda' => null,
                    'dsLocalAgenda' => $dsLocalAgenda
                );
                $data['idLocalAgenda'] = $modelLR->setLocalAgenda($array);
            }
        } else {
            $data['idLocalAgenda'] = null;
        }        
        
        if ($data['idAgenda'] == NULL) {
          $id =  $model->setAgenda($data);
        } else {
          $id = $data['idAgenda'];
          $model->updAgenda($data); //update
        }       
        
        //header('Location: /agenda/novo_agenda/idAgenda/' . $id . '/opcao/' . $this->opcao);        
        $retorno = array('id' => $id);
        echo json_encode($retorno);
        
        return;
    }

    public function gravar_adiar_agenda() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $model = new agendaModel();
        $data = $this->trataPostA($_POST);
        $id = $data['idAgenda'];
        $data['stSituacao'] = 1;
        $this->opcao = $_POST['opcao'];
        $model->updAgenda($data); //update
        
        // limpar as mensagens
        $modelM = new mensagemModel();
        $lista = $model->getParticipantes('P.idAgenda = ' . $id);
        foreach ($lista as $value) {
            $mensagem = $modelM->getMensagemItem('m.idTabela = ' . $value['idParticipante'] . ' and m.dsNomeTabela = "prodAgendaParticipantes"');
            if ($mensagem) {
                $dados = array(
                  'idMensagemItem' => $mensagem[0]['idMensagemItem']  
                );
                $modelM->delMensagemItem($dados);
                $dados = array(
                  'idMensagem' => $mensagem[0]['idMensagem']  
                );
                $modelM->delMensagem($dados);
            }
            // enviar mensagens avisando do adiamento da agenda
            $this->adicionarmensagem($value['idUsuarioSolicitante'], $value['idParticipante'], $value['idAgenda'], 'ADIAR',0);
        }
        
       // header('Location: /agenda/adiar_agenda/idAgenda/' . $id . '/opcao/' . $this->opcao);        

        $retorno = array('id' => $id);
        echo json_encode($retorno);
        
        return;
    }

    public function gravar_cancelar_agenda() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }

        $model = new agendaModel();
        $data = $this->trataPostC($_POST);
        $id = $data['idAgenda'];
        $data['stSituacao'] = 2;
        $model->updAgenda($data); //update
        
        // limpar as mensagens
        $modelM = new mensagemModel();
        $lista = $model->getParticipantes('P.idAgenda = ' . $id);
        foreach ($lista as $value) {
            $mensagem = $modelM->getMensagemItem('m.idTabela = ' . $value['idParticipante'] . ' and m.dsNomeTabela = "prodAgendaParticipantes"');
            if ($mensagem) {
                $dados = array(
                  'idMensagemItem' => $mensagem[0]['idMensagemItem']  
                );
                $modelM->delMensagemItem($dados);
                $dados = array(
                  'idMensagem' => $mensagem[0]['idMensagem']  
                );
                $modelM->delMensagem($dados);
            }
            // enviar mensagens avisando do adiamento da agenda
            $this->adicionarmensagem($value['idUsuarioSolicitante'], $value['idParticipante'], $value['idAgenda'], 'CANCELAR',0);
        }
        
//        header('Location: /agenda/cancelar_agenda/idAgenda/' . $id);        
        $retorno = array('id' => $id);
        echo json_encode($retorno);
        
        return;
    }
    public function gravar_ata_agenda() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }

        $dsLocalAgenda = ($_REQUEST['dsLocal'] != '') ? $_REQUEST['dsLocal'] : null;
        $model = new agendaModel();
        $data = $this->trataPostAta($_REQUEST);
        $id = $data['idAgenda'];
        $this->opcao = ($_REQUEST['opcao'] != '') ? $_REQUEST['opcao'] : null;        
        $this->voltarmenu = ($_REQUEST['voltarmenu'] != '') ? $_REQUEST['voltarmenu'] : null;        
        
        if ($dsLocalAgenda) {
            $modelLR = new localagendaModel();
            
            $where = "dsLocalAgenda = '" . $dsLocalAgenda . "'";
            $locais = $modelLR->getLocalAgenda($where);
            if ($locais) {
                $data['idLocalAgenda'] = $locais[0]['idLocalAgenda'];
            } else {
                $array = array(
                    'idLocalAgenda' => null,
                    'dsLocalAgenda' => $dsLocalAgenda
                );
                $data['idLocalAgenda'] = $modelLR->setLocalAgenda($array);
            }
        } else {
            $data['idLocalAgenda'] = null;
        }
        
        $model->updAgenda($data); //update        
        header('Location: /agenda/novo_ata/idAgenda/' . $id . '/opcao/' . $this->opcao . '/voltarmenu/' . $this->voltarmenu);        
        return;
    }
    
    public function adicionaritem() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
      
        $data = $this->trataPostItem($_POST);
        $modelG = new agendaModel();
        if ($data['idParticipante']) {
            $id = $data['idParticipante'];
            $modelG->updParticipantes($data);
        } else {
            $id = $modelG->setParticipantes($data);
        }               
        
        $lista = $modelG->getParticipantes('P.idParticipante = ' . $id);
        // ENVIAR MENSAGEM  
        if ($lista) {
            $this->adicionarmensagem($lista[0]['idUsuarioSolicitante'], $lista[0]['idParticipante'], $lista[0]['idAgenda'],'AGENDAR',1);
        }
        $lista = $modelG->getParticipantes('P.idAgenda = ' . $data['idAgenda']);
        
        $this->smarty->assign('lista_usuario', $this->carregarusuarios($data['idAgenda']));                  
        $this->smarty->assign('lista_itens',$lista);
        $html = $this->smarty->fetch("agenda/itens.html");
        $htmlU = $this->smarty->fetch("agenda/lista_usuario.tpl");
        
        $jsondata = array(
            'html' => $html,
            'htmlU' => $htmlU
        );
        echo json_encode($jsondata);        
    }
    
    public function adicionartarefa() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $data = $this->trataPostItemTarefa($_POST);
        $origem = $_POST['origem'];
        
        $modelT = new agendaModel();
        if ($data['idTarefa']) {
            $id = $data['idTarefa'];
            $modelT->updTarefas($data);
        } else {
            $id = $modelT->setTarefas($data);
        }               
        
        $lista = $modelT->getParticipantesTarefa('P.idTarefa = ' . $id);
        // ENVIAR MENSAGEM  
        if ($lista) {
            $this->adicionarmensagem($lista[0]['idUsuarioSolicitante'], $lista[0]['idTarefa'], $lista[0]['idAgenda'],'TAREFA',0);
        }
        
        $listaUT = $modelT->getParticipantesTarefa('P.idAgenda = ' . $data['idAgenda']);
//        var_dump($listaUT); die;
        $this->smarty->assign('lista_itensTarefa',$listaUT);
//        
//        $this->smarty->assign('lista_usuario', $this->carregarusuarios($data['idAgenda']));                  
        $html = $this->smarty->fetch("agenda/itenstarefa.html");
//        $htmlU = $this->smarty->fetch("agenda/lista_usuario.tpl");
        
        $jsondata = array(
            'html' => $html
        );
        echo json_encode($jsondata);        
    }
    
    public function adicionaritemAta() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $data = $this->trataPostItem($_POST);
        $modelG = new agendaModel();
        if ($data['idParticipante']) {
            $id = $data['idParticipante'];
            $modelG->updParticipantes($data);
        } else {
            $id = $modelG->setParticipantes($data);
        }               
        
        $lista = $modelG->getParticipantes('P.idParticipante = ' . $id);
        // ENVIAR MENSAGEM  
        if ($lista) {
            $this->adicionarmensagem($lista[0]['idUsuarioSolicitante'], $lista[0]['idParticipante'], $lista[0]['idAgenda'],'AGENDAR',0);
        }
        $lista = $modelG->getParticipantes('P.idAgenda = ' . $data['idAgenda']);
        $this->smarty->assign('lista_itens',$lista);
        $this->smarty->assign('lista_usuario', $this->carregarusuarios($data['idAgenda']));                  
        $this->smarty->assign('lista_usuariotarefa', $this->carregarusuariosTarefa($data['idAgenda']));                  
        $html = $this->smarty->fetch("agenda/itensata.html");
        $htmlU = $this->smarty->fetch("agenda/lista_usuario.tpl");
        $htmlUA = $this->smarty->fetch("agenda/lista_usuariotarefa.tpl");
        
        $jsondata = array(
            'html' => $html,
            'htmlU' => $htmlU,
            'htmlUA' => $htmlUA
        );
        echo json_encode($jsondata);        
    }

    public function envairemail() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
      
        $idParticipante = $_POST['idParticipante'];
        $idAgenda = $_POST['idAgenda'];
        $origem = $_POST['deOnde'];
        $retorno = $this->envair_mesmo_email($idAgenda,$idParticipante, $origem);
        if ($retorno) {
            $ok = 'E-mail enviado com sucesso';
        } else {
            $ok = 'ERRO AO ENVIAR E-MAIL';
        }
        $jsondata = array(
            'ok' => $ok
        );
        echo json_encode($jsondata);   
    }
    
    private function envair_mesmo_email($idAgenda, $idParticipante, $origem) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $model = new agendaModel();  
        if ($idParticipante) {
            $retorno = $model->getParticipantes('P.idParticipante = ' . $idParticipante);
        } else {
            $retorno = $model->getParticipantes('P.idAgenda = ' . $idAgenda);
        }
        $participantes = $model->getParticipantes('P.idAgenda = ' . $idAgenda);

        if ($origem == 'Ata') {
            $titulo = 'REUNIAO REALIZADA';
        } else {
        if ($origem == 'Agenda') {
            $titulo = 'REUNIAO AGENDADA';
        } else {
        if ($origem == 'Cancelar') {
            $titulo = 'REUNIAO CANCELADA';
        } else {
            $titulo = 'REUNIAO ADIADA';
        }}}

        $caminhoAta = $retorno[0]['dsCaminhoPDF'];
        
        $to = array();
        $emails = array();
        if ($retorno) {
            $emails = $retorno;
        }
        $x=0;
        if ((bool)$emails) {
            foreach ($emails as $value) {
                $to[$x]['email'] = filter_var(strtolower($value['email']), FILTER_VALIDATE_EMAIL);
                $to[$x]['nome'] = $value['dsUsuario'];
                $x++;
            }
        }
        array_filter($to);        
//                print_a_die($retorno); die;
        
        if (!empty($to)) {
            $x=0;
            $this->smarty->assign('idAgenda', $retorno[$x]['idAgenda']);
            $this->smarty->assign('idParticipante', $retorno[$x]['idParticipante']);
            foreach ($to as $value) {
                $title = $titulo;
                $this->smarty->assign('title', $title);
                $this->smarty->assign('usuariodestino', $retorno[$x]['dsUsuario']);
                $this->smarty->assign('data_envio', date('Y-m-d H:i'));
                $this->smarty->assign('usuarioenvio', $retorno[$x]['UsuarioSolicitante']);
                $this->smarty->assign('tipoagenda', $retorno[$x]['dsTipoAgenda']);
                $this->smarty->assign('dtAgenda', $retorno[$x]['dtAgenda']);
                $this->smarty->assign('dsPauta', $retorno[$x]['dsPauta']);
                $this->smarty->assign('dsAssunto', $retorno[$x]['dsAssunto']);
                $this->smarty->assign('dsTempoDuracao', $retorno[$x]['dsTempoDuracao']);
                $this->smarty->assign('dsLocalAgenda', $retorno[$x]['dsLocalAgenda']);
                $this->smarty->assign('lista_itens',$participantes);
                if ($origem == 'Ata') {
                    $this->smarty->assign('dsAta', $retorno[$x]['dsAta']);
                    $this->smarty->assign('dsTempoDuracaoReal', $retorno[$x]['dsTempoDuracaoReal']);
                    $this->smarty->assign('dtRealizacao', $retorno[$x]['dtRealizacao']);
//                    $body = $this->smarty->fetch('agenda/email/enviar_email_ata.tpl');
//                } else {
//                    $body = $this->smarty->fetch('agenda/email/enviar_email.tpl');
                }
                $body = $this->smarty->fetch('agenda/email/enviar_email.tpl');

                $anexos = array();
                $modelD = new documentoModel();
                $where = "dsTabela = 'prodAgenda' and idTabela = " . $idAgenda;
                $retanex = $modelD->getDocumento($where);
                $y=0;
                foreach ($retanex as $valuea) {
                    $anexos[$y]['path_anexo'] = $valuea['dsLocalArquivo'];
                    $anexos[$y]['nome_anexo'] = $valuea['dsNome'];
                    $y++;
                }

                $modelD = new documentoModel();
                $where = "dsTabela = 'prodAgendaParticipantes' and idTabela = " . $retorno[$x]['idParticipante'];
                $retanex = $modelD->getDocumento($where);
                foreach ($retanex as $valuea) {
                    $anexos[$y]['path_anexo'] = $valuea['dsLocalArquivo'];
                    $anexos[$y]['nome_anexo'] = $valuea['dsNome'];
                    $y++;
                }

                // envia a ata como anexo
                if ($caminhoAta) {
                    $anexos[$y]['path_anexo'] = $caminhoAta;
                    $anexos[$y]['nome_anexo'] = 'ATA DA REUNIÃO';
                }
                
                $ok = $this->Send(array('0' => array(
                    'email' => $value['email'],
                    'nome' => $value['nome'])), $titulo, $body, array(), $anexos);
                $x++;
            }
            return $ok;
        } else {
            return false;
        }
    }

    private function Send($to_list, $subject, $body, $addbcc = array(), $attachment = array()) {    
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->SMTPDebug = 2;
        $mail->SMTPSecure = 'tls';            
        $mail->Port = 587;
        $mail->FromName = 'Thopos';
        $mail->IsHTML('text/html');    
        $mail->CharSet = 'UTF-8';
        
        $model = new agendaModel('idDE = 1');
        $valores = $model->getValores();
        if ($valores) {
            $mail->Host = ($valores[0]['H']);
            $mail->Username = ($valores[0]['U']);
            $mail->Password = ($valores[0]['P']);
            $mail->From = ($valores[0]['F']);
        } else {
            return false;            
        }
        
        $mail->ClearAddresses();
        
        $concat_to = array();
        foreach ($to_list as $to) {
          if (empty($to['nome'])) {
            $to['nome'] = $to['email'];
          }
          $concat_to[] = "{$to['nome']} &lt;{$to['email']}&gt;"; 
        }
        foreach ($to_list as $to) {
            $mail->AddAddress($to["email"], $to["nome"]);
        }
          
        $mail->Subject = $subject;
        $mail->Body = $body;
        
        foreach ($attachment as $att) {     
            $mail->AddAttachment($att['path_anexo'], $att['nome_anexo']);
        }

        if ($mail->Send()) {
            return true;
        } else {
            return false;
        }
    }
    public function emitir_pdf() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $idAgenda = ($_POST['idAgenda'] != '') ? $_POST['idAgenda'] : null;        
        $url = $this->enviarPDF($idAgenda);
        $retorno = array();
        echo json_encode($retorno);        
    }

    public function enviarPDF($idAgenda) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $idAgenda = ($_POST['idAgenda'] != '') ? $_POST['idAgenda'] : null;
        global $PATH;
        $caminho = $GLOBALS['PATH'];
        if (!is_dir($this->pastaPedidos)) {
            mkdir($this->pastaPedidos, 0777, true);
        }
        $url = null;
        if (file_exists($this->pastaPedidos)) {
            
            $where = 'r.idAgenda = ' . $idAgenda;
            $model = new agendaModel();
            $modelD = new documentoModel();
            $cabec = $model->getAgenda($where)[0];
            $this->smarty->assign("agenda", $cabec);
            $participantes = $model->getParticipantes($where);
            $this->smarty->assign("participantes", $participantes);
            $tarefas = $model->getParticipantesTarefa($where);
//            var_dump($participantes);
//            var_dump($tarefas); die;
            $this->smarty->assign("tarefas", $tarefas);
            $anexos = $modelD->getDocumento("dsTabela = 'prodAgenda' and idTabela = " . $idAgenda);
            if ($anexos) {
                $this->smarty->assign("anexos", $anexos);
            } else {
                $this->smarty->assign("anexos", null);
            }

            // Filename
            $filename = "ATA_Agenda_" . $idAgenda . "_" . $cabec['dtAgenda'];
            // Dependecias
            require_once 'system/libs/mpdf/mpdf.php';
            $mpdf = new mPDF('c', 'A4', '5', 'Arial');

            $template = "agenda/pdf/PDFTemplate.tpl";

            $this->smarty->clearCache($template);
            $mpdf->WriteHTML($this->smarty->fetch($template));
            if (file_exists("{$this->pastaPedidos}{$filename}.pdf")) {
                unlink("{$this->pastaPedidos}{$filename}.pdf");
            }
            // Arquivo
            $mpdf->Output("{$this->pastaPedidos}{$filename}.pdf", 'F');

            if(file_exists("{$this->pastaPedidos}{$filename}.pdf")  ){
                $url = "{$this->pastaPedidos}{$filename}.pdf";
            }
            // ATUALIZR O CAMINHO DO PDF NO PEDIDO
            $data = array(
                'dsCaminhoPDF' => $url,
                'idAgenda' => $idAgenda
            );
            $model->updAgenda($data);
        }
        
        return $url;
    }    
    
    public function excluiranexo() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $model = new documentoModel();        
        $idDocumento = $_POST['idDocumento'];
        $idAgenda = $_POST['idAgenda'];
        $dsTabela = $_POST['dsTabela'];
        $idParticipante = $_POST['idParticipante'];

        $ok = 'Anexo excluido';
        $where = array('idDocumento' => $idDocumento);
        $model->delDocumento($where);
        if ($idParticipante) {
            $lista = $model->getDocumento("dsTabela = '" . $dsTabela . "' and idTabela  = " . $idParticipante);
        } else {
            $lista = $model->getDocumento("dsTabela = '" . $dsTabela . "' and idTabela  = " . $idAgenda);
        }
        $this->smarty->assign('fotos_lista', $lista);
        $html = $this->smarty->fetch("agenda/modalFotos/thumbnail1.tpl");        
        
        $jsondata = array(
            'html' => $html,
            'ok' => $ok
        );
        echo json_encode($jsondata);
    }
    
    public function delparticipante() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $model = new agendaModel();        
        $idAgenda = $_POST['idAgenda'];
        $idParticipante = $_POST['idParticipante'];
        $ok = 'Participante excluido';
        $where = 'idParticipante = ' . $idParticipante;
        $model->delParticipantes($where);        
        
        $lista = $model->getParticipantes('P.idAgenda = ' . $idAgenda);
        $this->smarty->assign('lista_itens',$lista);
        $this->smarty->assign('lista_usuario', $this->carregarusuarios($idAgenda));                  
        
        $html = $this->smarty->fetch("agenda/itens.html");
        $htmlU = $this->smarty->fetch("agenda/lista_usuario.tpl");
        
        $jsondata = array(
            'html' => $html,
            'htmlU' => $htmlU,
            'ok' => $ok
        );
        echo json_encode($jsondata);
    }
    public function deltarefa() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $model = new agendaModel();        
        $idAgenda = $_POST['idAgenda'];
        $idTarefa = $_POST['idTarefa'];
        $ok = 'Tarefa excluida';
        $where = 'idTarefa = ' . $idTarefa;
        $model->delTarefa($where);        
        
        $listaUT = $model->getParticipantesTarefa('P.idAgenda = ' . $idAgenda);
//        var_dump($listaUT); die;        
        $this->smarty->assign('lista_itensTarefa',$listaUT);
        $html = $this->smarty->fetch("agenda/itenstarefa.html");
        
        $jsondata = array(
            'html' => $html,
            'ok' => $ok
        );
        echo json_encode($jsondata);
    }
    
    public function concluirtarefa() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $model = new agendaModel();        
        $idTarefa = $_POST['idTarefa'];
        $array = array('idTarefa' => $idTarefa,
                'stStatusTarefa' => 1,
                'dtConclusao' => date('Y-m-d H:i:s')
                );
        $model->updTarefas($array);        
        echo json_encode(array());
    }
    
    public function concluirsubtarefa() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $model = new agendaModel();        
        $idData = $_POST['idData'];
        $origem = $_POST['origem'];
        $idTarefa = $_POST['idTarefa'];
        $idUsuario = $_POST['idUsuario'];
        $array = array('idData' => $idData,
                'stStatusTarefa' => 1,
                'dtConclusao' => date('Y-m-d H:i:s')
                );
        $model->updDatas($array);    
        
        $lista = $model->getDatas("idTarefa  = " . $idTarefa);
        $this->smarty->assign('datas_lista', $lista);
        $this->smarty->assign('origem', $origem);
        $this->smarty->assign('idTarefa', $idTarefa);
        $this->smarty->assign('idUsuario', $idUsuario);
        $html = $this->smarty->fetch("agenda/modalDatas/thumbnail1.tpl");        
        
        echo json_encode(array('html'=>$html));
    }
    
    public function presente() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $model = new agendaModel();        
        $idAgenda = $_POST['idAgenda'];
        $idParticipante = $_POST['idParticipante'];
        $ok = 'Participante presente';
        $array = array('idParticipante' => $idParticipante,
                'stSituacao' => 2
                );
        $model->updParticipantes($array);        
        
        $lista = $model->getParticipantes('P.idAgenda = ' . $idAgenda);
        $this->smarty->assign('lista_itens',$lista);
        $html = $this->smarty->fetch("agenda/itensata.html");
        
        $jsondata = array(
            'html' => $html,
            'ok' => $ok
        );
        echo json_encode($jsondata);
    }
    
    public function confirmar() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $model = new agendaModel();        
        $idAgenda = $_POST['idAgenda'];
        $idUsuario = $_SESSION['user']['usuario'];
        $ok = 1;

        $array = array('idUsuario' => $idUsuario,
                'idAgenda' => $idAgenda,
                'stPresenca' => 1
                );
        $model->updParticipantesPresenca($array);        

        $sql = "r.dtAgenda >= '" . date('Y-m-d H:i:s') . "' and r.idAgenda > 0 and r.stSituacao = 0 and (r.idUsuarioSolicitante = " . $_SESSION['user']['usuario'] . " or p.idUsuario = " . $_SESSION['user']['usuario'] . ")";        
        $agenda_lista = $model->getAgenda($sql);
        
        $this->smarty->assign('agenda_lista', $agenda_lista);
        $html = $this->smarty->fetch('agenda/menuIndex_lista.html');
        
        $jsondata = array(
            'ok' => $ok,
            'html' => $html
        );
        echo json_encode($jsondata);
    }
    
    public function naoconfirmar() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $model = new agendaModel();        
        $idAgenda = $_POST['idAgenda'];
        $idUsuario = $_SESSION['user']['usuario'];
        $ok = 1;

        $array = array('idUsuario' => $idUsuario,
                'idAgenda' => $idAgenda,
                'stPresenca' => 2
                );
        $model->updParticipantesPresenca($array);        
        
        $sql = "r.dtAgenda >= '" . date('Y-m-d H:i:s') . "' and r.idAgenda > 0 and r.stSituacao = 0 and (r.idUsuarioSolicitante = " . $_SESSION['user']['usuario'] . " or p.idUsuario = " . $_SESSION['user']['usuario'] . ")";        
        $agenda_lista = $model->getAgenda($sql);
        
        $this->smarty->assign('agenda_lista', $agenda_lista);
        $html = $this->smarty->fetch('agenda/menuIndex_lista.html');
        
        $jsondata = array(
            'ok' => $ok,
            'html' => $html
        );
        echo json_encode($jsondata);
    }
    
    public function ausente() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $model = new agendaModel();        
        $idAgenda = $_POST['idAgenda'];
        $idParticipante = $_POST['idParticipante'];
        $ok = 'Participante ausente';
        $array = array('idParticipante' => $idParticipante,
                'stSituacao' => 1
                );
        $model->updParticipantes($array);        
        
        $lista = $model->getParticipantes('P.idAgenda = ' . $idAgenda);
        $this->smarty->assign('lista_itens',$lista);
        $html = $this->smarty->fetch("agenda/itensata.html");
        
        $jsondata = array(
            'html' => $html,
            'ok' => $ok
        );
        echo json_encode($jsondata);
    }
    
    //Trata dados antes de Enviar para o Gravar
    private function trataPost($post) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $data['idAgenda'] = ($post['idAgenda'] != '') ? $post['idAgenda'] : null;
        $data['idTipoAgenda'] = ($post['idTipo'] != '') ? $post['idTipo'] : null;
        $data['idLocalAgenda'] = ($post['idLocal'] != '') ? $post['idLocal'] : null;
        $data['dsTempoDuracao'] = ($post['dsTempoDuracao'] != '') ? $post['dsTempoDuracao'] : null;
        $data['dtAgenda'] = ($post['dtAgenda'] != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $_POST["dtAgenda"]))) : date('Y-m-d h:m');
        $data['dsAssunto'] = ($post['dsAssunto'] != '') ? $post['dsAssunto'] : null;
        $data['idUsuarioSolicitante'] = $_SESSION['user']['usuario'];
        return $data;
    }

    private function trataPostA($post) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $data['idAgenda'] = ($post['idAgenda'] != '') ? $post['idAgenda'] : null;
        $data['dtAgenda'] = ($post['dtAgenda'] != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $_POST["dtAgenda"]))) : date('Y-m-d h:m');
        return $data;
    }

    private function trataPostAta($post) {    
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $data['idAgenda'] = ($post['idAgenda'] != '') ? $post['idAgenda'] : null;
        $data['dtRealizacao'] = ($post['dtRealizacao'] != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $_POST["dtRealizacao"]))) : date('Y-m-d h:m');
        $data['idUsuarioAta'] = ($post['idUsuarioAta'] != '') ? $post['idUsuarioAta'] : null;
        $data['idTipoAgenda'] = ($post['idTipo'] != '') ? $post['idTipo'] : null;
        $data['idLocalAgenda'] = ($post['idLocal'] != '') ? $post['idLocal'] : null;
        $data['dtAgenda'] = ($post['dtAgenda'] != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $_POST["dtAgenda"]))) : date('Y-m-d h:m');
        $data['dsAssunto'] = ($post['dsAssunto'] != '') ? $post['dsAssunto'] : null;
        $data['dsTempoDuracaoReal'] = ($post['dsTempoDuracaoReal'] != '') ? $post['dsTempoDuracaoReal'] : null;
        $data['dsAta'] = ($post['dsAta'] != '') ? $post['dsAta'] : null;
        return $data;
    }

    private function trataPostC($post) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $data['idAgenda'] = ($post['idAgenda'] != '') ? $post['idAgenda'] : null;
        $data['dsCancelado'] = ($post['dsCancelado'] != '') ? $post['dsCancelado'] : null;
        return $data;
    }

    private function trataPostItem($post) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $data['idAgenda'] = ($post['idAgenda'] != '') ? $post['idAgenda'] : null;
        $data['idParticipante'] = ($post['idParticipante'] != '') ? $post['idParticipante'] : null;
        $data['idUsuario'] = ($post['idUsuario'] != '') ? $post['idUsuario'] : null;
        $data['dsAssunto'] = ($post['dsAssunto'] != '') ? $post['dsAssunto'] : null;
        $data['dsEmail'] = ($post['dsEmail'] != '') ? $post['dsEmail'] : null;
        $data['dsNome'] = ($post['dsNome'] != '') ? $post['dsNome'] : null;
        $data['dsCelular'] = ($post['dsCelular'] != '') ? $post['dsCelular'] : null;
        $data['idParceiro'] =  null;
        $data['dsCaminhoAnexo'] = NULL;
        return $data;
    }

    private function trataPostItemTarefa($post) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $data['idAgenda'] = ($post['idAgenda'] != '') ? $post['idAgenda'] : null;
        $data['idTarefa'] = ($post['idTarefa'] != '') ? $post['idTarefa'] : null;
        $data['idUsuario'] = ($post['idUsuarioTarefa'] != '') ? $post['idUsuarioTarefa'] : null;
        $data['dsTarefa'] = ($post['dsTarefa'] != '') ? $post['dsTarefa'] : null;
        $data['stStatusTarefa'] = 0;
        $data['dtPrazo'] = ($post['dtPrazo'] != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $_POST["dtPrazo"]))) : date('Y-m-d h:m');
        return $data;
    }

    // Remove Paao
    public function delagenda() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $sy = new system\System();                
        $idAgenda = $sy->getParam('idAgenda');        
        if (!is_null($idAgenda)) {    
            $model = new agendaModel();
            $dados['idAgenda'] = $idAgenda;             
            $dados['stSituacao'] = 3;             
            $model->updAgenda($dados);
        }
        header('Location: /agenda');
    }
    
    public function arquivar() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $sy = new system\System();                
        $idAgenda = $sy->getParam('idAgenda');        
        if (!is_null($idAgenda)) {    
            $model = new agendaModel();
            $dados['idAgenda'] = $idAgenda;             
            $dados['stSituacao'] = 3;             
            $model->updAgenda($dados);
        }
        header('Location: /agenda');
    }
    
    public function importarfotos() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $idAgenda = $_POST['idAgenda'];
        $dsTabela = $_POST['dsTabela'];
        $idParticipante = $_POST['idParticipante'];
        $Origem = $_POST['Origem'];
        
        $model = new documentoModel();        
        $lista = $model->getDocumento("dsTabela = '" . $dsTabela . "' and idTabela  = " . $idParticipante);
        $this->smarty->assign('idParticipante', $idParticipante);
        $this->smarty->assign('idAgenda', $idAgenda);
        $this->smarty->assign('dsTabela', $dsTabela);
        $this->smarty->assign('fotos_lista', $lista);
        $this->smarty->assign('Origem', $Origem);
        $this->smarty->display("agenda/modalFotos/thumbnail.tpl");        
    }

    public function importarfotosR() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $idAgenda = $_POST['idAgenda'];
        $dsTabela = $_POST['dsTabela'];
        $Origem = $_POST['Origem'];
        
        $model = new documentoModel();        
        $lista = $model->getDocumento("dsTabela = '" . $dsTabela . "' and idTabela  = " . $idAgenda);
        $this->smarty->assign('idParticipante', null);
        $this->smarty->assign('idAgenda', $idAgenda);
        $this->smarty->assign('dsTabela', $dsTabela);
        $this->smarty->assign('Origem', $Origem);
        $this->smarty->assign('fotos_lista', $lista);
        $this->smarty->display("agenda/modalFotos/thumbnail.tpl");        
    }

    public function adicionarfoto() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $idAgenda = $_POST['idAgenda'];
        $dsTabela = $_POST['dsTabela'];
        $idParticipante = $_POST['idParticipante'];
        $dsNome = $_POST['dsNome'];
        $Origem = $_POST['Origem'];
        if (!$idParticipante) {
            $idParticipante = $idAgenda;
        }
        $extensao = explode('.',$_FILES['arquivo']['name'])[1];
        $novonome = $idParticipante . '_' . date('Ymdis') . '.' . $extensao;
        
        $_UP['pasta'] = 'storage/tmp/documentos/';
        // Tamanho máximo do arquivo (em Bytes)
        $_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
        // Array com as extensões permitidas
        $_UP['extensoes'] = array('jpg','jpeg','png','bmp');
        // Renomeia o arquivo? (Se true, o arquivo será salvo como .jpg e um nome único)
        $_UP['renomeia'] = false;
        // Array com os tipos de erros de upload do PHP
        $_UP['erros'][0] = 'Não houve erro';
        $_UP['erros'][1] = 'O arquivo no upload é maior do que o limite do PHP';
        $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
        $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
        $_UP['erros'][4] = 'Não foi feito o upload do arquivo';
        // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
        if ($_FILES['arquivo']['error'] != 0) {
          die("Não foi possível fazer o upload, erro:" . $_UP['erros'][$_FILES['arquivo']['error']]);
          exit; // Para a execução do script
        }
        // Caso script chegue a esse ponto, não houve erro com o upload e o PHP pode continuar
        // Faz a verificação da extensão do arquivo
        // Faz a verificação do tamanho do arquivo
        if ($_UP['tamanho'] < $_FILES['arquivo']['size']) {
          echo "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
          exit;
        }

        // Depois verifica se é possível mover o arquivo para a pasta escolhida
        if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $_UP['pasta'] . $novonome)) {
            $model = new documentoModel();    
            $dados = array(
                'idDocumento' => null,
                'dsTabela' => $dsTabela,
                'idTabela' => $idParticipante,
                'dsNome' => $dsNome,
                'dsLocalArquivo' => $_UP['pasta'] . $novonome                
            );
            $model->setDocumento($dados);                        
        } else {
          // Não foi possível fazer o upload, provavelmente a pasta está incorreta
          echo "Não foi possível enviar o arquivo, tente novamente";
        }    
        if ($Origem == "Agenda") {
            header('Location: /agenda/novo_agenda/idAgenda/' . $idAgenda);                
        } else {
            header('Location: /agenda/novo_ata/idAgenda/' . $idAgenda);                            
        }
    }    
    
    public function adicionardata() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $idData = $_POST['idData'];
        $idTarefa = $_POST['idTarefa'];
        $dtIntermediaria = $_POST['dtIntermediaria'];
        $dsSubTarefa = $_POST['dsSubTarefa'];
        $origem = $_POST['origem'];
        $model = new agendaModel();    
        if ($idData) {
            $dados = array(
                'idData' => $idData,
                'dsSubTarefa' => $dsSubTarefa,
                'dtIntermediaria' => ($dtIntermediaria != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $dtIntermediaria))) : date('Y-m-d h:m')
            );
            $model->updDatas($dados);                        
        } else {
            $dados = array(
                'idData' => null,
                'idTarefa' => $idTarefa,
                'dsSubTarefa' => $dsSubTarefa,
                'dtIntermediaria' => ($dtIntermediaria != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $dtIntermediaria))) : date('Y-m-d h:m')
            );
            $model->setDatas($dados);                        
        }
        $lista = $model->getDatas("idTarefa  = " . $idTarefa);
        $this->smarty->assign('datas_lista', $lista);
        $this->smarty->assign('origem', $origem);
        $html = $this->smarty->fetch("agenda/modalDatas/thumbnail1.tpl");        
        echo json_encode(array('html' => $html));
    }    
    public function deldata() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $idData = $_POST['idData'];
        $idTarefa = $_POST['idTarefa'];
        $origem = $_POST['origem'];
        $idUsuario = $_POST['idUsuario'];
        
        $model = new agendaModel();    
        $where = 'idData = ' . $idData;
        $model->delData($where);                        
        $lista = $model->getDatas("idTarefa  = " . $idTarefa);
        $this->smarty->assign('datas_lista', $lista);
        $this->smarty->assign('origem', $origem);
        $this->smarty->assign('idUsuario', $idUsuario);
        $html = $this->smarty->fetch("agenda/modalDatas/thumbnail1.tpl");        
        echo json_encode(array('html' => $html));
    }    
    public function verdatas() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $idAgenda = $_POST['idAgenda'];
        $idTarefa = $_POST['idTarefa'];
        $origem = $_POST['origem'];
        $idUsuario = $_POST['idUsuario'];
        
        
        $model = new agendaModel();        
        $lista = $model->getDatas("idTarefa  = " . $idTarefa);
        //$lista[0] = array('dtIntermediaria' => date('Y-m-d'), 'dsSubTarefa' => 'teste', 'dtConclusao' => date('Y-m-d'), 'stStatusTarefa' => '0', 'idData' => 1, 'idTarefa' => 1);
        //var_dump($lista); die;
        $this->smarty->assign('idTarefa', $idTarefa);
        $this->smarty->assign('idData', null);
        $this->smarty->assign('origem', $origem);
        $this->smarty->assign('idUsuario', $idUsuario);
        $this->smarty->assign('datas_lista', $lista);
        $this->smarty->display("agenda/modalDatas/thumbnail.tpl");        
    }

    private function adicionarmensagem($idUsuario, $idParticipante, $idAgenda, $qual, $tipo) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $mensagem = 'Reunião: ';
        $model = new agendaModel();    
        if ($qual == 'TAREFA') {
            $dados = $model->getParticipantesTarefa('P.idTarefa = ' . $idParticipante);
        } else {
            $dados = $model->getParticipantes('P.idParticipante = ' . $idParticipante);
        }
        
        if ($dados) {
            if ($dados[0]['idUsuario'] <> $_SESSION['user']['usuario']) { 
                if ($qual == 'AGENDAR') {
                    $descricao = 'Reunião: ' . $dados[0]['dsTipoAgenda'] . ' - Agendado por: ' . $dados[0]['UsuarioSolicitante'] . ' - Data ' . date("d/m/Y H:i", strtotime(str_replace("-", "/", $dados[0]['dtAgenda']))) . ' - Pauta: ' . $dados[0]['dsPauta'] . ' - Seu Assunto: ' . $dados[0]['assuntoP'];
                } else {
                if ($qual == 'ADIAR') {
                    $descricao = 'ADIADA - Reunião: ' . $dados[0]['dsTipoAgenda'] . ' - Agendado por: ' . $dados[0]['UsuarioSolicitante'] . ' - Data ' . date("d/m/Y H:i", strtotime(str_replace("-", "/", $dados[0]['dtAgenda']))) . ' - Pauta: ' . $dados[0]['dsPauta'] . ' - Seu Assunto: ' . $dados[0]['assuntoP'];
                } else {
                if ($qual == 'TAREFA') {
                    $descricao = 'TAREFA - Reunião: ' . $dados[0]['dsTipoAgenda'] . ' - Agendado por: ' . $dados[0]['UsuarioSolicitante'] . ' - Data ' . date("d/m/Y H:i", strtotime(str_replace("-", "/", $dados[0]['dtRealizacao']))) . ' - Pauta: ' . $dados[0]['dsPauta'] . ' - Sua Tarefa: ' . $dados[0]['tarefa'] . ' - Prazo ' . date("d/m/Y H:i", strtotime(str_replace("-", "/", $dados[0]['dtRealizacao'])));
                } else {
                    $descricao = 'CANCELADA - Reunião: ' . $dados[0]['dsTipoAgenda'] . ' - Agendado por: ' . $dados[0]['UsuarioSolicitante'] . ' - Data ' . date("d/m/Y H:i", strtotime(str_replace("-", "/", $dados[0]['dtAgenda']))) . ' - Pauta: ' . $dados[0]['dsPauta'] . ' - Seu Assunto: ' . $dados[0]['assuntoP'];
                }}}
                $dadosi = array(
                    'idUsuarioOrigem' => $_SESSION['user']['usuario'],
                    'idUsuarioDestino' => $dados[0]['idUsuario'],
                    'dsNomeTabela' => 'prodAgendaParticipantes',
                    'idOrigemInformacao' => 99,
                    'idTabela' => $idParticipante,
                    'idTipoMensagem' => $tipo,
                    'stSituacao' => 0,
                    'dtEnvio' => date('Y-m-d H:i:s'),
                    'dsMensagem' => $descricao,
                    'dsCaminhoArquivo' => null
                );
                $this->criarMensagem($dadosi);
            }
        }        
    }
    
    private function criarMensagem($array) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $modelMensagem = new mensagemModel();
        $dados = array(
          'idMensagem' => null,  
          'idOrigemInformacao' => $array['idOrigemInformacao'],
          'dsNomeTabela' => $array['dsNomeTabela'],
          'idTabela' => $array['idTabela']
        );
        $id = $modelMensagem->setMensagem($dados);
        $dados = array(
          'idMensagemAnterior' => $id,              
        );        
        $modelMensagem->updMensagem($dados, 'idMensagem = ' . $id);
        $dados = array(
            'idMensagemItem' => null,  
            'idMensagem' => $id,  
            'idUsuarioOrigem' => $array['idUsuarioOrigem'],
            'idUsuarioDestino' => $array['idUsuarioDestino'],
            'idTipoMensagem' => $array['idTipoMensagem'],
            'stSituacao' => $array['stSituacao'],
            'dtEnvio' => $array['dtEnvio'],
            'dsMensagem' => $array['dsMensagem'],
            'dsCaminhoArquivo' => $array['dsCaminhoArquivo']
        );
        $id = $modelMensagem->setMensagemItem($dados);
    }    
    public function getLocal() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $url = explode("=", $_SERVER['REQUEST_URI']);
        $key = str_replace('+', ' ', $url[1]);
        if (!empty($key)) {
          $busca = trim($key);
          $model = new localagendaModel();
          $where = "(UPPER(dsLocalAgenda) like UPPER('%{$key}%'))";
          $retorno = $model->getLocalAgenda($where);
          $return = array();
          if (count($retorno)) {
            $row = array();
            for ($i = 0; $i < count($retorno); $i++) {
              $row['value'] = strtoupper($retorno[$i]["dsLocalAgenda"]);
              $row["id"] = $retorno[$i]["idLocalAgenda"];
              array_push($return, $row);
            }
          }
          echo json_encode($return);
        }
    }    
}

?>