<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class pedido extends controller {
    
    private $pastaPedidos = PEDIDOS;
    private $idParceiro = null;
    
    public function index_action() {
        //Inicializa o Template
        $this->template->run();
        unset($_SESSION['pedido']['id']);
        
        $model = new pedidoModel();
        $registro = $model->getPedido('a.idSituacaoPedido < 3', $paginacao=true);

        $this->smarty->assign('pedidos', $registro);
        $this->smarty->assign('title', 'Pedido de Compra');
        $this->smarty->display('pedido/prelista.html');
        
    }

//Funcao de Busca
    public function busca_pedido() {
        //se nao existir o indice estou como padrao '';
        $texto = isset($_POST['buscadescricao']) ? $_POST['buscadescricao'] : '';
        //$texto = '';
        $model = new pedidoModel();
        $sql = "a.stStatus <> 0 and upper(a.dsPedido) like upper('%" . $texto . "%')"; //somente os nao excluidos
        $resultado = $model->getPedido($sql, $paginacao=true);

        if (sizeof($resultado) > 0) {
            $this->smarty->assign('pedido_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'pedido');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('pedido/lista.html');
        } else {
            $this->smarty->assign('pedido_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'pedido');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('pedido/lista.html');
        }
    }
    
    public function lersolicitacao() {        
        $idSolicitacao = ($_POST['idSolicitacao'] != '') ? $_POST['idSolicitacao'] : null;
        $idSolicitacaoItem = ($_POST['idSolicitacaoItem'] != '') ? $_POST['idSolicitacaoItem'] : null;
        if ($idSolicitacao) {
            $model = new solicitacaocomprasModel();
            $dados = $model->getSolicitacaoCompras('a.idSolicitacao = ' . $idSolicitacao . ' and a.idSituacao < 4');
            if ($dados) {
                foreach ($dados as $value) {
                    $value['idPedido'] = '';
                    $value['idParceiro'] = '';
                    $value['dtPedido'] = '';
                    $value['dtPrazoEntrega'] = '';
                    $value['nrPedido'] = '';
                    $value['dsComplementoEntrega'] = '';
                    $value['idUsuarioSolicitante'] = $value['idUsuarioSolicitante'];
                    $value['idPrioridade'] = $value['idPrioridade'];
                    $id = $this->gravar_pedido_from_solicitacao($value);                  
                    if ($idSolicitacaoItem) {
                        $model = new solicitacaocomprasModel();
                        $dadositem = $model->getSolicitacaoComprasItens('a.idSolicitacaoItem = ' . $idSolicitacaoItem . ' and a.idSituacao < 4');
                        if ($dadositem) {
                            foreach ($dadositem as $valueitem) {
                                $valueitem['idPedido'] = $id;
                                $valueitem['qtPedido'] = $valueitem['qtSolicitacao'];
                                $valueitem['vlPedido'] = '';
                                $valueitem['idLocalEstoque'] = '';
                                $valueitem['dsObservacaoItem'] = $valueitem['dsObservacao'];
                                $id_item = $this->gravar_pedido_item_from_solicitacao($valueitem);                  

                                // colocar aqui onde devo gravar o numero do pedido no ITEM DA SOLICITACAO
                                
                                $dadospedido = array(
                                    'idPedido' => $id,
                                    'idPedidoItem' => $id_item,
                                    'idSituacao' => 5
                                );
                                $where = 'idSolicitacaoItem = ' . $valueitem['idSolicitacaoItem'];
                                $model->updSolicitacaoComprasItem($dadospedido, $where);
                                
                            }
                        }
                    } else {
                        $model = new solicitacaocomprasModel();
                        $dadositem = $model->getSolicitacaoComprasItens('a.idSolicitacao = ' . $idSolicitacao . ' and a.idSituacao < 4');
                        if ($dadositem) {
                            foreach ($dadositem as $valueitem) {
                                $valueitem['idPedido'] = $id;
                                $valueitem['qtPedido'] = $valueitem['qtSolicitacao'];
                                $valueitem['vlPedido'] = '';
                                $valueitem['idLocalEstoque'] = '';
                                $valueitem['dsObservacaoItem'] = $valueitem['dsObservacao'];
                                $valueitem['opcao'] = $valueitem['stTipoIS'];
                                $id_item = $this->gravar_pedido_item_from_solicitacao($valueitem);   
                                
                                // colocar aqui onde devo gravar o numero do pedido no ITEM DA SOLICITACAO
                                
                                $dadospedido = array(
                                    'idPedido' => $id,
                                    'idPedidoItem' => $id_item,
                                    'idSituacao' => 5
                                );
                                $where = 'idSolicitacaoItem = ' . $valueitem['idSolicitacaoItem'];
                                $model->updSolicitacaoComprasItem($dadospedido, $where);
                                
                            }
                        }
                    }
                    // colocar aqui onde devo gravar o numero do pedido na SOLICITACAO

                    $dadospedido = array(
                        'idSituacao' => 5
                    );
                    $where = 'idSolicitacao = ' . $value['idSolicitacao'];
                    $model->updSolicitacaoCompras($dadospedido, $where);                    
                }
            }
        }
        
        $_SESSION['pedido']['id'] = $id;
        $jsondata["html"] = "pedido/form_novo.tpl";
        $jsondata["idPedido"] = $id;
        $jsondata["ok"] = true;
        echo json_encode($jsondata);
        
    }

    public function lerbudgetsetor() {
        $idPedido = $_POST['idPedido'];
        $idSetor = $_POST['idSetor'];
        $idAnoMes = substr($_POST['dtVencimento'],6,4) . substr($_POST['dtVencimento'],3,2);
        $valorBudget = 0;
        $valorTotal = 0;
        $valorPedido = 0;
        $ativo = 0;

        $modelBudget = new setorbudgetModel();
        $pedidoModel = new pedidoModel();
        
        // ler o total do pedido
        $where = 'p.idPedido = ' . $idPedido;
        $retorno = $pedidoModel->getTotalPedido($where);
        if ($retorno) {
            $valorPedido = $retorno[0]['TotalPedido'];
        }
        
        // ler o budget para o setor
        $where = 's.idSetor = ' . $idSetor . ' and sb.idAnoMes = ' . $idAnoMes;
        $retorno = $modelBudget->getSetorBudget($where);
        if ($retorno) {
            $ativo = $retorno[0]['stBudget'];
            $valorBudget = $retorno[0]['vlBudget'];
        }
                
        // ler o budget ja usado na data para o setor, pegar todos os pedidos
        
        $where = "p.idPedido <> " . $idPedido . " and p.idSetor = " . $idSetor . " and concat(substr(p.dtVencimento,1,4), substr(p.dtVencimento,6,2)) = '" . $idAnoMes . "'";
        $retorno = $pedidoModel->getTotalPedido($where);
        if ($retorno) {
            $valorTotal = $retorno[0]['TotalPedido'];
        }
        $this->smarty->assign('vlBudgetUsado', $valorTotal);
        $this->smarty->assign('vlBudgetDisponivel', ($valorBudget - $valorTotal));
        $this->smarty->assign('vlSaldo', $valorBudget - ($valorTotal + $valorPedido));
        $this->smarty->assign('total', $valorPedido);
        $html = $this->smarty->fetch('pedido/pedido_compra/modal/budget.tpl');
        
        echo json_encode(array('ativo' => $ativo, 'html' => $html, 'vlSaldo' => $valorBudget - ($valorTotal + $valorPedido)));
    }
    
    public function ler_dados() {
        $idPedidoItem = $_POST['idPedidoItem'];
        $model = new pedidoModel();
        $dados = $model->getPedidoItens('a.idPedidoItem = ' . $idPedidoItem);
        
        $idGrupoDR = $dados[0]['idGrupoDR'];
        $modelItemDR = new grupodrModel();
        $lista_itemdr = array('' => 'SELECIONE');
        foreach ($modelItemDR->getItemDr("i.idGrupoDR = " . $idGrupoDR . " and i.dsCanal = 'compras' and g.stGrupoDR = 'D'") as $value) {
            $lista_itemdr[$value['idItemDR']] = $value['dsItemDR'];
        }
        
        $this->smarty->assign('lista_itemdr', $lista_itemdr);
        $html = $this->smarty->fetch('solicitacaocompras/listaItemDR.tpl');

        echo json_encode(array('dados' => $dados[0], 'html' => $html));
    }
    
    //Funcao de Inserir
    public function novo_pedido() {
         $sy = new system\System();
 
        if (!isset($_SESSION['pedido']['id'])) {
           $idPedido = $sy->getParam('idPedido');
        } else {
          if ($_SESSION['pedido']['id'] == 0) {
             $idPedido = null;
          } else {
            $idPedido = $_SESSION['pedido']['id'];            
          }
        }
        
        $model = new pedidoModel();
        if (isset($idPedido)) {
            if ((bool) $idPedido) {
                $registro = $model->getPedido('a.idPedido=' . $idPedido);  
                if ($registro) {
                    $registro = $registro[0];
                } else {
                    //Novo Registro
                    $registro = $model->estrutura_vazia();
                    $registro = $registro[0];                    
                }
            } else {
                //Novo Registro
                $registro = $model->estrutura_vazia();
                $registro = $registro[0];                    
            }
        } else {
            //Novo Registro
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        
//        var_dump($registro); 
//        $modelParceiro = new parceiroModel();
//        $lista_Parceiro = array('' => 'SELECIONE');
//        foreach ($modelParceiro->getParceiro() as $value) {
//            $lista_Parceiro[$value['idParceiro']] = $value['dsParceiro'];
//        }
        $modelUsuario = new usuariosModel();
        $lista_usuario = array('' => 'SELECIONE');
        foreach ($modelUsuario->getUsuario() as $value) {
            $lista_usuario[$value['idUsuario']] = $value['dsUsuario'];
        }
//        $modelInsumo = new insumoModel();
//        $lista_insumo = array('' => 'SELECIONE');
//        foreach ($modelInsumo->getInsumo() as $value) {
//            $lista_insumo[$value['idInsumo']] = $value['cdInsumo'] . '-' . $value['dsInsumo'];
//        }
        $modelCentroCusto = new centrocustoModel();
        $lista_centrocusto = array('' => 'SELECIONE');
        foreach ($modelCentroCusto->getCentroCustoCombo() as $value) {
            $lista_centrocusto[$value['idCentroCusto']] = $value['codigocusto'];
        }
        $modelGrupoDR = new grupodrModel();
        $lista_grupodr = array('' => 'SELECIONE');
        foreach ($modelGrupoDR->getGrupoDR() as $value) {
            $lista_grupodr[$value['idGrupoDR']] = $value['dsGrupoDR'];
        }
//        $motivolOS = new motivoModel();
//        $lista_Motivo = array('' => 'SELECIONE');
//        foreach ($motivolOS->getMotivo() as $value) {
//            $lista_Motivo[$value['idMotivo']] = $value['dsMotivo'];
//        }
        $modellocalestoque = new localestoqueModel();
        $lista_localestoque = array('' => 'SELECIONE');
        foreach ($modellocalestoque->getLocalEstoque() as $value) {
            $desanterior = $modellocalestoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
            if ($desanterior) {
                if ($value['idLocalEstoque'] = $value['idLocalEstoqueSuperior']) {
                    $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
                } else {
                    $lista_localestoque[$value['idLocalEstoque']] = $desanterior[0]['dsLocalEstoque'] . '-' . $value['dsLocalEstoque'];
                }
            } else {
                $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
            }
        }
        $pModel = new prioridadeModel();        
        $lista_Prioridade = array('' => 'SELECIONE');
        foreach ($pModel->getPrioridade() as $value) {
            $lista_Prioridade[$value['idPrioridade']] = $value['dsPrioridade'];
        }
        
        $pedidoitens = array();
        $valortotal = null;
        if($idPedido) {
            $where = "a.idPedido = " . $idPedido;
            $pedidoitens = $model->getPedidoItens($where);
            $totalpedido = $model->getTotalPedidoItens($where);
            $valortotal = $totalpedido[0]['totalpedido'];
        }        
        
//        $this->smarty->assign('nrPedido', null);
//        if (!$idPedido) {
//            $nrUltimoPedido = $model->getUltimoPedido()[0];
//            if ($nrUltimoPedido['ultimo']) {                
//                $registro['nrPedido'] = ($nrUltimoPedido['ultimo'] !='') ? $nrUltimoPedido['ultimo'] + 1 :null;
//            }            
//        }
//        
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('lista_prioridade', $lista_Prioridade);
//        $this->smarty->assign('lista_Parceiro', $lista_Parceiro);
        $this->smarty->assign('lista_usuario', $lista_usuario);
//        $this->smarty->assign('lista_insumo', $lista_insumo);
        $this->smarty->assign('lista_localestoque', $lista_localestoque);
        $this->smarty->assign('lista_centrocusto', $lista_centrocusto);
        $this->smarty->assign('lista_grupodr', $lista_grupodr);
        $this->smarty->assign('lista_itemdr', null);
//        $this->smarty->assign('lista_motivo', $lista_Motivo);
        $this->smarty->assign('pedidoitens', $pedidoitens);
        $this->smarty->assign('totalpedido', $valortotal);
        $this->smarty->assign('title', 'Novo Pedido de Compra');
        $this->smarty->display('pedido/form_novo.tpl');
    }
    // Gravar Padrao
    public function novopedido() {        
        
        $_SESSION['pedido']['id'] = 0;
        $jsondata["idPedido"] = null;
        $jsondata["ok"] = true;

        echo json_encode($jsondata);
    }
    
    public function lerusuario() {
        $idUsuario = $_POST['idUsuario'];
        $modelUsuario = new usuariosModel();
        $retorno = $modelUsuario->getUsuario('L.idUsuario = ' . $idUsuario);
        $retorno = array(
            'nomeusuario' => $retorno[0]['dsUsuario']
        );
        echo json_encode($retorno);
    }
    
//    public function atualiza_Parceiroes() {
//        $modelParceiro = new parceiroModel();
//        $lista_Parceiro = array('' => 'SELECIONE');
//        foreach ($modelParceiro->getParceiro() as $value) {
//            $lista_Parceiro[$value['idParceiro']] = $value['dsParceiro'];
//        }
//        $this->smarty->assign('lista_Parceiro', $lista_Parceiro);
//        $html = $this->smarty->fetch('pedido/lista_Parceiroes.tpl');
//        $retorno = array(
//            'html' => $html
//        );
//        echo json_encode($retorno);
//    }
//    
//    public function atualiza_produtos() {
//        $opcao = $_POST['opcao'];
//        
//        if ($opcao == '0') { // ler produtos
//            $modelInsumo = new insumoModel();
//            $lista_insumo = array('' => 'SELECIONE');
//            foreach ($modelInsumo->getInsumo() as $value) {
//                $lista_insumo[$value['idInsumo']] = $value['cdInsumo'] . '-' . $value['dsInsumo'];
//            }
//        } else { // ler servicos
//            $modelInsumo = new servicoModel();
//            $lista_insumo = array('' => 'SELECIONE');
//            foreach ($modelInsumo->getServico() as $value) {
//                $lista_insumo[$value['idServico']] = $value['dsServico'];
//            }
//        }
//        $this->smarty->assign('lista_insumo', $lista_insumo);
//        $html = $this->smarty->fetch('pedido/lista_produtos.tpl');
//        $retorno = array(
//            'html' => $html
//        );
//        echo json_encode($retorno);
//    }
//    
    public function alterar_financeiro() { 
    //    var_dump($_POST); die;
        $idFinanceiroParcela = $_POST['idFinanceiroParcela'];
        $dados = array();
        $dados['dtVencimento'] = ($_POST['dtVencimento'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $_POST["dtVencimento"]))) : date('Y-m-d h:m:s');
        $dados['vlParcela']  = ($_POST['vlParcela'] != '') ? $_POST['vlParcela'] : null;
        
        $model = new pedidoModel();
        $where = 'idFinanceiroParcela = ' . $idFinanceiroParcela;             
        $model->updFinanceiroItem($dados, $where);
        $jsondata["ok"] = true;
        echo json_encode($jsondata);        
    }

    public function lerunidade() {
        $idInsumo = $_POST['idInsumo'];
        $dsUnidade = null;
        $qtEstoque = null;
        $modelUnidade = new unidadeModel();
        $where = 'idInsumo = ' . $idInsumo;
        $retorno = $modelUnidade->getInsumoUnidade($where);
        if ($retorno) {
            $dsUnidade = $retorno[0]['dsUnidade'];
            $qtEstoque = $retorno[0]['qtEstoque'];
        }
        $jsondata["dsUnidade"] = $dsUnidade;
        $jsondata["qtEstoque"] = $qtEstoque;
        $jsondata["ok"] = true;
        echo json_encode($jsondata);
    }
    
    public function desabilitaid() {
        unset($_SESSION['pedido']['id']);
        echo json_encode(true);
    }
    
    public function gravar_pedido() {
        
        $model = new pedidoModel();
        $data = $this->trataPost($_POST);

        if ($_POST['idPedido'] == '') {
            $id = $model->setPedido($data);
        } else {
            $id = $_POST['idPedido'];
            $where = 'idPedido = ' . $id;
            $model->updPedido($data, $where);            
        }
        
        $_SESSION['pedido']['id'] = $id;
        $jsondata["html"] = "pedido/form_novo.tpl";
        $jsondata["idPedido"] = $id;
        $jsondata["ok"] = true;
        echo json_encode($jsondata);
    }

    public function gravar_pedido_from_solicitacao($dados) {
        
        $model = new pedidoModel();
        $data = $this->trataPost($dados);
        $id = $model->setPedido($data);
        
        return $id;
    }

    public function gravar_item() {
        $model = new pedidoModel();
        $data = $this->trataPostItem($_POST);
        if ($data['idPedidoItem']) {
            $id = $data['idPedidoItem'];
            $where = 'idPedidoItem = ' . $id;
            $model->updPedidoItem($data, $where);
        } else {
            $data['idSituacaoPedido'] = 1;            
            $id = $model->setPedidoItem($data);
        }            

        // GRAVAR O Parceiro NO PRODUTO
        
        $modelinsumo = new insumoModel();
        $dados = array('idParceiro' => $this->idParceiro, 'idInsumo' => $data['idInsumo']);
        $modelinsumo->updInsumo($dados);
        
        $jsondata["html"] = "pedido/lista.tpl";
        $jsondata["idPedidoItem"] = $id;
        $jsondata["ok"] = true;
        echo json_encode($jsondata);
    }
    
    public function gravar_pedido_item_from_solicitacao($dados) {
        $data = $this->trataPostItem($dados);

        // GRAVAR O Parceiro NO PRODUTO
        
        $modelinsumo = new insumoModel();
        $dados = array('idParceiro' => $this->idParceiro, 'idInsumo' => $dados['idInsumo']);
        $modelinsumo->updInsumo($dados);

        $data['idSituacaoPedido'] = 1;                    
        $id = $model->setPedidoItem($data);    

        return $id;
    }
    
    public function gravar_financeiro() {
        $model = new pedidoModel();
        $totalpedido = $model->getTotalPedidoItens('i.idPedido = ' . $_POST['idPedido'] . ' and i.idSituacaoPedido = 1')[0];
        $data = $this->trataPostFinanceiro($_POST);
        $id = $model->setPedidoFinanceiro($data);
        $dataParcela = array();
        $dataParcela['idFinanceiroParcela'] = null;
        $dataParcela['idFinanceiro'] = $id;
        $datavencimento = $_POST['dtPrimeiroVencimento'];
        for ($x=0;$x<$_POST['qtParcelas'];$x++){
            $valor = ($totalpedido['totalpedido'] / $_POST['qtParcelas']);
            $dataParcela['vlParcela'] = $valor;
            $dataParcela['nrParcela'] = $x + 1;
            $dataParcela['dtVencimento'] = $datavencimento;
            $model->setPedidoFinanceiroItem($dataParcela);
        }        
            
        $jsondata["html"] = "pedido/financeiro.tpl";
        $jsondata["idFinanceiro"] = $id;
        $jsondata["ok"] = true;
        echo json_encode($jsondata);
    }
        
    //Trata dados antes de Enviar para o Gravar
    private function trataPost($post) {
        $data = array();
        $data['idPedido'] = ($post['idPedido'] != '') ? $post['idPedido'] : null;;
        $data['idParceiro'] = ($post['idParceiro'] != '') ? $post['idParceiro'] : null;
        $data['dsObservacao'] = ($post['dsObservacao'] != '') ? $post['dsObservacao'] : null;
        $data['dtPedido'] = ($post['dtPedido'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $post["dtPedido"]))) : date('Y-m-d h:m:s');
        $data['dtPrazoEntrega'] = ($post['dtPrazoEntrega'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $post["dtPrazoEntrega"]))) : date('Y-m-d h:m:s');
        $data['nrPedido'] = ($post['idPedido'] != '') ? $post['idPedido'] : null;
        $data['dsComplementoEntrega'] = ($post['dsComplementoEntrega'] != '') ? $post['dsComplementoEntrega'] : null;
        $data['dsSolicitante'] = ($post['dsSolicitante'] != '') ? $post['dsSolicitante'] : null;
        $data['idUsuarioSolicitante'] = ($post['idUsuarioSolicitante'] != '') ? $post['idUsuarioSolicitante'] : null;
        $data['idPrioridade'] = ($post['idPrioridade'] != '') ? $post['idPrioridade'] : null;
        $data['idUsuario'] = $_SESSION['user']['usuario'];
        return $data;
    }

    private function trataPostFinanceiro($post) {
        $data = array();
        $data['idFinanceiro'] = null;
        $data['idPedido'] = ($post['idPedido'] != '') ? $post['idPedido'] : null;
        $data['qtParcelas'] = ($post['qtParcelas'] != '') ? $post['qtParcelas'] : 1;
        $data['dsObservacao'] = ($post['dsObservacao'] != '') ? $post['dsObservacao'] : null;
        $data['dtPrevisaoEntrega'] = ($post['dtPrevisaoEntrega'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $post["dtPrevisaoEntrega"]))) : date('Y-m-d h:m:s');
        $data['dtPrimeiroVencimento'] = ($post['dtPrimeiroVencimento'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $post["dtPrimeiroVencimento"]))) : date('Y-m-d h:m:s');
        return $data;
    }

    private function trataPostItem($post) {
        $data = array();        
        $data['idInsumo'] = ($post['idInsumo'] != '') ? $post['idInsumo'] : null;
        $data['qtPedido'] = ($post['qtPedido'] != '') ? str_replace(",",".",str_replace(".","",$post['qtPedido'])) : null;
        $data['vlPedido'] = ($post['vlPedido'] != '') ? str_replace(",",".",str_replace(".","",$post['vlPedido'])) : null;
        $data['idPedido'] = ($post['idPedido'] != '') ? $post['idPedido'] : null;
        $data['idPedidoItem'] = ($post['idPedidoItem'] != '') ? $post['idPedidoItem'] : null;
        $data['idItemDR'] = ($post['idItemDR'] != '') ? $post['idItemDR'] : null;
        $data['dsJustificativa'] = ($post['dsJustificativa'] != '') ? $post['dsJustificativa'] : null;
        $data['idGrupoDR'] = ($post['idGrupoDR'] != '') ? $post['idGrupoDR'] : null;
        $data['idCentroCusto'] = ($post['idCentroCusto'] != '') ? $post['idCentroCusto'] : null;
        $data['idLocalEstoque'] = ($post['idLocalEstoque'] != '') ? $post['idLocalEstoque'] : null;
        $data['dsObservacao'] = ($post['dsObservacaoItem'] != '') ? $post['dsObservacaoItem'] : null;
        $data['stTipoIS'] = ($post['opcao'] != '') ? $post['opcao'] : null;
        $this->idParceiro = ($post['idParceiro'] != '') ? $post['idParceiro'] : null;
        return $data;
    }

    // Remove Padrao
    public function delpedidoitem() {                
        $idPedidoItem = $_POST['idPedidoItem'];
        $model = new pedidoModel();
        $modelSC = new solicitacaocomprasModel();
        $where = 'idPedidoItem = ' . $idPedidoItem;             
        $model->delPedidoItem($where);
        $jsondata["ok"] = true;
        
        
        // colocar aqui onde devo gravar o numero do pedido no ITEM DA SOLICITACAO

        $dadospedido = array(
            'idPedido' => '',
            'idPedidoItem' => '',
            'idSituacao' => 1
        );
        $where = 'idPedidoItem = ' . $idPedidoItem;
        $modelSC->updSolicitacaoComprasItem($dadospedido, $where);
        
        echo json_encode($jsondata);        
    }

    public function delfinanceiroitem() {                
        $idFinanceiroParcela = $_POST['idFinanceiroParcela'];
        $model = new pedidoModel();
        $where = 'idFinanceiroParcela = ' . $idFinanceiroParcela;             
        $model->delFinanceiroItem($where);
        $jsondata["ok"] = true;
        echo json_encode($jsondata);        
    }

    public function delpedido() {  
        $sy = new system\System();
        
        $idPedido = $sy->getParam('idPedido');
        $model = new pedidoModel();
        $where = 'idPedido = ' . $idPedido;  
        $data = array(
          'idSituacaoPedido' => 6  
        );
        $model->updPedidoItem($data, $where);
        $model->updPedido($data, $where);
        header('Location: /pedidoaberto');        
        return;
    }

    public function delpedidoencerrado() {   
        $sy = new system\System();
        
        $idPedido = $sy->getParam('idPedido');
        $model = new pedidoModel();
        $where = 'idPedido = ' . $idPedido;  
        $data = array(
          'idSituacaoPedido' => 6  
        );
        $model->updPedidoItem($data, $where);
        $model->updPedido($data, $where);
        header('Location: /pedidofechado');     
        return;
    }

    public function financeiro() {    
                $sy = new system\System();

        $idPedido = $sy->getParam('idPedido');
        
        $model = new pedidoModel();
        $registro = $model->getFinanceiro('idPedido = ' . $idPedido);
        if ($registro) {
            $registro = $registro[0];
            $financeiroitens = $model->getFinanceiroParcelas('idFinanceiro = ' . $registro['idFinanceiro']);
            $financeiroitens = $financeiroitens;
        } else {
            $registro = null;
            $financeiroitens = null;
        }
        $totalpedido = $model->getTotalPedidoItens('i.idPedido = ' . $idPedido . ' and i.idSituacaoPedido = 1');
        if ($totalpedido) {
            $totalpedido = $totalpedido[0]['totalpedido'];
        } else {
            $totalpedido = null;
        }
        $this->smarty->assign('registrofinanceiro', $registro);
        $this->smarty->assign('financeiroitens', $financeiroitens);
        $this->smarty->assign('title', 'Financeiro');
        $this->smarty->assign('idPedido', $idPedido);
        $this->smarty->assign('totalpedido', $totalpedido);
        $this->smarty->display('pedido/financeiro.tpl');
    }

    public function baixamanual() {  
        $sy = new system\System();
        
        $idPedido = $sy->getParam('idPedido');
        $model = new pedidoModel();
        $where = 'idPedido = ' . $idPedido;             
        $dados = array('idSituacaoPedido' => 4, 'idUsuarioBaixa' => $_SESSION['user']['usuario'], 'dtBaixa' => date('Y-m-d h:m:s'), 'nrNota' => '', 'idOrigemInformacao' => 1);
        $model->updPedido($dados, $where);
        header('Location: /pedidoaberto');        
        return;
    }
    public function relatoriopedido_pre() {
        $this->template->run();

        $this->smarty->assign('title', 'Pre Relatorio de Pedidos');
        $this->smarty->display('pedido/relatorio_pre.html');
    }

    public function emitirpedidocompra() {
        $idPedido = $_POST['idPedido'];

        $where = 'a.idPedido = ' . $idPedido;
        $model = new pedidoModel();
        $cabec = $model->getPedido($where)[0];
        $resultado = $model->getPedidoItens($where);
        
        $_SESSION['emitir_pedido']['idPedido'] = $idPedido;
        
        $lista_emp = $this->carrega_empresas();
        $lista_setor = $this->carrega_setor();
        
        $this->smarty->assign('lista_empresa', $lista_emp);
        $this->smarty->assign('lista_setor', $lista_setor);
        $this->smarty->assign('cabec', $cabec);
        $this->smarty->assign('pedidoitens', $resultado);
        $this->smarty->display("pedido/pedido_compra/modal/thumbnail.tpl");        
    }
    
    public function emitir_pedido_compra() {
        
        $idPedido = ($_POST['idPedido'] != '') ? $_POST['idPedido'] : null;
        $idParceiro = ($_POST['idParceiro'] != '') ? $_POST['idParceiro'] : null;
        $idEmpresa = ($_POST['idEmpresa'] != '') ? $_POST['idEmpresa'] : null;
        $idSetor = ($_POST['idSetor'] != '') ? $_POST['idSetor'] : null;
        $nrOrcamento =  ($_POST['nrOrcamento'] != '') ? $_POST['nrOrcamento'] : null;
        $dsContato =  ($_POST['dsContato'] != '') ? $_POST['dsContato'] : null;
        $dsFone = ($_POST['dsFone'] != '') ? $_POST['dsFone'] : null;
        $dsCelular =  ($_POST['dsCelular'] != '') ? $_POST['dsCelular'] : null;
        $dsEmail =  ($_POST['dsEmail'] != '') ? $_POST['dsEmail'] : null;
        $dsEndereco =  ($_POST['dsEndereco'] != '') ? $_POST['dsEndereco'] : null;
        $dsCidade =  ($_POST['dsCidade'] != '') ? $_POST['dsCidade'] : null;
        $dsFrete =  ($_POST['dsFrete'] != '') ? $_POST['dsFrete'] : null;
        $dsLocalEntrega =  ($_POST['dsLocalEntrega'] != '') ? $_POST['dsLocalEntrega'] : null;
        $cdCNPJ =  ($_POST['cdCNPJ'] != '') ? $_POST['cdCNPJ'] : null;
        $dtPrazoEntrega =  ($_POST['dtPrazoEntrega'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $_POST["dtPrazoEntrega"]))) : date('Y-m-d h:m:s');
        $dtVencimento =  ($_POST['dtVencimento'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $_POST["dtVencimento"]))) : date('Y-m-d h:m:s');
        $dsCondicoesPagamento =  ($_POST['dsCondicoesPagamento'] != '') ? $_POST['dsCondicoesPagamento'] : null;
        $dsObservacoes = ($_POST['dsObservacoes'] != '') ? $_POST['dsObservacoes'] : null;
        
        $dataParceiro = array(
            'idParceiro' => $idParceiro,
            'dsContato' => $dsContato,
            'dsFone' => $dsFone,
            'dsCelular' => $dsCelular,
            'dsEmail' => $dsEmail,
            'dsEndereco' => $dsEndereco,
            'dsCidade' => $dsCidade,
            'cdCNPJ' => $cdCNPJ
        );
        
        $modelParceiro = new parceiroModel();
        $modelParceiro->updParceiro($dataParceiro);
        
        $dataPedido = array(
            'nrOrcamentoParceiro' => $nrOrcamento,
            'dtPrazoEntrega' => $dtPrazoEntrega,
            'dtVencimento' => $dtVencimento,
            'dsCondicoesPagamento' => $dsCondicoesPagamento,
            'dsLocalEntrega' => $dsLocalEntrega,
            'dsFrete' => $dsFrete,
            'idEmpresa' => $idEmpresa,
            'idSetor' => $idSetor,
            'dsObservacaoEmissao' => $dsObservacoes
        );
        
        $modelPedido = new pedidoModel();
        $modelPedido->updPedido($dataPedido, 'idPedido = ' . $idPedido);
        
        $url = $this->enviarPDF($idPedido);
        $retorno = array();

//        $sql = $_SESSION['solicitacao']['where'];
//        $resultado = $model->getSolicitacaoComprasManutencao($sql, $paginacao=true);
//        $this->smarty->assign('solicitacaocompras', $resultado);
//        $html = $this->smarty->fetch('solicitacaocompras/listaManutencao.html');
//        
//        $retorno = array(
//            'url' => 'http://local.thopos.com.br/' . $url,
//            'html' => $html
//        );
        echo json_encode($retorno);        
    }

    public function emitir_orcamento_compra() {
        
        $idOrcamentoCompras = ($_POST['idOrcamentoCompras'] != '') ? $_POST['idOrcamentoCompras'] : null;
        $idParceiro = ($_POST['idParceiro'] != '') ? $_POST['idParceiro'] : null;
        $idEmpresa = ($_POST['idEmpresa'] != '') ? $_POST['idEmpresa'] : null;
        $dsContato =  ($_POST['dsContato'] != '') ? $_POST['dsContato'] : null;
        $dsFone = ($_POST['dsFone'] != '') ? $_POST['dsFone'] : null;
        $dsCelular =  ($_POST['dsCelular'] != '') ? $_POST['dsCelular'] : null;
        $dsEmail =  ($_POST['dsEmail'] != '') ? $_POST['dsEmail'] : null;
        $dsEndereco =  ($_POST['dsEndereco'] != '') ? $_POST['dsEndereco'] : null;
        $dsCidade =  ($_POST['dsCidade'] != '') ? $_POST['dsCidade'] : null;
        $dsLocalEntrega =  ($_POST['dsLocalEntrega'] != '') ? $_POST['dsLocalEntrega'] : null;
        $cdCNPJ =  ($_POST['cdCNPJ'] != '') ? $_POST['cdCNPJ'] : null;
        $dtPrazoEntrega =  ($_POST['dtPrazoEntrega'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $_POST["dtPrazoEntrega"]))) : date('Y-m-d h:m:s');
        $dsCondicoesPagamento =  ($_POST['dsCondicoesPagamento'] != '') ? $_POST['dsCondicoesPagamento'] : null;
        $dsObservacoes = ($_POST['dsObservacoes'] != '') ? $_POST['dsObservacoes'] : null;
        
        $dataParceiro = array(
            'idParceiro' => $idParceiro,
            'dsContato' => $dsContato,
            'dsFone' => $dsFone,
            'dsCelular' => $dsCelular,
            'dsEmail' => $dsEmail,
            'dsEndereco' => $dsEndereco,
            'dsCidade' => $dsCidade,
            'cdCNPJ' => $cdCNPJ
        );
        
        $modelParceiro = new parceiroModel();
        $modelParceiro->updParceiro($dataParceiro);
        
        $dataOrcamento = array(
            'dtPrazoEntrega' => $dtPrazoEntrega,
            'dsCondicoesPagamento' => $dsCondicoesPagamento,
            'dsObservacao' => $dsObservacoes
        );
        
        $model = new solicitacaocomprasModel();

        $model->updOrcamentoComprasParceiro($dataOrcamento, 'idOrcamentoCompras = ' . $idOrcamentoCompras . ' and idParceiro = ' . $idParceiro);
        
        $dataOrcamento = array(
            'idEmpresa' => $idEmpresa,
            'dsLocalEntrega' => $dsLocalEntrega
        );
        
        $model->updOrcamentoCompras($dataOrcamento, 'idOrcamentoCompras = ' . $idOrcamentoCompras);
        
        $url = $this->enviarOrcamentoPDF($idOrcamentoCompras, $idParceiro);
        $retorno = array();

        echo json_encode($retorno);        
    }
    
    private function carrega_empresas() {
        $lista_emp = null;
        $model = new empresaModel();
        $lista_emp = array('' => 'SELECIONE');
        foreach ($model->getEmpresa() as $value) {
            $lista_emp[$value['idEmpresa']] =  $value['dsEmpresa'];
        }
        return $lista_emp;
    }
    
    private function carrega_setor() {
        $lista_setor = null;
        $model = new setorModel();
        $lista_setor = array('' => 'SELECIONE');
        foreach ($model->getSetor() as $value) {
            $lista_setor[$value['idSetor']] =  $value['dsSetor'];
        }
        return $lista_setor;
    }
    
    public function enviarPDF($idPedido) {
        $idPedido = ($_POST['idPedido'] != '') ? $_POST['idPedido'] : null;
        global $PATH;
        $caminho = $GLOBALS['PATH'];
        if (!is_dir($this->pastaPedidos)) {
            mkdir($this->pastaPedidos, 0777, true);
        }
        $url = null;
        if (file_exists($this->pastaPedidos)) {
            
            $where = 'a.idPedido = ' . $idPedido;
            $model = new pedidoModel();
            $cabec = $model->getPedidoCabec($where)[0];
            $pedidoitens = $model->getPedidoItens($where);
//            $empresa = $modelEmpresa->getEmpresa("idEmpresa = " . $idEmpresa);
//            var_dump($cabec);
//            var_dump($cabec); die;
            $this->smarty->assign("pedido", $cabec);
            $this->smarty->assign("pedidoitens", $pedidoitens);
//            $this->smarty->assign("empresa", $empresa);

            // Filename
            $filename = "PDF{$idPedido}";
            // Dependecias
            require_once 'system/libs/mpdf/mpdf.php';
            $mpdf = new mPDF('c', 'A4', '5', 'Arial');

            $template = "pedido/pedido_compra/PDFTemplate.tpl";

            $this->smarty->clearCache($template);
            $mpdf->WriteHTML($this->smarty->fetch($template));
            if (file_exists("{$this->pastaPedidos}{$filename}.pdf")) {
                unlink("{$this->pastaPedidos}{$filename}.pdf");
            }
            // Arquivo
            $mpdf->Output("{$this->pastaPedidos}{$filename}.pdf", 'F');

            if(file_exists("{$this->pastaPedidos}{$filename}.pdf")  ){
                $url = "{$this->pastaPedidos}{$filename}.pdf";
            }
            // ATUALIZR O CAMINHO DO PDF NO PEDIDO
            $data = array(
                'dsCaminhoPDF' => $url
            );
            $model->updPedido($data, 'idPedido = ' . $idPedido);
        }
        
        return $url;
    }    
    public function enviarOrcamentoPDF($idOrcamentoCompras, $idParceiro) {
        $idOrcamentoCompras = ($_POST['idOrcamentoCompras'] != '') ? $_POST['idOrcamentoCompras'] : null;
        $idParceiro = ($_POST['idParceiro'] != '') ? $_POST['idParceiro'] : null;
        global $PATH;
        $caminho = $GLOBALS['PATH'];
        if (!is_dir($this->pastaPedidos)) {
            mkdir($this->pastaPedidos, 0777, true);
        }
        $url = null;
        if (file_exists($this->pastaPedidos)) {
            
            $where = 'oc.idOrcamentoCompras = ' . $idOrcamentoCompras . ' and ocp.idParceiro = ' . $idParceiro;
            $model = new solicitacaocomprasModel();
            $cabec = $model->getSolicitacaoComprasOrcamento($where)[0];
            $where = 'oc.idOrcamentoCompras = ' . $idOrcamentoCompras;
            $pedidoitens = $model->getOrcamentoComprasItens($where);
        //    var_dump($cabec); die;
            $this->smarty->assign("pedido", $cabec);
            $this->smarty->assign("pedidoitens", $pedidoitens);
            // Filename
            $filename = "OCPDF{$idOrcamentoCompras}";
            // Dependecias
            require_once 'system/libs/mpdf/mpdf.php';
            $mpdf = new mPDF('c', 'A4', '5', 'Arial');

            $template = "orcamentocompras/orcamento/PDFTemplate.tpl";

            $this->smarty->clearCache($template);
            $mpdf->WriteHTML($this->smarty->fetch($template));
            if (file_exists("{$this->pastaPedidos}{$filename}.pdf")) {
                unlink("{$this->pastaPedidos}{$filename}.pdf");
            }
            // Arquivo
            $mpdf->Output("{$this->pastaPedidos}{$filename}.pdf", 'F');

            if(file_exists("{$this->pastaPedidos}{$filename}.pdf")  ){
                $url = "{$this->pastaPedidos}{$filename}.pdf";
            }
            // ATUALIZR O CAMINHO DO PDF NO PEDIDO
            $data = array(
                'dsCaminhoPDF' => $url
            );
            $model->updOrcamentoComprasParceiro($data, 'idOrcamentoCompras = ' . $idOrcamentoCompras . ' and idParceiro = ' . $idParceiro);
        }
        
        return $url;
    }    

    public function lerempresa() {
        $retorno = array(
            'dsLocalEntrega' => null
        );
        $idEmpresa = $_POST['idEmpresa'];
        $model = new empresaModel();
        $dados = $model->getEmpresa('idEmpresa = ' . $idEmpresa);
        if ($dados) {
            $retorno['dsLocalEntrega'] = $dados[0]['dsEndereco'] . ' - ' . $dados[0]['dsBairro'] . ' - ' . $dados[0]['dsCidade'];
        }
        echo json_encode($retorno);
    }
    
    public function gravar_recebimento() {

        $idPedido = $_SESSION['baixa_pedido']['idPedido'];
        
        $nrNota = $_POST['nrNota'];
        $valores = ($_POST['valor'] != '') ? str_replace(",",".",str_replace(".","",$_POST['valor'])) : null;
        $quantidades = ($_POST['qtde'] != '') ? str_replace(",",".",str_replace(".","",$_POST['qtde'])) : null;
        if (!isset($_POST['localestoque'])) {
            $localestoque = null;
        } else {
            $localestoque = $_POST['localestoque'];
        }
        $centrocusto = $_POST['centrocusto'];
        if (!isset($_POST['ncm'])) {
            $ncm = null;
        } else {
            $ncm = $_POST['ncm'];
        }
        // vamos ler o pedido
        $where = 'i.idPedido = ' . $idPedido;
        $modelPedido = new pedidoModel();
        $retornoPedido = $modelPedido->getPedido($where)[0];
        if ($retornoPedido) {

            // vamos gravar o cabecalho do movimento, logo, vamos gravar os itens do movimento
            $modelMovimento = new entradaestoqueModel();
            $data = array();
            $data['idMovimento'] = null;
            $data['idParceiro'] = $retornoPedido['idParceiro'];
            $data['idTipoMovimento'] = 1;
            $data['dsObservacao'] = 'ENTRADA EM ESTOQUE VIA TELA DE PEDIDO';
            $data['dtMovimento'] = date('Y-m-d');
            $data['nrNota'] = $nrNota;
            $data['nrPedido'] = $idPedido;
            $data['idUsuario'] = $_SESSION['user']['usuario'];
            $id = $modelMovimento->setMovimento($data);
        }
        
        foreach ($quantidades as $key => $value) {
            if ($localestoque[$key]) {
                $this->baixarpedido_recebimento($idPedido,$nrNota,$key,$localestoque[$key], $value, $valores[$key], $centrocusto[$key], $id, $ncm[$key]);
            } else {
                if (!$ncm[$key]) {
                    // baixar manual pois se trata de serviço
                    $dadosBM = array(
                        'idSituacaoPedido' => 4
                    );
                    $where = 'idPedido = ' . $idPedido . ' and idPedidoItem = ' . $key;
                    $modelPedido->updPedidoItem($dadosBM, $where);      
                    $modelS = new solicitacaocomprasModel();
                    $dadosBM = array(
                        'idSituacao' => 7
                    );
                    $where = 'idPedido = ' . $idPedido . ' and idPedidoItem = ' . $key;
                    $modelS->updSolicitacaoComprasItemManut($dadosBM, $where);  
                    
                    // ler usuario da solictiacao para enviar msg
                    $where = 'a.idPedido = ' . $idPedido . ' and a.idPedidoItem = ' . $key;
                    $dadossci = $modelS->getSolicitacaoComprasItens($where);
                    if ($dadossci) {
                        if ($dadossci[0]['idUsuarioSolicitante'] <> $_SESSION['user']['usuario']) {            
                            $dadosim = array(
                                'idUsuarioOrigem' => 999,
                                'idUsuarioDestino' => $dadossci[0]['idUsuarioSolicitante'],
                                'dsNomeTabela' => 'prodSolicitacaoCompras',
                                'idOrigemInformacao' => 99,
                                'idTabela' => $dadossci[0]['idSolicitacaoItem'],
                                'idTipoMensagem' => 0,
                                'stSituacao' => 0,
                                'dtEnvio' => date('Y-m-d H:i:s'),
                                'dsMensagem' => 'SEU PEDIDO DE SERVIÇO ' . $dadossci[0]['dsProduto'] . ' FOI BAIXADO'
                            );
                            $this->criarMensagem($dadosim);
                        }
                    }        
                }
            }
        }
        
        // verificar se fechou todo o pedido ou ficou algum item em aberto
        
        $temParcial = 0;
        $sql = 'a.idPedido = ' . $idPedido . ' and a.stTipoIS = 0';
        $retornoPedido = $modelPedido->getPedidoItens($sql);
        if ($retornoPedido) {
            // baixa os itens do pedido
            foreach ($retornoPedido as $value) {
                if ($value['qtEntregue'] < $value['qtPedido']) {
                    $dadosBaixa = array(
                        'idSituacaoPedido' => 2
                    );
                    $temParcial = 1;
                } else {
                    $dadosBaixa = array(
                       'idSituacaoPedido' => 3
                    );
                }
                $where = 'idPedidoItem = ' . $value['idPedidoItem'];
                $modelPedido->updPedidoItem($dadosBaixa,$where);
            }
        }   
        
        $sql = 'a.idPedido = ' . $idPedido;
        $retornoPedido = $modelPedido->getPedido($sql);
        if ($retornoPedido) {
            if ($temParcial == 1) {
                $dadosBaixa = array(
                    'idSituacaoPedido ' => 2
                );
            } else {
                $dadosBaixa = array(
                    'idSituacaoPedido ' => 3
                );
            }
            $where = 'idPedido = ' . $idPedido;
            $modelPedido->updPedido($dadosBaixa,$where);
        }    
        header('Location: /pedidoaberto');        
        return;
                
    }
    
    private function baixarpedido_recebimento($idPedido, $nrNota, $idPedidoItem, $idLocalEstoque, $qtde, $valor_pedido, $idCentroCusto, $idMovimento, $ncm) {
        if ($idCentroCusto == '') {
            $idCentroCusto=null;
        }
        $modelEstoque = new estoqueModel();
        
        // baixar pedido        
        
        // vamos ler o pedido
        $where = 'i.idPedido = ' . $idPedido . ' and i.idPedidoItem = ' . $idPedidoItem;
        $modelPedido = new pedidoModel();
        $retornoPedido = $modelPedido->getPedido($where);

        if ($retornoPedido) {
            $qtPedido = ($retornoPedido[0]['qtPedido'] + ($retornoPedido[0]['qtEntregue']=!null?$retornoPedido[0]['qtEntregue']:0));
            $vlPedido = ($retornoPedido[0]['vlPedido'] + ($retornoPedido[0]['vlEntregue']=!null?$retornoPedido[0]['vlEntregue']:0));
            if ($qtPedido <> $qtde) {
                $situacao = 2;
            } else {
                $situacao = 3;
            }
            $qtparabaixa = ($qtde + ($retornoPedido[0]['qtEntregue']=!null?$retornoPedido[0]['qtEntregue']:0));
            $vlparabaixa = ($valor_pedido + ($retornoPedido[0]['vlEntregue']=!null?$retornoPedido[0]['vlEntregue']:0));
            $dadosBaixa = array(
                'idSituacaoPedido ' => $situacao,
                'qtEntregue ' => $qtparabaixa,
                'vlEntregue ' => $vlparabaixa,
                'nrNota ' => $nrNota,
                'dtEntrega ' => date('Y-m-d')
            );

            // baixa o pedido

            $where = 'idPedidoItem = ' . $idPedidoItem;
            $modelPedido->updPedidoItem($dadosBaixa,$where);

            $where = 'idPedido = ' . $idPedido;
            $dadosBaixa = array(
                'idSituacaoPedido ' => $situacao
            );
            $modelPedido->updPedido($dadosBaixa,$where);
            
            $modelSolicitacao = new solicitacaocomprasModel();

            // inicio do enviar a mensagem automatica
            
            // colocar aqui onde devo gravar o numero do pedido no ITEM DA SOLICITACAO

            $dsMensagem = 'ALTERACAO NO ITEM DA SUA SOLICITACAO DE COMPRAS';
            // situacao atual antes da alteracao
            $where = 'a.idPedido = ' . $idPedido . ' and a.idPedidoItem = ' . $idPedidoItem;
            $anterior = $modelSolicitacao->getSolicitacaoComprasItens($where);
            $dsAnterior = null;
            if ($anterior) {
                $dsAnterior = $anterior[0]['dsSituacao'];
                if ($anterior[0]['dsServico']) {
                    $dsInsumo = $anterior[0]['dsServico'];
                } else {
                    $dsInsumo = $anterior[0]['dsInsumo'];
                }
            }
            // situacao depois da alteracao
            if ($anterior) {
                $dsPosterior = null;
                if ($anterior[0]['idSituacao'] <> $situacao) {
                    $dsPosterior = 'PEDIDO';
                    $dsMensagem = 'STATUS DA SOLICITACAO DE COMPRAS DO ITEM ' . $dsInsumo . ' ALTERADO DE: ' . $dsAnterior . ' PARA: MERCADORIA RECEBIDA';
                }
            }    
            // ler usuario da solictiacao para enviar msg
            $dadossci = $modelSolicitacao->getSolicitacaoComprasItens($where);
            if ($dadossci) {
                if ($dadossci[0]['idUsuarioSolicitante'] <> $_SESSION['user']['usuario']) {            
                    $dadosim = array(
                        'idUsuarioOrigem' => 999,
                        'idUsuarioDestino' => $dadossci[0]['idUsuarioSolicitante'],
                        'dsNomeTabela' => 'prodSolicitacaoCompras',
                        'idOrigemInformacao' => 99,
                        'idTabela' => $dadossci[0]['idSolicitacaoItem'],
                        'idTipoMensagem' => 0,
                        'stSituacao' => 0,
                        'dtEnvio' => date('Y-m-d H:i:s'),
                        'dsMensagem' => $dsMensagem
                    );
                    $this->criarMensagem($dadosim);
                }
            }        
            // atualizar a solicitacao com novo status
            
            // baixa solicitacao de compras
            $situacao = $situacao + 4;
            $where = 'idPedido = ' . $idPedido . ' and idPedidoItem = ' . $idPedidoItem;
            $dataSol=array(
                'idSituacao ' => $situacao,
                'qtEntregue ' => $qtparabaixa,
                'vlEntregue ' => $vlparabaixa,
                'nrNota ' => $nrNota,
                'dtEntrega ' => date('Y-m-d')               
            );

            $modelSolicitacao->updSolicitacaoComprasItemManut($dataSol, $where);            
            // fim da baixa da solicitacaod e compras
            
            $where = 'e.idInsumo = ' . $retornoPedido[0]['idInsumo'] . ' and e.idLocalEstoque = ' . $idLocalEstoque;
            $retorno = $modelEstoque->getEstoque($where);
            if ($retorno) {
                $idEstoque = $retorno[0]['idEstoque'];
                $qtdeatual = $retorno[0]['qtEstoque'] + $qtde;
                $valoratual = $retorno[0]['vlEstoque'] + $valor_pedido;

                $dataE = array(
                    'idEstoque' => $idEstoque,
                    'qtEmSolicitacao' => 0,
                    'stEmSolicitacao' => 0,
                    'qtEstoque' => $qtdeatual,
                    'vlEstoque' => $valoratual,
                    'vlPME' => $valoratual / $qtdeatual
                );
                $modelEstoque->updEstoque($dataE);
            } else {
                $dataE = array(
                    'idInsumo' => $retornoPedido[0]['idInsumo'],
                    'idLocalEstoque' => $idLocalEstoque,
                    'qtEmSolicitacao' => 0,
                    'stEmSolicitacao' => 0,
                    'qtEstoque' => $qtde,
                    'vlEstoque' => $valor_pedido,
                    'vlPME' => $valor_pedido / $qtde
                );
                
                $idEstoque = $modelEstoque->setEstoque($dataE);
            }

            $model = new entradaestoqueModel();
            $data = array();
            $data['idMovimentoItem'] = null;
            $data['idMovimento'] = $idMovimento;
            $data['idCentroCusto'] = $idCentroCusto;
            $data['idLocalEstoque'] = $idLocalEstoque;
            $data['idInsumo'] = $retornoPedido[0]['idInsumo'];
            $data['qtMovimento'] = $qtde;
            $data['vlMovimento'] = $valor_pedido;
            $data['idTipoMovimento'] = 1;
            $data['dsObservacao'] = 'BAIXA ATRAVES DA TELA DE PEDIDO';
            $id = $model->setMovimentoItem($data);

            $modelInsumo = new insumoModel();
            $idOrigemInformacao = null;
            $mTeveCC = false;
            $mTeveOS = false;
            $mTeveGrupoDR = false;
            $modelTM = new tipomovimentoModel();
            // se for compras, atualizar dados do item
            $nome = $modelTM->getTipoMovimento("idTipoMovimento = 1");
            $dsTipoMovimento = $nome[0]['dsTipoMovimento'];
            if ($dsTipoMovimento == 'Compras') {
                $where = "idInsumo = " . $retornoPedido[0]['idInsumo'];

                $qtExistente = $modelInsumo->getInsumo($where)[0];
                if (!$qtExistente) {
                    $qtNova = $qtde;
                } else {
                    $qtNova = $qtExistente['qtEstoque'] + $qtde;
                }

                if ($ncm) { 
                    // verifica se o produto ja tem ncm
                    $where = 'a.idInsumo = ' . $retornoPedido[0]['idInsumo'];
                    $retornoinsumo = $modelInsumo->getInsumo($where);
                    if ($retornoinsumo) {
                        if (!$retornoinsumo[0]['idNCM']) {
                            // ler ncm e atualizar no insumo porque nao tem
                            $where = "cdNCM = '" . $ncm . "'";
                            $modelncm = new ncmModel();
                            $retornoncm = $modelncm->getNCM($where);
                            if ($retornoncm) {
                                $idNCM = $retornoncm[0]['idNCM'];
                            } else {
                                $idNCM = $ncm;                    
                            }
                        } else {
                            $idNCM = $retornoinsumo[0]['idNCM'];
                        }
                    } else {
                        $idNCM = $ncm;                    
                    }                    
                } else {
                    $idNCM = null;                    
                }
                
                $where = 'idInsumo = ' . $retornoPedido[0]['idInsumo'];
                $datainsumo = array(
                    'idParceiroUltimaCompra' => $retornoPedido[0]['idParceiro'],
                    'idUltimoCentroCusto' => $idCentroCusto,
                    'dtUltimaCompra' => date('Y-m-d'),
                    'nrNotaUltimaCompra' => $nrNota,
                    'vlUltimaCompra' => $valor_pedido,
                    'qtUltimaCompra' => $qtde,
                    'idNCM' => $idNCM,
                    'qtEstoque ' => $qtNova                
                );
                
                $model->updMovimentoItem($datainsumo, $where);
            }    
            // pegar a origem da informacao
            $modelOI = new origeminformacaoModel();
            $retorno = $modelOI->getOrigemInformacao("dsOrigemInformacao = '" . $dsTipoMovimento . "'");
            if ($retorno) {
                $idOrigemInformacao = $retorno[0]['idOrigemInformacao'];
            } else {
                // não tem origem da informacao, entao criar um e pegar o ID   
                $dadosorigem = array(
                    'dsOrigemInformacao' => $dsTipoMovimento
                );
                $idOrigemInformacao = $modelOI->setOrigemInformacao($dadosorigem);
            }
            // sempre que tiver centro de custo, gravar um rateio e depois dar saida por aplicacao direta
            if ($idCentroCusto) {
                $modelOI = new origeminformacaoModel();
                // gravar o rateio do valor
                $dadosaprarateio = array(
                    'idRateio' => null,
                    'dtRateio' => date('Y-m-d'),
                    'idCentroCusto' => $idCentroCusto,
                    'idOrigemInformacao' => $idOrigemInformacao,
                    'dsDocOrigem' => $nrNota,
                    'vlRateio' => $valor_pedido,
                    'stDC' => 'D'                    
                );
                $modelRateio = new rateioModel();
                $modelRateio->setRateio($dadosaprarateio);  

                // dar saida de estoque
                $mTeveCC = true;                    
            }
        }    
    }    
    
    public function recebimento() {
        $idPedido = $_POST['idPedido'];

        $modellocalestoque = new localestoqueModel();
        $lista_localestoque = array('' => 'SELECIONE');
        foreach ($modellocalestoque->getLocalEstoque() as $value) {
            if ($value['idLocalEstoque'] == $value['idLocalEstoqueSuperior']) {
               $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
            } else {
                $desanterior = $modellocalestoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
                if ($desanterior) {
                   $lista_localestoque[$value['idLocalEstoque']] = $desanterior[0]['dsLocalEstoque'] . '-' . $value['dsLocalEstoque'];
                } else {
                   $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
                }
            }
        }
        $modelCentroCusto = new centrocustoModel();
        $lista_centrocusto = array('' => 'SELECIONE');
        foreach ($modelCentroCusto->getCentroCustoCombo() as $value) {
            $lista_centrocusto[$value['idCentroCusto']] = $value['codigocusto'];
        }
        
        $where = 'a.idPedido = ' . $idPedido;
        $model = new pedidoModel();
        $cabec = $model->getPedido($where)[0];
        $resultado = $model->getPedidoItens($where);
        
        $x=0;
        foreach($resultado as $value) {
            $resultado[$x]['lista_localestoque'] = $lista_localestoque;
            $resultado[$x]['lista_centrocusto'] = $lista_centrocusto;
            $x++;
        }
        
        $_SESSION['baixa_pedido']['idPedido'] = $idPedido;
        
        $this->smarty->assign('cabec', $cabec);
        $this->smarty->assign('pedidoitens', $resultado);
        $this->smarty->display("pedido/recebimento/thumbnail.tpl");        
    }
    
    public function relatoriopedido() {
        $this->template->run();

        $model = new pedidoModel();
        $pedido_lista = $model->getPedido();
        //Passa a lista de registros
        $this->smarty->assign('pedido_lista', $pedido_lista);
        $this->smarty->assign('titulo_relatorio');
        //Chama o Smarty
        $this->smarty->assign('title', 'Relatorio de Pedidos');
        $this->smarty->display('pedido/relatorio.html');
    }

    public function getParceiro() {
        $url = explode("=", $_SERVER['REQUEST_URI']);
        $key = str_replace('+', ' ', $url[1]);
        if (!empty($key)) {
          $busca = trim($key);
          $model = new parceiroModel();
          $where = "(UPPER(dsParceiro) like UPPER('%{$key}%') OR UPPER(cdCNPJ) like UPPER('%{$key}%'))";
          $retorno = $model->getParceiro($where);
          $return = array();
          if (count($retorno)) {
            $row = array();
            for ($i = 0; $i < count($retorno); $i++) {
              $cnpj = $retorno[$i]["cdCNPJ"];
              $row['value'] = strtoupper($retorno[$i]["dsParceiro"]) . '-' . $cnpj;
              $row["id"] = $retorno[$i]["idParceiro"];
              array_push($return, $row);
            }
          }
          echo json_encode($return);
        }
    }    
    
    public function getProduto() {
        $url = explode("=", $_SERVER['REQUEST_URI']);
        $key = str_replace('+', ' ', $url[1]);
        $tipo = substr($url[0],19,6); 
        if ($tipo=='insumo') {
            if (!empty($key)) {
              $busca = trim($key);
              $model = new insumoModel();
              $where = "(UPPER(dsInsumo) like UPPER('%{$key}%') OR UPPER(cdInsumo) like UPPER('%{$key}%'))";
              $retorno = $model->getInsumo($where);
              $return = array();
              if (count($retorno)) {
                $row = array();
                for ($i = 0; $i < count($retorno); $i++) {
                  $cdInsump = $retorno[$i]["cdInsumo"];
                  $row['value'] = strtoupper($retorno[$i]["dsInsumo"]) . '-' . $cdInsump;
                  $row["id"] = $retorno[$i]["idInsumo"];
                  array_push($return, $row);
                }
              }
              echo json_encode($return);
            }
        } else {
            if (!empty($key)) {
              $busca = trim($key);
              $model = new servicoModel();
              $where = "(UPPER(a.dsServico) like UPPER('%{$key}%'))";
              $retorno = $model->getServico($where);
              $return = array();
              if (count($retorno)) {
                $row = array();
                for ($i = 0; $i < count($retorno); $i++) {                  
                  $row['value'] = strtoupper($retorno[$i]["dsServico"]);
                  $row["id"] = $retorno[$i]["idServico"];
                  array_push($return, $row);
                }
              }
              echo json_encode($return);
            }
        } 
        
    }    
    
    public function enviarEmail()   {
        
        $idPedido = ($_POST['idPedido'] != '') ? $_POST['idPedido'] : null;
        $model = new pedidoModel();
        $cabec = $model->getPedido('a.idPedido = ' . $idPedido)[0];        
        $attachment = array();
        $attachment[0]['path_anexo'] =  '/var/www/html/thopos.com.br/site/'.  $cabec['dsCaminhoPDF']; // "http://local.thopos.com.br/"
        $attachment[0]['nome_anexo'] = 'PEDIDO DE COMPRA';
        /* ENVIA E-MAIL */
        $body = 'SEGUE EM ANEXO PEDIDO DE COMPRA';
        require_once 'system/libs/phpmailer/Mail.php';        
        $phpmail = new Mail();
        $send_email = $phpmail->Send(array(
            '0' => array(
                'email' => strtolower($cabec['dsEmail']), 'nome' => strtoupper($cabec['dsParceiro']))), strtoupper('PEDIDO DE COMPRA'), $body, array(), $attachment);
    }
    
    public function criarMensagem($array) {
        
        $modelMensagem = new mensagemModel();
        $dados = array(
          'idMensagem' => null,  
          'idOrigemInformacao' => $array['idOrigemInformacao'],
          'dsNomeTabela' => $array['dsNomeTabela'],
          'idTabela' => $array['idTabela']
        );
        $id = $modelMensagem->setMensagem($dados);
        $dados = array(
          'idMensagemAnterior' => $id,              
        );        
        $modelMensagem->updMensagem($dados, 'idMensagem = ' . $id);
        $dados = array(
            'idMensagemItem' => null,  
            'idMensagem' => $id,  
            'idUsuarioOrigem' => $array['idUsuarioOrigem'],
            'idUsuarioDestino' => $array['idUsuarioDestino'],
            'idTipoMensagem' => $array['idTipoMensagem'],
            'stSituacao' => $array['stSituacao'],
            'dtEnvio' => $array['dtEnvio'],
            'dsMensagem' => $array['dsMensagem']
        );
        $id = $modelMensagem->setMensagemItem($dados);
        return;
    }
    
 }
?>