<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class listamovimentoestoquesetor extends controller {

    public function index_action() {
        //Inicializa o Template
        $this->template->run();

        $modelLocalEstoque = new localestoqueModel();
        $lista_localestoque = array('' => 'SELECIONE');
        foreach ($modelLocalEstoque->getLocalEstoque() as $value) {
            $desanterior = $modelLocalEstoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
            if ($desanterior) {
                if ($value['idLocalEstoque'] == $value['idLocalEstoqueSuperior']) {
                    $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
                } else {
                    $lista_localestoque[$value['idLocalEstoque']] = $desanterior[0]['dsLocalEstoque'] . '-' . $value['dsLocalEstoque'];
                }
            } else {
                $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
            }
        }
        $modelCentroCusto = new centrocustoModel();
        $lista_centrocusto = array('' => 'SELECIONE');
        foreach ($modelCentroCusto->getCentroCustoCombo() as $value) {
                $lista_centrocusto[$value['idCentroCusto']] = $value['codigocusto'];
        }
        $modelSetor = new setorModel();
        $lista_setor = array('' => 'SELECIONE');
        foreach ($modelSetor->getSetor() as $value) {
            $lista_setor[$value['idSetor']] = $value['dsSetor'];
        }
        $colaborador = new colaboradorModel();
        $lista_colaborador = array('' => 'SELECIONE');
        foreach ($colaborador->getColaborador() as $value) {
            $lista_colaborador[$value['idColaborador']] = $value['dsColaborador'];
        }

        $this->smarty->assign('lista_colaborador', $lista_colaborador);        
        $this->smarty->assign('localestoque', $lista_localestoque);
        $this->smarty->assign('centrocusto', $lista_centrocusto);
        $this->smarty->assign('lista_setor', $lista_setor);
        
        $this->smarty->assign('movimento', null);
        $this->smarty->assign('title', 'Movimentacao');
        $this->smarty->display('movimento/listamovimento_setor.html');
        
    }
    
    public function busca_movimento() {
        //se nao existir o indice estou como padrao '';
        $idSetor = isset($_POST['idSetor']) ? $_POST['idSetor'] : '';
        $dsGrupo = isset($_POST['dsGrupo']) ? $_POST['dsGrupo'] : '';
        $cdProduto = isset($_POST['cdProduto']) ? $_POST['cdProduto'] : '';
        $dsProduto = isset($_POST['dsProduto']) ? $_POST['dsProduto'] : '';
        $dsReferencia = isset($_POST['dsReferencia']) ? $_POST['dsReferencia'] : '';
        $dtInicio = isset($_POST['dtInicio']) ? $_POST['dtInicio'] : '';
        $dtFim = isset($_POST['dtFim']) ? $_POST['dtFim'] : '';
        $idLocalEstoque = isset($_POST['idLocalEstoque']) ? $_POST['idLocalEstoque'] : '';
        $idCentroCusto = isset($_POST['idCentroCusto']) ? $_POST['idCentroCusto'] : '';
        $dsOrgaoControlador = isset($_POST['dsOrgaoControlador']) ? $_POST['dsOrgaoControlador'] : '';
        $idColaborador = isset($_POST['idColaborador']) ? $_POST['idColaborador'] : '';

        $model = new movimentoModel();
        
        $busca = array();
        $sql = 'm.idTipoMovimento = 6'; // and not isnull(mi.idSetor)
        if ($idSetor) {
            $sql = $sql . " and mi.idSetor = " . $idSetor;
            $busca['idSetor'] = $idSetor;
        }
        if ($dsGrupo) {
            $sql = $sql . " and upper(g.dsGrupo) like upper('%" . strtoupper($dsGrupo) . "%')";
            $busca['dsGrupo'] = $dsGrupo;
        }
        if ($cdProduto) {
            $sql = $sql . " and upper(i.cdInsumo) like upper('%" . strtoupper($cdProduto) . "%')";
            $busca['cdProduto'] = $cdProduto;
        }
        if ($dsProduto) {
            $sql = $sql . " and upper(i.dsInsumo) like upper('%" . strtoupper($dsProduto) . "%')";
            $busca['dsProduto'] = $dsProduto;
        }
        if ($dsReferencia) {
            $sql = $sql . " and upper(mi.dsObservacao) like upper('%" . strtoupper($dsReferencia) . "%')";
            $busca['dsReferencia'] = $dsReferencia;
        }
        if ($idColaborador) {
            $sql = $sql . " and m.idColaborador = " . $idColaborador;
            $busca['idColaborador'] = $idColaborador;
        }        
        if ($idLocalEstoque) {
            $varios = $this->CriarArrayLocalEstoque($idLocalEstoque);                        
            $sql = $sql . " and (mi.idLocalEstoque " . $varios . " or mi.idLocalEstoqueDestino " . $varios . ")";
            $busca['idLocalEstoque'] = $idLocalEstoque;
        }
        if ($idCentroCusto) {
            $varios = $this->CriarArrayCentroCusto($idCentroCusto);                        
            $sql = $sql . " and mi.idCentroCusto " . $varios;
            $busca['idCentroCusto'] = $idCentroCusto;
        }

        $dtInicio = ($dtInicio != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $dtInicio))) : date('Y-m-d h:m:s');
        $dtFim = ($dtFim != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $dtFim))) : date('Y-m-d h:m:s');

        if ($dsOrgaoControlador) {
            $sql = $sql . " and upper(i.dsOrgaoControlador) like upper('%" . $dsOrgaoControlador . "%')";
            $busca['dsOrgaoControlador'] = $dsOrgaoControlador;
        }
        $controlados = null;
        if (isset($_POST['controlados'])) { 
            $controlados = true;
            $sql = $sql . " and i.stControlado = 1";
            $busca['controlados'] = 1;
        }
        
        $sql = $sql . " and m.dtMovimento >= '" . $dtInicio . " 00:00:00' and m.dtMovimento <= '" . $dtFim . " 23:59:59'";        
        $busca['dtInicio'] = $dtInicio;
        $busca['dtFim'] = $dtFim;
        
        $resultado = $model->getMovimentoSetor($sql, 'mi.idSetor, i.dsInsumo, m.dtMovimento');
      //  var_dump($resultado); die;
        $_SESSION['buscar_movimento_setor']['sql'] = $sql;
        $modelLocalEstoque = new localestoqueModel();
        $x = 0;
        foreach ($resultado as $value) {
            $resultado[$x]['itens'] = null;
            if ($value['idSetor'] || $value['idSetor'] == 0) {
                $where = $sql . " and mi.idSetor = " . $value['idSetor'] ;
                $itens = $model->getMovimentoItens($where, 'mi.idSetor, i.dsInsumo, m.dtMovimento');
                $y = 0;
                foreach($itens as $value1) {
                    // local estoque origem
                    $desanterior = $modelLocalEstoque->getLocalEstoque('idLocalEstoque = ' . $value1['localestoquesuperior']);
                    if ($desanterior) {
                        if ($value1['idLocalEstoque'] == $value1['localestoquesuperior']) {
                            $itens[$y]['localEstoque'] = $value1['dsLocalEstoque'];
                        } else {
                            $itens[$y]['localEstoque'] = $desanterior[0]['dsLocalEstoque'] . '-' . $value1['dsLocalEstoque'];
                        }
                    } else {
                        $itens[$y]['localEstoque'] = $value1['dsLocalEstoque'];
                    }
//                    // local estoque destino
//                    if ($value1['localestoquesuperiordestino']) {
//                        $desanterior = $modelLocalEstoque->getLocalEstoque('idLocalEstoque = ' . $value1['localestoquesuperiordestino']);
//                        if ($desanterior) {
//                            if ($value1['idLocalEstoque'] == $value1['localestoquesuperiordestino']) {
//                                $itens[$y]['localEstoqueDestino'] = $value1['dsLocalEstoque'];
//                            } else {
//                                $itens[$y]['localEstoqueDestino'] = $desanterior[0]['dsLocalEstoque'] . '-' . $value1['dsLocalEstoqueDestino'];
//                            }
//                        } else {
//                            $itens[$y]['localEstoqueDestino'] = $value1['dsLocalEstoque'];
//                        }
//                    } else {
//                        $itens[$y]['localEstoqueDestino'] = '';
//                    }    
                    $y++;
                }
                $resultado[$x]['itens'] = $itens;
            }
            $x++;
        }
        
        $modelLocalEstoque = new localestoqueModel();
        $lista_localestoque = array('' => 'SELECIONE');
        foreach ($modelLocalEstoque->getLocalEstoque() as $value) {
            $desanterior = $modelLocalEstoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
            if ($desanterior) {
                if ($value['idLocalEstoque'] == $value['idLocalEstoqueSuperior']) {
                    $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
                } else {
                    $lista_localestoque[$value['idLocalEstoque']] = $desanterior[0]['dsLocalEstoque'] . '-' . $value['dsLocalEstoque'];
                }
            } else {
                $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
            }
        }
        $modelCentroCusto = new centrocustoModel();
        $lista_centrocusto = array('' => 'SELECIONE');
        foreach ($modelCentroCusto->getCentroCustoCombo() as $value) {
                $lista_centrocusto[$value['idCentroCusto']] =  $value['codigocusto'];
        }

        $modelSetor = new setorModel();
        $lista_setor = array('' => 'SELECIONE');
        foreach ($modelSetor->getSetor() as $value) {
            $lista_setor[$value['idSetor']] = $value['dsSetor'];
        }
        
        $colaborador = new colaboradorModel();
        $lista_colaborador = array('' => 'SELECIONE');
        foreach ($colaborador->getColaborador() as $value) {
            $lista_colaborador[$value['idColaborador']] = $value['dsColaborador'];
        }

        $this->smarty->assign('lista_colaborador', $lista_colaborador);                
        $this->smarty->assign('localestoque', $lista_localestoque);
        $this->smarty->assign('centrocusto', $lista_centrocusto);
        $this->smarty->assign('lista_setor', $lista_setor);

        if (sizeof($resultado) > 0) {
            $this->smarty->assign('movimento', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'Estoque');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('movimento/listamovimento_setor.html');
        } else {
            $this->smarty->assign('movimento', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'Estoque');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('movimento/listamovimento_setor.html');
        }
    }

    public function busca_criarcsv() {
        $sql = $_SESSION['buscar_movimento_setor']['sql'];
        
        $model = new movimentoModel();
        $resultado = $model->getMovimentoItens($sql);

        if ($resultado) {
            $this->criarCSV($resultado);
        }
    }
    
    public function criarCSV($resultado) {
        
        $limit = 30000;
        $offset = 0;

        global $PATH;
        $caminho = "/var/www/html/thopos.com.br/site/storage/tmp/csv/";

        // Storage
        if (!is_dir($caminho)) {
          mkdir($caminho, 0777, true);
        }

        $filename = "movimentosetor_" . date("YmsHis") . ".csv";

        $headers[] = implode(";", array(
          "\"SETOR\"",
          "\"TIPO MOVIMENTO\"",
          "\"DATA MOVIMENTO\"",
          "\"CENTRO CUSTO\"",
          "\"LOCAL ESTOQUE\"",
          "\"GRUPO PRODUTO\"",
          "\"NOME DO PRODUTO\"",
          "\"QUANTIDADE\"",
          "\"VAL UNITARIO\"",
          "\"VALOR TOTAL\"",
          "\"REFERENCIA\"",
          "\"RETIRADO POR\""
        ));

        if (file_exists("{$caminho}" . '/' . "{$filename}")) {
          unlink("{$caminho}" . '/' . "{$filename}");
        }

        // Arquivo
        $handle = fopen("{$caminho}" . '/' . "{$filename}", 'w+');
        fwrite($handle, implode(";" . PHP_EOL, $headers));
        fwrite($handle, ";" . PHP_EOL);
        fflush($handle);

        // Fecha o arquivo da $_SESSION para liberar o servidor para servir outras requisições
        session_write_close();

        $output = array();
        foreach ($resultado as $value) {
            if ($value['qtMovimento'] > 0) {
                $vlUnitario = $value['vlMovimento'] / $value['qtMovimento'];
            } else {
                $vlUnitario = 0;
            } 
            $output[] = implode(";", array(
              "\"{$value["dsSetor"]}\"",
              "\"{$value["dsTipoMovimento"]}\"",
              "\"{$value["dtMovimento"]}\"",
              "\"{$value["dsCentroCusto"]}\"",
              "\"{$value["dsLocalEstoque"]}\"",
              "\"{$value["dsGrupo"]}\"",
              "\"{$value["cdInsumo"]} {$value["dsInsumo"]}\"",
              "\"{$value["qtMovimento"]}\"",
              "\"{$vlUnitario}\"",
              "\"{$value["vlMovimento"]}\"",
              "\"{$value["referencia"]}\"",
              "\"{$value["dsColaborador"]}\""
            ));
        }
        fwrite($handle, implode(";" . PHP_EOL, $output));
        fwrite($handle, ";" . PHP_EOL);
        fflush($handle);
        fclose($handle);
        $this->download($caminho . '/' . $filename, 'CSV', $filename);        
    }

    private function download($nome, $tipo, $filename) {
      if (!empty($nome)) {
        if (file_exists($nome)) {
          header('Content-Transfer-Encoding: binary'); // For Gecko browsers mainly
          header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($nome)) . ' GMT');
          header('Accept-Ranges: bytes'); // For download resume
          header('Content-Length: ' . filesize($nome)); // File size
          header('Content-Encoding: none');
          header("Content-Type: application/{$tipo}"); // Change this mime type if the file is not PDF
          header('Content-Disposition: attachment; filename=' . $filename);
          // Make the browser display the Save As dialog
          readfile($nome);
          unlink($nome);
        }
      }
    }    
    
    private function CriarArrayLocalEstoque($idLocalEstoque) {
        $model = new localestoqueModel();
        
        $locaisestoque[0] = array();
        $retorno = $model->getLocalEstoque("idLocalEstoqueSuperior = " . $idLocalEstoque);
        if ($retorno) {
            $locaisestoque[0] = $retorno[0]['idLocalEstoque'];
            if ($retorno) {
                $sql = 'in (';
                foreach ($retorno as $value) {
                    $sql = $sql . $value['idLocalEstoque'] . ',';                
                }
            }

            $x=0;
            foreach ($retorno as $value) {
                if ($x>0) {
                    $retorno[$x] = $model->getLocalEstoque("idLocalEstoqueSuperior = " . $value['idLocalEstoque']);            
                    foreach($retorno[$x] as $value1) {
                        $sql = $sql . $value1['idLocalEstoque'] . ',';                
                    }
                }
                $x++;
            }
            $sql = substr($sql, 0, strlen($sql)-1) . ")";
        } else {
            $sql = " = null";
        }    
        return $sql;
    }
    private function CriarArrayCentroCusto($idCentroCusto) {
        $model = new centrocustoModel();
        
        $centrocusto[0] = array();
        $retorno = $model->getCentroCusto("idCentroCustoSuperior = " . $idCentroCusto);
        if ($retorno) {
            $centrocusto[0] = $retorno[0]['idCentroCusto'];
            $sql = 'in (';
            foreach ($retorno as $value) {
                $sql = $sql . $value['idCentroCusto'] . ',';                
            }
        }
//        echo 'cc : ' . $sql . '</br>';
        
        $x=0;
        foreach ($retorno as $value) {
            if ($x>0) {
                $retorno[$x] = $model->getCentroCusto("idCentroCustoSuperior = " . $value['idCentroCusto']);            
                $y=0;
                foreach($retorno[$x] as $value1) {
                    $sql = $sql . $value1['idCentroCusto'] . ',';                
                    $retorno[$x][$y] = $model->getCentroCusto("idCentroCustoSuperior = " . $value1['idCentroCusto']);    
                    foreach ($retorno[$x][$y] as $value2) {
                        $sql = $sql . $value2['idCentroCusto'] . ',';                
                    }
                    $y++;
                }
            }
            $x++;
        }
        
        if (!isset($sql)) {
            $sql = 'in(' . $idCentroCusto . ')';
        } else {
            $sql = substr($sql, 0, strlen($sql)-1) . ")";
        }
  //      echo 'cc : ' . $sql; die;
        return $sql;
    }
    public function delmovimento() {
        $sy = new system\System();
        $idMovimento = $sy->getParam('idMovimento');
        $model = new movimentoModel();
        $where = 'mi.idMovimento = ' . $idMovimento;
        $retorno = $model->getMovimentoItens($where);
        $array = array(
          'idMovimento' => $idMovimento  
        );
        if (!$retorno) {
           $model->delMovimento($array); 
        }
        header('Location: /listamovimentoestoque/busca_movimento');                
    }
}

?>