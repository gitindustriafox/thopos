<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class listamovimentoestoquetotal extends controller {

    public function index_action() {
        //Inicializa o Template
        $this->template->run();

        $modelTipoMovimento = new tipomovimentoModel();
        $lista_tipomovimento = array('' => 'SELECIONE');
        foreach ($modelTipoMovimento->getTipoMovimento() as $value) {
                $lista_tipomovimento[$value['idTipoMovimento']] = $value['dsTipoMovimento'];
        }
        
        $modelLocalEstoque = new localestoqueModel();
        $lista_localestoque = array('' => 'SELECIONE');
        foreach ($modelLocalEstoque->getLocalEstoque() as $value) {
            $desanterior = $modelLocalEstoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
            if ($desanterior) {
                if ($value['idLocalEstoque'] == $value['idLocalEstoqueSuperior']) {
                    $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
                } else {
                    $lista_localestoque[$value['idLocalEstoque']] = $desanterior[0]['dsLocalEstoque'] . '-' . $value['dsLocalEstoque'];
                }
            } else {
                $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
            }
        }
        $modelCentroCusto = new centrocustoModel();
        $lista_centrocusto = array('' => 'SELECIONE');
        foreach ($modelCentroCusto->getCentroCustoCombo() as $value) {
                $lista_centrocusto[$value['idCentroCusto']] = $value['codigocusto'];
        }

//        $sql =  "m.dtMovimento >= '" . date('Y-m-d') . "' and m.dtMovimento <= '" . date('Y-m-d') . "'";                
//        $model = new movimentoModel();
//        $registro = $model->getMovimento($sql);
//        
//        $x = 0;
//        foreach ($registro as $value) {
//            $where = 'mi.idMovimento = ' . $value['idMovimento'];
//            $itens = $model->getMovimentoItens($where);
//            $registro[$x]['itens'] = $itens;
//            $x++;
//        }
        
    //    print_a($registro); die;
        
        $this->smarty->assign('tipomovimento', $lista_tipomovimento);
        $this->smarty->assign('localestoque', $lista_localestoque);
        $this->smarty->assign('centrocusto', $lista_centrocusto);
        $this->smarty->assign('movimentoTotal', null);
        $this->smarty->assign('title', 'Movimentacao');
        $this->smarty->display('movimento/listamovimentototal.html');
        
    }
    
    public function busca_movimento() {
        //se nao existir o indice estou como padrao '';
        $idTipoMovimento = isset($_POST['idTipoMovimento']) ? $_POST['idTipoMovimento'] : '';
        $dsParceiro = isset($_POST['dsParceiro']) ? $_POST['dsParceiro'] : '';
        $idPedido = isset($_POST['idPedido']) ? $_POST['idPedido'] : '';
        $nrNota = isset($_POST['nrNota']) ? $_POST['nrNota'] : '';
        $dsGrupo = isset($_POST['dsGrupo']) ? $_POST['dsGrupo'] : '';
        $cdProduto = isset($_POST['cdProduto']) ? $_POST['cdProduto'] : '';
        $dsProduto = isset($_POST['dsProduto']) ? $_POST['dsProduto'] : '';
        $dsReferencia = isset($_POST['dsReferencia']) ? $_POST['dsReferencia'] : '';
        $dtInicio = isset($_POST['dtInicio']) ? $_POST['dtInicio'] : '';
        $dtFim = isset($_POST['dtFim']) ? $_POST['dtFim'] : '';
        $idLocalEstoque = isset($_POST['idLocalEstoque']) ? $_POST['idLocalEstoque'] : '';
        $idCentroCusto = isset($_POST['idCentroCusto']) ? $_POST['idCentroCusto'] : '';

        $model = new movimentoModel();
        
        $busca = array();
        $sql = 'm.idMovimento > 0';
        $sqlMov = 'mov.idMovimento > 0';
        if ($idTipoMovimento) {
            $sql = $sql . " and tm.idTipoMovimento = " . $idTipoMovimento;
            $sqlMov = $sqlMov . " and tmov.idTipoMovimento = " . $idTipoMovimento;
            $busca['idTipoMovimento'] = $idTipoMovimento;
        }
        if ($idPedido) {
            $sql = $sql . " and m.nrPedido = " . $idPedido;
            $sqlMov = $sqlMov . " and mov.nrPedido = " . $idPedido;
            $busca['nrPedido'] = $idPedido;
        }
        if ($nrNota) {
            $sql = $sql . " and m.nrNota = " . $nrNota;
            $sqlMov = $sqlMov . " and mov.nrNota = " . $nrNota;
            $busca['nrNota'] = $nrNota;
        }
        if ($dsParceiro) {
            $sql = $sql . " and ((upper(f.dsParceiro) like upper('%" . $dsParceiro . "%')))";
            $sqlMov = $sqlMov . " and ((upper(fmov.dsParceiro) like upper('%" . $dsParceiro . "%')))";
            $busca['dsParceiro'] = $dsParceiro;
        }
        if ($dsGrupo) {
            $sql = $sql . " and upper(g.dsGrupo) like upper('%" . $dsGrupo . "%')";
            $sqlMov = $sqlMov . " and upper(gmov.dsGrupo) like upper('%" . $dsGrupo . "%')";
            $busca['dsGrupo'] = $dsGrupo;
        }
        if ($cdProduto) {
            $sql = $sql . " and upper(i.cdInsumo) like upper('%" . $cdProduto . "%')";
            $sqlMov = $sqlMov . " and upper(imov.cdInsumo) like upper('%" . $cdProduto . "%')";
            $busca['cdProduto'] = $cdProduto;
        }
        if ($dsProduto) {
            $sql = $sql . " and upper(i.dsInsumo) like upper('%" . $dsProduto . "%')";
            $sqlMov = $sqlMov . " and upper(imov.dsInsumo) like upper('%" . $dsProduto . "%')";
            $busca['dsProduto'] = $dsProduto;
        }
        if ($dsReferencia) {
            $sql = $sql . " and upper(mi.dsObservacao) like upper('%" . $dsReferencia . "%')";
            $busca['dsReferencia'] = $dsReferencia;
        }
        if ($idLocalEstoque) {
            $varios = $this->CriarArrayLocalEstoque($idLocalEstoque);                        
            $sql = $sql . " and (mi.idLocalEstoque " . $varios . " or mi.idLocalEstoqueDestino " . $varios . ")";
            $busca['idLocalEstoque'] = $idLocalEstoque;
        }
        if ($idCentroCusto) {
            $varios = $this->CriarArrayCentroCusto($idCentroCusto);                        
            $sql = $sql . " and mi.idCentroCusto " . $varios;
            $busca['idCentroCusto'] = $idCentroCusto;
        }

        $dtInicio = ($dtInicio != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $dtInicio))) : date('Y-m-d h:m:s');
        $dtFim = ($dtFim != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $dtFim))) : date('Y-m-d h:m:s');
        
        $sql = $sql . " and m.dtMovimento >= '" . $dtInicio . " 00:00:00' and m.dtMovimento <= '" . $dtFim . " 23:59:59'";        
        $sqlMov = $sqlMov . " and mov.dtMovimento >= '" . $dtInicio . " 00:00:00' and mov.dtMovimento <= '" . $dtFim . " 23:59:59'";        
        $busca['dtInicio'] = $dtInicio;
        $busca['dtFim'] = $dtFim;
        
        $resultado = $model->getMovimentoTotal($sql, $sqlMov);
        $_SESSION['buscar_estoque_total']['sql'] = $sql;
        $_SESSION['buscar_estoque_total']['sqlmov'] = $sqlMov;
        $modelTipoMovimento = new tipomovimentoModel();
        $lista_tipomovimento = array('' => 'SELECIONE');
        foreach ($modelTipoMovimento->getTipoMovimento() as $value) {
                $lista_tipomovimento[$value['idTipoMovimento']] = $value['dsTipoMovimento'];
        }
        $modelLocalEstoque = new localestoqueModel();
        $lista_localestoque = array('' => 'SELECIONE');
        foreach ($modelLocalEstoque->getLocalEstoque() as $value) {
            $desanterior = $modelLocalEstoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
            if ($desanterior) {
                if ($value['idLocalEstoque'] == $value['idLocalEstoqueSuperior']) {
                    $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
                } else {
                    $lista_localestoque[$value['idLocalEstoque']] = $desanterior[0]['dsLocalEstoque'] . '-' . $value['dsLocalEstoque'];
                }
            } else {
                $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
            }
        }
        $modelCentroCusto = new centrocustoModel();
        $lista_centrocusto = array('' => 'SELECIONE');
        foreach ($modelCentroCusto->getCentroCustoCombo() as $value) {
                $lista_centrocusto[$value['idCentroCusto']] =  $value['codigocusto'];
        }
        
        $this->smarty->assign('tipomovimento', $lista_tipomovimento);
        $this->smarty->assign('localestoque', $lista_localestoque);
        $this->smarty->assign('centrocusto', $lista_centrocusto);

        if (sizeof($resultado) > 0) {
            $this->smarty->assign('movimentoTotal', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'Estoque');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('movimento/listamovimentototal.html');
        } else {
            $this->smarty->assign('movimentoTotal', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'Estoque');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('movimento/listamovimentototal.html');
        }
    }

    public function busca_criarcsv() {
        $sql = $_SESSION['buscar_estoque_total']['sql'];
        $sqlMov = $_SESSION['buscar_estoque_total']['sqlmov'];
        
        $model = new movimentoModel();        
        $resultado = $model->getMovimentoTotal($sql, $sqlMov);
        if ($resultado) {
            $this->criarCSV($resultado);
        }
     //   header('Location: /solicitacaocomprasmanutencao');
    }
    
    public function criarCSV($resultado) {
        
        $limit = 30000;
        $offset = 0;

        global $PATH;
        $caminho = "/var/www/html/thopos.com.br/site/storage/tmp/csv/";

        // Storage
        if (!is_dir($caminho)) {
          mkdir($caminho, 0777, true);
        }

        $filename = "movimentototalestoque_" . date("YmsHis") . ".csv";

        $headers[] = implode(";", array(
          "\"ID PRODUTO\"",
          "\"CODIGO\"",
          "\"NOME DO PRODUTO\"",
          "\"QTDE TOTAL ENTRADAS\"",
          "\"VALOR TOTAL ENTRADAS\"",
          "\"QTDE TOTAL SAIDAS\"",
          "\"VALOR TOTAL SAIDAS\""
        ));

        if (file_exists("{$caminho}" . '/' . "{$filename}")) {
          unlink("{$caminho}" . '/' . "{$filename}");
        }

        // Arquivo
        $handle = fopen("{$caminho}" . '/' . "{$filename}", 'w+');
        fwrite($handle, implode(";" . PHP_EOL, $headers));
        fwrite($handle, ";" . PHP_EOL);
        fflush($handle);

        // Fecha o arquivo da $_SESSION para liberar o servidor para servir outras requisições
        session_write_close();

        $output = array();
        foreach ($resultado as $value) {
            $output[] = implode(";", array(
              "\"{$value["idInsumo"]}\"",
              "\"{$value["cdInsumo"]}\"",
              "\"{$value["dsInsumo"]}\"",
              "\"{$value["qtTotalEntradas"]}\"",
              "\"{$value["qtTotalSaidas"]}\"",
              "\"{$value["vlTotalEntradas"]}\"",
              "\"{$value["vlTotalSaidas"]}\"",
            ));
        }
        fwrite($handle, implode(";" . PHP_EOL, $output));
        fwrite($handle, ";" . PHP_EOL);
        fflush($handle);
        fclose($handle);
        $this->download($caminho . '/' . $filename, 'CSV', $filename);        
    }

    private function download($nome, $tipo, $filename) {
      if (!empty($nome)) {
        if (file_exists($nome)) {
          header('Content-Transfer-Encoding: binary'); // For Gecko browsers mainly
          header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($nome)) . ' GMT');
          header('Accept-Ranges: bytes'); // For download resume
          header('Content-Length: ' . filesize($nome)); // File size
          header('Content-Encoding: none');
          header("Content-Type: application/{$tipo}"); // Change this mime type if the file is not PDF
          header('Content-Disposition: attachment; filename=' . $filename);
          // Make the browser display the Save As dialog
          readfile($nome);
          unlink($nome);
        }
      }
    }        
    
    private function CriarArrayLocalEstoque($idLocalEstoque) {
        $model = new localestoqueModel();
        
        $locaisestoque[0] = array();
        $retorno = $model->getLocalEstoque("idLocalEstoqueSuperior = " . $idLocalEstoque);
        if ($retorno) {
            $locaisestoque[0] = $retorno[0]['idLocalEstoque'];
            if ($retorno) {
                $sql = 'in (';
                foreach ($retorno as $value) {
                    $sql = $sql . $value['idLocalEstoque'] . ',';                
                }
            }

            $x=0;
            foreach ($retorno as $value) {
                if ($x>0) {
                    $retorno[$x] = $model->getLocalEstoque("idLocalEstoqueSuperior = " . $value['idLocalEstoque']);            
                    foreach($retorno[$x] as $value1) {
                        $sql = $sql . $value1['idLocalEstoque'] . ',';                
                    }
                }
                $x++;
            }
            $sql = substr($sql, 0, strlen($sql)-1) . ")";
        } else {
            $sql = " = null";
        }    
        return $sql;
    }
    private function CriarArrayCentroCusto($idCentroCusto) {
        $model = new centrocustoModel();
        
        $centrocusto[0] = array();
        $retorno = $model->getCentroCusto("idCentroCustoSuperior = " . $idCentroCusto);
        if ($retorno) {
            $centrocusto[0] = $retorno[0]['idCentroCusto'];
            $sql = 'in (';
            foreach ($retorno as $value) {
                $sql = $sql . $value['idCentroCusto'] . ',';                
            }
        }
//        echo 'cc : ' . $sql . '</br>';
        
        $x=0;
        foreach ($retorno as $value) {
            if ($x>0) {
                $retorno[$x] = $model->getCentroCusto("idCentroCustoSuperior = " . $value['idCentroCusto']);            
                $y=0;
                foreach($retorno[$x] as $value1) {
                    $sql = $sql . $value1['idCentroCusto'] . ',';                
                    $retorno[$x][$y] = $model->getCentroCusto("idCentroCustoSuperior = " . $value1['idCentroCusto']);    
                    foreach ($retorno[$x][$y] as $value2) {
                        $sql = $sql . $value2['idCentroCusto'] . ',';                
                    }
                    $y++;
                }
            }
            $x++;
        }
        
        if (!isset($sql)) {
            $sql = 'in(' . $idCentroCusto . ')';
        } else {
            $sql = substr($sql, 0, strlen($sql)-1) . ")";
        }
  //      echo 'cc : ' . $sql; die;
        return $sql;
    }

}

?>