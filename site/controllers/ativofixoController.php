<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class ativofixo extends controller {

    public function index_action() {
//die("chegou");
        //Inicializa o Template
        $this->template->run();

        $model = new tipoativoModel();
        $lista = array('' => 'SELECIONE');
        foreach ($model->getTipoAtivo() as $value) {
            $lista[$value['idTipoAtivo']] = $value['dsTipoAtivo'];
        }
        $this->smarty->assign('lista_tipoativo', $lista);
        
        $model = new classeativoModel();
        $lista = array('' => 'SELECIONE');
        foreach ($model->getClasseAtivo() as $value) {
            $lista[$value['idClasseAtivo']] = $value['dsClasseAtivo'];
        }
        $this->smarty->assign('lista_classeativo', $lista);
        
        $lista = array('' => 'SELECIONE');
        foreach ($model->getItem() as $value) {
            $lista[$value['idGrupoAtivo']] = $value['dsGrupoAtivo'];
        }
        $this->smarty->assign('lista_grupoativo', $lista);
        
        $model = new marcaModel();
        $lista = array('' => 'SELECIONE');
        foreach ($model->getMarca() as $value) {
            $lista[$value['idMarca']] = $value['dsMarca'];
        }
        $this->smarty->assign('lista_marca', $lista);

        $model = new modeloModel();
        $lista = array('' => 'SELECIONE');
        foreach ($model->getModelo() as $value) {
            $lista[$value['idModelo']] = $value['dsModelo'];
        }
        $this->smarty->assign('lista_modelo', $lista);

        $model = new colaboradorModel();
        $lista = array('' => 'SELECIONE');
        foreach ($model->getColaborador() as $value) {
            $lista[$value['idColaborador']] = $value['dsColaborador'];
        }
        $this->smarty->assign('lista_colaborador', $lista);

        
        $this->smarty->assign('ativofixo_lista', null);
        $this->smarty->display('ativofixo/lista.html');
    }

    public function busca_ativofixo() {        
        
        //se nao existir o indice estou como padrao '';
        $idTipoAtivo = isset($_POST['idTipoAtivo']) ? $_POST['idTipoAtivo'] : '';
        $idClasseAtivo = isset($_POST['idClasseAtivo']) ? $_POST['idClasseAtivo'] : '';
        $idGrupoAtivo = isset($_POST['idGrupoAtivo']) ? $_POST['idGrupoAtivo'] : '';
        $idCodigoAtivo = isset($_POST['idCodigoAtivo']) ? $_POST['idCodigoAtivo'] : '';
        $dsAtivoFixo = isset($_POST['dsAtivoFixo']) ? $_POST['dsAtivoFixo'] : '';
        $idMarca = isset($_POST['idMarca']) ? $_POST['idMarca'] : '';
        $idModelo = isset($_POST['idModelo']) ? $_POST['idModelo'] : '';
        $nrSerie = isset($_POST['nrSerie']) ? $_POST['nrSerie'] : '';
        $dsCodigoDoFabricante = isset($_POST['dsCodigoDoFabricante']) ? $_POST['dsCodigoDoFabricante'] : '';
        $nrNotaAquisicao = isset($_POST['nrNotaAquisicao']) ? $_POST['nrNotaAquisicao'] : '';
        $dtInicio = isset($_POST['dtInicio']) ? $_POST['dtInicio'] : '';
        $dtFim = isset($_POST['dtFim']) ? $_POST['dtFim'] : '';
        $idParceiro = isset($_POST['idParceiro']) ? $_POST['idParceiro'] : '';
        $idColaborador = isset($_POST['idColaborador']) ? $_POST['idColaborador'] : '';
        
        $busca = array();
        $sql = 'idAtivoFixo > 0';
        if ($idTipoAtivo) {
            $sql = $sql . " and idTipoAtivo = " . $idTipoAtivo;
            $busca['idTipoAtivo'] = $idTipoAtivo;
        }
        if ($idClasseAtivo) {
            $sql = $sql . " and idClasseAtivo = " . $idClasseAtivo;
            $busca['idClasseAtivo'] = $idClasseAtivo;
        }
        if ($idGrupoAtivo) {
            $sql = $sql . " and idGrupoAtivo = " . $idGrupoAtivo;
            $busca['idGrupoAtivo'] = $idGrupoAtivo;
        }
        if ($idCodigoAtivo) {
            $sql = $sql . " and idCodigoAtivo = " . $idCodigoAtivo;
            $busca['idCodigoAtivo'] = $idCodigoAtivo;
        }
        if ($idMarca) {
            $sql = $sql . " and idMarca = " . $idMarca;
            $busca['idMarca'] = $idMarca;
        }
        if ($idModelo) {
            $sql = $sql . " and idModelo = " . $idModelo;
            $busca['idModelo'] = $idModelo;
        }
        if ($idParceiro) {
            $sql = $sql . " and idParceiroAquisicao = " . $idParceiro;
            $busca['idParceiro'] = $idParceiro;
        }
        if ($idColaborador) {
            $sql = $sql . " and idColaborador = " . $idColaborador;
            $busca['idColaborador'] = $idColaborador;
        }
        if ($dsAtivoFixo) {
            $sql = $sql . " and upper(dsAtivoFixo) like upper('%" . strtoupper($dsAtivoFixo) . "%')";
            $busca['dsAtivoFixo'] = $dsAtivoFixo;
        }
        if ($nrSerie) {
            $sql = $sql . " and upper(nrSerie) like upper('%" . $nrSerie . "%')";
            $busca['nrSerie'] = $nrSerie;
        }
        if ($dsCodigoDoFabricante) {
            $sql = $sql . " and upper(dsCodigoDoFabricante) like upper('%" . $dsCodigoDoFabricante . "%')";
            $busca['dsCodigoDoFabricante'] = $dsCodigoDoFabricante;
        }
        if ($nrNotaAquisicao) {
            $sql = $sql . " and upper(nrNotaAquisicao) like upper('%" . $nrNotaAquisicao . "%')";
            $busca['nrNotaAquisicao'] = $nrNotaAquisicao;
        }
        
        $dtInicio = ($dtInicio != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $dtInicio))) : '';
        $dtFim = ($dtFim != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $dtFim))) : '';
        if ($dtInicio && $dtFim) {
            $sql = $sql . " and dtAquisicao >= '" . $dtInicio . " 00:00:00' and dtAquisicao <= '" . $dtFim . " 23:59:59'";        
        }
        $busca['dtInicio'] = $dtInicio;
        $busca['dtFim'] = $dtFim;
        
        $cpagina = null;
        if (isset($_POST['cpagina'])) { 
            $cpagina = true;
            $busca['cpagina'] = 1;
        }

        $model = new ativofixoModel();
        $resultado = $model->getAtivoFixo($sql, $cpagina);
//        var_dump($resultado); die;
        $_SESSION['buscar_ativofixo']['sql'] = $sql;
        
        
        $model = new tipoativoModel();
        $lista = array('' => 'SELECIONE');
        foreach ($model->getTipoAtivo() as $value) {
            $lista[$value['idTipoAtivo']] = $value['dsTipoAtivo'];
        }
        $this->smarty->assign('lista_tipoativo', $lista);
        
        $model = new classeativoModel();
        $lista = array('' => 'SELECIONE');
        foreach ($model->getClasseAtivo() as $value) {
            $lista[$value['idClasseAtivo']] = $value['dsClasseAtivo'];
        }
        $this->smarty->assign('lista_classeativo', $lista);
        
        $lista = array('' => 'SELECIONE');
        foreach ($model->getItem() as $value) {
            $lista[$value['idGrupoAtivo']] = $value['dsGrupoAtivo'];
        }
        $this->smarty->assign('lista_grupoativo', $lista);
        
        $model = new marcaModel();
        $lista = array('' => 'SELECIONE');
        foreach ($model->getMarca() as $value) {
            $lista[$value['idMarca']] = $value['dsMarca'];
        }
        $this->smarty->assign('lista_marca', $lista);

        $model = new modeloModel();
        $lista = array('' => 'SELECIONE');
        foreach ($model->getModelo() as $value) {
            $lista[$value['idModelo']] = $value['dsModelo'];
        }
        $this->smarty->assign('lista_modelo', $lista);

        $model = new colaboradorModel();
        $lista = array('' => 'SELECIONE');
        foreach ($model->getColaborador() as $value) {
            $lista[$value['idColaborador']] = $value['dsColaborador'];
        }
        $this->smarty->assign('lista_colaborador', $lista);
        
        
        if (sizeof($resultado) > 0) {
            $this->smarty->assign('ativofixo_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'ativofixo');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('ativofixo/lista.html');
        } else {
            $this->smarty->assign('ativofixo_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'ativofixo');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('ativofixo/lista.html');
        }
    }

    public function busca_criarcsv() {
        $sql = $_SESSION['buscar_ativofixo']['sql'];
        $model = new insumoModel();
        $resultado = $model->getInsumo($sql);

        if ($resultado) {
            $this->criarCSV($resultado);
        }
     //   header('Location: /solicitacaocomprasmanutencao');
    }
    
    public function criarCSV($resultado) {
        
        $limit = 30000;
        $offset = 0;

        global $PATH;
        $caminho = "/var/www/html/thopos.com.br/site/storage/tmp/csv/";

        // Storage
        if (!is_dir($caminho)) {
          mkdir($caminho, 0777, true);
        }

        $filename = "produtos_" . date("YmsHis") . ".csv";

        $headers[] = implode(";", array(
          "\"ID\"",
          "\"CODIGO\"",
          "\"NOME DO PRODUTO\"",
          "\"GRUPO\"",
          "\"MARCA\"",
          "\"MODELO\"",
          "\"NUMERO DE SERIE\"",
          "\"VAL UNITARIO\"",
          "\"DATA CADASTRO\"",
          "\"VALIDADE\"",
          "\"NCM\"",
          "\"FORN ULT COMPRA\"",
          "\"QT ULT COMPRA\"",
          "\"DT ULT COMPRA\"",
          "\"QT ULT SAIDA\"",
          "\"DT ULT SAIDA\""
        ));

        if (file_exists("{$caminho}" . '/' . "{$filename}")) {
          unlink("{$caminho}" . '/' . "{$filename}");
        }

        // Arquivo
        $handle = fopen("{$caminho}" . '/' . "{$filename}", 'w+');
        fwrite($handle, implode(";" . PHP_EOL, $headers));
        fwrite($handle, ";" . PHP_EOL);
        fflush($handle);

        // Fecha o arquivo da $_SESSION para liberar o servidor para servir outras requisições
        session_write_close();

        $output = array();
        foreach ($resultado as $value) {
            $output[] = implode(";", array(
              "\"{$value["idInsumo"]}\"",
              "\"{$value["cdInsumo"]}\"",
              "\"{$value["dsInsumo"]}\"",
              "\"{$value["dsGrupo"]}\"",
              "\"{$value["dsMarca"]}\"",
              "\"{$value["dsModelo"]}\"",
              "\"{$value["nrSerie"]}\"",
              "\"{$value["vlUnitario"]}\"",
              "\"{$value["dtCadastro"]}\"",
              "\"{$value["dtValidade"]}\"",
              "\"{$value["cdNCM"]}\"",
              "\"{$value["dsParceiro"]}\"",
              "\"{$value["qtUltimaCompra"]}\"",
              "\"{$value["dtUltimaCompra"]}\"",
              "\"{$value["qtUltimaVenda"]}\"",
              "\"{$value["dtUltimaVenda"]}\"",
            ));
        }
        fwrite($handle, implode(";" . PHP_EOL, $output));
        fwrite($handle, ";" . PHP_EOL);
        fflush($handle);
        fclose($handle);
        $this->download($caminho . '/' . $filename, 'CSV', $filename);        
    }

    private function download($nome, $tipo, $filename) {
      if (!empty($nome)) {
        if (file_exists($nome)) {
          header('Content-Transfer-Encoding: binary'); // For Gecko browsers mainly
          header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($nome)) . ' GMT');
          header('Accept-Ranges: bytes'); // For download resume
          header('Content-Length: ' . filesize($nome)); // File size
          header('Content-Encoding: none');
          header("Content-Type: application/{$tipo}"); // Change this mime type if the file is not PDF
          header('Content-Disposition: attachment; filename=' . $filename);
          // Make the browser display the Save As dialog
          readfile($nome);
          unlink($nome);
        }
      }
    }    
    
    //Funcao de Inserir
    public function novo_ativofixo() {
        $sy = new system\System();
        $idAtivoFixo = $sy->getParam('idAtivoFixo');
        $ajustar = $sy->getParam('pkRsuviop');

        $model = new ativofixoModel();

        if ($idAtivoFixo > 0) {
            $registro = $model->getAtivoFixo('idAtivoFixo=' . $idAtivoFixo);
            $registro = $registro[0]; 
        } else {
            //Novo Registro
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }

        $model = new tipoativoModel();
        $lista = array('' => 'SELECIONE');
        foreach ($model->getTipoAtivo() as $value) {
            $lista[$value['idTipoAtivo']] = $value['dsTipoAtivo'];
        }
        $this->smarty->assign('lista_tipoativo', $lista);
        
        $lista = null;
        if ($idAtivoFixo > 0) {
            $lista = $this->carregaClasse();
        }
        $this->smarty->assign('lista_classeativo', $lista);

        $lista = null;
        if ($idAtivoFixo > 0) {
            $lista = $this->carregaGrupo();
        }
        $this->smarty->assign('lista_grupoativo', $lista);
        
        $model = new marcaModel();
        $lista = array('' => 'SELECIONE');
        foreach ($model->getMarca() as $value) {
            $lista[$value['idMarca']] = $value['dsMarca'];
        }
        $this->smarty->assign('lista_marca', $lista);

        $model = new modeloModel();
        $lista = array('' => 'SELECIONE');
        foreach ($model->getModelo() as $value) {
            $lista[$value['idModelo']] = $value['dsModelo'];
        }
        $this->smarty->assign('lista_modelo', $lista);

        $model = new colaboradorModel();
        $lista = array('' => 'SELECIONE');
        foreach ($model->getColaborador() as $value) {
            $lista[$value['idColaborador']] = $value['dsColaborador'];
        }
        $this->smarty->assign('lista_colaborador', $lista);

        $model = new unidadeModel();
        $lista = array('' => 'SELECIONE');
        foreach ($model->getUnidade() as $value) {
            $lista[$value['idUnidade']] = $value['dsUnidade'];
        }
        $this->smarty->assign('lista_unidade', $lista);

        $model = new motivobaixaModel();
        $lista = array('' => 'SELECIONE');
        foreach ($model->getMotivoBaixa() as $value) {
            $lista[$value['idMotivoBaixa']] = $value['dsMotivoBaixa'];
        }
        $this->smarty->assign('lista_motivobaixa', $lista);
        
        $this->smarty->assign('registro', $registro);
        //$this->smarty->assign('lista_NCM', $lista_NCM);
        $this->smarty->assign('title', 'Novo AtivoFixo');
        $this->smarty->display('ativofixo/form_novo.tpl');
    }

    public function carregaClasse() {
        $model = new classeativoModel();
        $lista = array('' => 'SELECIONE');
        foreach ($model->getClasseAtivo() as $value) {
            $lista[$value['idClasseAtivo']] = $value['dsClasseAtivo'];
        }
        return $lista;
    }
    
    public function carregaGrupo() {
        $model = new classeativoModel();
        $lista = array('' => 'SELECIONE');
        foreach ($model->getItem() as $value) {
            $lista[$value['idGrupoAtivo']] = $value['dsGrupoAtivo'];
        }
        return $lista;
    }
    
    public function lerClasseAtivo() {
        $idTipoAtivo = $_POST['idTipoAtivo'];
        $model = new classeativoModel();
        $lista = array('' => 'SELECIONE');
        foreach ($model->getClasseAtivo('idTipoAtivo = ' . $idTipoAtivo) as $value) {
            $lista[$value['idClasseAtivo']] = $value['dsClasseAtivo'];
        }
        $this->smarty->assign('lista_classeativo', $lista);
        $html = $this->smarty->fetch('ativofixo/lista_classeativo.tpl');
        $jsondata["html"] = $html;        
        echo json_encode($jsondata);
    }
    
    public function lerGrupoAtivo() {
        $idClasseAtivo = $_POST['idClasseAtivo'];
        $model = new classeativoModel();
        $lista = array('' => 'SELECIONE');
        foreach ($model->getItem('idClasseAtivo = ' . $idClasseAtivo) as $value) {
            $lista[$value['idGrupoAtivo']] = $value['dsGrupoAtivo'];
        }
        $this->smarty->assign('lista_grupoativo', $lista);
        $html = $this->smarty->fetch('ativofixo/lista_grupoativo.tpl');
        $jsondata["html"] = $html;        
        echo json_encode($jsondata);
    }
    
    public function lerCodigoAtivo() {
        $idClasseAtivo = $_POST['idClasseAtivo'];
        $idTipoAtivo = $_POST['idTipoAtivo'];
        $idGrupoAtivo = $_POST['idGrupoAtivo'];
        $where = "idTipoAtivo = {$idTipoAtivo} and idClasseAtivo = {$idClasseAtivo} and idGrupoAtivo = {$idGrupoAtivo}";
        $model = new ativofixoModel();
        $dados =  $model->getAtivoFixo($where);
        if ($dados) {
            $jsondata = array(
                "idCodigoAtivo" => $dados[0]['idCodigoAtivo'],
                "idSequenciaAtivo" => $dados[0]['idSequenciaAtivo']
                 );  
        } else {
            $jsondata = array(
                "idCodigoAtivo" => 1,
                "idSequenciaAtivo" => 0
                );
        }
        echo json_encode($jsondata);
    }
    
    public function gravar_ativofixo() {
        $model = new ativofixoModel();

        $data = $this->trataPost($_POST);

        if ($data['idAtivoFixo'] == NULL) {
            $data['dtCadastro'] = date('Y-m-d h:m:s');        
            $data['idUsuarioCadastro'] = $_SESSION['user']['usuario'];        
            $id = $model->setativofixo($data);             
        }
        else {
            $id = $data['idAtivoFixo'];
            $model->updativofixo($data);             
        }//update
        
        header('Location: /ativofixo/novo_ativofixo/idAtivoFixo/' . $id);        
        return;
    }

    //Trata dados antes de Enviar para o Gravar
    private function trataPost($post) {
        $data['idAtivoFixo'] = ($post['idAtivoFixo'] != '') ? $post['idAtivoFixo'] : null;
        $data['idTipoAtivo'] = ($post['idTipoAtivo'] != '') ? $post['idTipoAtivo'] : null;
        $data['idClasseAtivo'] = ($post['idClasseAtivo'] != '') ? $post['idClasseAtivo'] : null;
        $data['idGrupoAtivo'] = ($post['idGrupoAtivo'] != '') ? $post['idGrupoAtivo'] : null;
        $data['idCodigoAtivo'] = ($post['idCodigoAtivo'] != '') ? $post['idCodigoAtivo'] : null;
        $data['idSequenciaAtivo'] = ($post['idSequenciaAtivo'] != '') ? $post['idSequenciaAtivo'] : null;
        $data['dsAtivoFixo'] = ($post['dsAtivoFixo'] != '') ? $post['dsAtivoFixo'] : null;
        $data['idUnidade'] = ($post['idUnidade'] != '') ? $post['idUnidade'] : null;
        $data['idMarca'] = ($post['idMarca'] != '') ? $post['idMarca'] : null;
        $data['idModelo'] = ($post['idModelo'] != '') ? $post['idModelo'] : null;
        $data['nrSerie'] = ($post['nrSerie'] != '') ? $post['nrSerie'] : null;
        $data['dsCodigoDoFabricante'] = ($post['dsCodigoDoFabricante'] != '') ? $post['dsCodigoDoFabricante'] : null;
        $data['dsDetalhadaAtivoFixo'] = ($post['dsDetalhadaAtivoFixo'] != '') ? $post['dsDetalhadaAtivoFixo'] : null;
        $data['idNCM'] = ($post['idNCM'] != '') ? $post['idNCM'] : null;
        $data['idColaborador'] = ($post['idColaborador'] != '') ? $post['idColaborador'] : null;
        return $data;
    }
    public function importarfotos() {
        $idAtivoFixo = $_POST['idAtivoFixo'];
        $dsTabela = $_POST['dsTabela'];
        
        $model = new documentoModel();        
        $lista = $model->getDocumento("dsTabela = '" . $dsTabela . "' and idTabela  = " . $idAtivoFixo);
        $this->smarty->assign('idAtivoFixo', $idAtivoFixo);
        $this->smarty->assign('dsTabela', $dsTabela);
        $this->smarty->assign('fotos_lista', $lista);
        $this->smarty->display("ativofixo/modalFotos/thumbnail.tpl");        
    }
    public function adicionarfoto() {
        $idAtivoFixo = $_POST['idAtivoFixo'];
        $dsTabela = $_POST['dsTabela'];
        
        $extensao = explode('.',$_FILES['arquivo']['name'])[1];
        $novonome = $dsTabela . '_' . $idAtivoFixo . '_' . date('Ymdis') . '.' . $extensao;
        
        $_UP['pasta'] = 'storage/tmp/documentos/';
        // Tamanho máximo do arquivo (em Bytes)
        $_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
        // Array com as extensões permitidas
        $_UP['extensoes'] = array('jpg','jpeg','png','bmp', 'pdf');
        // Renomeia o arquivo? (Se true, o arquivo será salvo como .jpg e um nome único)
        $_UP['renomeia'] = false;
        // Array com os tipos de erros de upload do PHP
        $_UP['erros'][0] = 'Não houve erro';
        $_UP['erros'][1] = 'O arquivo no upload é maior do que o limite do PHP';
        $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
        $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
        $_UP['erros'][4] = 'Não foi feito o upload do arquivo';
        // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
        if ($_FILES['arquivo']['error'] != 0) {
          die("Não foi possível fazer o upload, erro:" . $_UP['erros'][$_FILES['arquivo']['error']]);
          exit; // Para a execução do script
        }
        // Caso script chegue a esse ponto, não houve erro com o upload e o PHP pode continuar
        // Faz a verificação da extensão do arquivo
//        $extensao = strtolower(end(explode('.', $_FILES['arquivo']['name'])));
//        if (array_search($extensao, $_UP['extensoes']) === false) {
//          echo "Por favor, envie arquivos com as seguinte extensão: csv";
//          exit;
//        }
        // Faz a verificação do tamanho do arquivo
        if ($_UP['tamanho'] < $_FILES['arquivo']['size']) {
          echo "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
          exit;
        }
        // O arquivo passou em todas as verificações, hora de tentar movê-lo para a pasta
        // Primeiro verifica se deve trocar o nome do arquivo
//        if ($_UP['renomeia'] == true) {
//          // Cria um nome baseado no UNIX TIMESTAMP atual e com extensão .jpg
//          $nome_final = md5(time()).'.csv';
//        } else {
          // Mantém o nome original do arquivo
//          $nome_final = $_FILES['arquivo']['name'];
//        }

        // Depois verifica se é possível mover o arquivo para a pasta escolhida
        if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $_UP['pasta'] . $novonome)) {
//          Upload efetuado com sucesso, exibe uma mensagem e um link para o arquivo
//          echo "Upload efetuado com sucesso!";
//          echo '<a href="' . $_UP['pasta'] . $nome_final . '">Clique aqui para acessar o arquivo</a>';
//          
//            exit;
//          $this->importar($_UP['pasta'] . $nome_final, $idPeriodo);
//          header('Location: /planodecontas/novo_planodecontas/idPeriodo/' . $idPeriodo);

            $model = new documentoModel();    
            $dados = array(
                'idDocumento' => null,
                'dsTabela' => $dsTabela,
                'idTabela' => $idAtivoFixo,
                'dsLocalArquivo' => $_UP['pasta'] . $novonome                
            );
            $model->setDocumento($dados);
            
        } else {
          // Não foi possível fazer o upload, provavelmente a pasta está incorreta
          echo "Não foi possível enviar o arquivo, tente novamente";
        }    
        header('Location: /listaAtivoFixos');        
    }

    // Remove Padrao
    public function delativofixo() {
        $sy = new system\System();
                
        $idAtivoFixo = $sy->getParam('idAtivoFixo');
        
        $ativofixo = $idAtivoFixo;
        
        if (!is_null($ativofixo)) {    
            $model = new ativofixoModel();
            $dados['idAtivoFixo'] = $ativofixo;             
            $model->delAtivoFixo($dados);
        }

        header('Location: /ativofixo');
    }

    public function relatorioativofixo_pre() {
        $this->template->run();

        $this->smarty->assign('title', 'Pre Relatorio de AtivoFixos');
        $this->smarty->display('ativofixo/relatorio_pre.html');
    }

    public function relatorioativofixo() {
        $this->template->run();

        $model = new ativofixoModel();
        $ativofixo_lista = $model->getAtivoFixo();
        //Passa a lista de registros
        $this->smarty->assign('ativofixo_lista', $ativofixo_lista);
        $this->smarty->assign('titulo_relatorio');
        //Chama o Smarty
        $this->smarty->assign('title', 'Relatorio de AtivoFixos');
        $this->smarty->display('ativofixo/relatorio.html');
    }
    
    public function gravaracertoestoque() {
        $entrada_saida = $_POST['entrada_saida'];
        $dsMotivo = $_POST['dsMotivo'];
        $qtAcerto = $_POST['qtAcerto'];
        $vlAcerto = $_POST['vlAcerto'];
        $idTipoMovimento = $_POST['idTipoMovimento'];
        $idLocalEstoque = $_POST['idLocalEstoque'];
        $datacabec = $this->preparaMovimento($_POST);
        $model = new entradaestoqueModel();
        $idMovimento = $model->setMovimento($datacabec);                
        $data = $this->preparaItem($_POST, $idMovimento);
        $this->gravar_itemmesmo($data, $entrada_saida);
        $retorno=array();
        echo json_encode($retorno);
    }
    private function preparaMovimento($post) {
        $data = array();
        $data['idMovimento'] = null;
        $data['idParceiro'] =  null;
        $data['idTipoMovimento'] = ($post['idTipoMovimento'] != '') ? $post['idTipoMovimento'] : null;
        $data['dsObservacao'] = ($post['dsMotivo'] != '') ? $post['dsMotivo'] : null;
        $data['dtMovimento'] = date('Y-m-d');
        $data['nrNota'] = null;
        $data['nrPedido'] = null;
        $data['qtTotalNota'] =  null;
        $data['idColaborador'] =  null;
        $data['nrPlaca'] = null;
        $data['nrLacre'] =  null;
        $data['nrNota2'] =  null;
        $data['qtTotalNota2'] =  null;
        $data['idUsuario'] = $_SESSION['user']['usuario'];
        return $data;
    }
    
    private function preparaItem($post, $idMovimento) {
        $data = array();
        $data['idMovimentoItem'] = null;
        $data['idAtivoFixo'] = ($post['idAtivoFixo'] != '') ? $post['idAtivoFixo'] : null;
        $data['qtMovimento'] = ($post['qtAcerto'] != '') ? str_replace(",",".",str_replace(".","",$post['qtAcerto'])) : null;
        $data['vlMovimento'] = ($post['vlAcerto'] != '') ? str_replace(",",".",str_replace(".","",$post['vlAcerto'])) : null;
        $data['idLocalEstoque'] = ($post['idLocalEstoque'] != '') ? $post['idLocalEstoque'] : null;
        $data['idMovimento'] = $idMovimento;
        $data['idTipoMovimento'] = ($post['idTipoMovimento'] != '') ? $post['idTipoMovimento'] : null;
        $data['dsObservacao'] = ($post['dsMotivo'] != '') ? $post['dsMotivo'] : null;
        return $data;
    }
    
    private function gravar_itemmesmo($data, $entrada_saida) {
        
        $modelEstoque = new estoqueModel();
        $where = 'e.idAtivoFixo = ' . $data['idAtivoFixo'] . ' and e.idLocalEstoque = ' . $data['idLocalEstoque'];
        $retorno = $modelEstoque->getEstoque($where);
        if ($retorno) {
            $idEstoque = $retorno[0]['idEstoque'];
            if ($entrada_saida == 'E') {
                $qtdeatual = $retorno[0]['qtEstoque'] + $data['qtMovimento'];
                $valoratual = $retorno[0]['vlEstoque'] + $data['vlMovimento'];
            } else {
                $qtdeatual = $retorno[0]['qtEstoque'] - $data['qtMovimento'];
                $valoratual = $retorno[0]['vlEstoque'] - $data['vlMovimento'];
            }
            
            $dataE = array(
                'idEstoque' => $idEstoque,
                'qtEmSolicitacao' => 0,
                'stEmSolicitacao' => 0,
                'qtEstoque' => $qtdeatual,
                'vlEstoque' => $valoratual,
                'vlPME' => $valoratual / $qtdeatual
            );
            $modelEstoque->updEstoque($dataE);
        } else {
            $dataE = array(
                'idAtivoFixo' => $data['idAtivoFixo'],
                'idLocalEstoque' => $data['idLocalEstoque'],
                'qtEmSolicitacao' => 0,
                'stEmSolicitacao' => 0,
                'qtEstoque' => $data['qtMovimento'],
                'vlEstoque' => $data['vlMovimento'],
                'vlPME' => $data['vlMovimento'] / $data['qtMovimento']
            );
            $idEstoque = $modelEstoque->setEstoque($dataE);
        }
        
        $model = new entradaestoqueModel();
        $data['idCentroCusto'] = null;
        $data['idMaquina'] = null;
        $data['idOS'] = null;
        $data['idMotivo'] = null;
        $id = $model->setMovimentoItem($data);
        
        $modelAtivoFixo = new ativofixoModel();  
        $dataativofixo = array(
            'idUltimoMovimento' => $data['idMovimento'],
            'idAtivoFixo' => $data['idAtivoFixo']
        );
        $modelAtivoFixo->updAtivoFixo($dataativofixo);
    }

    public function getParceiro() {
        $url = explode("=", $_SERVER['REQUEST_URI']);
        $key = str_replace('+', ' ', $url[1]);
        if (!empty($key)) {
          $busca = trim($key);
          $model = new parceiroModel();
          $where = "(UPPER(dsParceiro) like UPPER('%{$key}%') OR UPPER(cdCNPJ) like UPPER('%{$key}%'))";
          $retorno = $model->getParceiro($where);
          $return = array();
          if (count($retorno)) {
            $row = array();
            for ($i = 0; $i < count($retorno); $i++) {
              $cnpj = $retorno[$i]["cdCNPJ"];
              $row['value'] = strtoupper($retorno[$i]["dsParceiro"]) . '-' . $cnpj;
              $row["id"] = $retorno[$i]["idParceiro"];
              array_push($return, $row);
            }
          }
          echo json_encode($return);
        }
    }    
    
    public function getAtivoFixo() {
        $url = explode("=", $_SERVER['REQUEST_URI']);
        $key = str_replace('+', ' ', $url[1]);
        if (!empty($key)) {
          $busca = trim($key);
          $model = new ativofixoModel();
          $where = "(UPPER(a.dsAtivoFixo) like UPPER('%{$busca}%') OR UPPER(a.cdAtivoFixo) like UPPER('%{$busca}%'))";
          $retorno = $model->getAtivoFixo($where);
          $return = array();
          if (count($retorno)) {
            $row = array();
            for ($i = 0; $i < count($retorno); $i++) {
              $cdInsump = $retorno[$i]["cdInsumo"];
              $row['value'] = strtoupper($retorno[$i]["dsInsumo"]) . '-' . $cdInsump;
              $row["id"] = $retorno[$i]["idInsumo"];
              array_push($return, $row);
            }
          }
          echo json_encode($return);
        }
    }

}

?>