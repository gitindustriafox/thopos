<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class centrocusto extends controller {

    public function index_action() {
//die("chegou");
        //Inicializa o Template
        $this->template->run();

        $model = new centrocustoModel();
        $centrocusto_lista = $model->getCentroCusto(null,true, 10);

        $this->smarty->assign('centrocusto_lista', $centrocusto_lista);
        $this->smarty->display('centrocusto/lista.html');
    }

//Funcao de Busca
    public function busca_centrocusto() {
        //se nao existir o indice estou como padrao '';
        $busca['dsCentroCusto'] = isset($_POST['dsCentroCusto']) ? $_POST['dsCentroCusto'] : '';
        //$texto = '';
        $model = new centrocustoModel();
        $sql = "upper(dsCentroCusto) like upper('%" . $busca['dsCentroCusto'] . "%')"; //somente os nao excluidos
        $resultado = $model->getCentroCusto($sql, $paginacao=true, 10);
        $_SESSION['buscar_ccusto']['sql'] = $sql;
        if (sizeof($resultado) > 0) {
            $this->smarty->assign('centrocusto_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'centrocusto');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('centrocusto/lista.html');
        } else {
            $this->smarty->assign('centrocusto_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'centrocusto');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('centrocusto/lista.html');
        }
    }

    public function busca_criarcsv() {
        $sql = $_SESSION['buscar_ccusto']['sql'];
        $model = new centrocustoModel();
        $resultado = $model->getCentroCusto($sql);

        if ($resultado) {
            $this->criarCSV($resultado);
        }
     //   header('Location: /solicitacaocomprasmanutencao');
    }
    
    public function criarCSV($resultado) {
        
        $limit = 30000;
        $offset = 0;

        global $PATH;
        $caminho = "/var/www/html/thopos.com.br/site/storage/tmp/csv/";

        // Storage
        if (!is_dir($caminho)) {
          mkdir($caminho, 0777, true);
        }

        $filename = "centrocusto_" . date("YmsHis") . ".csv";

        $headers[] = implode(";", array(
          "\"ID\"",
          "\"CODIGO\"",
          "\"NOME DO CENTRO DE CUSTO\"",
          "\"TIPO\"",
          "\"ID DO SUPERIOR\""
        ));

        if (file_exists("{$caminho}" . '/' . "{$filename}")) {
          unlink("{$caminho}" . '/' . "{$filename}");
        }

        // Arquivo
        $handle = fopen("{$caminho}" . '/' . "{$filename}", 'w+');
        fwrite($handle, implode(";" . PHP_EOL, $headers));
        fwrite($handle, ";" . PHP_EOL);
        fflush($handle);

        // Fecha o arquivo da $_SESSION para liberar o servidor para servir outras requisições
        session_write_close();

        $output = array();
        foreach ($resultado as $value) {
            $output[] = implode(";", array(
              "\"{$value["idCentroCusto"]}\"",
              "\"{$value["cdCentroCusto"]}\"",
              "\"{$value["dsCentroCusto"]}\"",
              "\"{$value["stAnalitico"]}\"",
              "\"{$value["idCentroCustoSuperior"]}\""
            ));
        }
        fwrite($handle, implode(";" . PHP_EOL, $output));
        fwrite($handle, ";" . PHP_EOL);
        fflush($handle);
        fclose($handle);
        $this->download($caminho . '/' . $filename, 'CSV', $filename);        
    }

    private function download($nome, $tipo, $filename) {
      if (!empty($nome)) {
        if (file_exists($nome)) {
          header('Content-Transfer-Encoding: binary'); // For Gecko browsers mainly
          header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($nome)) . ' GMT');
          header('Accept-Ranges: bytes'); // For download resume
          header('Content-Length: ' . filesize($nome)); // File size
          header('Content-Encoding: none');
          header("Content-Type: application/{$tipo}"); // Change this mime type if the file is not PDF
          header('Content-Disposition: attachment; filename=' . $filename);
          // Make the browser display the Save As dialog
          readfile($nome);
          unlink($nome);
        }
      }
    }    
    
    //Funcao de Inserir
    public function novo_centrocusto() {
        $sy = new system\System();

        $idCentroCusto = $sy->getParam('idCentroCusto');

        $model = new centrocustoModel();

        if ($idCentroCusto > 0) {

            $registro = $model->getCentroCusto('idCentroCusto=' . $idCentroCusto);
            $registro = $registro[0]; //Passando CentroCusto
        } else {
            //Novo Registro
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        
        //Obter lista a de tipos fk
        $objLista = new centrocustoModel();
        //criar uma lista
        $lista_tipos = $objLista->getCentroCusto('idCentroCusto <> 0');
        foreach ($lista_tipos as $value) {
            $lista_tipos_log[$value['idCentroCusto']] = $value['dsCentroCusto'];
        }
        //Passar a lista de Tipo
        $this->smarty->assign('lista_centrocusto', $lista_tipos);
        //var_dump($lista_tipos_log);die;
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('title', 'Nova CentroCusto');
        $this->smarty->display('centrocusto/form_novo.tpl');
    }

    // Gravar Padrao
    public function gravar_centrocusto() {
        $model = new centrocustoModel();

        $data = $this->trataPost($_POST);

        if ($data['idCentroCusto'] == NULL)
            $model->setcentrocusto($data);
        else
            $model->updcentrocusto($data); //update
        
        header('Location: /centrocusto');        
        return;
    }

    //Trata dados antes de Enviar para o Gravar
    private function trataPost($post) {
        $data['idCentroCusto'] = ($post['idCentroCusto'] != '') ? $post['idCentroCusto'] : null;
        $data['dsCentroCusto'] = ($post['dsCentroCusto'] != '') ? $post['dsCentroCusto'] : null;
        $data['stAnalitico'] = ($post['stAnalitico'] != '') ? $post['stAnalitico'] : null;
        $data['idCentroCustoSuperior'] = ($post['idCentroCustoSuperior'] != '') ? $post['idCentroCustoSuperior'] : null;
        $data['cdCentroCusto'] = ($post['cdCentroCusto'] != '') ? $post['cdCentroCusto'] : null;
        return $data;
    }

    // Remove Padrao
    public function delcentrocusto() {
        $sy = new system\System();
                
        $idCentroCusto = $sy->getParam('idCentroCusto');
        
        $centrocusto = $idCentroCusto;
        
        if (!is_null($centrocusto)) {    
            $model = new centrocustoModel();
            $dados['idCentroCusto'] = $centrocusto;             
            $model->delCentroCusto($dados);
        }

        header('Location: /centrocusto');
    }

    public function relatoriocentrocusto_pre() {
        $this->template->run();

        $this->smarty->assign('title', 'Pre Relatorio de Centro de Custos');
        $this->smarty->display('centrocusto/relatorio_pre.html');
    }

    public function relatoriocentrocusto() {
        $this->template->run();

        $model = new centrocustoModel();
        $centrocusto_lista = $model->getCentroCusto();
        //Passa a lista de registros
        $this->smarty->assign('centrocusto_lista', $centrocusto_lista);
        $this->smarty->assign('titulo_relatorio');
        //Chama o Smarty
        $this->smarty->assign('title', 'Relatorio de Centro de Custos');
        $this->smarty->display('centrocusto/relatorio.html');
    }

}

?>