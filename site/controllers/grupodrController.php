<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class grupodr extends controller {

    public function index_action() {
//die("chegou");
        //Inicializa o Template
        $this->template->run();

        $model = new grupodrModel();
        $grupodr_lista = $model->getGrupoDR();

        $this->smarty->assign('grupodr_lista', $grupodr_lista);
        $this->smarty->display('grupodr/lista.html');
    }

//Funcao de Busca
    public function busca_grupodr() {
        //se nao existir o indice estou como padrao '';
        $texto = isset($_POST['buscadescricao']) ? $_POST['buscadescricao'] : '';
        //$texto = '';
        $model = new grupodrModel();
        $sql = "stStatus <> 0 and upper(dsGrupoDR) like upper('%" . $texto . "%')"; //somente os nao excluidos
        $resultado = $model->getGrupoDR($sql);

        if (sizeof($resultado) > 0) {
            $this->smarty->assign('grupodr_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'grupodr');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('grupodr/lista.html');
        } else {
            $this->smarty->assign('grupodr_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'grupodr');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('grupodr/lista.html');
        }
    }

    //Funcao de Inserir
    public function novo_grupodr() {
        $sy = new system\System();

        $idGrupoDR = $sy->getParam('idGrupoDR');

        $model = new grupodrModel();

        if ($idGrupoDR > 0) {

            $registro = $model->getGrupoDR('idGrupoDR=' . $idGrupoDR);
            $registro = $registro[0]; //Passando GrupoDR
            $lista = $model->getItemDR('i.idGrupoDR = ' . $idGrupoDR);
            if ($registro['stOrcamento'] == 1) {
                $checked = 'checked';
            } else {
                $checked = '';
            }
        } else {
            //Novo Registro
            $lista = null;
            $registro = $model->estrutura_vazia();
            $checked = 'checked';
            $registro = $registro[0];
        }
        
        $this->smarty->assign('lista_itens',$lista);
        $this->smarty->assign('checked',$checked);
        
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('title', 'Novo GrupoDR');
        $this->smarty->display('grupodr/form_novo.tpl');
    }

    // Gravar Padrao
    public function gravar_grupodr() {
        $model = new grupodrModel();

        $data = $this->trataPost($_POST);

        if ($data['idGrupoDR'] == NULL) {
          $id =  $model->setgrupodr($data);
        } else {
          $id = $data['idGrupoDR'];
          $model->updgrupodr($data); //update
        }
        header('Location: /grupodr/novo_grupodr/idGrupoDR/' . $id);        
        return;
    }

    public function adicionaritem() {
        $idGrupoDR = $_POST['idGrupoDR'];
        $dsItemDR = $_POST['dsItemDR'];
        $data = array(
            'idGrupoDR' => $idGrupoDR,
            'idItemDR' => null,
            'dsItemDR' => $dsItemDR
        );
        $modelGDR = new grupodrModel();
        $modelGDR->setItemDR($data);
        
        $lista = $modelGDR->getItemDR('i.idGrupoDR = ' . $idGrupoDR);
        $this->smarty->assign('lista_itens',$lista);
        $html = $this->smarty->fetch("grupodr/itens.html");
        $jsondata = array(
            'html' => $html
        );
        echo json_encode($jsondata);
        
    }
    
    public function delitem() {
        $model = new grupodrModel();        
        $idGrupoDR = $_POST['idGrupoDR'];
        $idItemDR = $_POST['idItemDR'];
        $ok = 'Item excluido';
        $modelOrcamento = new orcamentoModel();
        $where = 'idItemDR = ' . $idItemDR . ' and idGrupoDR = ' . $idGrupoDR;
        $retorno = $modelOrcamento->getOrcamentoItemDR($where);
        if ($retorno) {
            $ok = 'Item não pode ser excluido, existe(m) orçamento(s) com este item';
        } else {
            $model->delItemDR($where);        
        }
        
        $lista = $model->getItemDR('i.idGrupoDR = ' . $idGrupoDR);
        $this->smarty->assign('lista_itens',$lista);
        $html = $this->smarty->fetch("grupodr/itens.html");
        $jsondata = array(
            'html' => $html,
            'ok' => $ok
        );
        echo json_encode($jsondata);
        
    }
    
    //Trata dados antes de Enviar para o Gravar
    private function trataPost($post) {
        $data['idGrupoDR'] = ($post['idGrupoDR'] != '') ? $post['idGrupoDR'] : null;
        $data['dsGrupoDR'] = ($post['dsGrupoDR'] != '') ? $post['dsGrupoDR'] : null;
        $data['stGrupoDR'] = ($post['stGrupoDR'] != '') ? $post['stGrupoDR'] : 'D';
        
        if (isset($post['stOrcamento'])) {
            $data['stOrcamento'] = 1;
        } else {
            $data['stOrcamento'] = 0;
        }
        return $data;
    }

    // Remove Padrao
    public function delgrupodr() {
        $sy = new system\System();                
        $idGrupoDR = $sy->getParam('idGrupoDR');        
        $modelOrcamento = new orcamentoModel();
        $where = 'idGrupoDR = ' . $idGrupoDR;
        $retorno = $modelOrcamento->getOrcamentoItemDR($where);
        if (!$retorno) {
            if (!is_null($idGrupoDR)) {    
                $model = new grupodrModel();
                $dados['idGrupoDR'] = $idGrupoDR;             
                $model->delGrupoDR($dados);
            }
        }    
        header('Location: /grupodr');
    }

    public function relatoriogrupodr_pre() {
        $this->template->run();

        $this->smarty->assign('title', 'Pre Relatorio de GrupoDRs de Insumos/Produtos');
        $this->smarty->display('grupodr/relatorio_pre.html');
    }

    public function relatoriogrupodr() {
        $this->template->run();

        $model = new grupodrModel();
        $grupodr_lista = $model->getGrupoDR();
        //Passa a lista de registros
        $this->smarty->assign('grupodr_lista', $grupodr_lista);
        $this->smarty->assign('titulo_relatorio');
        //Chama o Smarty
        $this->smarty->assign('title', 'Relatorio de GrupoDRs de Insumos/Produtos');
        $this->smarty->display('grupodr/relatorio.html');
    }

}

?>