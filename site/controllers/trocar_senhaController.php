<?php

class trocar_senha extends controller {

    public function troca_senha() {
        $model = new usuariosModel();
        
        $idUsuario = $_SESSION['user']['usuario'];
        
//        echo 'id usuario: ' .  $idUsuario; die;
        $dados = $model->getTrocaSenha('ts.stStatus <> 0 and ts.idUsuario = '.$idUsuario);

        //var_dump($dados);die;

        $this->smarty->assign('title', 'TROCAR SENHA');
        $this->smarty->assign('dados', $dados[0]);
        $this->smarty->display('usuarios/trocar_senha.tpl');
    }

    public function grava_senha() {

        $model = new usuariosModel();

        $array = array();

        $array['idUsuario'] = $_POST['idUsuario'];
        $array['senha'] = md5($_POST['nova_senha']);

        $model->updUsuario($array);
        $log_o = new logModel;
        $log_o->logPadrao('trocou a senha', 1);

        header("Location: /trocar_senha/troca_senha");
        //jogar para home
    }

    public function valida_senha() {
        $sy = new system\System();
        
        $idUsuario = $sy->getParam('idUsuario');
        $nova_senha = $sy->getParam('nova_senha');

        $model = new usuariosModel();
        $dados = $model->getTrocaSenha("ts.stStatus <> 0 and ts.idUsuario = '{$idUsuario}'");

        $return = array();
        $return['stStatus'] = true;

        //print_a_die($dados[0]['Senha']);

        if ($dados[0]['Senha'] === md5($nova_senha)) {
            $return['stStatus'] = false;
        }

        echo json_encode($return);
        die;
    }

}

?>