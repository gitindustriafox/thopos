<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class tipoparceiro extends controller {

    public function index_action() {
//die("chegou");
        //Inicializa o Template
        $this->template->run();

        $model = new tipoparceiroModel();
        $tipoParceiro_lista = $model->getTipoParceiro();

        $this->smarty->assign('tipoparceiro_lista', $tipoParceiro_lista);
        $this->smarty->display('tipoparceiro/lista.html');
    }

//Funcao de Busca
    public function busca_tipoParceiro() {
        //se nao existir o indice estou como padrao '';
        $texto = isset($_POST['buscadescricao']) ? $_POST['buscadescricao'] : '';
        //$texto = '';
        $model = new tipoparceiroModel();
        $sql = "stStatus <> 0 and upper(dsTipoParceiro) like upper('%" . $texto . "%')"; //somente os nao excluidos
        $resultado = $model->getTipoParceiro($sql);

        if (sizeof($resultado) > 0) {
            $this->smarty->assign('tipoparceiro_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'tipoParceiro');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('tipoparceiro/lista.html');
        } else {
            $this->smarty->assign('tipoparceiro_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'tipoParceiro');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('tipoparceiro/lista.html');
        }
    }

    //Funcao de Inserir
    public function novo_tipoParceiro() {
        $sy = new system\System();

        $idTipoParceiro = $sy->getParam('idTipoParceiro');

        $model = new tipoparceiroModel();

        if ($idTipoParceiro > 0) {

            $registro = $model->getTipoParceiro('idTipoParceiro=' . $idTipoParceiro);
            $registro = $registro[0]; //Passando TipoParceiro
        } else {
            //Novo Registro
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        
        //Obter lista a de tipos fk
        $objLista = new tipoparceiroModel();
        //criar uma lista
        $lista_tipos = $objLista->getTipoParceiro('idTipoParceiro <> 0');
        foreach ($lista_tipos as $value) {
            $lista_tipos_log[$value['idTipoParceiro']] = $value['dsTipoParceiro'];
        }
        //Passar a lista de Tipo
        $this->smarty->assign('lista_tipoparceiro', $lista_tipos);
        //var_dump($lista_tipos_log);die;
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('title', 'Novo Tipo de Parceiro');
        $this->smarty->display('tipoparceiro/form_novo.tpl');
    }

    // Gravar Padrao
    public function gravar_tipoParceiro() {
        $model = new tipoparceiroModel();

        $data = $this->trataPost($_POST);

        if ($data['idTipoParceiro'] == NULL)
            $model->settipoParceiro($data);
        else
            $model->updtipoParceiro($data); //update
        
        header('Location: /tipoparceiro');        
        return;
    }

    //Trata dados antes de Enviar para o Gravar
    private function trataPost($post) {
        $data['idTipoParceiro'] = ($post['idTipoParceiro'] != '') ? $post['idTipoParceiro'] : null;
        $data['dsTipoParceiro'] = ($post['dsTipoParceiro'] != '') ? $post['dsTipoParceiro'] : null;
        return $data;
    }

    // Remove Padrao
    public function deltipoParceiro() {
        $sy = new system\System();
                
        $idTipoParceiro = $sy->getParam('idTipoParceiro');
        
        $tipoParceiro = $idTipoParceiro;
        
        if (!is_null($tipoParceiro)) {    
            $model = new tipoparceiroModel();
            $dados['idTipoParceiro'] = $tipoParceiro;             
            $model->delTipoParceiro($dados);
        }

        header('Location: /tipoparceiro');
    }

    public function relatoriotipoParceiro_pre() {
        $this->template->run();

        $this->smarty->assign('title', 'Pre Relatorio de Tipo de Parceiro');
        $this->smarty->display('tipoparceiro/relatorio_pre.html');
    }

    public function relatoriotipoParceiro() {
        $this->template->run();

        $model = new tipoparceiroModel();
        $tipoParceiro_lista = $model->getTipoParceiro();
        //Passa a lista de registros
        $this->smarty->assign('tipoparceiro_lista', $tipoParceiro_lista);
        $this->smarty->assign('titulo_relatorio');
        //Chama o Smarty
        $this->smarty->assign('title', 'Relatorio de Tipo de Parceiro');
        $this->smarty->display('tipoparceiro/relatorio.html');
    }

}

?>