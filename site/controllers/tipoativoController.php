<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class tipoativo extends controller {

    public function index_action() {
//die("chegou");
        //Inicializa o Template
        $this->template->run();

        $model = new tipoativoModel();
        $tipoativo_lista = $model->getTipoAtivo();

        $this->smarty->assign('tipoativo_lista', $tipoativo_lista);
        $this->smarty->display('tipoativo/lista.html');
    }

//Funcao de Busca
    public function busca_tipoativo() {
        //se nao existir o indice estou como padrao '';
        $texto = isset($_POST['buscadescricao']) ? $_POST['buscadescricao'] : '';
        //$texto = '';
        $model = new tipoativoModel();
        $sql = "upper(dsTipoAtivo) like upper('%" . $texto . "%')"; //somente os nao excluidos
        $resultado = $model->getTipoAtivo($sql);

        if (sizeof($resultado) > 0) {
            $this->smarty->assign('tipoativo_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'tipoativo');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('tipoativo/lista.html');
        } else {
            $this->smarty->assign('tipoativo_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'tipoativo');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('tipoativo/lista.html');
        }
    }

    //Funcao de Inserir
    public function novo_tipoativo() {
        $sy = new system\System();

        $idTipoAtivo = $sy->getParam('idTipoAtivo');

        $model = new tipoativoModel();

        if ($idTipoAtivo > 0) {

            $registro = $model->getTipoAtivo('idTipoAtivo=' . $idTipoAtivo);
            $registro = $registro[0]; //Passando TipoAtivo
        } else {
            //Novo Registro
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        
        //Obter lista a de tipos fk
        $objLista = new tipoativoModel();
        //criar uma lista
        $lista_tipos = $objLista->getTipoAtivo('idTipoAtivo <> 0');
        foreach ($lista_tipos as $value) {
            $lista_tipos_log[$value['idTipoAtivo']] = $value['dsTipoAtivo'];
        }
        //Passar a lista de Tipo
        $this->smarty->assign('lista_tipoativo', $lista_tipos);
        //var_dump($lista_tipos_log);die;
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('title', 'Novo TipoAtivo');
        $this->smarty->display('tipoativo/form_novo.tpl');
    }

    // Gravar Padrao
    public function gravar_tipoativo() {
        $model = new tipoativoModel();

        $data = $this->trataPost($_POST);

        if ($data['idTipoAtivo'] == NULL)
            $model->settipoativo($data);
        else
            $model->updtipoativo($data); //update
        
        header('Location: /tipoativo');        
        return;
    }

    //Trata dados antes de Enviar para o Gravar
    private function trataPost($post) {
        $data['idTipoAtivo'] = ($post['idTipoAtivo'] != '') ? $post['idTipoAtivo'] : null;
        $data['dsTipoAtivo'] = ($post['dsTipoAtivo'] != '') ? $post['dsTipoAtivo'] : null;
        return $data;
    }

    // Remove Padrao
    public function deltipoativo() {
        $sy = new system\System();
                
        $idTipoAtivo = $sy->getParam('idTipoAtivo');
        
        $tipoativo = $idTipoAtivo;
        
        if (!is_null($tipoativo)) {    
            $model = new tipoativoModel();
            $dados['idTipoAtivo'] = $tipoativo;             
            $model->delTipoAtivo($dados);
        }

        header('Location: /tipoativo');
    }

    public function relatoriotipoativo_pre() {
        $this->template->run();

        $this->smarty->assign('title', 'Pre Relatorio de Tipo Ativos');
        $this->smarty->display('tipoativo/relatorio_pre.html');
    }

    public function relatoriotipoativo() {
        $this->template->run();

        $model = new tipoativoModel();
        $tipoativo_lista = $model->getTipoAtivo();
        //Passa a lista de registros
        $this->smarty->assign('tipoativo_lista', $tipoativo_lista);
        $this->smarty->assign('titulo_relatorio');
        //Chama o Smarty
        $this->smarty->assign('title', 'Relatorio de Tipo Ativos');
        $this->smarty->display('tipoativo/relatorio.html');
    }

}

?>