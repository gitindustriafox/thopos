<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class listaInsumos extends controller {

    public function index_action() {
        //Inicializa o Template
        $this->template->run();

        $modelUnidade = new unidadeModel();
        $lista_unidade = array('' => 'SELECIONE');
        foreach ($modelUnidade->getUnidade() as $value) {
            $lista_unidade[$value['idUnidade']] = $value['dsUnidade'];
        }
        $this->smarty->assign('lista_unidades', $lista_unidade);

        $modelGrupo = new grupoModel();
        $listaGrupo = array('' => 'SELECIONE');
        foreach ($modelGrupo->getGrupo() as $value) {
            $listaGrupo[$value['idGrupo']] = $value['dsGrupo'];
        }
        $this->smarty->assign('lista_grupos', $listaGrupo);

        $this->smarty->assign('insumo_lista', null);
        $this->smarty->assign('title', 'Movimentacao');
        $this->smarty->display('estoque/listainsumo.html');
        
    }
    
    public function busca_insumo_cadastro() {        
        
        //se nao existir o indice estou como padrao '';
        $idGrupo = isset($_POST['idGrupo']) ? $_POST['idGrupo'] : '';
        $dsInsumo = isset($_POST['dsInsumo']) ? $_POST['dsInsumo'] : '';
        $dsParceiro = isset($_POST['dsParceiro']) ? $_POST['dsParceiro'] : '';
        $cdInsumo = isset($_POST['cdInsumo']) ? $_POST['cdInsumo'] : '';
        $cdNCM = isset($_POST['cdNCM']) ? $_POST['cdNCM'] : '';
        $idUnidade = isset($_POST['idUnidade']) ? $_POST['idUnidade'] : '';
        $dsOrgaoControlador = isset($_POST['dsOrgaoControlador']) ? $_POST['dsOrgaoControlador'] : '';
        $model = new estoqueModel();
        
        $busca = array();
        $sql = 'a.idInsumo > 0';
        if ($idGrupo) {
            $sql = $sql . " and a.idGrupo = {$idGrupo}";
            $busca['idGrupo'] = $idGrupo;
        }
        if ($dsInsumo) {
            $sql = $sql . " and upper(a.dsInsumo) like upper('%" . $dsInsumo . "%')";
            $busca['dsInsumo'] = $dsInsumo;
        }
        if ($dsParceiro) {
            $sql = $sql . " and upper(f.dsParceiro) like upper('%" . $dsParceiro . "%')";
            $busca['dsParceiro'] = $dsParceiro;
        }
        if ($cdInsumo) {
            $sql = $sql . " and upper(a.cdInsumo) like upper('%" . $cdInsumo . "%')";
            $busca['cdInsumo'] = $cdInsumo;
        }
        if ($idUnidade) {
            $sql = $sql . " and a.idUnidade = {$idUnidade}";
            $busca['idUnidade'] = $idUnidade;
        }
        if ($cdNCM) {
            $sql = $sql . " and upper(ncm.cdNCM) like upper('%" . $cdNCM . "%')";
            $busca['cdNCM'] = $cdNCM;
        }
        $cpagina = null;
        if (isset($_POST['cpagina'])) { 
            $cpagina = true;
            $busca['cpagina'] = 1;
        }
        
        $ordemcadastro = null;
        if (isset($_POST['ordemcadastro'])) { 
            $ordemcadastro = true;
            $busca['ordemcadastro'] = 1;
        }

        $semgrupo = null;
        if (isset($_POST['semgrupo'])) { 
            $semgrupo = true;
            $busca['semgrupo'] = 1;
        }

        if ($dsOrgaoControlador) {
            $sql = $sql . " and upper(a.dsOrgaoControlador) like upper('%" . $dsOrgaoControlador . "%')";
            $busca['dsOrgaoControlador'] = $dsOrgaoControlador;
        }
        $controlados = null;
        if (isset($_POST['controlados'])) { 
            $controlados = true;
            $sql = $sql . " and a.stControlado = 1";
            $busca['controlados'] = 1;
        }

        if ($semgrupo) {
            $sql = $sql . " and isnull(d.dsGrupo)";
        }
        $_SESSION['buscar_insumos']['sql'] = $sql;
        
        $modelUnidade = new unidadeModel();
        $lista_unidade = array('' => 'SELECIONE');
        foreach ($modelUnidade->getUnidade() as $value) {
            $lista_unidade[$value['idUnidade']] = $value['dsUnidade'];
        }
        $this->smarty->assign('lista_unidades', $lista_unidade);

        $modelGrupo = new grupoModel();
        $listaGrupo = array('' => 'SELECIONE');
        foreach ($modelGrupo->getGrupo() as $value) {
            $listaGrupo[$value['idGrupo']] = $value['dsGrupo'];
        }
        $this->smarty->assign('lista_grupos', $listaGrupo);
        
        $model = new insumoModel();
        $x=0;
        $resultado = $model->getInsumo($sql, $cpagina, $ordemcadastro);
        foreach ($resultado as $value) {
            $resultado[$x]['lista_grupo'] = $listaGrupo;
            $resultado[$x]['lista_unidade'] = $lista_unidade;
            $x++;
        }
        
        if (sizeof($resultado) > 0) {
            $this->smarty->assign('insumo_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'insumo');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('estoque/listainsumo.html');
        } else {
            $this->smarty->assign('insumo_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'insumo');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('estoque/listainsumo.html');
        }
    }    
    public function busca_criarcsv() {
        $sql = $_SESSION['buscar_insumos']['sql'];
        $model = new insumoModel();
        $resultado = $model->getInsumo($sql);

        if ($resultado) {
            $this->criarCSV($resultado);
        }
     //   header('Location: /solicitacaocomprasmanutencao');
    }
    
    public function criarCSV($resultado) {
        
        $limit = 30000;
        $offset = 0;

        global $PATH;
        $caminho = "/var/www/html/thopos.com.br/site/storage/tmp/csv/";

        // Storage
        if (!is_dir($caminho)) {
          mkdir($caminho, 0777, true);
        }

        $filename = "produtos_" . date("YmsHis") . ".csv";

        $headers[] = implode(";", array(
          "\"ID\"",
          "\"CODIGO\"",
          "\"NOME DO PRODUTO\"",
          "\"GRUPO\"",
          "\"MARCA\"",
          "\"MODELO\"",
          "\"NUMERO DE SERIE\"",
          "\"VAL UNITARIO\"",
          "\"DATA CADASTRO\"",
          "\"VALIDADE\"",
          "\"NCM\"",
          "\"FORN ULT COMPRA\"",
          "\"QT ULT COMPRA\"",
          "\"DT ULT COMPRA\"",
          "\"QT ULT SAIDA\"",
          "\"DT ULT SAIDA\""
        ));

        if (file_exists("{$caminho}" . '/' . "{$filename}")) {
          unlink("{$caminho}" . '/' . "{$filename}");
        }

        // Arquivo
        $handle = fopen("{$caminho}" . '/' . "{$filename}", 'w+');
        fwrite($handle, implode(";" . PHP_EOL, $headers));
        fwrite($handle, ";" . PHP_EOL);
        fflush($handle);

        // Fecha o arquivo da $_SESSION para liberar o servidor para servir outras requisições
        session_write_close();

        $output = array();
        foreach ($resultado as $value) {
            $output[] = implode(";", array(
              "\"{$value["idInsumo"]}\"",
              "\"{$value["cdInsumo"]}\"",
              "\"{$value["dsInsumo"]}\"",
              "\"{$value["dsGrupo"]}\"",
              "\"{$value["dsMarca"]}\"",
              "\"{$value["dsModelo"]}\"",
              "\"{$value["nrSerie"]}\"",
              "\"{$value["vlUnitario"]}\"",
              "\"{$value["dtCadastro"]}\"",
              "\"{$value["dtValidade"]}\"",
              "\"{$value["cdNCM"]}\"",
              "\"{$value["dsParceiro"]}\"",
              "\"{$value["qtUltimaCompra"]}\"",
              "\"{$value["dtUltimaCompra"]}\"",
              "\"{$value["qtUltimaVenda"]}\"",
              "\"{$value["dtUltimaVenda"]}\"",
            ));
        }
        fwrite($handle, implode(";" . PHP_EOL, $output));
        fwrite($handle, ";" . PHP_EOL);
        fflush($handle);
        fclose($handle);
        $this->download($caminho . '/' . $filename, 'CSV', $filename);        
    }

    private function download($nome, $tipo, $filename) {
      if (!empty($nome)) {
        if (file_exists($nome)) {
          header('Content-Transfer-Encoding: binary'); // For Gecko browsers mainly
          header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($nome)) . ' GMT');
          header('Accept-Ranges: bytes'); // For download resume
          header('Content-Length: ' . filesize($nome)); // File size
          header('Content-Encoding: none');
          header("Content-Type: application/{$tipo}"); // Change this mime type if the file is not PDF
          header('Content-Disposition: attachment; filename=' . $filename);
          // Make the browser display the Save As dialog
          readfile($nome);
          unlink($nome);
        }
      }
    }    
}

?>