<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class mensagem extends controller {
    
    public function index_action() {
        //Inicializa o Template
        $this->template->run();
    }

    public function mensagens() {
        
        // verifica se tem mensagens
        $modelMensagem = new mensagemModel();
        $where = 'mi.idUsuarioDestino = ' . $_SESSION['user']['usuario'] . ' or mi.idUsuarioOrigem = ' . $_SESSION['user']['usuario'];
        $temM = $modelMensagem->getMensagemItem($where);
        $this->smarty->assign("mensagens",$temM);         
        $this->smarty->display("dashboard/mensagem/thumbnail.tpl");         
    }
    
    public function buscaId() {
        
        $id = $_POST['idSolicitacao'];
        $descricao = $_POST['descricao'];
        
        // verifica se tem mensagens
        $modelMensagem = new mensagemModel();
        $where = "(mi.idUsuarioDestino = " . $_SESSION['user']['usuario'] . " or mi.idUsuarioOrigem = " . $_SESSION['user']['usuario'] . ")";
        if ($id) {
            $where .= " and m.idTabela = " . $id;
        }
        if ($descricao) {
            $where .= " and upper(mi.dsMensagem) like '%" . strtoupper($descricao) . "%'";
        }
        $temM = $modelMensagem->getMensagemItem($where);
        $this->smarty->assign("mensagens",$temM);         
        $html = $this->smarty->fetch("dashboard/mensagem/thumbnail_1.tpl"); 
        echo json_encode(array('html' => $html));
    }
    
    public function apagartodas() {
        
        // verifica se tem mensagens
        $modelMensagem = new mensagemModel();

        $where = "mi.idUsuarioDestino = " . $_SESSION['user']['usuario'] . " or mi.idUsuarioOrigem = " . $_SESSION['user']['usuario'];
        $temM = $modelMensagem->getMensagemItem($where);
        foreach ($temM as $value) {
            $array = array(
                'idMensagem' => $value['idMensagem']
                );
            $modelMensagem->delMensagemItem($array);
            $modelMensagem->delMensagem($array);
        }

        $where = "mi.idUsuarioDestino = " . $_SESSION['user']['usuario'] . " or mi.idUsuarioOrigem = " . $_SESSION['user']['usuario'];
        $temM = $modelMensagem->getMensagemItem($where);
        if ($temM) {
            $this->smarty->assign("mensagens",$temM);         
        } else {
            $this->smarty->assign("mensagens",null);         
        }
        $html = $this->smarty->fetch("dashboard/mensagem/thumbnail_1.tpl"); 
        echo json_encode(array('html' => $html));
    }
    
    public function digitarresposta() {
        $idMensagemItem = $_POST['idMensagemItem'];
        
        $where = 'mi.idMensagemItem = ' . $idMensagemItem;
        $model = new mensagemModel();
        $dadosPergunta = $model->getMensagemItem($where);
     //   var_dump($dadosPergunta); die;
        $idMensabemAnterior = $dadosPergunta[0]['idMensagemAnterior'];
        $dsTabela = $dadosPergunta[0]['dsNomeTabela'];
        $idTabela = $dadosPergunta[0]['idTabela'];
        $idUsuarioOrigem = $dadosPergunta[0]['idUsuarioOrigem'];

        if ($dsTabela == 'prodSolicitacaoCompras') {
            $where = 'idSolicitacao = ' . $idTabela;
            $model = new solicitacaocomprasModel();
            $dadosPergunta = $model->getSolicitacaoComprasItensE($where);
            if ($dadosPergunta) {
                $idpai = $dadosPergunta[0]['idSolicitacao'];
            } else {
                $idpai = null;
            }
        } else {
        if ($dsTabela == 'prodReuniaoParticipantes') {
            $where = 'P.idParticipante = ' . $idTabela;
            $model = new reuniaoModel();
            $dadosPergunta = $model->getParticipantes($where);
            if ($dadosPergunta) {
                $idpai = $dadosPergunta[0]['idParticipante'];
            } else {
                $idpai = null;
            }
        }}

        $model = new documentoModel();        
        $lista = $model->getDocumento("dsTabela = '" . $dsTabela . "' and idTabela  = " . $idTabela);
        $this->smarty->assign('fotos_lista', $lista);
        $this->smarty->assign('idUsuarioOrigem', $idUsuarioOrigem);
        $this->smarty->assign('idMensagemItem', $idMensabemAnterior);
        $this->smarty->assign('idSolicitacaoItem', $idTabela);
        $this->smarty->assign('idSolicitacaoPai', $idpai);
        $this->smarty->assign('dsTabela', $dsTabela);
        $this->smarty->display("dashboard/mensagem_responder/thumbnail.tpl");            
    }

    
    public function versetemmensagem() {
        // verificar se tem alguma nao lida
        $modelMensagem = new mensagemModel();
        $limpar = 0;
        $_SESSION['user']['temMensagem'] = 0;
        $where = "mi.idUsuarioDestino = " . $_SESSION['user']['usuario'] . " and mi.stSituacao = 0";
        $temNaoLida = $modelMensagem->getMensagemItem($where);
        if ($temNaoLida) {
            $_SESSION['user']['temMensagem'] = 1;
            $limpar = 1;
        }        
        $retorno = array('limpar' => $limpar);
        echo 'passsei';
        echo json_encode($retorno);   
    }
        
    public function marcarcomolido() {
        $idMensagemItem = $_POST['idMensagemItem'];
        $modelMensagem = new mensagemModel();
        $array = array(
          'stSituacao' => 1,  
          'dtLeitura' => date('Y-m-d H:i:s') 
        );
        $where = 'idMensagemItem = ' . $idMensagemItem;
        $modelMensagem->updMensagemItem($array, $where);

        // verificar se tem alguma nao lida
        $limpar = 0;
        $_SESSION['user']['temMensagem'] = 0;
        $where = "mi.idUsuarioDestino = " . $_SESSION['user']['usuario'] . " and mi.stSituacao = 0";
        $temNaoLida = $modelMensagem->getMensagemItem($where);
        if ($temNaoLida) {
            $_SESSION['user']['temMensagem'] = 1;
            $limpar = 1;
        }
        
        // trazer todas mensagens para o mesmo destino
        $where = 'mi.idUsuarioDestino = ' . $_SESSION['user']['usuario'];
        $temM = $modelMensagem->getMensagemItem($where);
        $this->smarty->assign("mensagens",$temM);         
        $html = $this->smarty->fetch("dashboard/mensagem/thumbnail.tpl");       
        $retorno = array('html' => $html,
            'limpar' => $limpar);
        echo json_encode($retorno);        
    }
    
    public function excluirmensagem() {
        $idMensagem = $_POST['idMensagem'];
        $idMensagemItem = $_POST['idMensagemItem'];
        $modelMensagem = new mensagemModel();
        $array = array('idMensagemItem' => $idMensagemItem);
        $modelMensagem->delMensagemItem($array);

        // trazer todas mensagens para o mesmo id
        $where = 'mi.idMensagem = ' . $idMensagem;
        $temM = $modelMensagem->getMensagemItem($where);
        if (!$temM) {
            $array = array('idMensagem' => $idMensagem);
            $modelMensagem->delMensagem($array);
        }
        
        // verificar se tem alguma nao lida
        $limpar = 0;
        $_SESSION['user']['temMensagem'] = 0;
        $where = "mi.idUsuarioDestino = " . $_SESSION['user']['usuario'] . " and mi.stSituacao = 0";
        $temNaoLida = $modelMensagem->getMensagemItem($where);
        if ($temNaoLida) {
            $_SESSION['user']['temMensagem'] = 1;
            $limpar = 1;
        }
        
        // trazer todas mensagens para o mesmo destino
        $where = 'mi.idUsuarioDestino = ' . $_SESSION['user']['usuario'];
        $temM = $modelMensagem->getMensagemItem($where);
        $this->smarty->assign("mensagens",$temM);         
        $html = $this->smarty->fetch("dashboard/mensagem/thumbnail.tpl");       
        $retorno = array('html' => $html,
            'limpar' => $limpar);
        echo json_encode($retorno);        
    }
    
    public function adicionarfoto() {
        $idMensagemItem = $_POST['idMensagemItem'];
        $idUsuarioOrigem = $_POST['idUsuarioOrigem'];
        $idSolicitacaoItem = $_POST['idSolicitacaoItem'];
        $dsTabela = $_POST['dsTabela'];
        $idSolicitacao = $_POST['idSolicitacaoPai'];
        $mensagem = $_POST['dsMensagem'];
        if ($_FILES['arquivo']['name']) {
            $extensao = explode('.',$_FILES['arquivo']['name'])[1];
            $novonome = $idSolicitacaoItem . '_' . date('Ymdis') . '.' . $extensao;

            $_UP['pasta'] = 'storage/tmp/documentos/';
            // Tamanho máximo do arquivo (em Bytes)
            $_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
            // Array com as extensões permitidas
            $_UP['extensoes'] = array('jpg','jpeg','png','bmp');
            // Renomeia o arquivo? (Se true, o arquivo será salvo como .jpg e um nome único)
            $_UP['renomeia'] = false;
            // Array com os tipos de erros de upload do PHP
            $_UP['erros'][0] = 'Não houve erro';
            $_UP['erros'][1] = 'O arquivo no upload é maior do que o limite do PHP';
            $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
            $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
            $_UP['erros'][4] = 'Não foi feito o upload do arquivo';
            // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
            if ($_FILES['arquivo']['error'] != 0) {
              die("Não foi possível fazer o upload, erro:" . $_UP['erros'][$_FILES['arquivo']['error']]);
              exit; // Para a execução do script
            }
            // Caso script chegue a esse ponto, não houve erro com o upload e o PHP pode continuar
            // Faz a verificação da extensão do arquivo
    //        $extensao = strtolower(end(explode('.', $_FILES['arquivo']['name'])));
    //        if (array_search($extensao, $_UP['extensoes']) === false) {
    //          echo "Por favor, envie arquivos com as seguinte extensão: csv";
    //          exit;
    //        }
            // Faz a verificação do tamanho do arquivo
            if ($_UP['tamanho'] < $_FILES['arquivo']['size']) {
              echo "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
              exit;
            }
            // O arquivo passou em todas as verificações, hora de tentar movê-lo para a pasta
            // Primeiro verifica se deve trocar o nome do arquivo
    //        if ($_UP['renomeia'] == true) {
    //          // Cria um nome baseado no UNIX TIMESTAMP atual e com extensão .jpg
    //          $nome_final = md5(time()).'.csv';
    //        } else {
              // Mantém o nome original do arquivo
    //          $nome_final = $_FILES['arquivo']['name'];
    //        }

            // Depois verifica se é possível mover o arquivo para a pasta escolhida
            if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $_UP['pasta'] . $novonome)) {
    //          Upload efetuado com sucesso, exibe uma mensagem e um link para o arquivo
    //          echo "Upload efetuado com sucesso!";
    //          echo '<a href="' . $_UP['pasta'] . $nome_final . '">Clique aqui para acessar o arquivo</a>';
    //          
    //            exit;
    //          $this->importar($_UP['pasta'] . $nome_final, $idPeriodo);
    //          header('Location: /planodecontas/novo_planodecontas/idPeriodo/' . $idPeriodo);

                $model = new documentoModel();    
                $dados = array(
                    'idDocumento' => null,
                    'dsTabela' => $dsTabela,
                    'idTabela' => $idSolicitacaoItem,
                    'dsLocalArquivo' => $_UP['pasta'] . $novonome                
                );
                $model->setDocumento($dados);

                if ($mensagem) {
                    $dadosi = array(
                        'idUsuarioOrigem' => $_SESSION['user']['usuario'],
                        'idUsuarioDestino' => $idUsuarioOrigem,
                        'idMensagemAnterior' => $idMensagemItem,
                        'dsNomeTabela' => $dsTabela,
                        'idOrigemInformacao' => 4,
                        'idTabela' => $idSolicitacao,
                        'idTipoMensagem' => 1,
                        'stSituacao' => 0,
                        'dtEnvio' => date('Y-m-d H:i:s'),
                        'dsMensagem' => $mensagem,
                        'dsCaminhoArquivo' => $_UP['pasta'] . $novonome
                    );
                    $this->criarMensagem($dadosi);
                }
            } else {
                if ($mensagem) {
                    $dadosi = array(
                        'idUsuarioOrigem' => $_SESSION['user']['usuario'],
                        'idUsuarioDestino' => $idUsuarioOrigem,
                        'idMensagemAnterior' => $idMensagemItem,
                        'dsNomeTabela' => $dsTabela,
                        'idOrigemInformacao' => 4,
                        'idTabela' => $idSolicitacao,
                        'idTipoMensagem' => 1,
                        'stSituacao' => 0,
                        'dtEnvio' => date('Y-m-d H:i:s'),
                        'dsMensagem' => $mensagem,
                        'dsCaminhoArquivo' => null
                    );
                    $this->criarMensagem($dadosi);
                }
            }    
            
        } else {
            if ($mensagem) {
                $dadosi = array(
                    'idUsuarioOrigem' => $_SESSION['user']['usuario'],
                    'idUsuarioDestino' => $idUsuarioOrigem,
                    'idMensagemAnterior' => $idMensagemItem,
                    'dsNomeTabela' => $dsTabela,
                    'idOrigemInformacao' => 4,
                    'idTabela' => $idSolicitacao,
                    'idTipoMensagem' => 1,
                    'stSituacao' => 0,
                    'dtEnvio' => date('Y-m-d H:i:s'),
                    'dsMensagem' => $mensagem,
                    'dsCaminhoArquivo' => null
                );
                $this->criarMensagem($dadosi);
            }            
        }
        
        header('Location: /dashboard');
        
    }
    public function criarMensagem($array) {
        
        $modelMensagem = new mensagemModel();
        $dados = array(
          'idMensagem' => null,  
          'idOrigemInformacao' => $array['idOrigemInformacao'],
          'dsNomeTabela' => $array['dsNomeTabela'],
          'idTabela' => $array['idTabela'],
          'idMensagemAnterior' => $array['idMensagemAnterior']             
        );
        $id = $modelMensagem->setMensagem($dados);
        $dados = array(
            'idMensagemItem' => null,  
            'idMensagem' => $id,  
            'idUsuarioOrigem' => $array['idUsuarioOrigem'],
            'idUsuarioDestino' => $array['idUsuarioDestino'],
            'idTipoMensagem' => $array['idTipoMensagem'],
            'stSituacao' => $array['stSituacao'],
            'dtEnvio' => $array['dtEnvio'],
            'dsMensagem' => $array['dsMensagem'],
            'dsCaminhoArquivo' => $array['dsCaminhoArquivo']
        );
        $id = $modelMensagem->setMensagemItem($dados);
        return;
    }
}
?>