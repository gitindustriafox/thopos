<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class pontopedido extends controller {

    private $onUm = 0;
    private $idSolicitacao = 0;
    
    public function index_action() {
        //Inicializa o Template
        $this->template->run();

        $modelLocalEstoque = new localestoqueModel();
        $lista_localestoque = array('' => 'SELECIONE');
        foreach ($modelLocalEstoque->getLocalEstoque() as $value) {
            $desanterior = $modelLocalEstoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
            if ($desanterior) {
                if ($value['idLocalEstoque'] == $value['idLocalEstoqueSuperior']) {
                    $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
                } else {
                    $lista_localestoque[$value['idLocalEstoque']] = $desanterior[0]['dsLocalEstoque'] . '-' . $value['dsLocalEstoque'];
                }
            } else {
                $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
            }
        }
        $pModel = new prioridadeModel();        
        $lista_Prioridade = array('' => 'SELECIONE');
        foreach ($pModel->getPrioridade() as $value) {
            $lista_Prioridade[$value['idPrioridade']] = $value['dsPrioridade'];
        }
        $modelCentroCusto = new centrocustoModel();
        $lista_centrocusto = array('' => 'SELECIONE');
        foreach ($modelCentroCusto->getCentroCustoCombo() as $value) {
            $lista_centrocusto[$value['idCentroCusto']] = $value['codigocusto'];
        }
        $modelGrupoDR = new grupodrModel();
        $lista_grupodr = array('' => 'SELECIONE');
        foreach ($modelGrupoDR->getGrupoDR() as $value) {
            $lista_grupodr[$value['idGrupoDR']] = $value['dsGrupoDR'];
        }

        $model = new estoqueModel();
        $where = 'e.qtEstoque < e.qtEstoqueMinimo';
        $insumo_lista = $model->getEstoqueAtual($where, $paginacao=true);
//        var_dump($insumo_lista); die;
        $this->smarty->assign('lista_grupodr', $lista_grupodr);
        
        $this->smarty->assign('lista_centrocusto', $lista_centrocusto);
        $this->smarty->assign('lista_prioridade', $lista_Prioridade);
        $this->smarty->assign('localestoque', $lista_localestoque);
        $this->smarty->assign('pontopedido_lista', $insumo_lista);
        $this->smarty->display('pontopedido/lista.html');
    }
    
    public function busca_criarsolicitacao() {
        
        if (isset($_POST['selecionar'])) {
            $dsParceiro = isset($_POST['dsParceiro']) ? $_POST['dsParceiro'] : '';
            $idParceiro = isset($_POST['idParceiro']) ? $_POST['idParceiro'] : '';
            $idPrioridade = isset($_POST['idPrioridade']) ? $_POST['idPrioridade'] : '';
            $idGrupoDR = isset($_POST['idGrupoDR']) ? $_POST['idGrupoDR'] : '';
            $idItemDR = isset($_POST['idItemDR']) ? $_POST['idItemDR'] : '';
            $idPrioridade = isset($_POST['idPrioridade']) ? $_POST['idPrioridade'] : '';
            $idCentroCusto = isset($_POST['idCentroCusto']) ? $_POST['idCentroCusto'] : '';
            $dsObservacao = isset($_POST['dsObservacao']) ? $_POST['dsObservacao'] : '';
            $quantidade = $_POST['qtde'];
            $this->criarSolicitacao($_POST['selecionar'], $quantidade, $idParceiro, $dsParceiro,$idPrioridade,$idCentroCusto, $dsObservacao, $idGrupoDR, $idItemDR);
        }
        
//        header('Location: /solicitacaocomprasmanutencao');        
       $this->busca_pontopedido();
    }
    
    public function busca_pontopedido() {
        
        //se nao existir o indice estou como padrao '';
        $dsProduto = isset($_POST['dsProduto']) ? $_POST['dsProduto'] : '';
        $dsParceiro = isset($_POST['dsParceiro']) ? $_POST['dsParceiro'] : '';
        $idParceiro = isset($_POST['idParceiro']) ? $_POST['idParceiro'] : '';
        $dsGrupo = isset($_POST['dsGrupo']) ? $_POST['dsGrupo'] : '';
        $cdInsumo = isset($_POST['cdInsumo']) ? $_POST['cdInsumo'] : '';
        $idLocalEstoque = isset($_POST['idLocalEstoque']) ? $_POST['idLocalEstoque'] : '';
        //$texto = '';
        $model = new estoqueModel();
        
        $busca = array();
        if ($dsGrupo) {
            if (strtoupper($dsGrupo) == 'EPI') {
                $sql = "upper(g.dsGrupo) like upper('%" . $dsGrupo . "%')";
            } else {
                $sql = 'e.qtEstoque < e.qtEstoqueMinimo';
                $sql = $sql . " and upper(g.dsGrupo) like upper('%" . $dsGrupo . "%')";
            }
            $busca['dsGrupo'] = $dsGrupo;
        } else {
            $sql = 'e.qtEstoque < e.qtEstoqueMinimo';
        }
        if ($dsProduto) {
            $sql = $sql . " and upper(i.dsInsumo) like upper('%" . $dsProduto . "%')";
            $busca['dsProduto'] = $dsProduto;
        }
        if ($cdInsumo) {
            $sql = $sql . " and upper(i.cdInsumo) like upper('%" . $cdInsumo . "%')";
            $busca['cdInsumo'] = $cdInsumo;
        }
        if ($idParceiro) {
            $sql = $sql . " and (i.idParceiro = {$idParceiro} or i.idParceiroUltimaCompra = {$idParceiro})";
            $busca['idParceiro'] = $idParceiro;
            $busca['dsParceiro'] = $dsParceiro;
        }
        if ($idLocalEstoque) {
            $varios = $this->CriarArrayLocalEstoque($idLocalEstoque);
            $sql = $sql . " and e.idLocalEstoque " . $varios;
            $busca['idLocalEstoque'] = $idLocalEstoque;
        }
        $cpagina = null;
        if (isset($_POST['cpagina'])) { 
            $cpagina = true;
            $busca['cpagina'] = 1;
        }
        
        $modelCentroCusto = new centrocustoModel();
        $lista_centrocusto = array('' => 'SELECIONE');
        foreach ($modelCentroCusto->getCentroCustoCombo() as $value) {
            $lista_centrocusto[$value['idCentroCusto']] = $value['codigocusto'];
        }
        $modelGrupoDR = new grupodrModel();
        $lista_grupodr = array('' => 'SELECIONE');
        foreach ($modelGrupoDR->getGrupoDR() as $value) {
            $lista_grupodr[$value['idGrupoDR']] = $value['dsGrupoDR'];
        }
        
        $modelLocalEstoque = new localestoqueModel();
        $lista_localestoque = array('' => 'SELECIONE');
        foreach ($modelLocalEstoque->getLocalEstoque() as $value) {
            $desanterior = $modelLocalEstoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
            if ($desanterior) {
                if ($value['idLocalEstoque'] == $value['idLocalEstoqueSuperior']) {
                    $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
                } else {
                    $lista_localestoque[$value['idLocalEstoque']] = $desanterior[0]['dsLocalEstoque'] . '-' . $value['dsLocalEstoque'];
                }
            } else {
                $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
            }
        }
        
        $insumo_lista = $model->getEstoqueAtualPP($sql, $cpagina);
        $x=0;
        foreach($insumo_lista as $value) {
            $desanterior = $modelLocalEstoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
            if ($desanterior) {
                if ($value['idLocalEstoque'] == $value['idLocalEstoqueSuperior']) {
                    $insumo_lista[$x]['localEstoque'] = $value['dsLocalEstoque'];
                } else {
                    $insumo_lista[$x]['localEstoque'] = $desanterior[0]['dsLocalEstoque'] . '-' . $value['dsLocalEstoque'];
                }
            } else {
                $insumo_lista[$x]['localEstoque'] = $value['dsLocalEstoque'];
            }
            $x++;
        }
        $pModel = new prioridadeModel();        
        $lista_Prioridade = array('' => 'SELECIONE');
        foreach ($pModel->getPrioridade() as $value) {
            $lista_Prioridade[$value['idPrioridade']] = $value['dsPrioridade'];
        }
//        var_dump($insumo_lista); die;

        $this->smarty->assign('lista_prioridade', $lista_Prioridade);
        $this->smarty->assign('lista_centrocusto', $lista_centrocusto);
        $this->smarty->assign('lista_grupodr', $lista_grupodr);
        
        if (sizeof($insumo_lista) > 0) {
            $this->smarty->assign('pontopedido_lista', $insumo_lista);
            //Chama o Smarty
            $this->smarty->assign('localestoque', $lista_localestoque);
            $this->smarty->assign('title', 'Estoque');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('pontopedido/lista.html');
        } else {
            $this->smarty->assign('pontopedido_lista', null);
            //Chama o Smarty
            $this->smarty->assign('localestoque', $lista_localestoque);
            $this->smarty->assign('title', 'Estoque');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('pontopedido/lista.html');
        }
    }
    
    public function CriarArrayLocalEstoque($idLocalEstoque) {
        $model = new localestoqueModel();
        
        $locaisestoque[0] = array();
        $retorno = $model->getLocalEstoque("idLocalEstoqueSuperior = " . $idLocalEstoque);
        $locaisestoque[0] = $retorno[0]['idLocalEstoque'];
        if ($retorno) {
            $sql = 'in (';
            foreach ($retorno as $value) {
                $sql = $sql . $value['idLocalEstoque'] . ',';                
            }
        }
        
        $x=0;
        foreach ($retorno as $value) {
            if ($x>0) {
                $retorno[$x] = $model->getLocalEstoque("idLocalEstoqueSuperior = " . $value['idLocalEstoque']);            
                foreach($retorno[$x] as $value1) {
                    $sql = $sql . $value1['idLocalEstoque'] . ',';                
                }
            }
            $x++;
        }
        
        $sql = substr($sql, 0, strlen($sql)-1) . ")";
        return $sql;
    }
    
    private function criarSolicitacao($dados, $quantidade, $idParceiro, $dsParceiro,$idPrioridade,$idCentroCusto, $dsObservacao, $idGrupoDR, $idItemDR) {
        
        $this->onUm = 0;
        foreach ($dados as $key => $value) {
            $idInsumo = $key;
            foreach ($dados[$key] as $idLocalEstoque => $value1) {
                $qtde = $quantidade[$idInsumo][$idLocalEstoque];
                $this->gravarASolicitacao($idInsumo,$idLocalEstoque, $qtde,$idPrioridade,$idCentroCusto, $idParceiro, $dsParceiro, $dsObservacao, $idGrupoDR, $idItemDR);
                 if ($dsParceiro == '' ) {
                     $this->onUm = 0;
                 }
            }
        }
    }

    private function gravarASolicitacao($idInsumo, $idLocalEstoque, $qtde,$idPrioridade,$idCentroCusto,$idParceiro, $dsParceiro, $dsObservacao, $idGrupoDR, $idItemDR) {      
        $modelEstoque = new estoqueModel();
        $where = 'e.idInsumo = ' . $idInsumo . ' and e.idLocalEstoque = ' . $idLocalEstoque;
        $dados = $modelEstoque->getEstoqueAtual($where);
        if ($dados) {
            if ($this->onUm == 0) {
                $value['idSolicitacao'] = '';
                $value['dtSolicitacao'] = '';
                $value['nrSolicitacao'] = '';
                $value['idPrioridade'] = $idPrioridade;
                if ($dsObservacao) {
                    $value['dsObservacao'] = $dsObservacao;
                } else {
                    $value['dsObservacao'] = "REPOSICAO DE ESTOQUE";
                }
                $value['dsLocalEntrega'] = 'CABREUVA';
                $this->idSolicitacao = $this->gravar_solicitacao_from_pontopedido($value);             
                $this->onUm = 1;
            }
            
            $valueitem['idSolicitacao'] = $this->idSolicitacao;
            $valueitem['idSolicitacaoItem'] = '';
            $valueitem['qtSolicitacao'] = $qtde;
            $valueitem['idInsumo'] = $idInsumo;
            $valueitem['idUnidade'] = $dados[0]['idUnidade'];
            $valueitem['idCentroCusto'] = $idCentroCusto;
            $valueitem['idGrupoDR'] = $idGrupoDR;
            $valueitem['idItemDR'] = $idItemDR;
            $valueitem['dsProduto'] = $dados[0]['dsInsumo'];
            $valueitem['idParceiro'] = $idParceiro;
            $valueitem['dsParceiroSugerido'] = $dsParceiro;
            $valueitem['dsObservacao'] = "REPOSICAO DE ESTOQUE LOCAL: " . $dados[0]['cdLocalEstoque'] . ' - ' . $dados[0]['dsLocalEstoque'];
            $valueitem['opcao'] = 0;
            $valueitem['idSituacao'] = 1;
            $id_item = $this->gravar_solicitacao_item_from_pp($valueitem);  
            
            $dadospedido = array(
                'stEmSolicitacao' => 1,
                'qtEmSolicitacao' => $qtde
            );
            $where = 'idInsumo = ' . $idInsumo . ' and idLocalEstoque = ' . $idLocalEstoque;
            $modelEstoque->updEstoquePP($dadospedido, $where);
            
        }
    }
    
    public function gravar_solicitacao_from_pontopedido($dados) {
        
        $model = new solicitacaocomprasModel();
        $data = $this->trataPostPP($dados);
        $id = $model->setSolicitacaoCompras($data);
        
        return $id;
    }    
    
    public function gravar_solicitacao_item_from_pp($dados) {
        $model = new solicitacaocomprasModel();
        $data = $this->trataPostItemPP($dados);
        $id = $model->setSolicitacaoComprasItem($data);    
        return $id;
    }

    //Trata dados antes de Enviar para o Gravar
    private function trataPostPP($post) {
        $data = array();
        $data['idSolicitacao'] = ($post['idSolicitacao'] != '') ? $post['idSolicitacao'] : null;;
        $data['dtSolicitacao'] = ($post['dtSolicitacao'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $_POST["dtSolicitacao"]))) : date('Y-m-d h:m:s');
        $data['nrSolicitacao'] = ($post['nrSolicitacao'] != '') ? $post['nrSolicitacao'] : null;
        $data['idPrioridade'] = ($post['idPrioridade'] != '') ? $post['idPrioridade'] : null;
        $data['dsObservacao'] = ($post['dsObservacao'] != '') ? $post['dsObservacao'] : null;
        $data['dsLocalEntrega'] = ($post['dsLocalEntrega'] != '') ? $post['dsLocalEntrega'] : null;
        $data['idUsuarioSolicitante'] = $_SESSION['user']['usuario'];
        $data['idUsuarioDigitacao'] = $_SESSION['user']['usuario'];
        $data['dsSolicitante'] = $_SESSION['user']['nome'];
        
        return $data;
    }
    
    private function trataPostItemPP($post) {
        $data = array();        
        $data['idSolicitacao'] = ($post['idSolicitacao'] != '') ? $post['idSolicitacao'] : null;
        $data['idSolicitacaoItem'] = ($post['idSolicitacaoItem'] != '') ? $post['idSolicitacaoItem'] : null;
        $data['dsObservacao'] = ($post['dsObservacao'] != '') ? $post['dsObservacao'] : null;
        $data['idSituacao'] = ($post['idSituacao'] != '') ? $post['idSituacao'] : null;
        $data['idInsumo'] = ($post['idInsumo'] != '') ? $post['idInsumo'] : null;
        $data['idUnidade'] = ($post['idUnidade'] != '') ? $post['idUnidade'] : null;
        $data['idGrupoDR'] = ($post['idGrupoDR'] != '') ? $post['idGrupoDR'] : null;
        $data['idItemDR'] = ($post['idItemDR'] != '') ? $post['idItemDR'] : null;
        $data['dsProduto'] = ($post['dsProduto'] != '') ? $post['dsProduto'] : null;
        $data['idCentroCusto'] = ($post['idCentroCusto'] != '') ? $post['idCentroCusto'] : null;
        $data['qtSolicitacao'] = ($post['qtSolicitacao'] != '') ? str_replace(",",".",str_replace(".","",$post['qtSolicitacao'])) : null;        
        $data['dsParceiroSugerido'] = ($post['dsParceiroSugerido'] != '') ? $post['dsParceiroSugerido'] : null;
        $data['idParceiro'] = ($post['idParceiro'] != '') ? $post['idParceiro'] : null;
        $data['stTipoIS'] = 0;
        return $data;
    }

}

?>