<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class planodecontas extends controller {

    public function index_action() {
//die("chegou");
        //Inicializa o Template
        $this->template->run();

        $model = new periodoContabilModel();
        $periodo_lista = $model->getPeriodo(null,$paginacao=true);

        $this->smarty->assign('periodo_lista', $periodo_lista);
        $this->smarty->display('contabilidade/listaPlanoDeContas.html');
    }

//Funcao de Busca
    public function busca_periodo() {
        //se nao existir o indice estou como padrao '';
        $texto = isset($_POST['buscadescricao']) ? $_POST['buscadescricao'] : '';
        //$texto = '';
        $model = new periodoContabilModel();
        $sql = "upper(dsPeriodo) like upper('%" . $texto . "%')"; //somente os nao excluidos
        $resultado = $model->getPeriodo($sql, $paginacao=true);

        if (sizeof($resultado) > 0) {
            $this->smarty->assign('periodo_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'periodo');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('contabilidade/listaPlanoDeContas.html');
        } else {
            $this->smarty->assign('periodo_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'periodo');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('contabilidade/listaPlanoDeContas.html');
        }
    }

    //Funcao de Inserir
    public function novo_planodecontas() {
        $sy = new system\System();
        $idPeriodo = $sy->getParam('idPeriodo');

        $model = new periodoContabilModel();

        if ($idPeriodo > 0) {
            $registro = $model->getPeriodo('idPeriodo=' . $idPeriodo);
            $registro = $registro[0]; //Passando Periodo
        } else {
            //Novo Registro
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        
        $model = new planodecontasModel();
        $where = 'p.idPeriodo = ' . $idPeriodo;
        $listaContas = $model->getPlanoDeContas($where);
        
        $model = new planodecontasModel();
        $where = 'p.idPeriodo = ' . ($idPeriodo - 1);
        $temperiodoanterior = $model->getPlanoDeContas($where);
        if ($temperiodoanterior) {
            $this->smarty->assign('temAnterior', 1);
        } else {
            $this->smarty->assign('temAnterior', 0);            
        }
        
        
        if ($listaContas) {
            $this->smarty->assign('temlista', 1);
        } else {
            $this->smarty->assign('temlista', 0);
        }
        
        $this->smarty->assign('contasitens', $listaContas);
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('title', 'Novo Periodo');
        $this->smarty->display('contabilidade/novoPlanoDeContas.tpl');
    }

    // Gravar Padrao
    public function gravar_planodecontas() {
        $model = new planodecontasModel();
        $data = $this->trataPost($_POST);

        if ($data['idConta'] == null) {
            $id=$model->setPlanoDeContas($data);
        } else {
            $id=$data['idConta'];
            $model->updPlanoDeContas($data); //update
        }    
        header('Location: /planodecontas/novo_planodecontas/idPeriodo/' . $data['idPeriodo']);
    }
    
    public function importarplanodecontas_mostrar() {
        $idPeriodo = $_POST['idPeriodo'];
        $this->smarty->assign('idPeriodo', $idPeriodo);
        $this->smarty->display("contabilidade/modalPlanoDeContas/thumbnail.tpl");        
    }
    
    public function importarplanodecontas() {
        $idPeriodo = $_POST['idPeriodo'];
        $_UP['pasta'] = 'storage/tmp/arquivosparaimportar/';
        // Tamanho máximo do arquivo (em Bytes)
        $_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
        // Array com as extensões permitidas
        $_UP['extensoes'] = array('csv');
        // Renomeia o arquivo? (Se true, o arquivo será salvo como .jpg e um nome único)
        $_UP['renomeia'] = false;
        // Array com os tipos de erros de upload do PHP
        $_UP['erros'][0] = 'Não houve erro';
        $_UP['erros'][1] = 'O arquivo no upload é maior do que o limite do PHP';
        $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
        $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
        $_UP['erros'][4] = 'Não foi feito o upload do arquivo';
        // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
        if ($_FILES['arquivo']['error'] != 0) {
          die("Não foi possível fazer o upload, erro:" . $_UP['erros'][$_FILES['arquivo']['error']]);
          exit; // Para a execução do script
        }
        // Caso script chegue a esse ponto, não houve erro com o upload e o PHP pode continuar
        // Faz a verificação da extensão do arquivo
//        $extensao = strtolower(end(explode('.', $_FILES['arquivo']['name'])));
//        if (array_search($extensao, $_UP['extensoes']) === false) {
//          echo "Por favor, envie arquivos com as seguinte extensão: csv";
//          exit;
//        }
        // Faz a verificação do tamanho do arquivo
        if ($_UP['tamanho'] < $_FILES['arquivo']['size']) {
          echo "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
          exit;
        }
        // O arquivo passou em todas as verificações, hora de tentar movê-lo para a pasta
        // Primeiro verifica se deve trocar o nome do arquivo
        if ($_UP['renomeia'] == true) {
          // Cria um nome baseado no UNIX TIMESTAMP atual e com extensão .jpg
          $nome_final = md5(time()).'.csv';
        } else {
          // Mantém o nome original do arquivo
          $nome_final = $_FILES['arquivo']['name'];
        }

        // Depois verifica se é possível mover o arquivo para a pasta escolhida
        if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $_UP['pasta'] . $nome_final)) {
//          Upload efetuado com sucesso, exibe uma mensagem e um link para o arquivo
//          echo "Upload efetuado com sucesso!";
//          echo '<a href="' . $_UP['pasta'] . $nome_final . '">Clique aqui para acessar o arquivo</a>';
//          
//            exit;
          $this->importar($_UP['pasta'] . $nome_final, $idPeriodo);
          header('Location: /planodecontas/novo_planodecontas/idPeriodo/' . $idPeriodo);
          
        } else {
          // Não foi possível fazer o upload, provavelmente a pasta está incorreta
          echo "Não foi possível enviar o arquivo, tente novamente";
        }    
        
    }
    
    private function importar($arquivo, $idPeriodo) {
        global $PATH;

        $delimitador = ',';
        $cerca = '"';
        $f = fopen($arquivo, 'r');
        
        while (($data = fgetcsv($f, 0, $delimitador, $cerca)) !== FALSE) {
            $dados = array();
            $dados['idContaReduzida'] = $data[0];
            $dados['idCE1'] = $data[1];
            $dados['idCE2'] = $data[2];
            $dados['idCE3'] = $data[3];
            $dados['idCE4'] = $data[4];
            $dados['idCE5'] = $data[5];
            $dados['stTipoConta'] = $data[6];
            $dados['dsConta'] = $data[7];
            $dados['vlSaldoInicial'] = $data[8];
            $dados['idConta'] = '';
            $dados['idPeriodo'] = $idPeriodo;
            $this->lerdados_csv($dados);  
        }     

        fclose($f);
        
    }

    public function importarExAnterior() {
        $sy = new system\System();                
        $idPeriodo = $sy->getParam('idPeriodo');
        $model = new planodecontasModel();
        
        $where = 'idPeriodo = ' . ($idPeriodo - 1);
        $periodoanterior = $model->getPlanoDeContas($where);
        if ($periodoanterior) {
            foreach($periodoanterior as $value) {
                $dados = array();
                
                $where = 'idPeriodo = ' . $idPeriodo . ' and idContaReduzida = ' . $value['idContaReduzida'];
                $periodoatual = $model->getPlanoDeContas($where);
                if ($periodoatual) {
                    $dados['vlSaldoInicial'] = doubleval($value['vlSaldoFinal']);
                    $dados['idConta'] = $periodoatual[0]['idConta'];
                    $model->updPlanoDeContasCP($dados);
                } else {
                    $dados['idContaReduzida'] = $value['idContaReduzida'];
                    $dados['idCE1'] = $value['idCE1'];
                    $dados['idCE2'] = $value['idCE2'];
                    $dados['idCE3'] = $value['idCE3'];
                    $dados['idCE4'] = $value['idCE4'];
                    $dados['idCE5'] = $value['idCE5'];
                    $dados['stTipoConta'] = $value['stTipoConta'];
                    $dados['dsConta'] = $value['dsConta'];
                    $dados['vlSaldoInicial'] = doubleval($value['vlSaldoFinal']);
                    $dados['idConta'] = '';
                    $dados['idPeriodo'] = $idPeriodo;
                 //   var_dump($dados);
                    $data = $this->trataPost($dados);
                    $id=$model->setPlanoDeContas($data);
                }
            }
        }
        header('Location: /planodecontas/novo_planodecontas/idPeriodo/' . $idPeriodo);        
    }
    
    private function lerdados_csv($dados) {
        $model = new planodecontasModel();
        $data = $this->trataPost($dados);

        $id=$model->setPlanoDeContas($data);        
    }
    
    private function trataPost($post) {
        $data['idConta'] = ($post['idConta'] != '') ? $post['idConta'] : null;
        $data['idContaReduzida'] = ($post['idContaReduzida'] != '') ? $post['idContaReduzida'] : null;
        $data['idPeriodo'] = ($post['idPeriodo'] != '') ? $post['idPeriodo'] : null;
        $data['idCE1'] = ($post['idCE1'] != '') ? $post['idCE1'] : 0;
        $data['idCE2'] = ($post['idCE2'] != '') ? $post['idCE2'] : 0;
        $data['idCE3'] = ($post['idCE3'] != '') ? $post['idCE3'] : 0;
        $data['idCE4'] = ($post['idCE4'] != '') ? $post['idCE4'] : 0;
        $data['idCE5'] = ($post['idCE5'] != '') ? $post['idCE5'] : 0;
        $data['dtCriacao'] = date("Y-m-d");
        $data['dsConta'] = ($post['dsConta'] != '') ? $post['dsConta'] : 'A';
        $data['stTipoConta'] = ($post['stTipoConta'] != '') ? $post['stTipoConta'] : 'A';
        $data['vlSaldoInicial'] = ($post['vlSaldoInicial'] != '') ? str_replace(",",".",str_replace(".","",$post['vlSaldoInicial'])) : null;
        return $data;
    }

    public function ler_conta() {
        $idPeriodo = $_POST['idPeriodo'];        
        $idContaReduzida = $_POST['idContaReduzida'];         
        $model = new planodecontasModel();
        $where = 'p.idPeriodo = ' . ($idPeriodo - 1) . ' and pc.idContaReduzida = ' . $idContaReduzida;
        $dados = $model->getPlanoDeContas($where);
        $saldo = null;
        if ($dados) {
            $saldo = str_replace(".",",",$dados[0]['vlSaldoFinal']);
        }
        $retorno = array(
            'saldo' => $saldo
        );
//        var_dump($retorno); die;
        echo json_encode($retorno);
    }
    
    public function del_conta() {
        $idPeriodo = $_POST['idPeriodo'];        
        $idConta = $_POST['idConta'];         
        $model = new planodecontasModel();
        $where = 'idPeriodo = ' . $idPeriodo . ' and idConta = ' . $idConta;
        $model->delPlanoDeContas($where);
        $retorno = array();
        echo json_encode($retorno);
    }

}

?>