<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */
use mensagem as mensagem;

class orcamentocompras extends controller {

    private $onUm = 0;
    private $idPedido = 0;
    private $idOrcamentoCompras = 0;
    
    public function index_action() {
        //Inicializa o Template
        $this->template->run();
        unset($_SESSION['solicitacaocompras']['id']);
        
        $model = new solicitacaocomprasModel();
        $registro = $model->getSolicitacaoComprasOrcamento('oc.idSituacao < 3',false);
//        var_dump($registro); die;
        $pModel = new prioridadeModel();        
        $lista_Prioridade = array('' => 'SELECIONE');
        foreach ($pModel->getPrioridade() as $value) {
            $lista_Prioridade[$value['idPrioridade']] = $value['dsPrioridade'];
        }
        
        $modelSituacao = new situacaoModel();
        $lista_situacao = array('' => 'SELECIONE');
        foreach ($modelSituacao->getSituacao() as $value) {
            $lista_situacao[$value['idSituacao']] = $value['dsSituacao'];
        }
        
        $this->smarty->assign('lista_prioridade', $lista_Prioridade);
        $this->smarty->assign('lista_situacao', $lista_situacao);
        $this->smarty->assign('orcamentocompras', $registro);
        $this->smarty->assign('title', 'Orcamento de Compras');
        $this->smarty->display('orcamentocompras/listaManutencao.html');
        
    }

    public function emitirorcamentocompra() {
        $idOrcamentoCompras = $_POST['idOrcamentoCompras'];

        $where = 'oc.idOrcamentoCompras = ' . $idOrcamentoCompras;
        $model = new solicitacaocomprasModel();
        $cabec = $model->getOrcamentoCompras($where)[0];
        $itens = $model->getOrcamentoComprasItens($where);
        $fornecedores = $model->getOrcamentoComprasParceiros($where)[0];
        
//        var_dump($cabec);
//        var_dump($itens);
//        var_dump($fornecedores); die;
        $lista_emp = $this->carrega_empresas();
        
        $this->smarty->assign('lista_empresa', $lista_emp);
        $this->smarty->assign('cabec', $cabec);
        $this->smarty->assign('orcamentoitens', $itens);
        $this->smarty->assign('parceiro', $fornecedores);
        $this->smarty->display("orcamentocompras/orcamento/modal/thumbnail.tpl");        
    }
    private function carrega_empresas() {
        $lista_emp = null;
        $model = new empresaModel();
        $lista_emp = array('' => 'SELECIONE');
        foreach ($model->getEmpresa() as $value) {
            $lista_emp[$value['idEmpresa']] =  $value['dsEmpresa'];
        }
        return $lista_emp;
    }
    

    public function delorcamentocompras() {   
        $sy = new system\System();        
        $idOrcamentoCompras = $sy->getParam('idOrcamentoCompras');
        $model = new solicitacaocomprasModel();

        // atualizar a cotacao
        $dados = array('idSituacao' => 3);
        $where = 'idOrcamentoCompras = ' . $idOrcamentoCompras;
        $model->updOrcamentoCompras($dados, $where);
        // $model->delSolicitacao($where);
        header('Location: /orcamentocompras');        
        return;
    }
    
//Funcao de Busca
    
    public function busca_criarpedido() {

        if (isset($_POST['selecionar'])) {
            $dsParceiro = isset($_POST['dsParceiro']) ? $_POST['dsParceiro'] : '';
            $idParceiro = isset($_POST['idParceiro']) ? $_POST['idParceiro'] : '';
            $valor = $_POST['valor'];            
            $this->criarPedido($_POST['selecionar'], $valor, $dsParceiro, $idParceiro);
        }
        
        header('Location: /pedidoaberto');
        //$this->busca_solicitacaocompras();
    }
    
    public function busca_criarcotacao() {

        if (isset($_POST['selecionar'])) {
            $this->criarCotacao($_POST['selecionar']);
        }
        
        header('Location: /solicitacaocomprasmanutencao');
        //$this->busca_solicitacaocompras();
    }    
    
    public function busca_criarcsv() {

        if (isset($_POST['selecionar'])) {
            $dsParceiro = isset($_POST['dsParceiro']) ? $_POST['dsParceiro'] : '';
            $idParceiro = isset($_POST['idParceiro']) ? $_POST['idParceiro'] : '';
            $this->criarCSV($_POST['selecionar'], $idParceiro, $dsParceiro);
        }
     //   header('Location: /solicitacaocomprasmanutencao');
    }
    
    public function criarCSV($dados, $idParceiro, $dsParceiro) {
        
        $limit = 30000;
        $offset = 0;

        global $PATH;
        $caminho = "/var/www/html/thopos.com.br/site/storage/tmp/csv/";

        // Storage
        if (!is_dir($caminho)) {
          mkdir($caminho, 0777, true);
        }

        $filename = "{$idParceiro}_" . date("YmsHis") . ".csv";

        $headers[] = implode(";", array(
          "\"PRODUTO\"",
          "\"QTE\"",
          "\"UNIDADE\""
        ));

        if (file_exists("{$caminho}" . '/' . "{$filename}")) {
          unlink("{$caminho}" . '/' . "{$filename}");
        }

        // Arquivo
        $handle = fopen("{$caminho}" . '/' . "{$filename}", 'w+');
        fwrite($handle, implode(";" . PHP_EOL, $headers));
        fwrite($handle, ";" . PHP_EOL);
        fflush($handle);

        // Fecha o arquivo da $_SESSION para liberar o servidor para servir outras requisições
        session_write_close();

        $output = array();
        $modelsc = new solicitacaocomprasModel();
        foreach ($dados as $key => $value) {
            $idSolicitacao = $key;
            foreach ($dados[$key] as $idSolicitacaoItem => $value1) {
                $dadosret = $modelsc->getSolicitacaoComprasItens('a.idSolicitacao = ' . $idSolicitacao . ' and a.idSolicitacaoItem = ' . $idSolicitacaoItem);
                $value = $dadosret[0];
                $valueitem = array();
                $valueitem['qtSolicitacao'] = $value['qtSolicitacao'];
                if ($value['dsServico']) {
                    $valueitem['dsInsumo'] = $value['dsServico'];
                } else {
                    $valueitem['dsInsumo'] = $value['dsProduto'];
                }
                $valueitem['dsUnidade'] = $value['dsUnidade'];

                $output[] = implode(";", array(
                  "\"{$valueitem["dsInsumo"]}\"",
                  "\"{$valueitem["qtSolicitacao"]}\"",
                  "\"{$valueitem["dsUnidade"]}\""
                ));
            }
        }
        fwrite($handle, implode(";" . PHP_EOL, $output));
        fwrite($handle, ";" . PHP_EOL);
        fflush($handle);
        fclose($handle);
        $this->download($caminho . '/' . $filename, 'CSV', $filename);        
    }

    private function download($nome, $tipo, $filename) {
      if (!empty($nome)) {
        if (file_exists($nome)) {
          header('Content-Transfer-Encoding: binary'); // For Gecko browsers mainly
          header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($nome)) . ' GMT');
          header('Accept-Ranges: bytes'); // For download resume
          header('Content-Length: ' . filesize($nome)); // File size
          header('Content-Encoding: none');
          header("Content-Type: application/{$tipo}"); // Change this mime type if the file is not PDF
          header('Content-Disposition: attachment; filename=' . $filename);
          // Make the browser display the Save As dialog
          readfile($nome);
          unlink($nome);
        }
      }
    }
    
    public function busca_orcamentocompras() {
        
        //se nao existir o indice estou como padrao '';
        $dsSolicitante = isset($_POST['dsSolicitante']) ? $_POST['dsSolicitante'] : '';
        $dsProduto = isset($_POST['dsProduto']) ? $_POST['dsProduto'] : '';
        $cdInsumo = isset($_POST['cdInsumo']) ? $_POST['cdInsumo'] : '';
        $dsParceiro = isset($_POST['dsParceiro']) ? $_POST['dsParceiro'] : '';
        $idParceiro = isset($_POST['idParceiro']) ? $_POST['idParceiro'] : '';
        $dsObservacao = isset($_POST['dsObservacao']) ? $_POST['dsObservacao'] : '';
        $idSituacao = isset($_POST['idSituacao']) ? $_POST['idSituacao'] : '';
        $idPrioridade = isset($_POST['idPrioridade']) ? $_POST['idPrioridade'] : '';
        $dsLocalEntrega = isset($_POST['dsLocalEntrega']) ? $_POST['dsLocalEntrega'] : '';
        //$texto = '';
        $model = new solicitacaocomprasModel();
        
        $pModel = new prioridadeModel();        
        $lista_Prioridade = array('' => 'SELECIONE');
        foreach ($pModel->getPrioridade() as $value) {
            $lista_Prioridade[$value['idPrioridade']] = $value['dsPrioridade'];
        }
        
        $modelSituacao = new situacaoModel();
        $lista_situacao = array('' => 'SELECIONE');
        foreach ($modelSituacao->getSituacao() as $value) {
            $lista_situacao[$value['idSituacao']] = $value['dsSituacao'];
        }
        
        $busca = array();
        $sql = 'a.idSolicitacao > 0';
        if ($idSituacao) {
            $sql = $sql . " and si.idSituacao = " . $idSituacao;
            $busca['idSituacao'] = $idSituacao;
        }
        if ($idPrioridade) {
            $sql = $sql . " and a.idPrioridade = " . $idPrioridade;
            $busca['idPrioridade'] = $idPrioridade;
        }
        if ($dsProduto) {
            $sql = $sql . " and upper(i.dsProduto) like upper('%" . $dsProduto . "%')";
            $busca['dsProduto'] = $dsProduto;
        }
        if ($idParceiro) {
            $sql = $sql . " and ocp.idParceiro = " . $idParceiro ;
            $busca['dsParceiro'] = $dsParceiro;
            $busca['idParceiro'] = $idParceiro;
        }
        if ($dsObservacao) {
            $sql = $sql . " and upper(a.dsObservacao) like upper('%" . $dsObservacao . "%')";
            $busca['dsObservacao'] = $dsObservacao;
        }
        if ($dsSolicitante) {
            $sql = $sql . " and upper(a.dsSolicitante) like upper('%" . $dsSolicitante . "%')";
            $busca['dsSolicitante'] = $dsSolicitante;
        }
        if ($dsLocalEntrega) {
            $sql = $sql . " and upper(a.dsLocalEntrega) like upper('%" . $dsLocalEntrega . "%')";
            $busca['dsLocalEntrega'] = $dsLocalEntrega;
        }
        if ($cdInsumo) {
            $sql = $sql . " and upper(ins.cdInsumo) like upper('%" . $cdInsumo . "%')";
            $busca['cdInsumo'] = $cdInsumo;
        }

        $cpagina = null;
        if (isset($_POST['cpagina'])) { 
            $cpagina = true;
            $busca['cpagina'] = 1;
        }
        
        $_SESSION['solicitacao']['where'] = $sql;
        $resultado = $model->getSolicitacaoComprasOrcamento($sql, $cpagina);

        if (sizeof($resultado) > 0) {
            
            $this->smarty->assign('lista_prioridade', $lista_Prioridade);
            $this->smarty->assign('lista_situacao', $lista_situacao);            
            $this->smarty->assign('orcamentocompras', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'solicitacaocompras');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('orcamentocompras/listaManutencao.html');
        } else {
            $this->smarty->assign('lista_prioridade', $lista_Prioridade);
            $this->smarty->assign('lista_situacao', $lista_situacao);            
            $this->smarty->assign('orcamentocompras', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'solicitacaocompras');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('orcamentocompras/listaManutencao.html');
        }
    }

    public function criarCotacao($dados) {
        $this->onUm = 0;        
        foreach ($dados as $key => $value) {
            $solicitacao = $key;
            foreach ($dados[$key] as $key1 => $value1) {
                $this->gravarCotacao($solicitacao,$key1);
            }
        }
    }

    private function gravarCotacao($idSolicitacao, $idSolicitacaoItem) {    
        
        $modelsc = new solicitacaocomprasModel();
        $model = new pedidoModel();
        
        $dados = $modelsc->getSolicitacaoComprasItens('a.idSolicitacao = ' . $idSolicitacao . ' and a.idSituacao < 4 and a.idSolicitacaoItem = ' . $idSolicitacaoItem);
        if ($dados) {
            if ($this->onUm == 0) {
                $value=array();
                $value['idOrcamentoCompras'] = null;
                $value['idUsuario'] = $_SESSION['user']['usuario'];
                $value['dtOrcamentoCompras'] = date('Y-m-d');
                $value['idSituacao'] = 1;
                $value['dsObservacao'] = $dados[0]['dsObservacao'];
                $value['idEmpresa'] = null;
                $value['dsLocalEntrega'] = null;
                $value['dsFrete'] = null;
                $value['idPrioridade'] = 0;
                $this->idOrcamentoCompras = $model->setOrcamentoCompras($value);
                $this->onUm = 1;
            }    
            $valueitem = array();
            $valueitem['idOrcamentoCompras'] = $this->idOrcamentoCompras;
            $valueitem['idOrcamentoComprasItem'] = null;
            $valueitem['idOrcamentoComprasParceiro'] = null;
            $valueitem['qtOrcamento'] = $dados[0]['qtSolicitacao'];
            $valueitem['idInsumo'] = $dados[0]['idInsumo'];
            $valueitem['vlOrcamento'] = 0;
            $valueitem['dsObservacao'] = null;
            $valueitem['stTipoIS'] = $dados[0]['stTipoIS'];
            //$id_item = $this->gravar_pedido_item_from_solicitacao($valueitem);                  
            $id_item = $model->setOrcamentoComprasItens($valueitem);    

            // colocar aqui onde devo gravar o numero do pedido no ITEM DA SOLICITACAO

            if ($dados[0]['dsServico']) {
                $dsMensagem = 'SEU SERVICO ' . $dados[0]['dsServico'] . ' ESTÁ EM COTAÇÃO';
            } else {
                $dsMensagem = 'SEU PRODUTO ' . $dados[0]['dsInsumo'] . ' ESTÁ EM COTAÇÃO';
            }
            
            // ler usuario da solictiacao para enviar msg
            $dadossci = $modelsc->getSolicitacaoComprasItens('a.idSolicitacaoItem = ' . $idSolicitacaoItem);
            if ($dadossci) {
                if ($dadossci[0]['idUsuarioSolicitante'] <> $_SESSION['user']['usuario']) {            
                    $dadosim = array(
                        'idUsuarioOrigem' => 999,
                        'idUsuarioDestino' => $dadossci[0]['idUsuarioSolicitante'],
                        'dsNomeTabela' => 'prodSolicitacaoCompras',
                        'idOrigemInformacao' => 99,
                        'idTabela' => $idSolicitacaoItem,
                        'idTipoMensagem' => 0,
                        'stSituacao' => 0,
                        'dtEnvio' => date('Y-m-d H:i:s'),
                        'dsMensagem' => $dsMensagem
                    );
                    $this->criarMensagem($dadosim);
                }
            }    
        }
    }
    
    public function criarPedido($dados, $valor, $dsParceiro, $idParceiro) {
        $this->onUm = 0;
        foreach ($dados as $key => $value) {
            $solicitacao = $key;
            foreach ($dados[$key] as $key1 => $value1) {
                $valor_pedido = $valor[$solicitacao][$key1];
                $this->gravarOPedido($solicitacao,$key1, $valor_pedido, $idParceiro);
                 if ($dsParceiro == '' ) {
                     $this->onUm = 0;
                 }
            }
        }
    }

    private function gravarOPedido($idSolicitacao, $idSolicitacaoItem, $valor_pedido, $idParceiro) {    
        
        $valor_pedido = str_replace(",",".",str_replace(".","",$valor_pedido));
//        var_dump($idSolicitacao);
//        var_dump($idSolicitacaoItem);
//        var_dump($valor_pedido); 
        $modelsc = new solicitacaocomprasModel();
        $model = new pedidoModel();
        
        $dados = $modelsc->getSolicitacaoComprasItens('a.idSolicitacao = ' . $idSolicitacao . ' and a.idSituacao < 4 and a.idSolicitacaoItem = ' . $idSolicitacaoItem);
//        var_dump($dados); die;
        if ($dados) {
            if ($this->onUm == 0) {
                $value=array();
                $value['idPedido'] = null;
                $value['idParceiro'] = $idParceiro;
                $value['dtPedido'] = date('Y-m-d');
                $value['dtPrazoEntrega'] = $dados[0]['dtNecessidade'];
                $value['nrPedido'] = '';
                $value['dsObservacao'] = $dados[0]['dsObservacao'];
                $value['dsComplementoEntrega'] = $dados[0]['dsLocalEntrega'];
                $value['idUsuarioSolicitante'] = $dados[0]['idUsuarioSolicitante'];
                $value['dsSolicitante'] = $dados[0]['dsSolicitante'];
                $value['idPrioridade'] = $dados[0]['idPrioridade'];
                //$this->idPedido = $this->gravar_pedido_from_solicitacao($value);    
                $this->idPedido = $model->setPedido($value);
                $this->onUm = 1;                
            }
            $valueitem = array();
            $valueitem['idPedido'] = $this->idPedido;
            $valueitem['idPedidoItem'] = null;
            $valueitem['qtPedido'] = $dados[0]['qtSolicitacao'];
            $valueitem['idInsumo'] = $dados[0]['idInsumo'];
            $valueitem['idOS'] = $dados[0]['idOS'];
            $valueitem['idCentroCusto'] = $dados[0]['idCentroCusto'];
            $valueitem['idMaquina'] = $dados[0]['idMaquina'];
            $valueitem['idMotivo'] = $dados[0]['idMotivo'];
            $valueitem['vlPedido'] = $valor_pedido;
            $valueitem['idLocalEstoque'] = '';
            $valueitem['dsObservacao'] = $dados[0]['dsObservacao'];
            $valueitem['stTipoIS'] = $dados[0]['stTipoIS'];
            $valueitem['idSituacaoPedido'] = 1;
            //$id_item = $this->gravar_pedido_item_from_solicitacao($valueitem);                  
            $id_item = $model->setPedidoItem($valueitem);    

            // colocar aqui onde devo gravar o numero do pedido no ITEM DA SOLICITACAO

            $dsMensagem = 'ALTERACAO NO ITEM DA SUA SOLICITACAO DE COMPRAS';
            // situacao atual antes da alteracao
            $where = 'a.idSolicitacaoItem = ' . $idSolicitacaoItem;        
            $anterior = $modelsc->getSolicitacaoComprasItens($where);
            $dsAnterior = null;
            if ($anterior) {
                $dsAnterior = $anterior[0]['dsSituacao'];
                if ($anterior[0]['dsServico']) {
                    $dsInsumo = $anterior[0]['dsServico'];
                } else {
                    $dsInsumo = $anterior[0]['dsInsumo'];
                }
            }

            // situacao depois da alteracao
            $dsPosterior = null;
            if ($anterior[0]['idSituacao'] <> 5) {
                $dsPosterior = 'PEDIDO';
                $dsMensagem = 'STATUS DA SOLICITACAO DE COMPRAS DO ITEM ' . $dsInsumo . ' ALTERADO DE: ' . $dsAnterior . ' PARA: ' . $dsPosterior;
            }
            
            // ler usuario da solictiacao para enviar msg
            $dadossci = $modelsc->getSolicitacaoComprasItens('a.idSolicitacaoItem = ' . $idSolicitacaoItem);
            if ($dadossci) {
                if ($dadossci[0]['idUsuarioSolicitante'] <> $_SESSION['user']['usuario']) {            
                    $dadosim = array(
                        'idUsuarioOrigem' => 999,
                        'idUsuarioDestino' => $dadossci[0]['idUsuarioSolicitante'],
                        'dsNomeTabela' => 'prodSolicitacaoCompras',
                        'idOrigemInformacao' => 99,
                        'idTabela' => $idSolicitacaoItem,
                        'idTipoMensagem' => 0,
                        'stSituacao' => 0,
                        'dtEnvio' => date('Y-m-d H:i:s'),
                        'dsMensagem' => $dsMensagem
                    );
                    $this->criarMensagem($dadosim);
                }
            }        

            // atualizar a solicitacao com o novo status
            
            $dadospedido = array(
                'idPedido' => $this->idPedido,
                'idPedidoItem' => $id_item,
                'idSituacao' => 5
            );
            $where = 'idSolicitacaoItem = ' . $idSolicitacaoItem;
            $modelsc->updSolicitacaoComprasItem($dadospedido, $where);

            $dadospedido = array(
                'idSituacao' => 5
            );
            $where = 'idSolicitacao = ' . $dados[0]['idSolicitacao'];
            $modelsc->updSolicitacaoCompras($dadospedido, $where);  
        }
    }
    
//    public function gravar_pedido_from_solicitacao($dados) {
//        
//        $model = new pedidoModel();
//        $data = $this->trataPostSolic($dados);
//        $id = $model->setPedido($data);
//        
//        return $id;
//    }    
//    
//    public function gravar_pedido_item_from_solicitacao($dados) {
//        $model = new pedidoModel();
//        $data = $this->trataPostItemSoli($dados);
//        $id = $model->setPedidoItem($data);    
//        return $id;
//    }
//
    //Trata dados antes de Enviar para o Gravar
    private function trataPostSolic($post) {
        $data = array();
        $data['idPedido'] = ($post['idPedido'] != '') ? $post['idPedido'] : null;;
        $data['idParceiro'] = ($post['idParceiro'] != '') ? $post['idParceiro'] : null;
        $data['dsObservacao'] = ($post['dsObservacao'] != '') ? $post['dsObservacao'] : null;
        $data['dtPedido'] = ($post['dtPedido'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $_POST["dtPedido"]))) : date('Y-m-d h:m:s');
        $data['dtPrazoEntrega'] = ($post['dtPrazoEntrega'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $_POST["dtPrazoEntrega"]))) : date('Y-m-d h:m:s');
        $data['nrPedido'] = ($post['idPedido'] != '') ? $post['idPedido'] : null;
        $data['dsComplementoEntrega'] = ($post['dsComplementoEntrega'] != '') ? $post['dsComplementoEntrega'] : null;
        $data['dsSolicitante'] = ($post['dsSolicitante'] != '') ? $post['dsSolicitante'] : null;
        $data['idUsuarioSolicitante'] = ($post['idUsuarioSolicitante'] != '') ? $post['idUsuarioSolicitante'] : null;
        $data['idPrioridade'] = ($post['idPrioridade'] != '') ? $post['idPrioridade'] : null;
        $data['idUsuario'] = $_SESSION['user']['usuario'];
        return $data;
    }
    
    private function trataPostItemSoli($post) {
        $data = array();        
        $data['idInsumo'] = ($post['idInsumo'] != '') ? $post['idInsumo'] : null;
        $data['qtPedido'] = ($post['qtPedido'] != '') ? str_replace(",",".",str_replace(".","",$post['qtPedido'])) : null;
        $data['vlPedido'] = ($post['vlPedido'] != '') ? str_replace(",",".",str_replace(".","",$post['vlPedido'])) : null;
        $data['idPedido'] = ($post['idPedido'] != '') ? $post['idPedido'] : null;
        $data['idPedidoItem'] = ($post['idPedidoItem'] != '') ? $post['idPedidoItem'] : null;
        $data['idOS'] = ($post['idOS'] != '') ? $post['idOS'] : null;
        $data['idMotivo'] = ($post['idMotivo'] != '') ? $post['idMotivo'] : null;
        $data['idMaquina'] = ($post['idMaquina'] != '') ? $post['idMaquina'] : null;
        $data['idCentroCusto'] = ($post['idCentroCusto'] != '') ? $post['idCentroCusto'] : null;
        $data['idLocalEstoque'] = ($post['idLocalEstoque'] != '') ? $post['idLocalEstoque'] : null;
        $data['dsObservacao'] = ($post['dsObservacaoItem'] != '') ? $post['dsObservacaoItem'] : null;
        $data['stTipoIS'] = ($post['opcao'] != '') ? $post['opcao'] : null;
        return $data;
    }

    
    //Funcao de Inserir
    public function nova_solicitacao() {
        $sy = new system\System();
        if (!isset($_SESSION['solicitacaocompras']['id'])) {
           $idSolicitacao = $sy->getParam('idSolicitacao');
        } else {
          if ($_SESSION['solicitacaocompras']['id'] == 0) {
             $idSolicitacao = null;
          } else {
            $idSolicitacao = $_SESSION['solicitacaocompras']['id'];            
          }
        }
        
        $model = new solicitacaocomprasModel();
        if (isset($idSolicitacao)) {
            if ((bool) $idSolicitacao) {
                $registro = $model->getSolicitacaoCompras('a.idSolicitacao=' . $idSolicitacao);  
                if ($registro) {
                    $registro = $registro[0];
                } else {
                    //Novo Registro
                    $registro = $model->estrutura_vazia();
                    $registro = $registro[0];                    
                }
            } else {
                //Novo Registro
                $registro = $model->estrutura_vazia();
                $registro = $registro[0];                    
            }
        } else {
            //Novo Registro
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        
//        var_dump($registro); 
        $modelUsuario = new usuariosModel();
        $lista_usuario = array('' => 'SELECIONE');
        foreach ($modelUsuario->getUsuario() as $value) {
            $lista_usuario[$value['idUsuario']] = $value['dsUsuario'];
        }
        $modelUnidade = new unidadeModel();
        $lista_unidade = array('' => 'SELECIONE');
        foreach ($modelUnidade->getUnidade() as $value) {
            $lista_unidade[$value['idUnidade']] = $value['dsUnidade'];
        }
        
//        $modelInsumo = new insumoModel();
//        $lista_insumo = array('' => 'SELECIONE');
//        foreach ($modelInsumo->getInsumo() as $value) {
//            $lista_insumo[$value['idInsumo']] = $value['cdInsumo'] . '-' . $value['dsInsumo'];
//        }
//
        $modelSituacao = new situacaoModel();
        $lista_situacao = array('' => 'SELECIONE');
        foreach ($modelSituacao->getSituacao() as $value) {
            $lista_situacao[$value['idSituacao']] = $value['dsSituacao'];
        }
//        
//        $modelParceiro = new parceiroModel();
//        $lista_Parceiro = array('' => 'SELECIONE');
//        foreach ($modelParceiro->getParceiro() as $value) {
//            $lista_Parceiro[$value['idParceiro']] = $value['dsParceiro'];
//        }
//
        $modelCentroCusto = new centrocustoModel();
        $lista_centrocusto = array('' => 'SELECIONE');
        foreach ($modelCentroCusto->getCentroCustoCombo() as $value) {
            $lista_centrocusto[$value['idCentroCusto']] = $value['codigocusto'];
        }
        $modelMaquina = new maquinaModel();
        $lista_maquina = array('' => 'SELECIONE');
        foreach ($modelMaquina->getMaquina() as $value) {
            $lista_maquina[$value['idMaquina']] = $value['dsMaquina'];
        }
        $modelOS = new osModel();
        $lista_OS = array('' => 'SELECIONE');
        foreach ($modelOS->getOS() as $value) {
            $lista_OS[$value['idOS']] = $value['nrOS'];
        }
        $motivolOS = new motivoModel();
        $lista_Motivo = array('' => 'SELECIONE');
        foreach ($motivolOS->getMotivo() as $value) {
            $lista_Motivo[$value['idMotivo']] = $value['dsMotivo'];
        }

        $solicitacaocomprasitens = array();
        if($idSolicitacao) {
            $where = "a.idSolicitacao = " . $idSolicitacao;
            $solicitacaocomprasitens = $model->getSolicitacaoComprasItens($where);
        }        

        $pModel = new prioridadeModel();        
        $lista_Prioridade = array('' => 'SELECIONE');
        foreach ($pModel->getPrioridade() as $value) {
            $lista_Prioridade[$value['idPrioridade']] = $value['dsPrioridade'];
        }
        
      //  var_dump($solicitacaocomprasitens); die;
        
        $this->smarty->assign('lista_situacao', $lista_situacao); 
        $busca['idSituacao'] = $registro['idSituacao'];
        $this->smarty->assign('busca', $busca); 
        
        $this->smarty->assign('lista_prioridade', $lista_Prioridade);
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('lista_usuario', $lista_usuario);
        $this->smarty->assign('lista_unidades', $lista_unidade);
//        $this->smarty->assign('lista_insumo', $lista_insumo);
//        $this->smarty->assign('lista_Parceiro', $lista_Parceiro);
        $this->smarty->assign('lista_centrocusto', $lista_centrocusto);
        $this->smarty->assign('lista_maquina', $lista_maquina);
        $this->smarty->assign('lista_os', $lista_OS);
        $this->smarty->assign('lista_motivo', $lista_Motivo);
        
        $this->smarty->assign('solicitacaocomprasitens', $solicitacaocomprasitens);
        $this->smarty->assign('title', 'Novo Solicitacao de Compra');
        $this->smarty->display('solicitacaocompras/form_novo_manutencao.tpl');
    }
    // Gravar Padrao
    public function novosolicitacaocompras() {        
        
        $_SESSION['solicitacaocompras']['id'] = 0;
        $jsondata["idSolicitacao"] = null;
        $jsondata["ok"] = true;

        echo json_encode($jsondata);
    }
    
    
    public function ler_dados() {
        $idSolicitacaoItem = $_POST['idSolicitacaoItem'];
        $opcao = $_POST['opcao'];
        $model = new solicitacaocomprasModel();
        $dados = $model->getSolicitacaoComprasManutencaoItens('a.idSolicitacaoItem = ' . $idSolicitacaoItem);
 //       var_dump($dados); die;
        $retorno = $dados[0];
        echo json_encode($retorno);
    }

    public function lerusuario() {
        $idUsuario = $_POST['idUsuario'];
        $modelUsuario = new usuariosModel();
        $retorno = $modelUsuario->getUsuario('L.idUsuario = ' . $idUsuario);
        $retorno = array(
            'nomeusuario' => $retorno[0]['dsUsuario']
        );
        echo json_encode($retorno);
    }

    public function lerParceiro() {
        $idParceiro = $_POST['idParceiro'];
        $modelParceiro = new parceiroModel();
        $retorno = $modelParceiro->getParceiro('a.idParceiro = ' . $idParceiro);
        $retorno = array(
            'nomeParceiro' => $retorno[0]['dsParceiro']
        );
        echo json_encode($retorno);
    }

    public function lerunidade() {
        $idInsumo = $_POST['idInsumo'];
        $dsUnidade = null;
        $qtEstoque = null;
        $modelUnidade = new unidadeModel();
        $where = 'idInsumo = ' . $idInsumo;
        $retorno = $modelUnidade->getInsumoUnidade($where);
        if ($retorno) {
            $idUnidade = $retorno[0]['idUnidade'];
            $nome_produto = $retorno[0]['dsInsumo'];
        }
        $jsondata["idUnidade"] = $dsUnidade;
        $jsondata["dsProduto"] = $nome_produto;
        $jsondata["ok"] = true;
        echo json_encode($jsondata);
    }
        
    public function atualiza_unidades() {
        $modelUnidade = new unidadeModel();
        $lista_unidade = array('' => 'SELECIONE');
        foreach ($modelUnidade->getUnidade() as $value) {
            $lista_unidade[$value['idUnidade']] = $value['dsUnidade'];
        }
        $this->smarty->assign('lista_unidades', $lista_unidade);
        $html = $this->smarty->fetch('solicitacaocompras/lista_unidades.tpl');
        $retorno = array(
            'html' => $html
        );
        echo json_encode($retorno);
    }

//    public function atualiza_produtos() {
//        $modelInsumo = new insumoModel();
//        $lista_insumo = array('' => 'SELECIONE');
//        foreach ($modelInsumo->getInsumo() as $value) {
//            $lista_insumo[$value['idInsumo']] = $value['cdInsumo'] . '-' . $value['dsInsumo'];
//        }
//        $this->smarty->assign('lista_insumo', $lista_insumo);
//        $html = $this->smarty->fetch('pedido/lista_produtos.tpl');
//        $retorno = array(
//            'html' => $html
//        );
//        echo json_encode($retorno);
//    }
//    
//    public function atualiza_Parceiroes() {
//        $modelParceiro = new parceiroModel();
//        $lista_Parceiro = array('' => 'SELECIONE');
//        foreach ($modelParceiro->getParceiro() as $value) {
//            $lista_Parceiro[$value['idParceiro']] = $value['dsParceiro'];
//        }
//        $this->smarty->assign('lista_Parceiro', $lista_Parceiro);
//        $html = $this->smarty->fetch('solicitacaocompras/lista_Parceiroes.tpl');
//        $retorno = array(
//            'html' => $html
//        );
//        echo json_encode($retorno);
//    }
//    
    
    public function desabilitaid() {
        unset($_SESSION['solicitacaocompras']['id']);
        echo json_encode(true);
    }
    
    public function gravar_solicitacao() {
        
        $model = new solicitacaocomprasModel();
        $data = $this->trataPost($_POST);
        $id = $_POST['idSolicitacao'];

        $dsMensagem = 'ALTERACAO EM SUA SOLICITACAO DE COMPRAS';
        // situacao atual antes da alteracao
        $where = 'a.idSolicitacao = ' . $id;        
        $anterior = $model->getSolicitacaoCompras($where);
        $dsAnterior = null;
        if ($anterior) {
            $dsAnterior = $anterior[0]['dsSituacao'];
        }

        // situacao depois da alteracao
        $dsPosterior = null;
        if ($anterior[0]['idSituacao'] <> $data['idSituacao']) {
            $modelSituacao = new situacaoModel();
            $depois = $modelSituacao->getSituacao('idSituacao = ' . $data['idSituacao']);
            if ($depois) {
                $dsPosterior = $depois[0]['dsSituacao'];
                $dsMensagem = 'SOLICITACAO ALTERADA DE: ' . $dsAnterior . ' PARA: ' . $dsPosterior;
            }
        }
        // atualizar a solicitacao
        $where = 'idSolicitacao = ' . $id;
        $model->updSolicitacaoCompras($data, $where);            
            
        $dataI = array(
            'idSituacao' => $data['idSituacao']
        );
        $where = 'idSolicitacao = ' . $id;
        $model->updSolicitacaoComprasItemManut($dataI, $where);

        // ler usuario da solictiacao para enviar msg
        $dados = $model->getSolicitacaoCompras('a.idSolicitacao = ' . $id);
        if ($dados) {
            if ($dados[0]['idUsuarioSolicitante'] <> $_SESSION['user']['usuario']) {
                $dadosM = array(
                    'idUsuarioOrigem' => 999,
                    'idUsuarioDestino' => $dados[0]['idUsuarioSolicitante'],
                    'dsNomeTabela' => 'prodSolicitacaoCompras',
                    'idOrigemInformacao' => 99,
                    'idTabela' => $id,
                    'idTipoMensagem' => 0,
                    'stSituacao' => 0,
                    'dtEnvio' => date('Y-m-d H:i:s'),
                    'dsMensagem' => $dsMensagem
                );
                $this->criarMensagem($dadosM);
            }
        }        
        $_SESSION['solicitacaocompras']['id'] = $id;
        $jsondata["html"] = "solicitacaocomprasmanutencao/form_novo_manutencao.tpl";
        $jsondata["idSolicitacao"] = $id;
        $jsondata["ok"] = true;
        echo json_encode($jsondata);
    }

    public function gravar_item() {
        $model = new solicitacaocomprasModel();
        $data = $this->trataPostItem($_POST);
        $id = $data['idSolicitacaoItem'];
        
        $dsMensagem = 'ALTERACAO NO ITEM DA SUA SOLICITACAO DE COMPRAS';
        // situacao atual antes da alteracao
        $where = 'a.idSolicitacaoItem = ' . $id;        
        $anterior = $model->getSolicitacaoComprasItens($where);
        $dsAnterior = null;
        if ($anterior) {
            $dsAnterior = $anterior[0]['dsSituacao'];
            if ($anterior[0]['dsServico']) {
                $dsInsumo = $anterior[0]['dsServico'];
            } else {
                $dsInsumo = $anterior[0]['dsInsumo'];
            }
        }

        // situacao depois da alteracao
        $dsPosterior = null;
        if ($anterior[0]['idSituacao'] <> $data['idSituacao']) {
            $modelSituacao = new situacaoModel();
            $depois = $modelSituacao->getSituacao('idSituacao = ' . $data['idSituacao']);
            if ($depois) {
                $dsPosterior = $depois[0]['dsSituacao'];
                $dsMensagem = 'STATUS DA SOLICITACAO DE COMPRAS DO ITEM ' . $dsInsumo . ' ALTERADO DE: ' . $dsAnterior . ' PARA: ' . $dsPosterior;
            }
        }
        
        // atualizar os dados do item
        $where = 'idSolicitacaoItem = ' . $id;
        $model->updSolicitacaoComprasItemManut($data, $where);
        // atualizar os dados do cabecalho - somente o status que deve ser o mesmo
        $where = 'idSolicitacao = ' . $data['idSolicitacao'];
        $array = array('idSituacao' => $data['idSituacao']);        
        $model->updSolicitacaoCompras($array, $where);

        // ler usuario da solictiacao para enviar msg
        $dados = $model->getSolicitacaoComprasItens('a.idSolicitacaoItem = ' . $data['idSolicitacaoItem']);
        if ($dados) {
            if ($dados[0]['idUsuarioSolicitante'] <> $_SESSION['user']['usuario']) {            
                $dadosi = array(
                    'idUsuarioOrigem' => 999,
                    'idUsuarioDestino' => $dados[0]['idUsuarioSolicitante'],
                    'dsNomeTabela' => 'prodSolicitacaoCompras',
                    'idOrigemInformacao' => 99,
                    'idTabela' => $data['idSolicitacaoItem'],
                    'idTipoMensagem' => 0,
                    'stSituacao' => 0,
                    'dtEnvio' => date('Y-m-d H:i:s'),
                    'dsMensagem' => $dsMensagem
                );
                $this->criarMensagem($dadosi);
            }
        }        
        
        $jsondata["html"] = "solicitacaocomprasmanutencao/lista_itens1.tpl";
        $jsondata["idSolicitacaoItem"] = $data['idSolicitacaoItem'];
        $jsondata["ok"] = true;
        echo json_encode($jsondata);
    }
    
    //Trata dados antes de Enviar para o Gravar
    private function trataPost($post) {
        $data = array();
        $data['idSolicitacao'] = ($post['idSolicitacao'] != '') ? $post['idSolicitacao'] : null;;
        $data['idSituacao'] = ($post['idSituacao'] != '') ? $post['idSituacao'] : null;
        $data['dsObservacao'] = ($post['dsObservacao'] != '') ? $post['dsObservacao'] : null;
        $data['dsLocalEntrega'] = ($post['dsLocalEntrega'] != '') ? $post['dsLocalEntrega'] : null;
        
        return $data;
    }

    private function trataPostItem($post) {
        $data = array();
        $data['idSolicitacaoItem'] = ($post['idSolicitacaoItem'] != '') ? $post['idSolicitacaoItem'] : null;
        $data['dsObservacao'] = ($post['dsObservacaoItem'] != '') ? $post['dsObservacaoItem'] : null;
        $data['idSituacao'] = ($post['idSituacao'] != '') ? $post['idSituacao'] : null;
        $data['idParceiro'] = ($post['idParceiro'] != '') ? $post['idParceiro'] : null;
        $data['idInsumo'] = ($post['idInsumo'] != '') ? $post['idInsumo'] : null;
        $data['stTipoIS'] = ($post['opcao'] != '') ? $post['opcao'] : null;        
        $data['idSolicitacao'] = ($post['idSolicitacao'] != '') ? $post['idSolicitacao'] : null;;
        
        return $data;
    }

    // Remove Padrao
    public function delsolicitacaocomprasitem() {                
        $idSolicitacaoItem = $_POST['idSolicitacaoItem'];
        $model = new solicitacaocomprasModel();
        $where = 'idSolicitacaoItem = ' . $idSolicitacaoItem;             
        $model->delSolicitacaoComprasItem($where);
        $jsondata["ok"] = true;
        echo json_encode($jsondata);        
    }

    public function delfinanceiroitem() {                
        $idFinanceiroParcela = $_POST['idFinanceiroParcela'];
        $model = new solicitacaocomprasModel();
        $where = 'idFinanceiroParcela = ' . $idFinanceiroParcela;             
        $model->delFinanceiroItem($where);
        $jsondata["ok"] = true;
        echo json_encode($jsondata);        
    }

    public function delsolicitacaocompras() {   
        $sy = new system\System();        
        $idSolicitacao = $sy->getParam('idSolicitacao');
        $idSolicitacaoItem = $sy->getParam('idSolicitacaoItem');
        $model = new solicitacaocomprasModel();

        $dsMensagem = 'ALTERACAO NO ITEM DA SUA SOLICITACAO DE COMPRAS';
        // situacao atual antes da alteracao
        $where = 'a.idSolicitacaoItem = ' . $idSolicitacaoItem;        
        $anterior = $model->getSolicitacaoComprasItens($where);
        $dsAnterior = null;
        if ($anterior) {
            $dsAnterior = $anterior[0]['dsSituacao'];
            if ($anterior[0]['dsServico']) {
                $dsInsumo = $anterior[0]['dsServico'];
            } else {
                $dsInsumo = $anterior[0]['dsInsumo'];
            }
        }

        // situacao depois da alteracao
        $dsPosterior = null;
        if ($anterior[0]['idSituacao'] <> 9) {
            $dsPosterior = 'ARQUIVADO';
            $dsMensagem = 'STATUS DA SOLICITACAO DE COMPRAS DO ITEM ' . $dsInsumo . ' ALTERADO DE: ' . $dsAnterior . ' PARA: ' . $dsPosterior;
        }

        // ler usuario da solictiacao para enviar msg
        $dados = $model->getSolicitacaoComprasItens('a.idSolicitacaoItem = ' . $idSolicitacaoItem);
        if ($dados) {
            if ($dados[0]['idUsuarioSolicitante'] <> $_SESSION['user']['usuario']) {            
                $dadosi = array(
                    'idUsuarioOrigem' => 999,
                    'idUsuarioDestino' => $dados[0]['idUsuarioSolicitante'],
                    'dsNomeTabela' => 'prodSolicitacaoCompras',
                    'idOrigemInformacao' => 99,
                    'idTabela' => $idSolicitacaoItem,
                    'idTipoMensagem' => 0,
                    'stSituacao' => 0,
                    'dtEnvio' => date('Y-m-d H:i:s'),
                    'dsMensagem' => $dsMensagem
                );
                $this->criarMensagem($dadosi);
            }
        }        
        // atualizar a solicitacao
        $dados = array('idSituacao' => 9);
        $where = 'idSolicitacao = ' . $idSolicitacao . ' and idSolicitacaoItem = ' . $idSolicitacaoItem;             
        $model->updSolicitacaoComprasItem($dados, $where);
        // $model->delSolicitacao($where);
        header('Location: /solicitacaocomprasmanutencao');        
        return;
    }

    public function baixamanual() {   
        $sy = new system\System();
        
        $idSolicitacao = $sy->getParam('idSolicitacao');
        $model = new solicitacaocomprasModel();
        $where = 'idSolicitacao = ' . $idSolicitacao;             
        $dados = array('idSituacao' => 7, 'idUsuarioBaixa' => $_SESSION['user']['usuario'], 'dtBaixa' => date('Y-m-d h:m:s'), 'nrNota' => '', 'idOrigemInformacao' => 1);
        $model->updSolicitacao($dados, $where);
        header('Location: /solicitacaocomprasaberto');        
        return;
    }
    public function relatoriosolicitacaocompras_pre() {
        $this->template->run();

        $this->smarty->assign('title', 'Pre Relatorio de Solicitacaos');
        $this->smarty->display('solicitacaocompras/relatorio_pre.html');
    }

    public function relatoriosolicitacaocompras() {
        $this->template->run();

        $model = new solicitacaocomprasModel();
        $solicitacaocompras_lista = $model->getSolicitacao();
        //Passa a lista de registros
        $this->smarty->assign('solicitacaocompras_lista', $solicitacaocompras_lista);
        $this->smarty->assign('titulo_relatorio');
        //Chama o Smarty
        $this->smarty->assign('title', 'Relatorio de Solicitacoes');
        $this->smarty->display('solicitacaocompras/relatorio.html');
    }
    
    public function criarMensagem($array) {
        
        $modelMensagem = new mensagemModel();
        $dados = array(
          'idMensagem' => null,  
          'idOrigemInformacao' => $array['idOrigemInformacao'],
          'dsNomeTabela' => $array['dsNomeTabela'],
          'idTabela' => $array['idTabela']
        );
        $id = $modelMensagem->setMensagem($dados);
        $dados = array(
          'idMensagemAnterior' => $id,              
        );        
        $modelMensagem->updMensagem($dados, 'idMensagem = ' . $id);
        $dados = array(
            'idMensagemItem' => null,  
            'idMensagem' => $id,  
            'idUsuarioOrigem' => $array['idUsuarioOrigem'],
            'idUsuarioDestino' => $array['idUsuarioDestino'],
            'idTipoMensagem' => $array['idTipoMensagem'],
            'stSituacao' => $array['stSituacao'],
            'dtEnvio' => $array['dtEnvio'],
            'dsMensagem' => $array['dsMensagem']
        );
        $id = $modelMensagem->setMensagemItem($dados);
        return;
    }

}

?>