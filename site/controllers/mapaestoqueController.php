<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class mapaestoque extends controller {

    public function index_action() {
        //Inicializa o Template
        $this->template->run();
        $this->listar('local');        
    }
    
    public function trocarordem() {
        $url = explode("/", $_SERVER['REQUEST_URI']);
        $this->listar($url[4]);                
    }

    private function listar($ordem) {
        $modelLocalEstoque = new localestoqueModel();
        $model = new estoqueModel();
        
        if ($ordem=='produto') {
            $orderby = 'i.dsInsumo';
        } else {
            $orderby = 'le.cdLocalEstoque';
        }
        $registro = $model->getMapaEstoque($orderby,"le.stAnalitico = 'A'");
        $x = 0;
        foreach ($registro as $value) {
            
            $desanterior = $modelLocalEstoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
            if ($desanterior) {
                if ($value['idLocalEstoque'] == $value['idLocalEstoqueSuperior']) {
                    $value['localEstoque'] = $value['dsLocalEstoque'];
                } else {
                    $value['localEstoque'] = $desanterior[0]['dsLocalEstoque'] . '-' . $value['dsLocalEstoque'];
                }
            } else {
                $value['localEstoque'] = $value['dsLocalEstoque'];
            }
            
            $valores[substr($value['cdLocalEstoque'],0,1)][$x]['cdLocalEstoque'] = $value['cdLocalEstoque'];
            $valores[substr($value['cdLocalEstoque'],0,1)][$x]['dsLocalEstoque'] = $value['localEstoque'];
            $valores[substr($value['cdLocalEstoque'],0,1)][$x]['cdInsumo'] = $value['cdInsumo'];
            $valores[substr($value['cdLocalEstoque'],0,1)][$x]['idInsumo'] = $value['idInsumo'];
            $valores[substr($value['cdLocalEstoque'],0,1)][$x]['idLocalEstoque'] = $value['idLocalEstoque'];
            $valores[substr($value['cdLocalEstoque'],0,1)][$x]['idEstoque'] = $value['idEstoque'];
            $valores[substr($value['cdLocalEstoque'],0,1)][$x]['dsInsumo'] = $value['dsInsumo'];
            $valores[substr($value['cdLocalEstoque'],0,1)][$x]['qtdeEstoque'] = $value['qtdeEstoque'];
            $valores[substr($value['cdLocalEstoque'],0,1)][$x]['qtUltimaVenda'] = $value['qtUltimaVenda'];
            $valores[substr($value['cdLocalEstoque'],0,1)][$x]['nrNotaUltimaVenda'] = $value['nrNotaUltimaVenda'];
            $valores[substr($value['cdLocalEstoque'],0,1)][$x]['dtUltimaVenda'] = $value['dtUltimaVenda'];
            $valores[substr($value['cdLocalEstoque'],0,1)][$x]['dtUltimaCompra'] = $value['dtUltimaCompra'];
            $valores[substr($value['cdLocalEstoque'],0,1)][$x]['qtUltimaCompra'] = $value['qtUltimaCompra'];
            $valores[substr($value['cdLocalEstoque'],0,1)][$x]['nrNotaUltimaCompra'] = $value['nrNotaUltimaCompra'];
            $valores[substr($value['cdLocalEstoque'],0,1)][$x]['dsParceiroUltimaVenda'] = $value['dsParceiroUltimaVenda'];
            $valores[substr($value['cdLocalEstoque'],0,1)][$x]['dsParceiroUltimaCompra'] = $value['dsParceiroUltimaCompra'];
            $valores[substr($value['cdLocalEstoque'],0,1)][$x]['dsTipoMovimento'] = $value['dsTipoMovimento'];
            $valores[substr($value['cdLocalEstoque'],0,1)][$x]['dtMovimento'] = $value['dtMovimento'];
            $x++;
        }

        $colunas = $modelLocalEstoque->getLocalEstoqueMapa();
        $totcol = count($colunas);
        $x=0;
        foreach($colunas as $value) {
            $colunas[$x]['totalcol'] = $totcol;
            $LinhasVazias = $modelLocalEstoque->getLocalEstoqueMapaTotalLinhas("substr(le.cdLocalEstoque,1,1) = '" . $value['cdLocalEstoque'] . "' and e.qtEstoque > 0");
            if ($LinhasVazias) {
                $colunas[$x]['totallinhasCheias'] = $LinhasVazias[0]['totallinhasCheias'];
            } else {
                $colunas[$x]['totallinhasCheias'] = 0;
            }
            $x++;
        }
//        var_dump($valores); die;
        if ($ordem=='local') {        
            $this->smarty->assign('ordem', 'produto');
            $this->smarty->assign('nomedaordem', 'ORDEM DE PRODUTO');
        } else {
            $this->smarty->assign('ordem', 'local');
            $this->smarty->assign('nomedaordem', 'ORDEM DE LOCAL DE ESTOQUE');
        }
        $this->smarty->assign('title', 'Mapa do Almoxarifado');
        $this->smarty->assign('colunas', $colunas);
        $this->smarty->assign('mapaestoque', $valores);
        
        $this->smarty->display('estoque/mapaestoque.html');        
    }    
}

?>