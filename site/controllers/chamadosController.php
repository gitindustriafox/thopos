<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */
// use controllers\chamado\EnviarEmail;

class chamados extends controller {
    private $pastaPedidos = PEDIDOS;
    private $opcao = null;
    private $voltarmenu = null;
    public function index_action() {
        //Inicializa o Template
        $this->template->run();
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }

        $model = new chamadoModel();
        $sql = 'r.idChamado > 0 and r.stSituacao = 0 and r.idUsuarioSolicitante = ' . $_SESSION['user']['usuario'];
        
        $chamado_lista = $model->getChamado($sql);
        
        $this->smarty->assign('lista_tipo', $this->carregaTipoChamado());          
        $this->smarty->assign('lista_local', $this->carregaLocalChamado());   

        $listaS = array('' => 'SELECIONE', '0' => 'AGENDADA', '1' => 'ADIADA', '2' => 'CANCELADA', '3' => 'ARQUIVADA', '4' => 'REALIZADA');
        $this->smarty->assign('opcao', $this->opcao);            
        $this->smarty->assign('lista_status', $listaS);            
                
        $this->smarty->assign('chamado_lista', $chamado_lista);
        $this->smarty->display('chamado/lista.html');
    }
    
    public function recebeEmail() {

        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $url = explode("/", $_GET['url']);
        $idChamado = $url[4];        
        $idParticipante = $url[6];        
        $status = $url[8];            
        $model = new chamadoModel();        
        $dados = array('idChamado' => $idChamado, 'idParticipante' => $idParticipante, 'stRespostaEmail' => $status);        
        $model->updChamadoParticipantes($dados);        
    }

    private function finaliza() {
        header('Location: /login');            
        die;            
    }

    //Funcao de Inserir
    public function novo_chamado() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $model = new chamadosModel();

        $sy = new system\System();
        $this->opcao = $sy->getParam('opcao');
        $voltarMenu = false;
        $idChamado = $sy->getParam('idChamado');
        if ($idChamado == 999999) {
            $idChamado = null;
            $voltarMenu = true;
        }
        
        if ($idChamado > 0) {
            $registro = $model->getChamado('r.idChamado=' . $idChamado);
            $registro = $registro[0]; //Passando dados do chamado
            $lista = $model->getChamadoSequencia('r.idChamado = ' . $idChamado);
        } else {
            //Novo Registro
            $lista = null;
            $registro = null;
        }
        
        
        $this->smarty->assign('lista_setor', $this->carregaSetor());         
        $this->smarty->assign('lista_prioridade', $this->carregaPrioridade());         
        
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('title', 'Novo Chamado');
        $this->smarty->display('chamados/form_novo.tpl');
    }
    
    //Funcao de participantes

    public function acao() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $sy = new system\System();
        $this->opcao = $sy->getParam('opcao');
        $this->index_action();        
    }

    private function carregaSetor() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $model = new setorModel();
        $lista = array('' => 'SELECIONE');
        foreach ($model->getSetor('stRecebeTickets=1') as $value) {
            $lista[$value['idSetor']] = $value['dsSetor'];
        }
        return $lista;        
    }

    private function carregaPrioridade() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $lista = array('' => 'SELECIONE','0' => 'NORMAL','1' => 'URGENTE');
        return $lista;        
    }

    // Gravar Paao
    public function gravar_chamado() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $data = $this->trataPost($_POST);
        
        $model = new chamadosModel();

        if ($data['idChamado'] == NULL) {
          $id =  $model->setChamado($data);          
          $this->gravarSequencia($id);
        } else {
          $id = $data['idChamado'];
          $model->updChamado($data); //update
        }       
        
        $retorno = array('id' => $id);
        echo json_encode($retorno);
        
        return;
    }

    public function gravarSequencia($id) {
        $data = array('idChamado' => $id, 'idStatus' => 0, 'dtStatus' => date('Y-m-d H:i:s'), 'idUsuario' => $_SESSION['user']['usuario']);
        $model = new chamadosModel();
        $model->setChamadoSequencia($data);
    }

    public function envairemail() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
      
        $idParticipante = $_POST['idParticipante'];
        $idChamado = $_POST['idChamado'];
        $origem = $_POST['deOnde'];
        $retorno = $this->envair_mesmo_email($idChamado,$idParticipante, $origem);
        if ($retorno) {
            $ok = 'E-mail enviado com sucesso';
        } else {
            $ok = 'ERRO AO ENVIAR E-MAIL';
        }
        $jsondata = array(
            'ok' => $ok
        );
        echo json_encode($jsondata);   
    }
    
    private function envair_mesmo_email($idChamado, $idParticipante, $origem) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $model = new chamadoModel();  
        if ($idParticipante) {
            $retorno = $model->getParticipantes('P.idParticipante = ' . $idParticipante);
        } else {
            $retorno = $model->getParticipantes('P.idChamado = ' . $idChamado);
        }
        $participantes = $model->getParticipantes('P.idChamado = ' . $idChamado);

        if ($origem == 'Ata') {
            $titulo = 'REUNIAO REALIZADA';
        } else {
        if ($origem == 'Chamado') {
            $titulo = 'REUNIAO AGENDADA';
        } else {
        if ($origem == 'Cancelar') {
            $titulo = 'REUNIAO CANCELADA';
        } else {
            $titulo = 'REUNIAO ADIADA';
        }}}

        $caminhoAta = $retorno[0]['dsCaminhoPDF'];
        
        $to = array();
        $emails = array();
        if ($retorno) {
            $emails = $retorno;
        }
        $x=0;
        if ((bool)$emails) {
            foreach ($emails as $value) {
                $to[$x]['email'] = filter_var(strtolower($value['email']), FILTER_VALIDATE_EMAIL);
                $to[$x]['nome'] = $value['dsUsuario'];
                $x++;
            }
        }
        array_filter($to);        
//                print_a_die($retorno); die;
        
        if (!empty($to)) {
            $x=0;
            $this->smarty->assign('idChamado', $retorno[$x]['idChamado']);
            $this->smarty->assign('idParticipante', $retorno[$x]['idParticipante']);
            foreach ($to as $value) {
                $title = $titulo;
                $this->smarty->assign('title', $title);
                $this->smarty->assign('usuariodestino', $retorno[$x]['dsUsuario']);
                $this->smarty->assign('data_envio', date('Y-m-d H:i'));
                $this->smarty->assign('usuarioenvio', $retorno[$x]['UsuarioSolicitante']);
                $this->smarty->assign('tipochamado', $retorno[$x]['dsTipoChamado']);
                $this->smarty->assign('dtChamado', $retorno[$x]['dtChamado']);
                $this->smarty->assign('dsPauta', $retorno[$x]['dsPauta']);
                $this->smarty->assign('dsAssunto', $retorno[$x]['dsAssunto']);
                $this->smarty->assign('dsTempoDuracao', $retorno[$x]['dsTempoDuracao']);
                $this->smarty->assign('dsLocalChamado', $retorno[$x]['dsLocalChamado']);
                $this->smarty->assign('lista_itens',$participantes);
                if ($origem == 'Ata') {
                    $this->smarty->assign('dsAta', $retorno[$x]['dsAta']);
                    $this->smarty->assign('dsTempoDuracaoReal', $retorno[$x]['dsTempoDuracaoReal']);
                    $this->smarty->assign('dtRealizacao', $retorno[$x]['dtRealizacao']);
//                    $body = $this->smarty->fetch('chamado/email/enviar_email_ata.tpl');
//                } else {
//                    $body = $this->smarty->fetch('chamado/email/enviar_email.tpl');
                }
                $body = $this->smarty->fetch('chamado/email/enviar_email.tpl');

                $anexos = array();
                $modelD = new documentoModel();
                $where = "dsTabela = 'prodChamado' and idTabela = " . $idChamado;
                $retanex = $modelD->getDocumento($where);
                $y=0;
                foreach ($retanex as $valuea) {
                    $anexos[$y]['path_anexo'] = $valuea['dsLocalArquivo'];
                    $anexos[$y]['nome_anexo'] = $valuea['dsNome'];
                    $y++;
                }

                $modelD = new documentoModel();
                $where = "dsTabela = 'prodChamadoParticipantes' and idTabela = " . $retorno[$x]['idParticipante'];
                $retanex = $modelD->getDocumento($where);
                foreach ($retanex as $valuea) {
                    $anexos[$y]['path_anexo'] = $valuea['dsLocalArquivo'];
                    $anexos[$y]['nome_anexo'] = $valuea['dsNome'];
                    $y++;
                }

                // envia a ata como anexo
                if ($caminhoAta) {
                    $anexos[$y]['path_anexo'] = $caminhoAta;
                    $anexos[$y]['nome_anexo'] = 'ATA DA REUNIÃO';
                }
                
                $ok = $this->Send(array('0' => array(
                    'email' => $value['email'],
                    'nome' => $value['nome'])), $titulo, $body, array(), $anexos);
                $x++;
            }
            return $ok;
        } else {
            return false;
        }
    }

    private function Send($to_list, $subject, $body, $addbcc = array(), $attachment = array()) {    
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->SMTPDebug = 2;
        $mail->SMTPSecure = 'tls';            
        $mail->Port = 587;
        $mail->FromName = 'Thopos';
        $mail->IsHTML('text/html');    
        $mail->CharSet = 'UTF-8';
        
        $model = new chamadoModel('idDE = 1');
        $valores = $model->getValores();
        if ($valores) {
            $mail->Host = ($valores[0]['H']);
            $mail->Username = ($valores[0]['U']);
            $mail->Password = ($valores[0]['P']);
            $mail->From = ($valores[0]['F']);
        } else {
            return false;            
        }
        
        $mail->ClearAddresses();
        
        $concat_to = array();
        foreach ($to_list as $to) {
          if (empty($to['nome'])) {
            $to['nome'] = $to['email'];
          }
          $concat_to[] = "{$to['nome']} &lt;{$to['email']}&gt;"; 
        }
        foreach ($to_list as $to) {
            $mail->AddAddress($to["email"], $to["nome"]);
        }
          
        $mail->Subject = $subject;
        $mail->Body = $body;
        
        foreach ($attachment as $att) {     
            $mail->AddAttachment($att['path_anexo'], $att['nome_anexo']);
        }

        if ($mail->Send()) {
            return true;
        } else {
            return false;
        }
    }
    
    public function emitir_pdf() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $idChamado = ($_POST['idChamado'] != '') ? $_POST['idChamado'] : null;        
        $url = $this->enviarPDF($idChamado);
        $retorno = array();
        echo json_encode($retorno);        
    }

    public function enviarPDF($idChamado) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $idChamado = ($_POST['idChamado'] != '') ? $_POST['idChamado'] : null;
        global $PATH;
        $caminho = $GLOBALS['PATH'];
        if (!is_dir($this->pastaPedidos)) {
            mkdir($this->pastaPedidos, 0777, true);
        }
        $url = null;
        if (file_exists($this->pastaPedidos)) {
            
            $where = 'r.idChamado = ' . $idChamado;
            $model = new chamadoModel();
            $modelD = new documentoModel();
            $cabec = $model->getChamado($where)[0];
            $this->smarty->assign("chamado", $cabec);
            $participantes = $model->getParticipantes($where);
            $this->smarty->assign("participantes", $participantes);
            $tarefas = $model->getParticipantesTarefa($where);
//            var_dump($participantes);
//            var_dump($tarefas); die;
            $this->smarty->assign("tarefas", $tarefas);
            $anexos = $modelD->getDocumento("dsTabela = 'prodChamado' and idTabela = " . $idChamado);
            if ($anexos) {
                $this->smarty->assign("anexos", $anexos);
            } else {
                $this->smarty->assign("anexos", null);
            }

            // Filename
            $filename = "ATA_Chamado_" . $idChamado . "_" . $cabec['dtChamado'];
            // Dependecias
            require_once 'system/libs/mpdf/mpdf.php';
            $mpdf = new mPDF('c', 'A4', '5', 'Arial');

            $template = "chamado/pdf/PDFTemplate.tpl";

            $this->smarty->clearCache($template);
            $mpdf->WriteHTML($this->smarty->fetch($template));
            if (file_exists("{$this->pastaPedidos}{$filename}.pdf")) {
                unlink("{$this->pastaPedidos}{$filename}.pdf");
            }
            // Arquivo
            $mpdf->Output("{$this->pastaPedidos}{$filename}.pdf", 'F');

            if(file_exists("{$this->pastaPedidos}{$filename}.pdf")  ){
                $url = "{$this->pastaPedidos}{$filename}.pdf";
            }
            // ATUALIZR O CAMINHO DO PDF NO PEDIDO
            $data = array(
                'dsCaminhoPDF' => $url,
                'idChamado' => $idChamado
            );
            $model->updChamado($data);
        }
        
        return $url;
    }    
    
    public function excluiranexo() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $model = new documentoModel();        
        $idDocumento = $_POST['idDocumento'];
        $idChamado = $_POST['idChamado'];

        $ok = 'Anexo excluido';
        $where = array('idDocumento' => $idDocumento);
        $model->delDocumento($where);
        $lista = $model->getDocumento("idTabela  = " . $idChamado);
        $this->smarty->assign('fotos_lista', $lista);
        $html = $this->smarty->fetch("chamados/modalFotos/thumbnail1.tpl");        
        
        $jsondata = array(
            'html' => $html,
            'ok' => $ok
        );
        echo json_encode($jsondata);
    }

    //Trata dados antes de Enviar para o Gravar
    private function trataPost($post) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        $data['idChamado'] = ($post['idChamado'] != '') ? $post['idChamado'] : null;
        $data['dsChamado'] = ($post['dsChamado'] != '') ? $post['dsChamado'] : null;
        $data['dsObservacao'] = ($post['dsObservacao'] != '') ? $post['dsObservacao'] : null;
        $data['idPrioridade'] = ($post['idPrioridade'] != '') ? $post['idPrioridade'] : null;
        $data['dtLimite'] = ($post['dtLimite'] != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $_POST["dtLimite"]))) : date('Y-m-d h:m');
        $data['idSetorExecutor'] = ($post['idSetor'] != '') ? $post['idSetor'] : null;
        return $data;
    }

    // Remove Paao
    public function delchamado() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $sy = new system\System();                
        $idChamado = $sy->getParam('idChamado');        
        if (!is_null($idChamado)) {    
            $model = new chamadoModel();
            $dados['idChamado'] = $idChamado;             
            $dados['stSituacao'] = 3;             
            $model->updChamado($dados);
        }
        header('Location: /chamado');
    }
    
    public function arquivar() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $sy = new system\System();                
        $idChamado = $sy->getParam('idChamado');        
        if (!is_null($idChamado)) {    
            $model = new chamadoModel();
            $dados['idChamado'] = $idChamado;             
            $dados['stSituacao'] = 3;             
            $model->updChamado($dados);
        }
        header('Location: /chamado');
    }
    
    public function importarfotos() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        
        $idChamado = $_POST['idChamado'];
        
        $model = new documentoModel();        
        $lista = $model->getDocumento("dsTabela = 'prodChamados' and idTabela  = " . $idChamado);
        $this->smarty->assign('idChamado', $idChamado);
        $this->smarty->assign('dsTabela', 'prodChamados');
        $this->smarty->assign('fotos_lista', $lista);
        $this->smarty->display("chamados/modalFotos/thumbnail.tpl");        
    }

    public function importarfotosR() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $idChamado = $_POST['idChamado'];
        
        $model = new documentoModel();        
        $lista = $model->getDocumento("dsTabela = 'prodChamados' and idTabela  = " . $idChamado);
        $this->smarty->assign('idChamado', $idChamado);
        $this->smarty->assign('fotos_lista', $lista);
        $this->smarty->display("chamados/modalFotos/thumbnail.tpl");        
    }

    public function adicionarfoto() {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
        $idChamado = $_POST['idChamado'];
        $nome = $_POST['dsNome'];
        $extensao = explode('.',$_FILES['arquivo']['name'])[1];
        $novonome = $idChamado . '_' . date('Ymdis') . '.' . $extensao;
        
        $_UP['pasta'] = 'storage/tmp/documentos/';
        // Tamanho máximo do arquivo (em Bytes)
        $_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
        // Array com as extensões permitidas
        $_UP['extensoes'] = array('jpg','jpeg','png','bmp','pdf');
        // Renomeia o arquivo? (Se true, o arquivo será salvo como .jpg e um nome único)
        $_UP['renomeia'] = false;
        // Array com os tipos de erros de upload do PHP
        $_UP['erros'][0] = 'Não houve erro';
        $_UP['erros'][1] = 'O arquivo no upload é maior do que o limite do PHP';
        $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
        $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
        $_UP['erros'][4] = 'Não foi feito o upload do arquivo';
        // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
        if ($_FILES['arquivo']['error'] != 0) {
          die("Não foi possível fazer o upload, erro:" . $_UP['erros'][$_FILES['arquivo']['error']]);
          exit; // Para a execução do script
        }
        // Caso script chegue a esse ponto, não houve erro com o upload e o PHP pode continuar
        // Faz a verificação da extensão do arquivo
        // Faz a verificação do tamanho do arquivo
        if ($_UP['tamanho'] < $_FILES['arquivo']['size']) {
          echo "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
          exit;
        }

        // Depois verifica se é possível mover o arquivo para a pasta escolhida
        if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $_UP['pasta'] . $novonome)) {
            $model = new documentoModel();    
            $dados = array(
                'idDocumento' => null,
                'dsTabela' => 'prodChamados',
                'idTabela' => $idChamado,
                'dsNome' => $nome,
                'dsLocalArquivo' => $_UP['pasta'] . $novonome                
            );
            $model->setDocumento($dados);                        
        } else {
          // Não foi possível fazer o upload, provavelmente a pasta está incorreta
          echo "Não foi possível enviar o arquivo, tente novamente";
        }    
        header('Location: /chamados/novo_chamado/idChamado/' . $idChamado);                
    }    

    private function adicionarmensagem($idUsuario, $idParticipante, $idChamado, $qual, $tipo) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $mensagem = 'Reunião: ';
        $model = new chamadoModel();    
        if ($qual == 'TAREFA') {
            $dados = $model->getParticipantesTarefa('P.idTarefa = ' . $idParticipante);
        } else {
            $dados = $model->getParticipantes('P.idParticipante = ' . $idParticipante);
        }
        
        if ($dados) {
            if ($dados[0]['idUsuario'] <> $_SESSION['user']['usuario']) { 
                if ($qual == 'AGENDAR') {
                    $descricao = 'Reunião: ' . $dados[0]['dsTipoChamado'] . ' - Agendado por: ' . $dados[0]['UsuarioSolicitante'] . ' - Data ' . date("d/m/Y H:i", strtotime(str_replace("-", "/", $dados[0]['dtChamado']))) . ' - Pauta: ' . $dados[0]['dsPauta'] . ' - Seu Assunto: ' . $dados[0]['assuntoP'];
                } else {
                if ($qual == 'ADIAR') {
                    $descricao = 'ADIADA - Reunião: ' . $dados[0]['dsTipoChamado'] . ' - Agendado por: ' . $dados[0]['UsuarioSolicitante'] . ' - Data ' . date("d/m/Y H:i", strtotime(str_replace("-", "/", $dados[0]['dtChamado']))) . ' - Pauta: ' . $dados[0]['dsPauta'] . ' - Seu Assunto: ' . $dados[0]['assuntoP'];
                } else {
                if ($qual == 'TAREFA') {
                    $descricao = 'TAREFA - Reunião: ' . $dados[0]['dsTipoChamado'] . ' - Agendado por: ' . $dados[0]['UsuarioSolicitante'] . ' - Data ' . date("d/m/Y H:i", strtotime(str_replace("-", "/", $dados[0]['dtRealizacao']))) . ' - Pauta: ' . $dados[0]['dsPauta'] . ' - Sua Tarefa: ' . $dados[0]['tarefa'] . ' - Prazo ' . date("d/m/Y H:i", strtotime(str_replace("-", "/", $dados[0]['dtRealizacao'])));
                } else {
                    $descricao = 'CANCELADA - Reunião: ' . $dados[0]['dsTipoChamado'] . ' - Agendado por: ' . $dados[0]['UsuarioSolicitante'] . ' - Data ' . date("d/m/Y H:i", strtotime(str_replace("-", "/", $dados[0]['dtChamado']))) . ' - Pauta: ' . $dados[0]['dsPauta'] . ' - Seu Assunto: ' . $dados[0]['assuntoP'];
                }}}
                $dadosi = array(
                    'idUsuarioOrigem' => $_SESSION['user']['usuario'],
                    'idUsuarioDestino' => $dados[0]['idUsuario'],
                    'dsNomeTabela' => 'prodChamadoParticipantes',
                    'idOrigemInformacao' => 99,
                    'idTabela' => $idParticipante,
                    'idTipoMensagem' => $tipo,
                    'stSituacao' => 0,
                    'dtEnvio' => date('Y-m-d H:i:s'),
                    'dsMensagem' => $descricao,
                    'dsCaminhoArquivo' => null
                );
                $this->criarMensagem($dadosi);
            }
        }        
    }
    
    private function criarMensagem($array) {
        if (!isset($_SESSION['user']['usuario'])) {
            $this->finaliza();
        }
       
        $modelMensagem = new mensagemModel();
        $dados = array(
          'idMensagem' => null,  
          'idOrigemInformacao' => $array['idOrigemInformacao'],
          'dsNomeTabela' => $array['dsNomeTabela'],
          'idTabela' => $array['idTabela']
        );
        $id = $modelMensagem->setMensagem($dados);
        $dados = array(
          'idMensagemAnterior' => $id,              
        );        
        $modelMensagem->updMensagem($dados, 'idMensagem = ' . $id);
        $dados = array(
            'idMensagemItem' => null,  
            'idMensagem' => $id,  
            'idUsuarioOrigem' => $array['idUsuarioOrigem'],
            'idUsuarioDestino' => $array['idUsuarioDestino'],
            'idTipoMensagem' => $array['idTipoMensagem'],
            'stSituacao' => $array['stSituacao'],
            'dtEnvio' => $array['dtEnvio'],
            'dsMensagem' => $array['dsMensagem'],
            'dsCaminhoArquivo' => $array['dsCaminhoArquivo']
        );
        $id = $modelMensagem->setMensagemItem($dados);
    }
}
?>