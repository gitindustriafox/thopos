<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class listagemestoque extends controller {

    public function index_action() {
        //Inicializa o Template
        $this->template->run();
        
        $modelLocalEstoque = new localestoqueModel();
        $lista_localestoque = array('' => 'SELECIONE');
        foreach ($modelLocalEstoque->getLocalEstoque() as $value) {
            $desanterior = $modelLocalEstoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
            if ($desanterior) {
                if ($value['idLocalEstoque'] == $value['idLocalEstoqueSuperior']) {
                    $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
                } else {
                    $lista_localestoque[$value['idLocalEstoque']] = $desanterior[0]['dsLocalEstoque'] . '-' . $value['dsLocalEstoque'];
                }
            } else {
                $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
            }
        }

//        $model = new estoqueModel();
//        $resultado = $model->getEstoqueAtual(null,$paginacao=true);
//        $x=0;
//        foreach($resultado as $value) {
//            $desanterior = $modelLocalEstoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
//            if ($desanterior) {
//                if ($value['idLocalEstoque'] == $value['idLocalEstoqueSuperior']) {
//                    $resultado[$x]['localEstoque'] = $value['dsLocalEstoque'];
//                } else {
//                    $resultado[$x]['localEstoque'] = $desanterior[0]['dsLocalEstoque'] . '-' . $value['dsLocalEstoque'];
//                }
//            } else {
//                $resultado[$x]['localEstoque'] = $value['dsLocalEstoque'];
//            }
//            $x++;
//        }
        $this->smarty->assign('localestoque', $lista_localestoque);
        $this->smarty->assign('estoque', null);
        $this->smarty->assign('title', 'Estoque Atual');
        $this->smarty->display('estoque/listagemestoque.html');        
    }
    
    public function busca_estoque() {
        //se nao existir o indice estou como padrao '';
        $idLocalEstoque = isset($_POST['idLocalEstoque']) ? $_POST['idLocalEstoque'] : '';
        $dsProduto = isset($_POST['dsProduto']) ? $_POST['dsProduto'] : '';
        $cdInsumo = isset($_POST['cdInsumo']) ? $_POST['cdInsumo'] : '';
        $dsGrupo = isset($_POST['dsGrupo']) ? $_POST['dsGrupo'] : '';
        $model = new estoqueModel();
        $listaqtzeros = false;
        $listavlzeros = false;
        $cpagina = null;
        $busca = array();
        if (isset($_POST['qtZeros'])) { 
            $listaqtzeros = true;
        }
        if (isset($_POST['vlZeros'])) { 
            $listavlzeros = true;
        }
        if (isset($_POST['cpagina'])) { 
            $cpagina = true;
            $busca['cpagina'] = 1;
        }
        
        $sql = 'e.idEstoque > 0';
        if ($listaqtzeros) {
            $sql = $sql . ' and e.qtEstoque <= 0';
            $busca['qtZerosT'] = 1;
        }
        if ($listavlzeros) {
            $sql = $sql . ' and e.vlEstoque <= 0';
            $busca['vlZerosT'] = 1;
        }
        if ($idLocalEstoque) {
            $varios = $this->CriarArrayLocalEstoque($idLocalEstoque);            
            $sql = $sql . " and e.idLocalEstoque " . $varios;
            $busca['idLocalEstoque'] = $idLocalEstoque;
        }
        if ($dsProduto) {
            $sql = $sql . " and upper(i.dsInsumo) like upper('%" . $dsProduto . "%')";
            $busca['dsProduto'] = $dsProduto;
        }
        if ($cdInsumo) {
            $sql = $sql . " and upper(i.cdInsumo) like upper('%" . $cdInsumo . "%')";
            $busca['cdInsumo'] = $cdInsumo;
        }
        if ($dsGrupo) {
            $sql = $sql . " and upper(g.dsGrupo) like upper('%" . $dsGrupo . "%')";
            $busca['dsGrupo'] = $dsGrupo;
        }
        $modelLocalEstoque = new localestoqueModel();
        $lista_localestoque = array('' => 'SELECIONE');
        foreach ($modelLocalEstoque->getLocalEstoque() as $value) {
            $desanterior = $modelLocalEstoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
            if ($desanterior) {
                if ($value['idLocalEstoque'] == $value['idLocalEstoqueSuperior']) {
                    $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
                } else {
                    $lista_localestoque[$value['idLocalEstoque']] = $desanterior[0]['dsLocalEstoque'] . '-' . $value['dsLocalEstoque'];
                }
            } else {
                $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
            }
        }
        
        $resultado = $model->getEstoqueAtual($sql, $cpagina);
        $_SESSION['buscar_estoque']['sql'] = $sql;
        $x=0;
        foreach($resultado as $value) {
            $desanterior = $modelLocalEstoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
            if ($desanterior) {
                if ($value['idLocalEstoque'] == $value['idLocalEstoqueSuperior']) {
                    $resultado[$x]['localEstoque'] = $value['dsLocalEstoque'];
                } else {
                    $resultado[$x]['localEstoque'] = $desanterior[0]['dsLocalEstoque'] . '-' . $value['dsLocalEstoque'];
                }
            } else {
                $resultado[$x]['localEstoque'] = $value['dsLocalEstoque'];
            }
            $x++;
        }

        if (sizeof($resultado) > 0) {
            $this->smarty->assign('localestoque', $lista_localestoque);
            $this->smarty->assign('estoque', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'Estoque');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('estoque/listagemestoque.html');
        } else {
            $this->smarty->assign('localestoque', $lista_localestoque);
            $this->smarty->assign('estoque', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'Estoque');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('estoque/listagemestoque.html');
        }
    }
    
    private function CriarArrayLocalEstoque($idLocalEstoque) {
        $model = new localestoqueModel();
        
        $locaisestoque[0] = array();
        $retorno = $model->getLocalEstoque("idLocalEstoqueSuperior = " . $idLocalEstoque);
        if ($retorno) {
            $locaisestoque[0] = $retorno[0]['idLocalEstoque'];
            if ($retorno) {
                $sql = 'in (';
                foreach ($retorno as $value) {
                    $sql = $sql . $value['idLocalEstoque'] . ',';                
                }
            }

            $x=0;
            foreach ($retorno as $value) {
                if ($x>0) {
                    $retorno[$x] = $model->getLocalEstoque("idLocalEstoqueSuperior = " . $value['idLocalEstoque']);            
                    foreach($retorno[$x] as $value1) {
                        $sql = $sql . $value1['idLocalEstoque'] . ',';                
                    }
                }
                $x++;
            }
        } else {
            $sql = " = null";
        }    
        
        $sql = substr($sql, 0, strlen($sql)-1) . ")";
        return $sql;
    }

    public function busca_criarcsv() {
        $sql = $_SESSION['buscar_estoque']['sql'];
        $model = new estoqueModel();
        $modelLocalEstoque = new localestoqueModel();
        
        $resultado = $model->getEstoqueAtual($sql, false);
        $x=0;
        foreach($resultado as $value) {
            $desanterior = $modelLocalEstoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
            if ($desanterior) {
                if ($value['idLocalEstoque'] == $value['idLocalEstoqueSuperior']) {
                    $resultado[$x]['localEstoque'] = $value['dsLocalEstoque'];
                } else {
                    $resultado[$x]['localEstoque'] = $desanterior[0]['dsLocalEstoque'] . '-' . $value['dsLocalEstoque'];
                }
            } else {
                $resultado[$x]['localEstoque'] = $value['dsLocalEstoque'];
            }
            $x++;
        }
        
        if ($resultado) {
            $this->criarCSV($resultado);
        }
     //   header('Location: /solicitacaocomprasmanutencao');
    }
    
    public function criarCSV($resultado) {
        
        $limit = 30000;
        $offset = 0;

        global $PATH;
        $caminho = "/var/www/html/thopos.com.br/site/storage/tmp/csv/";

        // Storage
        if (!is_dir($caminho)) {
          mkdir($caminho, 0777, true);
        }

        $filename = "estoqueprodutos_" . date("YmsHis") . ".csv";

        $headers[] = implode(";", array(
          "\"ID\"",
          "\"CODIGO\"",
          "\"NOME DO PRODUTO\"",
          "\"GRUPO\"",
          "\"LOCAL ESTOQUE\"",
          "\"QUANTIDADE\"",
          "\"UNIDADE\"",
          "\"P.M.E\""
        ));

        if (file_exists("{$caminho}" . '/' . "{$filename}")) {
          unlink("{$caminho}" . '/' . "{$filename}");
        }

        // Arquivo
        $handle = fopen("{$caminho}" . '/' . "{$filename}", 'w+');
        fwrite($handle, implode(";" . PHP_EOL, $headers));
        fwrite($handle, ";" . PHP_EOL);
        fflush($handle);

        // Fecha o arquivo da $_SESSION para liberar o servidor para servir outras requisições
        session_write_close();

        $output = array();
        foreach ($resultado as $value) {
            $output[] = implode(";", array(
              "\"{$value["idInsumo"]}\"",
              "\"{$value["cdInsumo"]}\"",
              "\"{$value["dsInsumo"]}\"",
              "\"{$value["dsGrupo"]}\"",
              "\"{$value["localEstoque"]}\"",
              "\"{$value["qtdeEstoque"]}\"",
              "\"{$value["dsUnidade"]}\"",
              "\"{$value["vlPME"]}\"",
              "\"{$value["vlEstoque"]}\"",
            ));
        }
        fwrite($handle, implode(";" . PHP_EOL, $output));
        fwrite($handle, ";" . PHP_EOL);
        fflush($handle);
        fclose($handle);
        $this->download($caminho . '/' . $filename, 'CSV', $filename);        
    }

    private function download($nome, $tipo, $filename) {
      if (!empty($nome)) {
        if (file_exists($nome)) {
          header('Content-Transfer-Encoding: binary'); // For Gecko browsers mainly
          header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($nome)) . ' GMT');
          header('Accept-Ranges: bytes'); // For download resume
          header('Content-Length: ' . filesize($nome)); // File size
          header('Content-Encoding: none');
          header("Content-Type: application/{$tipo}"); // Change this mime type if the file is not PDF
          header('Content-Disposition: attachment; filename=' . $filename);
          // Make the browser display the Save As dialog
          readfile($nome);
          unlink($nome);
        }
      }
    }        

}

?>