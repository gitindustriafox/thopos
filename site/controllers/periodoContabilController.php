<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class periodoContabil extends controller {

    public function index_action() {
//die("chegou");
        //Inicializa o Template
        $this->template->run();

        $model = new periodoContabilModel();
        $periodo_lista = $model->getPeriodo(null,$paginacao=true);

        $this->smarty->assign('periodo_lista', $periodo_lista);
        $this->smarty->display('contabilidade/listaPeriodos.html');
    }

//Funcao de Busca
    public function busca_periodo() {
        //se nao existir o indice estou como padrao '';
        $texto = isset($_POST['buscadescricao']) ? $_POST['buscadescricao'] : '';
        //$texto = '';
        $model = new periodoContabilModel();
        $sql = "upper(dsPeriodo) like upper('%" . $texto . "%')"; //somente os nao excluidos
        $resultado = $model->getPeriodo($sql, $paginacao=true);

        if (sizeof($resultado) > 0) {
            $this->smarty->assign('periodoContabil_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'periodo');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('contabilidade/listaPeriodos.html');
        } else {
            $this->smarty->assign('periodoContabil_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'periodo');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('contabilidade/listaPeriodos.html');
        }
    }

    //Funcao de Inserir
    public function novo_periodo() {
        $sy = new system\System();
        $idPeriodo = $sy->getParam('idPeriodo');

        $model = new periodoContabilModel();

        if ($idPeriodo > 0) {
            $registro = $model->getPeriodo('idPeriodo=' . $idPeriodo);
            $registro = $registro[0]; //Passando Periodo
        } else {
            //Novo Registro
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('title', 'Novo Periodo');
        $this->smarty->display('contabilidade/novoPeriodo.tpl');
    }

    // Gravar Padrao
    public function gravar_periodo() {
        $model = new periodoContabilModel();

        $data = $this->trataPost($_POST);

        if ($data['idPeriodo'] == null) {
            $id=$model->setPeriodo($data);
        } else {
            $id=$data['idPeriodo'];
            $model->updPeriodo($data); //update
        }    
        
        header('Location: /periodoContabil/novo_periodo/idPeriodo/' . $id);
    }
    private function trataPost($post) {
        $data['idPeriodo'] = ($post['idPeriodo'] != '') ? $post['idPeriodo'] : null;
        $data['dsPeriodo'] = ($post['dsPeriodo'] != '') ? $post['dsPeriodo'] : null;
        $data['dtCriacao'] = ($post['dtCriacao'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $post["dtCriacao"]))) : date('Y-m-d h:m:s');
        $data['dtInicial'] = ($post['dtInicial'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $post["dtInicial"]))) : date('Y-m-d h:m:s');
        $data['dtFinal'] = ($post['dtFinal'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $post["dtFinal"]))) : date('Y-m-d h:m:s');
        $data['stSituacao'] = ($post['stSituacao'] != '') ? $post['stSituacao'] : 'A';
        return $data;
    }
    // Remove Padrao
    public function delperiodo() {
        $sy = new system\System();

        // colocar aqui a verificacao se tem contas contabeis cadastradas neste periodo, se sim nao excluir
        
        
        
        $idPeriodo = $sy->getParam('idPeriodo');        
        if (!is_null($idPeriodo)) {    
            $model = new periodoContabilModel();
            $dados['idPeriodo'] = $idPeriodo;             
            $model->delPeriodo($dados);
        }
        header('Location: /periodoContabil');
    }

}

?>