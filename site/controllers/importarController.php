<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class importar extends controller {
    private $xle = 0;
    private $xun = 0;
    private $xgr = 0;
    private $x = 0;

    public function index_action() {
        $this->template->run();
        $this->smarty->display('importar/lista.html');
    }

    public function importar_produtos() {
        $this->lerarquivo_produtos();
    }

    private function lerarquivo_produtos() {

        global $PATH;
        $arquivo = 'storage/tmp/arquivosparaimportar/epi.csv';

        $delimitador = ',';
        $cerca = '"';
        $f = fopen($arquivo, 'r');
        while (($data = fgetcsv($f, 0, $delimitador, $cerca)) !== FALSE) {
            $dados = array();
            $dados['cdInsumo'] = $data[0];
            $dados['dsInsumo'] = $data[1];
            $dados['dsGrupo'] = $data[2];
            $dados['dsUnidade'] = $data[3];
            $dados['dsLocalEstoque'] = $data[4];
            $dados['qtEstoque'] = $data[5];
            $dados['qtEstoqueMinimo'] = $data[6];
            $dados['qtEstoqueMaximo'] = $data[7];
            $dados['vlEstoque'] = 0;
            $dados['vlUltimaCompra'] = $data[8];
            $dados['dtValidade'] = $data[9];
            $this->lerdados_csv_produtos($dados);  
        }     

        fclose($f);
    }

    private function lerdados_csv_produtos($dados) {
        
//        var_dump($dados); die;
        if ($dados['dsLocalEstoque'] <> '-') {
            // ler o local de estoque
            $model = new localestoqueModel();
            $where = "dsLocalEstoque = '" . strtoupper($dados['dsLocalEstoque']) . "'";
            $ret = $model->getLocalEstoque($where);
            if ($ret) {
                $idLocalEstoque = $ret[0]['idLocalEstoque'];
            } else {
//                // inserir o local de estoque porque não achou
//                $this->xle = $this->xle + 1;
                $dadosparainserir = array(
                  'idLocalEstoque' => null,
                  'dsLocalEstoque' =>  strtoupper($dados['dsLocalEstoque'])
                );
//                $idLocalEstoque = $this->xle;
                $idLocalEstoque = $model->setLocalEstoque($dadosparainserir);
            }
        } else {
            $idLocalEstoque = null;
        }
        
        // ler a unidade
        
        if ($dados['dsUnidade']) {
            $model = new unidadeModel();
            $where = "dsUnidade = '" . strtoupper($dados['dsUnidade']) . "'";
            $ret = $model->getUnidade($where);
            if ($ret) {
                $idUnidade = $ret[0]['idUnidade'];
            } else {
                // inserir o local de estoque porque não achou
//                $this->xun = $this->xun + 1;                
                $dadosparainserir = array(
                  'idUnidade' => null,
                  'dsUnidade' =>  strtoupper($dados['dsUnidade'])
                );
//                $idUnidade = $this->xun;
                $idUnidade = $model->setUnidade($dadosparainserir);
            }
        } else {
            $idUnidade = null;
        }
        
        // ler o grupo
        
        if ($dados['dsGrupo']) {
            $model = new grupoModel();
            $where = "dsGrupo = '" . strtoupper($dados['dsGrupo']) . "'";
            $ret = $model->getGrupo($where);
            if ($ret) {
                $idGrupo = $ret[0]['idGrupo'];
            } else {
                // inserir o local de estoque porque não achou
//                $this->xgr = $this->xgr + 1;
                $dadosparainserir = array(
                  'idGrupo' => null,
                  'dsGrupo' =>  strtoupper($dados['dsGrupo'])
                );
//                $idGrupo = $this->xgr;
                $idGrupo = $model->setGrupo($dadosparainserir);
            }
        } else {
            $idGrupo = null;
        }
        
        // ler o Produto
        
        $model = new insumoModel();
        $where = 'a.dsInsumo = "' . strtoupper($dados['dsInsumo']) . '"';
        $ret = $model->getInsumo($where);
        if ($ret) {
            $idInsumo = $ret[0]['idInsumo'];
        } else {
            // inserir o PRODUTO porque não achou
//            $this->x = $this->x + 1;
            $dadosparainserir = array(
              'idInsumo' => null,
              'dsInsumo' =>  strtoupper($dados['dsInsumo']),
              'idUnidade' =>  $idUnidade,
              'idGrupo' =>  $idGrupo,
              'cdInsumo' =>  $dados['cdInsumo'],
              'dtValidade' =>  $dados['dtValidade'],
              'vlUnitario' =>  $dados['vlUltimaCompra'],
              'vlUltimaCompra' =>  $dados['vlUltimaCompra']
            );
//            $idInsumo = $this->x;
            $idInsumo = $model->setInsumo($dadosparainserir);
        }
        
        if ($dados['vlUltimaCompra'] > 0) {
            if ($dados['vlEstoque'] == 0) {
                if ($dados['qtEstoque']) {
                    $dados['vlEstoque'] = $dados['vlUltimaCompra'] * $dados['qtEstoque'];
                }    
            }
        }
        
        if ($dados['qtEstoque'] > 0 && $dados['vlEstoque'] > 0) {
            $pme = $dados['vlEstoque'] / $dados['qtEstoque'];
        } else {
            $pme = 0;
        }
        
        if ($idLocalEstoque) {
            $model = new estoqueModel();
            $dadosparainserir = array(
              'idEstoque' => null,
              'idInsumo' =>  $idInsumo,
              'idLocalEstoque' =>  $idLocalEstoque,
              'qtEstoque' =>  $dados['qtEstoque'],
              'vlEstoque' =>  $dados['vlEstoque'],
              'vlPME' =>  $pme,
              'qtEstoqueMinimo' =>  $dados['qtEstoqueMinimo'],
              'qtLoteReposicao' =>  $dados['qtEstoqueMaximo']
            );
            $model->setEstoque($dadosparainserir);        
        }
    }
    
}

?>