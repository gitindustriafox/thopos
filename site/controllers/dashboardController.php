<?php

class dashboard extends controller {

    public function index_action() {
        if (!isset($_SESSION['user']['usuario'])) {
            header('Location: /login'); 
            die;            
        }
        
//        $totalsc = 0;
//        $totalpa = 0;
//        $totalpp = 0;
//        $totale = 0;
////        var_dump($_SESSION['user']['perfil_compras']); die;
//        $where = "a.idSituacao = 1";
//        // total solicitacao compras em aberto
//        $model = new solicitacaocomprasModel();
//        if (isset($_SESSION['user']['perfil_compras'])) {
//            if ($_SESSION['user']['perfil_compras'] == 'N') {
//                $where = "a.idSituacao = 1 and a.idUsuarioSolicitante = {$_SESSION['user']['usuario']}";
//            }
//        }
//        $registro = $model->getSolicitacaoCompras($where);
//        if ($registro) {
//            $totalsc = count($registro);
//        }
//        $this->smarty->assign('totalsc', $totalsc);
//
//        // total pedidos em aberto
//
//        $where = "a.idSituacaoPedido < 3";
//        $model = new pedidoModel();
//        if (isset($_SESSION['user']['perfil_compras'])) {
//            if ($_SESSION['user']['perfil_compras'] == 'N') {
//                $where = "a.idSituacaoPedido < 3 and s.idUsuarioSolicitante = {$_SESSION['user']['usuario']}";
//            }
//        }
//        $registro = $model->getPedidoEmAberto($where);
//        if ($registro) {
//            $totalpa = count($registro);
//        }
//        $this->smarty->assign('totalpa', $totalpa);
//
//        // total de itens em ponto de pedido
//
//        $model = new estoqueModel();
//        $where = 'e.qtEstoque < e.qtEstoqueMinimo';
//        $registro = $model->getEstoqueAtual($where);
//        if ($registro) {
//            $totalpp = count($registro);
//        }
//        $this->smarty->assign('totalpp', $totalpp);
//
//        // total do valor do estoque
//        
//        $registro = $model->getValTotalEstoque();
//        if ($registro) {
//            $totale = $registro[0]['valortotal'];            
//        }
//        $this->smarty->assign('totale', $totale);
//
//        $this->smarty->assign('titulo1','SOLICITACOES DE COMPRAS EM ABERTO');        

        $this->smarty->assign("classea",'fa-tasks');
        $this->smarty->assign("classeb",'fa-comments');

        $this->lerSolicitacoes();
        
        if (!isset($_SESSION["user"]["tipousuario"])) {
            $_SESSION["user"]["tipousuario"] = null;
        }
        if (!isset($_SESSION["user"]["perfil"][0]["idPerfil"])) {
            $_SESSION["user"]["perfil"][0]["idPerfil"] = null;
        }
        
        $this->smarty->assign("id_idTipoUsuario", $_SESSION["user"]["tipousuario"]);
        $this->smarty->assign("idPerfil", $_SESSION["user"]["perfil"][0]["idPerfil"]);
        $this->smarty->assign("title", "Dashboard");
        $this->smarty->display("dashboard/dashboard.tpl");

    }

    public function lerSolicitacoes() {
        $idSolicitacao = null;
        $where = 'a.idSituacao < 4';
        $model = new solicitacaocomprasModel();
        if (isset($_SESSION['user']['perfil_compras'])) {
            if ($_SESSION['user']['perfil_compras'] == 'N') {
                $where = "a.idSituacao < 4 and a.idUsuarioSolicitante = {$_SESSION['user']['usuario']}";
            }
        }
        
        $registro = $model->getSolicitacaoCompras($where);
        $this->smarty->assign('solicitacaocompras', $registro);
        if ($registro) {
            $idSolicitacao = $registro[0]['idSolicitacao'];
        }
        if($idSolicitacao) {
            $registro = $model->getSolicitacaoComprasItens('a.idSolicitacao = ' . $idSolicitacao);
        }
        if ($registro) {
            $this->smarty->assign('solicitacaocomprasitens', $registro);            
        }
        $this->smarty->assign('titulo2','DETALHES DA SOLICITACAO - ' . $idSolicitacao);
        $this->smarty->assign("classea",'fa-tasks');
        $this->smarty->assign('titulo1','SOLICITACOES DE COMPRAS EM ABERTO');        
    }
        
    public function lerestoquegrupo() {
        $model = new estoqueModel();
        $LEmodel = new localestoqueModel();
        $registro = null;
        $nomegrupo = null;
        $idGrupo = null;
        $registro = $model->getValTotalEstoqueGrupo('e.vlEstoque <> 0');
        if ($registro) {
            $this->smarty->assign('estoquegrupo', $registro);  
            //var_dump($registro);
            $nomegrupo = $registro[0]['dsGrupo'];
            $idGrupo = $registro[0]['idGrupo'];
        }
        $this->smarty->assign("classea",'fa-tasks');
        $this->smarty->assign('titulo1','VALOR DO ESTOQUE POR GRUPO / LOCAL DE ESTOQUE');        
        $this->smarty->assign('estoquegrupoitens', null);            

        // primeira linha do detalhe
        
        $registro = null;
        $nomeGrupo = null;
        if ($idGrupo) {
            $model = new estoqueModel();
            $registro = $model->getValTotalEstoqueInsumo('i.idGrupo = ' . $idGrupo . ' and e.vlEstoque <> 0');
        }
        if ($registro) {
            $this->smarty->assign('movimentoitens', $registro); 
            $nomeGrupo = $registro[0]['dsGrupo'];
        }
        $this->smarty->assign('arquivodetalhes', 'dashboard/valorestoqueitens.html');
        $this->smarty->assign("classeb",'fa-comments');
        $this->smarty->assign('titulo2','VALOR POR PRODUTO NO GRUPO - ' . $nomeGrupo);

        // local de estoque
        
        $registro = $model->getValTotalEstoqueLocalEstoque('e.vlEstoque <> 0');
        if ($registro) {
            $x=0;
            foreach($registro as $value) {
                if ($registro[$x]['cdLocalEstoque']) {
                    $le = $LEmodel->getLocalEstoque("cdLocalEstoque = '" . $registro[$x]['cdLocalEstoque'] ."'");
//                    var_dump($value);
//                    var_dump($le);
                    if ($le) {
                        $registro[$x]['dsLocalEstoque'] = $le[0]['dsLocalEstoque'];
                    }
                }
                $x++;
            }
            // die;
            $this->smarty->assign('estoquelocalestoque', $registro);  
        }

        // mostrar
        
        $html = $this->smarty->fetch("dashboard/valorestoque.html");
        $html2 = $this->smarty->fetch("dashboard/valorestoqueitens.html");
        $retorno = array(
            'html' => $html,
            'html2' => $html2
        );
        echo json_encode($retorno);
        
    }
        
    public function lerestoquegrupoLE() {
        $cdLocalEstoque = $_POST['cdLocalEstoque'];
        $model = new estoqueModel();
        $LEmodel = new localestoqueModel();
        $registro = null;
        $nomegrupo = null;
        $idGrupo = null;
        $registro = $model->getValTotalEstoqueGrupoLE("e.vlEstoque <> 0 and substr(le.cdLocalEstoque,1,1) = '" . $cdLocalEstoque . "'");
        if ($registro) {
            $this->smarty->assign('estoquegrupo', $registro);  
            //var_dump($registro);
            $nomegrupo = $registro[0]['dsGrupo'];
            $idGrupo = $registro[0]['idGrupo'];
        }
        $this->smarty->assign("classea",'fa-tasks');
        $this->smarty->assign('titulo1','VALOR DO ESTOQUE POR GRUPO / LOCAL DE ESTOQUE');        
        $this->smarty->assign('estoquegrupoitens', null);            

        // primeira linha do detalhe
        
        $registro = null;
        $nomeGrupo = null;
        if ($idGrupo) {
            $model = new estoqueModel();
            $registro = $model->getValTotalEstoqueInsumo('i.idGrupo = ' . $idGrupo . ' and e.vlEstoque <> 0');
        }
        if ($registro) {
            $this->smarty->assign('movimentoitens', $registro); 
            $nomeGrupo = $registro[0]['dsGrupo'];
        }
        $this->smarty->assign('arquivodetalhes', 'dashboard/valorestoqueitens.html');
        $this->smarty->assign("classeb",'fa-comments');
        $this->smarty->assign('titulo2','VALOR POR PRODUTO NO GRUPO - ' . $nomeGrupo);

        // local de estoque
        
        $registro = $model->getValTotalEstoqueLocalEstoque('e.vlEstoque <> 0');
        if ($registro) {
            $x=0;
            foreach($registro as $value) {
                if ($value['cdLocalEstoque']) {
                    $le = $LEmodel->getLocalEstoque("cdLocalEstoque = '" . $value['cdLocalEstoque'] . "'");
                    if ($le) {
                        $registro[$x]['dsLocalEstoque'] = $le[0]['dsLocalEstoque'];
                    }
                }
                $x++;
            }
     //       var_dump($registro); die;
            $this->smarty->assign('estoquelocalestoque', $registro);  
        }
        // mostrar
        
        $html = $this->smarty->fetch("dashboard/valorestoque.html");
        $html2 = $this->smarty->fetch("dashboard/valorestoqueitens.html");
        $retorno = array(
            'html' => $html,
            'html2' => $html2
        );
        echo json_encode($retorno);
        
    }
        
    public function lerPedidos() {
        $idPedido = null;
        $model = new pedidoModel();
        $modelLocalEstoque = new localestoqueModel();

        if ($_SESSION['user']['perfil_compras'] === 'N') {
            $where = "a.idSituacaoPedido < 3 and s.idUsuarioSolicitante = {$_SESSION['user']['usuario']}";
        } else {
            $where = "a.idSituacaoPedido < 3";
        }
        
        $registro = $model->getPedidoEmAberto($where);
        $this->smarty->assign('arquivoprincipal', 'dashboard/listapedidos.html');
        $this->smarty->assign("classea",'fa-tasks');
        $this->smarty->assign('pedidos', $registro);
        $this->smarty->assign('titulo1','PEDIDOS EM ABERTO');
        if ($registro) {
            $idPedido = $registro[0]['idPedido'];
        }
        $registro = $model->getPedidoItens('a.idPedido = ' . $idPedido);
        $y = 0;
        foreach($registro as $value1) {
            if ($value1['idLocalEstoqueSuperior']) {
                $desanterior = $modelLocalEstoque->getLocalEstoque('idLocalEstoque = ' . $value1['idLocalEstoqueSuperior']);
                if ($desanterior) {
                    if ($value1['idLocalEstoque'] == $value1['idLocalEstoqueSuperior']) {
                        $registro[$y]['localEstoque'] = $value1['dsLocalEstoque'];
                    } else {
                        $registro[$y]['localEstoque'] = $desanterior[0]['dsLocalEstoque'] . '-' . $value1['dsLocalEstoque'];
                    }
                } else {
                    $registro[$y]['localEstoque'] = $value1['dsLocalEstoque'];
                }
            } else {
                $registro[$y]['localEstoque'] = null;
            }
            $y++;
        }
        
        if ($registro) {
            $this->smarty->assign('pedidoitens', $registro);            
        }
        $this->smarty->assign('arquivodetalhes', 'dashboard/listapedidositens.html');
        $this->smarty->assign("classeb",'fa-comments');
        $this->smarty->assign('titulo2','DETALHES DO PEDIDO - ' . $idPedido);
        
        $html = $this->smarty->fetch("dashboard/listapedidos.html");
        $html2 = $this->smarty->fetch("dashboard/listapedidositens.html");
        $retorno = array(
            'html' => $html,
            'html2' => $html2
        );
        echo json_encode($retorno);
    }
    
    public function lerpontopedido() {
        $model = new estoqueModel();
        $modelLocalEstoque = new localestoqueModel();
        
        $where = 'e.qtEstoque < e.qtEstoqueMinimo';
        $registro = $model->getEstoqueAtual($where);
        $y = 0;
        foreach($registro as $value1) {
            $desanterior = $modelLocalEstoque->getLocalEstoque('idLocalEstoque = ' . $value1['idLocalEstoqueSuperior']);
            if ($desanterior) {
                if ($value1['idLocalEstoque'] == $value1['idLocalEstoqueSuperior']) {
                    $registro[$y]['localEstoque'] = $value1['dsLocalEstoque'];
                } else {
                    $registro[$y]['localEstoque'] = $desanterior[0]['dsLocalEstoque'] . '-' . $value1['dsLocalEstoque'];
                }
            } else {
                $registro[$y]['localEstoque'] = $value1['dsLocalEstoque'];
            }
            $y++;
        }
        $this->smarty->assign('titulo1','PRODUTOS EM PONTO DE PEDIDO');        
        $this->smarty->assign("classea",'fa-tasks');
        $this->smarty->assign('pontopedido_lista', $registro);
        $this->smarty->assign('arquivodetalhes', 'dashboard/listappitens.html');
        $this->smarty->assign('pedidoitens', null);            
        $this->smarty->assign('titulo2',null);
        $this->smarty->assign("classeb",'fa-comments');
        $html = $this->smarty->fetch("dashboard/listapontopedido.html");
        $html2 = $this->smarty->fetch("dashboard/listappitens.html");
        $retorno = array(
            'html' => $html,
            'html2' => $html2
        );
        echo json_encode($retorno);
    }
    
    public function lersolicitacaodetalhes() {
        $idSolicitacao = $_POST['idSolicitacao'];
        $registro = null;
        $model = new solicitacaocomprasModel();
        $registro = $model->getSolicitacaoComprasItens('a.idSolicitacao = ' . $idSolicitacao);
        if ($registro) {
            $this->smarty->assign('solicitacaocomprasitens', $registro);            
        }

        $this->smarty->assign('arquivodetalhes', 'dashboard/listasolcomprasitens.html');
        $this->smarty->assign("classeb",'fa-comments');
        $this->smarty->assign('titulo2','DETALHES DA SOLICITACAO - ' . $idSolicitacao);

        $html = $this->smarty->fetch("dashboard/listasolcomprasitens.html");
        
        $retorno = array(
            'html' => $html
        );
        echo json_encode($retorno);
    }
    
    public function grafico() {
        $model = new estoqueModel();
        $LEmodel = new localestoqueModel();
        $registro = $model->getValTotalEstoqueLocalEstoque('e.vlEstoque <> 0');
        $retorno = array('');
        if ($registro) {
            $x=0;
            foreach($registro as $value) {
                $le = $LEmodel->getLocalEstoque("cdLocalEstoque = '" . $registro[$x]['cdLocalEstoque'] . "'");
                if ($le) {
                    $registro[$x]['dsLocalEstoque'] = $le[0]['dsLocalEstoque'];

                    $retorno['local'][$x][0] = $le[0]['dsLocalEstoque'];
                    $retorno['local'][$x][1] = doubleval($registro[$x]['valortotal']);
                }
                $x++;
            }
        }
        $registro = null;
        $registro = $model->getValTotalEstoqueGrupo('e.vlEstoque <> 0');
        if ($registro) {
            $x=0;
            foreach($registro as $value) {
                $retorno['grupo'][$x][0] = $value['dsGrupo'];
                $retorno['grupo'][$x][1] = doubleval($value['valortotal']);
                $x++;
            }
        }
        $InsumoSemEstoque = $model->getInsumosSemEstoque();
        if ($InsumoSemEstoque) {
            $isest = $InsumoSemEstoque[0]['totalSemEstoque'];
        }
        $retornogeral = array(
            'grupo' => $retorno['grupo'],
            'local' => $retorno['local'],            
            'qtInsumoSemEstoque' => $isest            
        );

        echo json_encode($retornogeral);
        
    }
    
    public function graficoInsumo() {
        $idInsumo = $_POST['idInsumo'];
        $model = new movimentoModel();
        
        // LER O VALOR MEDIO ANTES DOS 12 MESES E DAR CONTINUIDADE     
        
        $valorPMEAnterior = 0;
        $dados_finais=array();
        $dia= date('Y-m-d', strtotime('-365 days'));
        $dataInicial = substr($dia,0,8) . '01 00:00:00';
        $dataFinal = date("Y-m-t",strtotime($dia)) . " 23:59:59";

        $where = "m.dtMovimento >= '" . $dataInicial . "' and m.dtMovimento <= '" . $dataFinal . "' and mi.idInsumo = " . $idInsumo;
        $dados = $model->getMovimentoTotalInsumo($where);  
        if ($dados) {
           $valorPMEAnterior = $dados[0]['pme'];
        }   

        // LER A SOMA DAS ENTRADAS E SAÍDAS DOS ULTIMOS 12 MESES        
        
        $dados_finais=array();
        $dia= date('Y-m-d', strtotime('-365 days'));
        $dia= date('Y-m-d', strtotime('+30 days',strtotime($dia)));
        $dados_finais[0][0] = 'MES';
        $dados_finais[0][1] = 'ENTRADAS';
        $dados_finais[0][2] = 'SAIDAS';
        $dados_finais[0][3] = 'P.M.E';
        for ($x=1;$x<13;$x++) {
            
            $dataInicial = substr($dia,0,8) . '01 00:00:00';
            $dataFinal = date("Y-m-t",strtotime($dia)) . " 23:59:59";

            $where = "m.dtMovimento >= '" . $dataInicial . "' and m.dtMovimento <= '" . $dataFinal . "' and mi.idInsumo = " . $idInsumo;
            $dados = $model->getMovimentoTotalInsumo($where);  
            if (!$dados) {
                $dados_finais[$x][0] = substr($dataInicial,5,2) . '/' . substr($dataInicial,0,4);  //(string)$x;
                $dados_finais[$x][1] = 0;
                $dados_finais[$x][2] = 0;
                $dados_finais[$x][3] = doubleval($valorPMEAnterior);
            } else {
                $dados_finais[$x][0] =  substr($dataInicial,5,2) . '/' . substr($dataInicial,0,4); //(string)$x;
                $y = 1;
                $dados_finais[$x][1] = doubleval(0);
                $dados_finais[$x][2] = doubleval(0);
                foreach ($dados as $value) {
                     if ($value['nome'] == 'ENTRADA') {
                         $dados_finais[$x][1] = doubleval($value['valor']);
                     } else {
                         $dados_finais[$x][2] = doubleval($value['valor']);
                     }
                     $y++;
                }
                $dados_finais[$x][3] = doubleval($dados[0]['pme']);
                $valorPMEAnterior = doubleval($dados[0]['pme']);
            }
            $dia= date('Y-m-d', strtotime('+30 days',strtotime($dia)));
        }
        
        echo json_encode($dados_finais);
        
    }
    
    public function graficoPedido() {
        $model = new movimentoModel();
        
        // LER A SOMA DAS ENTRADAS E SAÍDAS DOS ULTIMOS 12 MESES        
        
        $dados_finais=array();
        $dia= date('Y-m-d', strtotime('-365 days'));
        $dia= date('Y-m-d', strtotime('+30 days',strtotime($dia)));
        $dados_finais[0][0] = 'MES';
        $dados_finais[0][1] = 'BAIXA';
        $dados_finais[0][2] = 'NORMAL';
        $dados_finais[0][3] = 'ALTA';
        $dados_finais[0][4] = 'URGENTE';
        for ($x=1;$x<13;$x++) {
            
            $dataInicial = substr($dia,0,8) . '01 00:00:00';
            $dataFinal = date("Y-m-t",strtotime($dia)) . " 23:59:59";

            $where = "not isnull(idPrioridade) and  dtPedido >= '" . $dataInicial . "' and dtPedido <= '" . $dataFinal . "'";
            $dados = $model->getMovimentoTotalPedido($where);  
            if (!$dados) {
                $dados_finais[$x][0] = substr($dataInicial,5,2) . '/' . substr($dataInicial,0,4);  //(string)$x;
                $dados_finais[$x][1] = 0;
                $dados_finais[$x][2] = 0;
                $dados_finais[$x][3] = 0;
                $dados_finais[$x][4] = 0;
            } else {
                $dados_finais[$x][0] =  substr($dataInicial,5,2) . '/' . substr($dataInicial,0,4); //(string)$x;
                $dados_finais[$x][1] = 0;
                $dados_finais[$x][2] = 0;
                $dados_finais[$x][3] = 0;
                $dados_finais[$x][4] = 0;
                foreach ($dados as $valor) {
                    $dados_finais[$x][$valor['idPrioridade']] = doubleval($valor['qttotal']);
                }
            }
            $dia= date('Y-m-d', strtotime('+30 days',strtotime($dia)));
        }
        echo json_encode($dados_finais);
    }
    public function graficoSolicitacao() {
        $model = new movimentoModel();
        
        // LER A SOMA DAS ENTRADAS E SAÍDAS DOS ULTIMOS 12 MESES        
        
        $dados_finais=array();
        $dia= date('Y-m-d', strtotime('-365 days'));
        $dia= date('Y-m-d', strtotime('+30 days',strtotime($dia)));
        $dados_finais[0][0] = 'MES';
        $dados_finais[0][1] = 'BAIXA';
        $dados_finais[0][2] = 'NORMAL';
        $dados_finais[0][3] = 'ALTA';
        $dados_finais[0][4] = 'URGENTE';
        for ($x=1;$x<13;$x++) {
            
            $dataInicial = substr($dia,0,8) . '01 00:00:00';
            $dataFinal = date("Y-m-t",strtotime($dia)) . " 23:59:59";

            $where = "not isnull(idPrioridade) and dtSolicitacao >= '" . $dataInicial . "' and dtSolicitacao <= '" . $dataFinal . "'";
            $dados = $model->getMovimentoTotalSolicitacao($where);  
            if (!$dados) {
                $dados_finais[$x][0] = substr($dataInicial,5,2) . '/' . substr($dataInicial,0,4);  //(string)$x;
                $dados_finais[$x][1] = 0;
                $dados_finais[$x][2] = 0;
                $dados_finais[$x][3] = 0;
                $dados_finais[$x][4] = 0;
            } else {
                $dados_finais[$x][0] =  substr($dataInicial,5,2) . '/' . substr($dataInicial,0,4); //(string)$x;
                $dados_finais[$x][1] = 0;
                $dados_finais[$x][2] = 0;
                $dados_finais[$x][3] = 0;
                $dados_finais[$x][4] = 0;
                foreach ($dados as $valor) {
                    $dados_finais[$x][$valor['idPrioridade']] = doubleval($valor['qttotal']);
                }
            }
            $dia= date('Y-m-d', strtotime('+30 days',strtotime($dia)));
        }

        // LER OS SOLICITANTES     
        
        $valorPMEAnterior = 0;
        $dados_finaisU=array();
        $dia= date('Y-m-d', strtotime('-365 days'));
        $dataInicial = substr($dia,0,8) . '01 00:00:00';
        $dataFinal = date("Y-m-d") . " 23:59:59";

        $where = "not isnull(sol.idPrioridade) and not isnull(sol.idUsuarioSolicitante) and sol.dtSolicitacao >= '" . $dataInicial . "' and sol.dtSolicitacao <= '" . $dataFinal . "'";
        $solicitantes = $model->getMovimentoTotalSolicitacaoS($where);  
        
        // LER A SOMA DAS ENTRADAS E SAÍDAS DOS ULTIMOS 12 MESES   - por solicitante      
        
        $dia= date('Y-m-d', strtotime('-365 days'));
        $dia= date('Y-m-d', strtotime('+30 days',strtotime($dia)));
        
        $y = 0;
        $dados_finaisU[0][0] = 'MES';
        foreach($solicitantes as $value) {
            // $dados_finaisU[0][$value['idUsuarioSolicitante']] = $value['dsUsuario'];
            $dados_finaisU[0][$y] = $value['dsUsuario'];
            $y++;
        }
        for ($x=1;$x<13;$x++) {
            
            $dataInicial = substr($dia,0,8) . '01 00:00:00';
            $dataFinal = date("Y-m-t",strtotime($dia)) . " 23:59:59";
            $y=0;
            foreach ($solicitantes as $value) {
                $where = "not isnull(sol.idPrioridade) and sol.idUsuarioSolicitante = {$value['idUsuarioSolicitante']} and sol.dtSolicitacao >= '" . $dataInicial . "' and sol.dtSolicitacao <= '" . $dataFinal . "'";
                $dados = $model->getMovimentoTotalSolicitacaoS($where);  
                if (!$dados) {
                    $dados_finaisU[$x][0] = substr($dataInicial,5,2) . '/' . substr($dataInicial,0,4);  //(string)$x;
                    $dados_finaisU[$x][$y] = 0;
                } else {
                    $dados_finaisU[$x][0] =  substr($dataInicial,5,2) . '/' . substr($dataInicial,0,4); //(string)$x;
                    $dados_finaisU[$x][$y] = doubleval($dados[0]['qttotal']);
                }
                $y++;
            }
            $dia= date('Y-m-d', strtotime('+30 days',strtotime($dia)));
        }
        
//        print_a($dados_finais);
//        print_a_die($dados_finaisU);
        
        $retornar = array(
            'prioridade' => $dados_finais,
            'solicitante' => $dados_finaisU
        );
        
        echo json_encode($retornar);
    }

    public function lerpedidodetalhes() {
        $idPedido = $_POST['idPedido'];
        $registro = null;
        $model = new pedidoModel();
        $registro = $model->getPedidoItens('a.idPedido = ' . $idPedido);
        if ($registro) {
            $this->smarty->assign('pedidoitens', $registro);            
        }
        $this->smarty->assign('arquivodetalhes', 'dashboard/listapedidositens.html');
        $this->smarty->assign("classeb",'fa-comments');
        $this->smarty->assign('titulo2','DETALHES DO PEDIDO - ' . $idPedido);
        
        $html = $this->smarty->fetch("dashboard/listapedidositens.html");
        
        $retorno = array(
            'html' => $html
        );
        echo json_encode($retorno);
    }
    
    public function lermovimentoitens() {
        $idInsumo = $_POST['idInsumo'];
        $modelLocalEstoque = new localestoqueModel();
        $registro = null;
        $nomeInsumo = null;
        $model = new movimentoModel();
        $registro = $model->getMovimentoItens('mi.idInsumo = ' . $idInsumo);
        $y=0;
        foreach($registro as $value1) {
            // local estoque origem
            $desanterior = $modelLocalEstoque->getLocalEstoque('idLocalEstoque = ' . $value1['localestoquesuperior']);
            if ($desanterior) {
                if ($value1['idLocalEstoque'] == $value1['localestoquesuperior']) {
                    $registro[$y]['localEstoque'] = $value1['dsLocalEstoque'];
                } else {
                    $registro[$y]['localEstoque'] = $desanterior[0]['dsLocalEstoque'] . '-' . $value1['dsLocalEstoque'];
                }
            } else {
                $registro[$y]['localEstoque'] = $value1['dsLocalEstoque'];
            }
            // local estoque destino
            if ($value1['localestoquesuperiordestino']) {
                $desanterior = $modelLocalEstoque->getLocalEstoque('idLocalEstoque = ' . $value1['localestoquesuperiordestino']);
                if ($desanterior) {
                    if ($value1['idLocalEstoque'] == $value1['localestoquesuperiordestino']) {
                        $registro[$y]['localEstoqueDestino'] = $value1['dsLocalEstoque'];
                    } else {
                        $registro[$y]['localEstoqueDestino'] = $desanterior[0]['dsLocalEstoque'] . '-' . $value1['dsLocalEstoqueDestino'];
                    }
                } else {
                    $registro[$y]['localEstoqueDestino'] = $value1['dsLocalEstoque'];
                }
            } else {
                $registro[$y]['localEstoqueDestino'] = '';
            }    
            $y++;
        }
        
        //var_dump($registro); die;
        if ($registro) {
            $this->smarty->assign('movimentoitens', $registro); 
            $nomeInsumo = $registro[0]['dsInsumo'];
        }
        $this->smarty->assign('arquivodetalhes', 'dashboard/listappitens.html');
        $this->smarty->assign("classeb",'fa-comments');
        $this->smarty->assign('titulo2','MOVIMENTACAO DO PRODUTO - ' . $nomeInsumo);
        
        $html = $this->smarty->fetch("dashboard/listappitens.html");
        
        $retorno = array(
            'html' => $html
        );
        echo json_encode($retorno);
    }
    
    public function lerestoquegrupoitens() {
        $idGrupo = $_POST['idGrupo'];
        $registro = null;
        $nomeGrupo = null;
        if ($idGrupo) {
            $model = new estoqueModel();
            $registro = $model->getValTotalEstoqueInsumo('e.vlEstoque <> 0 and i.idGrupo = ' . $idGrupo);
        }
        if ($registro) {
            $this->smarty->assign('movimentoitens', $registro); 
            $nomeGrupo = $registro[0]['dsGrupo'];
        }
        $this->smarty->assign('arquivodetalhes', 'dashboard/valorestoqueitens.html');
        $this->smarty->assign("classeb",'fa-comments');
        $this->smarty->assign('titulo2','VALOR POR PRODUTO NO GRUPO - ' . $nomeGrupo);
        
        $html = $this->smarty->fetch("dashboard/valorestoqueitens.html");
        
        $retorno = array(
            'html' => $html
        );
        echo json_encode($retorno);
    }
    
    public function lerestoquelocalestoqueitens() {
        $idInsumo = $_POST['idInsumo'];
        $registro = null;
        $nomeInsumo = null;
        $model = new movimentoModel();
        $registro = $model->getMovimento('mi.idInsumo = ' . $idInsumo);
        if ($registro) {
            $this->smarty->assign('movimentoitens', $registro); 
            $nomeInsumo = $registro[0]['dsInsumo'];
        }
        $this->smarty->assign('arquivodetalhes', 'dashboard/listappitens.html');
        $this->smarty->assign("classeb",'fa-comments');
        $this->smarty->assign('titulo2','MOVIMENTACAO DO PRODUTO - ' . $nomeInsumo);
        
        $html = $this->smarty->fetch("dashboard/listappitens.html");
        
        $retorno = array(
            'html' => $html
        );
        echo json_encode($retorno);
    }    
}
