<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class localreuniao extends controller {

    public function index_action() {
//die("chegou");
        //Inicializa o Template
        $this->template->run();

        $model = new localreuniaoModel();
        $localreuniao_lista = $model->getLocalReuniao();

        $this->smarty->assign('localreuniao_lista', $localreuniao_lista);
        $this->smarty->display('localreuniao/lista.html');
    }

//Funcao de Busca
    public function busca_localreuniao() {
        //se nao existir o indice estou como padrao '';
        $texto = isset($_POST['buscadescricao']) ? $_POST['buscadescricao'] : '';
        //$texto = '';
        $model = new localreuniaoModel();
        $sql = "upper(dsLocalReuniao) like upper('%" . $texto . "%')"; //somente os nao excluidos
        $resultado = $model->getLocalReuniao($sql);

        if (sizeof($resultado) > 0) {
            $this->smarty->assign('localreuniao_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'localreuniao');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('localreuniao/lista.html');
        } else {
            $this->smarty->assign('localreuniao_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'localreuniao');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('localreuniao/lista.html');
        }
    }

    //Funcao de Inserir
    public function novo_localreuniao() {
        $sy = new system\System();

        $idLocalReuniao = $sy->getParam('idLocalReuniao');

        $model = new localreuniaoModel();

        if ($idLocalReuniao > 0) {

            $registro = $model->getLocalReuniao('idLocalReuniao=' . $idLocalReuniao);
            $registro = $registro[0]; //Passando LocalReuniao
        } else {
            //Novo Registro
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        
        //Obter lista a de locals fk
        $objLista = new localreuniaoModel();
        //criar uma lista
        $lista_locals = $objLista->getLocalReuniao('idLocalReuniao <> 0');
        foreach ($lista_locals as $value) {
            $lista_locals_log[$value['idLocalReuniao']] = $value['dsLocalReuniao'];
        }
        //Passar a lista de Local
        $this->smarty->assign('lista_localreuniao', $lista_locals);
        //var_dump($lista_locals_log);die;
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('title', 'Novo Local de Movimento');
        $this->smarty->display('localreuniao/form_novo.tpl');
    }

    // Gravar Padrao
    public function gravar_localreuniao() {
        $model = new localreuniaoModel();

        $data = $this->trataPost($_POST);

        if ($data['idLocalReuniao'] == NULL)
            $model->setlocalreuniao($data);
        else
            $model->updlocalreuniao($data); //update
        
        header('Location: /localreuniao');        
        return;
    }

    //Trata dados antes de Enviar para o Gravar
    private function trataPost($post) {
        $data['idLocalReuniao'] = ($post['idLocalReuniao'] != '') ? $post['idLocalReuniao'] : null;
        $data['dsLocalReuniao'] = ($post['dsLocalReuniao'] != '') ? $post['dsLocalReuniao'] : null;
        return $data;
    }

    // Remove Padrao
    public function dellocalreuniao() {
        $sy = new system\System();
                
        $idLocalReuniao = $sy->getParam('idLocalReuniao');
        
        $localreuniao = $idLocalReuniao;
        
        if (!is_null($localreuniao)) {    
            $model = new localreuniaoModel();
            $dados['idLocalReuniao'] = $localreuniao;             
            $model->delLocalReuniao($dados);
        }

        header('Location: /localreuniao');
    }

    public function relatoriolocalreuniao_pre() {
        $this->template->run();

        $this->smarty->assign('title', 'Pre Relatorio de Local de Movimento');
        $this->smarty->display('localreuniao/relatorio_pre.html');
    }

    public function relatoriolocalreuniao() {
        $this->template->run();

        $model = new localreuniaoModel();
        $localreuniao_lista = $model->getLocalReuniao();
        //Passa a lista de registros
        $this->smarty->assign('localreuniao_lista', $localreuniao_lista);
        $this->smarty->assign('titulo_relatorio');
        //Chama o Smarty
        $this->smarty->assign('title', 'Relatorio de Local de Movimento');
        $this->smarty->display('localreuniao/relatorio.html');
    }

}

?>