<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class listakardex extends controller {

    public function index_action() {
        //Inicializa o Template
        $this->template->run();

        $modelLocalEstoque = new localestoqueModel();
        $lista_localestoque = array('' => 'SELECIONE');
        foreach ($modelLocalEstoque->getLocalEstoque() as $value) {
            $desanterior = $modelLocalEstoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
            if ($desanterior) {
                if ($value['idLocalEstoque'] == $value['idLocalEstoqueSuperior']) {
                    $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
                } else {
                    $lista_localestoque[$value['idLocalEstoque']] = $desanterior[0]['dsLocalEstoque'] . '-' . $value['dsLocalEstoque'];
                }
            } else {
                $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
            }
        }
        $modelCentroCusto = new centrocustoModel();
        $lista_centrocusto = array('' => 'SELECIONE');
        foreach ($modelCentroCusto->getCentroCustoCombo() as $value) {
                $lista_centrocusto[$value['idCentroCusto']] = $value['codigocusto'];
        }

        $sql =  "m.dtMovimento >= '" . date('Y-m-d') . "' and m.dtMovimento <= '" . date('Y-m-d') . "' and tm.dsTipoMovimento = 'Entrada de Sucata'";                
        $model = new movimentoModel();
        $registro = $model->getMovimento($sql);
        
        $x = 0;
        foreach ($registro as $value) {
            $where = 'mi.idMovimento = ' . $value['idMovimento'];
            $itens = $model->getMovimentoItens($where);
            $registro[$x]['itens'] = $itens;
            $x++;
        }
        
        $this->smarty->assign('localestoque', $lista_localestoque);
        $this->smarty->assign('centrocusto', $lista_centrocusto);
        $this->smarty->assign('movimento', $registro);
        $this->smarty->assign('title', 'Movimentacao');
        $this->smarty->display('movimento/lista_kardex.html');
        
    }
    
    public function busca_kardex() {
        //se nao existir o indice estou como padrao '';
        $dsParceiro = isset($_POST['dsParceiro']) ? $_POST['dsParceiro'] : '';
        $nrPlaca = isset($_POST['nrPlaca']) ? $_POST['nrPlaca'] : '';
        $nrNota = isset($_POST['nrNota']) ? $_POST['nrNota'] : '';
        $nrLacre = isset($_POST['nrLacre']) ? $_POST['nrLacre'] : '';
        $dsColaborador = isset($_POST['dsColaborador']) ? $_POST['dsColaborador'] : '';
        $cdProduto = isset($_POST['cdProduto']) ? $_POST['cdProduto'] : '';
        $dsProduto = isset($_POST['dsProduto']) ? $_POST['dsProduto'] : '';
        $dsReferencia = isset($_POST['dsReferencia']) ? $_POST['dsReferencia'] : '';
        $dtInicio = isset($_POST['dtInicio']) ? $_POST['dtInicio'] : '';
        $dtFim = isset($_POST['dtFim']) ? $_POST['dtFim'] : '';
        $idLocalEstoque = isset($_POST['idLocalEstoque']) ? $_POST['idLocalEstoque'] : '';
        $idCentroCusto = isset($_POST['idCentroCusto']) ? $_POST['idCentroCusto'] : '';

        $model = new movimentoModel();
        
        $busca = array();
        $sql = "m.idMovimento > 0 and tm.dsTipoMovimento = 'Entrada de Sucata'";
        if ($nrPlaca) {
            $sql = $sql . " and m.nrLacre = " . $nrPlaca;
            $busca['nrLacre'] = $nrPlaca;
        }
        if ($nrLacre) {
            $sql = $sql . " and m.nrNota = " . $nrLacre;
            $busca['nrNota'] = $nrLacre;
        }
        if ($nrNota) {
            $sql = $sql . " and m.nrNota = " . $nrNota;
            $busca['nrNota'] = $nrNota;
        }
        if ($dsParceiro) {
            $sql = $sql . " and ((upper(f.dsParceiro) like upper('%" . $dsParceiro . "%')) or (upper(c.dsParceiro) like upper('%" . $dsParceiro . "%')))";
            $busca['dsParceiro'] = $dsParceiro;
        }
        if ($dsColaborador) {
            $sql = $sql . " and upper(co.dsColaborador) like upper('%" . $dsColaborador . "%')";
            $busca['dsColaborador'] = $dsColaborador;
        }
        if ($cdProduto) {
            $sql = $sql . " and upper(i.cdInsumo) like upper('%" . $cdProduto . "%')";
            $busca['cdProduto'] = $cdProduto;
        }
        if ($dsProduto) {
            $sql = $sql . " and upper(i.dsInsumo) like upper('%" . $dsProduto . "%')";
            $busca['dsProduto'] = $dsProduto;
        }
        if ($dsReferencia) {
            $sql = $sql . " and upper(mi.dsObservacao) like upper('%" . $dsReferencia . "%')";
            $busca['dsReferencia'] = $dsReferencia;
        }
        if ($idLocalEstoque) {
            $varios = $this->CriarArrayLocalEstoque($idLocalEstoque);                        
            $sql = $sql . " and (mi.idLocalEstoque " . $varios . " or mi.idLocalEstoqueDestino " . $varios . ")";
            $busca['idLocalEstoque'] = $idLocalEstoque;
        }
        if ($idCentroCusto) {
            $varios = $this->CriarArrayCentroCusto($idCentroCusto);                        
            $sql = $sql . " and mi.idCentroCusto " . $varios;
            $busca['idCentroCusto'] = $idCentroCusto;
        }

        $dtInicio = ($dtInicio != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $dtInicio))) : date('Y-m-d h:m:s');
        $dtFim = ($dtFim != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $dtFim))) : date('Y-m-d h:m:s');
        
        $sql = $sql . " and m.dtMovimento >= '" . $dtInicio . "' and m.dtMovimento <= '" . $dtFim . "'";        
        $busca['dtInicio'] = $dtInicio;
        $busca['dtFim'] = $dtFim;
        
//        var_dump($sql); die;
        
        $resultado = $model->getMovimento($sql);

        $x = 0;
        foreach ($resultado as $value) {
            $where = 'mi.idMovimento = ' . $value['idMovimento'];
            $itens = $model->getMovimentoItens($where);
            $resultado[$x]['itens'] = $itens;
            $x++;
        }
        
        $modelLocalEstoque = new localestoqueModel();
        $lista_localestoque = array('' => 'SELECIONE');
        foreach ($modelLocalEstoque->getLocalEstoque() as $value) {
            $desanterior = $modelLocalEstoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
            if ($desanterior) {
                if ($value['idLocalEstoque'] == $value['idLocalEstoqueSuperior']) {
                    $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
                } else {
                    $lista_localestoque[$value['idLocalEstoque']] = $desanterior[0]['dsLocalEstoque'] . '-' . $value['dsLocalEstoque'];
                }
            } else {
                $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
            }
        }
        $modelCentroCusto = new centrocustoModel();
        $lista_centrocusto = array('' => 'SELECIONE');
        foreach ($modelCentroCusto->getCentroCustoCombo() as $value) {
                $lista_centrocusto[$value['idCentroCusto']] =  $value['codigocusto'];
        }
        
        $this->smarty->assign('localestoque', $lista_localestoque);
        $this->smarty->assign('centrocusto', $lista_centrocusto);

        if (sizeof($resultado) > 0) {
            $this->smarty->assign('movimento', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'Estoque');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('movimento/lista_kardex.html');
        } else {
            $this->smarty->assign('movimento', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'Estoque');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('movimento/lista_kardex.html');
        }
    }
    
    private function CriarArrayLocalEstoque($idLocalEstoque) {
        $model = new localestoqueModel();
        
        $locaisestoque[0] = array();
        $retorno = $model->getLocalEstoque("idLocalEstoqueSuperior = " . $idLocalEstoque);
        if ($retorno) {
            $locaisestoque[0] = $retorno[0]['idLocalEstoque'];
            if ($retorno) {
                $sql = 'in (';
                foreach ($retorno as $value) {
                    $sql = $sql . $value['idLocalEstoque'] . ',';                
                }
            }

            $x=0;
            foreach ($retorno as $value) {
                if ($x>0) {
                    $retorno[$x] = $model->getLocalEstoque("idLocalEstoqueSuperior = " . $value['idLocalEstoque']);            
                    foreach($retorno[$x] as $value1) {
                        $sql = $sql . $value1['idLocalEstoque'] . ',';                
                    }
                }
                $x++;
            }
        } else {
            $sql = " = null";
        }    
        $sql = substr($sql, 0, strlen($sql)-1) . ")";
        return $sql;
    }
    private function CriarArrayCentroCusto($idCentroCusto) {
        $model = new centrocustoModel();
        
        $centrocusto[0] = array();
        $retorno = $model->getCentroCusto("idCentroCustoSuperior = " . $idCentroCusto);
        if ($retorno) {
            $centrocusto[0] = $retorno[0]['idCentroCusto'];
            $sql = 'in (';
            foreach ($retorno as $value) {
                $sql = $sql . $value['idCentroCusto'] . ',';                
            }
        }
//        echo 'cc : ' . $sql . '</br>';
        
        $x=0;
        foreach ($retorno as $value) {
            if ($x>0) {
                $retorno[$x] = $model->getCentroCusto("idCentroCustoSuperior = " . $value['idCentroCusto']);            
                $y=0;
                foreach($retorno[$x] as $value1) {
                    $sql = $sql . $value1['idCentroCusto'] . ',';                
                    $retorno[$x][$y] = $model->getCentroCusto("idCentroCustoSuperior = " . $value1['idCentroCusto']);    
                    foreach ($retorno[$x][$y] as $value2) {
                        $sql = $sql . $value2['idCentroCusto'] . ',';                
                    }
                    $y++;
                }
            }
            $x++;
        }
        
        if (!isset($sql)) {
            $sql = 'in(' . $idCentroCusto . ')';
        } else {
            $sql = substr($sql, 0, strlen($sql)-1) . ")";
        }
  //      echo 'cc : ' . $sql; die;
        return $sql;
    }

}

?>