<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class lancamentosContabeis extends controller {

    public function index_action() {
//die("chegou");
        //Inicializa o Template
        $this->template->run();

//        $model = new LancamentosContabeisModel();
//        $lancamentocontabil_lista = $model->getLancamentoContabil(null,$paginacao=true);

        $modelPeriodo = new periodoContabilModel();
        $periodo = $modelPeriodo->getUltimoPeriodo();
        if ($periodo) {
            $busca['dtInicial'] = $periodo[0]['dtInicial'];
            $busca['dtFinal'] = $periodo[0]['dtFinal'];
            $this->smarty->assign('busca', $busca);
        }
        
        $this->smarty->assign('lancamentoscontabeis_lista', null);
        $this->smarty->display('contabilidade/listaLancamentoContabil.html');
    }

//Funcao de Busca
    public function busca_lc() {
        $idLancamento = isset($_POST['idLancamento']) ? $_POST['idLancamento'] : '';
        $idContaDebito = isset($_POST['idContaDebito']) ? $_POST['idContaDebito'] : '';
        $dsContaDebito = isset($_POST['dsContaDebito']) ? $_POST['dsContaDebito'] : '';
        $idContaCredito = isset($_POST['idContaCredito']) ? $_POST['idContaCredito'] : '';
        $dsContaCredito = isset($_POST['dsContaCredito']) ? $_POST['dsContaCredito'] : '';
        $dtInicial = (!$_POST['dtInicial']=='') ? date("Y-m-d", strtotime(str_replace("/", "-", $_POST["dtInicial"]))) : '';
        $dtFinal = (!$_POST['dtFinal']=='') ? date("Y-m-d", strtotime(str_replace("/", "-", $_POST["dtFinal"]))) : '';
        $nrDocumento = isset($_POST['nrDocumento']) ? $_POST['nrDocumento'] : '';
        $dsHistorico = isset($_POST['dsHistorico']) ? $_POST['dsHistorico'] : '';
        $stSituacao = null;
        if (isset($_POST['stConferidos'])) {
            $stSituacao = 'L';
        }
        $stDiferenca = null;
        if (isset($_POST['stDiferenca'])) {
            $stDiferenca = 'SIM';
        }
        
        $busca = array();
//        $busca['idPeriodo'] = $idPeriodo;
        $sql = "l.idLancamento > 0";
        if ($idLancamento) {
            $sql = $sql . ' and l.idLancamento = ' . $idLancamento;
            $busca['idLancamento'] = $idLancamento;
        }
        if ($idContaDebito) {
            $sql = $sql . ' and ld.idContaDebito = ' . $idContaDebito;
            $busca['idContaDebito'] = $idContaDebito;
            $busca['dsContaDebito'] = $dsContaDebito;
        }
        if ($idContaCredito) {
            $sql = $sql . ' and lc.idContaCredito = ' . $idContaCredito;
            $busca['idContaCredito'] = $idContaCredito;
            $busca['dsContaCredito'] = $dsContaCredito;
        }
        if ($dtInicial) {
            $sql = $sql . " and l.dtLancamento >= '{$dtInicial} 00:00:00'";
            $busca['dtInicial'] = date("d-m-Y", strtotime(str_replace("-", "/", $dtInicial)));
        }
        if ($dtFinal) {
            $sql = $sql . " and l.dtLancamento <= '{$dtFinal} 23:59:59'";
            $busca['dtFinal'] = date("d-m-Y", strtotime(str_replace("-", "/", $dtFinal)));
        }
        if ($nrDocumento) {
            $sql = $sql . " and upper(l.nrDocumento) like upper('%" . $nrDocumento . "%')";
            $busca['nrDocumento'] = $nrDocumento;
        }
        if ($dsHistorico) {
            $sql = $sql . " and upper(l.dsHistorico) like upper('%" . $dsHistorico . "%')";
            $busca['dsHistorico'] = $dsHistorico;
        }
        if ($stSituacao) {
            $sql = $sql . " and l.stSituacao = '{$stSituacao}'";
            $busca['stConferidos'] = 1;
        } else {
            $busca['stConferidos'] = 0;
        }

        if ($stDiferenca) {
            $busca['stDiferenca'] = 1;
        } else {
            $busca['stDiferenca'] = 0;
        }

        $lancamentosDebito = array();
        $lancamentosCredito = array();
        
        $model = new LancamentosContabeisModel();
        $resultado = $model->getLancamentoContabil($sql, $paginacao=true);
        $x=0;
        foreach ($resultado as $value) {
            $resultado[$x]['valortotaldebito'] = 0;
            $resultado[$x]['linhastotaldebito'] = 0;
            $resultado[$x]['valortotalcredito'] = 0;
            $resultado[$x]['linhastotalcredito'] = 0;
            if ($stDiferenca) {
                $totalDebito = $model->getLancamentoContabilDebitoTotal('idLancamento=' . $value['idLancamento']);
                if ($totalDebito) {
                    $resultado[$x]['valortotaldebito'] = $totalDebito[0]['totaldebito'];
                    $resultado[$x]['linhastotaldebito'] = $totalDebito[0]['totallinhasdebito'];
                }
                $totalCredito = $model->getLancamentoContabilCreditoTotal('idLancamento=' . $value['idLancamento']);
                if ($totalCredito) {
                    $resultado[$x]['valortotalcredito'] = $totalCredito[0]['totalcredito'];
                    $resultado[$x]['linhastotalcredito'] = $totalCredito[0]['totallinhascredito'];
                }
                if ($totalDebito && $totalCredito) {
                    if ($totalDebito[0]['totaldebito'] == $totalCredito[0]['totalcredito'] && $totalDebito[0]['totaldebito'] == $value['vlLancamento']) {
                        unset($resultado[$x]);
                    } else {
                        $lancamentosDebito[$value['idLancamento']] = $model->getLancamentoContabilDebito('l.idLancamento=' . $value['idLancamento']);
                        $lancamentosCredito[$value['idLancamento']] = $model->getLancamentoContabilCredito('l.idLancamento=' . $value['idLancamento']);
                        if ($totalCredito[0]['totallinhascredito'] == 1 && $totalDebito[0]['totallinhasdebito'] == 1) {
                            $resultado[$x]['nomecontadebito'] = $lancamentosDebito[$value['idLancamento']][0]['idContaReduzida'] . ' - ' . $lancamentosDebito[$value['idLancamento']][0]['dsConta'];
                            $resultado[$x]['nomecontacredito'] = $lancamentosCredito[$value['idLancamento']][0]['idContaReduzida'] . ' - ' . $lancamentosCredito[$value['idLancamento']][0]['dsConta'];
                        } else {
                            $resultado[$x]['nomecontadebito'] = '';
                            $resultado[$x]['nomecontacredito'] = '';
                        }                
                    }
                } else {
                    $lancamentosDebito[$value['idLancamento']] = $model->getLancamentoContabilDebito('l.idLancamento=' . $value['idLancamento']);
                    $lancamentosCredito[$value['idLancamento']] = $model->getLancamentoContabilCredito('l.idLancamento=' . $value['idLancamento']);
                    if ($totalCredito[0]['totallinhascredito'] == 1 && $totalDebito[0]['totallinhasdebito'] == 1) {
                        $resultado[$x]['nomecontadebito'] = $lancamentosDebito[$value['idLancamento']][0]['idContaReduzida'] . ' - ' . $lancamentosDebito[$value['idLancamento']][0]['dsConta'];
                        $resultado[$x]['nomecontacredito'] = $lancamentosCredito[$value['idLancamento']][0]['idContaReduzida'] . ' - ' . $lancamentosCredito[$value['idLancamento']][0]['dsConta'];
                    } else {
                        $resultado[$x]['nomecontadebito'] = '';
                        $resultado[$x]['nomecontacredito'] = '';
                    }                
                }
            } else {
                
                $totalDebito = $model->getLancamentoContabilDebitoTotal('idLancamento=' . $value['idLancamento']);
                if ($totalDebito) {
                    $resultado[$x]['valortotaldebito'] = $totalDebito[0]['totaldebito'];
                    $resultado[$x]['linhastotaldebito'] = $totalDebito[0]['totallinhasdebito'];
                }
                $totalCredito = $model->getLancamentoContabilCreditoTotal('idLancamento=' . $value['idLancamento']);
                if ($totalCredito) {
                    $resultado[$x]['valortotalcredito'] = $totalCredito[0]['totalcredito'];
                    $resultado[$x]['linhastotalcredito'] = $totalCredito[0]['totallinhascredito'];
                }
                
                $lancamentosDebito[$value['idLancamento']] = $model->getLancamentoContabilDebito('l.idLancamento=' . $value['idLancamento']);
                $lancamentosCredito[$value['idLancamento']] = $model->getLancamentoContabilCredito('l.idLancamento=' . $value['idLancamento']);
                if ($totalCredito[0]['totallinhascredito'] == 1 && $totalDebito[0]['totallinhasdebito'] == 1) {
                    $resultado[$x]['nomecontadebito'] = $lancamentosDebito[$value['idLancamento']][0]['idContaReduzida'] . ' - ' . $lancamentosDebito[$value['idLancamento']][0]['dsConta'];
                    $resultado[$x]['nomecontacredito'] = $lancamentosCredito[$value['idLancamento']][0]['idContaReduzida'] . ' - ' . $lancamentosCredito[$value['idLancamento']][0]['dsConta'];
                } else {
                    $resultado[$x]['nomecontadebito'] = '';
                    $resultado[$x]['nomecontacredito'] = '';
                }
            }
            $x++;
        }
        
        if (sizeof($resultado) > 0) {
            $this->smarty->assign('lancamentosdebito', $lancamentosDebito);
            $this->smarty->assign('lancamentoscredito', $lancamentosCredito);
            $this->smarty->assign('lancamentoscontabeis_lista', $resultado);
            $this->smarty->assign('title', 'Lancamentos Contabeis');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('contabilidade/listaLancamentoContabil.html');
        } else {
            $this->smarty->assign('lancamentosdebito', $lancamentosDebito);
            $this->smarty->assign('lancamentoscredito', $lancamentosCredito);
            $this->smarty->assign('lancamentoscontabeis_lista', null);
            $this->smarty->assign('title', 'Lancamentos Contabeis');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('contabilidade/listaLancamentoContabil.html');
        }
    }

    //Funcao de Inserir
    public function novo_lancamentoContabil() {
        $sy = new system\System();
        $idLancamento = $sy->getParam('idLancamento');

        $model = new LancamentosContabeisModel();

        if ($idLancamento > 0) {
            $registro = $model->getLancamentoContabil('l.idLancamento=' . $idLancamento);
            $registro = $registro[0]; //Passando Periodo
            $lancamentosDebito = $model->getLancamentoContabilDebito('l.idLancamento=' . $idLancamento);
            $lancamentosCredito = $model->getLancamentoContabilCredito('l.idLancamento=' . $idLancamento);
            $totalDebito = $model->getLancamentoContabilDebitoTotal('idLancamento=' . $idLancamento);
            if ($totalDebito) {
                $totalDebito = $totalDebito[0]['totaldebito'];
            }
            $totalCredito = $model->getLancamentoContabilCreditoTotal('idLancamento=' . $idLancamento);
            if ($totalCredito) {
                $totalCredito = $totalCredito[0]['totalcredito'];
            }
            
            if ($registro['stSituacao'] == 'C') {
                $registro['stConferido'] = 'CHECKED';
            }
        } else {
            //Novo Registro
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
            $lancamentosDebito = null;
            $lancamentosCredito = null;
            $totalDebito = null;
            $totalCredito = null;
        }
        
        
        $this->smarty->assign('lancamentosdebito', $lancamentosDebito);
        $this->smarty->assign('lancamentoscredito', $lancamentosCredito);
        $this->smarty->assign('totaldebito', $totalDebito);
        $this->smarty->assign('totalcredito', $totalCredito);
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('title', 'Novo Lancamento Contabil');
        $this->smarty->display('contabilidade/novoLancamentoContabil.tpl');
    }

    public function verPeriodo() {
        $dataLancamento = (!$_POST['dtLancamento']=='') ? date("Y-m-d", strtotime(str_replace("/", "-", $_POST["dtLancamento"]))) : '';        
        $where = "'{$dataLancamento}' between dtInicial and dtFinal"; 
        $model = new periodoContabilModel();
        $retorno = $model->getPeriodo($where);
        $msg = null;
        if ($retorno) {
            if ($retorno[0]['stSituacao'] == 'E') {
                $msg = 'Data dentro de um período já encerrado';
            }
        } else {
            $msg = 'Data fora do período contabil';
        }
        $retorno = array(
            'msg' => $msg
        );
        echo json_encode($retorno);
    }
    
    // Gravar Padrao
    public function gravar_LancamentoContabil() {
        $model = new LancamentosContabeisModel();
        $data = $this->trataPost($_POST);

        if ($data['idLancamento'] == null) {
            $id=$model->setLancamentoContabil($data);
        } else {
            $id=$data['idLancamento'];
            $model->updLancamentoContabil($data); //update
        }    
        header('Location: /lancamentosContabeis/novo_lancamentoContabil/idLancamento/' . $id);
    }
    
    public function gravar_lancamentodebito() {
        $model = new LancamentosContabeisModel();
        $data = $this->trataPostDebito($_POST);
        $model->setLancamentoContabilDebito($data);
        $retorno = array();
        echo json_encode($retorno);        
    }
    
    public function gravar_lancamentocredito() {
        $model = new LancamentosContabeisModel();
        $data = $this->trataPostCredito($_POST);
        $model->setLancamentoContabilCredito($data);
        $retorno = array();
        echo json_encode($retorno);        
    }
    
    private function trataPostDebito($post) {
        $data['idLancamentoDebito'] =  null;
        $data['idLancamento'] = ($post['idLancamento'] != '') ? $post['idLancamento'] : null;
        $data['idContaDebito'] = ($post['idContaDebito'] != '') ? $post['idContaDebito'] : null;
        $data['dsHistorico'] = ($post['dsHistoricoDebito'] != '') ? $post['dsHistoricoDebito'] : null;
        $data['vlLancamento'] = ($post['vlLancamentoDebito'] != '') ? str_replace(",",".",str_replace(".","",$post['vlLancamentoDebito'])) : null;
        return $data;
    }
    
    private function trataPostCredito($post) {
        $data['idLancamentoCredito'] =  null;
        $data['idLancamento'] = ($post['idLancamento'] != '') ? $post['idLancamento'] : null;
        $data['idContaCredito'] = ($post['idContaCredito'] != '') ? $post['idContaCredito'] : null;
        $data['dsHistorico'] = ($post['dsHistoricoCredito'] != '') ? $post['dsHistoricoCredito'] : null;
        $data['vlLancamento'] = ($post['vlLancamentoCredito'] != '') ? str_replace(",",".",str_replace(".","",$post['vlLancamentoCredito'])) : null;
        return $data;
    }
    
    private function trataPost($post) {
        $data['stSituacao'] = isset($post['stConferido']) ? 'C' : 'L';
        $data['idLancamento'] = ($post['idLancamento'] != '') ? $post['idLancamento'] : null;
        $data['nrDocumento'] = ($post['nrDocumento'] != '') ? $post['nrDocumento'] : null;
        $data['dsHistorico'] = ($post['dsHistorico'] != '') ? $post['dsHistorico'] : null;
        $data['dtLancamento'] = ($post['dtLancamento'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $_POST["dtLancamento"]))) : date('Y-m-d h:m:s');
        $data['vlLancamento'] = ($post['vlLancamento'] != '') ? str_replace(",",".",str_replace(".","",$post['vlLancamento'])) : null;
        return $data;
    }

    public function del_lancamento() {
        $idLancamento = $_POST['idLancamento'];        
        $model = new LancamentosContabeisModel();
        $where = 'idLancamento = ' . $idLancamento;
        $model->delLancamentoContabil($where);
        $retorno = array();
        echo json_encode($retorno);
    }

    public function del_lancamentodebito() {
        $idLancamento = $_POST['idLancamentoDebito'];        
        $model = new LancamentosContabeisModel();
        $where = 'idLancamentoDebito = ' . $idLancamento;
        $model->delLancamentoContabilDebito($where);
        $retorno = array();
        echo json_encode($retorno);
    }

    public function del_lancamentocredito() {
        $idLancamento = $_POST['idLancamentoCredito'];        
        $model = new LancamentosContabeisModel();
        $where = 'idLancamentoCredito = ' . $idLancamento;
        $model->delLancamentoContabilCredito($where);
        $retorno = array();
        echo json_encode($retorno);
    }

    public function getConta() {
        $url = explode("=", $_SERVER['REQUEST_URI']);
        $dtLancamento = date("Y-m-d", strtotime(str_replace("/", "-", substr($url[0],44,10))));
        $key = $url[1];
        if (!empty($key)) {
          $busca = trim($key);
          $model = new planodecontasModel();
          $where = "stTipoConta = 'A' and ('{$dtLancamento}' between p.dtInicial and p.dtFinal) and (UPPER(dsConta) like UPPER('%{$key}%') OR UPPER(idContaReduzida) like UPPER('%{$key}%'))";
          $retorno = $model->getPlanoDeContas($where);
          $return = array();
          if (count($retorno)) {
            $row = array();
            for ($i = 0; $i < count($retorno); $i++) {
              $row['value'] = $retorno[$i]["idContaReduzida"] . '-' . strtoupper($retorno[$i]["dsConta"]);
              $row["id"] = $retorno[$i]["idContaReduzida"];
              array_push($return, $row);
            }
          }
          echo json_encode($return);
        }        
    }            
}

?>