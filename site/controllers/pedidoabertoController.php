<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class pedidoaberto extends controller {

    private $local = LOCAL;

    public function index_action() {
        //Inicializa o Template
        $this->template->run();
        
        $model = new pedidoModel();
        $registro = $model->getPedido('a.idSituacaoPedido < 3', false);
        $_SESSION['buscar_pedidosaberto']['sql'] = 'a.idSituacaoPedido < 3';
        $modelSituacaoPedido = new situacaoPedidoModel();
        $lista_situacao = array('' => 'SELECIONE');
        foreach ($modelSituacaoPedido->getSituacaoPedido() as $value) {
//            if ($value['idSituacaoPedido'] < 3) {
                $lista_situacao[$value['idSituacaoPedido']] = $value['dsSituacaoPedido'];
//            }
        }
        
        $this->smarty->assign('local', $this->local);
        $this->smarty->assign('lista_situacao', $lista_situacao);
        $this->smarty->assign('pedidos', $registro);
        $this->smarty->assign('title', 'Pedidos de Compra em Aberto');
        $this->smarty->display('pedido/pedidoaberto.html');
    }
    
    public function busca_pedido() {
        //se nao existir o indice estou como padrao '';
        $dsSolicitante = isset($_POST['dsSolicitante']) ? $_POST['dsSolicitante'] : '';
        $dsProduto = isset($_POST['dsProduto']) ? $_POST['dsProduto'] : '';
        $dsParceiro = isset($_POST['dsParceiro']) ? $_POST['dsParceiro'] : '';
        $dsObservacao = isset($_POST['dsObservacao']) ? $_POST['dsObservacao'] : '';
        $idSituacaoPedido = isset($_POST['idSituacaoPedido']) ? $_POST['idSituacaoPedido'] : '';
        //$texto = '';
        $model = new pedidoModel();

        $modelSituacaoPedido = new situacaoPedidoModel();
        $lista_situacao = array('' => 'SELECIONE');
        foreach ($modelSituacaoPedido->getSituacaoPedido() as $value) {
//            if ($value['idSituacaoPedido'] < 3) {
                $lista_situacao[$value['idSituacaoPedido']] = $value['dsSituacaoPedido'];
//            }
        }
        
        $busca = array();
        $sql = 'a.idPedido > 0';
        if ($idSituacaoPedido) {
            $sql = $sql . " and a.idSituacaoPedido = " . $idSituacaoPedido;
            $busca['idSituacaoPedido'] = $idSituacaoPedido;
        }
        if ($dsProduto) {
            $sql = $sql . " and upper(prod.dsInsumo) like upper('%" . $dsProduto . "%')";
            $busca['dsProduto'] = $dsProduto;
        }
        if ($dsParceiro) {
            $sql = $sql . " and upper(m.dsParceiro) like upper('%" . $dsParceiro . "%')";
            $busca['dsParceiro'] = $dsParceiro;
        }
        if ($dsObservacao) {
            $sql = $sql . " and upper(a.dsObservacao) like upper('%" . $dsObservacao . "%')";
            $busca['dsObservacao'] = $dsObservacao;
        }
        if ($dsSolicitante) {
            $sql = $sql . " and upper(a.dsSolicitante) like upper('%" . $dsSolicitante . "%')";
            $busca['dsSolicitante'] = $dsSolicitante;
        }
        
        $cpagina = null;
        if (isset($_POST['cpagina'])) { 
            $cpagina = true;
            $busca['cpagina'] = 1;
        }
        
        $resultado = $model->getPedido($sql, $cpagina);
        $_SESSION['buscar_pedidosaberto']['sql'] = $sql;
        if (sizeof($resultado) > 0) {
            $this->smarty->assign('lista_situacao', $lista_situacao);            
            $this->smarty->assign('pedidos', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'Pedidos em Aberto');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('pedido/pedidoaberto.html');
        } else {
            $this->smarty->assign('lista_situacao', $lista_situacao);            
            $this->smarty->assign('pedidos', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'Pedidos em Aberto');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('pedido/pedidoaberto.html');
        }
    }
    
    public function busca_criarcsv() {
        $sql = $_SESSION['buscar_pedidosaberto']['sql'];
        $model = new pedidoModel();
        $resultado = $model->getPedido($sql, false);

        if ($resultado) {
            $this->criarCSV($resultado);
        }
     //   header('Location: /solicitacaocomprasmanutencao');
    }
    
    public function criarCSV($resultado) {
        
        $limit = 30000;
        $offset = 0;

        global $PATH;
        $caminho = "/var/www/html/thopos.com.br/site/storage/tmp/csv/";

        // Storage
        if (!is_dir($caminho)) {
          mkdir($caminho, 0777, true);
        }

        $filename = "pedidosemaberto_" . date("YmsHis") . ".csv";

        $headers[] = implode(";", array(
          "\"SOLICITANTE\"",
          "\"EMPRESA\"",
          "\"CENTRO CUSTO\"",
          "\"GRUPO DESPESA\"",
          "\"ITEM DESPESA\"",
          "\"FORNECEDOR\"",
          "\"ID PEDIDO\"",
          "\"DATA PEDIDO\"",
          "\"PRIORIDADE\"",
          "\"STATUS PEDIDO\"",
          "\"PRODUTO\"",
          "\"QUANTIDADE\"",
          "\"VAL ITEM\""
        ));

        if (file_exists("{$caminho}" . '/' . "{$filename}")) {
          unlink("{$caminho}" . '/' . "{$filename}");
        }

        // Arquivo
        $handle = fopen("{$caminho}" . '/' . "{$filename}", 'w+');
        fwrite($handle, implode(";" . PHP_EOL, $headers));
        fwrite($handle, ";" . PHP_EOL);
        fflush($handle);

        // Fecha o arquivo da $_SESSION para liberar o servidor para servir outras requisições
        session_write_close();

        $output = array();
        foreach ($resultado as $value) {
            $output[] = implode(";", array(
              "\"{$value["dsSolicitante"]}\"",
              "\"{$value["dsEmpresa"]}\"",
              "\"{$value["dsCentroCusto"]}\"",
              "\"{$value["dsGrupoDR"]}\"",
              "\"{$value["dsItemDR"]}\"",
              "\"{$value["dsParceiro"]}\"",
              "\"{$value["idPedido"]}\"",
              "\"{$value["dtPedido"]}\"",
              "\"{$value["dsPrioridade"]}\"",
              "\"{$value["dsSituacaoPedido"]}\"",
              "\"{$value["dsInsumo"]}\"",
              "\"{$value["qtEntregue"]}\"",
              "\"{$value["vlTotalPedido"]}\""
            ));
        }
        fwrite($handle, implode(";" . PHP_EOL, $output));
        fwrite($handle, ";" . PHP_EOL);
        fflush($handle);
        fclose($handle);
        $this->download($caminho . '/' . $filename, 'CSV', $filename);        
    }

    private function download($nome, $tipo, $filename) {
      if (!empty($nome)) {
        if (file_exists($nome)) {
          header('Content-Transfer-Encoding: binary'); // For Gecko browsers mainly
          header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($nome)) . ' GMT');
          header('Accept-Ranges: bytes'); // For download resume
          header('Content-Length: ' . filesize($nome)); // File size
          header('Content-Encoding: none');
          header("Content-Type: application/{$tipo}"); // Change this mime type if the file is not PDF
          header('Content-Disposition: attachment; filename=' . $filename);
          // Make the browser display the Save As dialog
          readfile($nome);
          unlink($nome);
        }
      }
    }    

}

?>