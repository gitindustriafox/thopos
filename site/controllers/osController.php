<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class os extends controller {

    public function index_action() {
        //Inicializa o Template
        $this->template->run();
        unset($_SESSION['os']['id']);
        
        $model = new osModel();
        $registro = $model->getOS('a.idStatusOS = 1');

        $this->smarty->assign('os', $registro);
        $this->smarty->assign('title', 'Ordem de Serviço');
        $this->smarty->display('os/lista.html');
        
    }

    public function minhasOS() {
        //Inicializa o Template
        $this->template->run();
        unset($_SESSION['os']['id']);
        
        $model = new osModel();
        $registro = $model->getOS('a.idStatusOS = 1 and a.idUsuario = ' . $_SESSION["user"]["usuario"]);

        $modelSetor = new setorModel();
        $lista_setor = array('' => 'SELECIONE');
        foreach ($modelSetor->getSetor() as $value) {
            $lista_setor[$value['idSetor']] = $value['dsSetor'];
        }
        
        $ccusto = new centrocustoModel();
        $lista_centrocusto= array('' => 'SELECIONE');
        foreach ($ccusto->getCentroCusto() as $value) {
            $lista_centrocusto[$value['idCentroCusto']] = $value['dsCentroCusto'];
        }
        $listaS = array('' => 'SELECIONE', '0' => 'DIGITADA', '3' => 'EM ANDAMENTO', '4' => 'CANCELADA', '5' => 'AGUARDANDO APROVAÇÃO', '6' => 'PAUSADA', '7' => 'FINALIZADA', '8' => 'APROVADA');
        
        $this->smarty->assign('lista_CentroCusto', $lista_centrocusto);
        $this->smarty->assign('lista_setor', $lista_setor);
        $this->smarty->assign('lista_status', $listaS);
        $this->smarty->assign('os', $registro);
        $this->smarty->display('os/minhasOS.html');
        
    }

//Funcao de Busca
    public function busca_os() {
        //se nao existir o indice estou como padrao '';
        $texto = isset($_POST['buscadescricao']) ? $_POST['buscadescricao'] : '';
        //$texto = '';
        $model = new osModel();
        $sql = "a.stStatus <> 0 and upper(a.dsOS) like upper('%" . $texto . "%')"; //somente os nao excluidos
        $resultado = $model->getOS($sql);

        if (sizeof($resultado) > 0) {
            $this->smarty->assign('os_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'os');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('os/lista.html');
        } else {
            $this->smarty->assign('os_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'os');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('os/lista.html');
        }
    }
    //Funcao de busca
    public function busca_minhas_os() {
        //se nao existir o indice estou como padrao '';
        
        $idOS = isset($_POST['idOS']) ? $_POST['idOS'] : '';
        $dsProblema = isset($_POST['dsProblema']) ? $_POST['dsProblema'] : '';
        $dtInicio = isset($_POST['dtInicio']) ? $_POST['dtInicio'] : '';
        $dtFim = isset($_POST['dtFim']) ? $_POST['dtFim'] : '';
        $idCentroCusto = isset($_POST['idCentroCusto']) ? $_POST['idCentroCusto'] : '';
        $idSetor = isset($_POST['idSetor']) ? $_POST['idSetor'] : '';
        $status = isset($_POST['idStatusOS']) ? $_POST['idStatusOS'] : '';
        
        $sql = "a.stStatus <> 0 and a.idUsuario = " . $_SESSION["user"]["usuario"];
        if ($dsProblema) {
            $sql = $sql .  " and upper(a.dsProblema) like upper('%" . $dsProblema . "%')"; //somente os nao excluidos
            $busca['dsProblema'] = $dsProblema;
        }

        if ($idOS) {
            $sql = $sql .  " and a.idOS = " . $idOS; //somente os nao excluidos
            $busca['idOS'] = $idOS;
        }

        if ($idCentroCusto) {
            $sql = $sql .  " and a.idCentroCusto = " . $idCentroCusto; //somente os nao excluidos
            $busca['idCentroCusto'] = $idCentroCusto;
        }

        if ($idSetor) {
            $sql = $sql .  " and a.idSetor = " . $idSetor; //somente os nao excluidos
            $busca['idSetor'] = $idSetor;
        }

        if ($status) {
            $sql = $sql .  " and a.idStatusOS = " . $status; //somente os nao excluidos
            $busca['idStatusOS'] = $status;
        }

        $dtInicio = ($dtInicio != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $dtInicio))) : '';
        $dtFim = ($dtFim != '') ? date("Y-m-d H:i", strtotime(str_replace("/", "-", $dtFim))) : '';
        if ($dtInicio && $dtFim) {
            $sql = $sql . " and a.dtOS >= '" . $dtInicio . "' and a.dtOS <= '" . $dtFim . "'";  
        }
        $busca['dtInicio'] = $dtInicio;
        $busca['dtFim'] = $dtFim;

        $model = new osModel();
        $resultado = $model->getOS($sql);        

        $modelSetor = new setorModel();
        $lista_setor = array('' => 'SELECIONE');
        foreach ($modelSetor->getSetor() as $value) {
            $lista_setor[$value['idSetor']] = $value['dsSetor'];
        }
        
        $ccusto = new centrocustoModel();
        $lista_centrocusto= array('' => 'SELECIONE');
        foreach ($ccusto->getCentroCusto() as $value) {
            $lista_centrocusto[$value['idCentroCusto']] = $value['dsCentroCusto'];
        }
        $listaS = array('' => 'SELECIONE', '1' => 'DIGITADA', '3' => 'EM ANDAMENTO', '4' => 'CANCELADA', '5' => 'AGUARDANDO APROVAÇÃO', '6' => 'PAUSADA', '7' => 'FINALIZADA', '8' => 'APROVADA');
        
        $this->smarty->assign('lista_CentroCusto', $lista_centrocusto);
        $this->smarty->assign('lista_setor', $lista_setor);
        $this->smarty->assign('lista_status', $listaS);        

        $this->smarty->assign('busca', $busca);
        $this->smarty->assign('title', 'Ordem de Serviço');
        $this->smarty->assign('os', $resultado);
        $this->smarty->display('os/minhasOS.html');
    }
    //Funcao de Inserir
    public function novo_os() {
        $sy = new system\System();
  
        if (!isset($_SESSION['os']['id'])) {
           $idOS = $sy->getParam('idOS');
        } else {
          if ($_SESSION['os']['id'] == 0) {
             $idOS = null;
          } else {
            $idOS = $_SESSION['os']['id'];            
          }
        }
        
        $model = new osModel();
        if (isset($idOS)) {
            if ((bool) $idOS) {
                $registro = $model->getOS('a.idOS=' . $idOS);  
                if ($registro) {
                    $registro = $registro[0];
                } else {
                    //Novo Registro
                    $registro = $model->estrutura_vazia();
                    $registro = $registro[0];                    
                }
            } else {
                //Novo Registro
                $registro = $model->estrutura_vazia();
                $registro = $registro[0];                    
            }
        } else {
            //Novo Registro
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        $modelColaborador = new colaboradorModel();
        $lista_colaboradorsolicitante = array('' => 'SELECIONE');
        foreach ($modelColaborador->getColaborador('stFazParteAgenda = 2') as $value) {
            $lista_colaboradorsolicitante[$value['idColaborador']] = $value['dsColaborador'];
        }
        $lista_colaboradorresponsavel = array('' => 'SELECIONE');
        foreach ($modelColaborador->getColaborador('stFazParteAgenda = 3') as $value) {
            $lista_colaboradorresponsavel[$value['idColaborador']] = $value['dsColaborador'];
        }
        $modelSetor = new setorModel();
        $lista_setorsolicitante = array('' => 'SELECIONE');
        foreach ($modelSetor->getSetor() as $value) {
            $lista_setorsolicitante[$value['idSetor']] = $value['dsSetor'];
        }
        $lista_setoros = array('' => 'SELECIONE');
        foreach ($modelSetor->getSetor() as $value) {
            $lista_setoros[$value['idSetor']] = $value['dsSetor'];
        }
        $modelOS = new osModel();
        $lista_OS = array('' => 'SELECIONE');
        foreach ($modelOS->getOS() as $value) {
            $lista_OS[$value['idOS']] = $value['nrOS'];
        }
        $motivolOS = new motivoModel();
        $lista_Motivo = array('' => 'SELECIONE');
        foreach ($motivolOS->getMotivo() as $value) {
            $lista_Motivo[$value['idMotivo']] = $value['dsMotivo'];
        }
        $tipomanutencao= new tipomanutencaoModel();
        $lista_tipomanutencao= array('' => 'SELECIONE');
        foreach ($tipomanutencao->getTipoManutencao() as $value) {
            $lista_tipomanutencao[$value['idTipoManutencao']] = $value['dsTipoManutencao'];
        }
        $ccusto = new centrocustoModel();
        $lista_centrocusto= array('' => 'SELECIONE');
        foreach ($ccusto->getCentroCusto() as $value) {
            $lista_centrocusto[$value['idCentroCusto']] = $value['dsCentroCusto'];
        }
                
        $tipofalha= new tipofalhaModel();
        $lista_tipofalha= array('' => 'SELECIONE');
        foreach ($tipofalha->getTipoFalha() as $value) {
            $lista_tipofalha[$value['idTipoFalha']] = $value['dsTipoFalha'];
        }
        $osGrupo = new osgrupoModel();
        $lista_osgrupo= array('' => 'SELECIONE');
        foreach ($osGrupo->getOSGrupo() as $value) {
            $lista_osgrupo[$value['idOSGrupo']] = $value['dsOSGrupo'];
        }
       
        $this->smarty->assign('nrOS', null);
        if (!$idOS) {
            $nrUltimoOS = $model->getUltimaOS()[0];
            if ($nrUltimoOS['UltimaOS']) {                
                $registro['nrOS'] = ($nrUltimoOS['UltimaOS'] !='') ? $nrUltimoOS['UltimaOS'] + 1 :null;
            }            
        }
        
        $lista_osstatus = null;
        if($idOS) {
            $lista_osstatus = $model->getOSStatus('a.idOS = ' . $idOS);
        }
        $this->smarty->assign('os_listastatus', $lista_osstatus);
        
        $colaboradoresos = null;
        
        if ($idOS) {
            $valores = $this->calculaDatas($registro['dtInicio'], $registro['dtFim']);
            $this->smarty->assign('lista_reduzida', $valores['statusreduzido']);
            $this->smarty->assign('lista_status', $valores['periodo']);
            if ($registro['dtInicio'] && $registro['dtFim']) {                
                $colaboradoresos = $model->getOSColaboradores('a.stStatus = 1', $registro['dtInicio'], $registro['dtFim']);   
            }    
        
        } else {
            $this->smarty->assign('lista_reduzida', null);
            $this->smarty->assign('lista_status', null);
        }
        
        $this->smarty->assign('os_listacolabos', $colaboradoresos);
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('idOS', $idOS);
        $this->smarty->assign('lista_colaboradorresponsavel', $lista_colaboradorresponsavel);
        $this->smarty->assign('lista_colaboradorsolicitante', $lista_colaboradorsolicitante);
        $this->smarty->assign('lista_setoros', $lista_setoros);
        $this->smarty->assign('lista_setorsolicitante', $lista_setorsolicitante);
        $this->smarty->assign('lista_tipomanutencao', $lista_tipomanutencao);
        $this->smarty->assign('lista_tipofalha', $lista_tipofalha);
        $this->smarty->assign('lista_osgrupo', $lista_osgrupo);
        $this->smarty->assign('lista_centrocusto', $lista_centrocusto);
        $this->smarty->assign('title', 'Ordem de Serviço');
        $this->smarty->display('os/form_novo.tpl');
    }

    public function importarfotos() {
        $idOS = $_POST['idOS'];
        $dsTabela = $_POST['dsTabela'];
        
        $model = new documentoModel();        
        $lista = $model->getDocumento("dsTabela = '" . $dsTabela . "' and idTabela  = " . $idOS);
        $this->smarty->assign('idOS', $idOS);
        $this->smarty->assign('dsTabela', $dsTabela);
        $this->smarty->assign('fotos_lista', $lista);
        $this->smarty->display("os/modalFotos/thumbnail.tpl");        
    }

    public function delFoto() {
        $idOS = $_POST['idOS'];
        $idDocumento = $_POST['idDocumento'];
        $dsTabela = 'prodOS';
        $model = new documentoModel();   
        $dados = array('idDocumento' => $idDocumento, 'idTabela' => $idOS);
        $model->delDocumento($dados);        
        
        $lista = $model->getDocumento("dsTabela = '" . $prodOS . "' and idTabela  = " . $idOS);
        $this->smarty->assign('fotos_lista', $lista);
        $html = $this->smarty->fetch("os/modalFotos/thumbnail.tpl");   
        $retorno = array('html' => $html);
        json_encode($retorno);   
    }

    public function adicionarfoto() {
        
        $idOS = $_POST['idOS'];
        $dsTabela = $_POST['dsTabela'];
        if (!$_FILES['arquivo']['name']) {
          echo "Favor escolher um arquivo para fazer o upload";
          exit;            
        }
        
        $extensao = explode('.',$_FILES['arquivo']['name'])[1];
        $novonome = $idOS . '_' . date('Ymdis') . '.' . $extensao;
        
        $_UP['pasta'] = 'storage/tmp/documentos/';
        // Tamanho máximo do arquivo (em Bytes)
        $_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
        // Array com as extensões permitidas
        $_UP['extensoes'] = array('jpg','jpeg','png','bmp');
        // Renomeia o arquivo? (Se true, o arquivo será salvo como .jpg e um nome único)
        $_UP['renomeia'] = false;
        // Array com os tipos de erros de upload do PHP
        $_UP['erros'][0] = 'Não houve erro';
        $_UP['erros'][1] = 'O arquivo no upload é maior do que o limite do PHP';
        $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
        $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
        $_UP['erros'][4] = 'Não foi feito o upload do arquivo';
        // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
        if ($_FILES['arquivo']['error'] != 0) {
          die("Não foi possível fazer o upload, erro:" . $_UP['erros'][$_FILES['arquivo']['error']]);
          exit; // Para a execução do script
        }
        // Caso script chegue a esse ponto, não houve erro com o upload e o PHP pode continuar
        // Faz a verificação da extensão do arquivo
//        $extensao = strtolower(end(explode('.', $_FILES['arquivo']['name'])));
//        if (array_search($extensao, $_UP['extensoes']) === false) {
//          echo "Por favor, envie arquivos com as seguinte extensão: csv";
//          exit;
//        }
        // Faz a verificação do tamanho do arquivo
        if ($_UP['tamanho'] < $_FILES['arquivo']['size']) {
          echo "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
          exit;
        }
        // O arquivo passou em todas as verificações, hora de tentar movê-lo para a pasta
        // Primeiro verifica se deve trocar o nome do arquivo
//        if ($_UP['renomeia'] == true) {
//          // Cria um nome baseado no UNIX TIMESTAMP atual e com extensão .jpg
//          $nome_final = md5(time()).'.csv';
//        } else {
          // Mantém o nome original do arquivo
//          $nome_final = $_FILES['arquivo']['name'];
//        }

        // Depois verifica se é possível mover o arquivo para a pasta escolhida
        if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $_UP['pasta'] . $novonome)) {
//          Upload efetuado com sucesso, exibe uma mensagem e um link para o arquivo
//          echo "Upload efetuado com sucesso!";
//          echo '<a href="' . $_UP['pasta'] . $nome_final . '">Clique aqui para acessar o arquivo</a>';
//          
//            exit;
//          $this->importar($_UP['pasta'] . $nome_final, $idPeriodo);
//          header('Location: /planodecontas/novo_planodecontas/idPeriodo/' . $idPeriodo);

            $model = new documentoModel();    
            $dados = array(
                'idDocumento' => null,
                'dsTabela' => $dsTabela,
                'idTabela' => $idOS,
                'dsLocalArquivo' => $_UP['pasta'] . $novonome                
            );
            $model->setDocumento($dados);
        } else {
          // Não foi possível fazer o upload, provavelmente a pasta está incorreta
          echo "Não foi possível enviar o arquivo, tente novamente";
        }    
        header('Location: /os/solicitar/idOS/' . $idOS);
//        $model = new documentoModel();        
//        $lista = $model->getDocumento("dsTabela = '" . $dsTabela . "' and idTabela  = " . $idOS);
//        $this->smarty->assign('fotos_lista', $lista);
//        $html = $this->smarty->fetch("os/modalFotos/thumbnail.tpl");    
//        $retorno = array('html' => $html);
//        json_encode($retorno);        
    }
    
    public function solicitar() {
        $sy = new system\System();
  
        if (!isset($_SESSION['os']['id'])) {
           $idOS = $sy->getParam('idOS');
        } else {
          if ($_SESSION['os']['id'] == 0) {
             $idOS = null;
          } else {
            $idOS = $_SESSION['os']['id'];            
          }
        }
        $modelSetor = new setorModel();
        $ccusto = new centrocustoModel();
        $model = new osModel();
        if (isset($idOS)) {
            if ((bool) $idOS) {
                $registro = $model->getOS('a.idOS=' . $idOS);  
                if ($registro) {
                    $registro = $registro[0];
                } else {
                    //Novo Registro
                    $registro = $model->estrutura_vazia();
                    $registro = $registro[0];                    
                }
            } else {
                //Novo Registro
                $registro = $model->estrutura_vazia();
                $registro = $registro[0];                    
            }
        } else {
            //Novo Registro
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }

        $lista_setoros = array('' => 'SELECIONE');
        foreach ($modelSetor->getSetor() as $value) {
            $lista_setoros[$value['idSetor']] = $value['dsSetor'];
        }
        $ccusto = new centrocustoModel();
        $lista_centrocusto= array('' => 'SELECIONE');
        foreach ($ccusto->getCentroCusto() as $value) {
            $lista_centrocusto[$value['idCentroCusto']] = $value['dsCentroCusto'];
        }

        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('lista_setoros', $lista_setoros);
        $this->smarty->assign('lista_centrocusto', $lista_centrocusto);
        $this->smarty->assign('title', 'Ordem de Serviço');
        $this->smarty->display('os/solicitar.tpl');
    }
    // Gravar Padrao
    public function gravar_os() {
        
        $model = new osModel();
        $data = $this->trataPost($_POST);
        if ($data['idOS'] == null) {
            $id = $model->setOS($data);            
            
            $datastatus = array(
            'idOSMudancaStatus' => null,
            'dtMudanca' => $data['dtOS'],
            'idOS' => $id,
            'idStatusOS' => $data['idStatusOS'],
            'idUsuario' => $_SESSION["user"]["usuario"],
            'dsObservacao' => 'Digitação da Ordem de Serviço',
            'idOrigemInformacao' => 1
            );
            $model->setNovoStatusOS($datastatus);
        } else {
            $model->updOS($data);
        }
        header('Location: /os');                
    }
    
    public function gravar_solicitacao_os() {
        
        $model = new osModel();
        $data = $this->trataPostSolicitacao($_POST);
        if ($data['idOS'] == null) {
            $id = $model->setOS($data);            

            $datastatus = array(
            'idOSMudancaStatus' => null,
            'dtMudanca' => $data['dtOS'],
            'idOS' => $id,
            'idStatusOS' => $data['idStatusOS'],
            'idUsuario' => $_SESSION["user"]["usuario"],
            'dsObservacao' => 'Digitação da Ordem de Serviço',
            'idOrigemInformacao' => 1
            );
            $model->setNovoStatusOS($datastatus);
        } else {
            $model->updOS($data);
        }    
        header('Location: /os/solicitar/idOS/' . $id);                
    }
    
    public function verhoras() {
        
        $dtInicio = substr($_POST['dtInicio'],6,4) . '-' . substr($_POST['dtInicio'],3,2) . '-' . substr($_POST['dtInicio'],0,2);
        $dtFim = substr($_POST['dtFim'],6,4) . '-' . substr($_POST['dtFim'],3,2) . '-' . substr($_POST['dtFim'],0,2);
        
        $valores = $this->calculaDatas($dtInicio, $dtFim);
        
        $this->smarty->assign('lista_reduzida', $valores['statusreduzido']);
        //print_a_die($valores['periodo']);
        $this->smarty->assign('lista_status', $valores['periodo']);
        $html = $this->smarty->fetch("os/osanalitico.html");
        $jasonretorno = array(
            'html' => $html
        );
        echo json_encode($jasonretorno);                
    }

    public function calculaDatas ($datainicial, $datafinal) {    
        
        $modelTipoAgenda = new tipoagendaModel();
        $cabec = array();
        $model = new agendahorarioModel();
        $where = "ai.dtAgenda >= '" . $datainicial . "' and ai.dtAgenda <= '" . $datafinal . "'";
        $lerperiodo = $model->getAgendaHorarioOS($where);
        $i=0;
        foreach ($lerperiodo as $value) {
            $ok = $modelTipoAgenda->getTipoAgenda();
            foreach($ok as $valueTipoAgenda) {
                $where = "ai.dtAgenda >= '" . $datainicial . "' and ai.dtAgenda <= '" . $datafinal . "' and ac.idColaborador = " . $value['idColaborador'] . ' and ac.idTipoAgenda = ' . $valueTipoAgenda['idTipoAgenda'];
                $dias = $model->getAgendaHorarioOSSoma($where);
                if ($dias) {
                   $lerperiodo[$i]['tipoagenda'][$dias[0]['idTipoAgenda']]['idTipoAgenda'] = $dias[0]['idTipoAgenda'];
                   $lerperiodo[$i]['tipoagenda'][$dias[0]['idTipoAgenda']]['dsTipoAgenda'] = $dias[0]['dsTipoAgenda'];
                   $lerperiodo[$i]['tipoagenda'][$dias[0]['idTipoAgenda']]['dsCor'] = $dias[0]['dsCor'];
                   $lerperiodo[$i]['tipoagenda'][$dias[0]['idTipoAgenda']]['dsResumida'] = $dias[0]['dsResumida'];
                   $lerperiodo[$i]['tipoagenda'][$dias[0]['idTipoAgenda']]['qtDias'] = $dias[0]['qtosta'];
                   $lerperiodo[$i]['tipoagenda'][$dias[0]['idTipoAgenda']]['qtHoras'] = $dias[0]['totalHoras'];
                } else {
                   $lerperiodo[$i]['tipoagenda'][$valueTipoAgenda['idTipoAgenda']]['idTipoAgenda'] = $valueTipoAgenda['idTipoAgenda'];
                   $lerperiodo[$i]['tipoagenda'][$valueTipoAgenda['idTipoAgenda']]['dsTipoAgenda'] = $valueTipoAgenda['dsTipoAgenda'];
                   $lerperiodo[$i]['tipoagenda'][$valueTipoAgenda['idTipoAgenda']]['dsCor'] = '';
                   $lerperiodo[$i]['tipoagenda'][$valueTipoAgenda['idTipoAgenda']]['dsResumida'] = '';
                   $lerperiodo[$i]['tipoagenda'][$valueTipoAgenda['idTipoAgenda']]['qtDias'] = 0;
                   $lerperiodo[$i]['tipoagenda'][$valueTipoAgenda['idTipoAgenda']]['qtHoras'] = 0;                    
                }
            }
            $i++;
        }
        $cabec['periodo'] = $lerperiodo;
        $ok = $modelTipoAgenda->getTipoAgenda();
        $cabec['statusreduzido'] = $ok;
        return $cabec;
    }
    
    //Trata dados antes de Enviar para o Gravar
    private function trataPost($post) {
        $data = array();
        $data['idOS'] = ($post['idOS'] != '') ? $post['idOS'] : null;
        $data['idColaboradorSolicitante'] = ($post['idColaboradorSolicitante'] != '') ? $post['idColaboradorSolicitante'] : null;
        $data['idColaboradorResponsavel'] = ($post['idColaboradorResponsavel'] != '') ? $post['idColaboradorResponsavel'] : null;
        $data['dsProblema'] = ($post['dsProblema'] != '') ? $post['dsProblema'] : null;
        $data['idSetor'] = ($post['idSetor'] != '') ? $post['idSetor'] : null;
        $data['idCentroCusto'] = ($post['idCentroCusto'] != '') ? $post['idCentroCusto'] : null;
        $data['idOSGrupo'] = ($post['idOSGrupo'] != '') ? $post['idOSGrupo'] : null;
        $data['idSetorSolicitante'] = ($post['idSetorSolicitante'] != '') ? $post['idSetorSolicitante'] : null;
        $data['idTipoManutencao'] = ($post['idTipoManutencao'] != '') ? $post['idTipoManutencao'] : null;
        $data['idTipoFalha'] = ($post['idTipoFalha'] != '') ? $post['idTipoFalha'] : null;
        $data['dtOS'] = ($post['dtOS'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $post["dtOS"]))) : date('Y-m-d h:m:s');
        $data['dtInicio'] = ($post['dtInicio'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $post["dtInicio"]))) : date('Y-m-d h:m:s');
        $data['dtFim'] = ($post['dtFim'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $post["dtFim"]))) : date('Y-m-d h:m:s');
        $data['nrOS'] = ($post['nrOS'] != '') ? $post['nrOS'] : null;
        $data['idStatusOS'] = 1;
        $data['idUsuario'] = $_SESSION['user']['usuario'];
        return $data;
    }

    private function trataPostSolicitacao($post) {
        $data = array();
        $data['idOS'] = ($post['idOS'] != '') ? $post['idOS'] : null;
        $data['dsProblema'] = ($post['dsProblema'] != '') ? $post['dsProblema'] : null;
        $data['idSetor'] = ($post['idSetor'] != '') ? $post['idSetor'] : null;
        $data['idCentroCusto'] = ($post['idCentroCusto'] != '') ? $post['idCentroCusto'] : null;
        $data['dtOS'] = ($post['dtOS'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $post["dtOS"]))) : date('Y-m-d h:m:s');
        $data['idStatusOS'] = 1;
        $data['idUsuario'] = $_SESSION['user']['usuario'];
        return $data;
    }

    public function editarColaborador() {
        $dtInicio = substr($_POST['dtInicio'],6,4) . '-' . substr($_POST['dtInicio'],3,2) . '-' . substr($_POST['dtInicio'],0,2);
        $dtFim = substr($_POST['dtFim'],6,4) . '-' . substr($_POST['dtFim'],3,2) . '-' . substr($_POST['dtFim'],0,2);
        $idColaborador = $_POST['idColaborador'];
        $idOS = $_POST['idOS'];
        
        $dados = array(
            'dtInicio' => $dtInicio,
            'dtFim' => $dtFim,
            'idColaborador' => $idColaborador,
            'idOS' => $idOS
        );
        $model = new osModel();
        $model->setOSColaborador($dados);        
        $jasonretorno = array(
            'html' => null
        );
        echo json_encode($jasonretorno);                
    }
    
    public function delOS() {     
        $sy = new system\System();
        
        $idOS = $sy->getParam('idOS');
        $model = new osModel();
        $where = 'idOS = ' . $idOS;             
        $model->delOS($where);
        
        $jasonretorno = array(
            'html' => null
        );
        echo json_encode($jasonretorno);                
    }    
    
    public function delOSColaborador() {
        $dtInicio = substr($_POST['dtInicio'],6,4) . '-' . substr($_POST['dtInicio'],3,2) . '-' . substr($_POST['dtInicio'],0,2);
        $dtFim = substr($_POST['dtFim'],6,4) . '-' . substr($_POST['dtFim'],3,2) . '-' . substr($_POST['dtFim'],0,2);
        $idColaborador = $_POST['idColaborador'];
        $idOS = $_POST['idOS'];
        $model = new osModel();
        $where = 'idOS = ' . $idOS . ' and idColaborador = ' . $idColaborador;  
        
        $model->delOSColaborador($where);
        $colaboradoresos = $model->getOSColaboradores('a.stStatus = 1', $dtInicio, $dtFim);   
        $this->smarty->assign('os_listacolabos', $colaboradoresos);
        $html = $this->smarty->fetch("os/oscolabanalitico.html");
        $jasonretorno = array(
            'html' => $html
        );
        echo json_encode($jasonretorno);                
    }    
}

?>