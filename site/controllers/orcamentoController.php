<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class orcamento extends controller {

    public function index_action() {
//die("chegou");
        //Inicializa o Template
        $this->template->run();

        $model = new orcamentoModel();
        $orcamento_lista = $model->getOrcamento(null,$paginacao=true);

        $this->smarty->assign('orcamento_lista', $orcamento_lista);
        $this->smarty->display('orcamento/lista.html');
    }

//Funcao de Busca
    public function busca_orcamento() {
        //se nao existir o indice estou como padrao '';
        $texto = isset($_POST['buscadescricao']) ? $_POST['buscadescricao'] : '';
        //$texto = '';
        $model = new orcamentoModel();
        $sql = "stStatus <> 0 and upper(dsOrcamento) like upper('%" . $texto . "%')"; //somente os nao excluidos
        $resultado = $model->getOrcamento($sql, $paginacao=true);

        if (sizeof($resultado) > 0) {
            $this->smarty->assign('orcamento_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'orcamento');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('orcamento/lista.html');
        } else {
            $this->smarty->assign('orcamento_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'orcamento');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('orcamento/lista.html');
        }
    }

    //Funcao de Inserir
    public function novo_orcamento() {
        $sy = new system\System();
        $idOrcamento = $sy->getParam('idOrcamento');

        $model = new orcamentoModel();

        if ($idOrcamento > 0) {
            $registro = $model->getOrcamento('o.idOrcamento=' . $idOrcamento);
            $registro = $registro[0]; //Passando Orcamento
        } else {
            //Novo Registro
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        
        $lista_cc = $this->carrega_cc($idOrcamento);
        $this->smarty->assign('lista_cc',$lista_cc);
        $lista_colaboradores = $this->carrega_colaborador($idOrcamento);
        $this->smarty->assign('lista_colaboradores',$lista_colaboradores);
        
        $lista_orcamentocc = null;
        $lista_orcamentocolaborador = null;
        
        if(!$lista_orcamentocc) {
            $lista_orcamentocc = null;
        }
        if(!$lista_orcamentocolaborador) {
            $lista_orcamentocolaborador = null;
        }
        $totalDespesas = 0;
        $totalReceitas = 0;
        if ($idOrcamento) {
            $where = 'oc.idOrcamento = ' . $idOrcamento;
            $lista_orcamentocolaborador = $model->getOrcamentoColaboradores($where);   
            $x=0;
            foreach ($lista_orcamentocolaborador as $value) {
                $groupby = 'occ.idOrcamento,occ.idColaborador';

                $where = "gd.stGrupoDR = 'D' and occ.idOrcamento = " . $value['idOrcamento'] . " and occ.idColaborador = " . $value['idColaborador'];
                $totais = $model->getTotais($where, $groupby);
                if ($totais) {
                    $lista_orcamentocolaborador[$x]['vlDespesas'] = $totais[0]['vlTotal'];
                } else {
                    $lista_orcamentocolaborador[$x]['vlDespesas'] = 0;
                }

                $where = "gd.stGrupoDR = 'R' and occ.idOrcamento = " . $value['idOrcamento'] . " and occ.idColaborador = " . $value['idColaborador'];
                $totais = $model->getTotais($where, $groupby);
                if ($totais) {
                    $lista_orcamentocolaborador[$x]['vlReceitas'] = $totais[0]['vlTotal'];
                } else {
                    $lista_orcamentocolaborador[$x]['vlReceitas'] = 0;
                }
                $totalDespesas = $totalDespesas + $lista_orcamentocolaborador[$x]['vlDespesas'];
                $totalReceitas = $totalReceitas + $lista_orcamentocolaborador[$x]['vlReceitas'];
                $x++;
            }
            $registro['vlSaldoFinal'] = $registro['vlSaldoInicial'] + ($totalReceitas - $totalDespesas);
        } else {
            $registro['vlSaldoFinal'] = 0;
        }
        $this->smarty->assign('lista_orcamentocolaborador', $lista_orcamentocolaborador);

        $this->smarty->assign('lista_orcamentocc', $lista_orcamentocc);
        $this->smarty->assign('idOrcamentoColaborador', null);
        
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('totalorcamento', ($totalReceitas - $totalDespesas));
        
        $this->smarty->assign('title', 'Novo Orcamento');
        $this->smarty->display('orcamento/form_novo.tpl');
    }

    public function finalizarDigitacaoCC() {
        $idOrcamento = $_POST['idOrcamento'];
        $idCentroCusto = $_POST['idCentroCusto'];
        $encerra = $_POST['encerra'];
        if ($encerra == 'sim') {
            $encerrar = 1;
        } else {
            $encerrar = 0;
        }
        $model = new orcamentoModel();
        $where = 'idOrcamento = ' . $idOrcamento . ' and idCentroCusto = ' . $idCentroCusto;
        $data = array('stSituacao' => $encerrar);
        $model->updOrcamentoColaboradorCC($data, $where);
        echo json_encode(array());
        
    }
    // Gravar Padrao
    public function gravar_orcamento() {
        $model = new orcamentoModel();

        $data = $this->trataPost($_POST);        

        if ($data['idOrcamento'] == null) {
            $id=$model->setorcamento($data);
        } else {
            $id=$data['idOrcamento'];
            $model->updorcamento($data); //update
        }    
        
        header('Location: /orcamento/novo_orcamento/idOrcamento/' . $id);
    }
    public function ler_colaborador() {
        $sy = new system\System();
        $idColaborador = $_POST['idColaborador'];
        $modelColaborador = new colaboradorModel();
        $where = 'a.idColaborador = ' . $idColaborador;
        $retorno = $modelColaborador->getColaborador($where);
        $jsondata = array();
        if ($retorno) {
            $jsondata["dsSetor"] = $retorno[0]['dsSetor'];
            $jsondata["dsCargo"] = $retorno[0]['dsCargo'];
            $jsondata["dsEmail"] = $retorno[0]['dsEmail'];
        }
        echo json_encode($jsondata);
    }
    public function delcolaborador() {
        $sy = new system\System();
        // recebe as variaveis do js
        $idOrcamento = $_POST['idOrcamento'];
        $idOrcamentoColaborador = $_POST['idOrcamentoColaborador'];
        // exclui o colaborador do orcamento
        $modelColaborador = new orcamentoModel();
        $where = 'idOrcamentoColaborador = ' . $idOrcamentoColaborador;
        $modelColaborador->delOrcamentoColaborador($where);
        // carrega o html com a lista de colaboradores por orcamento
        $where = 'oc.idOrcamento = ' . $idOrcamento;
        $model = new orcamentoModel();        
        $lista_orcamentoc = $model->getOrcamentoColaboradores($where);        
        $x=0;
        foreach ($lista_orcamentoc as $value) {
            $groupby = 'occ.idOrcamento,occ.idColaborador';

            $where = "gd.stGrupoDR = 'D' and occ.idOrcamento = " . $value['idOrcamento'] . " and occ.idColaborador = " . $value['idColaborador'];
            $totais = $model->getTotais($where, $groupby);
            if ($totais) {
                $lista_orcamentoc[$x]['vlDespesas'] = $totais[0]['vlTotal'];
            } else {
                $lista_orcamentoc[$x]['vlDespesas'] = 0;
            }

            $where = "gd.stGrupoDR = 'R' and occ.idOrcamento = " . $value['idOrcamento'] . " and occ.idColaborador = " . $value['idColaborador'];
            $totais = $model->getTotais($where, $groupby);
            if ($totais) {
                $lista_orcamentoc[$x]['vlReceitas'] = $totais[0]['vlTotal'];
            } else {
                $lista_orcamentoc[$x]['vlReceitas'] = 0;
            }
            $x++;
        }

        $lista_colaboradores = $this->carrega_colaborador($idOrcamento);
        $this->smarty->assign('lista_colaboradores',$lista_colaboradores);
        $html1 = $this->smarty->fetch("orcamento/form_listacolaborador.tpl");
        
        $this->smarty->assign('lista_orcamentocolaborador', $lista_orcamentoc);    
        $html = $this->smarty->fetch("orcamento/orcamentocolaborador.html");
        $jsondata = array(
            'html' => $html,
            'html1' => $html1
        );
        echo json_encode($jsondata);
    }
    public function delcentrocusto() {
        $sy = new system\System();
        // recebe as variaveis do js
        $idOrcamento = $_POST['idOrcamento'];
        $idColaborador = $_POST['idColaborador'];
        $idOrcamentoColaboradorCC = $_POST['idOrcamentoColaboradorCC'];
        // exclui o colaborador do orcamento
        $model = new orcamentoModel();
        $where = 'idOrcamentoColaboradorCC = ' . $idOrcamentoColaboradorCC;
        $model->delOrcamentoColaboradorCC($where);
        // carrega o html com a lista de centro de custo por orcamento/colaborador
        $where = 'occ.idOrcamento = ' . $idOrcamento . ' and occ.idColaborador = ' . $idColaborador;
        $lista_orcamentocc = $model->getOrcamentoColaboradoresCC($where);        
        $x=0;
        foreach ($lista_orcamentocc as $value) {
            $groupby = 'occ.idOrcamento, occ.idColaborador, occ.idCentroCusto';

            $where = "gd.stGrupoDR = 'D' and occ.idOrcamento = " . $value['idOrcamento'] . " and occ.idColaborador = " . $value['idColaborador']  . " and occ.idCentroCusto = " . $value['idCentroCusto'];
            $totais = $model->getTotais($where, $groupby);
            if ($totais) {
                $lista_orcamentocc[$x]['vlDespesas'] = $totais[0]['vlTotal'];
            } else {
                $lista_orcamentocc[$x]['vlDespesas'] = 0;
            }

            $where = "gd.stGrupoDR = 'R' and occ.idOrcamento = " . $value['idOrcamento'] . " and occ.idColaborador = " . $value['idColaborador']  . " and occ.idCentroCusto = " . $value['idCentroCusto'];
            $totais = $model->getTotais($where, $groupby);
            if ($totais) {
                $lista_orcamentocc[$x]['vlReceitas'] = $totais[0]['vlTotal'];
            } else {
                $lista_orcamentocc[$x]['vlReceitas'] = 0;
            }
            $x++;
        }

        $lista_cc = $this->carrega_cc($idOrcamento);
        $this->smarty->assign('lista_cc',$lista_cc);
        $html1 = $this->smarty->fetch("orcamento/form_listacc.tpl");

        $this->smarty->assign('lista_orcamentocc', $lista_orcamentocc);    
        $html = $this->smarty->fetch("orcamento/orcamentocentrocusto.html");
        $jsondata = array(
            'html' => $html,
            'html1' => $html1
        );
        echo json_encode($jsondata);
    }

    public function verorcamento() {
        $dtInicioVigencia = $_POST['dtInicioVigencia'];
        $dtFimVigencia = $_POST['dtFimVigencia'];
        $stSituacao = $_POST['stSituacao'];
        $idOrcamento = $_POST['idOrcamento'];
        $idCentroCusto = $_POST['idCentroCusto'];
        $idColaborador = $_POST['idColaborador'];
        $data = $dtInicioVigencia;
        $dataI = substr($dtInicioVigencia, 6,4) . substr($dtInicioVigencia, 3,2);
        $dataF = substr($dtFimVigencia, 6,4) . substr($dtFimVigencia, 3,2);
        
        $qtde = ($dataF - $dataI)+1;
        
        $meses = array();
        for ($x=1;$x<=$qtde;$x++) {
            $meses[$x]['meses'] = date('m/Y',strtotime($data));
            $meses[$x]['idmesano'] = date('Ym',strtotime($data));
            $data = date('Y-m-d', strtotime('+1 months', strtotime($data))) . ' 00:00:00';
        }
        $idOrcamentoColaboradorCC = $_POST['idOrcamentoColaboradorCC'];
        $model = new orcamentoModel();
        $where = 'gd.stOrcamento = 1 and l.idOrcamentoColaboradorCC = ' . $idOrcamentoColaboradorCC;
        $listaGI = $model->getOrcamentoGRIT($where);        
        $lista = array();
        foreach ($listaGI as $value) {
            $x=0;
            foreach ($meses as $value1) {
                $where = 'gd.stOrcamento = 1 and l.idOrcamentoColaboradorCC = ' . $idOrcamentoColaboradorCC . ' and l.idItemDR = ' . $value['idItemDR'] . ' and CONCAT(substr(dtValorOrcado,1,4),substr(dtValorOrcado,6,2)) = ' . $value1['idmesano'];
                $ret =  $model->getOrcamentoValores($where);    
                if (!$ret) {
                   $lista[$value['idItemDR']][$x]['idmesano'] = $value1['idmesano'];
                   $lista[$value['idItemDR']][$x]['dsGrupoDR'] = '';
                   $lista[$value['idItemDR']][$x]['dsItemDR'] = '';
                   $lista[$value['idItemDR']][$x]['valormes'] = 0;
                   $lista[$value['idItemDR']][$x]['mesano'] = '';
                   $lista[$value['idItemDR']][$x]['idItemDR'] = $value['idItemDR'];
                } else {
                   $ret = $ret[0];
                   $lista[$value['idItemDR']][$x]['idmesano'] = $ret['idmesano'];
                   $lista[$value['idItemDR']][$x]['dsGrupoDR'] = $ret['dsGrupoDR'];
                   $lista[$value['idItemDR']][$x]['dsItemDR'] = $ret['dsItemDR'];
                   $lista[$value['idItemDR']][$x]['valormes'] = $ret['valormes'];
                   $lista[$value['idItemDR']][$x]['mesano'] = $ret['mesano'];
                   $lista[$value['idItemDR']][$x]['idItemDR'] = $ret['idItemDR'];
                }
                $x++;
            }
        }
        
        // pegar os valores
        $where = 'ccc.idOrcamento = ' . $idOrcamento . ' and ccc.idColaborador = ' . $idColaborador . ' and ccc.idCentroCusto = ' . $idCentroCusto;
        $valores = $model->getOrcamentoCC($where);        
        if ($valores) {
            $vlLimiteReceitas = $valores[0]['vlLimiteReceitas'];
            $vlLimiteDespesas = $valores[0]['vlLimiteDespesas'];
            $dsCentroCusto = $valores[0]['cdCentroCusto'] . '-' . $valores[0]['dsCentroCusto'];
        }
        // pegar o total das despesas por centro custo
        $where = "gd.stOrcamento = 1 and occ.idOrcamento = " . $idOrcamento . " and occ.idColaborador = " . $idColaborador . " and occ.idCentroCusto = " . $idCentroCusto  . " and gd.stGrupoDR = 'D'";
        $valores = $model->getTotais($where);        
        if ($valores) {
            $vlTotalDespesas = $valores[0]['vlTotal'];
        }
        // pegar o total das receitas por centro custo
        $where = "gd.stOrcamento = 1 and occ.idOrcamento = " . $idOrcamento . " and occ.idColaborador = " . $idColaborador . " and occ.idCentroCusto = " . $idCentroCusto  . " and gd.stGrupoDR = 'R'";
        $valores = $model->getTotais($where);        
        if ($valores) {
            $vlTotalReceitas = $valores[0]['vlTotal'];
        }
        
//        $this->smarty->assign('CentroCusto', $dsCentroCusto);
        $this->smarty->assign('listaGI', $listaGI);
        $this->smarty->assign('idOrcamentoColaboradorCC', $idOrcamentoColaboradorCC);
        $this->smarty->assign('orcamentolista', $meses);
        $this->smarty->assign('pedidoitens', $lista);
        $this->smarty->assign('stSituacao', $stSituacao);
        $this->smarty->assign('idOrcamento', $idOrcamento);
        $this->smarty->assign('idColaborador', $idColaborador);
        $this->smarty->assign('vlLimiteReceitas', $vlLimiteReceitas);
        $this->smarty->assign('vlLimiteDespesas', $vlLimiteDespesas);
        $this->smarty->assign('vlTotalReceitas', $vlTotalReceitas);
        $this->smarty->assign('vlTotalDespesas', $vlTotalDespesas);
        $this->smarty->display("orcamento/verorcamento/thumbnail.tpl");
    }
    
    public function verorcamentoG() {
        $idOrcamento = $_POST['idOrcamento'];
        
        $model = new orcamentoModel();
        $where = 'o.idOrcamento = ' . $idOrcamento;
        $retorno = $model->getOrcamento($where)[0];        
        $lista = array();

        $dtInicioVigencia = $retorno['dtInicioVigencia'];
        $dtFimVigencia = $retorno['dtFimVigencia'];
        $saldoinicial = $retorno['vlSaldoInicial'];
        $data = $dtInicioVigencia;
        $dataI = substr($dtInicioVigencia, 0,4) . substr($dtInicioVigencia, 5,2);
        $dataF = substr($dtFimVigencia, 0,4) . substr($dtFimVigencia, 5,2);
        
        $qtde = ($dataF - $dataI)+1;
        
        $meses = array();
        for ($x=1;$x<=$qtde;$x++) {
            $meses[$x]['meses'] = date('m/Y',strtotime($data));
            $meses[$x]['idmesano'] = date('Ym',strtotime($data));
            $data = date('Y-m-d', strtotime('+1 months', strtotime($data))) . ' 00:00:00';
        }
        $where = 'occ.idOrcamento = ' . $idOrcamento;
        $listaGI = $model->getOrcamentoGRIT($where);        

        foreach ($listaGI as $value) {
            $x=0;
            foreach ($meses as $value1) {
                $where = 'occ.idOrcamento = ' . $idOrcamento . ' and l.idItemDR = ' . $value['idItemDR'] . ' and CONCAT(substr(dtValorOrcado,1,4),substr(dtValorOrcado,6,2)) = ' . $value1['idmesano'];
                $ret =  $model->getOrcamentoValores($where);    
                if (!$ret) {
                   $lista[$value['idItemDR']][$x]['idmesano'] = $value1['idmesano'];
                   $lista[$value['idItemDR']][$x]['dsGrupoDR'] = '';
                   $lista[$value['idItemDR']][$x]['dsItemDR'] = '';
                   $lista[$value['idItemDR']][$x]['valormes'] = 0;
                   $lista[$value['idItemDR']][$x]['mesano'] = '';
                   $lista[$value['idItemDR']][$x]['idItemDR'] = $value['idItemDR'];
                } else {
                   $ret = $ret[0];
                   $lista[$value['idItemDR']][$x]['idmesano'] = $ret['idmesano'];
                   $lista[$value['idItemDR']][$x]['dsGrupoDR'] = $ret['dsGrupoDR'];
                   $lista[$value['idItemDR']][$x]['dsItemDR'] = $ret['dsItemDR'];
                   $lista[$value['idItemDR']][$x]['valormes'] = $ret['valormes'];
                   $lista[$value['idItemDR']][$x]['mesano'] = $ret['mesano'];
                   $lista[$value['idItemDR']][$x]['idItemDR'] = $ret['idItemDR'];
                }
                $x++;
            }
        }
        
        // pegar os valores
        $where = 'ccc.idOrcamento = ' . $idOrcamento ;
        $valores = $model->getOrcamentoCCSoma($where);        
        if ($valores) {
            $vlLimiteReceitas = $valores[0]['valorlimitereceitas'];
            $vlLimiteDespesas = $valores[0]['valorlimitedespesas'];
            $dsCentroCusto = '';
        }
        // pegar o total das despesas por centro custo
        $where = "occ.idOrcamento = " . $idOrcamento . " and gd.stGrupoDR = 'D'";
        $valores = $model->getTotaisSoma($where);        
        if ($valores) {
            $vlTotalDespesas = $valores[0]['vlTotal'];
        }
        // pegar o total das receitas por centro custo
        $where = "occ.idOrcamento = " . $idOrcamento . " and gd.stGrupoDR = 'R'";
        $valores = $model->getTotaisSoma($where);        
        if ($valores) {
            $vlTotalReceitas = $valores[0]['vlTotal'];
        }
        
//        $this->smarty->assign('CentroCusto', $dsCentroCusto);
        $this->smarty->assign('listaGI', $listaGI);
        $this->smarty->assign('idOrcamentoColaboradorCC', '');
        $this->smarty->assign('orcamentolista', $meses);
        $this->smarty->assign('pedidoitens', $lista);
        $this->smarty->assign('stSituacao', '');
        $this->smarty->assign('idOrcamento', $idOrcamento);
        $this->smarty->assign('idColaborador', '');
        $this->smarty->assign('vlLimiteReceitas', $vlLimiteReceitas);
        $this->smarty->assign('vlLimiteDespesas', $vlLimiteDespesas);
        $this->smarty->assign('vlTotalReceitas', $vlTotalReceitas);
        $this->smarty->assign('vlTotalDespesas', $vlTotalDespesas);
        $this->smarty->assign('saldoinicial', $saldoinicial);
        $this->smarty->display("orcamento/verorcamento/thumbnail.tpl");
    }
    
    public function aprovarOrcamento() {
        $idOrcamento = $_POST['idOrcamento'];
        $model = new orcamentoModel();
        
        $ret = $model->getOrcamento('o.idOrcamento = ' . $idOrcamento);
        if ($ret[0]['idStatusOrcamento'] == 3) {
            $idStatusOrcamento = 2;
        } else {
            $idStatusOrcamento = 3;
        }
        $data = array(
            'idOrcamento' => $idOrcamento,
            'idStatusOrcamento' => $idStatusOrcamento
            );
        $model->updOrcamento($data);
        $jsondata = array();
        echo json_encode($jsondata);                
    }
    
    public function realizar() {
        $model = new orcamentoModel();
        $orcamento_lista = $model->getOrcamentoLancar($_SESSION['user']['usuario']);
        $this->smarty->assign('orcamento_lista', $orcamento_lista);
        $this->smarty->display('orcamento/lancardespesasrealizado.html');        
    }

    public function lancardespesasrealizadas() {
        $sy = new system\System();
        $idOrcamento = $sy->getParam('idOrcamento');

        $model = new orcamentoModel();

        if ($idOrcamento > 0) {
            $registro = $model->getOrcamento('idOrcamento=' . $idOrcamento);
            $registro = $registro[0]; //Passando Orcamento
        } else {
            //Novo Registro
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        
        $lista_cc = $this->carrega_cc($idOrcamento, $idColaborador);
        $this->smarty->assign('lista_cc',$lista_cc);
        $lista_grupodr = $this->carrega_grupodr();
        $this->smarty->assign('lista_grupodr',$lista_grupodr);
//        $lista_itemdr = $this->carrega_itemdr();
        $this->smarty->assign('lista_itemdr',null);
        
        $lista_orcamentocc = null;
        $lista_orcamentocolaborador = null;
        
        $totalorcamento = null;

        if(!$lista_orcamentocc) {
            $lista_orcamentocc = null;
        }
        if(!$lista_orcamentocolaborador) {
            $lista_orcamentocolaborador = null;
        }
        if ($idOrcamento) {
            $where = 'oc.idOrcamento = ' . $idOrcamento;
            $lista_orcamentocolaborador = $model->getOrcamentoColaboradores($where);        
        }
        $this->smarty->assign('lista_orcamentocolaborador', $lista_orcamentocolaborador);

        $this->smarty->assign('lista_orcamentocc', $lista_orcamentocc);
        $this->smarty->assign('idColaborador', $idColaborador);
        
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('totalorcamento', $totalorcamento);
        $this->smarty->assign('title', 'Novo Orcamento');
        $this->smarty->display('orcamento/form_orcar.tpl');
    }
    
    public function aprovar() {
        $idOrcamentoColaboradorCC = $_POST['idOrcamentoColaboradorCC'];
        $stSituacao = $_POST['stSituacao'];
        $idOrcamento = $_POST['idOrcamento'];
        $idColaborador = $_POST['idColaborador'];
        if ($stSituacao == 1) {
            $stSituacao = 2;
        } else {
            $stSituacao = 1;
        }
        $data = array(
          'stSituacao' => $stSituacao  
        );
        $model = new orcamentoModel();
        $model->updOrcamentoCC($data, $idOrcamentoColaboradorCC);
        
        $where = 'occ.idOrcamento = ' . $idOrcamento . ' and occ.idColaborador = ' . $idColaborador;
        $lista_orcamentocc = $model->getOrcamentoColaboradoresCC($where);        
        $x=0;
        foreach ($lista_orcamentocc as $value) {
            $groupby = 'occ.idOrcamento, occ.idColaborador, occ.idCentroCusto';

            $where = "gd.stOrcamento = 1 and gd.stGrupoDR = 'D' and occ.idOrcamento = " . $value['idOrcamento'] . " and occ.idColaborador = " . $value['idColaborador']  . " and occ.idCentroCusto = " . $value['idCentroCusto'];
            $totais = $model->getTotais($where, $groupby);
            if ($totais) {
                $lista_orcamentocc[$x]['vlDespesas'] = $totais[0]['vlTotal'];
            } else {
                $lista_orcamentocc[$x]['vlDespesas'] = 0;
            }

            $where = "gd.stOrcamento = 1 and gd.stGrupoDR = 'R' and occ.idOrcamento = " . $value['idOrcamento'] . " and occ.idColaborador = " . $value['idColaborador']  . " and occ.idCentroCusto = " . $value['idCentroCusto'];
            $totais = $model->getTotais($where, $groupby);
            if ($totais) {
                $lista_orcamentocc[$x]['vlReceitas'] = $totais[0]['vlTotal'];
            } else {
                $lista_orcamentocc[$x]['vlReceitas'] = 0;
            }
            $x++;
        }

        $this->smarty->assign('lista_orcamentocc', $lista_orcamentocc);    
        $html = $this->smarty->fetch("orcamento/orcamentocentrocusto.html");
        
        $jsondata = array('html' => $html);
        echo json_encode($jsondata);        
    }
    
    public function gravarvalorescc() {
        $idOrcamentoColaboradorCC = $_POST['idOrcamentoColaboradorCC'];
        $stSituacao = $_POST['stSituacao'];
        $idOrcamento = $_POST['idOrcamento'];
        $idColaborador = $_POST['idColaborador'];
        $vlLimiteDespesas = ($_POST['vlLimiteDespesas'] != '') ? str_replace(",",".",str_replace(".","",$_POST['vlLimiteDespesas'])) : null;
        $vlLimiteReceitas = ($_POST['vlLimiteReceitas'] != '') ? str_replace(",",".",str_replace(".","",$_POST['vlLimiteReceitas'])) : null;
        $data = array(
          'vlLimiteReceitas' => $vlLimiteReceitas,  
          'vlLimiteDespesas' => $vlLimiteDespesas
        );
        $model = new orcamentoModel();
        $model->updOrcamentoCC($data, $idOrcamentoColaboradorCC);
        $jasonretorno = array(
        );
        echo json_encode($jasonretorno);                
    }
    
    public function graficocc () {
        $idOrcamentoColaboradorCC = $_POST['idOrcamentoColaboradorCC'];
        $idOrcamento = $_POST['idOrcamento'];
        $origem = $_POST['origem'];
        if ($origem == 'centrocusto') {
            $groupby = 'occ.idOrcamento, occ.idColaborador, occ.idCentroCusto, gd.idGrupoDR';
        } else {
        if ($origem == 'colaborador') {
            $groupby = 'occ.idOrcamento, occ.idColaborador, gd.idGrupoDR';
        } else { // origem = orcamento
            $groupby = 'occ.idOrcamento, gd.idGrupoDR';
        }}
        
        $model = new orcamentoModel();
        if ($origem == 'centrocusto') {
            $where = "occ.idOrcamento = " . $idOrcamento . " and gd.stGrupoDR = 'R' and l.idOrcamentoColaboradorCC = " . $idOrcamentoColaboradorCC;
        } else {
        if ($origem == 'colaborador') {
            $where = "occ.idOrcamento = " . $idOrcamento . " and gd.stGrupoDR = 'R' and occ.idColaborador = " . $idOrcamentoColaboradorCC;
        } else { // origem = orcamento
            $where = "occ.idOrcamento = " . $idOrcamento . " and gd.stGrupoDR = 'R'";
        }}
        $registro = $model->getTotais($where, $groupby);
        $retorno = array('');
        if ($registro) {
            $x=0;
            foreach($registro as $value) {
                $retorno['receitas'][$x][0] = $value['dsGrupoDR'];
                $retorno['receitas'][$x][1] = doubleval($value['vlTotal']);
                $x++;
            }
        }
        if ($origem == 'centrocusto') {
            $where = "occ.idOrcamento = " . $idOrcamento . " and gd.stGrupoDR = 'D' and l.idOrcamentoColaboradorCC = " . $idOrcamentoColaboradorCC;
        } else {
        if ($origem == 'colaborador') {
            $where = "occ.idOrcamento = " . $idOrcamento . " and gd.stGrupoDR = 'D' and occ.idColaborador = " . $idOrcamentoColaboradorCC;
        } else {
            $where = "occ.idOrcamento = " . $idOrcamento . " and gd.stGrupoDR = 'D'";
        }}
        $registro = $model->getTotais($where, $groupby);
        if ($registro) {
            $x=0;
            foreach($registro as $value) {
                $retorno['despesas'][$x][0] = $value['dsGrupoDR'];
                $retorno['despesas'][$x][1] = doubleval($value['vlTotal']);
                $x++;
            }
        }
        if (!isset($retorno['receitas'])) {
            $retorno['receitas'][0][0] = 'SEM RECEITAS ORCADAS';
            $retorno['receitas'][0][1] = 100;
        }
        if (!isset($retorno['despesas'])) {
            $retorno['despesas'][0][0] = 'SEM DESPESAS ORCADAS';
            $retorno['despesas'][0][1] = 100;
        }

        // fim do grafico de pizza por totais de despesas e receitas
        
        // agora vamos fazer o grafico de linha mes a mes do periodo do orcamento por grupo de despesa e receita
        
        $dadosorcamento = $model->getOrcamento('o.idOrcamento = ' . $idOrcamento)[0];
        $dataInicial = $dadosorcamento['dtInicioVigencia'];
        $dataFinal = $dadosorcamento['dtFimVigencia'];
        $vlSaldoInicial = $dadosorcamento['vlSaldoInicial'];
        // quantos meses tem o orcamento
        $per = floor(strtotime($dataFinal) - strtotime($dataInicial));
        $meses = $per / (60 * 60 * 24 * 30);
        $meses = intval($meses);
        $periodo = $dataInicial;
        
        $dia= $dataInicial;
        $dia= date('Y-m-d', strtotime('+31 days',strtotime($dia)));

        // só receitas por grupo
        
        $modeldr = new grupodrModel();
        $despesasLista = $modeldr->getGrupoDR("stGrupoDR = 'R'","dsGrupoDR");
        $retorno['gruporeceitasmes'][0][0] = 'MES';
        $z=1;
        foreach($despesasLista as $value) {
            $retorno['gruporeceitasmes'][0][$z] = $value['dsGrupoDR'];
            $z++;
        }
        
        $w = 0;
        foreach ($retorno['gruporeceitasmes'][$w] as $grupodr) {            
    
            for ($y=1; $y<=$meses; $y++) {                       
                $dataI = substr($periodo,0,8) . '01 00:00:00';
                $dataF = date("Y-m-t",strtotime($periodo)) . " 23:59:59";
                // echo 'inicial: ' . $dataInicial . ' final : ' . $dataFinal . '<br>';
                if ($grupodr == 'MES') {
                    $retorno['gruporeceitasmes'][$y][$w] = substr(strftime("%B", strtotime($periodo)),0,3); //substr($periodo,5,2); // . '/' . substr($periodo,0,4);
                } else {                    

                    if ($origem == 'centrocusto') {
                        $groupby = "l.idOrcamentoColaboradorCC, gd.idGrupoDR, CONCAT(substr(dtValorOrcado,6,2),'/',substr(dtValorOrcado,1,4))";
                        $where = "occ.idOrcamento = " . $idOrcamento . " and gd.stOrcamento = 1 and gd.stGrupoDR = 'R' and l.idOrcamentoColaboradorCC = " . $idOrcamentoColaboradorCC;
                        $where .= " and l.dtValorOrcado >= '" . $dataI . "' and l.dtValorOrcado <= '" . $dataF . "'";
                        $where .= " and gd.dsGrupoDR = '" . $grupodr . "'";
                    } else {
                    if ($origem == 'colaborador') {
                        $groupby = "gd.idGrupoDR, CONCAT(substr(dtValorOrcado,6,2),'/',substr(dtValorOrcado,1,4))";
                        $where = "occ.idOrcamento = " . $idOrcamento . " and gd.stOrcamento = 1 and gd.stGrupoDR = 'R' and occ.idColaborador = " . $idOrcamentoColaboradorCC;
                        $where .= " and l.dtValorOrcado >= '" . $dataI . "' and l.dtValorOrcado <= '" . $dataF . "'";
                        $where .= " and gd.dsGrupoDR = '" . $grupodr . "'";
                    } else {
                        $groupby = "gd.idGrupoDR, CONCAT(substr(dtValorOrcado,6,2),'/',substr(dtValorOrcado,1,4))";
                        $where = "occ.idOrcamento = " . $idOrcamento . " and gd.stOrcamento = 1 and gd.stGrupoDR = 'R'";
                        $where .= " and l.dtValorOrcado >= '" . $dataI . "' and l.dtValorOrcado <= '" . $dataF . "'";
                        $where .= " and gd.dsGrupoDR = '" . $grupodr . "'";
                    }}
                    
                    $receitasmesames = $model->getOrcamentoGrupoValores($where, $groupby);
                    if ($receitasmesames) {
                        $retorno['gruporeceitasmes'][$y][$w] = doubleval($receitasmesames[0]['valormes']);
                    } else {
                        $retorno['gruporeceitasmes'][$y][$w] = 0;
                    }
               }
               $periodo = date('Y-m-d', strtotime('+31 days',strtotime($periodo)));
            }   
            $periodo = $dataInicial;  
            $w++;
        }

        // só despesas por grupo
        
        $modeldr = new grupodrModel();
        $despesasLista = $modeldr->getGrupoDR("stOrcamento = 1 and stGrupoDR = 'D'","dsGrupoDR");
        $retorno['grupodespesasmes'][0][0] = 'MES';
        $z=1;
        foreach($despesasLista as $value) {
            $retorno['grupodespesasmes'][0][$z] = $value['dsGrupoDR'];
            $z++;
        }
        
        $w = 0;
        foreach ($retorno['grupodespesasmes'][$w] as $grupodr) {            
    
            for ($y=1; $y<=$meses; $y++) {                       
                $dataI = substr($periodo,0,8) . '01 00:00:00';
                $dataF = date("Y-m-t",strtotime($periodo)) . " 23:59:59";
                // echo 'inicial: ' . $dataInicial . ' final : ' . $dataFinal . '<br>';
                if ($grupodr == 'MES') {
                    $retorno['grupodespesasmes'][$y][$w] = substr(strftime("%B", strtotime($periodo)),0,3); //substr($periodo,5,2); // . '/' . substr($periodo,0,4);
                } else {                    

                    if ($origem == 'centrocusto') {
                        $groupby = "l.idOrcamentoColaboradorCC, gd.idGrupoDR, CONCAT(substr(dtValorOrcado,6,2),'/',substr(dtValorOrcado,1,4))";
                        $where = "occ.idOrcamento = " . $idOrcamento . " and gd.stOrcamento = 1 and gd.stGrupoDR = 'D' and l.idOrcamentoColaboradorCC = " . $idOrcamentoColaboradorCC;
                        $where .= " and l.dtValorOrcado >= '" . $dataI . "' and l.dtValorOrcado <= '" . $dataF . "'";
                        $where .= " and gd.dsGrupoDR = '" . $grupodr . "'";
                    } else {
                    if ($origem == 'colaborador') {
                        $groupby = "gd.idGrupoDR, CONCAT(substr(dtValorOrcado,6,2),'/',substr(dtValorOrcado,1,4))";
                        $where = "occ.idOrcamento = " . $idOrcamento . " and gd.stOrcamento = 1 and gd.stGrupoDR = 'D' and occ.idColaborador = " . $idOrcamentoColaboradorCC;
                        $where .= " and l.dtValorOrcado >= '" . $dataI . "' and l.dtValorOrcado <= '" . $dataF . "'";
                        $where .= " and gd.dsGrupoDR = '" . $grupodr . "'";
                    } else {
                        $groupby = "gd.idGrupoDR, CONCAT(substr(dtValorOrcado,6,2),'/',substr(dtValorOrcado,1,4))";
                        $where = "occ.idOrcamento = " . $idOrcamento . " and gd.stOrcamento = 1 and gd.stGrupoDR = 'D'";
                        $where .= " and l.dtValorOrcado >= '" . $dataI . "' and l.dtValorOrcado <= '" . $dataF . "'";
                        $where .= " and gd.dsGrupoDR = '" . $grupodr . "'";
                    }}
                    
                    $receitasmesames = $model->getOrcamentoGrupoValores($where, $groupby);
                    if ($receitasmesames) {
                        $retorno['grupodespesasmes'][$y][$w] = doubleval($receitasmesames[0]['valormes']);
                    } else {
                        $retorno['grupodespesasmes'][$y][$w] = 0;
                    }
               }
               $periodo = date('Y-m-d', strtotime('+31 days',strtotime($periodo)));
            }   
            $periodo = $dataInicial;  
            $w++;
        }

        // DESPESAS / RECEITAS E ACUMULADO
        
        $periodo = $dataInicial;  
        
        $acumulado = array();
        $acumulado['acumulado'][0][0] = 'MES';
        $acumulado['acumulado'][0][1] = 'Receita bruta';
        $acumulado['acumulado'][0][2] = 'Despesas';
        $acumulado['acumulado'][0][3] = 'Receita liquida';
        $acumulado['acumulado'][0][4] = 'Acumulado';
        for ($y=1; $y<=$meses; $y++) {                       
            $dataI = substr($periodo,0,8) . '01 00:00:00';
            $dataF = date("Y-m-t",strtotime($periodo)) . " 23:59:59";
            $acumulado['acumulado'][$y][0] = substr(strftime("%B", strtotime($periodo)),0,3); // substr($periodo,5,2); // . '/' . substr($periodo,0,4);
            $periodo = date('Y-m-d', strtotime('+31 days',strtotime($periodo)));
        }
        
        // receitas
        
        $periodo = $dataInicial;  
        for ($y=1; $y<=$meses; $y++) {                       
            $dataI = substr($periodo,0,8) . '01 00:00:00';
            $dataF = date("Y-m-t",strtotime($periodo)) . " 23:59:59";

            if ($origem == 'centrocusto') {
                $groupby = "l.idOrcamentoColaboradorCC, CONCAT(substr(dtValorOrcado,6,2),'/',substr(dtValorOrcado,1,4))";
                $where = "occ.idOrcamento = " . $idOrcamento . " and gd.stOrcamento = 1 and gd.stGrupoDR = 'R' and l.idOrcamentoColaboradorCC = " . $idOrcamentoColaboradorCC;
                $where .= " and l.dtValorOrcado >= '" . $dataI . "' and l.dtValorOrcado <= '" . $dataF . "'";
            } else {
            if ($origem == 'colaborador') {
                $groupby = "CONCAT(substr(dtValorOrcado,6,2),'/',substr(dtValorOrcado,1,4))";
                $where = "occ.idOrcamento = " . $idOrcamento . " and gd.stOrcamento = 1 and gd.stGrupoDR = 'R' and occ.idColaborador = " . $idOrcamentoColaboradorCC;
                $where .= " and l.dtValorOrcado >= '" . $dataI . "' and l.dtValorOrcado <= '" . $dataF . "'";
            } else {
                $groupby = "CONCAT(substr(dtValorOrcado,6,2),'/',substr(dtValorOrcado,1,4))";
                $where = "occ.idOrcamento = " . $idOrcamento . " and gd.stOrcamento = 1 and gd.stGrupoDR = 'R'";
                $where .= " and l.dtValorOrcado >= '" . $dataI . "' and l.dtValorOrcado <= '" . $dataF . "'";
            }}
            
            $receitasmesames = $model->getOrcamentoGrupoValoresT($where, $groupby);
            if ($receitasmesames) {
                $acumulado['acumulado'][$y][1] = doubleval($receitasmesames[0]['valormes']);
            } else {
                $acumulado['acumulado'][$y][1] = 0;
            }
            $periodo = date('Y-m-d', strtotime('+31 days',strtotime($periodo)));
        }   
        
        // despesas
        
        $periodo = $dataInicial;   
        for ($y=1; $y<=$meses; $y++) {                       
            $dataI = substr($periodo,0,8) . '01 00:00:00';
            $dataF = date("Y-m-t",strtotime($periodo)) . " 23:59:59";

            if ($origem == 'centrocusto') {
                $groupby = "l.idOrcamentoColaboradorCC, CONCAT(substr(dtValorOrcado,6,2),'/',substr(dtValorOrcado,1,4))";
                $where = "occ.idOrcamento = " . $idOrcamento . " and gd.stOrcamento = 1 and gd.stGrupoDR = 'D' and l.idOrcamentoColaboradorCC = " . $idOrcamentoColaboradorCC;
                $where .= " and l.dtValorOrcado >= '" . $dataI . "' and l.dtValorOrcado <= '" . $dataF . "'";
            } else {
            if ($origem == 'colaborador') {
                $groupby = "CONCAT(substr(dtValorOrcado,6,2),'/',substr(dtValorOrcado,1,4))";
                $where = "occ.idOrcamento = " . $idOrcamento . " and gd.stOrcamento = 1 and gd.stGrupoDR = 'D' and occ.idColaborador = " . $idOrcamentoColaboradorCC;
                $where .= " and l.dtValorOrcado >= '" . $dataI . "' and l.dtValorOrcado <= '" . $dataF . "'";
            } else {
                $groupby = "CONCAT(substr(dtValorOrcado,6,2),'/',substr(dtValorOrcado,1,4))";
                $where = "occ.idOrcamento = " . $idOrcamento . " and gd.stOrcamento = 1 and gd.stGrupoDR = 'D'";
                $where .= " and l.dtValorOrcado >= '" . $dataI . "' and l.dtValorOrcado <= '" . $dataF . "'";
            }}
            
            $receitasmesames = $model->getOrcamentoGrupoValoresT($where, $groupby);
            if ($receitasmesames) {
                $acumulado['acumulado'][$y][2] = doubleval($receitasmesames[0]['valormes']);
            } else {
                $acumulado['acumulado'][$y][2] = 0;
            }
            $periodo = date('Y-m-d', strtotime('+31 days',strtotime($periodo)));
        }   
        $vacumulado = $vlSaldoInicial;
        for ($x=1; $x<=$meses; $x++) {
            $valorR = $acumulado['acumulado'][$x][1];
            $valorD = $acumulado['acumulado'][$x][2];
            $vacumulado = $vacumulado + ($valorR - $valorD);
            $acumulado['acumulado'][$x][3] = $valorR - $valorD;
            $acumulado['acumulado'][$x][4] = $vacumulado;
        }
        
        $retornogeral = array(
            'despesas' => $retorno['despesas'],
            'receitas' => $retorno['receitas'],            
            'gruporeceitasmes' => $retorno['gruporeceitasmes'],        
            'grupodespesasmes' => $retorno['grupodespesasmes'],        
            'acumulado' => $acumulado['acumulado']
        );

        echo json_encode($retornogeral);
                
    }
    
    public function adicionarcolaborador() {
       // $sy = new system\System();
        $idColaborador = $_POST['idColaborador'];
        $idOrcamento = $_POST['idOrcamento'];
        
        $model = new orcamentoModel();
        $data = array(
            'idOrcamento' => $idOrcamento,
            'idColaborador' => $idColaborador
        );
        $model->setOrcamentoColaborador($data);
        $where = 'oc.idOrcamento = ' . $idOrcamento;
        $lista_orcamentoc = $model->getOrcamentoColaboradores($where);        
        $x=0;
        foreach ($lista_orcamentoc as $value) {
            $groupby = 'occ.idOrcamento,occ.idColaborador';

            $where = "gd.stOrcamento = 1 and gd.stGrupoDR = 'D' and occ.idOrcamento = " . $value['idOrcamento'] . " and occ.idColaborador = " . $value['idColaborador'];
            $totais = $model->getTotais($where, $groupby);
            if ($totais) {
                $lista_orcamentoc[$x]['vlDespesas'] = $totais[0]['vlTotal'];
            } else {
                $lista_orcamentoc[$x]['vlDespesas'] = 0;
            }

            $where = "gd.stOrcamento = 1 and gd.stGrupoDR = 'R' and occ.idOrcamento = " . $value['idOrcamento'] . " and occ.idColaborador = " . $value['idColaborador'];
            $totais = $model->getTotais($where, $groupby);
            if ($totais) {
                $lista_orcamentoc[$x]['vlReceitas'] = $totais[0]['vlTotal'];
            } else {
                $lista_orcamentoc[$x]['vlReceitas'] = 0;
            }
            $x++;
        }

        $lista_colaboradores = $this->carrega_colaborador($idOrcamento);
        $this->smarty->assign('lista_colaboradores',$lista_colaboradores);
        $html1 = $this->smarty->fetch("orcamento/form_listacolaborador.tpl");

        $this->smarty->assign('lista_orcamentocolaborador', $lista_orcamentoc);    
        $html = $this->smarty->fetch("orcamento/orcamentocolaborador.html");

        // criar o array de retorno
        $jasonretorno = array(
            'html' => $html,
            'html1' => $html1
        );
        echo json_encode($jasonretorno);        
    }    

    private function carrega_cc($idOrcamento) {
        if ($idOrcamento) {
            $whereCC = ' idCentroCusto not in (select idCentroCusto from prodOrcamentoColaboradoresCC where idOrcamento = ' . $idOrcamento . ')';
        } else {
            $whereCC = null;
        }
        $modelCC = new centrocustoModel();
        $lista_cc = array('' => 'SELECIONE');
        foreach ($modelCC->getCentroCusto($whereCC) as $value) {
            $lista_cc[$value['idCentroCusto']] =  $value['cdCentroCusto'] . '-' . $value['dsCentroCusto'];
        }
        
        return $lista_cc;
    }
    
    private function carrega_colaborador($idOrcamento) {
        if ($idOrcamento) {
            $whereCo = ' a.idColaborador not in (select idColaborador from prodOrcamentoColaboradores where idOrcamento = ' . $idOrcamento . ')';
        } else {
            $whereCo = null;
        }
        $modelColaboradores = new colaboradorModel();
        $lista_colaboradores = array('' => 'SELECIONE');
        foreach ($modelColaboradores->getColaborador($whereCo) as $value) {
            $lista_colaboradores[$value['idColaborador']] = $value['dsColaborador'];
        }
        
        return $lista_colaboradores;
    }
    
    public function adicionarcentrocusto() {
        $sy = new system\System();
        $idCentroCusto = $_POST['idCentroCusto'];
        $idColaborador = $_POST['idColaborador'];
        $idOrcamento = $_POST['idOrcamento'];
        $vlLimiteDespesas = ($_POST['vlLimiteDespesas'] != '') ? str_replace(",",".",str_replace(".","",$_POST['vlLimiteDespesas'])) : null;
        $vlLimiteReceitas = ($_POST['vlLimiteReceitas'] != '') ? str_replace(",",".",str_replace(".","",$_POST['vlLimiteReceitas'])) : null;
        
        $model = new orcamentoModel();
        $data = array(
            'idOrcamento' => $idOrcamento,
            'idColaborador' => $idColaborador,
            'vlLimiteDespesas' => $vlLimiteDespesas,
            'vlLimiteReceitas' => $vlLimiteReceitas,
            'idCentroCusto' => $idCentroCusto
        );
        $model->setOrcamentoCentroCusto($data);
        
        $where = 'occ.idOrcamento = ' . $idOrcamento . ' and occ.idColaborador = ' . $idColaborador;
        $lista_orcamentocc = $model->getOrcamentoColaboradoresCC($where);        
        $x=0;
        foreach ($lista_orcamentocc as $value) {
            $groupby = 'occ.idOrcamento, occ.idColaborador, occ.idCentroCusto';

            $where = "gd.stOrcamento = 1 and gd.stGrupoDR = 'D' and occ.idOrcamento = " . $value['idOrcamento'] . " and occ.idColaborador = " . $value['idColaborador']  . " and occ.idCentroCusto = " . $value['idCentroCusto'];
            $totais = $model->getTotais($where, $groupby);
            if ($totais) {
                $lista_orcamentocc[$x]['vlDespesas'] = $totais[0]['vlTotal'];
            } else {
                $lista_orcamentocc[$x]['vlDespesas'] = 0;
            }

            $where = "gd.stOrcamento = 1 and gd.stGrupoDR = 'R' and occ.idOrcamento = " . $value['idOrcamento'] . " and occ.idColaborador = " . $value['idColaborador']  . " and occ.idCentroCusto = " . $value['idCentroCusto'];
            $totais = $model->getTotais($where, $groupby);
            if ($totais) {
                $lista_orcamentocc[$x]['vlReceitas'] = $totais[0]['vlTotal'];
            } else {
                $lista_orcamentocc[$x]['vlReceitas'] = 0;
            }
            $x++;
        }

        $lista_cc = $this->carrega_cc($idOrcamento);
        $this->smarty->assign('lista_cc',$lista_cc);
        $html1 = $this->smarty->fetch("orcamento/form_listacc.tpl");
        
        $this->smarty->assign('lista_orcamentocc', $lista_orcamentocc);    
        $html = $this->smarty->fetch("orcamento/orcamentocentrocusto.html");

        // criar o array de retorno
        $jasonretorno = array(
            'html1' => $html1,
            'html' => $html
        );
        echo json_encode($jasonretorno);        
    }    
    public function vercentrocusto() {
        $sy = new system\System();
        $idColaborador = $_POST['idColaborador'];
        $idOrcamento = $_POST['idOrcamento'];
        $model = new orcamentoModel();
        $where = 'occ.idOrcamento = ' . $idOrcamento . ' and occ.idColaborador = ' . $idColaborador;
        $lista_orcamentocc = $model->getOrcamentoColaboradoresCC($where);        
        $x=0;
        foreach ($lista_orcamentocc as $value) {
            $groupby = 'occ.idOrcamento, occ.idColaborador, occ.idCentroCusto';

            $where = "gd.stOrcamento = 1 and gd.stGrupoDR = 'D' and occ.idOrcamento = " . $value['idOrcamento'] . " and occ.idColaborador = " . $value['idColaborador']  . " and occ.idCentroCusto = " . $value['idCentroCusto'];
            $totais = $model->getTotais($where, $groupby);
            if ($totais) {
                $lista_orcamentocc[$x]['vlDespesas'] = $totais[0]['vlTotal'];
            } else {
                $lista_orcamentocc[$x]['vlDespesas'] = 0;
            }

            $where = "gd.stOrcamento = 1 and gd.stGrupoDR = 'R' and occ.idOrcamento = " . $value['idOrcamento'] . " and occ.idColaborador = " . $value['idColaborador']  . " and occ.idCentroCusto = " . $value['idCentroCusto'];
            $totais = $model->getTotais($where, $groupby);
            if ($totais) {
                $lista_orcamentocc[$x]['vlReceitas'] = $totais[0]['vlTotal'];
            } else {
                $lista_orcamentocc[$x]['vlReceitas'] = 0;
            }
            $x++;
        }

        $lista_cc = $this->carrega_cc($idOrcamento);
        $this->smarty->assign('lista_cc',$lista_cc);
        
        $this->smarty->assign('lista_orcamentocc', $lista_orcamentocc);    
        $html = $this->smarty->fetch("orcamento/orcamentocentrocusto.html");

        // criar o array de retorno
        $jasonretorno = array(
            'html' => $html
        );
        echo json_encode($jasonretorno);        
    }    
    private function trataPost($post) {
        $data['idOrcamento'] = ($post['idOrcamento'] != '') ? $post['idOrcamento'] : null;
        $data['dsOrcamento'] = ($post['dsOrcamento'] != '') ? $post['dsOrcamento'] : null;
        $data['vlSaldoInicial'] = ($post['vlSaldoInicial'] != '') ? str_replace(",",".",str_replace(".","",$post['vlSaldoInicial'])) : null;
        $data['idUsuarioProprietario'] = $_SESSION['user']['usuario'];
        $data['dtCriacao'] = ($post['dtCriacao'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $post["dtCriacao"]))) : date('Y-m-d h:m:s');
        $data['dtInicioVigencia'] = ($post['dtInicioVigencia'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $post["dtInicioVigencia"]))) : date('Y-m-d h:m:s');
        $data['dtFimVigencia'] = ($post['dtFimVigencia'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $post["dtFimVigencia"]))) : date('Y-m-d h:m:s');
        $data['dsTermoAbertura'] = ($post['dsTermoAbertura'] != '') ? $post['dsTermoAbertura'] : null;
        return $data;
    }
    // Remove Padrao
    public function delorcamento() {
        $sy = new system\System();
                
        $idOrcamento = $sy->getParam('idOrcamento');        
        $orcamento = $idOrcamento;        
        if (!is_null($orcamento)) {    
            $model = new orcamentoModel();
            $dados['idOrcamento'] = $orcamento;             
            $model->delOrcamento($dados);
        }
        header('Location: /orcamento');
    }

}

?>