<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class insumo extends controller {

    public function index_action() {
//die("chegou");
        //Inicializa o Template
        $this->template->run();

        $model = new insumoModel();
       // $insumo_lista = $model->getInsumo(null,false);

        $this->smarty->assign('insumo_lista', null);
        $this->smarty->display('insumo/lista.html');
    }

    public function busca_insumo() {        
        
        //se nao existir o indice estou como padrao '';
        $dsGrupo = isset($_POST['dsGrupo']) ? $_POST['dsGrupo'] : '';
        $dsInsumo = isset($_POST['dsInsumo']) ? $_POST['dsInsumo'] : '';
        $dsParceiro = isset($_POST['dsParceiro']) ? $_POST['dsParceiro'] : '';
        $cdInsumo = isset($_POST['cdInsumo']) ? $_POST['cdInsumo'] : '';
        $cdNCM = isset($_POST['cdNCM']) ? $_POST['cdNCM'] : '';
        $dsOrgaoControlador = isset($_POST['dsOrgaoControlador']) ? $_POST['dsOrgaoControlador'] : '';
        //$texto = '';
        $model = new estoqueModel();
        
        $busca = array();
        $sql = 'a.idInsumo > 0';
        if ($dsGrupo) {
            $sql = $sql . " and upper(d.dsGrupo) like upper('%" . $dsGrupo . "%')";
            $busca['dsGrupo'] = $dsGrupo;
        }
        if ($dsInsumo) {
            $sql = $sql . " and upper(a.dsInsumo) like upper('%" . $dsInsumo . "%')";
            $busca['dsInsumo'] = $dsInsumo;
        }
        if ($dsParceiro) {
            $sql = $sql . " and upper(f.dsParceiro) like upper('%" . $dsParceiro . "%')";
            $busca['dsParceiro'] = $dsParceiro;
        }
        if ($cdInsumo) {
            $sql = $sql . " and upper(a.cdInsumo) like upper('%" . $cdInsumo . "%')";
            $busca['cdInsumo'] = $cdInsumo;
        }
        if ($cdNCM) {
            $sql = $sql . " and upper(ncm.cdNCM) like upper('%" . $cdNCM . "%')";
            $busca['cdNCM'] = $cdNCM;
        }
        if ($dsOrgaoControlador) {
            $sql = $sql . " and upper(a.dsOrgaoControlador) like upper('%" . $dsOrgaoControlador . "%')";
            $busca['dsOrgaoControlador'] = $dsOrgaoControlador;
        }
        $cpagina = null;
        if (isset($_POST['cpagina'])) { 
            $cpagina = true;
            $busca['cpagina'] = 1;
        }
        $controlados = null;
        if (isset($_POST['controlados'])) { 
            $controlados = true;
            $sql = $sql . " and a.stControlado = 1";
            $busca['controlados'] = 1;
        }
        $comfispq = null;
        if (isset($_POST['comfispq'])) { 
            $comfispq = true;
            $busca['comfispq'] = 1;
        }
        
        $model = new insumoModel();
        $resultado = $model->getInsumo($sql, $cpagina, null, $comfispq);
        $_SESSION['buscar_insumos']['sql'] = $sql;
        if (sizeof($resultado) > 0) {
            $this->smarty->assign('insumo_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'insumo');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('insumo/lista.html');
        } else {
            $this->smarty->assign('insumo_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'insumo');
            $this->smarty->assign('busca', $busca);
            $this->smarty->display('insumo/lista.html');
        }
    }

    public function busca_criarcsv() {
        $sql = $_SESSION['buscar_insumos']['sql'];
        $model = new insumoModel();
        $resultado = $model->getInsumo($sql);

        if ($resultado) {
            $this->criarCSV($resultado);
        }
     //   header('Location: /solicitacaocomprasmanutencao');
    }
    
    public function criarCSV($resultado) {
        
        $limit = 30000;
        $offset = 0;

        global $PATH;
        $caminho = "/var/www/html/thopos.com.br/site/storage/tmp/csv/";

        // Storage
        if (!is_dir($caminho)) {
          mkdir($caminho, 0777, true);
        }

        $filename = "produtos_" . date("YmsHis") . ".csv";

        $headers[] = implode(";", array(
          "\"ID\"",
          "\"CODIGO\"",
          "\"NOME DO PRODUTO\"",
          "\"GRUPO\"",
          "\"MARCA\"",
          "\"MODELO\"",
          "\"NUMERO DE SERIE\"",
          "\"VAL UNITARIO\"",
          "\"DATA CADASTRO\"",
          "\"VALIDADE\"",
          "\"NCM\"",
          "\"FORN ULT COMPRA\"",
          "\"QT ULT COMPRA\"",
          "\"DT ULT COMPRA\"",
          "\"QT ULT SAIDA\"",
          "\"DT ULT SAIDA\""
        ));

        if (file_exists("{$caminho}" . '/' . "{$filename}")) {
          unlink("{$caminho}" . '/' . "{$filename}");
        }

        // Arquivo
        $handle = fopen("{$caminho}" . '/' . "{$filename}", 'w+');
        fwrite($handle, implode(";" . PHP_EOL, $headers));
        fwrite($handle, ";" . PHP_EOL);
        fflush($handle);

        // Fecha o arquivo da $_SESSION para liberar o servidor para servir outras requisições
        session_write_close();

        $output = array();
        foreach ($resultado as $value) {
            $output[] = implode(";", array(
              "\"{$value["idInsumo"]}\"",
              "\"{$value["cdInsumo"]}\"",
              "\"{$value["dsInsumo"]}\"",
              "\"{$value["dsGrupo"]}\"",
              "\"{$value["dsMarca"]}\"",
              "\"{$value["dsModelo"]}\"",
              "\"{$value["nrSerie"]}\"",
              "\"{$value["vlUnitario"]}\"",
              "\"{$value["dtCadastro"]}\"",
              "\"{$value["dtValidade"]}\"",
              "\"{$value["cdNCM"]}\"",
              "\"{$value["dsParceiro"]}\"",
              "\"{$value["qtUltimaCompra"]}\"",
              "\"{$value["dtUltimaCompra"]}\"",
              "\"{$value["qtUltimaVenda"]}\"",
              "\"{$value["dtUltimaVenda"]}\"",
            ));
        }
        fwrite($handle, implode(";" . PHP_EOL, $output));
        fwrite($handle, ";" . PHP_EOL);
        fflush($handle);
        fclose($handle);
        $this->download($caminho . '/' . $filename, 'CSV', $filename);        
    }

    private function download($nome, $tipo, $filename) {
      if (!empty($nome)) {
        if (file_exists($nome)) {
          header('Content-Transfer-Encoding: binary'); // For Gecko browsers mainly
          header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($nome)) . ' GMT');
          header('Accept-Ranges: bytes'); // For download resume
          header('Content-Length: ' . filesize($nome)); // File size
          header('Content-Encoding: none');
          header("Content-Type: application/{$tipo}"); // Change this mime type if the file is not PDF
          header('Content-Disposition: attachment; filename=' . $filename);
          // Make the browser display the Save As dialog
          readfile($nome);
          unlink($nome);
        }
      }
    }    
    
    //Funcao de Inserir
    public function novo_insumo() {
        $sy = new system\System();
        $idInsumo = $sy->getParam('idInsumo');
        $ajustar = $sy->getParam('pkRsuviop');

        $model = new insumoModel();

        if ($idInsumo > 0) {
            $registro = $model->getInsumo('idInsumo=' . $idInsumo);
            $registro = $registro[0]; //Passando Insumo
        } else {
            //Novo Registro
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        $modelGrupo = new grupoModel();
        $lista_grupo = array('' => 'SELECIONE');
        foreach ($modelGrupo->getGrupo() as $value) {
            $lista_grupo[$value['idGrupo']] = $value['dsGrupo'];
        }
        $modelUnidade = new unidadeModel();
        $lista_unidade = array('' => 'SELECIONE');
        foreach ($modelUnidade->getUnidade() as $value) {
            $lista_unidade[$value['idUnidade']] = $value['dsUnidade'];
        }
        $modelMarca = new marcaModel();
        $lista_marca = array('' => 'SELECIONE');
        foreach ($modelMarca->getMarca() as $value) {
            $lista_marca[$value['idMarca']] = $value['dsMarca'];
        }
        $modelModelo = new modeloModel();
        $lista_modelo = array('' => 'SELECIONE');
        foreach ($modelModelo->getModelo() as $value) {
            $lista_modelo[$value['idModelo']] = $value['dsModelo'];
        }
        $lista_controlado = array('' => 'SELECIONE','0' => 'NAO','1' => 'SIM');

        $modellocalestoque = new localestoqueModel();
        $lista_localestoque = array('' => 'SELECIONE');
        foreach ($modellocalestoque->getLocalEstoque() as $value) {
            $desanterior = $modellocalestoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
            if ($desanterior) {
                if ($value['idLocalEstoque'] == $value['idLocalEstoqueSuperior']) {
                    $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
                } else {
                    $lista_localestoque[$value['idLocalEstoque']] = $desanterior[0]['dsLocalEstoque'] . '-' . $value['dsLocalEstoque'];
                }
            } else {
                $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
            }
        }
        $modelParceiro = new parceiroModel();
        $lista_Parceiro = array('' => 'SELECIONE');
        foreach ($modelParceiro->getParceiro() as $value) {
            $lista_Parceiro[$value['idParceiro']] = $value['dsParceiro'];
        }

        $listaestoque = null;
        if ($idInsumo) {
            $modelest = new estoqueModel();
            $where = 'e.idInsumo = ' . $idInsumo;
            $listaestoque = $modelest->getEstoqueAtual($where);
            $x = 0;
            foreach ($listaestoque as  $value) {
                if ($value['idLocalEstoqueSuperior']) {
                   $desanterior = $modellocalestoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
                   if ($desanterior) {
                       $listaestoque[$x]['dsLocalEstoqueSuperior'] = $desanterior[0]['dsLocalEstoque'];
                   }
                }
                $x++;
            }
        }
        
        $this->smarty->assign('ajustar', $ajustar);
        $this->smarty->assign('produtos_localestoque', $listaestoque);
        $this->smarty->assign('lista_Parceiro', $lista_Parceiro);
        $this->smarty->assign('lista_controlado', $lista_controlado);
        $this->smarty->assign('lista_localestoque', $lista_localestoque);
        $this->smarty->assign('lista_tipomovimento', $this->carregar_tipo_movimento('E'));
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('lista_grupo', $lista_grupo);
        $this->smarty->assign('lista_unidade', $lista_unidade);
        $this->smarty->assign('lista_marca', $lista_marca);
        $this->smarty->assign('lista_modelo', $lista_modelo);
        //$this->smarty->assign('lista_NCM', $lista_NCM);
        $this->smarty->assign('title', 'Novo Insumo');
        $this->smarty->display('insumo/form_novo.tpl');
    }

    public function ajustarPME() {
        $idEstoque = $_POST['idEstoque'];
        $model = new estoqueModel();
        $retorno = $model->getEstoque('e.idEstoque = ' . $idEstoque);
        if ($retorno) {
            $array = array(
                'idEstoque' => $idEstoque,
                'vlPME' => $retorno[0]['vlEstoque'] / $retorno[0]['qtEstoque']);
            $model->updEstoque($array);
        }
        $retorno = array();
        echo json_encode($retorno);
    }
    
    public function carrega_tipo_mov() {
        $tipo = $_POST['entrada_saida'];
        $this->smarty->assign('lista_tipomovimento', $this->carregar_tipo_movimento($tipo));
        $html = $this->smarty->fetch('insumo/listatm.tpl');       
        $retorno = array(
            'html' => $html
        );
        echo json_encode($retorno);
    }
    
    private function carregar_tipo_movimento($tipo) {
        $modelTipoMovimento = new tipomovimentoModel();
        $lista_tipomovimento = array('' => 'SELECIONE');
        foreach ($modelTipoMovimento->getTipoMovimento("stDC='{$tipo}'") as $value) {
            $lista_tipomovimento[$value['idTipoMovimento']] = $value['dsTipoMovimento'];
        }
//        var_dump($lista_tipomovimento); die;
        return $lista_tipomovimento;
    }

    public function atualizagrupo() {
        $idGrupo = $_POST['idGrupo'];
        $idInsumo = $_POST['idInsumo'];
        $model = new insumoModel();
        $dados = array(
          'idGrupo' => $idGrupo,  
          'idInsumo' => $idInsumo  
        );
        
        $model->updInsumo($dados);
        $jsondata["ok"] = true;
        echo json_encode($jsondata);        
    }
    
    public function atualizaunidade() {
        $idUnidade = $_POST['idUnidade'];
        $idInsumo = $_POST['idInsumo'];
        $model = new insumoModel();
        $dados = array(
          'idUnidade' => $idUnidade,  
          'idInsumo' => $idInsumo  
        );
        
        $model->updInsumo($dados);
        $jsondata["ok"] = true;
        echo json_encode($jsondata);        
    }
    
    public function gravar_insumo() {
        $model = new insumoModel();

        $data = $this->trataPost($_POST);

        if ($data['idInsumo'] == NULL) {
            $data['dtCadastro'] = date('Y-m-d h:m:s');        
            $model->setinsumo($data);             
        }
        else {
            $model->updinsumo($data);             
        }//update
        
        header('Location: /insumo');        
        return;
    }

    public function gravar_loalestoque() {
        $model = new estoqueModel();
        $data = array();
        $idInsumo = ($_POST['idInsumo'] != '') ? $_POST['idInsumo'] : null;
        $idLocalEstoque = ($_POST['idLocalEstoque'] != '') ? $_POST['idLocalEstoque'] : null;

        $data['qtEstoqueMinimo'] = ($_POST['qtEstoqueMinimo'] != '') ? str_replace(",",".",str_replace(".","",$_POST['qtEstoqueMinimo'])) : null;
        $data['qtLoteReposicao'] = ($_POST['qtLoteReposicao'] != '') ? str_replace(",",".",str_replace(".","",$_POST['qtLoteReposicao'])) : null;
        
        $where = 'e.idInsumo = ' . $idInsumo . ' and e.idLocalEstoque = ' . $idLocalEstoque;
        $retorno = $model->getEstoqueAtual($where);
        if ($retorno) {
            $where = 'idInsumo = ' . $idInsumo . ' and idLocalEstoque = ' . $idLocalEstoque;
            $model->updEstoquePP($data,$where);
        } else {
            $data['idLocalEstoque'] = ($_POST['idLocalEstoque'] != '') ? $_POST['idLocalEstoque'] : null;
            $data['idInsumo'] = ($_POST['idInsumo'] != '') ? $_POST['idInsumo'] : null;
            $model->setEstoque($data);
        }
        $jsondata["ok"] = true;
        echo json_encode($jsondata);
        
    }
    
    //Trata dados antes de Enviar para o Gravar
    private function trataPost($post) {
        $data['idInsumo'] = ($post['idInsumo'] != '') ? $post['idInsumo'] : null;
        $data['dsInsumo'] = ($post['dsInsumo'] != '') ? $post['dsInsumo'] : null;
        $data['cdInsumo'] = ($post['cdInsumo'] != '') ? $post['cdInsumo'] : null;
        $data['idUnidade'] = ($post['idUnidade'] != '') ? $post['idUnidade'] : null;
        $data['dtValidade'] = ($post['dtValidade'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $_POST["dtValidade"]))) : date('Y-m-d h:m:s');        
        $data['idMarca'] = ($post['idMarca'] != '') ? $post['idMarca'] : null;
        $data['idModelo'] = ($post['idModelo'] != '') ? $post['idModelo'] : null;
        $data['nrSerie'] = ($post['nrSerie'] != '') ? $post['nrSerie'] : null;
        $data['dsCodigoDoFabricante'] = ($post['dsCodigoDoFabricante'] != '') ? $post['dsCodigoDoFabricante'] : null;
        $data['vlUnitario'] = ($post['vlUnitario'] != '') ? $post['vlUnitario'] : null;
        $data['idGrupo'] = ($post['idGrupo'] != '') ? $post['idGrupo'] : null;
        $data['idNCM'] = ($post['idNCM'] != '') ? $post['idNCM'] : null;
        $data['idParceiro'] = ($post['idParceiro'] != '') ? $post['idParceiro'] : null;
        $data['stControlado'] = ($post['stControlado'] != '') ? $post['stControlado'] : 0;
        $data['dsOrgaoControlador'] = ($post['dsOrgaoControlador'] != '') ? $post['dsOrgaoControlador'] : '';
        return $data;
    }
    public function importarfotos() {
        $idInsumo = $_POST['idInsumo'];
        $dsTabela = $_POST['dsTabela'];
        
        $model = new documentoModel();        
        $lista = $model->getDocumento("dsTabela = '" . $dsTabela . "' and idTabela  = " . $idInsumo);
        $this->smarty->assign('idInsumo', $idInsumo);
        $this->smarty->assign('dsTabela', $dsTabela);
        $this->smarty->assign('fotos_lista', $lista);
        $this->smarty->display("insumo/modalFotos/thumbnail.tpl");        
    }
    public function adicionarfoto() {
        $idInsumo = $_POST['idInsumo'];
        $dsTabela = $_POST['dsTabela'];
        
        $extensao = explode('.',$_FILES['arquivo']['name'])[1];
        $novonome = $dsTabela . '_' . $idInsumo . '_' . date('Ymdis') . '.' . $extensao;
        
        $_UP['pasta'] = 'storage/tmp/documentos/';
        // Tamanho máximo do arquivo (em Bytes)
        $_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
        // Array com as extensões permitidas
        $_UP['extensoes'] = array('jpg','jpeg','png','bmp', 'pdf');
        // Renomeia o arquivo? (Se true, o arquivo será salvo como .jpg e um nome único)
        $_UP['renomeia'] = false;
        // Array com os tipos de erros de upload do PHP
        $_UP['erros'][0] = 'Não houve erro';
        $_UP['erros'][1] = 'O arquivo no upload é maior do que o limite do PHP';
        $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
        $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
        $_UP['erros'][4] = 'Não foi feito o upload do arquivo';
        // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
        if ($_FILES['arquivo']['error'] != 0) {
          die("Não foi possível fazer o upload, erro:" . $_UP['erros'][$_FILES['arquivo']['error']]);
          exit; // Para a execução do script
        }
        // Caso script chegue a esse ponto, não houve erro com o upload e o PHP pode continuar
        // Faz a verificação da extensão do arquivo
//        $extensao = strtolower(end(explode('.', $_FILES['arquivo']['name'])));
//        if (array_search($extensao, $_UP['extensoes']) === false) {
//          echo "Por favor, envie arquivos com as seguinte extensão: csv";
//          exit;
//        }
        // Faz a verificação do tamanho do arquivo
        if ($_UP['tamanho'] < $_FILES['arquivo']['size']) {
          echo "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
          exit;
        }
        // O arquivo passou em todas as verificações, hora de tentar movê-lo para a pasta
        // Primeiro verifica se deve trocar o nome do arquivo
//        if ($_UP['renomeia'] == true) {
//          // Cria um nome baseado no UNIX TIMESTAMP atual e com extensão .jpg
//          $nome_final = md5(time()).'.csv';
//        } else {
          // Mantém o nome original do arquivo
//          $nome_final = $_FILES['arquivo']['name'];
//        }

        // Depois verifica se é possível mover o arquivo para a pasta escolhida
        if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $_UP['pasta'] . $novonome)) {
//          Upload efetuado com sucesso, exibe uma mensagem e um link para o arquivo
//          echo "Upload efetuado com sucesso!";
//          echo '<a href="' . $_UP['pasta'] . $nome_final . '">Clique aqui para acessar o arquivo</a>';
//          
//            exit;
//          $this->importar($_UP['pasta'] . $nome_final, $idPeriodo);
//          header('Location: /planodecontas/novo_planodecontas/idPeriodo/' . $idPeriodo);

            $model = new documentoModel();    
            $dados = array(
                'idDocumento' => null,
                'dsTabela' => $dsTabela,
                'idTabela' => $idInsumo,
                'dsLocalArquivo' => $_UP['pasta'] . $novonome                
            );
            $model->setDocumento($dados);
            
        } else {
          // Não foi possível fazer o upload, provavelmente a pasta está incorreta
          echo "Não foi possível enviar o arquivo, tente novamente";
        }    
        header('Location: /insumo');        
    }

    // Remove Padrao
    public function delinsumo() {
        $sy = new system\System();
                
        $idInsumo = $sy->getParam('idInsumo');
        
        $insumo = $idInsumo;
        
        if (!is_null($insumo)) {    
            $model = new insumoModel();
            $dados['idInsumo'] = $insumo;             
            $model->delInsumo($dados);
        }

        header('Location: /insumo');
    }

    public function relatorioinsumo_pre() {
        $this->template->run();

        $this->smarty->assign('title', 'Pre Relatorio de Insumos');
        $this->smarty->display('insumo/relatorio_pre.html');
    }

    public function relatorioinsumo() {
        $this->template->run();

        $model = new insumoModel();
        $insumo_lista = $model->getInsumo();
        //Passa a lista de registros
        $this->smarty->assign('insumo_lista', $insumo_lista);
        $this->smarty->assign('titulo_relatorio');
        //Chama o Smarty
        $this->smarty->assign('title', 'Relatorio de Insumos');
        $this->smarty->display('insumo/relatorio.html');
    }
    
    public function gravaracertoestoque() {
        $entrada_saida = $_POST['entrada_saida'];
        $dsMotivo = $_POST['dsMotivo'];
        $qtAcerto = $_POST['qtAcerto'];
        $vlAcerto = $_POST['vlAcerto'];
        $idTipoMovimento = $_POST['idTipoMovimento'];
        $idLocalEstoque = $_POST['idLocalEstoque'];
        $datacabec = $this->preparaMovimento($_POST);
        $model = new entradaestoqueModel();
        $idMovimento = $model->setMovimento($datacabec);                
        $data = $this->preparaItem($_POST, $idMovimento);
        $this->gravar_itemmesmo($data, $entrada_saida);
        $retorno=array();
        echo json_encode($retorno);
    }
    private function preparaMovimento($post) {
        $data = array();
        $data['idMovimento'] = null;
        $data['idParceiro'] =  null;
        $data['idTipoMovimento'] = ($post['idTipoMovimento'] != '') ? $post['idTipoMovimento'] : null;
        $data['dsObservacao'] = ($post['dsMotivo'] != '') ? $post['dsMotivo'] : null;
        $data['dtMovimento'] = date('Y-m-d');
        $data['nrNota'] = null;
        $data['nrPedido'] = null;
        $data['qtTotalNota'] =  null;
        $data['idColaborador'] =  null;
        $data['nrPlaca'] = null;
        $data['nrLacre'] =  null;
        $data['nrNota2'] =  null;
        $data['qtTotalNota2'] =  null;
        $data['idUsuario'] = $_SESSION['user']['usuario'];
        return $data;
    }
    
    private function preparaItem($post, $idMovimento) {
        $data = array();
        $data['idMovimentoItem'] = null;
        $data['idInsumo'] = ($post['idInsumo'] != '') ? $post['idInsumo'] : null;
        $data['qtMovimento'] = ($post['qtAcerto'] != '') ? str_replace(",",".",str_replace(".","",$post['qtAcerto'])) : null;
        $data['vlMovimento'] = ($post['vlAcerto'] != '') ? str_replace(",",".",str_replace(".","",$post['vlAcerto'])) : null;
        $data['idLocalEstoque'] = ($post['idLocalEstoque'] != '') ? $post['idLocalEstoque'] : null;
        $data['idMovimento'] = $idMovimento;
        $data['idTipoMovimento'] = ($post['idTipoMovimento'] != '') ? $post['idTipoMovimento'] : null;
        $data['dsObservacao'] = ($post['dsMotivo'] != '') ? $post['dsMotivo'] : null;
        return $data;
    }
    
    private function gravar_itemmesmo($data, $entrada_saida) {
        
        $modelEstoque = new estoqueModel();
        $where = 'e.idInsumo = ' . $data['idInsumo'] . ' and e.idLocalEstoque = ' . $data['idLocalEstoque'];
        $retorno = $modelEstoque->getEstoque($where);
        if ($retorno) {
            $idEstoque = $retorno[0]['idEstoque'];
            if ($entrada_saida == 'E') {
                $qtdeatual = $retorno[0]['qtEstoque'] + $data['qtMovimento'];
                $valoratual = $retorno[0]['vlEstoque'] + $data['vlMovimento'];
            } else {
                $qtdeatual = $retorno[0]['qtEstoque'] - $data['qtMovimento'];
                $valoratual = $retorno[0]['vlEstoque'] - $data['vlMovimento'];
            }
            
            $dataE = array(
                'idEstoque' => $idEstoque,
                'qtEmSolicitacao' => 0,
                'stEmSolicitacao' => 0,
                'qtEstoque' => $qtdeatual,
                'vlEstoque' => $valoratual,
                'vlPME' => $valoratual / $qtdeatual
            );
            $modelEstoque->updEstoque($dataE);
        } else {
            $dataE = array(
                'idInsumo' => $data['idInsumo'],
                'idLocalEstoque' => $data['idLocalEstoque'],
                'qtEmSolicitacao' => 0,
                'stEmSolicitacao' => 0,
                'qtEstoque' => $data['qtMovimento'],
                'vlEstoque' => $data['vlMovimento'],
                'vlPME' => $data['vlMovimento'] / $data['qtMovimento']
            );
            $idEstoque = $modelEstoque->setEstoque($dataE);
        }
        
        $model = new entradaestoqueModel();
        $data['idCentroCusto'] = null;
        $data['idMaquina'] = null;
        $data['idOS'] = null;
        $data['idMotivo'] = null;
        $id = $model->setMovimentoItem($data);
        
        $modelInsumo = new insumoModel();  
        $datainsumo = array(
            'idUltimoMovimento' => $data['idMovimento'],
            'idInsumo' => $data['idInsumo']
        );
        $modelInsumo->updInsumo($datainsumo);
    }

    public function getNCM() {
        $url = explode("=", $_SERVER['REQUEST_URI']);
        $key = str_replace('+', ' ', $url[1]);
        if (!empty($key)) {
          $busca = trim($key);
          $model = new ncmModel();
          $where = "(UPPER(dsNCM) like UPPER('%{$key}%') OR UPPER(cdNCM) like UPPER('%{$key}%'))";
          $retorno = $model->getNCM($where,false);
          $return = array();
          if (count($retorno)) {
            $row = array();
            for ($i = 0; $i < count($retorno); $i++) {
              $row['value'] = $retorno[$i]["cdNCM"] . '-' . $retorno[$i]["dsNCM"];
              $row["id"] = $retorno[$i]["idNCM"];
              array_push($return, $row);
            }
          }
          echo json_encode($return);
        }        
    }            

}

?>