<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class tiporeuniao extends controller {

    public function index_action() {
//die("chegou");
        //Inicializa o Template
        $this->template->run();

        $model = new tiporeuniaoModel();
        $tiporeuniao_lista = $model->getTipoReuniao();

        $this->smarty->assign('tiporeuniao_lista', $tiporeuniao_lista);
        $this->smarty->display('tiporeuniao/lista.html');
    }

//Funcao de Busca
    public function busca_tiporeuniao() {
        //se nao existir o indice estou como padrao '';
        $texto = isset($_POST['buscadescricao']) ? $_POST['buscadescricao'] : '';
        //$texto = '';
        $model = new tiporeuniaoModel();
        $sql = "upper(dsTipoReuniao) like upper('%" . $texto . "%')"; //somente os nao excluidos
        $resultado = $model->getTipoReuniao($sql);

        if (sizeof($resultado) > 0) {
            $this->smarty->assign('tiporeuniao_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'tiporeuniao');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('tiporeuniao/lista.html');
        } else {
            $this->smarty->assign('tiporeuniao_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'tiporeuniao');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('tiporeuniao/lista.html');
        }
    }

    //Funcao de Inserir
    public function novo_tiporeuniao() {
        $sy = new system\System();

        $idTipoReuniao = $sy->getParam('idTipoReuniao');

        $model = new tiporeuniaoModel();

        if ($idTipoReuniao > 0) {

            $registro = $model->getTipoReuniao('idTipoReuniao=' . $idTipoReuniao);
            $registro = $registro[0]; //Passando TipoReuniao
        } else {
            //Novo Registro
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
        
        //Obter lista a de tipos fk
        $objLista = new tiporeuniaoModel();
        //criar uma lista
        $lista_tipos = $objLista->getTipoReuniao('idTipoReuniao <> 0');
        foreach ($lista_tipos as $value) {
            $lista_tipos_log[$value['idTipoReuniao']] = $value['dsTipoReuniao'];
        }
        //Passar a lista de Tipo
        $this->smarty->assign('lista_tiporeuniao', $lista_tipos);
        //var_dump($lista_tipos_log);die;
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('title', 'Novo Tipo de Movimento');
        $this->smarty->display('tiporeuniao/form_novo.tpl');
    }

    // Gravar Padrao
    public function gravar_tiporeuniao() {
        $model = new tiporeuniaoModel();

        $data = $this->trataPost($_POST);

        if ($data['idTipoReuniao'] == NULL)
            $model->settiporeuniao($data);
        else
            $model->updtiporeuniao($data); //update
        
        header('Location: /tiporeuniao');        
        return;
    }

    //Trata dados antes de Enviar para o Gravar
    private function trataPost($post) {
        $data['idTipoReuniao'] = ($post['idTipoReuniao'] != '') ? $post['idTipoReuniao'] : null;
        $data['dsTipoReuniao'] = ($post['dsTipoReuniao'] != '') ? $post['dsTipoReuniao'] : null;
        return $data;
    }

    // Remove Padrao
    public function deltiporeuniao() {
        $sy = new system\System();
                
        $idTipoReuniao = $sy->getParam('idTipoReuniao');
        
        $tiporeuniao = $idTipoReuniao;
        
        if (!is_null($tiporeuniao)) {    
            $model = new tiporeuniaoModel();
            $dados['idTipoReuniao'] = $tiporeuniao;             
            $model->delTipoReuniao($dados);
        }

        header('Location: /tiporeuniao');
    }

    public function relatoriotiporeuniao_pre() {
        $this->template->run();

        $this->smarty->assign('title', 'Pre Relatorio de Tipo de Movimento');
        $this->smarty->display('tiporeuniao/relatorio_pre.html');
    }

    public function relatoriotiporeuniao() {
        $this->template->run();

        $model = new tiporeuniaoModel();
        $tiporeuniao_lista = $model->getTipoReuniao();
        //Passa a lista de registros
        $this->smarty->assign('tiporeuniao_lista', $tiporeuniao_lista);
        $this->smarty->assign('titulo_relatorio');
        //Chama o Smarty
        $this->smarty->assign('title', 'Relatorio de Tipo de Movimento');
        $this->smarty->display('tiporeuniao/relatorio.html');
    }

}

?>