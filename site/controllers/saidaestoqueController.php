<?php

/*
 * Gerado pelo Framework Tools 1.0
 * Classe: Controller
 *
 */

class saidaestoque extends controller {

    public function index_action() {
        //Inicializa o Template
        $this->template->run();
        $this->novo_saidaestoque();
    }

//Funcao de Busca
    public function busca_saidaestoque() {
        //se nao existir o indice estou como padrao '';
        $texto = isset($_POST['buscadescricao']) ? $_POST['buscadescricao'] : '';
        //$texto = '';
        $model = new saidaestoqueModel();
        $sql = "stStatus <> 0 and upper(dsMovimento) like upper('%" . $texto . "%')"; //somente os nao excluidos
        $resultado = $model->getMovimento($sql);

        if (sizeof($resultado) > 0) {
            $this->smarty->assign('saidaestoque_lista', $resultado);
            //Chama o Smarty
            $this->smarty->assign('title', 'saidaestoque');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('saidaestoque/lista.html');
        } else {
            $this->smarty->assign('saidaestoque_lista', null);
            //Chama o Smarty
            $this->smarty->assign('title', 'saidaestoque');
            $this->smarty->assign('buscadescricao', $texto);
            $this->smarty->display('saidaestoque/lista.html');
        }
    }

    //Funcao de Inserir
    public function novo_saidaestoque() {
        $sy = new system\System();
        $idMovimento = $sy->getParam('idMovimento');
        if ($idMovimento) {
            $_SESSION['movimento']['id'] = $idMovimento;
        }

        if (isset($_SESSION['movimento']['id'])) {
            if (!is_null($_SESSION['movimento']['id'])) {
                $idMovimento = $_SESSION['movimento']['id'];
            }
        }
        
        $model = new saidaestoqueModel();
        if (isset($idMovimento)) {
            if ((bool) $idMovimento) {
                $registro = $model->getMovimento('idMovimento=' . $idMovimento);  
                if ($registro) {
                    $registro = $registro[0];
                } else {
                    //Novo Registro
                    $registro = $model->estrutura_vazia();
                    $registro = $registro[0];
                }
            } else {
                //Novo Registro
                $registro = $model->estrutura_vazia();
                $registro = $registro[0];
            }
        } else {
            //Novo Registro
            $registro = $model->estrutura_vazia();
            $registro = $registro[0];
        }
//        var_dump($registro); die;
        $modelTipoMovimento = new tipomovimentoModel();
        $lista_tipomovimento = array('' => 'SELECIONE');
        foreach ($modelTipoMovimento->getTipoMovimento("stDC='S'") as $value) {
            $lista_tipomovimento[$value['idTipoMovimento']] = $value['dsTipoMovimento'];
        }
        $modelCentroCusto = new centrocustoModel();
        $lista_centrocusto = array('' => 'SELECIONE');
        foreach ($modelCentroCusto->getCentroCustoCombo() as $value) {
            $lista_centrocusto[$value['idCentroCusto']] = $value['codigocusto'];
        }
        $modelSetor = new setorModel();
        $lista_setor = array('' => 'SELECIONE');
        foreach ($modelSetor->getSetor() as $value) {
            $lista_setor[$value['idSetor']] = $value['dsSetor'];
        }
        $modelMaquina = new maquinaModel();
        $lista_maquina = array('' => 'SELECIONE');
        foreach ($modelMaquina->getMaquina() as $value) {
            $lista_maquina[$value['idMaquina']] = $value['dsMaquina'];
        }
        $modelOS = new osModel();
        $lista_OS = array('' => 'SELECIONE');
        foreach ($modelOS->getOS() as $value) {
            $lista_OS[$value['idOS']] = $value['nrOS'];
        }
        $motivolOS = new motivoModel();
        $lista_Motivo = array('' => 'SELECIONE');
        foreach ($motivolOS->getMotivo() as $value) {
            $lista_Motivo[$value['idMotivo']] = $value['dsMotivo'];
        }
        $modellocalestoque= new localestoqueModel();
        $listaLE = array('' => 'SELECIONE');
        foreach ($modellocalestoque->getLocalEstoque() as $value) {
            $listaLE[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
        }
        $movimentoitens = array();
        $valortotal = null;
        if($idMovimento) {
            $where = "a.idMovimento = " . $idMovimento . " and t.stDC = 'S'";
            $movimentoitens = $model->getMovimentoItens($where);
            
            $x=0;
            foreach($movimentoitens as $value) {
                $desanterior = $modellocalestoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
                if ($desanterior) {
                    if ($value['idLocalEstoque'] == $value['idLocalEstoqueSuperior']) {
                        $movimentoitens[$x]['localEstoque'] = $value['dsLocalEstoque'];
                    } else {
                        $movimentoitens[$x]['localEstoque'] = $desanterior[0]['dsLocalEstoque'] . '-' . $value['dsLocalEstoque'];
                    }
                } else {
                    $movimentoitens[$x]['localEstoque'] = $value['dsLocalEstoque'];
                }
                $x++;
            }
            
            $totalmovimento = $model->getTotalMovimentoItens($where);
            $valortotal = $totalmovimento[0]['totalmovimento'];
        }  
        $colaborador = new colaboradorModel();
        $lista_colaborador = array('' => 'SELECIONE');
        foreach ($colaborador->getColaborador() as $value) {
            $lista_colaborador[$value['idColaborador']] = $value['dsColaborador'];
        }
        
        $this->smarty->assign('lista_colaborador', $lista_colaborador);
        $this->smarty->assign('lista_localestoqueentrada', $listaLE);
        $this->smarty->assign('registro', $registro);
        $this->smarty->assign('lista_tipomovimento', $lista_tipomovimento);
        $this->smarty->assign('lista_centrocusto', $lista_centrocusto);
        $this->smarty->assign('lista_setor', $lista_setor);
        $this->smarty->assign('lista_maquina', $lista_maquina);
        $this->smarty->assign('lista_os', $lista_OS);
        $this->smarty->assign('lista_motivo', $lista_Motivo);
        $this->smarty->assign('movimentoitens', $movimentoitens);
        $this->smarty->assign('totalmovimento', $valortotal);
        $this->smarty->assign('title', 'Novo Movimento');
        $this->smarty->display('saidaestoque/form_novo.tpl');
    }
    // Gravar Padrao
//    public function novasaida() {
//        $_SESSION['movimento']['id'] = null;
//        $jsondata["idMovimento"] = null;
//        $jsondata["ok"] = true;
//        echo json_encode($jsondata);
//    }
    public function novasaida() {
        $_SESSION['movimento']['id'] = null;
        header('Location: /saidaestoque/novo_saidaestoque');                
    }
    
    public function lerlocalestoque() {
        $idInsumo = $_POST['idInsumo'];

        $modellocalestoque = new localestoqueModel();
        $lista_localestoque = array('' => 'SELECIONE');
        if ($idInsumo) {
            foreach ($modellocalestoque->getLocalEstoqueProdutos('e.idInsumo = ' . $idInsumo) as $value) {
                if ($value['idLocalEstoque'] == $value['idLocalEstoqueSuperior']) {
                   $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
                } else {
                    $desanterior = $modellocalestoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
                    if ($desanterior) {
                       $lista_localestoque[$value['idLocalEstoque']] = $desanterior[0]['dsLocalEstoque'] . '-' . $value['dsLocalEstoque'];
                    } else {
                       $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
                    }
                }
            }
        } else {
            foreach ($modellocalestoque->getLocalEstoque() as $value) {
                if ($value['idLocalEstoque'] == $value['idLocalEstoqueSuperior']) {
                   $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
                } else {
                    $desanterior = $modellocalestoque->getLocalEstoque('idLocalEstoque = ' . $value['idLocalEstoqueSuperior']);
                    if ($desanterior) {
                       $lista_localestoque[$value['idLocalEstoque']] = $desanterior[0]['dsLocalEstoque'] . '-' . $value['dsLocalEstoque'];
                    } else {
                       $lista_localestoque[$value['idLocalEstoque']] = $value['dsLocalEstoque'];
                    }
                }
            }
        }    
        $this->smarty->assign('lista_localestoque', $lista_localestoque);        
        $html = $this->smarty->fetch('saidaestoque/lista_localestoque.tpl');        
        $jsondata["html"] = $html;
        echo json_encode($jsondata);
    }
    
    
    public function lerunidade() {
        $idInsumo = $_POST['idInsumo'];
        $idLocalEstoque = $_POST['idLocalEstoque'];
        $dsUnidade = null;
        $qtEstoque = null;
        $vlEstoque = null;
        $vlUltimaCompra = null;
        $modelEstoque = new estoqueModel();
        
        $where = 'e.idInsumo = ' . $idInsumo . ' and e.idLocalEstoque = ' . $idLocalEstoque;
        $retorno = $modelEstoque->getEstoque($where);
        if ($retorno) {
            $dsUnidade = $retorno[0]['dsUnidade'];
            $qtEstoque = $retorno[0]['qtEstoque'];
            $vlEstoque = $retorno[0]['vlEstoque'];
            $vlUltimaCompra = ($retorno[0]['vlEstoque'] / $retorno[0]['qtEstoque']);
        }
        $jsondata["dsUnidade"] = $dsUnidade;
        $jsondata["qtEstoque"] = $qtEstoque;
        $jsondata["vlEstoque"] = $vlEstoque;
        $jsondata["vlUltimaCompra"] = $vlUltimaCompra;
        $jsondata["ok"] = true;
        echo json_encode($jsondata);
    }
    
    
    
    public function desabilitaid() {
        $_SESSION['movimento']['id'] = null;
        echo json_encode(true);
    }
    public function gravar_movimento() {
        
        $model = new saidaestoqueModel();

        $data = $this->trataPost($_POST);
//        var_dump($data); die;
        if ($data['idMovimento']) {
            $model->updMovimento($data);
            $id = $data['idMovimento'];
        } else {
            $id = $model->setMovimento($data);
        }
        
        $_SESSION['movimento']['id'] = $id;
        $jsondata["html"] = "saidaestoque/form_novo.tpl";
        $jsondata["idMovimento"] = $id;
        $jsondata["ok"] = true;
        echo json_encode($jsondata);
    }

    public function estornar() {
        
    }
    
    public function gravar_item() {
        
        $model = new saidaestoqueModel();
        $data = $this->trataPostItem($_POST);
        $datacabec = $this->trataPost($_POST);
        
        $idLocalEstoqueEntrada = ($_POST['idLocalEstoqueEntrada'] != '') ? $_POST['idLocalEstoqueEntrada'] : null;
        
        $id = $model->setMovimentoItem($data);
        $modelInsumo = new insumoModel();
        
        $mTeveCC = false;
        $mTeveOS = false;
        $mTeveMaquina = false;
        $mTeveLocal = false;
        // dados do item    
        $dataitemoutros = $this->trataPostItemOutros($_POST);        

        $modelEstoque = new estoqueModel();        
        
        // gravando a saida de estoque
        
        $where = 'e.idInsumo = ' . $data['idInsumo'] . ' and e.idLocalEstoque = ' . $data['idLocalEstoque'];
        $retorno = $modelEstoque->getEstoque($where);
        
        if ($retorno) {
            $idEstoque = $retorno[0]['idEstoque'];
            $qtdeatual = $retorno[0]['qtEstoque'] - $data['qtMovimento'];
            $valoratual = $retorno[0]['vlEstoque'] - $data['vlMovimento'];
            if ($qtdeatual ==0) {
                $vlPME = 0;
            } else {
                $vlPME = $valoratual / $qtdeatual;                
            }
            $dataE = array(
                'idEstoque' => $idEstoque,
                'qtEstoque' => $qtdeatual,
                'vlEstoque' => $valoratual,
                'vlPME' => $vlPME
            );
            $modelEstoque->updEstoque($dataE);
        } else {
            $dataE = array(
                'idInsumo' => $data['idInsumo'],
                'idLocalEstoque' => $data['idLocalEstoque'],
                'qtEstoque' => $data['qtMovimento'],
                'vlEstoque' => $data['vlMovimento'],
                'vlPME' => $data['vlMovimento'] / $data['qtMovimento']
            );
            $idEstoque = $modelEstoque->setEstoque($dataE);
        }        
        
        // gravando a entrada de estoque caso tenha escolhido o local de estoque para entrada
        
        if ($idLocalEstoqueEntrada) {
//            echo 'passei';
            $where = 'e.idInsumo = ' . $data['idInsumo'] . ' and e.idLocalEstoque = ' . $idLocalEstoqueEntrada;
            $retorno = $modelEstoque->getEstoque($where);
//            echo 'retorno'; var_dump($retorno);
            if ($retorno) {
                $idEstoque = $retorno[0]['idEstoque'];
                $qtdeatual = $retorno[0]['qtEstoque'] + $data['qtMovimento'];
                $valoratual = $retorno[0]['vlEstoque'] + $data['vlMovimento'];

//                if ($qtdeatual) $qtdeatual =1;
                $dataE = array(
                    'idEstoque' => $idEstoque,
                    'qtEstoque' => $qtdeatual,
                    'vlEstoque' => $valoratual,
                    'vlPME' => $valoratual / $qtdeatual
                );
                $modelEstoque->updEstoque($dataE);
            } else {
                $dataE = array(
                    'idInsumo' => $data['idInsumo'],
                    'idLocalEstoque' => $idLocalEstoqueEntrada,
                    'qtEstoque' => $data['qtMovimento'],
                    'vlEstoque' => $data['vlMovimento'],
                    'vlPME' => $data['vlMovimento'] / $data['qtMovimento']
                );
                $idEstoque = $modelEstoque->setEstoque($dataE);
            }                    
        }
        
        // pegar a descrição do tipo de movimento
        $modelTM = new tipomovimentoModel();
        $nome = $modelTM->getTipoMovimento("idTipoMovimento = " . $dataitemoutros['idTipoMovimento']);
        $dsTipoMovimento = $nome[0]['dsTipoMovimento'];

        // atualizar a saida do estoque
        $where = "idInsumo = " . $data['idInsumo'];
        $qtExistente = $modelInsumo->getInsumo($where)[0];
        if (!$qtExistente) {
            $qtNova = $data['qtMovimento'];
        } else {
            $qtNova = $qtExistente['qtEstoque'] - $data['qtMovimento'];
        }

        $datainsumo = array(
            'idUltimoCentroCusto' => $dataitemoutros['idCentroCusto'],
            'idUltimaOS' => $dataitemoutros['idOS'],
            'idMaquina' => $dataitemoutros['idMaquina'],
            'qtEstoque ' => $qtNova                
        );
        $model->updMovimentoItem($datainsumo, $where);

        // atualiza dados do produto
        
        $datainsumo = array(
            'idUltimoMovimento' => $data['idMovimento'],
            'idInsumo' => $data['idInsumo']
        );
        if ($dsTipoMovimento == 'Venda') {
            $datainsumo['idParceiroUltimaVenda'] = $datacabec['idParceiro'];
            $datainsumo['dtUltimaVenda'] = $datacabec['dtMovimento'];
            $datainsumo['nrNotaUltimaVenda'] = $datacabec['nrNota'];
            $datainsumo['qtUltimaVenda'] = $data['qtMovimento'];
            $datainsumo['vlUltimaVenda'] = $data['vlMovimento'];
        }
//        var_dump($datainsumo); die;
        $modelInsumo->updInsumo($datainsumo);
        
        
        // pegar a origem da informacao de acordo com a descrição do tipo de movimento
        $modelOI = new origeminformacaoModel();
        $retorno = $modelOI->getOrigemInformacao("dsOrigemInformacao = '" . $dsTipoMovimento . "'");
        if ($retorno) {
            $idOrigemInformacao = $retorno[0]['idOrigemInformacao'];
        } else {
            // não tem origem da informacao, entao criar um e pegar o ID   
            $dadosorigem = array(
                'dsOrigemInformacao' => $dsTipoMovimento
            );
            $idOrigemInformacao = $modelOI->setOrigemInformacao($dadosorigem);
        }
        // sempre que tiver centro de custo, gravar um rateio e depois dar saida por aplicacao direta
        if ($dataitemoutros['idCentroCusto']) {
            $modelOI = new origeminformacaoModel();
            // gravar o rateio do valor
            $dadosaprarateio = array(
                'idRateio' => null,
                'dtRateio' => date('Y-m-d'),
                'idCentroCusto' => $dataitemoutros['idCentroCusto'],
                'idOrigemInformacao' => $idOrigemInformacao,
                'dsDocOrigem' => $dataitemoutros['nrNota'],
                'vlRateio' => $data['vlMovimento'],
                'stDC' => 'D'                    
            );
            $modelRateio = new rateioModel();
            $modelRateio->setRateio($dadosaprarateio);  

            // dar saida de estoque
            $mTeveCC = true;                    
        }
        if ($dataitemoutros['idMaquina']) {
            // dar saida de estoque
            $mTeveMaquina = true;                    
        }
        // gravar ocorrencia com a Ordem de Serviço
        if ($dataitemoutros['idOS']) {
            // dar saida de estoque
            $mTeveOS = true;   

            // pegar o tipo de ocorrencia
            $modelTO = new tipoocorrenciaModel();
            $retorno = $modelTO->getTipoOcorrencia("dsTipoOcorrencia = '" . $dsTipoMovimento . "'");
            if ($retorno) {
                $idTipoOcorrencia = $retorno[0]['idTipoOcorrencia'];
            } else {
                // não tem tipo de ocorrencia, entao criar um e pegar o ID   
                $dadostipoocorrencia = array(
                    'dsTipoOcorrencia' => $dsTipoMovimento
                );
                $idTipoOcorrencia = $modelTO->setTipoOcorrencia($dadostipoocorrencia);
            }                       
            $dadosOcorrencia = array(
                'idOcorrencia' => null,
                'dsOcorrencia' => 'Compra de ' . $data['qtMovimento'] . ' para uso ',
                'idOS' => $dataitemoutros['idOS'],
                'dtOcorrencia' => date('Y-m-d'),
                'idTipoOcorrencia' => $idTipoOcorrencia,
                'dtInicio' => date('Y-m-d'),
                'dtFim' => date('Y-m-d'),
                'idOrigemInformacao' => $idOrigemInformacao,
                'stStatus' => 1,
                'nrDocumento' => $dataitemoutros['nrNota'],
                'idMotivo' => $dataitemoutros['idCentroCusto']
            );
            $modelOcorrencias = new ocorrenciaModel();            
            $idOcorrencia = $modelOcorrencias->setOcorrencia($dadosOcorrencia);
            $dadosocorrenciainsumo = array(
                    'idOcorrenciasInsumo' => null,
                    'idOcorrencia' => $idOcorrencia,
                    'idOS' => $dataitemoutros['idOS'],
                    'idInsumo' => $data['idInsumo'],
                    'qtUtilizada' => $data['qtMovimento'],
                    'vlInsumo' => $data['vlMovimento'],
                    'stStatus' => 1
                    );
            $modelOcorrencias->setOcorrenciaInsumo($dadosocorrenciainsumo);
        }
        
            
        $jsondata["html"] = "saidaestoque/lista.tpl";
        $jsondata["idMovimentoItem"] = $data['idMovimento'];
        $jsondata["ok"] = true;
        echo json_encode($jsondata);
    }
    
    
    //Trata dados antes de Enviar para o Gravar
    private function trataPost($post) {
//        var_dump($post); die;
        $data = array();
        $data['idMovimento'] = ($post['idMovimento'] != '') ? $post['idMovimento'] : null;
        $data['idParceiro'] = ($post['idParceiro'] != '') ? $post['idParceiro'] : null;
        $data['idColaborador'] = ($post['idColaborador'] != '') ? $post['idColaborador'] : null;
        $data['idTipoMovimento'] = ($post['idTipoMovimento'] != '') ? $post['idTipoMovimento'] : null;
        $data['dsObservacao'] = ($post['dsObservacao'] != '') ? $post['dsObservacao'] : null;
        $data['dtMovimento'] = ($post['dtMovimento'] != '') ? date("Y-m-d", strtotime(str_replace("/", "-", $_POST["dtMovimento"]))) : date('Y-m-d h:m:s');
        $data['nrNota'] = ($post['nrNota'] != '') ? $post['nrNota'] : null;
        $data['nrPedido'] = ($post['nrPedido'] != null) ? $post['nrPedido'] : null;
        $data['idUsuario'] = $_SESSION['user']['usuario'];
        return $data;
    }

    private function trataPostItem($post) {
        $data = array();
        $data['idMovimentoItem'] = null;
        $data['idInsumo'] = ($post['idInsumo'] != '') ? $post['idInsumo'] : null;
        $data['qtMovimento'] = ($post['qtMovimento'] != '') ? str_replace(",",".",str_replace(".","",$post['qtMovimento'])) : null;
        $data['vlMovimento'] = ($post['vlMovimento'] != '') ? str_replace(",",".",str_replace(".","",$post['vlMovimento'])) : null;
        $data['idMovimento'] = ($post['idMovimento'] != '') ? $post['idMovimento'] : null;
        $data['idTipoMovimento'] = ($post['idTipoMovimento'] != '') ? $post['idTipoMovimento'] : null;
        $data['idLocalEstoque'] = ($post['idLocalEstoque'] != '') ? $post['idLocalEstoque'] : null;
        $data['idCentroCusto'] = ($post['idCentroCusto'] != '') ? $post['idCentroCusto'] : null;
        $data['idSetor'] = ($post['idSetor'] != '') ? $post['idSetor'] : null;
        $data['idMaquina'] = ($post['idMaquina'] != '') ? $post['idMaquina'] : null;
        $data['idOS'] = ($post['idOS'] != '') ? $post['idOS'] : null;
        $data['idMotivo'] = ($post['idMotivo'] != '') ? $post['idMotivo'] : null;
        $data['idLocalEstoqueDestino'] = ($post['idLocalEstoqueEntrada'] != '') ? $post['idLocalEstoqueEntrada'] : null;
        $data['dsObservacao'] = ($post['dsObservacao'] != '') ? $post['dsObservacao'] : null;
        return $data;
    }

    private function trataPostItemOutros($post) {
        $dataoutros = array();
        $dataoutros['idTipoMovimento'] = ($post['idTipoMovimento'] != '') ? $post['idTipoMovimento'] : null;
        $dataoutros['idParceiro'] = ($post['idParceiro'] != '') ? $post['idParceiro'] : null;
        $dataoutros['idCentroCusto'] = ($post['idCentroCusto'] != '') ? $post['idCentroCusto'] : null;
        $dataoutros['idMaquina'] = ($post['idMaquina'] != '') ? $post['idMaquina'] : null;
        $dataoutros['idOS'] = ($post['idOS'] != '') ? $post['idOS'] : null;
        $dataoutros['idMotivo'] = ($post['idMotivo'] != '') ? $post['idMotivo'] : null;
        $dataoutros['idLocalEstoque'] = ($post['idLocalEstoque'] != '') ? $post['idLocalEstoque'] : null;
        $dataoutros['idLocalEstoqueDestino'] = ($post['idLocalEstoqueEntrada'] != '') ? $post['idLocalEstoqueEntrada'] : null;
        $dataoutros['nrNota'] = ($post['nrNota'] != '') ? $post['nrNota'] : null;
        return $dataoutros;
    }
    // Remove Padrao
    public function delmovimentoitem() {                
        $idMovimentoItem = $_POST['idMovimentoItem'];
        $model = new saidaestoqueModel();
        $where = 'idMovimentoItem = ' . $idMovimentoItem;             
        $model->delMovimentoItem($where);
        $jsondata["ok"] = true;
        echo json_encode($jsondata);        
    }

    public function delmovimento() {
        $sy = new system\System();
        
        $idMovimento = $sy->getParam('idMovimento');
        $model = new entradaestoqueModel();
        $where = 'idMovimento = ' . $idMovimento;
        
        $ok = $model->getTotalMovimentoItensE($where);
        if (!$ok) {
            $model->delMovimentoItem($where);
            $model->delMovimento($where);
            header('Location: /saidaestoque');        
        } else {
            header('Location: /saidaestoque/novo_saidaestoque/idMovimento/' . $idMovimento);        
        }
        
        return;
    }
        
    public function relatoriosaidaestoque_pre() {
        $this->template->run();

        $this->smarty->assign('title', 'Pre Relatorio de Movimentos');
        $this->smarty->display('saidaestoque/relatorio_pre.html');
    }

    public function relatoriosaidaestoque() {
        $this->template->run();

        $model = new saidaestoqueModel();
        $saidaestoque_lista = $model->getMovimento();
        //Passa a lista de registros
        $this->smarty->assign('saidaestoque_lista', $saidaestoque_lista);
        $this->smarty->assign('titulo_relatorio');
        //Chama o Smarty
        $this->smarty->assign('title', 'Relatorio de Movimentos');
        $this->smarty->display('saidaestoque/relatorio.html');
    }

    public function getParceiro() {
        $url = explode("=", $_SERVER['REQUEST_URI']);
        $key = str_replace('+', ' ', $url[1]);
        if (!empty($key)) {
          $busca = trim($key);
          $model = new parceiroModel();
          $where = "(UPPER(dsParceiro) like UPPER('%{$key}%') OR UPPER(cdCNPJ) like UPPER('%{$key}%'))";
          $retorno = $model->getParceiro($where);
          $return = array();
          if (count($retorno)) {
            $row = array();
            for ($i = 0; $i < count($retorno); $i++) {
              $cnpj = $retorno[$i]["cdCNPJ"];
              $row['value'] = strtoupper($retorno[$i]["dsParceiro"]) . '-' . $cnpj;
              $row["id"] = $retorno[$i]["idParceiro"];
              array_push($return, $row);
            }
          }
          echo json_encode($return);
        }
    }    
    
    public function getProduto() {
        $url = explode("=", $_SERVER['REQUEST_URI']);
        $key = str_replace('+', ' ', $url[1]);
        if (!empty($key)) {
          $busca = trim($key);
          $model = new insumoModel();
          $where = "(UPPER(a.dsInsumo) like UPPER('%{$key}%') OR UPPER(a.cdInsumo) like UPPER('%{$key}%'))";
          $retorno = $model->getInsumo($where);
          $return = array();
          if (count($retorno)) {
            $row = array();
            for ($i = 0; $i < count($retorno); $i++) {
              $cdInsump = $retorno[$i]["cdInsumo"];
              $row['value'] = strtoupper($retorno[$i]["dsInsumo"]) . '-' . $cdInsump;
              $row["id"] = $retorno[$i]["idInsumo"];
              array_push($return, $row);
            }
          }
          echo json_encode($return);
        }        
    }        
}

?>