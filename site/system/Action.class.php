<?php

namespace system;

use controller;
use menuModel;
use paginacao;
use system\Request;
use util;

/**
* Controla acao recebida pelo controller
*/
class Action extends Request
{

    protected $controller = null;
    protected $title = null;

    /**
    * Controi acao apatir de um controller
    * @param controller $controller
    */
    public function __construct(controller $controller)
    {
        parent::__construct($_REQUEST);
        global $CONTAINER;
        $this->container = $CONTAINER;
        $this->controller = $controller;
        $this->smarty = $this->controller->smarty;
        $this->setPageLocal();
        $this->init();
    }

    public function getEntityManager()
    {
        global $CONTAINER;
        return $CONTAINER->entityManager;
    }

    protected function getTagHelper(\system\Entity $entity)
    {
        return new \TecnologiaFox\GestorFox\System\Tag\TagHelper($entity);
    }

    public function getParamPost($name = null, $upper = false)
    {
        return $this->controller->getParamPost($name, $upper);
    }

    public function getParam($name = null, $upper = false)
    {
        return $this->controller->getParam($name, $upper);
    }

    /**
    * Excuta apos controir classe
    * @return null
    */
    public function init()
    {
        return null;
    }

    /**
    * Controi html conforme template
    * @param string $template
    */
    public function renderTemplate($template)
    {
        $this->controller->smarty->assign('template', $template);
        $this->controller->smarty->display('comuns/standard/template.tpl');
    }

    /**
    * Controi paginacao conforme registros
    * @param array $data
    * @param string $porPagina
    */
    protected function renderPaginacao(array &$data, $porPagina = 10)
    {
        $pagina = $this->getInt("pagina");
        $paginacao = new paginacao();
        $registros = $paginacao->prepara($data, (int) $porPagina, $pagina);
        if (is_array($registros)) {
            if (isset($registros["paginacao"])) {
                $this->controller->smarty
                ->assign(
                    "paginacao",
                    $paginacao->montarPaginacao($registros["paginacao"], 5)
                );
            }
            if (isset($registros["regs"])) {
                $data = $registros["regs"];
            }
        }
    }

    /**
    * Controi titulo da pagina conforme URL
    * @return Array
    */
    private function setPageLocal()
    {
        $local = array();
        if ($url = $this->getUrl()) {
            $data = explode('/', $url);
            if (isset($data[0])) {
                $modelMenu = new menuModel();
                if ($rowSet = $modelMenu
                    ->getMenu("M.stat = 1 AND M.url = '/{$data[0]}'")) {
                    $local = end($rowSet);
                    //PROCESSO PARA TRADUÇÃO DO TÍTULO DO TEMPLATE
                    $traducao = \util::translate(
                        'erp_menu',
                        'des',
                        $local['id_menu']
                    );
                    if ($traducao) {
                        $local["des"] = $traducao;
                    }
                    //$local['numero_menu'] = sprintf(
                    // "%03s", $local['numero_menu']);
                }
            }
        }
        $this->controller->smarty->assign('local', $local);
        return $local;
    }

    /**
    * Função para adicionar alerta de messagem por SESSION
    * $type: success (DEFAULT), error, warning
    * $msg: REGISTTRO SALVO COM SUCESSO (DEFAULT)
    */
    public function alertMessage(
        $type = 'success',
        $msg = 'REGISTRO SALVO COM SUCESSO'
    ) {
        $_SESSION['retorno'] = ['tipo' => $type, 'msg' => $msg];
        return true;
    }


    /**
    * render fe_stat and stat
    * @param array $data
    */
    protected function renderFeStat(array &$data, $tableName)
    {
        if ($data && !is_null($data)) {
            if (isset($data['fe_stat']) && !empty($data['fe_stat'])) {
                $data['fe_stat_des'] = '';
                if ($fe = util::getListFromFeatureExpression(
                    $tableName,
                    'fe_stat',
                    $data['fe_stat'],
                    false
                )) {
                    $data['fe_stat_des'] = $fe[$data['fe_stat']];
                };
            } elseif (isset($data['stat']) && !empty($data['stat'])) {
                $data['stat_des'] = '';
                if ($fe = util::getListFromFeatureExpression(
                    $tableName,
                    'stat',
                    $data['stat'],
                    false
                )) {
                    $data['stat_des'] = $fe[$data['stat']];
                };
            }
        }
    }

    /**
     * Funcao para renderizar uma VIEW passando como paramentros
     * @viewFile = Arquivo TPL para ser renderizado
     * @arrDataView = array contendo as variaveis que serao utilizadas
     */
    protected function renderView($viewFile, $arrDataView = null)
    {
        $this->controller->smarty->assign($arrDataView);
        $this->renderTemplate($this->controller->smarty->fetch($viewFile));
    }
}
