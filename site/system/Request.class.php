<?php

namespace system;

use util as util;

/**
 * Controla e formata _REQUEST recebidos em um controller
 *
 */
class Request
{

    private $data;
    private $exceptions = array("url");

    /**
     * Controi a class apartir do _REQUEST
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        foreach ($data as $key => $value) {
            if (!in_array($key, $this->exceptions)) {
                $this->data[$key] = $this->patternData($value);
            } else {
                $this->data[$key] = $value;
            }
        }
        $this->renderGET();
    }

    /**
     * Verifica e retorna texto e data
     *
     * @param string $text
     * @param string $format
     * @return string
     */
    public function formatDate($text, $format = "d/m/Y")
    {
        $result = $text;
        if (!empty($text)) {
            if ($time = strtotime($text)) {
                $result = Date($format, $time);
            }
        }
        return $result;
    }

    /**
     * Verifica e retorna texto e o formate de moeda
     *
     * @param string $text
     * @return string
     */
    public function formatReal($text)
    {
        $result = $text;
        if (!empty($text)) {
            $result = number_format($text, 2, ',', '.');
        }
        return $result;
    }

    /**
     * Verifica e retorna texto e uma URL
     *
     * @return strinfg
     */
    public function getUrl()
    {
        return $this->getValue("url");
    }

    /**
     * Verifica e retorna texto e um boleano
     *
     * @param string $key
     * @return bool
     */
    public function getBool($key)
    {
        return ((bool)$this->getValue($key)) ? 1 : 0;
    }

    /**
     * Verifica e retorna de texto e um texto
     *
     * @param string $key
     * @param int $lenght
     * @return string
     */
    public function getString($key, $lenght = 0)
    {
        $value = $this->getValue($key, $lenght);
        if (!is_string($value)) {
            $value = null;
        }
        return util::letrasMaiusculaSemAcento($value);
    }

    /**
     * Verifica e retorna se texto e um email
     *
     * @param string $key
     * @param int $lenght
     * @return string
     */
    public function getEmail($key, $lenght = 0)
    {
        $value = $this->getValue($key, $lenght);
        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
            $value = null;
        }
        return strtolower($value);
    }

    /**
     * Verifica e retorna se text e um CEP
     *
     * @param string $key
     * @return string
     */
    public function getCEP($key)
    {
        $value = null;
        $metadata = $this->getValue($key, 9);
        if (preg_match("/^(\d{5})-(\d{3})$/", $metadata)) {
            $value = preg_replace("/(-)/s", null, $metadata);
        }
        return $value;
    }

    /**
     * Verifica e retorna se text e um Inscricao Estadual
     *
     * @param string $key
     * @return string
     */
    public function getIE($key)
    {
        $value = null;
        $metadata = $this->getValue($key, 16);
        if (!empty($metadata)) {
            $value = $metadata;
        }
        return $value;
    }

    /**
     * Verifica e retorna se text e um CNPJ
     *
     * @param string $key
     * @return string
     */
    public function getCNPJ($key)
    {
        $value = null;
        $metadata = $this->getValue($key, 18);
        if (preg_match("/^(\d{2})\.(\d{3})\.(\d{3})\/(\d{4})-(\d{2})$/", $metadata)) {
            $value = preg_replace("/(\.)|(\/)|(-)/s", null, $metadata);
        }
        return $value;
    }

    /**
     * Verifica e retorna se text e um CPF
     *
     * @param string $key
     * @return string
     */
    public function getCPF($key)
    {
        $value = null;
        $metadata = $this->getValue($key, 14);
        if (preg_match("/^(\d{3})\.(\d{3})\.(\d{3})-(\d{2})$/", $metadata)) {
            $value = preg_replace("/(\.)|(-)/s", null, $metadata);
        } else if (preg_match('/^(\d{11})$/', $metadata)) {
            $value = $metadata;
        }
        return $value;
    }

    /**
     * Verifica e retorna se text e um CNPJ / CPF
     *
     * @param string $key
     * @return string
     */
    public function getCNPJCPF($key)
    {
        $value = null;
        if ($data = $this->getCNPJ($key)) {
            $value = $data;
        } else if ($data = $this->getCPF($key)) {
            $value = $data;
        }
        return $value;
    }

    /**
     * Verifica e retorna se text e um Inteiro
     * @param $key
     * @param int $length
     * @param bool $returnNull
     * @return int|null
     */
    public function getInt($key, $length = 0, $returnNull = false)
    {
        $value = (int)$this->getValue($key, $length);
        if (!is_integer($value)) {
            $value = null;
        }
        if ($value == '0' && $returnNull) {
            $value = null;
        }
        return $value;
    }

    public function getIntOrNull($key, $length = 0)
    {
        $value = $this->getValue($key, $length);
        if (empty($value) and $value !== '0') {
            return null;
        }
        $value = (int)$value;
        if (!is_integer($value)) {
            return null;
        }
        return $value;
    }

    /**
     * Verifica e retorna se text e um Inscricao Estadual
     *
     * @param string $key
     * @param int $lenght
     * @return float
     */
    public function getNumeric($key, $lenght = 0)
    {
        $value = 0.00;
        $metadata = $this->getValue($key, $lenght);
        $ponto = str_replace(".", null, $metadata);
        $virgula = str_replace(",", ".", $ponto);
        if ((bool)$virgula) {
            $value = $virgula;
        }
        return $value;
    }

    /**
     * Verifica e retorna se text no formato da Tempo
     *
     * @param string $key
     * @return string
     */
    public function getTime($key)
    {
        $value = null;
        $metadata = $this->getValue($key);
        if (preg_match("/^(\d{1,})\:(\d{2})$/", $metadata)) {
            $value = $metadata;
        }
        return $value;
    }

    /**
     * Verifica e retorna se texto e um numero com virgula
     *
     * @param string $key
     * @param int $lenght
     * @return string
     */
    public function getNumericVirgula($key, $lenght = 0)
    {
        $value = 0.00;
        $metadata = $this->getValue($key, $lenght);
        $ponto = str_replace(",", ".", str_replace('.', null, $metadata));
        if ((bool)$ponto) {
            $value = $ponto;
        }
        return $value;
    }

    /**
     * Verifica e retorna se texto e um timestamp
     *
     * @param string $key
     * @return int
     */
    public function getDateUnixtime($key)
    {
        $value = "";
        $metadata = $this->getValue($key, 10);
        if (!empty($metadata)) {
            $date = preg_replace("/^(\d{1,2})\/(\d{1,2})\/(\d{4})$/", "$3-$2-$1T00:00:00", $metadata);
            if ($time = strtotime($date)) {
                $value = $time;
            }
        }
        return $value;
    }

    /**
     * Verifica e retorna se campo e uma data
     *
     * @param string $key
     * @return string
     */
    public function getDateFormat($key)
    {
        $value = "";
        $metadata = $this->getValue($key, 10);
        if (!empty($metadata) && preg_match("/^(\d{1,2})\/(\d{1,2})\/(\d{4})$/", $metadata)) {
            $date = preg_replace("/^(\d{1,2})\/(\d{1,2})\/(\d{4})$/", "$3-$2-$1T00:00:00", $metadata);
            if ($time = strtotime($date)) {
                $value = $date;
            }
        }
        return $value;
    }

    /**
     * Verifica e retorna se texto e uma data e adciona dias
     *
     * @param string $key
     * @return string
     */
    public function getDatePlusDays($key)
    {
        $value = "";
        $metadata = $this->getValue($key, 10);
        if (!empty($metadata)) {
            $value = date('Y-m-d', strtotime(' + ' . $metadata . ' days'));
        }
        return $value;
    }

    /**
     * Verifica e retorna texto e um horario
     *
     * @param string $key
     * @return string
     */
    public function getHourFormat($key)
    {
        $value = "";
        $metadata = $this->getValue($key, 8);
        if (!empty($metadata)) {
            if (preg_match("/^(\d{2}):(\d{2}):(\d{2})$/", $metadata)) {
                $value = $metadata;
            }
        }
        return $value;
    }

    /**
     * Verifica e retorna texto e um telefone
     *
     * @param string $key
     * @param int $lenght
     * @return int
     */
    public function getPhone($key, $lenght = 0)
    {
        $datavalue = $this->getValue($key);
        $datareplace = preg_replace("/[^0-9]/s", "", $datavalue);
        return (strlen($datareplace) <= $lenght) ? $datareplace : null;
    }

    /**
     * Verifica e retorna texto e uma Array
     *
     * @param string $key
     * @param int $lenght
     * @return array
     */
    public function getArray($key, $lenght = 0)
    {
        $value = array();
        $metadata = $this->getValue($key, $lenght);
        if (is_array($metadata)) {
            $value = $metadata;
        }
        return $value;
    }

    /**
     * Verifica e retorna texto e um texto livre
     *
     * @param string $key
     * @param int $lenght
     * @return string
     */
    public function getValue($key, $lenght = 0)
    {
        $value = "";
        if (isset($this->data[$key])) {
            $strlen = (is_array($this->data[$key])) ? count($this->data[$key]) : strlen($this->data[$key]);
            if (!(bool)$lenght || ((int)$strlen <= (int)$lenght)) {
                $value = $this->data[$key];
            }
        }
        return $value;
    }

    /**
     * Torna as vaiaveis em maiusculo
     *
     * @param string $string
     * @return string
     */
    private function patternData($string)
    {
        if (!is_array($string) && !is_object($string)) {
            $utilClass = new util();
            //$trata_string = preg_replace('/[`^~\'"]/', null, iconv('UTF-8', 'ASCII//TRANSLIT', $string));
            $trata_string = addslashes($utilClass->letrasMaiusculaSemAcento($string));
            return trim($trata_string);
        }
        return $string;
    }

    /**
     * Formata as variaveis que vem por URL
     */
    private function renderGET()
    {
        if (isset($this->data["url"]) && !empty($this->data["url"])) {
            if ($explode = explode('/', $this->data["url"])) {
                unset($explode[0], $explode[1]);
                $args = array_values($explode);
                if (count($args)) {
                    for ($i = 0; $i < count($args); $i++) {
                        if (isset($args[($i + 1)])) {
                            $this->data[$args[$i]] = $args[++$i];
                        }
                    }
                }
            }
        }
    }

    /**
     * Nao faz nada
     * @param array $array
     */
    public function array_not_indice(array &$array)
    {
        $array = isset($array[0]) ? $array[0] : $array;
    }

    /**
     * Formata um texto em um numero
     *
     * @param string $key
     * @return float
     */
    public function formatNumeric($key)
    {
        $value = 0.00;
        $ponto = str_replace(".", null, $key);
        $virgula = str_replace(",", ".", $ponto);
        if ((bool)$virgula) {
            $value = $virgula;
        }
        return $value;
    }

    /**
     * Formata data
     *
     * @param string $key
     * @return string
     */
    public function _formatDate($key)
    {
        $value = "";
        if (!empty($key) && preg_match("/^(\d{1,2})\/(\d{1,2})\/(\d{4})$/", $key)) {
            $date = preg_replace("/^(\d{1,2})\/(\d{1,2})\/(\d{4})$/", "$3-$2-$1T00:00:00", $key);
            if ($time = strtotime($date)) {
                $value = $date;
            }
        }
        return $value;
    }

    // Returns value only if matches regex, NULL otherwise
    public function getRegex($key, $regex)
    {
        $value = $this->getValue($key);
        if (preg_match($regex, $value) === 1) {
            return $value;
        } else {
            return NULL;
        }
    }

}
