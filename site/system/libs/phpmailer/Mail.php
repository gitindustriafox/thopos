<?php

// include('class.phpmailer.php');

class Mail {

    private $mail;

    function __construct() {
        global $CFG;
        $this->mail = new PHPMailer();
        $this->mail->IsSMTP();
        $this->mail->Host = 'smtp.tecnologiafox.com';
        $this->mail->SMTPAuth = false;
        $this->mail->SMTPDebug = 1;
        $this->mail->SMTPSecure = 'STARTTLS';            
        $this->mail->Port = 587;
        $this->mail->Username = 'JOSE DOS REIS LEMOS';
        $this->mail->Password = '100603rr';
        $this->mail->From = 'jose.lemos@tecnologiafox.com';
        $this->mail->FromName = 'Jose dos Reis Lemos';
        $this->mail->IsHTML('text/html');    
        $this->mail->CharSet = 'UTF-8';
    }

    function Send($to_list, $subject, $body, $addbcc = array(), $attachment = array()) {               

//        print_a($to_list);
//        var_dump($subject);
//        var_dump($body);
//        print_a($addbcc); 
//        print_a_die($attachment);
        
        $this->mail->ClearAddresses();
        
        global $CFG;
        /*
         * Se no config.json estiver configurado mail->fake_send: true, e mail->fake_email,
         * então sobrescrevemos o destinatário da mensagem
         */
        //if (isset($CFG->mail, $CFG->mail->fake_send, $CFG->mail, $CFG->mail->fake_email) and $CFG->mail->fake_send and !empty($CFG->mail->fake_email)) {
          /* Adicionamos os destinatários originais no início da mensagem */
          $concat_to = array();
          foreach ($to_list as $to) {
            if (empty($to['nome'])) {
              $to['nome'] = $to['email'];
            }
            $concat_to[] = "{$to['nome']} &lt;{$to['email']}&gt;"; 
          }
          foreach ($addbcc as $to) {
            if (empty($to['nome'])) {
              $to['nome'] = $to['email'];
            }
            $concat_to[] = "{$to['nome']} &lt;{$to['email']}&gt;"; 
          }
          $body .= "<table>
                        <tr> 
                            <td style='font-family:arial; font-size:11px; text-align:center' valign='top'>
                                <a href='http://www.tecnologiafox.com.br' target='_blank'>
                                    <img src='http:www.tecnologiafox.com/sites/default/files/fox_logo.png' border='0' />
                                </a>
                            </td>
                            <td>

                            </td>
                            <td style='font-family:arial; font-size:10px; padding-left:10px'>
                                <table>
                                    <tr> 
                                        <td style='font-family:arial; font-size:10px; padding-left:10px'>
                                                <br />
                                                <strong>José dos Reis Lemos</strong><br />
                                                <strong>Coordenador Suporte TI</strong><br />
                                                <br />
                                                Rod. D. Gabriel Paulino Bueno Couto, 1.800 - KM 87,5<br />
                                                Bairro Pedregulho - CEP 13318-000 - Cabreúva-SP, Brasil<br />
                                                Fone:  (11) 97259-3753<br />
                                                <br />
                                                e-mail: jose.lemos@tecnologiafox.com<br />
                                                web: <a href='http://www.tecnologiafox.com.br' target='_blank'> tecnologiafox.com.br </a> <br />
                                                skype: jreislemos@outlook.com<br />
                                       </td>
                                    </tr>
                                </table>	
                            </td> 
                        </tr>
                    </table>" ;
          $body .= implode($concat_to, ",<br/>");
          $body .= ".</pre><hr/>";
//          $this->mail->AddAddress($CFG->mail->fake_email, $CFG->mail->fake_email);
//        } else {
//          /* Senão, adicionamos os destinatários normalmente passados via parâmetro */
          foreach ($to_list as $to) {
              $this->mail->AddAddress($to["email"], $to["nome"]);
          }
          foreach ($addbcc as $bbc) {
              $this->mail->AddBcc($bbc["email"]);
          }
//        }

        $this->mail->Subject = $subject;
        $this->mail->Body = $body;
        
        foreach ($attachment as $att) {            
            $this->mail->AddAttachment($att['path_anexo'], $att['nome_anexo']);
        }

        /* Se no config.json estiver configurado mail->fake_send: true e não
         * estiver configurado o mail->fake_email, então não enviamos o e-mail
         */
//        if (isset($CFG->mail, $CFG->mail->fake_send) and $CFG->mail->fake_send and (!isset($CFG->mail->fake_email) or empty($CFG->mail->fake_email))) {
//          error_log('EMAIL FAKE SEND (phpmailer/Mail.php): ' . json_encode(array('recipients' => $to_list, 'subject' => $subject, 'body' => $body)));
//        } else {
          if (!$this->mail->Send()) {
              return false;
          }
//        }
        return true;
    }
}
