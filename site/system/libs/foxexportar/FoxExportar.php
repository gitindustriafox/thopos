<?php

class FoxExportar
{
    private $file = null;
    private $formato = null;
    private $storage = null;
    private $storageFolder = 'foxexportar';
    private $filename = null;
    private $memory = 512;
    private $max_execution_time = null;
    private $memory_limit = null;
    private $timeout_file = 30;

    public function __construct($formato, $storage, $memory = 0)
    {
        if (file_exists($storage) && is_dir($storage)) {
            $tmp = preg_replace("/^(.*)(\/|\\\\)$/", '$1', $storage).DIRECTORY_SEPARATOR.$this->storageFolder;
            @mkdir($tmp, true);
            if (file_exists($tmp) && is_dir($tmp)) {
                @chmod($tmp, 0777);
                if (is_readable($tmp) && is_writable($tmp)) {
                    $this->storage = $tmp;
                    $this->formato = strtoupper($formato);
                }
            }
        }
        if (is_integer($memory) && (bool) $memory) {
            $this->memory = $memory;
        }
        if (!preg_match('/(CSV|HTML|ODS|PDF)/i', $this->formato)) {
            die('O FORMATO INFORMADO NAO E VALIDO');
        }
        if (empty($this->storage)) {
            die("SEM PERMISSAO DE LEITURA NO DIRETORIO \"{$storage}\"");
        }
        $this->max_execution_time = ini_get('max_execution_time');
        $this->memory_limit = ini_get('memory_limit');
    }

    public function getFilename()
    {
        return $this->filename;
    }

    public function getFilenameInStorage()
    {
        return $this->storageFolder.DIRECTORY_SEPARATOR.$this->filename;
    }

    public function getPathFilename()
    {
        return $this->storage.DIRECTORY_SEPARATOR.$this->filename;
    }

    public function setHeader($metadata)
    {
        if ($this->file instanceof SplFileObject) {
            $this->file->fwrite($metadata.PHP_EOL);
            $this->file->fflush();
        }
    }

    public function setFooter($metadata)
    {
        if ($this->file instanceof SplFileObject) {
            $this->file->fwrite($metadata.PHP_EOL);
            $this->file->fflush();
        }
    }

    public function prepare()
    {
        @ini_set('max_execution_time', '0');
        @ini_set('memory_limit', "{$this->memory}M");
        if ($this->filename = $this->renderFilename($this->formato)) {
            //Verifica se existe o processo rodando no servidor
            if ('PDF' == $this->formato) {
                $checkProcess = exec("ps caux | grep 'wkhtmltopdf' 2>&1");
                if (strlen($checkProcess) > 4) {
                    return false;
                }
            }
            //Cria o arquivo inicial
            $this->file = new SplFileObject($this->storage.DIRECTORY_SEPARATOR.$this->filename, 'w+');
            @chmod($this->file->getPathname(), 0777);
        }

        return true;
    }

    public function forceDownload($path)
    {
        if (file_exists($path)) {
            header('Content-Transfer-Encoding: binary');
            header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($path)).' GMT');
            header('Accept-Ranges: bytes');
            header('Content-Length: '.filesize($path));
            header('Content-Encoding: none');
            header("Content-Type: application/{$this->formato}");
            header('Content-Disposition: attachment; filename='.$this->getFilename());
            readfile($path);
            unlink($path);
        }
    }

    public function flushArray(array $data)
    {
        switch ($this->formato) {
            case 'CSV':
            case 'ODS':
                $this->flushArrayCSV($data);
                break;
            case 'HTML':
            case 'PDF':
                $this->flushArrayHTML($data);
                break;
        }
    }

    private function flushArrayCSV(array $data)
    {
        if ($this->file instanceof SplFileObject) {
            $metadata = '"'.implode('";"', $data).'";'.PHP_EOL;
            $this->file->fwrite($metadata);
            $this->file->fflush();
        }
    }

    private function flushArrayHTML(array $data)
    {
        if ($this->file instanceof SplFileObject) {
            $this->file->fwrite('<tr>'.PHP_EOL);
            $metadata = '<td>'.implode('</td>'.PHP_EOL.'<td>', $data).'</td>'.PHP_EOL;
            $this->file->fwrite($metadata);
            $this->file->fwrite('</tr>'.PHP_EOL);
            $this->file->fflush();
        }
    }

    public function flush($metadata)
    {
        switch ($this->formato) {
            case 'CSV':
            case 'ODS':
                $this->flushCSV($metadata);
                break;
            case 'HTML':
            case 'PDF':
                $this->flushHTML($metadata);
                break;
        }
    }

    private function flushCSV($metadata)
    {
    }

    private function flushHTML($metadata)
    {
        if ($this->file instanceof SplFileObject) {
            $this->file->fwrite($metadata);
            $this->file->fflush();
        }
    }

    public function close($output = true, $orientation = 'Portrait')
    {
        if ($this->file instanceof SplFileObject) {
            $this->file->fflush();
            $this->file = null;
        }

        switch ($this->formato) {
            case 'PDF':
                //Conversão do html para PDF através do linux
                $this->pdfConvert($output, $orientation);
                break;
            case 'CSV':
            case 'HTML':
            case 'ODS':
                break;
        }

        @ini_set('max_execution_time', $this->max_execution_time);
        @ini_set('memory_limit', $this->memory_limit);
    }

    private function renderFilename($formato)
    {
        $filename = null;
        if (!empty($formato)) {
            sleep(1);
            $filename = strtoupper($formato);
            $filename .= '_'.session_id();
            $filename .= '_'.time();
            $filename .= '.'.strtolower($formato);
        }

        return $filename;
    }

    /**
     * Export the HTML file to PDF
     * The script runs in the directory where the attribute $filename in $storage, and does the .html file to .pdf.
     */
    private function pdfConvert($output = true, $orientation = 'Portrait')
    {
        global $CFG;
        set_time_limit(0);
        $fileName = pathinfo($this->filename, PATHINFO_FILENAME);
        $fileNameTmp = 'tmp_'.session_id().'.html';

        //Renomear o arquivo
        exec('mv '.$this->storage.DIRECTORY_SEPARATOR.$this->filename.' '.$this->storage.DIRECTORY_SEPARATOR.$fileNameTmp);

        //Comando
        //die("{$CFG->standard->wkhtmltopdf} -T10 -O $orientation " . $this->storage . DIRECTORY_SEPARATOR . $fileNameTmp . ' ' . $this->storage . DIRECTORY_SEPARATOR . $fileName . '.pdf 2>&1');
        $handle = popen("{$CFG->standard->wkhtmltopdf} --minimum-font-size 2 -T 5 -R 5 -B 5 -L 5 -O $orientation ".$this->storage.DIRECTORY_SEPARATOR.$fileNameTmp.' '.$this->storage.DIRECTORY_SEPARATOR.$fileName.'.pdf 2>&1', 'r');

        //Verifico se o buffer está aberto
        if (ob_get_level() == 0) {
            ob_start();
        }

        //Leitura do que está sendo escrito no buffer
        while (!feof($handle)) {
            $read = fread($handle, 8192);
            if ($output) {
                echo "{$read}<br />";
            }
            usleep(1500000);

            if ($output) {
                ob_flush();
                flush();
            }
        }

        //Fechamento do buffer
        pclose($handle);

        if ($output) {
            ob_end_flush();
        } else {
            ob_end_clean();
        }

        //Removendo HTML
        unlink($this->storage.DIRECTORY_SEPARATOR.$fileNameTmp);
    }
}
