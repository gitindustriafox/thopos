<?php

function smarty_modifier_value_format($string, $format) {
    $data = $string;
    if ((bool) $data) {
        if (isset($format["-"], $format["+"])) {
            $replace = str_replace("-", "", $string);
            $convert = doubleval($replace);
            $data = number_format($convert, 2, ",", "");
            if (doubleval($string) < 0) {
                $data .= $format["-"];
            } else {
                $data .= $format["+"];
            }
        }
    } else {
        $data = "-";
    }
    return $data;
}
