<?php

use kernel\Lang;

function smarty_modifier_lang($namespace, $local) {
    return Lang::get($namespace, $local);
}
