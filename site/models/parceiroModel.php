<?php

class parceiroModel extends model {

    var $tabPadrao = 'prodParceiro';
    var $campo_chave = 'idParceiro';

    //Estrutura da Tabela Vazia Utilizada para novos Cadastros
    public function estrutura_vazia() {
        $dados = null;
        $dados[0]['idParceiro'] = NULL;
        $dados[0]['dsParceiro'] = NULL;
        return $dados;
    }
    
    public function getParceiro($where = null, $paginacao=null) {
        $tables = 'prodParceiro as a';
        $tables .= ' left join prodTipoParceiro as d on d.idTipoParceiro = a.idTipoParceiro';
        $orderby = 'a.dsParceiro';
        return $this->read($tables, array('a.*', 'd.dsTipoParceiro'), $where, null, null, null, $orderby, null, $paginacao);
    }

    //Grava o perfil
    public function setParceiro($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert($this->tabPadrao, $array, false));
        $this->commit();
        return $id;
    }

    //Atualiza o Log
    public function updParceiro($array) {
        //Chave    
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->update($this->tabPadrao, $array, $where));
        $this->commit();
        return true;
    }

    //Remove perfil    
    public function delParceiro($array) {
        //Chave
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->delete($this->tabPadrao, $where, true));
        $this->commit();
        return true;
    }
}
?>
