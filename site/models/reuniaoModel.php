<?php

class reuniaoModel extends model {

    var $tabPadrao = 'prodReuniao';
    var $campo_chave = 'idReuniao';

    //Estrutura da Tabela Vazia Utilizada para novos Cadastros
    public function estrutura_vazia() {
        $dados = null;
        $dados[0]['idReuniao'] = NULL;
        $dados[0]['dsAssunto'] = NULL;
        $dados[0]['dsAta'] = NULL;
        $dados[0]['dtReuniao'] = NULL;
        return $dados;
    }

    public function getValores($where = null) {
        $table = 'prodDE';
        return $this->read($table, array('dsH as H','dsU as U','dsP as P','dsF as F'), $where);         
    }

    public function getReuniao($where = null) {
        $table = 'prodReuniao r '
                . ' left join prodUsuarios u on u.idUsuario = r.idUsuarioSolicitante'
                . ' left join prodUsuarios ur on ur.idUsuario = r.idUsuarioAta'
                . ' left join prodLocalReuniao lr on lr.idLocalReuniao = r.idLocalReuniao'
                . ' left join prodReuniaoParticipantes p on p.idReuniao = r.idReuniao';
        return $this->read($table, array('p.stPresenca','ur.dsUsuario as usuarioresponsavel','r.*','u.dsUsuario','lr.dsLocalReuniao'), $where, null, null, null, 'r.idReuniao desc',null,null,false);         
    }

    public function getSoReuniao($where = null) {
        $table = 'prodReuniao r '
                . ' left join prodUsuarios u on u.idUsuario = r.idUsuarioSolicitante'
                . ' left join prodUsuarios ur on ur.idUsuario = r.idUsuarioAta'
                . ' left join prodLocalReuniao lr on lr.idLocalReuniao = r.idLocalReuniao';
        return $this->read($table, array('ur.dsUsuario as usuarioresponsavel','r.*','u.dsUsuario','lr.dsLocalReuniao'), $where, null, null, null, 'r.idReuniao desc',null,null,false);         
    }

    public function getDatas($where = null) {
        $table = 'prodReuniaoDatas';
        return $this->read($table, array('*'), $where, null, null, null, 'dtIntermediaria');         
    }

    public function getParticipantes($where = null) {
        $table = "prodReuniaoParticipantes P inner join prodUsuarios u on u.idUsuario = P.idUsuario inner join prodReuniao r on r.idReuniao = P.idReuniao"
                . " left join prodTipoReuniao t on t.idTipoReuniao = r.idTipoReuniao"
                . " left join prodUsuarios us on us.idUsuario = r.idUsuarioSolicitante"
                . " left join prodMensagem m on P.idParticipante = m.idTabela and m.dsNomeTabela = 'prodReuniaoParticipantes'"
                . " left join prodMensagemItem mi on mi.idMensagem = m.idMensagem"
                . " left join prodLocalReuniao lr on lr.idLocalReuniao = r.idLocalReuniao";
        $groupBy = 'P.idParticipante, m.idTabela, m.dsNomeTabela';
        return $this->read($table, array('r.idUsuarioAta','r.dsTempoDuracaoReal','P.*','mi.stSituacao as leu','r.idUsuarioSolicitante','u.dsUsuario','P.dsNome','P.dsEmail as email','P.dsCelular as telefone1','P.dsAssunto as assuntoP','t.dsTipoReuniao','us.dsUsuario as UsuarioSolicitante','r.dsAssunto as dsPauta', 'r.dtReuniao','r.dsAta','r.dtRealizacao','lr.dsLocalReuniao','r.dsCaminhoPDF','r.dsTempoDuracao', 'r.stSituacao', 'P.stSituacao as ausenteoupresente'), $where, $groupBy);         
    }

    public function getParticipantesTarefa($where = null, $orderby = null) {
        $table = 'prodReuniaoTarefas P inner join prodReuniao r on r.idReuniao = P.idReuniao'
                . ' left join prodTipoReuniao t on t.idTipoReuniao = r.idTipoReuniao'
                . ' Left join prodReuniaoParticipantes part on part.idUsuario = P.idUsuario and part.idReuniao = P.idReuniao'
                . ' left join prodUsuarios us on us.idUsuario = r.idUsuarioSolicitante'
                . ' left join prodUsuarios ust on ust.idUsuario = P.idUsuario'
                . ' left join prodLocalReuniao lr on lr.idLocalReuniao = r.idLocalReuniao';
        return $this->read($table, array('r.idUsuarioAta','ust.dsUsuario as UTarefa','part.idParticipante','part.dsAssunto','P.*','P.idUsuario as idusuariotarefa','r.idUsuarioSolicitante','part.dsNome','part.dsEmail as email','part.dsCelular as telefone1','P.dsTarefa as tarefa','t.dsTipoReuniao','us.dsUsuario as UsuarioSolicitante','r.dsAssunto as dsPauta', 'r.dtReuniao','r.dtRealizacao','r.dsAta','lr.dsLocalReuniao','r.dsCaminhoPDF','r.dsTempoDuracao', 'r.stSituacao'), $where,null,null,null,$orderby,null,null,false);         
    }

    public function getParticipantesTarefaG($where = null) {
        $table = 'prodReuniaoTarefas P inner join prodUsuarios u on u.idUsuario = P.idUsuario inner join prodReuniao r on r.idReuniao = P.idReuniao'
                . ' left join prodTipoReuniao t on t.idTipoReuniao = r.idTipoReuniao'
                . ' Left join prodReuniaoParticipantes part on part.idUsuario = P.idUsuario and part.idReuniao = P.idReuniao'
                . ' left join prodUsuarios us on us.idUsuario = r.idUsuarioSolicitante'
                . ' left join prodLocalReuniao lr on lr.idLocalReuniao = r.idLocalReuniao';
        $groupby = 'u.dsUsuario';
        return $this->read($table, array('u.dsUsuario as Usuario','count(u.dsUsuario) as tarefas'), $where,$groupby,null,null,null,null,null,false);         
    }

    public function getParticipantesTarefaGStatus($where = null) {
        $table = 'prodReuniaoTarefas P inner join prodUsuarios u on u.idUsuario = P.idUsuario inner join prodReuniao r on r.idReuniao = P.idReuniao'
                . ' left join prodTipoReuniao t on t.idTipoReuniao = r.idTipoReuniao'
                . ' Left join prodReuniaoParticipantes part on part.idUsuario = P.idUsuario and part.idReuniao = P.idReuniao'
                . ' left join prodUsuarios us on us.idUsuario = r.idUsuarioSolicitante'
                . ' left join prodLocalReuniao lr on lr.idLocalReuniao = r.idLocalReuniao';
        return $this->read($table, array('count(P.stStatusTarefa) as totalstatus'), $where);         
    }

    public function getTipo($where = null) {
        $table = 'prodTipoReuniao';
        return $this->read($table, array('*'), $where);         
    }

    //Grava o perfil
    public function setReuniao($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert($this->tabPadrao, $array, false));
        $this->commit();
        return $id;
    }

    public function setParticipantes($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodReuniaoParticipantes', $array, false));
        $this->commit();
        return $id;
    }

    public function setDatas($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodReuniaoDatas', $array, false));
        $this->commit();
        return $id;
    }

    public function setTarefas($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodReuniaoTarefas', $array, false));
        $this->commit();
        return $id;
    }

    //Atualiza o Log
    public function updParticipantes($array) {
        //Chave    
        $where = "idParticipante = " . $array['idParticipante'];
        $this->startTransaction();
        $this->transaction($this->update('prodReuniaoParticipantes', $array, $where));
        $this->commit();
        return true;
    }

    public function updParticipantesPresenca($array) {
        //Chave    
        $where = "idUsuario = " . $array['idUsuario'] . ' and idReuniao = ' . $array['idReuniao'];
        $this->startTransaction();
        $this->transaction($this->update('prodReuniaoParticipantes', $array, $where));
        $this->commit();
        return true;
    }

    public function updTarefas($array) {
        //Chave    
        $where = "idTarefa = " . $array['idTarefa'];
        $this->startTransaction();
        $this->transaction($this->update('prodReuniaoTarefas', $array, $where));
        $this->commit();
        return true;
    }

    public function updDatas($array) {
        //Chave    
        $where = "idData = " . $array['idData'];
        $this->startTransaction();
        $this->transaction($this->update('prodReuniaoDatas', $array, $where));
        $this->commit();
        return true;
    }

    //Atualiza o Log
    public function updReuniao($array) {
        //Chave    
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->update($this->tabPadrao, $array, $where));
        $this->commit();
        return true;
    }

    public function updReuniaoParticipantes($array) {
        //Chave    
        $where = 'idReuniao = ' . $array['idReuniao'] . " and idParticipante =  " . $array['idParticipante'];
        $this->startTransaction();
        $this->transaction($this->update('prodReuniaoParticipantes', $array, $where));
        $this->commit();
        return true;
    }
    //Remove perfil    
    public function delReuniao($array) {
        //Chave
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->delete($this->tabPadrao, $where, true));
        $this->commit();
        return true;
    }
    public function delParticipantes($where = null) {
        //Chave
        $this->startTransaction();
        $this->transaction($this->delete('prodReuniaoParticipantes', $where, true));
        $this->commit();
        return true;
    }
    public function delTarefa($where = null) {
        //Chave
        $this->startTransaction();
        $this->transaction($this->delete('prodReuniaoTarefas', $where, true));
        $this->commit();
        return true;
    }
    public function delData($where = null) {
        //Chave
        $this->startTransaction();
        $this->transaction($this->delete('prodReuniaoDatas', $where, true));
        $this->commit();
        return true;
    }

}
?>
