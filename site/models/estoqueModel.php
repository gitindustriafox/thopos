<?php

class estoqueModel extends model {

    var $tabPadrao = 'prodEstoque';
    var $campo_chave = 'idEstoque';

    public function getEstoque($where = null) {
        $tables = 'prodEstoque as e '
                . ' left join prodInsumo i on i.idInsumo = e.idInsumo'
                . ' left join prodUnidade u on u.idUnidade = i.idUnidade';
        return $this->read($tables, array('e.*','u.dsUnidade'), $where);
    }
    
    public function getValTotalEstoque($where = null) {
        $tables = 'prodEstoque';
        $filed = array(' SUM(vlEstoque) as valortotal');
        return $this->read($tables, $filed, $where);
    }
    public function getValTotalEstoqueGrupo($where = null) {
        $tables = 'prodEstoque e left join prodInsumo i on i.idInsumo = e.idInsumo '
                . 'left join prodGrupo g on i.idGrupo = g.idGrupo ';
        $filed = array('g.dsGrupo','i.idGrupo', 'SUM(vlEstoque) as valortotal');
        $groupby = 'i.idGrupo';
        $orderby = 'SUM(vlEstoque) desc';
        return $this->read($tables, $filed, $where, $groupby,null,null, $orderby);
    }
    public function getValTotalEstoqueGrupoLE($where = null) {
        $tables = 'prodEstoque e left join prodInsumo i on i.idInsumo = e.idInsumo '
                . 'left join prodGrupo g on i.idGrupo = g.idGrupo '
                . 'left join prodLocalEstoque le on e.idLocalEstoque = le.idLocalEstoque ';
        $filed = array('g.dsGrupo','i.idGrupo', 'SUM(e.vlEstoque) as valortotal');
        $groupby = 'i.idGrupo';
        $orderby = 'SUM(vlEstoque) desc';
        return $this->read($tables, $filed, $where, $groupby,null,null, $orderby);
    }
    public function getValTotalEstoqueLocalEstoque($where = null) {
        $tables = 'prodEstoque e left join prodInsumo i on i.idInsumo = e.idInsumo '
                . 'left join prodLocalEstoque le on e.idLocalEstoque = le.idLocalEstoque ';
        $filed = array('SUM(vlEstoque) as valortotal, substr(cdLocalEstoque,1,1) as cdLocalEstoque');
        $groupby = 'substr(cdLocalEstoque,1,1)';
        $orderby = 'SUM(vlEstoque) desc';
        return $this->read($tables, $filed, $where, $groupby,null,null, $orderby);
    }
    
    public function getInsumosSemEstoque() {
        return $this->read('prodInsumo i
        LEFT JOIN
    prodEstoque e ON e.idInsumo = i.idInsumo', array('count(i.idInsumo) as totalSemEstoque'), 'isnull(e.idInsumo)');
    }
    
    public function getValTotalEstoqueInsumo($where = null) {
        $tables = 'prodEstoque e
                LEFT JOIN
            prodInsumo i ON i.idInsumo = e.idInsumo
                LEFT JOIN
            prodGrupo g ON i.idGrupo = g.idGrupo
                LEFT JOIN
            prodUnidade u ON u.idUnidade = i.idUnidade';        
        $filed = array('g.dsGrupo,i.idGrupo,SUM(e.vlEstoque) AS valortotal,SUM(e.qtEstoque) AS qtdetotal,i.dsInsumo,i.idInsumo, u.dsUnidade, SUM(e.vlEstoque) / SUM(e.qtEstoque) AS vlPME');
        $groupby = 'e.idInsumo';
        $orderby = 'SUM(vlEstoque) DESC';
        return $this->read($tables, $filed, $where, $groupby,null,null, $orderby);
    }

    public function getEstoqueAtual($where = null, $paginacao=null) {
        $tables = 'prodEstoque e'
                . ' inner join prodInsumo i on i.idInsumo = e.idInsumo'
                . ' inner join prodLocalEstoque le on le.idLocalEstoque = e.idLocalEstoque'
                . ' left join prodUnidade u on u.idUnidade = i.idUnidade'
                . ' left join prodParceiro f on f.idParceiro = i.idParceiroUltimaCompra'
                . ' left join prodParceiro fi on fi.idParceiro = i.idParceiro'
                . ' left join prodGrupo g on g.idGrupo = i.idGrupo';
        return $this->read($tables, array('e.*', 'e.qtEstoque as qtdeEstoque','(e.qtLoteReposicao - e.qtEstoque) as qtAComprar','i.*','u.dsUnidade','g.dsGrupo','le.dsLocalEstoque','le.cdLocalEstoque','f.dsParceiro as dsParceiroUC','i.dtUltimaCompra','i.qtUltimaCompra','fi.dsParceiro as dsParceiro','le.idLocalEstoqueSuperior'), $where,null,null,null,'i.dsInsumo',null,$paginacao,null,13);
    }
    public function getMapaEstoque($orderby=null,$where = null) {
        $tables = 'prodLocalEstoque le'
                . ' left join prodEstoque e on le.idLocalEstoque = e.idLocalEstoque'
                . ' left join prodInsumo i on i.idInsumo = e.idInsumo'
                . ' left join prodParceiro f on f.idParceiro = i.idParceiroUltimaCompra'
                . ' left join prodParceiro c on c.idParceiro = i.idParceiroUltimaVenda'
                . ' left join prodMovimento m on m.idMovimento = i.idUltimoMovimento'
                . ' left join prodTipoMovimento tm on tm.idTipoMovimento = m.idTipoMovimento';
        return $this->read($tables, array('e.*','m.dtMovimento','tm.dsTipoMovimento','c.dsParceiro as dsParceiroUltimaVenda','f.dsParceiro as dsParceiroUltimaCompra','i.qtUltimaVenda','i.nrNotaUltimaVenda','i.dtUltimaCompra','i.qtUltimaCompra','i.nrNotaUltimaCompra','i.dtUltimaVenda','le.cdLocalEstoque' ,'i.dsInsumo','i.cdInsumo','e.qtEstoque as qtdeEstoque','le.dsLocalEstoque','le.idLocalEstoqueSuperior'), $where,null,null,null,$orderby);
    }
    public function getEstoqueAtualPP($where = null, $paginacao=null) {
        $tables = 'prodInsumo i'
                . ' inner join prodEstoque e on i.idInsumo = e.idInsumo'
                . ' inner join prodLocalEstoque le on le.idLocalEstoque = e.idLocalEstoque'
                . ' left join prodUnidade u on u.idUnidade = i.idUnidade'
                . ' left join prodParceiro f on f.idParceiro = i.idParceiroUltimaCompra'
                . ' left join prodParceiro fi on fi.idParceiro = i.idParceiro'
                . ' left join prodGrupo g on g.idGrupo = i.idGrupo';
        return $this->read($tables, array('e.*','i.*','e.qtEstoque as estoqueatual','u.dsUnidade','g.dsGrupo','le.dsLocalEstoque','le.cdLocalEstoque','f.dsParceiro as dsParceiroUC','i.dtUltimaCompra','i.qtUltimaCompra','fi.dsParceiro as dsParceiro','le.idLocalEstoqueSuperior'), $where,null,null,null,null,null,$paginacao);
    }

    public function getEstoqueItens($where = null) {
        $tables = 'prodEstoqueItens as a';
        $tables .= ' left join prodEstoque as i on i.idEstoque = a.idEstoque';
        $tables .= ' left join prodTipoEstoque as t on t.idTipoEstoque = a.idTipoEstoque';
        $tables .= ' left join prodInsumo as d on d.idInsumo = a.idInsumo';
        $tables .= ' left join prodGrupo as m on m.idGrupo = d.idGrupo';
        $orderby = 'a.idEstoqueItem';
        return $this->read($tables, array('a.*', 'd.dsInsumo', 'm.dsGrupo', '(a.vlEstoque / a.qtEstoque) as vlUnitario'), $where, null, null, null, $orderby);
    }

    public function getTotalEstoqueItens($where = null) {
        $tables = 'prodEstoqueItens as a';
        $tables .= ' left join prodEstoque as i on i.idEstoque = a.idEstoque';
        $tables .= ' left join prodTipoEstoque as t on t.idTipoEstoque = a.idTipoEstoque';
        return $this->read($tables, array('sum(a.vlEstoque) as totalmovimento'), $where, null, null, null, null);
    }

    //Grava o perfil
    public function setEstoque($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert($this->tabPadrao, $array, false));
        $this->commit();
        return $id;
    }

    public function setEstoqueItem($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodEstoqueItens', $array, false));
        $this->commit();
        return $id;
    }
    //Atualiza o Log
    public function updEstoque($array) {
        //Chave    
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->update($this->tabPadrao, $array, $where));
        $this->commit();
        return true;
    }
    public function updEstoquePP($array, $where) {
        $this->startTransaction();
        $this->transaction($this->update($this->tabPadrao, $array, $where));
        $this->commit();
        return true;
    }
    //Atualiza o Log
    public function updEstoqueItem($array, $where) {
        //Chave    
        $this->startTransaction();
        $this->transaction($this->update('prodInsumo', $array, $where));
        $this->commit();
        return true;
    }

    //Remove perfil    
    public function delEstoqueItem($where = null) {
        //Chave
        $this->startTransaction();
        $this->transaction($this->delete('prodEstoqueItens', $where, true));
        $this->commit();
        return true;
    }
}
?>
