<?php

class movimentoModel extends model {

    var $tabPadrao = 'prodMovimento';
    var $campo_chave = 'idMovimento';

    public function getMovimento($where = null, $orderby = null, $limit = null) {
        $tables = 'prodMovimento as m '
                . ' left join prodMovimentoItens mi on mi.idMovimento = m.idMovimento'
                . ' left join prodInsumo i on i.idInsumo = mi.idInsumo'
                . ' left join prodGrupo g on g.idGrupo = i.idGrupo'
                . ' left join prodParceiro f on f.idParceiro = m.idParceiro'
                . ' left join prodLocalEstoque l on l.idLocalEstoque = mi.idLocalEstoque'
                . ' left join prodLocalEstoque ld on ld.idLocalEstoque = mi.idLocalEstoqueDestino'
                . ' left join prodCentroCusto cc on cc.idCentroCusto = mi.idCentroCusto'
                . ' left join prodParceiro c on c.idParceiro = m.idParceiro'
                . ' left join prodSetor s on s.idSetor = mi.idSetor'
                . ' left join prodColaborador co on co.idColaborador = m.idColaborador'
                . ' left join prodTipoMovimento tm on tm.idTipoMovimento = m.idTipoMovimento';
        return $this->read($tables, array('mi.idSetor','s.dsSetor','m.*','mi.dsObservacao as referencia','mi.qtMovimento','mi.vlMovimento', 'l.cdLocalEstoque', 'l.dsLocalEstoque', 'ld.cdLocalEstoque as cdLocalEstoqueDestino', 'ld.dsLocalEstoque as dsLocalEstoqueDestino','i.dsInsumo','cc.dsCentroCusto','weekday(dtMovimento) AS semana','f.dsParceiro', 'f.dsCidade', 'f.cdCNPJ', 'co.dsColaborador','c.dsParceiro','tm.dsTipoMovimento','tm.stDC'), $where,'idMovimento',$limit,null,$orderby,null,null);
    }
    public function getMovimentoSetor($where = null, $orderby = null, $limit = null) {
        $tables = 'prodMovimento as m '
                . ' INNER join prodMovimentoItens mi on mi.idMovimento = m.idMovimento'
                . ' INNER join prodInsumo i on i.idInsumo = mi.idInsumo'
                . ' INNER join prodGrupo g on g.idGrupo = i.idGrupo'
                . ' INNER join prodLocalEstoque l on l.idLocalEstoque = mi.idLocalEstoque'
                . ' INNER join prodCentroCusto cc on cc.idCentroCusto = mi.idCentroCusto'
                . ' INNER join prodSetor s on s.idSetor = mi.idSetor'
                . ' INNER join prodColaborador co on co.idColaborador = m.idColaborador'
                . ' INNER join prodTipoMovimento tm on tm.idTipoMovimento = m.idTipoMovimento';
        return $this->read($tables, array('mi.idSetor','s.dsSetor','m.*','mi.dsObservacao as referencia','mi.qtMovimento','mi.vlMovimento', 'l.cdLocalEstoque', 'l.dsLocalEstoque','i.dsInsumo','cc.dsCentroCusto','weekday(dtMovimento) AS semana', 'co.dsColaborador','tm.dsTipoMovimento','tm.stDC'), $where,'mi.idSetor',$limit,null,$orderby,null,null);
    }
    public function getMovimentoTotal($where = null, $sqlMov = null) {
        $tables = 'prodMovimento as m '
                . ' left join prodMovimentoItens mi on mi.idMovimento = m.idMovimento'
                . ' left join prodInsumo i on i.idInsumo = mi.idInsumo'
                . ' left join prodGrupo g on g.idGrupo = i.idGrupo'
                . ' left join prodParceiro f on f.idParceiro = m.idParceiro';
        $fields = array("mi.idInsumo,
    i.dsInsumo,i.cdInsumo,
    (SELECT 
            SUM(movi.qtMovimento)
        FROM
            prodMovimento AS mov
                LEFT JOIN
            prodMovimentoItens movi ON movi.idMovimento = mov.idMovimento
                LEFT JOIN
            prodTipoMovimento tmov ON tmov.idTipoMovimento = mov.idTipoMovimento
                left join 
            prodParceiro fmov on fmov.idParceiro = mov.idParceiro
                left join 
            prodInsumo imov on imov.idInsumo = movi.idInsumo
                left join 
            prodGrupo gmov on gmov.idGrupo = imov.idGrupo
        WHERE
            {$sqlMov}
                AND movi.idInsumo = mi.idInsumo
                AND tmov.stDC = 'S') AS qtTotalSaidas,
    (SELECT 
            SUM(movi.qtMovimento)
        FROM
            prodMovimento AS mov
                LEFT JOIN
            prodMovimentoItens movi ON movi.idMovimento = mov.idMovimento
                LEFT JOIN
            prodTipoMovimento tmov ON tmov.idTipoMovimento = mov.idTipoMovimento
                left join 
            prodParceiro fmov on fmov.idParceiro = mov.idParceiro
                left join 
            prodInsumo imov on imov.idInsumo = movi.idInsumo
                left join 
            prodGrupo gmov on gmov.idGrupo = imov.idGrupo
        WHERE
            {$sqlMov}
                AND movi.idInsumo = mi.idInsumo
                AND tmov.stDC = 'E') AS qtTotalEntradas,
    (SELECT 
            SUM(movi.vlMovimento)
        FROM
            prodMovimento AS mov
                LEFT JOIN
            prodMovimentoItens movi ON movi.idMovimento = mov.idMovimento
                LEFT JOIN
            prodTipoMovimento tmov ON tmov.idTipoMovimento = mov.idTipoMovimento
                left join 
            prodParceiro fmov on fmov.idParceiro = mov.idParceiro
                left join 
            prodInsumo imov on imov.idInsumo = movi.idInsumo
                left join 
            prodGrupo gmov on gmov.idGrupo = imov.idGrupo
        WHERE
            {$sqlMov}
                AND movi.idInsumo = mi.idInsumo
                AND tmov.stDC = 'S') AS vlTotalSaidas, 
        (SELECT 
            SUM(movi.vlMovimento)
        FROM
            prodMovimento AS mov
                LEFT JOIN
            prodMovimentoItens movi ON movi.idMovimento = mov.idMovimento
                LEFT JOIN
            prodTipoMovimento tmov ON tmov.idTipoMovimento = mov.idTipoMovimento
                left join 
            prodParceiro fmov on fmov.idParceiro = mov.idParceiro
                left join 
            prodInsumo imov on imov.idInsumo = movi.idInsumo
                left join 
            prodGrupo gmov on gmov.idGrupo = imov.idGrupo
        WHERE
            {$sqlMov}
                AND movi.idInsumo = mi.idInsumo
                AND tmov.stDC = 'E') AS vlTotalEntradas");
        
        $groupBy = 'mi.idInsumo';
        return $this->read($tables, $fields, $where,$groupBy,null,null,'i.dsInsumo',null,null,false);
    }
    public function getMovimentoItens($where = null, $orderby = null, $limit = null) {
        $tables = 'prodMovimentoItens mi'
                . ' left join prodMovimento m on m.idMovimento = mi.idMovimento'
                . ' left join prodInsumo i on i.idInsumo = mi.idInsumo'
                . ' left join prodUnidade u on u.idUnidade = i.idUnidade'
                . ' left join prodLocalEstoque l on l.idLocalEstoque = mi.idLocalEstoque'
                . ' left join prodLocalEstoque ld on ld.idLocalEstoque = mi.idLocalEstoqueDestino'
                . ' left join prodParceiro f on f.idParceiro = m.idParceiro'
                . ' left join prodParceiro c on c.idParceiro = m.idParceiro'
                . ' left join prodColaborador co on co.idColaborador = m.idColaborador'
                . ' left join prodMotivo mot on mot.idMotivo = mi.idMotivo'
                . ' left join prodMaquina maq on maq.idMaquina = mi.idMaquina'
                . ' left join prodCentroCusto cc on cc.idCentroCusto = mi.idCentroCusto'
                . ' left join prodGrupo g on g.idGrupo = i.idGrupo'
                . ' left join prodSetor s on s.idSetor = mi.idSetor'
                . ' left join prodTipoMovimento tm on tm.idTipoMovimento = m.idTipoMovimento';
        return $this->read($tables, array('s.dsSetor,i.stControlado, i.dsOrgaoControlador, u.dsUnidade, co.dsColaborador, mi.*, m.dtMovimento, mi.dsObservacao as referencia,tm.stDC, tm.dsTipoMovimento,g.dsGrupo, i.cdInsumo,i.dsInsumo, l.cdLocalEstoque, l.dsLocalEstoque, ld.cdLocalEstoque as cdLocalEstoqueDestino, ld.dsLocalEstoque as dsLocalEstoqueDestino, mot.dsMotivo, maq.dsMaquina, cc.dsCentroCusto, f.dsParceiro, c.dsParceiro, l.idLocalEstoqueSuperior as localestoquesuperior, ld.idLocalEstoqueSuperior as localestoquesuperiordestino'), $where,null,null,null,$orderby,null,false);
    }

    public function getMovimentoTotalInsumo($where = null) {
        $tables = 'prodMovimentoItens mi
            INNER JOIN
                prodMovimento m on m.idMovimento = mi.idMovimento
            INNER JOIN
                prodTipoMovimento tm ON tm.idTipoMovimento = mi.idTipoMovimento';
        $fields = array('sum(vlMovimento) as valor',"if(tm.stDC = 'E','ENTRADA','SAIDA') as nome","if(tm.stDC = 'E',sum(vlMovimento / qtMovimento),0) as pme");
        $groupby = "mi.idInsumo , tm.stDC";        
        return $this->read($tables, $fields, $where,$groupby,null,null,null);
    }

    public function getMovimentoTotalPedido($where = null) {
        $tables = 'prodPedido';
        $fields = array('idPrioridade','count(idPedido) as qttotal');
        $groupby = "idPrioridade";        
        return $this->read($tables, $fields, $where,$groupby,null,null,null,null,null);
    }
    public function getMovimentoTotalSolicitacaoS($where = null) {
        $tables = 'prodSolicitacaoCompras sol
        INNER JOIN
    prodSolicitacaoComprasItens soli ON soli.idSolicitacao = sol.idSolicitacao
    inner join prodUsuarios u on u.idUsuario = sol.idUsuarioSolicitante';
        $fields = array('u.dsUsuario','idUsuarioSolicitante','count(idSolicitacaoItem) as qttotal');
        $groupby = "idUsuarioSolicitante";        
        return $this->read($tables, $fields, $where,$groupby,null,null,'u.dsUsuario',null,null, false);
    }

    public function getMovimentoTotalSolicitacao($where = null) {
        $tables = 'prodSolicitacaoCompras sol
        INNER JOIN
    prodSolicitacaoComprasItens soli ON soli.idSolicitacao = sol.idSolicitacao';
        $fields = array('idPrioridade','count(idSolicitacaoItem) as qttotal');
        $groupby = "idPrioridade";        
        return $this->read($tables, $fields, $where,$groupby,null,null,null,null,null, null);
    }
    public function delMovimento($array) {
        //Chave
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->delete($this->tabPadrao, $where, true));
        $this->commit();
        return true;
    }
    public function updMovimentoItem($array) {
        //Chave    
        $where =  'idMovimentoItem = ' . $array['idMovimentoItem'];
        $this->startTransaction();
        $this->transaction($this->update('prodMovimentoItens', $array, $where));
        $this->commit();
        return true;
    }    
}
?>
