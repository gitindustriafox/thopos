<?php

class chamadosModel extends model {

    var $tabPadrao = 'prodChamados';
    var $campo_chave = 'idChamado';

    //Estrutura da Tabela Vazia Utilizada para novos Cadastros
    public function estrutura_vazia() {
        $dados = null;
        $dados[0]['idChamado'] = NULL;
        $dados[0]['dsChamado'] = NULL;
        $dados[0]['dsObservacao'] = NULL;
        $dados[0]['idPrioridade'] = NULL;
        $dados[0]['idSetorExecutor'] = NULL;
        return $dados;
    }

    public function getValores($where = null) {
        $table = 'prodDE';
        return $this->read($table, array('dsH as H','dsU as U','dsP as P','dsF as F'), $where);         
    }

    public function getChamado($where = null) {
        $table = 'prodChamados r '
                . ' left join prodChamadosSequencia cs on cs.idChamado = r.idChamado'
                . ' left join prodChamadosStatus s on s.idStatus = cs.idStatus'
                . ' left join prodChamadosInteracao lr on lr.idChamado = r.idChamado';
        return $this->read($table, array('*','r.dsObservacao as obs','r.idChamado as id'), $where, null, null, null, 'r.idChamado desc',null,null,false);         
    }

    public function getChamadoSequencia($where = null) {
        $table = 'prodChamadosSequencia r '
                . ' left join prodChamadosStatus s on s.idStatus = r.idStatus'
                . ' left join prodUsuarios u on u.idUsuario = r.idUsuario';
        return $this->read($table, array('*'), $where, null, null, null, 'r.idChamado desc',null,null,false);         
    }

    public function getSoChamado($where = null) {
        $table = 'prodChamado r '
                . ' left join prodUsuarios u on u.idUsuario = r.idUsuarioSolicitante'
                . ' left join prodUsuarios ur on ur.idUsuario = r.idUsuarioAta'
                . ' left join prodLocalChamado lr on lr.idLocalChamado = r.idLocalChamado';
        return $this->read($table, array('ur.dsUsuario as usuarioresponsavel','r.*','u.dsUsuario','lr.dsLocalChamado'), $where, null, null, null, 'r.idChamado desc',null,null,false);         
    }

    public function getDatas($where = null) {
        $table = 'prodChamadoDatas';
        return $this->read($table, array('*'), $where, null, null, null, 'dtIntermediaria');         
    }

    public function getParticipantes($where = null) {
        $table = "prodChamadoParticipantes P inner join prodUsuarios u on u.idUsuario = P.idUsuario inner join prodChamado r on r.idChamado = P.idChamado"
                . " left join prodTipoChamado t on t.idTipoChamado = r.idTipoChamado"
                . " left join prodUsuarios us on us.idUsuario = r.idUsuarioSolicitante"
                . " left join prodMensagem m on P.idParticipante = m.idTabela and m.dsNomeTabela = 'prodChamadoParticipantes'"
                . " left join prodMensagemItem mi on mi.idMensagem = m.idMensagem"
                . " left join prodLocalChamado lr on lr.idLocalChamado = r.idLocalChamado";
        $groupBy = 'P.idParticipante, m.idTabela, m.dsNomeTabela';
        return $this->read($table, array('r.idUsuarioAta','r.dsTempoDuracaoReal','P.*','mi.stSituacao as leu','r.idUsuarioSolicitante','u.dsUsuario','P.dsNome','P.dsEmail as email','P.dsCelular as telefone1','P.dsAssunto as assuntoP','t.dsTipoChamado','us.dsUsuario as UsuarioSolicitante','r.dsAssunto as dsPauta', 'r.dtChamado','r.dsAta','r.dtRealizacao','lr.dsLocalChamado','r.dsCaminhoPDF','r.dsTempoDuracao', 'r.stSituacao', 'P.stSituacao as ausenteoupresente'), $where, $groupBy);         
    }

    public function getParticipantesTarefa($where = null, $orderby = null) {
        $table = 'prodChamadoTarefas P inner join prodChamado r on r.idChamado = P.idChamado'
                . ' left join prodTipoChamado t on t.idTipoChamado = r.idTipoChamado'
                . ' Left join prodChamadoParticipantes part on part.idUsuario = P.idUsuario and part.idChamado = P.idChamado'
                . ' left join prodUsuarios us on us.idUsuario = r.idUsuarioSolicitante'
                . ' left join prodUsuarios ust on ust.idUsuario = P.idUsuario'
                . ' left join prodLocalChamado lr on lr.idLocalChamado = r.idLocalChamado';
        return $this->read($table, array('r.idUsuarioAta','ust.dsUsuario as UTarefa','part.idParticipante','part.dsAssunto','P.*','P.idUsuario as idusuariotarefa','r.idUsuarioSolicitante','part.dsNome','part.dsEmail as email','part.dsCelular as telefone1','P.dsTarefa as tarefa','t.dsTipoChamado','us.dsUsuario as UsuarioSolicitante','r.dsAssunto as dsPauta', 'r.dtChamado','r.dtRealizacao','r.dsAta','lr.dsLocalChamado','r.dsCaminhoPDF','r.dsTempoDuracao', 'r.stSituacao'), $where,null,null,null,$orderby,null,null,false);         
    }

    public function getParticipantesTarefaG($where = null) {
        $table = 'prodChamadoTarefas P inner join prodUsuarios u on u.idUsuario = P.idUsuario inner join prodChamado r on r.idChamado = P.idChamado'
                . ' left join prodTipoChamado t on t.idTipoChamado = r.idTipoChamado'
                . ' Left join prodChamadoParticipantes part on part.idUsuario = P.idUsuario and part.idChamado = P.idChamado'
                . ' left join prodUsuarios us on us.idUsuario = r.idUsuarioSolicitante'
                . ' left join prodLocalChamado lr on lr.idLocalChamado = r.idLocalChamado';
        $groupby = 'u.dsUsuario';
        return $this->read($table, array('u.dsUsuario as Usuario','count(u.dsUsuario) as tarefas'), $where,$groupby,null,null,null,null,null,false);         
    }

    public function getParticipantesTarefaGStatus($where = null) {
        $table = 'prodChamadoTarefas P inner join prodUsuarios u on u.idUsuario = P.idUsuario inner join prodChamado r on r.idChamado = P.idChamado'
                . ' left join prodTipoChamado t on t.idTipoChamado = r.idTipoChamado'
                . ' Left join prodChamadoParticipantes part on part.idUsuario = P.idUsuario and part.idChamado = P.idChamado'
                . ' left join prodUsuarios us on us.idUsuario = r.idUsuarioSolicitante'
                . ' left join prodLocalChamado lr on lr.idLocalChamado = r.idLocalChamado';
        return $this->read($table, array('count(P.stStatusTarefa) as totalstatus'), $where);         
    }

    public function getTipo($where = null) {
        $table = 'prodTipoChamado';
        return $this->read($table, array('*'), $where);         
    }

    //Grava o perfil
    public function setChamado($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert($this->tabPadrao, $array, false));
        $this->commit();
        return $id;
    }

    public function setChamadoSequencia($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodChamadosSequencia', $array, false));
        $this->commit();
        return $id;
    }

    public function setParticipantes($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodChamadoParticipantes', $array, false));
        $this->commit();
        return $id;
    }

    public function setDatas($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodChamadoDatas', $array, false));
        $this->commit();
        return $id;
    }

    public function setTarefas($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodChamadoTarefas', $array, false));
        $this->commit();
        return $id;
    }

    //Atualiza o Log
    public function updParticipantes($array) {
        //Chave    
        $where = "idParticipante = " . $array['idParticipante'];
        $this->startTransaction();
        $this->transaction($this->update('prodChamadoParticipantes', $array, $where));
        $this->commit();
        return true;
    }

    public function updParticipantesPresenca($array) {
        //Chave    
        $where = "idUsuario = " . $array['idUsuario'] . ' and idChamado = ' . $array['idChamado'];
        $this->startTransaction();
        $this->transaction($this->update('prodChamadoParticipantes', $array, $where));
        $this->commit();
        return true;
    }

    public function updTarefas($array) {
        //Chave    
        $where = "idTarefa = " . $array['idTarefa'];
        $this->startTransaction();
        $this->transaction($this->update('prodChamadoTarefas', $array, $where));
        $this->commit();
        return true;
    }

    public function updDatas($array) {
        //Chave    
        $where = "idData = " . $array['idData'];
        $this->startTransaction();
        $this->transaction($this->update('prodChamadoDatas', $array, $where));
        $this->commit();
        return true;
    }

    //Atualiza o Log
    public function updChamado($array) {
        //Chave    
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->update($this->tabPadrao, $array, $where));
        $this->commit();
        return true;
    }

    public function updChamadoParticipantes($array) {
        //Chave    
        $where = 'idChamado = ' . $array['idChamado'] . " and idParticipante =  " . $array['idParticipante'];
        $this->startTransaction();
        $this->transaction($this->update('prodChamadoParticipantes', $array, $where));
        $this->commit();
        return true;
    }
    //Remove perfil    
    public function delChamado($array) {
        //Chave
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->delete($this->tabPadrao, $where, true));
        $this->commit();
        return true;
    }
    public function delParticipantes($where = null) {
        //Chave
        $this->startTransaction();
        $this->transaction($this->delete('prodChamadoParticipantes', $where, true));
        $this->commit();
        return true;
    }
    public function delTarefa($where = null) {
        //Chave
        $this->startTransaction();
        $this->transaction($this->delete('prodChamadoTarefas', $where, true));
        $this->commit();
        return true;
    }
    public function delData($where = null) {
        //Chave
        $this->startTransaction();
        $this->transaction($this->delete('prodChamadoDatas', $where, true));
        $this->commit();
        return true;
    }

}
?>
