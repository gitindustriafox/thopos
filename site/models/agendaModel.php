<?php

class agendaModel extends model {

    var $tabPadrao = 'prodAgenda';
    var $campo_chave = 'idAgenda';

    //Estrutura da Tabela Vazia Utilizada para novos Cadastros
    public function estrutura_vazia() {
        $dados = null;
        $dados[0]['idAgenda'] = NULL;
        $dados[0]['dsAssunto'] = NULL;
        $dados[0]['dsAta'] = NULL;
        $dados[0]['dtAgenda'] = NULL;
        return $dados;
    }

    public function getValores($where = null) {
        $table = 'prodDE';
        return $this->read($table, array('dsH as H','dsU as U','dsP as P','dsF as F'), $where);         
    }

    public function getAgenda($where = null) {
        $table = 'prodAgenda r '
                . ' left join prodUsuarios u on u.idUsuario = r.idUsuarioSolicitante'
                . ' left join prodUsuarios ur on ur.idUsuario = r.idUsuarioAta'
                . ' left join prodLocalAgenda lr on lr.idLocalAgenda = r.idLocalAgenda'
                . ' left join prodAgendaParticipantes p on p.idAgenda = r.idAgenda';
        return $this->read($table, array('p.stPresenca','ur.dsUsuario as usuarioresponsavel','r.*','u.dsUsuario','lr.dsLocalAgenda'), $where, null, null, null, 'r.idAgenda desc',null,null,false);         
    }

    public function getSoAgenda($where = null) {
        $table = 'prodAgenda r '
                . ' left join prodUsuarios u on u.idUsuario = r.idUsuarioSolicitante'
                . ' left join prodUsuarios ur on ur.idUsuario = r.idUsuarioAta'
                . ' left join prodLocalAgenda lr on lr.idLocalAgenda = r.idLocalAgenda';
        return $this->read($table, array('ur.dsUsuario as usuarioresponsavel','r.*','u.dsUsuario','lr.dsLocalAgenda'), $where, null, null, null, 'r.idAgenda desc',null,null,false);         
    }

    public function getDatas($where = null) {
        $table = 'prodAgendaDatas';
        return $this->read($table, array('*'), $where, null, null, null, 'dtIntermediaria');         
    }

    public function getParticipantes($where = null) {
        $table = "prodAgendaParticipantes P inner join prodUsuarios u on u.idUsuario = P.idUsuario inner join prodAgenda r on r.idAgenda = P.idAgenda"
                . " left join prodTipoAgenda t on t.idTipoAgenda = r.idTipoAgenda"
                . " left join prodUsuarios us on us.idUsuario = r.idUsuarioSolicitante"
                . " left join prodMensagem m on P.idParticipante = m.idTabela and m.dsNomeTabela = 'prodAgendaParticipantes'"
                . " left join prodMensagemItem mi on mi.idMensagem = m.idMensagem"
                . " left join prodLocalAgenda lr on lr.idLocalAgenda = r.idLocalAgenda";
        $groupBy = 'P.idParticipante'; //, m.idTabela, m.dsNomeTabela
        return $this->read($table, array('r.idUsuarioAta','r.dsTempoDuracaoReal','P.*','r.idUsuarioSolicitante','u.dsUsuario','P.dsNome','P.dsEmail as email','P.dsCelular as telefone1','P.dsAssunto as assuntoP','t.dsTipoAgenda','us.dsUsuario as UsuarioSolicitante','r.dsAssunto as dsPauta', 'r.dtAgenda','r.dsAta','r.dtRealizacao','lr.dsLocalAgenda','r.dsCaminhoPDF','r.dsTempoDuracao', 'r.stSituacao', 'P.stSituacao as ausenteoupresente'), $where, $groupBy,null,null,null,null,null,null); //,'mi.stSituacao as leu'
    }

    public function getParticipantesTarefa($where = null, $orderby = null) {
        $table = 'prodAgendaTarefas P inner join prodAgenda r on r.idAgenda = P.idAgenda'
                . ' left join prodTipoAgenda t on t.idTipoAgenda = r.idTipoAgenda'
                . ' Left join prodAgendaParticipantes part on part.idUsuario = P.idUsuario and part.idAgenda = P.idAgenda'
                . ' left join prodUsuarios us on us.idUsuario = r.idUsuarioSolicitante'
                . ' left join prodUsuarios ust on ust.idUsuario = P.idUsuario'
                . ' left join prodLocalAgenda lr on lr.idLocalAgenda = r.idLocalAgenda';
        return $this->read($table, array('r.idUsuarioAta','ust.dsUsuario as UTarefa','part.idParticipante','part.dsAssunto','P.*','P.idUsuario as idusuariotarefa','r.idUsuarioSolicitante','part.dsNome','part.dsEmail as email','part.dsCelular as telefone1','P.dsTarefa as tarefa','t.dsTipoAgenda','us.dsUsuario as UsuarioSolicitante','r.dsAssunto as dsPauta', 'r.dtAgenda','r.dtRealizacao','r.dsAta','lr.dsLocalAgenda','r.dsCaminhoPDF','r.dsTempoDuracao', 'r.stSituacao'), $where,null,null,null,$orderby,null,null,false);         
    }

    public function getParticipantesTarefaG($where = null) {
        $table = 'prodAgendaTarefas P inner join prodUsuarios u on u.idUsuario = P.idUsuario inner join prodAgenda r on r.idAgenda = P.idAgenda'
                . ' left join prodTipoAgenda t on t.idTipoAgenda = r.idTipoAgenda'
                . ' Left join prodAgendaParticipantes part on part.idUsuario = P.idUsuario and part.idAgenda = P.idAgenda'
                . ' left join prodUsuarios us on us.idUsuario = r.idUsuarioSolicitante'
                . ' left join prodLocalAgenda lr on lr.idLocalAgenda = r.idLocalAgenda';
        $groupby = 'u.dsUsuario';
        return $this->read($table, array('u.dsUsuario as Usuario','count(u.dsUsuario) as tarefas'), $where,$groupby,null,null,null,null,null,false);         
    }

    public function getParticipantesTarefaGStatus($where = null) {
        $table = 'prodAgendaTarefas P inner join prodUsuarios u on u.idUsuario = P.idUsuario inner join prodAgenda r on r.idAgenda = P.idAgenda'
                . ' left join prodTipoAgenda t on t.idTipoAgenda = r.idTipoAgenda'
                . ' Left join prodAgendaParticipantes part on part.idUsuario = P.idUsuario and part.idAgenda = P.idAgenda'
                . ' left join prodUsuarios us on us.idUsuario = r.idUsuarioSolicitante'
                . ' left join prodLocalAgenda lr on lr.idLocalAgenda = r.idLocalAgenda';
        return $this->read($table, array('count(P.stStatusTarefa) as totalstatus'), $where);         
    }

    public function getTipo($where = null) {
        $table = 'prodTipoAgenda';
        return $this->read($table, array('*'), $where);         
    }

    //Grava o perfil
    public function setAgenda($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert($this->tabPadrao, $array, false));
        $this->commit();
        return $id;
    }

    public function setParticipantes($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodAgendaParticipantes', $array, false));
        $this->commit();
        return $id;
    }

    public function setDatas($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodAgendaDatas', $array, false));
        $this->commit();
        return $id;
    }

    public function setTarefas($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodAgendaTarefas', $array, false));
        $this->commit();
        return $id;
    }

    //Atualiza o Log
    public function updParticipantes($array) {
        //Chave    
        $where = "idParticipante = " . $array['idParticipante'];
        $this->startTransaction();
        $this->transaction($this->update('prodAgendaParticipantes', $array, $where));
        $this->commit();
        return true;
    }

    public function updParticipantesPresenca($array) {
        //Chave    
        $where = "idUsuario = " . $array['idUsuario'] . ' and idAgenda = ' . $array['idAgenda'];
        $this->startTransaction();
        $this->transaction($this->update('prodAgendaParticipantes', $array, $where));
        $this->commit();
        return true;
    }

    public function updTarefas($array) {
        //Chave    
        $where = "idTarefa = " . $array['idTarefa'];
        $this->startTransaction();
        $this->transaction($this->update('prodAgendaTarefas', $array, $where));
        $this->commit();
        return true;
    }

    public function updDatas($array) {
        //Chave    
        $where = "idData = " . $array['idData'];
        $this->startTransaction();
        $this->transaction($this->update('prodAgendaDatas', $array, $where));
        $this->commit();
        return true;
    }

    //Atualiza o Log
    public function updAgenda($array) {
        //Chave    
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->update($this->tabPadrao, $array, $where));
        $this->commit();
        return true;
    }

    public function updAgendaParticipantes($array) {
        //Chave    
        $where = 'idAgenda = ' . $array['idAgenda'] . " and idParticipante =  " . $array['idParticipante'];
        $this->startTransaction();
        $this->transaction($this->update('prodAgendaParticipantes', $array, $where));
        $this->commit();
        return true;
    }
    //Remove perfil    
    public function delAgenda($array) {
        //Chave
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->delete($this->tabPadrao, $where, true));
        $this->commit();
        return true;
    }
    public function delParticipantes($where = null) {
        //Chave
        $this->startTransaction();
        $this->transaction($this->delete('prodAgendaParticipantes', $where, true));
        $this->commit();
        return true;
    }
    public function delTarefa($where = null) {
        //Chave
        $this->startTransaction();
        $this->transaction($this->delete('prodAgendaTarefas', $where, true));
        $this->commit();
        return true;
    }
    public function delData($where = null) {
        //Chave
        $this->startTransaction();
        $this->transaction($this->delete('prodAgendaDatas', $where, true));
        $this->commit();
        return true;
    }

}
?>
