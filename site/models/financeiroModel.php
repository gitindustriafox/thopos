<?php

class financeiroModel extends model {

    var $tabPadrao = 'prodFinanceiro';
    var $campo_chave = 'idFinanceiro';

    //Estrutura da Tabela Vazia Utilizada para novos Cadastros
    public function estrutura_vazia() {
        $dados = null;
        $dados[0]['idFinanceiro'] = '';
        $dados[0]['dtEmissao'] = NULL;
        $dados[0]['nrNF'] = NULL;
        $dados[0]['idParceiro'] = NULL;
        $dados[0]['idPedido'] = NULL;
        $dados[0]['cdDocumento'] = NULL;
        $dados[0]['stStatus'] = 0;
        return $dados;
    }
    
    public function getFinanceiro($where = null, $paginacao=true) {
        $tables = 'prodFinanceiro f';
        $tables .= ' left join prodParceiro p on f.idParceiro = p.idParceiro';
        return $this->read($tables, array('f.*','p.dsParceiro'), $where, null, null, null, null,null,$paginacao,false);
    }

    public function getFinanceiroBusca($where = null, $paginacao=true) {
        $tables = 'prodFinanceiro f';
        $tables .= ' left join prodFinanceiroParcelas p on f.idFinanceiro = p.idFinanceiro';
        $tables .= ' left join prodParceiro par on f.idParceiro = par.idParceiro';
        return $this->read($tables, array('f.*','par.dsParceiro'), $where, 'f.idFinanceiro', null, null, null,null,$paginacao,false);
    }

    public function getFinanceiroParcelas($where = null,$paginacao=true) {
        $tables = 'prodFinanceiro f left join prodFinanceiroParcelas p on p.idFinanceiro = f.idFinanceiro';
        return $this->read($tables, array('p.*'), $where, null, null, null, null,null,$paginacao,false);
    }

    //Grava o perfil
    public function setFinanceiro($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert($this->tabPadrao, $array, false));
        $this->commit();
        return $id;
    }

    public function setFinanceiroP($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodFinanceiroParcelas', $array, false));
        $this->commit();
        return $id;
    }

    //Atualiza o Log
    public function updFinanceiro($array,$where) {
        //Chave    
        $this->startTransaction();
        $this->transaction($this->update($this->tabPadrao, $array, $where));
        $this->commit();
        return true;
    }
    //Atualiza o Log
    public function delFinanceiro($where = null) {
        //Chave
        $this->startTransaction();
        $this->transaction($this->delete('prodFinanceiro', $where, true));
        $this->commit();
        return true;
    }
}
?>
