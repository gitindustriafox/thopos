<?php

class prioridadeModel extends model {

    var $tabPadrao = 'prodPrioridade';
    var $campo_chave = 'idPrioridade';

    //Estrutura da Tabela Vazia Utilizada para novos Cadastros
    public function estrutura_vazia() {
        $dados = null;
        $dados[0]['idPrioridade'] = NULL;
        $dados[0]['dsPrioridade'] = NULL;
        return $dados;
    }

    public function getPrioridade() {      
        return $this->read('prodPrioridade', array('*'),null,null,null,null,'dsPrioridade');         
    }
}

?>
