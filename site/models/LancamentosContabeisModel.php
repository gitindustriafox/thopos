<?php

class LancamentosContabeisModel extends model {

    var $tabPadrao = 'prodLancamentoContabil';
    var $campo_chave = 'idLancamento';

    //Estrutura da Tabela Vazia Utilizada para novos Cadastros
    public function estrutura_vazia() {
        $dados = null;
        $dados[0]['idLancamento'] = NULL;
        $dados[0]['dtLancamento'] = NULL;
        $dados[0]['dsHistorico'] = NULL;
        $dados[0]['nrDocumento'] = NULL;
        $dados[0]['stSituacao'] = NULL;
        $dados[0]['idOrigemInformacao'] = NULL;
        $dados[0]['vlLancamento'] = NULL;
        return $dados;
    }
    
    public function getLancamentoContabil($where = null, $paginacao=null) {
        $tables = 'prodLancamentoContabil l'
                . ' left join prodLancamentoContabilDebito ld on l.idLancamento = ld.idLancamento'
                . ' left join prodLancamentoContabilCredito lc on l.idLancamento = lc.idLancamento';
        $orderby = 'l.idLancamento';
        $groupby = 'l.idLancamento';
        return $this->read($tables, array('l.*'), $where, $groupby, null, null, $orderby,null,$paginacao);
    }
    public function getLancamentoContabilAbertos($where = null, $paginacao=null) {
        $tables = 'prodLancamentoContabil l '
                . ' left join prodLancamentoContabilDebito ld on l.idLancamento = ld.idLancamento'
                . ' left join prodPlanoDeContas pc on pc.idConta = ld.idContaDebito'
                . ' left join prodPeriodoContabil p on p.idPeriodo = pc.idPeriodo'
                . ' left join prodLancamentoContabilCredito lc on l.idLancamento = lc.idLancamento';
        $orderby = 'l.idLancamento';
        return $this->read($tables, array('l.*'), $where, null, null, null, $orderby,null,$paginacao,false);
    }
    public function getLancamentoContabilDebito($where = null, $paginacao=null) {
        $tables = 'prodLancamentoContabilDebito l inner join prodPlanoDeContas pc on l.idContaDebito = pc.idContaReduzida' ;
        $orderby = 'l.idLancamento';
        $groupby = 'pc.idContaReduzida';
        return $this->read($tables, array('l.*','pc.dsConta','pc.idContaReduzida','count(idLancamentoDebito) as totallinhasdebito'), $where, $groupby, null, null, $orderby,null,$paginacao);
    }
    public function getLancamentoContabilDebitoTotal($where = null) {
        $tables = 'prodLancamentoContabilDebito';
        return $this->read($tables, array('sum(vlLancamento) as totaldebito','count(idLancamentoDebito) as totallinhasdebito'), $where);
    }
    public function getLancamentoContabilCreditoTotal($where = null) {
        $tables = 'prodLancamentoContabilCredito';
        return $this->read($tables, array('sum(vlLancamento) as totalcredito','count(idLancamentoCredito) as totallinhascredito'), $where);
    }
    public function getLancamentoContabilCredito($where = null, $paginacao=null) {
        $tables = 'prodLancamentoContabilCredito l inner join prodPlanoDeContas pc on l.idContaCredito = pc.idContaReduzida';
        $orderby = 'l.idLancamento';
        $groupby = 'pc.idContaReduzida';
        return $this->read($tables, array('l.*','pc.dsConta','pc.idContaReduzida','count(idLancamentoCredito) as totallinhascredito'), $where, $groupby, null, null, $orderby,null,$paginacao);
    }

    public function setLancamentoContabil($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert($this->tabPadrao, $array, false));
        $this->commit();
        return $id;
    }

    public function setLancamentoContabilDebito($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodLancamentoContabilDebito', $array, false));
        $this->commit();
        return $id;
    }

    public function setLancamentoContabilCredito($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodLancamentoContabilCredito', $array, false));
        $this->commit();
        return $id;
    }

    public function updLancamentoContabil($array) {
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->update($this->tabPadrao, $array, $where));
        $this->commit();
        return true;
    }
    public function updLancamentoContabilCP($array) {
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->update($this->tabPadrao, $array, $where));
        $this->commit();
        return true;
    }

    public function delLancamentoContabil($where) {
        $this->startTransaction();
        $this->transaction($this->delete($this->tabPadrao, $where, true));
        $this->commit();
        return true;
    }
    public function delLancamentoContabilDebito($where) {
        $this->startTransaction();
        $this->transaction($this->delete('prodLancamentoContabilDebito', $where, true));
        $this->commit();
        return true;
    }
    public function delLancamentoContabilCredito($where) {
        $this->startTransaction();
        $this->transaction($this->delete('prodLancamentoContabilCredito', $where, true));
        $this->commit();
        return true;
    }
}
?>
