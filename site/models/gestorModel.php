<?php

class gestorModel extends model {

    var $tabPadrao = 'prodProdutos105';
    var $campo_chave = 'idSequencia';

    public function getProdutos105($where = null) {
        return $this->read($this->tabPadrao, array('*'), $where);
    }

    //Grava o perfil
    public function setProdutos105($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert($this->tabPadrao, $array, false));
        $this->commit();
        return $id;
    }
}
?>
