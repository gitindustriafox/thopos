<?php

class solicitacaocomprasModel extends model {

    var $tabPadrao = 'prodSolicitacaoCompras';
    var $campo_chave = 'idSolicitacao';

    //Estrutura da Tabela Vazia Utilizada para novos Cadastros
    public function estrutura_vazia() {
        $dados = null;
        $dados[0]['idSolicitacao'] = '';
        $dados[0]['dtSolicitacao'] = date('Y-m-d');
        $dados[0]['nrSolicitacao'] = NULL;
        $dados[0]['idUsuarioSolicitante'] = NULL;
        $dados[0]['idUsuarioDigitacao'] = NULL;
        $dados[0]['dsObservacao'] = NULL;
        $dados[0]['dsSolicitante'] = NULL;
        $dados[0]['idSituacao'] = 0;
        return $dados;
    }
    
    public function getSolicitacaoCompras($where = null) {
        $tables = 'prodSolicitacaoCompras a '
                . ' left join prodSituacao s on a.idSituacao = s.idSituacao'
                . ' left join prodPrioridade p on p.idPrioridade = a.idPrioridade';
        return $this->read($tables, array('a.*, s.dsSituacao, p.dsPrioridade'), $where, null, null, null, 'idSolicitacao desc');
    }

    public function getOrcamentoCompras($where = null) {
        $tables = 'prodOrcamentoCompras oc '
                . ' left join prodPrioridade p on p.idPrioridade = oc.idPrioridade';
        return $this->read($tables, array('oc.*, p.dsPrioridade'), $where);
    }

    public function getSolicitacaoComprasManutencao($where = null, $paginacao=null) {
        $tables = 'prodSolicitacaoCompras a '
                . ' left join prodSolicitacaoComprasItens i on i.idSolicitacao = a.idSolicitacao'
                . ' left join prodParceiro as f on f.idParceiro = i.idParceiro'
                . ' left join prodInsumo as ins on ins.idInsumo = i.idInsumo'
                . ' left join prodGrupo as gr on gr.idGrupo = ins.idGrupo'
                . ' left join prodUnidade as u on u.idUnidade = i.idUnidade'
                . ' left join prodServicos as se on se.idServico = i.idInsumo'
                . ' left join prodPrioridade pri on pri.idPrioridade = a.idPrioridade'
                . ' left join prodSituacao s on s.idSituacao = a.idSituacao '
                . ' left join prodPedido ped on ped.idPedido = i.idPedido '
                . ' left join prodCentroCusto cc on cc.idCentroCusto = i.idCentroCusto '                
                . ' left join prodSituacao si on si.idSituacao = i.idSituacao ';
        $tables .= ' left join prodGrupoDR as m on m.idGrupoDR = i.idGrupoDR';
        $tables .= ' left join prodItemDR as os on os.idItemDR = i.idItemDR';
        $groupby = 'i.idSolicitacao, i.idSolicitacaoItem, ins.idInsumo';
        return $this->read($tables, array('m.dsGrupoDR','os.dsItemDR',"if(gr.dsGrupo like '%QUIMICO%' ,1,0) as stQuimico",'gr.dsGrupo','ins.stControlado,ins.dsOrgaoControlador, u.dsUnidade,a.*,i.dsObservacao as obsItem, ped.dsCaminhoPDF,cc.dsCentroCusto,i.idParceiro, f.dsParceiro ,i.qtSolicitacao, se.dsServico ,s.dsSituacao,si.dsSituacao as dsSituacaoItem, si.idSituacao as idSituacaoItem ,i.stTipoIS,i.dsParceiroSugerido, pri.dsPrioridade, ins.dsInsumo, ins.cdInsumo,i.idSolicitacaoItem,i.idPedido, i.dsProduto, i.idInsumo, i.stAprovado'), $where, $groupby, null, null, 'idSolicitacao desc, i.idSolicitacaoItem', null,$paginacao,false);
    }
    public function getSolicitacaoComprasOrcamento($where = null, $paginacao=null) {
        $tables = 'prodOrcamentoCompras oc'
                . ' inner join prodOrcamentoComprasItens oci on oci.idOrcamentoCompras = oc.idOrcamentoCompras'
                . ' inner join prodSolicitacaoComprasItens i on i.idSolicitacao = oci.idSolicitacao and i.idSolicitacaoItem = oci.idSolicitacaoItem'
                . ' left join prodOrcamentoComprasParceiro ocp on ocp.idOrcamentoCompras = oc.idOrcamentoCompras'
                . ' left join prodSolicitacaoCompras a on i.idSolicitacao = a.idSolicitacao'
                . ' left join prodParceiro as f on ocp.idParceiro = f.idParceiro'
                . ' left join prodInsumo as ins on oci.idInsumo = ins.idInsumo'
                . ' left join prodGrupo as gr on gr.idGrupo = ins.idGrupo'
                . ' left join prodServicos as se on se.idServico = oci.idInsumo'
                . ' left join prodPrioridade pri on pri.idPrioridade = a.idPrioridade'
                . ' left join prodSituacao s on s.idSituacao = a.idSituacao '
                . ' left join prodSituacao si on si.idSituacao = i.idSituacao '
                . ' left join prodEmpresa emp on emp.idEmpresa = oc.idEmpresa ';
        $groupby = 'a.idSolicitacao, ins.idInsumo';
        return $this->read($tables, array('emp.cdCNPJ as cnpjempresa','gr.dsGrupo','emp.dsCelular as celularempresa','emp.dsContato as contatoempresa','emp.dsEndereco as enderecoempresa','emp.dsCidade as cidadeempresa','emp.dsBairro as bairroempresa','emp.dsEmpresa','oc.idOrcamentoCompras','a.*','i.idParceiro','f.*' ,'i.qtSolicitacao', 'se.dsServico' ,'s.dsSituacao','si.dsSituacao as dsSituacaoItem', 'si.idSituacao as idSituacaoItem' ,'i.stTipoIS','i.dsParceiroSugerido', 'pri.dsPrioridade', 'ins.dsInsumo', 'ins.cdInsumo','i.idSolicitacaoItem','i.idPedido', 'i.dsProduto', 'ocp.*', 'oc.dsLocalEntrega as localentrega'), $where, $groupby, null, null, 'idSolicitacao desc, i.idSolicitacaoItem', null,$paginacao);
    }
    public function getOrcamentoComprasItens($where = null, $paginacao=null) {
        $tables = 'prodOrcamentoCompras oc'
                . ' inner join prodOrcamentoComprasItens oci on oci.idOrcamentoCompras = oc.idOrcamentoCompras'
                . ' inner join prodSolicitacaoComprasItens i on i.idSolicitacao = oci.idSolicitacao and i.idSolicitacaoItem = oci.idSolicitacaoItem'
                . ' left join prodSolicitacaoCompras a on i.idSolicitacao = a.idSolicitacao'
                . ' left join prodInsumo as ins on ins.idInsumo = i.idInsumo'
                . ' left join prodServicos as se on se.idServico = i.idInsumo'
                . ' left join prodPrioridade pri on pri.idPrioridade = a.idPrioridade'
                . ' left join prodSituacao s on s.idSituacao = a.idSituacao '
                . ' left join prodPedido ped on ped.idPedido = i.idPedido '
                . ' left join prodSituacao si on si.idSituacao = i.idSituacao ';
        $groupby = 'a.idSolicitacao, ins.idInsumo';
        return $this->read($tables, array('oc.idOrcamentoCompras,a.*,ped.dsCaminhoPDF,i.qtSolicitacao, se.dsServico ,s.dsSituacao,si.dsSituacao as dsSituacaoItem, si.idSituacao as idSituacaoItem ,i.stTipoIS, pri.dsPrioridade, ins.dsInsumo, ins.cdInsumo,i.idSolicitacaoItem,i.idPedido, i.dsProduto'), $where, $groupby, null, null, 'idSolicitacao desc, i.idSolicitacaoItem', null,$paginacao,false);
    }
    public function getOrcamentoComprasParceiros($where = null) {
        $tables = 'prodOrcamentoCompras oc'
                . ' left join prodOrcamentoComprasParceiro ocp on ocp.idOrcamentoCompras = oc.idOrcamentoCompras'
                . ' left join prodParceiro as f on ocp.idParceiro = f.idParceiro';
        return $this->read($tables, array('*'), $where);
    }
    public function getSolicitacaoComprasItensSA($where = null) {
        $tables = 'prodSolicitacaoComprasItens as a';
        $tables .= ' left join prodSituacao as s on s.idSituacao = a.idSituacao';
        return $this->read($tables, array('s.*'), $where);
    }
    
    public function getSolicitacaoComprasItens($where = null) {
        $tables = 'prodSolicitacaoComprasItens as a';
        $tables .= ' left join prodSolicitacaoCompras as sol on sol.idSolicitacao = a.idSolicitacao';
        $tables .= ' left join prodSituacao as d on d.idSituacao = a.idSituacao';
        $tables .= ' left join prodUnidade as u on u.idUnidade = a.idUnidade';
        $tables .= ' left join prodServicos se on se.idServico = a.idInsumo';
        $tables .= ' left join prodInsumo ins on ins.idInsumo = a.idInsumo';
        $tables .= ' left join prodPrioridade pri on pri.idPrioridade = sol.idPrioridade';
        $tables .= ' left join prodParceiro as f on f.idParceiro = a.idParceiro';
        $tables .= ' left join prodPedido as p on p.idPedido = a.idPedido';
        $tables .= ' left join prodCentroCusto as cc on cc.idCentroCusto = a.idCentroCusto';
        $tables .= ' left join prodPedidoItens as pi on pi.idPedido = a.idPedido and pi.idPedidoItem = a.idPedidoItem';
        $orderby = 'a.idSolicitacaoItem';
        return $this->read($tables, array('a.*, sol.dtNecessidade,cc.dsCentroCusto,se.dsServico, f.dsParceiro, sol.dsLocalEntrega,sol.idUsuarioSolicitante,sol.idPrioridade,sol.dsSolicitante, ins.dsInsumo,d.dsSituacao, u.dsUnidade, p.dtPrazoEntrega, pri.dsPrioridade, p.dtPedido, pi.dsObservacao as dsObservacaoPedido,a.stTipoIS,sol.dsObservacao as obsSol'), $where, null, null, null, $orderby,null,null,null);
    }
    
    public function getSolicitacaoComprasItensE($where = null) {
        $tables = 'prodSolicitacaoComprasItens';
        return $this->read($tables, array('*'), $where);
    }
    
    public function getSolicitacaoComprasManutencaoItens($where = null) {
        $tables = 'prodSolicitacaoComprasItens as a';
        $tables .= ' left join prodInsumo as i on i.idInsumo = a.idInsumo';
        $tables .= ' left join prodServicos as s on s.idServico = a.idInsumo';
        $tables .= ' left join prodParceiro as f on f.idParceiro = a.idParceiro';
        $tables .= ' left join prodSituacao as d on d.idSituacao = a.idSituacao';
        $tables .= ' left join prodUnidade as u on u.idUnidade = a.idUnidade';
        $tables .= ' left join prodCentroCusto as cc on cc.idCentroCusto = a.idCentroCusto';
        $tables .= ' left join prodGrupoDR as m on m.idGrupoDR = a.idGrupoDR';
        $tables .= ' left join prodItemDR as os on os.idItemDR = a.idItemDR';
        $tables .= ' left join prodMotivo as mot on mot.idMotivo = a.idMotivo';
        $tables .= ' left join prodPedido as p on p.idPedido = a.idPedido';
        $tables .= ' left join prodPedidoItens as pi on pi.idPedido = a.idPedido and pi.idPedidoItem = a.idPedidoItem';
        
        $fields = array('a.*, d.dsSituacao, u.dsUnidade, i.dsInsumo, f.dsParceiro, u.dsUnidade, cc.dsCentroCusto, m.dsGrupoDR, os.dsItemDR, mot.dsMotivo, p.dtPrazoEntrega, p.dtPedido, pi.dsObservacao as dsObservacaoPedido,a.stTipoIS,s.dsServico');
        
        return $this->read($tables, $fields, $where, null, null, null,null,null,null,null);
    }

    public function getUltimaSolicitacao($where = null) {
        return $this->read('prodSolicitacaoCompras', array('max(nrSolicitacao) as ultimo'), null, null, null, null, null);
    }

    public function getTotalSolicitacaoComprasItens($where = null) {
        $tables = 'prodSolicitacaoComprasItens as a';
        $tables .= ' left join prodSolicitacaoCompras as i on i.idSolicitacaoCompras = a.idSolicitacaoCompras';
        return $this->read($tables, array('sum(a.vlSolicitacaoCompras) as totalpedido'), $where, null, null, null, null);
    }

    //Grava o perfil
    public function setSolicitacaoCompras($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert($this->tabPadrao, $array, false));
        $this->commit();
        return $id;
    }

    public function setSolicitacaoComprasItem($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodSolicitacaoComprasItens', $array, false));
        $this->commit();
        return $id;
    }
    //Atualiza o Log
    public function updSolicitacaoCompras($array,$where) {
        //Chave    
        $this->startTransaction();
        $this->transaction($this->update($this->tabPadrao, $array, $where));
        $this->commit();
        return true;
    }
    public function updSolicitacaoComprasItemManut($array,$where) {
        //Chave    
        $this->startTransaction();
        $this->transaction($this->update('prodSolicitacaoComprasItens', $array, $where));
        $this->commit();
        return true;
    }
    //Atualiza o Log
    public function updOrcamentoCompras($array, $where) {
        //Chave    
        $this->startTransaction();
        $this->transaction($this->update('prodOrcamentoCompras', $array, $where));
        $this->commit();
        return true;
    }
    public function updOrcamentoComprasParceiro($array, $where) {
        //Chave    
        $this->startTransaction();
        $this->transaction($this->update('prodOrcamentoComprasParceiro', $array, $where));
        $this->commit();
        return true;
    }
    public function updSolicitacaoComprasItem($array, $where) {
        //Chave    
        $this->startTransaction();
        $this->transaction($this->update('prodSolicitacaoComprasItens', $array, $where));
        $this->commit();
        return true;
    }

    //Remove perfil    
    public function delSolicitacaoComprasItem($where = null) {
        //Chave
        $this->startTransaction();
        $this->transaction($this->delete('prodSolicitacaoComprasItens', $where, true));
        $this->commit();
        return true;
    }
    public function delSolicitacaoCompras($where = null) {
        //Chave
        $this->startTransaction();
        $this->transaction($this->delete('prodSolicitacaoCompras', $where, true));
        $this->commit();
        return true;
    }
}
?>
