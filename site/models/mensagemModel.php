<?php

class mensagemModel extends model {

    var $tabPadrao = 'prodMensagem';
    var $campo_chave = 'idMensagem';
    var $tabPadraoItem = 'prodMensagemItem';
    var $campo_chave_item = 'idMensagemItem';

    public function getMensagem($where = null) {
        return $this->read($this->tabPadrao, array('*'), $where, null, null, null, null);         
    }

    public function getMensagemItem($where = null) {
        $table = 'prodMensagem m inner join prodMensagemItem mi on mi.idMensagem = m.idMensagem'
                . ' inner join prodUsuarios u on u.idUsuario = mi.idUsuarioOrigem'
                . ' inner join prodUsuarios ud on ud.idUsuario = mi.idUsuarioDestino'
                . ' inner join prodTabelasSistema ts on ts.dsTabela = m.dsNomeTabela';
        $fields = array('m.*', 'u.dsUsuario as enviadopor', 'ud.dsUsuario as enviadopara','mi.*','ts.dsNomeAmigavel');
        return $this->read($table, $fields, $where,null,null,null,'m.idMensagemAnterior desc, m.idMensagem  desc');         
    }

    //Grava o perfil
    public function setMensagem($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert($this->tabPadrao, $array, false));
        $this->commit();
        return $id;
    }

    public function setMensagemItem($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodMensagemItem' ,$array, false));
        $this->commit();
        return $id;
    }

    //Atualiza o Log
    public function updMensagemItem($array, $where) {
        //Chave    
        $this->startTransaction();
        $this->transaction($this->update('prodMensagemItem', $array, $where));
        $this->commit();
        return true;
    }
    public function updMensagem($array, $where) {
        //Chave    
        $this->startTransaction();
        $this->transaction($this->update('prodMensagem', $array, $where));
        $this->commit();
        return true;
    }

    //Remove perfil    
    public function delMensagem($array) {
        //Chave
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->delete($this->tabPadrao, $where, true));
        $this->commit();
        return true;
    }

    public function delMensagemItem($array) {
        //Chave
        $where = $this->campo_chave_item . " = " . $array[$this->campo_chave_item];
        $this->startTransaction();
        $this->transaction($this->delete($this->tabPadraoItem, $where, true));
        $this->commit();
        return true;
    }

}

?>
