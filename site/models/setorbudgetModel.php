<?php

class setorbudgetModel extends model {

    var $tabPadrao = 'prodSetorBudget';
    var $campo_chave = 'idSetorBudget';

    //Estrutura da Tabela Vazia Utilizada para novos Cadastros
    public function estrutura_vazia() {
        $dados = null;
        $dados[0]['idSetorBudget'] = NULL;
        $dados[0]['idSetor'] = NULL;
        $dados[0]['idAnoMes'] = NULL;
        $dados[0]['vlBudget'] = NULL;
        return $dados;
    }

    public function getSetor($where = null) {
        return $this->read('prodSetor', array('*'), $where, null, null, null, null);         
    }
    
    public function getSetorBudget($where = null, $orderby = null) {
        return $this->read('prodSetor s '
                . ' left join prodSetorBudget sb on s.idSetor = sb.idSetor', array('*'), $where, null, null, null, $orderby,null,null,null);         
    }

    //Grava o perfil
    public function setSetorBudget($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert($this->tabPadrao, $array, false));
        $this->commit();
        return $id;
    }

    //Atualiza o Log
    public function updSetorBudget($array) {
        //Chave    
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->update($this->tabPadrao, $array, $where));
        $this->commit();
        return true;
    }

    //Remove perfil    
    public function delSetorBudget($array) {
        //Chave
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->delete($this->tabPadrao, $where, true));
        $this->commit();
        return true;
    }

}

?>
