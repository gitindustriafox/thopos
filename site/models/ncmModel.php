<?php

class ncmModel extends model {

    var $tabPadrao = 'prodNCM';
    var $campo_chave = 'idNCM';

    //Estrutura da Tabela Vazia Utilizada para novos Cadastros
    public function estrutura_vazia() {
        $dados = null;
        $dados[0]['idNCM'] = NULL;
        $dados[0]['dsNCM'] = NULL;
        return $dados;
    }

    public function getNCM($where = null, $paginacao = null) {
        return $this->read($this->tabPadrao, array('*'), $where, null, null, null, 'idNCM',null,$paginacao);         
    }

    public function getNCMCombo($where = null) {
        $table = "prodNCM c";
        $fields = array("c.*, CONCAT(c.cdNCM, ' - ', c.dsNCM) as codigoNCM");
        return $this->read($table, $fields, $where, null, null, null, 'idNCM');         
    }

    //Grava o perfil
    public function setNCM($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert($this->tabPadrao, $array, false));
        $this->commit();
        return $id;
    }

    //Atualiza o Log
    public function updNCM($array) {
        //Chave    
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->update($this->tabPadrao, $array, $where));
        $this->commit();
        return true;
    }

    //Remove perfil    
    public function delNCM($array) {
        //Chave
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->delete($this->tabPadrao, $where, true));
        $this->commit();
        return true;
    }

}

?>
