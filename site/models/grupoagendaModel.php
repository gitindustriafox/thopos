<?php

class grupoagendaModel extends model {

    var $tabPadrao = 'prodGrupoAgenda';
    var $campo_chave = 'idGrupoAgenda';

    //Estrutura da Tabela Vazia Utilizada para novos Cadastros
    public function estrutura_vazia() {
        $dados = null;
        $dados[0]['idGrupoAgenda'] = NULL;
        $dados[0]['dsGrupoAgenda'] = NULL;
        return $dados;
    }

    public function getGrupoAgenda($where = null) {
        return $this->read($this->tabPadrao, array('*'), $where, null, null, null, 'dsGrupoAgenda');         
    }

    //Grava o perfil
    public function setGrupoAgenda($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert($this->tabPadrao, $array, false));
        $this->commit();
        return $id;
    }

    //Atualiza o Log
    public function updGrupoAgenda($array) {
        //Chave    
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->update($this->tabPadrao, $array, $where));
        $this->commit();
        return true;
    }

    //Remove perfil    
    public function delGrupoAgenda($array) {
        //Chave
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->delete($this->tabPadrao, $where, true));
        $this->commit();
        return true;
    }

}

?>
