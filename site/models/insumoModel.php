<?php

class insumoModel extends model {

    var $tabPadrao = 'prodInsumo';
    var $campo_chave = 'idInsumo';

    //Estrutura da Tabela Vazia Utilizada para novos Cadastros
    public function estrutura_vazia() {
        $dados = null;
        $dados[0]['idInsumo'] = NULL;
        $dados[0]['dsInsumo'] = NULL;
        return $dados;
    }
    
    public function getInsumo($where = null, $paginacao=null, $ordem = null, $comfispq = null) {
        $tables = " prodInsumo as a";
        $tables .= " left join prodGrupo as d on d.idGrupo = a.idGrupo";
        $tables .= " left join prodMarca as m on m.idMarca = a.idMarca";
        $tables .= " left join prodUnidade as u on u.idUnidade = a.idUnidade";
        $tables .= " left join prodModelo as c on c.idModelo = a.idModelo";
        $tables .= " left join prodParceiro as f on f.idParceiro = a.idParceiro";
        $tables .= " left join prodNCM as ncm on ncm.idNCM = a.idNCM";
        
        if ($comfispq) {
            $tables .= " inner join prodDocumentos as doc on doc.dsTabela = 'prodInsumo' and doc.idTabela = a.idInsumo";
        }
        
        if ($ordem) {
            $orderby = 'a.dtCadastro desc, a.dsInsumo';
        } else {
            $orderby = 'a.dsInsumo';
        }
        $fields = array("if(d.dsGrupo like '%QUIMICO%' ,1,0) as stQuimico"
            , "if(a.stControlado =0 ,'NAO','SIM') as controlado"            
            , "u.dsUnidade"
            , "a.*"
            , "ncm.cdNCM"
            , "ncm.dsNCM" 
            , "d.dsGrupo"
            , "m.dsMarca"
            , "c.dsModelo"
            , "f.dsParceiro"            
        );
        return $this->read($tables, $fields, $where, 'a.idInsumo', null, null, $orderby,null,$paginacao,false);
    }

    public function getInsumoPP($where = null) {
        $tables = 'prodInsumo as a';
        $tables .= ' left join prodGrupo as d on d.idGrupo = a.idGrupo';
        $tables .= ' left join prodMarca as m on m.idMarca = a.idMarca';
        $tables .= ' left join prodModelo as c on c.idModelo = a.idModelo';
        $tables .= ' left join prodParceiro as f on f.idParceiro = a.idParceiroUltimaCompra';
        $orderby = 'a.dsInsumo';
        return $this->read($tables, array('a.*', 'd.dsGrupo', 'm.dsMarca', 'c.dsModelo', 'f.dsParceiro'), $where, null, null, null, $orderby);
    }
    //Grava o perfil
    public function setInsumo($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert($this->tabPadrao, $array, false));
        $this->commit();
        return $id;
    }

    //Atualiza o Log
    public function updInsumo($array) {
        //Chave    
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->update($this->tabPadrao, $array, $where));
        $this->commit();
        return true;
    }

    //Remove perfil    
    public function delInsumo($array) {
        //Chave
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->delete($this->tabPadrao, $where, true));
        $this->commit();
        return true;
    }
}
?>
