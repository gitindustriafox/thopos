<?php

class pedidoModel extends model {

    var $tabPadrao = 'prodPedido';
    var $campo_chave = 'idPedido';

    //Estrutura da Tabela Vazia Utilizada para novos Cadastros
    public function estrutura_vazia() {
        $dados = null;
        $dados[0]['idPedido'] = '';
        $dados[0]['dtPedido'] = NULL;
        $dados[0]['nrPedido'] = NULL;
        $dados[0]['idParceiro'] = NULL;
        $dados[0]['idUsuario'] = NULL;
        $dados[0]['dsObservacao'] = NULL;
        return $dados;
    }
    
    public function getPedidoEmAberto($where = null) {
        $tables = 'prodPedido as a';
        $tables .= ' left join prodParceiro as m on m.idParceiro = a.idParceiro';
        $tables .= ' left join prodUsuarios as u on u.idUsuario = a.idUsuarioBaixa';
        $tables .= ' left join prodEmpresa as emp on emp.idEmpresa = a.idEmpresa';
        $tables .= ' left join prodPrioridade as p on p.idPrioridade = a.idPrioridade';
        $tables .= ' left join prodSituacaoPedido as d on d.idSituacaoPedido = a.idSituacaoPedido';        
        $tables .= ' left join prodSolicitacaoComprasItens as si on si.idPedido = a.idPedido';        
        $tables .= ' left join prodSolicitacaoCompras as s on s.idSolicitacao = si.idSolicitacao';        
        $group_by = 'a.idPedido';
        return $this->read($tables, array('a.*', 'emp.dsEmpresa', 'p.dsPrioridade','d.dsSituacaoPedido', 'm.dsParceiro', 'u.dsUsuario'), $where, $group_by, null, null, 'idPedido desc');
    }

    public function getPedido($where = null, $paginacao=true) {
        $tables = 'prodPedido as a';
        $tables .= ' left join prodParceiro as m on m.idParceiro = a.idParceiro';
        $tables .= ' left join prodUsuarios as u on u.idUsuario = a.idUsuarioBaixa';
        $tables .= ' left join prodEmpresa as emp on emp.idEmpresa = a.idEmpresa';
        $tables .= ' left join prodPedidoItens as i on i.idPedido = a.idPedido';
        $tables .= ' left join prodSolicitacaoComprasItens as si on si.idPedido = i.idPedido and si.idPedidoItem = i.idPedidoItem';
        $tables .= ' left join prodSolicitacaoCompras as sc on sc.idSolicitacao = si.idSolicitacao';
        $tables .= ' left join prodInsumo as prod on prod.idInsumo = i.idInsumo';        
        $tables .= ' left join prodOrigemInformacao as o on o.idOrigemInformacao = a.idOrigemInformacao';
        $tables .= ' left join prodSituacaoPedido as d on d.idSituacaoPedido = a.idSituacaoPedido';        
        $tables .= ' left join prodCentroCusto as cc on cc.idCentroCusto = i.idCentroCusto';        
        $tables .= ' left join prodGrupoDR as grdr on grdr.idGrupoDR = i.idGrupoDR';        
        $tables .= ' left join prodItemDR as itdr on itdr.idItemDR = i.idItemDR';        
        $tables .= ' left join prodPrioridade as pr on pr.idPrioridade = a.idPrioridade';        
        $group_by = 'a.idPedido';
        return $this->read($tables, array('i.dsJustificativa','pr.dsPrioridade','grdr.dsGrupoDR','itdr.dsItemDR','cc.dsCentroCusto','m.*','a.*', 'a.idParceiro as parceiro', 'emp.cdCNPJ as cnpjempresa','emp.dsCelular as celularempresa','emp.dsContato as contatoempresa','emp.dsEndereco as enderecoempresa','emp.dsCidade as cidadeempresa','emp.dsBairro as bairroempresa','emp.dsEmpresa','i.idInsumo' ,'i.qtPedido','i.qtEntregue','i.idPedidoItem','d.dsSituacaoPedido', 'prod.dsInsumo', 'o.dsOrigemInformacao', 'u.dsUsuario', 'sum(i.vlPedido) as vlTotalPedido', 'sc.dsObservacao as obssol','si.dsObservacao as obsitem'), $where, $group_by, null, null, 'idPedido desc',null,$paginacao,false);
    }
    public function getPedidoCabec($where = null, $paginacao=true) {
        $tables = 'prodPedido as a';
        $tables .= ' left join prodParceiro as m on m.idParceiro = a.idParceiro';
        $tables .= ' left join prodUsuarios as u on u.idUsuario = a.idUsuarioBaixa';
        $tables .= ' left join prodEmpresa as emp on emp.idEmpresa = a.idEmpresa';
        $tables .= ' left join prodSituacaoPedido as d on d.idSituacaoPedido = a.idSituacaoPedido';        
        return $this->read($tables, array('m.*','a.*', 'emp.cdCNPJ as cnpjempresa','emp.dsCelular as celularempresa','emp.dsContato as contatoempresa','emp.dsEndereco as enderecoempresa','emp.dsCidade as cidadeempresa','emp.dsBairro as bairroempresa','emp.dsEmpresa', 'u.dsUsuario'), $where);
    }

    public function getTotalPedido($where = null) {
        $tables = 'prodPedido p inner join prodPedidoItens i on p.idPedido = i.idPedido';
        $campos = array('sum(i.vlPedido) as TotalPedido');
        return $this->read($tables, $campos, $where);
    }

    public function getFinanceiro($where = null) {
        $tables = 'prodFinanceiro';
        return $this->read($tables, array('*'), $where, null, null, null, null);
    }
    public function getFinanceiroParcelas($where = null) {
        $tables = 'prodFinanceiroParcelas';
        return $this->read($tables, array('*'), $where, null, null, null, null);
    }
    public function getFinanceiroParcelasEmAberto($where = null) {
        $tables = 'producao.prodFinanceiroParcelas parc 
            inner join producao.prodFinanceiro fin on fin.idFinanceiro = parc.idFinanceiro
            inner join producao.prodPedido ped on ped.idPedido = fin.idPedido
            inner join producao.prodParceiro forn on ped.idParceiro = forn.idParceiro';
        $campos = array('ped.nrPedido, ped.idPedido, parc.nrParcela, parc.idFinanceiroParcela, parc.vlParcela, parc.dtVencimento, ped.dsObservacao, forn.dsParceiro');
        $orderby = 'parc.dtVencimento';
        return $this->read($tables, $campos, $where,null ,null,null, $orderby, null, null);
    }
    public function getFinanceiroParcelasEmAbertoMes($where = null) {
        $tables = 'producao.prodFinanceiroParcelas parc 
            inner join producao.prodFinanceiro fin on fin.idFinanceiro = parc.idFinanceiro
            inner join producao.prodPedido ped on ped.idPedido = fin.idPedido
            inner join producao.prodParceiro forn on ped.idParceiro = forn.idParceiro';
        $campos = array("sum(parc.vlParcela) as total, date_format(parc.dtVencimento,'%M %Y') as datavencimento");
        $orderby = 'parc.dtVencimento';
        $groupby = 'year(parc.dtVencimento), month(parc.dtVencimento)';
        return $this->read($tables, $campos, $where,$groupby ,null,null, $orderby, null, null);
    }
    public function getPedidoItens($where = null) {
        $tables = 'prodPedidoItens as a';
        $tables .= ' left join prodPedido as i on i.idPedido = a.idPedido';
        $tables .= ' left join prodInsumo as d on d.idInsumo = a.idInsumo';
        $tables .= ' left join prodServicos as s on s.idServico = a.idInsumo';
        $tables .= ' left join prodCentroCusto as cc on cc.idCentroCusto = a.idCentroCusto';
        $tables .= ' left join prodGrupoDR as grdr on grdr.idGrupoDR = a.idGrupoDR';        
        $tables .= ' left join prodItemDR as itdr on itdr.idItemDR = a.idItemDR';        
        $tables .= ' left join prodLocalEstoque as le on le.idLocalEstoque = a.idLocalEstoque';
        $tables .= ' left join prodSituacaoPedido as sit on sit.idSituacaoPedido = a.idSituacaoPedido';
        $tables .= ' left join prodGrupo as m on m.idGrupo = d.idGrupo';
        $tables .= ' left join prodNCM as ncm on ncm.idNCM = d.idNCM';
        $orderby = 'a.idPedidoItem';
        return $this->read($tables, array('grdr.dsGrupoDR','itdr.dsItemDR','ncm.cdNCM','a.*','sit.dsSituacaoPedido','le.dsLocalEstoque','le.cdLocalEstoque' ,'cc.dsCentroCusto','d.cdInsumo' ,'s.dsServico','d.dsInsumo', 'm.dsGrupo', '(a.vlPedido / a.qtPedido) as vlUnitario','le.idLocalEstoqueSuperior'), $where, null, null, null, $orderby);
    }

    public function getUltimoPedido($where = null) {
        return $this->read('prodPedido', array('max(nrPedido) as ultimo'), null, null, null, null, null);
    }

    public function getTotalPedidoItens($where = null) {
        $tables = 'prodPedidoItens as a';
        $tables .= ' left join prodPedido as i on i.idPedido = a.idPedido';
        return $this->read($tables, array('sum(a.vlPedido) as totalpedido'), $where, null, null, null, null);
    }

    //Grava o perfil
    public function setPedido($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert($this->tabPadrao, $array, false));
        $this->commit();
        return $id;
    }

    public function setPedidoFinanceiro($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodFinanceiro', $array, false));
        $this->commit();
        return $id;
    }

    public function setPedidoFinanceiroItem($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodFinanceiroParcelas', $array, false));
        $this->commit();
        return $id;
    }

    public function setOrcamentoCompras($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodOrcamentoCompras', $array, false));
        $this->commit();
        return $id;
    }
    public function setOrcamentoComprasItens($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodOrcamentoComprasItens', $array, false));
        $this->commit();
        return $id;
    }
    public function setOrcamentoComprasParceiro($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodOrcamentoComprasParceiro', $array, false));
        $this->commit();
        return $id;
    }
    public function setPedidoItem($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodPedidoItens', $array, false));
        $this->commit();
        return $id;
    }
    //Atualiza o Log
    public function updPedido($array,$where) {
        //Chave    
        $this->startTransaction();
        $this->transaction($this->update($this->tabPadrao, $array, $where));
        $this->commit();
        return true;
    }
    //Atualiza o Log
    public function updPedidoItem($array, $where) {
        //Chave    
        $this->startTransaction();
        $this->transaction($this->update('prodPedidoItens', $array, $where));
        $this->commit();
        return true;
    }

    public function updFinanceiroItem($array, $where) {
        $this->startTransaction();
        $this->transaction($this->update('prodFinanceiroParcelas', $array, $where));
        $this->commit();
        return true;
    }
    //Remove perfil    
    public function delPedidoItem($where = null) {
        //Chave
        $this->startTransaction();
        $this->transaction($this->delete('prodPedidoItens', $where, true));
        $this->commit();
        return true;
    }
    public function delFinanceiroItem($where = null) {
        //Chave
        $this->startTransaction();
        $this->transaction($this->delete('prodFinanceiroParcelas', $where, true));
        $this->commit();
        return true;
    }
    public function delPedido($where = null) {
        //Chave
        $this->startTransaction();
        $this->transaction($this->delete('prodPedido', $where, true));
        $this->commit();
        return true;
    }
}
?>
