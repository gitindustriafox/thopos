<?php

class planodecontasModel extends model {

    var $tabPadrao = 'prodPlanoDeContas';
    var $campo_chave = 'idConta';

    //Estrutura da Tabela Vazia Utilizada para novos Cadastros
    public function estrutura_vazia() {
        $dados = null;
        $dados[0]['idConta'] = NULL;
        $dados[0]['idPlanoDeContas'] = NULL;
        $dados[0]['idContaReduzida'] = NULL;
        $dados[0]['idCE1'] = NULL;
        $dados[0]['idCE2'] = NULL;
        $dados[0]['idCE3'] = NULL;
        $dados[0]['idCE4'] = NULL;
        $dados[0]['idCE5'] = NULL;
        $dados[0]['dsConta'] = NULL;
        $dados[0]['stTipoConta'] = NULL;
        $dados[0]['stAberta'] = NULL;
        $dados[0]['dtCriacao'] = NULL;
        return $dados;
    }
    
    public function getPlanoDeContas($where = null, $paginacao=null) {
        $tables = 'prodPlanoDeContas pc inner join prodPeriodoContabil p on p.idPeriodo = pc.idPeriodo';
        $orderby = 'idCE1, idCE2, idCE3, idCE4, idCE5';
        return $this->read($tables, array('*'), $where, null, null, null, $orderby,null,$paginacao);
    }

    public function setPlanoDeContas($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert($this->tabPadrao, $array, false));
        $this->commit();
        return $id;
    }

    public function updPlanoDeContas($array) {
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->update($this->tabPadrao, $array, $where));
        $this->commit();
        return true;
    }
    public function updPlanoDeContasCP($array) {
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->update($this->tabPadrao, $array, $where));
        $this->commit();
        return true;
    }

    public function delPlanoDeContas($where) {
        $this->startTransaction();
        $this->transaction($this->delete($this->tabPadrao, $where, true));
        $this->commit();
        return true;
    }
}
?>
