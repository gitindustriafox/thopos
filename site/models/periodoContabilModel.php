<?php

class periodoContabilModel extends model {

    var $tabPadrao = 'prodPeriodoContabil';
    var $campo_chave = 'idPeriodo';

    //Estrutura da Tabela Vazia Utilizada para novos Cadastros
    public function estrutura_vazia() {
        $dados = null;
        $dados[0]['idPeriodo'] = NULL;
        $dados[0]['dsPeriodo'] = NULL;
        $dados[0]['dtCriacao'] = NULL;
        $dados[0]['stSituacao'] = NULL;
        $dados[0]['dtInicial'] = NULL;
        $dados[0]['dtFinal'] = NULL;
        return $dados;
    }
    
    public function getPeriodo($where = null, $paginacao=null) {
        $tables = 'prodPeriodoContabil';
        $orderby = 'dsPeriodo';
        return $this->read($tables, array('*'), $where, null, null, null, $orderby,null,$paginacao);
    }
    public function getUltimoPeriodo() {
        $tables = 'prodPeriodoContabil';
        $orderby = 'idPeriodo desc';
        return $this->read($tables, array('*'), null, null, 1, null, $orderby);
    }
    //Grava o perfil
    public function setPeriodo($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert($this->tabPadrao, $array, false));
        $this->commit();
        return $id;
    }

    //Atualiza o Log
    public function updPeriodo($array) {
        //Chave    
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->update($this->tabPadrao, $array, $where));
        $this->commit();
        return true;
    }

    //Remove perfil    
    public function delPeriodo($array) {
        //Chave
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->delete($this->tabPadrao, $where, true));
        $this->commit();
        return true;
    }
}
?>
