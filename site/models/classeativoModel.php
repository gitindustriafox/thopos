<?php

class classeativoModel extends model {

    var $tabPadrao = 'prodClasseAtivo';
    var $campo_chave = 'idClasseAtivo';

    //Estrutura da Tabela Vazia Utilizada para novos Cadastros
    public function estrutura_vazia() {
        $dados = null;
        $dados[0]['idClasseAtivo'] = NULL;
        $dados[0]['dsClasseAtivo'] = NULL;
        $dados[0]['vlTaxaDepreciacao'] = NULL;
        return $dados;
    }

    public function getClasseAtivo($where = null) {
        return $this->read($this->tabPadrao, array('*'), $where, null, null, null, null);         
    }

    public function getItem($where = null) {
        $table = 'prodGrupoAtivo';
        return $this->read($table, array('*'), $where);         
    }

    //Grava o perfil
    public function setClasseAtivo($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert($this->tabPadrao, $array, false));
        $this->commit();
        return $id;
    }

    public function setItem($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodGrupoAtivo', $array, false));
        $this->commit();
        return $id;
    }

    //Atualiza o Log
    public function updClasseAtivo($array) {
        //Chave    
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->update($this->tabPadrao, $array, $where));
        $this->commit();
        return true;
    }

    //Remove perfil    
    public function delClasseAtivo($array) {
        //Chave
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->delete($this->tabPadrao, $where, true));
        $this->commit();
        return true;
    }
    public function delItem($where = null) {
        //Chave
        $this->startTransaction();
        $this->transaction($this->delete('prodGrupoAtivo', $where, true));
        $this->commit();
        return true;
    }

}
?>
