<?php

class orcamentoModel extends model {

    var $tabPadrao = 'prodOrcamento';
    var $campo_chave = 'idOrcamento';

    //Estrutura da Tabela Vazia Utilizada para novos Cadastros
    public function estrutura_vazia() {
        $dados = null;
        $dados[0]['idOrcamento'] = NULL;
        $dados[0]['idUsuarioProprietario'] = $_SESSION['user']['usuario'];
        $dados[0]['dsOrcamento'] = NULL;
        $dados[0]['dtCriacao'] = NULL;
        $dados[0]['idEmpresa'] = NULL;
        $dados[0]['idStatusOrcamento'] = NULL;
        $dados[0]['vlOrcadoTotal'] = NULL;
        $dados[0]['dsTermoAbertura'] = NULL;
        $dados[0]['dsTermoEncerramento'] = NULL;
        $dados[0]['dtInicioVigencia'] = NULL;
        $dados[0]['dtFimVigencia'] = NULL;
        $dados[0]['stStatus'] = NULL;
        $dados[0]['vlSaldoInicial'] = NULL;
        return $dados;
    }
    
    public function getOrcamento($where = null, $paginacao=null) {
        $tables = 'prodOrcamento o inner join prodStatusOrcamento s on o.idStatusOrcamento = s.idStatusOrcamento';
        $orderby = 'dsOrcamento';
        return $this->read($tables, array('o.*, s.dsStatusOrcamento'), $where, null, null, null, $orderby,null,$paginacao);
    }
    public function getOrcamentoLancar($usuario) {
        $tables = 'prodOrcamento o
        left join prodStatusOrcamento s on o.idStatusOrcamento = s.idStatusOrcamento
        left join prodColaborador c on c.idUsuario = o.idUsuarioProprietario
        left join prodUsuarios u on u.idUsuario = c.idUsuario
        left join prodOrcamentoColaboradores oc on oc.idOrcamento = o.idOrcamento
        left join prodColaborador co on oc.idColaborador = co.idColaborador and co.idUsuario = ' 
        . $usuario . ' left join prodOrcamentoColaboradoresCC occ on occ.idOrcamento = oc.idOrcamento
	and occ.idColaborador = oc.idColaborador';
        $groupby = 'occ.idOrcamento, occ.idColaborador';
        return $this->read($tables, array('o.*','u.dsUsuario','oc.idColaborador','s.dsStatusOrcamento'), null, $groupby,null,null,null,null,null,false);
    }

    public function getOrcamentoItemDR($where = null) {
        $tables = 'prodOrcamentoLancamento';
        return $this->read($tables, array('*'), $where);
    }

    public function getOrcamentoTotalInsumos($where = null) {
        $tables = 'prodOrcamentoInsumos as a';
        return $this->read($tables, array('sum(a.vlTotal) as total'), $where, null, null, null);
    }
    public function getOrcamentoTotalMaoObra($where = null) {
        $tables = 'prodOrcamentoMaoObra as a';
        return $this->read($tables, array('sum(a.vlTotal) as total'), $where, null, null, null);
    }
    public function getOrcamentoTotalMaquina($where = null) {
        $tables = 'prodOrcamentoMaquinas as a';
        return $this->read($tables, array('sum(a.vlTotal) as total'), $where, null, null, null);
    }
//    public function getOrcamentoColaboradores($where = null) {
//        $filds = array("oc.idOrcamentoColaborador, b.dsCargo, c.dsSetor, cc.dsEmail, occ.*, cc.dsColaborador");
//        $tables = 'prodOrcamentoColaboradoresCC as occ
//            inner join prodColaborador as cc on cc.idColaborador = occ.idColaborador
//            inner join prodOrcamentoColaboradores as oc on oc.idOrcamento = occ.idOrcamento and oc.idColaborador = occ.idColaborador
//            left join prodCargo as b on b.idCargo = cc.idCargo
//            left join prodSetor as c on c.idSetor = cc.idSetor';
//        $orderby = 'cc.dsColaborador';
//        return $this->read($tables, $filds, $where, null, null, null, $orderby);
//    }
    public function getTotais($where=null, $groupby=null) {
        $filds = array("sum(l.vlOrcado) as  vlTotal","gd.dsGrupoDR");
        $tables = "prodOrcamentoLancamento l 
            inner join prodOrcamentoColaboradoresCC occ on occ.idOrcamentoColaboradorCC = l.idOrcamentoColaboradorCC 
            left join prodGrupoDR gd on gd.idGrupoDR = l.idGrupoDR 
            left join prodItemDR idr on idr.idItemDR = l.idItemDR and idr.idGrupoDR = gd.idGrupoDR";
        return $this->read($tables, $filds, $where, $groupby,null,null,null,null,null,false);
    }
    public function getTotaisSoma($where=null, $groupby=null) {
        $filds = array("sum(l.vlOrcado) as  vlTotal");
        $tables = "prodOrcamentoLancamento l 
            inner join prodOrcamentoColaboradoresCC occ on occ.idOrcamentoColaboradorCC = l.idOrcamentoColaboradorCC 
            left join prodGrupoDR gd on gd.idGrupoDR = l.idGrupoDR 
            left join prodItemDR idr on idr.idItemDR = l.idItemDR and idr.idGrupoDR = gd.idGrupoDR";
        return $this->read($tables, $filds, $where, $groupby,null,null,null,null,null,false);
    }
    public function getOrcamentoGRIT($where = null) {
        $tables = "prodOrcamentoLancamento l 
        INNER JOIN prodOrcamentoColaboradoresCC occ ON occ.idOrcamentoColaboradorCC = l.idOrcamentoColaboradorCC
        LEFT JOIN
    prodGrupoDR gd ON gd.idGrupoDR = l.idGrupoDR
        LEFT JOIN
    prodItemDR idr ON idr.idItemDR = l.idItemDR
        AND idr.idGrupoDR = gd.idGrupoDR";
        $fields = array("l.idItemDR, gd.dsGrupoDR, idr.dsItemDR, gd.stGrupoDR");
        $groupby = "gd.dsGrupoDR, idr.dsItemDR";
        $orderby = "gd.stGrupoDR, gd.dsGrupoDR, idr.dsItemDR";
        return $this->read($tables, $fields, $where, $groupby,null,null,$orderby);
    }
    public function getOrcamentoValores($where = null) {
        $tables = "prodOrcamentoLancamento l 
        INNER JOIN prodOrcamentoColaboradoresCC occ ON occ.idOrcamentoColaboradorCC = l.idOrcamentoColaboradorCC
        LEFT JOIN
    prodGrupoDR gd ON gd.idGrupoDR = l.idGrupoDR
        LEFT JOIN
    prodItemDR idr ON idr.idItemDR = l.idItemDR
        AND idr.idGrupoDR = gd.idGrupoDR";
        $fields = array("l.idItemDR, gd.dsGrupoDR, idr.dsItemDR, sum(vlOrcado) as valormes, CONCAT(substr(dtValorOrcado,6,2),'/',substr(dtValorOrcado,1,4)) as mesano, CONCAT(substr(dtValorOrcado,1,4),substr(dtValorOrcado,6,2)) as idmesano");
        $groupby = "gd.dsGrupoDR, idr.dsItemDR,CONCAT(substr(dtValorOrcado,6,2),'/',substr(dtValorOrcado,1,4))";

        return $this->read($tables, $fields, $where, $groupby);
    }
    public function getOrcamentoGrupoValores($where = null, $groupby = null) {
        $tables = "prodOrcamentoLancamento l 
        LEFT JOIN
    prodGrupoDR gd ON gd.idGrupoDR = l.idGrupoDR
    LEFT JOIN prodOrcamentoColaboradoresCC occ on occ.idOrcamentoColaboradorCC = l.idOrcamentoColaboradorCC";
        $fields = array("gd.dsGrupoDR, sum(vlOrcado) as valormes, CONCAT(substr(dtValorOrcado,6,2),'/',substr(dtValorOrcado,1,4)) as mesano, CONCAT(substr(dtValorOrcado,1,4),substr(dtValorOrcado,6,2)) as idmesano");
        return $this->read($tables, $fields, $where, $groupby,null,null,null,null,null,false);
    }
    
    public function getOrcamentoGrupoValoresT($where = null, $groupby = null) {
        $tables = "prodOrcamentoLancamento l 
        LEFT JOIN
    prodGrupoDR gd ON gd.idGrupoDR = l.idGrupoDR 
    LEFT JOIN prodOrcamentoColaboradoresCC occ on occ.idOrcamentoColaboradorCC = l.idOrcamentoColaboradorCC";
        $fields = array("sum(vlOrcado) as valormes");
        return $this->read($tables, $fields, $where, $groupby,null,null,null,null,null,false);
    }
    
    public function getOrcamentoColaboradores($where = null) {
        $tables = 'prodOrcamentoColaboradores as oc';
        $tables .= ' left join prodColaborador as a on a.idColaborador = oc.idColaborador';
        $tables .= ' left join prodCargo as b on b.idCargo = a.idCargo';
        $tables .= ' left join prodSetor as c on c.idSetor = a.idSetor';
        $orderby = 'a.dsColaborador';
        return $this->read($tables, array('a.*', 'b.dsCargo', 'c.dsSetor','oc.*'), $where, null, null, null, $orderby);
    }
    public function getOrcamentoColaboradoresCC($where = null) {
        $filds = array("occ.*, cc.dsCentroCusto, cc.cdCentroCusto");
        $groupby = "occ.idOrcamento, occ.idColaborador, occ.idCentroCusto";
        $tables = 'prodOrcamentoColaboradoresCC as occ
            inner join prodCentroCusto as cc on cc.idCentroCusto = occ.idCentroCusto';
        $orderby = 'cc.dsCentroCusto';
        return $this->read($tables, $filds, $where, $groupby, null, null, $orderby);
    }
    public function getOrcamentoCC($where = null) {
        $filds = array("*");
        $tables = 'prodOrcamentoColaboradoresCC ccc inner join prodCentroCusto cc on cc.idCentroCusto = ccc.idCentroCusto';
        return $this->read($tables, $filds, $where);
    }
    public function getOrcamentoCCSoma($where = null) {
        $filds = array("sum(vlLimiteReceitas) as valorlimitereceitas","sum(vlLimiteDespesas) as valorlimitedespesas");
        $tables = 'prodOrcamentoColaboradoresCC ccc inner join prodCentroCusto cc on cc.idCentroCusto = ccc.idCentroCusto';
        return $this->read($tables, $filds, $where);
    }
    public function getOrcamentoLancamento($where = null) {
        $tables = 'prodOrcamentoLancamento l inner join prodGrupoDR g on g.idGrupoDR = l.idGrupoDR'
                . ' inner join prodOrcamentoColaboradoresCC occ on occ.idOrcamentoColaboradorCC = l.idOrcamentoColaboradorCC'
                . ' inner join prodItemDR i on i.idItemDR = l.idItemDR';
        $orderby = 'g.dsGrupoDR, i.dsItemDR, l.dtValorOrcado';
        return $this->read($tables, array('l.*','g.dsGrupoDR','i.dsItemDR','occ.stSituacao as situacaocc'), $where,null,null,null,$orderby);
    }
    //Grava o perfil
    public function setOrcamento($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert($this->tabPadrao, $array, false));
        $this->commit();
        return $id;
    }

    public function setOrcamentoColaborador($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodOrcamentoColaboradores', $array, false));
        $this->commit();
        return $id;
    }
    
    public function setOrcamentoCentroCusto($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodOrcamentoColaboradoresCC', $array, false));
        $this->commit();
        return $id;
    }
    public function setOrcamentoOrcado($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodOrcamentoLancamento', $array, false));
        $this->commit();
        return $id;
    }
    //Atualiza o Log
    public function updOrcamento($array) {
        //Chave    
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->update($this->tabPadrao, $array, $where));
        $this->commit();
        return true;
    }
    public function updOrcamentoCC($array, $id) {
        //Chave    
        $where =  "idOrcamentoColaboradorCC = " . $id;
        $this->startTransaction();
        $this->transaction($this->update('prodOrcamentoColaboradoresCC', $array, $where));
        $this->commit();
        return true;
    }
    
    public function updOrcamentoColaboradorCC($array, $where) {
        //Chave    
        $this->startTransaction();
        $this->transaction($this->update('prodOrcamentoColaboradoresCC', $array, $where));
        $this->commit();
        return true;
    }

    //Remove perfil    
    public function delOrcamento($array) {
        //Chave
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->delete($this->tabPadrao, $where, true));
        $this->commit();
        return true;
    }
    public function delOrcamentoColaborador($where) {
        //Chave
        $this->startTransaction();
        $this->transaction($this->delete('prodOrcamentoColaboradores', $where, true));
        $this->commit();
        return true;
    }
    public function delOrcamentoColaboradorCC($where) {
        //Chave
        $this->startTransaction();
        $this->transaction($this->delete('prodOrcamentoColaboradoresCC', $where, true));
        $this->commit();
        return true;
    }
    public function delOrcamentoLancamento($where) {
        //Chave
        $this->startTransaction();
        $this->transaction($this->delete('prodOrcamentoLancamento', $where, true));
        $this->commit();
        return true;
    }
}
?>
