<?php

class grupodrModel extends model {

    var $tabPadrao = 'prodGrupoDR';
    var $campo_chave = 'idGrupoDR';

    //Estrutura da Tabela Vazia Utilizada para novos Cadastros
    public function estrutura_vazia() {
        $dados = null;
        $dados[0]['idGrupoDR'] = NULL;
        $dados[0]['dsGrupoDR'] = NULL;
        $dados[0]['stGrupoDR'] = NULL;
        return $dados;
    }

    public function getGrupoDR($where = null, $orderby = 'dsGrupoDR') {
        return $this->read($this->tabPadrao, array('*'), $where, null, null, null, $orderby);         
    }

    public function getItemDR($where = null) {
        $table = 'prodItemDR i inner join prodGrupoDR g on g.idGrupoDR = i.idGrupoDR'
                . ' left join prodOrcamentoLancamento l on i.idItemDR = l.idItemDr';
        $groupby = 'i.idGrupoDR, i.idItemDR';
        return $this->read($table, array('i.*','l.idLancamento'), $where, $groupby, null, null, 'i.dsItemDR');         
    }

    //Grava o perfil
    public function setGrupoDR($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert($this->tabPadrao, $array, false));
        $this->commit();
        return $id;
    }

    public function setItemDR($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert('prodItemDR', $array, false));
        $this->commit();
        return $id;
    }

    //Atualiza o Log
    public function updGrupoDR($array) {
        //Chave    
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->update($this->tabPadrao, $array, $where));
        $this->commit();
        return true;
    }

    //Remove perfil    
    public function delGrupoDR($array) {
        //Chave
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->delete($this->tabPadrao, $where, true));
        $this->commit();
        return true;
    }
    public function delItemDR($where = null) {
        //Chave
        $this->startTransaction();
        $this->transaction($this->delete('prodItemDR', $where, true));
        $this->commit();
        return true;
    }

}

?>
