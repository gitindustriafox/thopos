<?php

class ativofixoModel extends model {

    var $tabPadrao = 'prodAtivoFixo';
    var $campo_chave = 'idAtivoFixo';

    //Estrutura da Tabela Vazia Utilizada para novos Cadastros
    public function estrutura_vazia() {
        $dados = null;
        $dados[0]['idAtivoFixo'] = NULL;
        $dados[0]['dsAtivoFixo'] = NULL;
        return $dados;
    }
    
    public function getAtivoFixo($where = null, $paginacao=false, $ordem = null) {
        $tables = 'prodAtivoFixo';
        $fields = array("*");
        return $this->read($tables, $fields, $where, null, null, null, null,null,$paginacao,false);
    }

    public function getAtivoFixoPP($where = null) {
        $tables = 'prodAtivoFixo as a';
        $tables .= ' left join prodGrupo as d on d.idGrupo = a.idGrupo';
        $tables .= ' left join prodMarca as m on m.idMarca = a.idMarca';
        $tables .= ' left join prodModelo as c on c.idModelo = a.idModelo';
        $tables .= ' left join prodParceiro as f on f.idParceiro = a.idParceiroUltimaCompra';
        $orderby = 'a.dsAtivoFixo';
        return $this->read($tables, array('a.*', 'd.dsGrupo', 'm.dsMarca', 'c.dsModelo', 'f.dsParceiro'), $where, null, null, null, $orderby);
    }
    //Grava o perfil
    public function setAtivoFixo($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert($this->tabPadrao, $array, false));
        $this->commit();
        return $id;
    }

    //Atualiza o Log
    public function updAtivoFixo($array) {
        //Chave    
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->update($this->tabPadrao, $array, $where));
        $this->commit();
        return true;
    }

    //Remove perfil    
    public function delAtivoFixo($array) {
        //Chave
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->delete($this->tabPadrao, $where, true));
        $this->commit();
        return true;
    }
}
?>
