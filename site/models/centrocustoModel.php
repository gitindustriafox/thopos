<?php

class centrocustoModel extends model {

    var $tabPadrao = 'prodCentroCusto';
    var $campo_chave = 'idCentroCusto';

    //Estrutura da Tabela Vazia Utilizada para novos Cadastros
    public function estrutura_vazia() {
        $dados = null;
        $dados[0]['idCentroCusto'] = NULL;
        $dados[0]['dsCentroCusto'] = NULL;
        return $dados;
    }

    public function getCentroCusto($where = null, $paginacao = null, $qtlinhas = null) {
        return $this->read($this->tabPadrao, array('*'), $where, null, null, null, 'idCentroCusto',null,$paginacao, null,$qtlinhas);         
    }

    public function getCentroCustoCombo($where = null) {
        $table = "prodCentroCusto c";
        $fields = array("c.*, CONCAT(c.cdCentroCusto, ' - ', c.dsCentroCusto) as codigocusto");
        return $this->read($table, $fields, $where, null, null, null, 'c.cdCentroCusto, c.dsCentroCusto');         
    }

    //Grava o perfil
    public function setCentroCusto($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert($this->tabPadrao, $array, false));
        $this->commit();
        return $id;
    }

    //Atualiza o Log
    public function updCentroCusto($array) {
        //Chave    
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->update($this->tabPadrao, $array, $where));
        $this->commit();
        return true;
    }

    //Remove perfil    
    public function delCentroCusto($array) {
        //Chave
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->delete($this->tabPadrao, $where, true));
        $this->commit();
        return true;
    }

}

?>
