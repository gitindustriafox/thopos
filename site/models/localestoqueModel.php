<?php

class localestoqueModel extends model {

    var $tabPadrao = 'prodLocalEstoque';
    var $campo_chave = 'idLocalEstoque';

    //Estrutura da Tabela Vazia Utilizada para novos Cadastros
    public function estrutura_vazia() {
        $dados = null;
        $dados[0]['idLocalEstoque'] = NULL;
        $dados[0]['dsLocalEstoque'] = NULL;
        return $dados;
    }

    public function getLocalEstoque($where = null, $paginacao=null) {
        $fields = array('idLocalEstoque, stAnalitico','stStatus','idLocalEstoqueSuperior','cdLocalEstoque',"dsLocalEstoque");
        return $this->read($this->tabPadrao, $fields, $where, null, null, null, 'cdLocalEstoque',null,$paginacao);         
    }
    public function getLocalEstoqueProdutos($where = null) {
        $tables = 'prodEstoque e inner join prodLocalEstoque le on le.idLocalEstoque = e.idLocalEstoque';
        $fields = array('le.*');
        return $this->read($tables, $fields, $where, null, null, null, 'le.cdLocalEstoque');         
    }
    public function getLocalEstoqueMapa($where = null) {
        $groupby = 'substr(cdLocalEstoque,1,1)';
        $fields = array('substr(cdLocalEstoque,1,1) as cdLocalEstoque','count(cdLocalEstoque) as totallinhas');
        return $this->read($this->tabPadrao, $fields, $where, $groupby, null, null, 'cdLocalEstoque');         
    }
    public function getLocalEstoqueMapaTotalLinhas($where = null) {
        $tables = 'prodEstoque e inner join prodLocalEstoque le on le.idLocalEstoque = e.idLocalEstoque';
        $groupby = 'substr(le.cdLocalEstoque,1,1)';
        $fields = array('le.cdLocalEstoque','count(e.idLocalEstoque) as totallinhasCheias');
        return $this->read($tables, $fields, $where, $groupby);         
    }

    //Grava o perfil
    public function setLocalEstoque($array) {
        $this->startTransaction();
        $id = $this->transaction($this->insert($this->tabPadrao, $array, false));
        $this->commit();
        return $id;
    }

    //Atualiza o Log
    public function updLocalEstoque($array) {
        //Chave    
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->update($this->tabPadrao, $array, $where));
        $this->commit();
        return true;
    }

    //Remove perfil    
    public function delLocalEstoque($array) {
        //Chave
        $where = $this->campo_chave . " = " . $array[$this->campo_chave];
        $this->startTransaction();
        $this->transaction($this->delete($this->tabPadrao, $where, true));
        $this->commit();
        return true;
    }

}

?>
